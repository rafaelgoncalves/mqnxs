module.exports = {
  siteMetadata: {
    title: "Maquinações",
    author: "Rafael Gonçalves",
    lang: "pt-br",
    description:
      "Experimentações teóricas, fichamentos e outros comentários descartáveis",
    siteUrl: "https://maquinacoes.rafaelg.net.br",
    icon: "/favicon.ico",
  },
  plugins: [
    "gatsby-plugin-sass",
    "gatsby-plugin-react-helmet",
    "gatsby-plugin-sitemap",
    "gatsby-plugin-sharp",
    "gatsby-transformer-sharp",
    "gatsby-plugin-fontawesome-css",
    {
      resolve: "gatsby-transformer-remark",
      options: {
        excerpt_separator: "<!-- more -->",
        plugins: [
          {
            resolve: 'gatsby-remark-audio',
            options: {
              preload: 'auto',
              loop: false,
              controls: true,
              muted: false,
              autoplay: false
            }
          },
          {
            resolve: `gatsby-remark-autolink-headers`,
            options: { offsetY: `24` },
          },
          {
            resolve: `gatsby-remark-images`,
            options: {
              // It's important to specify the maxWidth (in pixels) of
              // the content container as this plugin uses this as the
              // base for generating different widths of each image.
              maxWidth: 640,
            },
          },
        ],
      },
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "content",
        path: "./content/",
      },
      __key: "content",
    },
    {
      resolve: "gatsby-plugin-feed",
      options: {
        query: `
        {
          site {
            siteMetadata {
              title
              author
              description
              siteUrl
              site_url: siteUrl
            }
          }
        }
        `,
        feeds: [
          {
            serialize: ({ query: { site, allMarkdownRemark } }) => {
              return allMarkdownRemark.edges.map((edge) => {
                return Object.assign({}, edge.node.frontmatter, {
                  title: edge.node.frontmatter.title,
                  author:
                    edge.node.frontmatter.author || site.siteMetadata.author,
                  description: edge.node.excerpt,
                  date: edge.node.frontmatter.date,
                  url: site.siteMetadata.siteUrl + edge.node.frontmatter.path,
                  guid: site.siteMetadata.siteUrl + edge.node.frontmatter.path,
                  custom_elements: [{ "content:encoded": edge.node.html }],
                });
              });
            },
            query: `
          {
            allMarkdownRemark (
              sort: { order: DESC, fields: [frontmatter___date] },
              ) {
              edges {
                node {
                  excerpt
                  html
                  frontmatter {
                    title
                    author
                    date
                    path
                  }
                }
              }
            }
          }
        `,
            output: "/rss.xml",
            title: "Maquinações",
          },
        ],
      },
    },
  ],
};
