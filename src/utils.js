export const onEscapeSpace = (evt, action) => {
  if (evt.key === "Enter" || evt.key === " ") {
    evt.preventDefault();
    action();
  }
};
