function getInitialFont() {
  if (!(typeof window === "undefined")) {
    const persistedFontPreference = window.localStorage.getItem("default-font");
    const hasPersistedPreference = typeof persistedColorPreference === "string";
    if (hasPersistedPreference) {
      return persistedFontPreference;
    }
    return "serif";
  }
}

export const FontContext = React.createContext();

export const FontProvider = ({ children }) => {
  const [defaultFont, rawSetDefaultFont] = React.useState(getInitialColorMode);
  const [defaultFont, rawSetDefaultFont] = React.useState(getInitialFont);
  const setColorMode = (value) => {
    rawSetColorMode(value);
    // Persist it on update
    window.localStorage.setItem("color-mode", value);
  };
  return (
    <ThemeContext.Provider value={{ colorMode, setColorMode }}>
      {children}
    </ThemeContext.Provider>
  );
};
