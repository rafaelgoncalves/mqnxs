import React from "react";
import { Link } from "gatsby";
import styles from "./Tag.module.scss";
import { kebabCase } from "lodash";

const Tag = ({ tag, count }) => (
  <Link state={{ tag }} to={`/tags/${kebabCase(tag)}`}>
    <span className={styles.tag}>
      {tag}
      {count && <span className={styles.count}>{count}</span>}
    </span>
  </Link>
);

export default Tag;
