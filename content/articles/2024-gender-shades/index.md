---
title: "Tons de gênero: disparidades interseccionais de acurácia em classificadores de gênero comerciais"
tags:
    - Joy Buolamwini
    - Timnit Gebru
    - mediação técnica
    - sexismo algorítmico
    - racismo algorítmico
    - viés
    - gênero
    - microsoft
    - ibm
    - algoritmos
    - algoritmos
    - fichamento

path: "/gender-shades"
date: 2024-11-02
category: fichamento
featuredImage: "./fig1.png"
srcInfo: "Fig. 1: Example images and average faces from the new Pilot Parliaments Benchmark (PPB). As the examples show, the images are constrained with relatively little variation in pose. The subjects are composed of male and female parliamentarians from 6 countries. On average, Senegalese subjects are the darkest skinned while those from Finland and Iceland are the lightest skinned."
published: true
---

Fichamento do artigo "Tons de gênero"[^1] de Joy Buolamwini e Timnit Gebru.

[^1]: Buolamwini, Joy, e Timnit Gebru. 2018. “Gender Shades: Intersectional Accuracy Disparities in Commercial Gender Classiﬁication”. Em Proceedings of Machine Learning Research, 81:1–18. <http://gendershades.org/>.

# Abstract

> Recent studies demonstrate that machine learning algorithms can discriminate based on classes like race and gender. **In this work, we present an approach to evaluate bias present in automated facial analysis algorithms and datasets with respect to phenotypic subgroups.** Using the dermatologist approved Fitzpatrick Skin Type classification system, we characterize the gender and skin type distribution of two facial analysis benchmarks, IJB-A and Adience. **We find that these datasets are overwhelmingly composed of lighter-skinned subjects (79.6% for IJB-A and 86.2% for Adience) and introduce a new facial analysis dataset which is balanced by gender and skin type. We evaluate 3 commercial gender classification systems using our dataset and show that darker-skinned females are the most misclassified group (with error rates of up to 34.7%). The maximum error rate for lighter-skinned males is 0.8%.** The substantial disparities in the accuracy of classifying darker females, lighter females, darker males, and lighter males in gender classification systems require urgent atten- tion if commercial companies are to build genuinely fair, transparent and accountable facial analysis algorithms.

# Não-neutralidade da IA no reconhecimento facial

> Even AI-based technologies that are not specifically trained to perform high-stakes tasks (such as determining how long someone spends in prison) can be used in a pipeline that performs such tasks. For example, **while face recognition software by itself should not be trained to determine the fate of an individual in the criminal justice system, it is very likely that such software is used to identify suspects.** Thus, an error in the output of a face recognition algorithm used as input for other tasks can have serious consequences. For example, **someone could be wrongfully accused of a crime based on erroneous but confident misidentification of the perpetrator from security video footage analysis.**
> (p. 1)

# Vies de gênero no word2vec

> Bolukbasi et al. {2016, "Man is to computer programmer as woman is to homemaker? debiasing word embeddings"} even showed that the popular word embedding space, Word2Vec, encodes societal gender biases. **The authors used Word2Vec to train an analogy generator that fills in missing words in analogies. The analogy man is to computer programmer as woman is to “X” was completed with “homemaker”, conforming to the stereotype that programming is associated with men and homemaking with women.**
> (p. 1)

# Preocupação: acurácia menor para certos grupos

> However, without a dataset that has labels for various skin characteristics such as color, thickness, and the amount of hair, one cannot measure the accuracy of such automated skin cancer de- tection systems for individuals with different skin types.
> (p. 2)

# Outro problema: acurácia menor pode significar maiores taxas de falso positivo. Exemplo do sistema prisonal.

> In other contexts, **a demographic group that is underrepresented in benchmark datasets can nonetheless be subjected to frequent targeting.** The use of automated face recognition by **law enforcement provides such an example.** At least 117 million Americans are included in law enforcement face recognition networks. **A year-long research investigation across 100 police departments revealed that African-American individuals are more likely to be stopped by law enforcement and be subjected to face recognition searches than individuals of other ethnicities** (Garvie et al., 2016). False positives and unwarranted searches pose a threat to civil liberties.
> (p. 2)

# 1) Criação de um dataset balanceado

> We take a step in this direction by making two contributions. First, **our work advances gender classification benchmarking by introducing a new face dataset composed of 1270 unique individu- als that is more phenotypically balanced on the basis of skin type than existing benchmarks.** To our knowledge this is the first gender classification benchmark labeled by the Fitzpatrick (TB, 1988) six-point skin type scale, allowing us to benchmark the performance of gender classification algorithms by skin type. 
> (p. 2)

# 2) Proposição de métrica de avaliação interseccional para reconhecimento de gênero

> Second, **this work introduces the first intersectional demographic and phenotypic evaluation of face-based gender classification accuracy.** Instead of evaluating accuracy by gender or skin type alone, accuracy is also examined on 4 intersectional subgroups: darker females, darker males, lighter females, and lighter males.
(p. 2)

# Apresentação de trabalhos relacionados

Análise facial automatizada (p. 2-3)

- face detection
- face recognition
- emotion id
- help autism
- sexuality detection
- individual characteristics
- crime prevention
- gender classification

Benchmarks (p. 3)

# Rótulos binários para gêneros

> An evaluation of gender classification performance currently requires reducing the construct of gender into defined classes. **In this work we use the sex labels of “male” and “female” to define gender classes since the evaluated benchmarks and classification systems use these binary labels.**
> (p. 3)

# Rótulos binários para cor de pele

> To assess the suitability of existing datasets for intersectional benchmarking, we provided skin type annotations for unique subjects within two selected datasets, and compared the distribution of darker females, darker males, lighter females, and lighter males.
> (p. 3)

# Por que raça não é suficiente para rótulos

> While race labels are suitable for assessing potential algorithmic discrimination in some forms of data (e.g. those used to predict criminal recidivism rates), they face two key limitations when used on visual images. First, **subjects’ phenotypic features can vary widely within a racial or ethnic category**. For example, the skin types of individuals identifying as Black in the US can represent many hues. Thus, **facial analysis benchmarks consisting of lighter-skinned Black individuals would not adequately represent darker-skinned ones**.
> (p. 4)

# Tipo de pele como marcação mais objetiva

> Since race and ethnic labels are unstable, **we decided to use skin type as a more visually precise label to measure dataset diversity.** **Skin type is one phenotypic attribute that can be used to more objectively characterize datasets along with eye and nose shapes.**
> (p. 4)

# Outras base de dados existentes: IJB-A e Adience

> IJB-A is a US government benchmark released by the National Institute of Standards and Tech{5}nology (NIST) in 2015.
> (p. 4-5)

> Adience is a gender classification benchmark released in 2014 and was selected due to its recency and unconstrained nature.
(p. 5)

# Problema: bases de dados não-balanceadas. PPB como alternativa.

> Preliminary analysis of the **IJB-A and Adience benchmarks revealed overrepresentation of lighter males, underrepresentation of darker females, and underrepresentation of darker individuals in general**. We developed the **Pilot Parliaments Benchmark (PPB) to achieve better intersectional representation on the basis of gender and skin type.**
> (p. 5)

# Por que parlamentares

> We decided to use images of parliamentarians since they are public figures with known identities and photos available under non-restrictive licenses posted on government websites.
> (p. 5)

# Por que esses países

![Fig. 2 - The global distribution of skin color. Most Africans have darker skin while those from Nordic countries are lighter-skinned. Image from (Encyclopedia Britannica). Copyright 2012 Encyclopedia Britannica.](./fig2.png)

> **The specific African and European countries were selected based on their ranking for gender parity as assessed by the Inter Parliamentary Union** (Inter Parliamentary Union Ranking). Of all the countries in the world, Rwanda has the highest proportion of women in parliament. Nordic countries were also well represented in the top 10 nations. **Given the gender parity and prevalence of lighter skin in the region, Iceland, Finland, and Sweden were chosen. To balance for darker skin, the next two highest-ranking African nations, Senegal and South Africa, were also added.**
> (p. 6)

# Pilot Parliaments Benchmark (PPB)

> PPB is highly constrained since **it is composed of official profile photos of parliamentarians**. These profile photos are taken under conditions with cooperative subjects where pose is relatively fixed, illumination is constant, and expressions are neutral or smiling. Conversely, the images in the IJB-A and Adience benchmarks are unconstrained and subject pose, illumination, and expression by construction have more variation.
> (p. 6)

# Rótulos para tipo de pele: Fitzpatrick

> __Skin Type Labels.__ We chose the Fitzpatrick six-point labeling system to determine skin type labels given its scientific origins. Dermatologists use this scale as the gold standard for skin classification and determining risk for skin cancer (TB, 1988).
> (p. 6)

# Rótulos para gênero

> __Gender Labels.__ All evaluated companies provided a “gender classification” feature **that uses the binary sex labels of female and male. This reductionist view of gender does not adequately capture the complexities of gender or address transgender identities. The companies provide no documentation to clarify if their gender classification systems which provide sex labels are classifying gender identity or biological sex.** To label the PPB data, we use female and male labels to indicate subjects perceived as women or men respectively.
> (p. 6)

# Processo de rotulamento (IJB-A e Adience)

> __Labeling Process.__ For existing benchmarks, one author labeled each image with one of six Fitzpatrick skin types and provided gender annotations for the IJB-A dataset. The Adience benchmark was already annotated for gender. These preliminary skin type annotations on existing datasets were used to determine if a new benchmark was needed.
> (p. 6)

# Processo de rotulamento (PPB)

> More annotation resources were used to label PPB. For the new parliamentarian benchmark, 3 annotators including the authors provided gender and Fitzpatrick labels. **A board-certified surgical dermatologist provided the definitive labels for the Fitzpatrick skin type.** **Gender labels were determined based on the name of the parliamentarian, gendered title, prefixes such as Mr or Ms, and the appearance of the photo.**
> (p. 6)

> For the purposes of our analysis, lighter subjects will refer to faces with a Fitzpatrick skin type of I,II, or III. Darker subjects will refer to faces labeled with a Fitzpatrick skin type of IV,V, or VI. **We intentionally choose countries with majority populations at opposite ends of the skin type scale to make the lighter/darker dichotomy more distinct.**
> (p. 7)

# Comparação entre bases de dados

![Fig. 3 - The percentage of darker female, lighter female, darker male, and lighter male subjects in PPB, IJB-A and Adience. Only 4.4% of subjects in Adience are darker-skinned and female in comparison to 21.3% in PPB.](./fig3.png)

> Darker females are the least represented in IJB-A (4.4%) and darker males are the least represented in Adience (6.4%). **Lighter males are the most represented unique subjects in all datasets.**  IJB-A is composed of 59.4% unique lighter males whereas this percentage is reduced to 41.6% in Adience and 30.3% in PPB.
> (p. 7)

# Resutados

![Tab. 4 - Gender classification performance as measured by the positive predictive value (PPV), error rate (1-TPR), true positive rate (TPR), and false positive rate (FPR) of the 3 evaluated commercial classifiers on the PPB dataset. All classifiers have the highest error rates for darker-skinned females (ranging from 20.8% for Microsoft to 34.7% for IBM).](./tbl4.png)

> We evaluated 3 commercial gender classifiers. **Overall, male subjects were more accurately classified than female subjects replicating previous findings** (Ngan et al., 2015), and **lighter subjects were more accurately classified than darker individuals.** An intersectional breakdown reveals that **all classifiers performed worst on darker fe- male subjects.**
> (p. 8)

-  All classifiers perform better on male faces than female faces (8.1% − 20.6% difference in error rate)
- All classifiers perform better on lighter faces than darker faces (11.8% − 19.2% difference in error rate)
- All classifiers perform worst on darker female faces (20.8% − 34.7% error rate)
- Microsoft and IBM classifiers perform best on lighter male faces (error rates of 0.0% and 0.3% respectively)
- Face++ classifiers perform best on darker male faces (0.7% error rate)
- The maximum difference in error rate be- tween the best and worst classified groups is 34.4%

# Por que estas companhias

> **Microsoft’s Cognitive Services Face API and IBM’s Watson Visual Recognition API were chosen since both companies have made large investments in artificial intelligence**, capture significant market shares in the machine learning services domain, and provide public demonstrations of their facial analysis technology. At the time of evaluation, **Google did not provide a publicly available gender classifier.** Previous studies have shown that face recognition systems developed in Western nations and those developed in Asian nations tend to perform better on their respective populations (Phillips et al., 2011). **Face++, a computer vision company headquartered in China with facial analysis technology previously integrated with some Lenovo computers, was thus chosen to see if this observation holds for gender classification.** Like Microsoft and IBM, Face++ also provided a publicly available demonstration of their gender classification capabilities at the time of evaluation(April and May 2017).
> (p. 8)

> All of the companies offered gender classification as a component of a set of proprietary facial analysis API services (Microsoft; IBM; Face++). **The description of classification methodology lacked detail and there was no mention of what training data was used.** At the time of evaluation, Microsoft’s Face Detect service was described as using advanced statistical algorithms that “may not always be 100% precise” (Microsoft API Reference). IBM Watson Visual Recognition and Face++ services were said to use deep learning-based algorithms (IBM API Reference; Face++ Terms of Service). **None of the commercial gender classifiers chosen for this analysis reported performance metrics on existing gender estimation benchmarks in their provided documentation.** The Face++ terms of use explicitly disclaim any warranties of accuracy. Only IBM provided confidence scores (between 0 and 1) for face-based gender classification labels. But it did not report how any metrics like true positive rates (TPR) or false positive rates (FPR) were balanced.
> (p. 8)

# Metodologia

> In following the gender classification evaluation precedent established by the National Institute for Standards and Technology (NIST), we assess {9} the **overall classification accuracy, male classification accuracy, and female classification accuracy as measured by the true positive rate (TPR)**. Extending beyond the NIST methodology **we also evaluate the positive predictive value, false positive rate, and error rate (1-TPR) of the following groups: all subjects, male subjects, female subjects, lighter subjects, darker subjects, darker females, darker males, lighter females, and lighter males.** See Table 2 in supplementary materials for results disaggregated by gender and each Fitz- patrick Skin Type.
> (p. 8-9)

# Taxas de erro para masculino e feminino

> The NIST Evaluation of Automated Gender Classification Algorithms report revealed that **gender classification performance on female faces was 1.8% to 12.5% lower than performance on male faces for the nine evaluated algorithms** (Ngan et al., 2015). The gender misclassification rates on the Pilot Parliaments Benchmark replicate this trend across all classifiers. The differences between female and male classification error rates range from 8.1% to 20.6%. **The relatively high positive predictive value for females indicate that when a face is predicted to be female the estimation is more likely to be correct than when a face is predicted to be male.** For the **Microsoft and IBM classifiers, the false positive rates (FPR) for males are triple or more than the FPR for females. The FPR for males is more than 30 times that of females with the Face++ classifier.**
> (p. 10)

# Taxas de erro para peles escuras e claras

> **All classifiers perform better on lighter subjects than darker subjects in PPB.** Microsoft achieves the best result with error rates of 12.9% on darker subjects and 0.7% on lighter individuals. On darker subjects, IBM achieves the worst classification accuracy with an error rate of 22.4%. This rate is nearly 7 times higher than the IBM error rate on lighter faces.
> (p. 10)

# Taxas de erro intersectional

> Across the board, **darker females account for the largest proportion of misclassified subjects.**  Even though darker females make up 21.3% of the PPB benchmark, they constitute between 61.0% to 72.4% of the classification error. Lighter males who make up 30.3% of the benchmark con- tribute only 0.0% to 2.4% of the total errors from these classifiers (See Table 1 in supplementary materials).
> (p. 10)

> We present a **deeper look at images from South Africa** to see if differences in algorithmic performance are mainly **due to image quality from each parliament**. In PPB, the European parliamentary images tend to be of higher resolution with less pose variation when compared to images from African parliaments. **The South African parliament, however, has comparable image resolution and has the largest skin type spread of all the parliaments.** Lighter subjects makeup 20.8% (n=91) of the images, and darker subjects make up the remaining 79.2% (n=346) of images. Table 5 shows that **all algorithms perform worse on female and darker subjects when compared to their counterpart male and lighter subjects.** The Microsoft gender classifier performs the best, with zero errors on classifying all males and lighter females.
> (p. 10)

> On the South African subset of the PPB benchmark, **all the error for Microsoft arises from misclassifying images of darker females.** Table 5 also shows that all classifiers perform worse on darker females. Face++ is flawless on lighter males. IBM performs best on lighter females with 0.0% error rate. **Examining classification performance on the South African subset of PPB reveals trends that closely match the algorithmic performance on the entire dataset.** Thus, we conclude that **variation in performance due to the image characteristics of each country does not fully account for the differences in misclassification rates between intersectional subgroups.** In other words, **the presence of more darker individuals is a better explanation for error rates than a deviation in how images of parliamentarians are composed and produced.** However, darker skin alone may not be fully responsible for misclassification. Instead, **darker skin may be highly correlated with facial geometries or gender display norms that were less represented in the training data of the evaluated classifiers.**
> (p. 10)

# Acurácia não é uma métrica suficiente

> **The overall gender classification accuracy results show the obfuscating nature of single performance metrics.** Taken at face value, gender classification accuracies ranging from 87.9% to 93.7% on the PPB dataset, suggest that these classifiers can be used for all populations represented by the benchmark. A company might justify the market readiness of a classifier by presenting performance results in aggregate. **Yet a gender and phenotypic breakdown of the results shows that performance differs substantially for distinct subgroups. Classification is 8.1% − 20.6% worse on female than male subjects and 11.8% − 19.2% worse on darker than lighter subjects.**
> (p. 11)

> When examining the gap in lighter and darker skin classification, we see that **even though darker females are most impacted, darker males are still more misclassified than lighter males for IBM and Microsoft.** The most improvement is needed on darker females specifically.
> (p. 11)

# Além do resultado, probabilidades e limites deveriam ser métricas adicionais providas por APIs

> While confidence values give users more information, commercial classifiers should provide additional metrics. All 3 evaluated APIs only provide gender classifications, **they do not output probabilities associated with the likelihood of being a particular gender.** This indicates that **companies are choosing a threshold which determines the classification:** if the prediction probability is greater than this threshold, the image is determined to be that of a male (or female) subject, and viceversa if the probability is less than this number. **This does not give users the ability to analyze true positive (TPR) and false positive (FPR) rates for various subgroups if different thresholds were to be chosen.** The commercial classifiers have picked thresholds that result in specific TPR and FPR rates for each subgroup.  And the FPR for some groups can be much higher than those for others. **By having APIs that fail to provide the ability to adjust these thresholds, they are limiting users’ ability to pick their own TPR/FPR trade-off.**
> (p. 11)

# Variabilidade dos sensores

> **With full awareness of the challenges that arise due to pose and illumination, we intentionally chose an optimistic sample of constrained images that were taken from the parliamentarian websites.** Each country had its peculiarities. Images from Rwanda and Senegal had more pose and illumination variation than images from other countries (Figure 1). The Swedish parliamentarians all had photos that were taken with a shadow on the face. The South African images had the most consistent pose and illumination.  The South African subset was also composed of a substantial number of lighter and darker subjects. **Given the diversity of the subset, the high image resolution, and the consistency of illumination and pose, our finding that classification accuracy varied by gender, skin type, and the intersection of gender with skin type do not appear to be confounded by the quality of sensor readings.** The disparities presented with such a constrained dataset do suggest that error rates would be higher on more challenging unconstrained datasets. **Future work should explore gender classification on an inclusive benchmark composed of unconstrained images.**
> (p. 12)

# Conclusão

> Because algorithmic fairness is based on different contextual assumptions and optimizations for accuracy, this work aimed to show why we need rigorous reporting on the performance metrics on which algorithmic fairness debates center. The work focuses on increasing phenotypic and demographic representation in face datasets and algorithmic evaluation. Inclusive benchmark datasets and subgroup accuracy reports will be necessary to increase transparency and accountability in artificial intelligence. For human-centered computer vision, we define transparency as providing information on the demographic and phenotypic composition of training and benchmark datasets. We define accountability as reporting algorithmic performance on demographic and phenotypic subgroups and actively working to close performance gaps where they arise. Algorithmic transparency and accountability reach beyond technical reports and should include mechanisms for consent and redress which we do not focus on here. Nonetheless, the findings from this work concerning benchmark representation and intersectional auditing provide empirical support for increased demographic and phenotypic transparency and accountability in artificial intelligence.
> (p. 12)
