---
path: "/mediacao-tecnica-Latour"
title: Sobre mediações técnicas
author: Rafael Gonçalves
date: 2020-10-08
category: fichamento
featuredImage: "../../images/2001-monolith-apes.jpg"
srcInfo: "Cena de 2001 - Uma odisséia no espaço"
published: true
tags: ["Bruno Latour", "tecnociência", "mediação técnica", "fichamento"]
---

Mais um pequeno fichamento, desta vez do texto *On technical mediation*[^1] de Bruno Latour.

A hipótese latouriana é a de que ação mediada tecnicamente não é executada nem por um sujeito que *utiliza* uma tecnologia, nem unicamente pela tecnologia, mas por um terceiro agente (actante), uma agenciamento de ambos. Nem o ser-humano, nem o avião voa, mas sim, o agenciamente humano-avião ou avião-humano.

> You are different wirh a gun in hand; the gun is different with
> you holding it. You are another subject because you hold the gun; the gun is another
> object because it has entered into a relationship wirh you. The gun is no longer the
> gun-in-the-armory or the gun-in-the-drawer or the gun-in-the-pocket, but the gun-in-your-hand, aimed at someone who is screaming. What is true of the subject, of the
> gunman, is as true of the object, of rhe gun that is held. A good cicizen becomes a
> criminal, a bad guy becomes a worse guy; a silent gun becomes a Iired gun, a new gun becomes a used gun, a sporting gun becomes a weapon. (p. 33)

> the prime mover of
> an action becomes a new, distributed, and nested series of practices whose sum might
> be made but only if we respect the mediating role of all the actants mobilized in the
> list. (p. 34)

> Technical skill is not uniquely possessed by humans and reluctantly granted to nonhumans.
> Skills emerge in the zone of transaction, they are properties of the assembly that circulate or are rediscributed among human and nonhuman technicians, enabling and authorizing them to act. (p. 45)

Um exemplo da agência técnica mediadora - *traduzindo* valores de significação - é ilustrada da seguinte maneira:

> A simple example of what I have in mind: a speed bump that forces drivers to slow
> down on campus. The driver's goal is translated, by means of the speed bump, from
> "slow down so as not to endanger students" into "slow down and protect my car's
> suspension." The two goals are far apart, and we recognize here the same displacement
> as in our gun story. The driver's first version appeals to morality, enlightened disinterest, and reflection, whereas the second appeals to pure selfishness and reflex action. In
> my experience, there are many more people who would respond to the second than to
> the first: selfrshness is a trait more widely distributed than respect for law and life -
> at least in France. The driver modifies his behavior through the mediation of the speed
> bump: he falls back from morality to force. But from an observer's point of view, it
> does not matter through which channel a given behavior is attained. From her window,
> the chancellor sees that cars are slowing down and, for her, that is enough. (p. 38)

"Deslocamento" político acontece por meio de mediações técnicas, mesmo que não se perceba, ou se dê atenção a isso.

> And through such detours, finally, the political order is subverted, since
> I rely on many delegated actions that themselves make me do things on behalf of
> others who are no longer here and that I have not elected and the course of whose
> existence I cannot even fetface. (p. 40)

Desta forma, ao meu ver, a tecnociência nascida no seio de um sistema capitalista, engloba em si seus valores. Então sim, existe agência própria a objetos tecnicos, mas ela nao é arbitraria. A tecnociência parte de um "coletivo" que, por sua vez, age sobre indivíduos.

Aqui o autor argumenta que ação intencionada não é uma propriedade de objetos ou humanos, mas de instituições ("corporative bodies").

> Purposeful action and intentionality may
> not be properties of objects, but they are not properties of humans either. They are
> the properties of institutions, dispositifs. Only corporate bodies are able ro absorb the
> proliferation of mediators, to regulate their expression, to redistribute skills, to require
> boxes to blacken and close. Boeing-747s do not fly, airlines fly. (p. 46)

Assim, Latour propõe uma sociologia que englobe ao mesmo tempo atores humanos e não-humanos.

> Objects that exist simply as objects, linished, not part of a collective life, are unknown, buried under soil. Real ob jects are always parts of institutions, trembling in their
> mixed status as mediators, mobilizing faraway lands and people, ready to become people
> or things, not knowing if they are composed of one or of many, of a black box counting
> for one or of a labyrinth concealing muititudes. And this is why the philosophy of tech nology cannot go very far: an object is a subject that only sociology can study - a sociology, in any case, that is prepared to deal with nonhuman as well as human actants. (p. 46)

Aqui, assim como em *Jamais fomos modernos*[^2], há uma refuta na noção de que a tecnociência atual seria mais avançada que a chamada "primitiva", colocando que a diferença é quantitativa: quantidade de híbridos, multiplicados cada vez mais com o desenvolvimento da humanidade.

> The difference between an ancient or "primitive" collective and a modern or "advanced" one is not that the former manifests a rich mixture of social and technical
> culture while the latter exhibits a technology devoid of ties with the social order. The
> difference, rather, is that the latter translates, crosses over, enrolls, and mobilizes more elemets,
> more intimately connected, with a more linely woven social fabric than the
> former does. The relation between the scale of collectives and the number of nonhumans
> enlisted in their midst is crucial. One finds, of course, longer chains of action in
> "modern" collectives, a greater number of nonhumans (machines, automatons, devices) associated with one another, but one must not overlook the size of markets, the number of
> people in their orbits, the amplitude of the mobilization: more objects, yes, but many
> more subjecrs as well. (p. 47)

Assim, agentes não-humanos podem reproduzir agência mobilizada por atores que não estariam mais presentes. Não-humanos cristalizariam relações criadas por atores em outro momento.

> An action in the distant past, in
> a faraway place, by actors now absent, can still be present, on condition that it be
> shifted, translated, delegated, or displaced to other types of actants, those I have been
> calling nonhumans. My word processor, your copy of Common Knowledge,
> Oxford University Press, the International Postal Union, all of them organize, shape, and limit
> our interactions. (p. 50)

> Everything in the definition of macro social
> order is due to che enrollment of nonhumans - that is, to technicai mediation. Even
> the simple effect of duration, of long-lasting social force, cannot be obtained without
> the durability of nonhumans to which human local interacrions have been shifted. (p. 51)

Mas também podem pertubar, manchar, desviar e produzir relações novas. Desta forma a sociedade é construída tanto por humanos e suas relações quanto por não-humanos.

> But techniques are not fetishes, they are unpredicrable, not means but mediators,
> means and ends at the same time; and that is why they bear on the social fabric. Critical
> theory is unable to explain why artifacts enter the stream of our relations, why we so
> constantly recruit and socialize nonhumans. It is not to mirror, inscribe, or hide social
> relations, but to remake them through fresh and unexpected sources ofpower. Society
> is not stable enough to inscribe itself in anything. On the contrary, most of the features
> of what we mean by social order - scale, asymmetry, durability, power, hierarchy, the
> distribution of roles - are impossible even to define without recruiting socialized nonhumans. Yes, society is constructed, but not socially constructed. Only che Machiirvellian baboon, the Kubrick ape, constructs its sociery socially. Humans, for millions of
> years, have extended their social relations to other actants with which, with whom,
>  they form collectives. (p. 53)

Por fim, o autor reafirma o caráter simétrico objeto-sujeito, e sua tese de que os coletivos cresceriam com o tempo multiplicando tanto agentes humanos como não-humanos.

> Objectivity and subjectivity are not opposed, they grow together, and
> they grow irreversibly togerher. The challenge to our philosophy, social theory, and
> morality is to invent political institutions that can absorb this much history, this huge
> spiralling movement, this destiny, this fate... (p. 64)


[^1]: LATOUR, Bruno. On technical mediation. 1994.
[^2]: LATOUR, Bruno. <strong>Jamais fomos modernos</strong>. Editora 34, 1994.
