---
title: Cosmotécnica como cosmopolítica
date: 2022-01-26
path: "/cosmotecnica-cosmopolitica"
category: fichamento
author: Rafael Gonçalves
published: true
tags:
 - Yuk Hui
 - Kant
 - Heidegger
 - cosmopolítica
 - cosmotécnica
 - tecnologia
featuredImage: "./yukn.jpg"
srcInfo: Foto retirada de <a href='http://www.digitalmilieu.net/yuk/'>digitalmilieu</a>

---

Fichamento do texto _Cosmotécnica como cosmopolítica_[^1] de Yuk Hui.

[^1]: HUI, Y. Cosmotécnica como cosmopolítica. In: **Tecnodiversidade**. Ubu Editora, 2020. 

# Globalização unilateral

> Até agora, a assim chamada “globalização” tem sido em sua maior parte um processo que emana de um só lado e traz consigo a universalização de epistemologias particulares e, através de meios tecnoeconômicos, a elevação de uma visão de mundo regional ao status de metafísica supostamente global.

# 11/set como evidência de seu fim (por que?)

> Sabemos que essa globalização unilateral chegou ao fim graças à leitura equivocada que se atribuiu aos ataques do 11 de Setembro, vistos como um ataque do Outro contra o Ocidente. Na verdade, o 11 de Setembro foi um evento “autoimune”, interno ao bloco Atlântico, no qual suas próprias células anticomunistas, adormecidas desde o fim da Guerra Fria, se voltaram contra seu hospedeiro. Ainda assim, as imagens espetaculares do evento forneceram uma espécie de teste de Rorschach em que os representantes da globalização puderam projetar suas inseguranças crescentes quanto a serem deixados ilhados entre a antiga configuração e a nova – um exemplo do que Hegel chamou de “consciência infeliz”.

# A consciência infeliz e a disputa pela posição hegemônica global

> A consciência infeliz de Peter Thiel evoca uma época de glória comercial a que se renunciou com o fim da globalização unilateral e aspira a um futurismo transumanista baseado na aceleração tecnológica de todas as escalas cósmicas. Isso leva a uma redefinição do Estado-nação soberano como resultado da competição tecnológica global. Precisamos começar a imaginar uma nova política que não seja apenas mais uma continuação desse mesmo tipo de geopolítica com uma ligeira mudança nas configurações do poder – ou seja, uma geopolítica cuja diferença estaria no fato de que o papel de superpotência seria desempenhado pela China ou pela Rússia em vez dos Estados Unidos. Precisamos de uma nova linguagem de cosmopolítica para que possamos formular uma nova ordem mundial que vá além de uma única hegemonia.

# Cosmotécnica (na forma de uma antítese kantiana)

> Tese: a tecnologia, como formulada por alguns antropólogos e filósofos, é um universo antropológico entendido como a exteriorização da memória e a superação da dependência dos órgãos. Antítese: a tecnologia não é antropologicamente universal; seu funcionamento é assegurado e limitado por cosmologias particulares que vão além da mera funcionalidade e da utilidade. Assim, não há uma tecnologia única, mas uma multiplicidade de cosmotécnicas.

# Cosmopolítica e natureza em Kant

> Se Kant vê a constituição republicana e a paz perpétua como formas políticas que talvez possam trazer à tona uma história universal da espécie humana, isso se deve à sua concepção de que um progresso desse tipo também é um progresso da razão, o _telos_ da natureza. Esse progresso em direção a um objetivo final (mais especificamente, a história universal e a constituição política perfeita) é a “realização de um plano oculto da natureza” (_Vollziehung eines verborgenen Plans der Natur_).

# Cosmopolítica = telos ~ (des)encantamento da natureza

> Nossa intenção aqui é mostrar que Kant desenvolve seu pensamento em direção ao universalismo e que seu conceito de relação entre a cosmopolítica e o propósito da natureza está situado em um momento peculiar da história: o encantamento e o desencantamento simultâneos da natureza.

# Morte do cosmos (Bargue)

> > A nova astronomia, dando continuidade a Copérnico e seus sucessores, teve consequências para o ponto de vista moderno do mundo […]. Pensadores medievais e da Antiguidade apresentaram um esquema sincrônico da estrutura do mundo físico que apagava os rastros de sua origem; os modernos, por outro lado, lembraram do passado e, além disso, ofereceram uma visão diacrônica da astronomia – como se a evolução das ideias sobre o cosmos fosse ainda mais importante do que a verdade sobre ele […]. Seria ainda possível falar de cosmologia? Parece que o Ocidente perdeu uma cosmologia com o fim do mundo de Aristóteles e de Ptolomeu, um fim atribuível a Copérnico, Galileu e Newton. O “mundo”, a partir de então, já não formava um todo.


# Tecnologia como momento em que a natureza deixa de ser antropomórfica

> Diane Morgan, estudiosa de Kant, sugere que a natureza perdeu o caráter antropomórfico quando foi confrontada com os “mundos para além de mundos” revelados pela tecnologia, já que a relação entre humanos e natureza foi, então, virada do avesso, com os humanos agora colocados diante do universo “imensamente grande”.

# Primazia das tecnologias de comunicação no projeto político moderno

> Para além da revelação da natureza e de sua teleologia através de instrumentos técnicos, a tecnologia também assume um papel decisivo quando Kant afirma em sua filosofia política que a comunicação é a condição de realização do todo organicista.

> Mas tal _sensus communis_ somente é alcançado por meio de tecnologias especiais, e é com base nisso que devemos problematizar qualquer discurso ingênuo que enxergue o comum como algo já dado ou que preceda a tecnologia. A era do Iluminismo, como destacado por Arendt (e também por Bernard Stiegler), é a do “uso público da razão individual”, e esse exercício da razão se manifesta na liberdade de expressão e de imprensa, que necessariamente envolvem as tecnologias de impressão. Em um nível internacional, Kant escreve em _À paz perpétua_ que “em um comércio de diferentes povos, pelo que os povos foram levados pela primeira vez a uma relação pacífica uns com os outros e, assim, à compreensão da comunidade e da relação pacífica uns com os outros, mesmo com os mais distantes” e, mais adiante, acrescenta que “é o espírito comercial, que não pode subsistir juntamente com a guerra e que mais cedo ou mais tarde se apodera de cada povo”.

# Encantamento: Única natureza -> única racionalidade (moralidade e Estado)

> De algum modo, Kant admite _uma única natureza_ que a razão nos impele a reconhecer como racional; a racionalidade corresponde à universalidade teleológica organicista ostensivamente concretizada na constituição tanto da moralidade quanto do Estado.

# Desencantamento da natureza na Rev. Industrial como exemplar da ineficácia da metáfora biológica do cosmopolitismo

> Esse encantamento da natureza é acompanhado por um desencantamento da natureza guiado pela mecanização imposta pela Revolução Industrial. A “morte do cosmos” de Brague, empreendida pela modernidade europeia e por sua globalização da tecnologia moderna, necessariamente forma uma das condições para que hoje reflitamos sobre a cosmopolítica, uma vez que ilustra a ineficácia de uma metáfora biológica para o cosmopolitismo.

# A virada ontológica na antropologia

> A “virada ontológica” na antropologia é um movimento associado a antropólogos como Philippe Descola, Eduardo Viveiros de Castro, Bruno Latour e Tim Ingold, e, antes deles, a Roy Wagner e Marilyn Strathern, entre outros.21 Essa virada ontológica é uma resposta direta à crise da modernidade que, de modo geral, se expressa em termos de uma crise ecológica que, agora, está intimamente ligada ao Antropoceno. O movimento da virada ontológica é uma tentativa de levar diferentes ontologias em diferentes culturas a sério (devemos ter em mente que saber onde diferentes ontologias estão não é o mesmo que levá-las a sério).

# Tétrade ontológica em Descola

> Descola convence ao esboçar quatro ontologias principais: o naturalismo, o animismo, o totemismo e o analogismo.22 O moderno é caracterizado pelo que o autor chama de “naturalismo”, o que significa uma oposição entre cultura e natureza e entre a dominância da primeira em relação à segunda. 

> Em _Par-delà Nature et culture_ [Para além da natureza e da cultura], Descola propõe um pluralismo ontológico que é irredutível ao socioconstrutivismo. Ele sugere que o reconhecimento dessas diferenças ontológicas pode servir como antídoto à dominância que o naturalismo vem exercendo desde o advento da modernidade europeia.

# Compatibilização entre naturalismo e tecnologia na modernidade

> Se o naturalismo foi bem-sucedido na dominação do pensamento moderno, é porque uma imaginação cosmológica do gênero é compatível com seu desenvolvimento tecno-lógico: a natureza deve ser dominada para o bem do homem e, de acordo com as próprias regras da natureza, pode efetivamente sê-lo. Ou, colocado de outra maneira: a natureza é considerada a fonte de contingências devido à sua “fragilidade conceitual” e, por isso, precisa ser subjugada pela lógica.

# Necessidade de ressituar as "naturezas nativas"

> A restauração de “naturezas nativas” precisa primeiro ser questionada – não porque elas não existam, mas porque estão situadas em uma nova época e são transformadas de tal modo que dificilmente haverá como voltar atrás e restaurá-las.

# Cosmologia: Multiontologias -> multinaturezas

> Retomemos o que foi dito antes sobre a virada ontológica. A cosmologia é essencial para o conceito de “natureza” e de “ontologia” dos antropólogos, já que essa “natureza” é definida de acordo com diferentes “ecologias de relações”, nas quais observamos diferentes constelações de relações, como o parentesco entre mulheres e vegetais ou a fraternidade entre caçadores e animais. Essas multiontologias se expressam como multinaturezas; as quatro ontologias de Descola, por exemplo, correspondem a diferentes visões cosmológicas.

# Cosmotécnica: além da cosmologia

> Proponho ir além da noção de cosmologia; em vez disso, seria mais produtivo abordarmos o que chamo de cosmotécnica. Aqui vai uma definição preliminar: cosmotécnica é a unificação do cosmos e da moral por meio das atividades técnicas, sejam elas da criação de produtos ou de obras de arte. Não há apenas uma ou duas técnicas, mas muitas cosmotécnicas. Que tipo de moralidade, qual cosmos e a quem ele pertence e como unificar isso tudo variam de uma cultura para a outra de acordo com dinâmicas diferentes.

# Cosmotécnica como bifurcação de futuros tecnológicos possíveis (contra o Antropoceno)

> Estou convencido de que, a fim de confrontar a crise diante da qual nos encontramos – mais precisamente, o Antropoceno, a intrusão de Gaia (Latour e Stengers) ou o “Entropoceno” (Stiegler), todas essas noções apresentadas como o futuro inevitável da humanidade –, precisamos rearticular a questão da tecnologia, de modo a vislumbrar a existência de uma bifurcação de futuros tecnológicos sob a concepção de cosmotécnicas diferentes.

# Techné grega e técnica moderna em Heidegger

> Ao acompanharmos o ensaio de Heidegger, podemos distinguir duas noções de tal conceito. Primeiro, temos a noção grega de technē, que Heidegger desenvolve por meio de sua leitura dos gregos antigos, notadamente os pré-socráticos – mais precisamente, os três pensadores “iniciais” (anfängliche), Parmênides, Heráclito e Anaximandro.26 Em seu curso de 1949, Heidegger propõe uma distinção entre a essência da technē grega e a tecnologia moderna (moderne Technik).

# Técnica moderna europeia como única sucessora da tecné

> Se a essência da technē é a poiesis, ou produção (Hervorbringen), então a tecnologia moderna, um produto da modernidade europeia, deixa de possuir a mesma essência da technē e se torna um aparato de “composição” (Gestell), no sentido de que todos os seres se tornam disponíveis (Bestand) para isso. Heidegger não inclui essas duas essências como técnicas, mas também não dá espaço para outras técnicas – como se só houvesse uma única e homogênea Machenschaft [maquinação] depois da technē grega, uma técnica calculável, internacional e até planetária.

# Antecipação Heideggeriana sobre globalização tecnológica como neocolonização (?)

> “Se o comunismo chegasse ao poder na China, seria possível admitir que esse seria o único modo pelo qual os chineses poderiam se ver ‘desimpedidos’ para a tecnologia. Que processo é esse?”.27 Heidegger insinua duas coisas aqui: primeiro, que a tecnologia é internacional (não universal); segundo, que os chineses não tiveram capacidade nenhuma de resistir à tecnologia depois que o comunismo tomou o poder no país. Esse veredito antecipa a globalização tecnológica como uma forma de neocolonização que impõe sua racionalidade via instrumentalidade, como o que observamos nas políticas transumanistas e neorreacionárias.

# Motivações do desenvolvimento do conceito de cosmotécnica

> Minha tentativa de ir além do discurso de Heidegger quanto à tecnologia tem como base, sobretudo, duas motivações: 1) o desejo de responder à virada ontológica na antropologia que pretende lidar com o problema da modernidade com uma proposta de pluralismo ontológico; e 2) o desejo de atualizar o discurso insuficiente que é largamente associado à crítica de Heidegger à tecnologia. Propus que recolocássemos a questão da técnica como uma variedade de cosmotécnica, e não como technē ou tecnologia moderna.

# China como exemplo, sistematização possível em todas culturas não européias

> Em minha pesquisa, usei a China como laboratório para minha tese e tentei reconstruir uma genealogia do pensamento tecnológico chinês. Essa tarefa, no entanto, não se limita à China, já que a ideia central é a de que todas as culturas não europeias deveriam sistematizar as próprias cosmotécnicas e as histórias dessas cosmotécnicas.

# Cosmotécnica chinesa

> O pensamento cosmotécnico chinês consiste em uma longa história de discursos intelectuais sobre a unidade e a relação entre _chi_ e _tao_. A união do _chi_ e do _tao_ também é a união da moral e do cósmico, já que a metafísica chinesa é, em essência, uma cosmologia moral ou uma metafísica moral, como foi demonstrado pelo filósofo do novo confucionismo Mou Tsung-San.

# Tao: acima das formas

> _Tao_ não é um objeto. Não é um conceito. Não é uma _différance_. No _Tseu-Hi_ de _Yi Zhuan_ (易傳‧繫辭), _Tao_ é tido apenas como “acima das formas”, enquanto _chi_ é o que está “abaixo das formas”. É digno de nota que _xin er shang xue_ (o estudo do que está acima das formas) seja a expressão usada para traduzir “metafísica” (uma das equivalências que precisa ser desfeita).

# Chi: algo que ocupa o espaço

> _Chi_ é algo que ocupa espaço, como podemos notar pela leitura de um dicionário etimológico e também por sua representação gráfica (氣) – quatro bocas ou recipientes e, no meio deles, um cão que guarda utensílios de cozinha. O _chi_ apresenta sentidos variados em doutrinas diferentes; no confucionismo clássico, por exemplo, há _Li chi_ (禮器), no qual o chi é essencial para o Li (um rito) – que, por sua vez, não é apenas uma cerimônia, mas sobretudo a busca por união entre o ser humano e os céus.

# Tao ~ númeno; Chi ~ fenômeno

> Para os nossos propósitos, basta dizer que _tao_ pertence ao númeno de acordo com a distinção kantiana, enquanto _chi_ se relaciona ao fenômeno.

# Arte como infinitizar o chi (e o eu) e adentrar o númeno

> Mas é possível infinitizar o _chi_ de modo a infinitizar o eu e adentrar o númeno – essa é a questão da arte.

# _Tao_ como vida, não como técnica. Ou técnica como processo, não como objeto?

> O duque Wen Huei, que havia feito a pergunta, responde que “agora, depois de ter ouvido Pao Ding falar, aprendi a viver”; e, de fato, essa história está incluída na seção intitulada “Maestria no viver”. É, portanto, a questão do “viver”, mais do que a da técnica, que está no centro da narrativa. Se há um conceito de “técnica” aqui, ele está separado do objeto técnico: ainda que o objeto técnico não seja desprovido de importância, não se pode buscar a perfeição da técnica pelo aperfeiçoamento de uma ferramenta ou de uma habilidade, já que a perfeição só pode ser alcançada pelo _tao_. A faca de Pao Ding nunca corta tendões ou ossos; em vez disso, ela busca pelos vãos e os percorre com facilidade. E, ao fazê-lo, desempenha a função de destrinchar uma vaca sem se colocar em risco – isto é, sem que a faca perca o fio e seja substituída. Ela, assim, se realiza inteiramente como faca.

# Não recusar a tecnologia moderna, mas analisar a possibilidade de futuros tecnológicos diferentes

> Pelo contrário, todas as culturas devem refletir sobre a questão da cosmotécnica a fim de que surja uma nova cosmopolítica, uma vez que, para superarmos a modernidade sem recair em guerras e no fascismo, parece-me necessário nos reapropriar da tecnologia moderna através da estrutura renovada de uma cosmotécnica que consista em diferentes epistemologias e epistemes. Por isso, este não é um projeto de substancialização da tradição, como no caso de tradicionalistas como René Guénon ou Aleksandr Dugin; o objetivo aqui não é recusar a tecnologia moderna, mas analisar a possibilidade de futuros tecnológicos diferentes.

# Recolocar a questão da tecnologia é repensar o futuro tecnológico homogêneo (que o Antropoceno evidencia como problemático)

> O Antropoceno é a planetarização das composições (Gestell), e a crítica de Heidegger à tecnologia é hoje mais significativa do que nunca. A globalização unilateral que chegou ao fim está dando lugar a uma competição de acelerações tecnológicas e às tentações da guerra, da singularidade tecnológica e dos sonhos (ou delírios) transumanistas. O Antropoceno é um eixo de tempo global e de sincronização que tem como base essa visão do progresso tecnológico rumo à singularidade. Recolocar a questão da tecnologia é recusar esse futuro tecnológico homogêneo que nos é apresentado como a única opção.
