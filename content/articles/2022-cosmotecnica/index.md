---
title: Sobre a cosmotécnica
date: 2022-01-31
path: "/cosmotecnica"
category: fichamento
author: Rafael Gonçalves
published: true
featuredImage: "./image.jpg"
srcInfo: Foto de <a href='https://www.flickr.com/photos/hypotekyfidler/13441809283/'>Jan Fidler</a>
tags:
 - Yuk Hui
 - antropoceno
 - cosmotécnica
 - tecnologia
 - Gilbert Simondon
 - Philippe Descola
 - Donna Haraway
---

Fichamento de uma tradução do texto _Sobre a Cosmotécnica_[^1] de Yuk Hui.

[^1]: HUI, Y. Sobre a Cosmotécnica: Por uma Relação Renovada entre Tecnologia e Natureza no Antropoceno [On Cosmotechnics: For a Renewed Relaton between Technology and Nature in the Anthropocene]. Tradução: Thiago Novaes. 2021 [2017]. Disponível em: <https://www.academia.edu/49181375/Sobre_a_Cosmot%C3%A9cnica_Por_uma_Rela%C3%A7%C3%A3o_Renovada_entre_Tecnologia_e_Natureza_no_Antropoceno_Yuk_Hui>. Acesso: 31/01/2022

# Antropoceno como crise decorrente da relação entre humanidade e natureza pós-industriaõzação

> Parece inegável que o Antropoceno, além de sua óbvia significação como nova era
> geológica, represente também uma crise que é a culminação de dois séculos de
> industrialização. A relação entre humanidade e “natureza” passou por uma grande
> transformação, e a constante chegada de crises ecológicas e desastres tecnológicos
> documentou bem esse momento histórico, e urge uma nova direção da humanidade para
> evitar seu próprio fim.
> (p. 1)

# Objetivos: clarificar a relação entre natureza e tecnologia. Propor o conceito de cosmotécnica como resolução dessa tensão.

> Este artigo tenta contribuir para uma clarificação da relação entre
> tecnologia e natureza, e para a inevitável tarefa da filosofia da técnica em refletir sobre o
> futuro desenvolvimento tecnológico planetário. Ele também visa resolver a tensão acima
> mencionada através do conceito de cosmotécnica, com a esperança de ir além do limite da
> noção de tecnologia6 e entendê-la a partir de uma perspectiva verdadeiramente
> cosmopolítica.
> (p. 2)

# Sobre técnica e tecnologia em Heidegger

> 6: Ver M. Heidegger, 1977. Como o conceito de “técnica” - um termo que uso aqui para abranger todas as formas de atividade técnica - é muito limitado, podemos provavelmente dizer, seguindo Martin Heidegger, que existem dois conceitos de técnica: em primeiro lugar, a noção grega de technē, que significa poiesis ou "produzir" (Hervorbringen); e, em segundo lugar, a tecnologia moderna, cuja essência, segundo Heidegger, não é mais technē, mas Gestell, na qual o ser é entendido como “reserva permanente” ou “estoque” (Bestand). O limite é que não há lugar para qualquer conceito não-europeu de técnica, uma vez que essa teorização seja aceita globalmente, como é atualmente o caso.
> (p. 2)

# Primeira natureza (pura e inocente) e segunda natureza

> Nas duas atitudes extremas mencionadas acima - uma enfocando a sacralidade e pureza da natureza, a outra em seu domínio - há uma falta de compreensão da profunda questão das diferentes formas de participação de seres humanos e não-humanos. A participação do não-humano é eclipsada pela questão do domínio tecnológico e, portanto, tornada insignificante; ou a cultura é vista como uma mera possibilidade da natureza no sentido de que a natureza é a mãe que dá origem a todos e à qual todos retornarão. Eu gostaria de falar, ao contrário, de uma segunda natureza 7 , para evitar a ilusão de uma primeira natureza pura e inocente, bem como para evitar nos aprisionarmos numa pura racionalidade tecnológica
> (p. 2)

> 7: Tomo o conceito de segunda natureza de Bernard Stiegler em seu diálogo com Elie During em Philosopher par accident (B. Stiegler e During, 2004), bem como de Gilbert Simondon, para quem (como para Blaise Pascal) segunda natureza significa mais sobre hábito (G. Simondon 2020b, p. 148). Bruno Latour, em seu Polítics of Nature, propõe abandonar o conceito de natureza, como ele escreve: “Quando o mais frenético dos ecologistas grita, tremendo: “A natureza vai morrer”, eles não sabem como estão certos. Graças a Deus a natureza vai morrer. Sim, o grande Pan está morto. Depois da morte de Deus e da morte do homem, a natureza também teve que desistir do fantasma. Chegou a hora: estávamos prestes a ser incapazes de nos envolver mais na política” (B. Latour, 2004, p. 24-25). Em oposição a Latour, acredito que só se pode abandonar a noção de primeira natureza como totalmente inocente e pura, mas não se pode abandonar o conceito de natureza, pois é isso que nos reconecta à questão do cosmos.
> (p. 2)

# Levar a sério ontologias (humanas e não-humanas)

> O fracasso do construtivismo social do século XX, como sugerido pelo sociólogo Andrew Pickering, deve nos ensinar a levar essas ontologias a sério, já que elas não são meramente “construídas”, mas “reais” - humanos variam de uma cultura a outra de acordo com diferentes cosmologias. Essas cosmologias não são apenas esquemas que definem os modos de participação, mas também correspondem aos fundamentos morais de tal participação.
> (p. 3)

# Cosmologia (segunda natureza) implica a moral

> Uma forma particular de participação é apenas justificada na medida em que atende ou ilumina a moral - o que não significa necessariamente harmonia, mas sim os códigos e crenças que constituem a dinâmica da vida individual e comunitária. Podemos falar sobre a moral somente na medida em que os seres humanos estão sendo no mundo; e o mundo é apenas um mundo e não um mero ambiente quando está de acordo com tais crenças. É na questão da segunda natureza que podemos localizar a questão da moral, já que a moral só é revelada através de uma certa interpretação da natureza; ou, em outras palavras, a natureza é conhecida de acordo com ordens e exceções.
> (p. 3)

> Na Grécia antiga, é
> conhecida como cosmologia: _kosmos_ significa ordem; cosmologia, o estudo da ordem. A
> natureza não é mais independente dos humanos, mas sim do outro. A cosmologia não é
> um conhecimento teórico puro; de fato, as cosmologias antigas são necessariamente
> cosmotécnicas.
> (p. 3)

# Cosmotécnica como unificação da ordem cósmica e moral na atividade técnica

> Deixe-me dar uma definição preliminar de cosmotécnica aqui: significa a
> unificação da ordem cósmica e ordem moral através de atividades técnicas. As atividades
> humanas, sempre acompanhadas de objetos técnicos, como ferramentas, são sempre
> cosmotécnicas.
> (p. 3)

# A tecnologia moderna derruba a relação entre cosmos e técnica (Heidegger)

> A tecnologia moderna derrubou a tradicional relação entre o cosmos e a técnica; torna-se uma força gigantesca, que transforma todos os seres em mera "reserva permanente" ou "estoque" (Bestand), como observa Martin Heidegger em sua famosa palestra 1949/1954 “The Question Concerning Technology” .
> (p. 3)

> Procuraremos mostrar, tanto no trabalho de Simondon quanto no dos antropólogos, que a relação entre natureza e tecnologia tem uma raiz moral que foi desintegrada pela industrialização planetária; e a partir daí, tentaremos abordar a possibilidade de uma relação renovada entre tecnologia e natureza, e assim lançar luz sobre o conceito de cosmotécnica.
> (p. 3-4)

# Natureza x Cultura em Descola. Cultura x Técnica em Simondon.

> O dualismo cultura-natureza de Descola parece ter assumido que a técnica está do lado da cultura, enquanto está claro que, para Gilbert Simondon, a técnica ainda não está - pelo menos no decorrer de sua escrita - totalmente integrada à cultura. Para Simondon, há um mal-entendido e uma ignorância da técnica na cultura10, que é uma das fontes de uma dupla alienação: a alienação dos seres humanos no sentido de Marx e a alienação dos objetos técnicos – tratados, por exemplo, como escravos ou produtos de consumo, como os escravos da época romana esperando indefinidamente no mercado pelos compradores11 . De fato, podemos ver um paralelo bastante intrigante entre Descola e Simondon, esquematizado no seguinte diagrama:
> 
>  ```
>  Natureza Vs. Cultura Vs. Técnica
>      \         /  \        /
>       \       /    \      /
>         Descola    Simondon
> ```
> (p. 4)

# Aparente distanciamento entre ambos e possibilidade de unificação

> É minha convicção que a antropologia da natureza e a filosofia da técnica deveriam conversar entre si para abordar a questão do Antropoceno. Alguns leitores podem duvidar que essas duas escolas de pensamento possam ser reunidas: a escola antropológica está amplamente enraizada na obra de Lévi-Strauss, que adota uma perspectiva transcendental quase kantiana sobre os mitos e a cosmologia, enquanto o pensamento de Simondon poderia ser caracterizado como um empirismo transcendental no sentido de Deleuze, que se situa na imanência das relações, energias e informações. No entanto, essa oposição é apenas aparente, pois, por um lado, na Parte III de Do modo de existência dos objetos técnicos, de Simondon, ele ofereceu uma maneira de situar o progresso técnico além da realidade técnica (por exemplo, na dinâmica interna dos objetos técnicos) em direção a uma realidade cósmica; por outro lado, a questão da relação (embora um pouco esquematizada) desempenha um papel central na concepção do modo de operação dentro de diferentes ontologias em Par delà nature et culture de Descola (para não falar da Metafísica Canibal de Viveiros de Castro, um tratado antropológico pós-estrutural inspirado no conceito deleuziano de intensidade). Desta vez, queremos mudar o contexto; isto é, facilitar o diálogo entre a antropologia pósestruturalista e a filosofia da tecnologia.
> (p. 4-5)

# Divisão natureza/cultura (Naturalismo em Descola)

> A oposição cultura _versus_ natureza compreende uma das quatro ontologias que Descola
> chama de “naturalismo”, ao lado das quais ele define três outras: “animismo”, “totemismo”
> e “analogismo”. No naturalismo, encontramos a divisão natureza/ cultura, que se
> manifesta como uma divisão entre o humano e o não-humano. Tal divisão é caracterizada
> pela continuidade física e descontinuidade espiritual entre humanos e não-humanos, em
> que a participação dos não-humanos se limita a ser objeto do domínio e dominação do
> humano. 
> (p. 5)

> É importante reconhecer que o naturalismo não existe desde o começo da cultura europeia; na verdade, é um produto “recente” ou, para Bruno Latour, um produto “incompleto”, no sentido de que “nunca fomos modernos
> (p. 5)

> Descola mostrou que o analogismo, e não o naturalismo, esteve significativamente presente na Europa durante o Renascimento, e se esse for o caso, a “virada” que ocorreu durante a modernidade europeia parece ter fornecido uma epistemologia completamente diferente sobre a relação entre humano e não-humano e natureza, sujeito e objeto, entre cosmos e física; uma epistemologia que podemos analisar retrospectivamente no trabalho de Galileu, Kepler, Newton e outros.
> (p. 5)

> Se o naturalismo conseguiu dominar o pensamento moderno é porque tal imaginação cosmológica peculiar é compatível com seu desenvolvimento tecnológico: a natureza deve ser dominada e pode ser dominada de acordo com as leis da natureza.
> (p. 5)

# Esquema das 4 ontologias em Descola

| | | | |
|-----------------------------------------------------|-----------------|----------------|-----------------------------------------------------|
| Interioridade similar<br>Fisicalidade dissimilar    | **Animismo**    | **Totemismo**  | Interioridade similar<br>Fisicalidade similar       |
| Interioridade dissimilar<br>Fisicalidade similar    | **Naturalismo** | **Analogismo** | Interioridade dissimilar<br>Fisicalidade dissimilar |

# Antropoceno como crise da modernidade e do naturalismo. Virada ontológica chama uma política de ontologias.

> O Antropoceno é ao mesmo tempo a crise do naturalismo e a crise da modernidade. É sob tal crise que a modernidade é novamente questionada15 , desta vez por antropólogos. A virada ontológica na antropologia é uma chamada para uma política de ontologias.
> (p. 6)

# Reconhecimento da pluralidade

> Essa política nos leva principalmente a um pluralismo que foi ameaçado pela disseminação do naturalismo em todo o mundo pela colonização. No centro de tal política está o reconhecimento de uma pluralidade de ontologias em que as naturezas desempenham papéis diferentes na vida cotidiana. 
> (p. 6)

# Questão: como escapar do naturalismo realizado nas tecnologias modernas?

> No entanto, a questão que resta a ser respondida é: qual será o destino dessas ontologias e práticas indígenas ao se confrontarem com a tecnologia moderna, que é a realização do naturalismo? Ou essas “práticas” são capazes de transformar a tecnologia moderna de modo que esta última adquira uma nova direção de desenvolvimento, um novo modo de existência? Essa é uma das questões mais cruciais, pois é também sobre como escapar do colonialismo e do etnocentrismo
> (p. 6)

# Descola não desenvolve o impacto da tecnologia sobre as ontologias. A tecnologia moderna muda tanto a ontologia européia quanto a não-européia. Não há a possibilidade de retorno à ontologias anteriores.

> Teremos que reconhecer que a tensão entre ontologia e técnica não está claramente definida no pensamento de Descola. Ao falar de uma tensão entre ontologia e técnica, quero dizer que essas ontologias só são possíveis quando já são cúmplices da vida técnica - abrangendo a invenção, a produção e o uso cotidiano. Como resultado, qualquer transformação desta última alterará diretamente a primeira. A chegada da tecnologia moderna em países não-europeus nos séculos passados criou uma transformação inimaginável para os observadores europeus. O conceito de uma “ontologia indígena” em si tem de ser questionado, primeiro, não porque não existisse, mas porque está situado em uma nova época e se transformou de tal forma que dificilmente há como voltar atrás e restaurá-lo. Esta é precisamente a razão pela qual temos que conceber um pensamento cosmotécnico do ponto de vista dessas ontologias sem cair em um etnocentrismo19. A transformação provocada pela tecnologia moderna não só ocorreu em culturas nãoeuropeias no decorrer da colonização, mas também na cultura europeia, com a diferença significativa de que, para a primeira, foi através da importação de aparelhos tecnológicos tão avançados quanto as tecnologias militares, e para a última, principalmente através da invenção tecnológica.
> (p. 7)

> 19: Em The Question Concerning Technology in China: an Essay in Cosmothechnics (Y. Hui, 2016b), eu uso a China como um exemplo para explicar como o conhecimento tradicional foi destruído ou minado durante o processo de modernização. No entanto, também argumento que um “retorno” não é mais uma opção real, já que é impossível em vista da atual situação geopolítica e socioeconômica. Proponho desenvolver um pensamento cosmotécnico da filosofia chinesa, a fim de demonstrar como tal linhagem do pensamento tecnológico, extraída do pensamento chinês, pode contribuir para refletir sobre o problema e o desenvolvimento futuro das tecnologias globais. Déborah Danowski e Eduardo Viveiros de Castro, em seu livro Há mundo por vir? (2014), criticaram o fracasso de Latour em reconhecer as vantagens e recursos das “pequenas populações e da tecnologia “relativamente rudimentar” dos povos indígenas” (p. 127), e parece-me que aí se pode ser facilmente vítima de um etnocentrismo, acreditando que a solução já está presente no pensamento ocidental ou indígena, de alguma forma, desde sempre. A grande questão para nós é de que maneira as ontologias indígenas podem entrar em diálogo com a tecnologia e a metafísica ocidentais e, assim, transformar a tendência atual das tecnologias globais.
> (p. 7)

# Cosmologia (natureza e ontologia) -> cosmotécnica (evidenciar práticas além de discussões sobre conhecimentos e teorias)

> O que é central para os conceitos de “natureza” e “ontologia” dos antropólogos é a cosmologia, uma vez que tal “natureza” é definida de acordo com diferentes “ecologias de relações” nas quais observamos diferentes constelações de relações, por exemplo, a relação parental entre o feminino e os vegetais, ou na fraternidade entre caçadores e animais. Essas relações podem ser rastreadas em atividades técnicas, como a invenção e o uso de ferramentas. Esta é a razão pela qual podemos querer conceber uma cosmotécnica em vez de falar apenas de cosmologia, o que pode nos limitar a discussões sobre conhecimentos e atitudes teóricas
> (p. 7)

# Haraway sobre o Chthuluceno. Simbiose, autopoiesis => sympoiesis

 > Se Descola vê a política como o momento do encontro e da negociação entre diferentes ontologias, num sentido mais ou menos latouriano, Haraway não tem uma apresentação esquemática das ontologias, mas sim uma concepção mais generalizada da política não-humana. Haraway caracteriza o Chthuluceno como a próxima fase depois do Antropoceno e do Capitaloceno. Chthuluceno, de acordo com Haraway, é um composto de duas raízes gregas - khthôn e kainos - o que significa olhar de baixo a partir da perspectiva de outras formas de seres vivos22. Como bióloga e cientista social, Haraway se propõe a pensar a política como uma maneira de “criar parentesco”, de conceber _sympoiesis_ entre diferentes espécies. Este neologismo, “sympoiesis”, relaciona-se com a simbiose e a autopoiese. No entanto, difere deles, pois, em primeiro lugar, simplesmente não significa que seja mutuamente benéfico23; e, em segundo lugar, enfatiza o fato de que a relação entre seres humanos e outros seres é altamente interdependente, problematizando o “auto”.
 > (p. 8)

> É uma “biopolítica” por excelência, como Haraway escreve, que as “biologias, artes e políticas precisam umas das outras; com o momentum involutivo, elas atraem umas às outras para pensar/criar em _sympoiesis_ mundos mais habitáveis, que eu chamo de Chthuluceno
> (p. 8)

> Podemos provavelmente resumir a proposta de Haraway como uma “biologia contra tecnocracia”, de tal forma que, sempre que os humanos desenvolvam tecnologias, terão que avaliar seu impacto sobre outras formas de seres vivos. A abordagem de Haraway é profundamente ética e tem a vantagem de fornecer uma política ontológica generalizada e ética para superar o Antropoceno. O conceito de simpoiese é uma tentativa de estabelecer limites ao desenvolvimento tecnocrático, e a possibilidade da simpoiese se torna a condição para proteger as espécies da destruição.
> (p. 8-9)

# Simondon sobre a oposição entre cultura e técnica

> Podemos simplesmente entendê-lo das seguintes maneiras. Por um lado, as máquinas se tornam opacas para seus usuários, e apenas especialistas entendem como reparar suas peças (e cada vez menos a máquina inteira). Esta é uma das fontes de alienação nos séculos XIX e XX: trabalhadores que estavam acostumados a utilizar ferramentas simples não são capazes de lidar com as novas operações ou entender a realidade técnica. Por outro lado, as máquinas são tratadas meramente como objetos funcionais, isto é, utilitários; são produtos consumíveis secundários a objetos estéticos e, em casos extremos, escravos, como manifestado na concepção pública dos robôs.
> (p. 9)

# A compatibilização entre natureza e cultura só pode ser pensada em conjunto com a tecnologia

> Simondon está se referindo aqui a uma relação operacional e ética entre o humano e a máquina. A questão da coexistência ou estar-com é de suprema importância nesta conexão. Haraway e Descola estão certos em ressaltar a necessidade de reconsiderar a questão da coexistência com a natureza. No entanto, essa coexistência só é possível quando refletimos sobre o papel dos objetos técnicos, que não só têm sua própria existência, mas também função como _relações_ com outras existências. A questão da coexistência, portanto, diz respeito não apenas às relações entre o humano e o nãohumano. Devemos também acrescentar-lhe a questão dos objetos técnicos, ou máquinas. 
> (p. 9)

# Simondon sobre tecnologia como reticulação => cosmopoiesis

> Ou, dito de outra forma: se há uma ecologia em Simondon, ela entende a tecnologia em termos de modos de reticulação e progresso tecnológico como a constante transformação de formas de reticulação. Esse ponto fica evidente quando pensamos no surgimento de diferentes redes de comunicação, do analógico ao digital no século XX, e agora com todo tipo de redes sociais. No entanto, nem todos os modos de reticulação levam a uma reconciliação entre natureza e técnica; ou talvez possamos dizer que, no pensamento de Simondon, ela é caracterizada por uma _cosmopoeisis_.
> (p. 10)

> 27: Em _On the Existence of Digital Objects_ (Hui 2016a), reprovei Simondon por limitar demais sua noção de reticulação às limitações geográficas, e argumentei que sua teoria de reticulação, portanto, não pode fornecer uma ordem apropriada de magnitude para entender os objetos digitais. No entanto, no que diz respeito às crises ecológicas e ambientais, sua ênfase na compatibilidade entre o meio geográfico e o meio técnico ainda é significativa.
> (p. 10)

# Cosmos -> técnica -> natureza (existência humana, progresso humano)

> Eu sugiro, em primeiro lugar, considerar o técnico a priori no conceito de natureza, o que nos permite abandonar uma imagem pura e inocente da natureza, trazendo-nos uma “segunda natureza”; e, em segundo lugar, o cósmico a priori no desenvolvimento tecnológico, o que significa que as técnicas são sempre cosmotécnicas desde o início. Esses são os dois lados da mesma moeda que chamamos de existência humana e progresso humano. 
> (p. 10)

# A natureza como pré-individual em Simondon

> Em _A Individuação à luz das noções de forma e informação_, a natureza é considerada como o pré-individual, que é como o _apeiron_ de Anaximandro, um potencial inesgotável30 . O préindividual é o que permite a individuação seguinte a acontecer. No entanto, isso não significa que a natureza é um reservatório de energia, mas sim que é sempre anterior ao ser já individualizado e que dá origem a uma segunda individuação quando são dadas as condições.
> (p. 10)

# Origem da tecnologia na fase mágica (reticulação ~ pontos chave cronogeográficos)

> Para Simondon, a história da tecnologia pode ser vista como um progresso constante dos modos de reticulação das forças espirituais. O início da história da tecnologia começou com o que ele chama de “fase mágica”31. A reticulação da fase mágica é caracterizada pelo que ele chama de pontos-chave (points clés): por exemplo, uma árvore gigante, uma rocha enorme, um pico alto ou um rio. Esses pontos geográficos são os pontos-chave, que mantêm a reticulação das forças; ou, mais precisamente, não é que esses pontos-chave são a origem das forças, mas sim essas forças são reguladas de acordo com os pontos-chave. Na fase mágica, propõe Simondon, há uma forma de unidade, onde não há distinção entre sujeito e objeto; o fundo e a figura se apoiam mutamente, o que significa que o fundo dá forma e as figuras limitam o fundo, como vemos em exemplos típicos da psicologia da Gestalt.
> (p. 10-1)

> > O universo mágico já é estruturado, mas segundo um modo anterior à segregação do objeto e do sujeito. Esse modo primitivo de estruturação é o que distingue figura e fundo, marcando pontos-chave no universo. (...) Mas, antes da segregação das unidades, instutuise uma reticulação do espaço e do tempo que evidencia lugares e momentos privilegiados, como se todo o poder de ação do homem e toda a capacidade do mundo de influenciá-lo se concentrassem nesses lugares e momentos.
> (p. 11)

> A defasagem ou eliminação gradual [_déphasage_] da fase mágica é desenvolvida nas técnicas e religiões. As veias do rito - que são objetos técnicos - tornam-se os pontoschave de outro modo de reticulação33. Este é o ponto a partir do qual podemos tematizar o conceito de _cosmotécnica_. Esta fase marcou um pensamento estético que foi capaz de criar uma convergência após a bifurcação entre a religião e a técnica, mas que mais tarde foi considerada insuficiente.
> (p. 11)

# Ruptura entre técnica e natureza na modernidade

> Como em Heidegger, em Simondon nós encontramos outra apresentação sobre a ruptura da relação entre técnica e natureza durante a modernidade europeia. Simondon está de acordo com a crítica de Jean-Jacques Rousseau à enciclopédia de Denis Diderot e Jean le Rond D'Alembert, por desvincular a técnica da natureza - ou, nas palavras de Simondon, dos “elementos” no sentido dos pré-socráticos (por exemplo, a água de Thales, o fogo de Heráclito e o _apeiron_ de Anaximandro)36. O distanciamento das técnicas da natureza continuou durante a modernidade europeia e, como Simondon notou, no final do século XVIII, a ruptura foi ampliada na medida em que as antigas técnicas foram reprimidas, a relação com o mundo natural foi perdida e os objetos técnicos tornaram-se objetos “artificiais” - artificiais, no sentido de que não têm nada a ver com a natureza37. Este período corresponde a “uma concepção dramática e apaixonada do progresso, que se transforma em violação da natureza, conquista do mundo, captação de energias”.
> (p. 11)

# Pensamento estético e filosófico em Simondon

> É esta questão que leva Simondon a mediar sobre a questão da convergência e a possível reconciliação entre a natureza e a tecnologia como uma tarefa do pensamento filosófico em vez do pensamento estético. No entanto, não seria justificável dizer que Simondon opõe pensamento estético e pensamento filosófico. A crítica de Simondon é que, ao glorificar o valor estético dos objetos (o que ele chama de “objetos estéticos”39), tende-se a reduzir o papel dos objetos técnicos como mera utilidade e ignorar a significação de sua realidade técnica; mas sempre precisaremos de pensamento estético, e complementá-lo com o pensamento filosófico.
> (p. 12)

# Essência da realidade técnica (esse ir além se refere a ver o ponto de encontro entre técnia e religião no mágico?)

> Como a técnica é fundamentalmente uma questão de reticulação de modos, há sempre a possibilidade de reconstituir diferentes pontos-chave. Isso é apenas para dizer que essa imaginação filosófico-antropológica das unidades, que caracteriza o início da gênese da tecnicidade, exige uma busca de convergência que reúna as diferentes profissões e diferentes especialidades da história humana. Nessa conexão, Simondon invocou Martin Heidegger em seu MEOT:
>
> > Na tecnicidade integrada no mundo natural e no mundo humano, essas formas de respeito e desrespeito manifestam a inerência de valores que ultrapassam a utilidade. O pensamento que reconhece a natureza da realidade técnica é o que, indo além dos objetos separados, dos utensílios, segundo a expressão de Heidegger, descobre a essência e o alcance da organização técnica, para além dos objetos distintos e das profissões especializadas.
>  (p. 12)

> Não está claramente indicado onde a referência a Heidegger deve ser encontrada. No entanto, podemos provavelmente fazer uma alusão ao ensaio de Heidegger “The Thing”41 , no qual ele propõe o _four-fold_ [_das Geviert_], ou seja, o paraíso, a terra, os mortais e Deus, para caracterizar tal convergência na _Thing_.
> (p. 12)

> 42: A coisa para Heidegger é opor-se ao objeto no sentido de Gegenstand – por-se contra; coisa, ou em alemão Ding, vem do verbo dinc, que significa juntar, reunir; na política de objetos, há um dualismo entre sujeito e objeto, enquanto na coisa, a política é sobre pertencer-junto [belong-together], portanto, uma coisa tem a função de reunir o quádruplo (cf. Y. Hui 2016a, p. 161-64).
> (p. 12)

# Gênese da tecnicidade na cosmotécnica. Convergência também deveria mediar o moderno e o tradicional.

> Eu reformulo a gênese da tecnicidade de Simondon como um pensamento cosmotécnico, e iria além de Simondon, acrescentando que esta busca por convergência deveria também mediar o moderno e o tradicional, que no processo de modernização se tornaram estranhos um ao outro - este foi o caso na Europa, e tem sido muito mais grave na China, Japão e outros países não-europeus nos últimos dois séculos.
> (p. 12)

# Desenvolvimento tecnológico como poiesis (Simondon). Forma cosmo-geográfica a priori (em relação ao meio tecnogeográfico?).

> Em vez de ver o desenvolvimento tecnológico como um estupro da natureza, Simondon tende a descobrir a _poeisis_ em um certo desenvolvimento da tecnologia, que tem tanto uma dimensão estética quanto uma dimensão produtiva. No entanto, devemos salientar que, no pensamento de Simondon, a reticulação é sempre dada como uma forma _cosmo-geográfica a priori_, e é o ponto de partida que podemos descrever como um _meio tecnogeográfico_ de conjuntos técnicos, como redes de ferrovias e arenas.
> (p. 12-3)

> Simondon analisa a relação tecnologia-natureza através de um desvio do antagonismo entre cultura e técnica. A tecnologia não está violando a natureza, como é reclamado com frequência; tal percepção resulta de um mal-entendido e da ignorância sobre a tecnologia. O objetivo do pensamento de Simondon é propor um programa através do qual a cultura seja capaz de reintegrar a tecnologia reconectando a natureza com a técnica. Ao fazê-lo, o antagonismo entre tecnologia e natureza pode ser resolvido. 
> (p. 13)

# Exemplo de co-naturalidade (natureza-técnica?) em Simondon

> > Olhe para esta antena da televisão como ela é... ela é rígida, mas é orientada; vemos que ela olha para a distância e pode receber sinais de um emissor distante. Para mim, parece ser mais que um símbolo; parece representar um tipo de gesto, um poder quase mágico de intencionalidade, uma forma contemporânea de magia. Nesse encontro entre o lugar mais alto e o ponto nodal, que é o ponto de transmissão das hiper frequências, há uma espécie de “co-naturalidade” entre a rede humana e a geografia natural da região. Há uma dimensão poética, bem como uma dimensão que tem a ver com a significação e o encontro entre significações.
> (p. 13)

> Nessa citação, podemos ver a unidade entre o meio geográfico e o meio técnico, na qual uma cosmopoiese é apresentada como uma “co-naturalidade”. A questão é, em primeiro lugar, de onde vem esse a priori cosmogeográfico? Não é puramente universal, pois varia de uma cultura para outra e condiciona diferentes formas de vida; sendo ainda transcendental, uma vez que não é universal, também carrega uma dimensão empírica e está, portanto, sujeita à renovação. Em segundo lugar, qual é o sentido do termo “poiesis” como experimentado em diferentes culturas? O senso de experiência poética dos gregos não é necessariamente o mesmo que o dos chineses. Simondon não era antropólogo, embora fosse um grande conhecedor das culturas grega e romana. Ele propôs uma teoria geral da gênese da tecnicidade que pode ser complementada por debates atuais em antropologia sobre o papel da natureza e da cosmologia. E as cosmologias, quando realizadas como cosmotécnicas, permitirão ir além dos limites do sistema técnico que está no progresso da realização, bem como ver como o pensamento cosmológico pode intervir na imaginação do desenvolvimento tecnológico.
> (p. 13-4)

# Turbina de Guimbal: exemplo da relação cosmo-geográfico a priori e construção do meio tecno-geográfico

> A fim de investigar isso, teremos que examinar outro exemplo que Simondon deu, relativo à conexão entre os meios técnicos e naturais, demonstrado pela turbina Guimbal46. Com este exemplo, queremos também levar esta linha de pensamento mais adiante do que fez Simondon ele mesmo. As turbinas anteriores à turbina Guimbal sofreram com o problema do superaquecimento: a turbina produzia tanto calor que se destruía. A invenção de Guimbal consistiu em um passo muito importante para a integração do “mundo natural” na operação da turbina. O “mundo natural” está aqui, por exemplo, um rio. A turbina é bem fechada e isolada com óleo, e colocada no rio. A corrente do rio levará a turbina a se mover; ao mesmo tempo, também transporta o calor produzido pela turbina. Teoricamente, quanto mais rápida a água, maior a quantidade de calor produzida; como a água é rápida, o calor se dissipa rapidamente. Neste caso, o rio se torna parte da operação, embora não seja realmente um componente no interior da turbina; ao contrário, é o que Simondon chama de _meio associado_ (_milieu associé_).
> (p. 14)

# Meio associado em Simondon

> Um meio associado é definido por uma causalidade recorrente entre _output_ e _input_, da mesma maneira como podemos entender o “feedback” na cibernética48. No entanto, Simondon também vai além do mecanismo de “feedback” da cibernética e considera a formação do meio associado dentro de um processo técnico geral de _concretização_. O meio associado é, neste caso, também um meio tecno-geográfico. A máquina exige um meio associado, que é parte de um mecanismo que permite que a máquina retome seu _status_ normal de trabalho diante de perturbações externas e internas.
> (p. 14)

> Podemos querer considerar que aqui o _cosmo-geográfico a priori_ não é meramente estético, nem pano de fundo, mas também operacional. Tem sua _significação_, não simplesmente como um objeto estético, mas também como um esquema interior ao objeto técnico, e não simplesmente a função exterior ao objeto. 
> (p. 14)

# Contrapondo com Haraway a análise de Simondon falha em não considerar outras espécies no meio associado

> No entanto, podemos querer problematizar este exemplo dado por Simondon: no caso da turbina, e sua integração do mundo natural como parte de sua operação, o que acontece com os outros seres vivos, por exemplo, os peixes nadando no rio? Essa é uma questão que Simondon não abordou em seu MEOT, e que também está fora do escopo do antagonismo estabelecido por Simondon no início de seu trabalho: cultura vs. técnica. Como tal, pode servir como um exemplo negativo para Haraway (2016) em seu _Staying with the Trouble_. Vamos propor que é por essa razão que podemos complementar a análise de Simondon com os debates atuais em antropologia, a fim de conceber uma _cosmotécnica_ confrontando a atual exploração tecnológica global.
> (p. 14)

# Feedback em Simondon

> 48: Na coleção póstuma de artigos de Simondon _Sur la philosophie_ (G. Simondon, 2016, p. 51), descobrimos que ele postulava várias traduções para o termo “feedback”, por exemplo “ressonância interna [“résonance interne”], “contra-reação” [“contre-réaction”], “recorrência de causalidade” [“récurrence de causalité”], e “causalidade circular” [“causalité circulaire”]
> (p. 14)

# Fim do cosmos e nascimento da ecologia (McLuhan)

> > O Sputnik criou um novo ambiente para o planeta. Pela primeira vez, o mundo natural estava completamente fechado em um recipiente feito pelo homem. No momento em que a Terra entrou neste novo artefato, a Natureza terminou e a Ecologia nasceu. O pensamento "ecológico" tornou-se inevitável assim que o planeta se elevou ao _status_ de obra de arte.
> (p. 15)

# Modernização como homogeneização tecnológica imposta (vinculada a uma epistemologia específica)

> Ao mesmo tempo, a globalização tecnológica apenas exporta tecnologias homogêneas inseridas em uma epistemologia muito estreita e pré-definida, e outras culturas são forçadas a se adaptar a essa tecnologia ou então a replicá-la. Podemos chamar esse processo de modernização. O processo de modernização impulsionado pela competição econômica e militar nos impediu de ver a multiplicidade das cosmotécnicas; em vez disso, obrigou-nos a identificar todas as cosmotécnicas como parte de uma linhagem tecnológica universal.
> (p. 15)

# As tecnologias industriais são eficientes porque são homogêneas (por que?)

> As tecnologias industriais capitalistas são eficientes porque são na maior parte homogêneas e puramente calculistas. Elas são homogêneas porque ignoram epistemologias e práticas heterogêneas. Essas tecnologias industriais tendem a se universalizar, ou seja, elas podem ir além das fronteiras culturais e nacionais, em um processo conhecido como globalização
> (p. 15)

# Exemplo de alternativa: rede social baseada em coletividades

> No entanto, é necessário acessar criticamente esses modelos industriais e demonstrar alternativas51. Um exemplo concreto que gostaria de fornecer é um projeto que conduzi com o cientista da computação Harry Halpin para desenvolver um modelo alternativo para plataformas como o Facebook. Traçando a história da rede social de volta ao psicólogo social Jacob Moreno e sua invenção da sociometria, mostramos que tal rede social é baseada em um conceito individualista, ou seja, de átomos sociais, em que cada indivíduo é um átomo social e a sociedade é um agregado de átomos sociais52 . Propusemo-nos a desenvolver outro modelo baseado em grupos ao invés de indivíduos, em colaboração ao invés de atividades individuais. Os exemplos certamente não se limitam às redes sociais (por exemplo, neste caso, o conceito de relações sociais, indivíduos, grupos, coletivos), onde não se deve sentir desamparado diante da totalidade da tecnologia moderna, mas sim buscar a possibilidade de reapropriar-se, como famosamente diz Gilles Deleuze: “não há necessidade de temer ou esperar, mas apenas de procurar novas armas”.
> (p. 16)

# Conceber o cosmo como exterioridade para desenvolver novas sensibilidades históricas e inventar cosmotécnicas

> Em segundo lugar, da exterioridade, devemos conceber o cosmos como uma exterioridade ao sistema tecnológico, em vez da visão antropocêntrica das atividades humanas como o centro da universalidade, para ter em mente o limite de tal sistema, além do qual é desconhecido e misterioso54. No entanto, isso não significa de forma alguma mistificar o cosmos novamente, ou uma proposta de voltar à cosmologia pré-moderna, mas sim desenvolver novas sensibilidades históricas que nos permitam reapropriar-nos da tecnologia moderna, não apenas para reutilizá-la (como nós mencionamos acima), mas também para inventar uma cosmotécnica de nossa época. 
> (p. 16)

> Uso a sensibilidade e cosmotécnica no plural para enfatizar que não é apenas uma sensibilidade ou uma cosmotécnica, mas uma reabertura da questão da tecnologia através da afirmação de culturas não-modernas. Para permitir que isso ocorra, _toda_ cultura terá que recuperar e formular sua própria história das cosmotécnicas e somente por meio de um estudo histórico poderá nos revelar uma nova cosmotécnica. O Antropoceno apresenta a necessidade de reconceber a relação entre os seres humanos e a Terra/cosmos, que se reflete nas discussões entre antropólogos, no pluralismo ontológico de Descola, no multinaturalismo de Viveiros de Castro e na teoria de Gaia de Latour (2015). 
> (p. 16)

> Entretanto, essa nova relação não pode evitar a questão da tecnologia, já que a natureza não é um porto seguro, e essa é a tarefa que eu acho que a filosofia da tecnologia precisa abrir - isto é, redescobrir as múltiplas cosmotécnicas além do discurso atual da tecnologia, limitado como está ao grego _technè_ e à tecnologia moderna advinda da modernidade ocidental, e desenvolver um referencial teórico que permita uma apropriação das tecnologias modernas como um _Ereignis_ no Antropoceno e uma superação das oposições entre cultura e natureza, e entre cultura e técnica.
> (p. 16)
