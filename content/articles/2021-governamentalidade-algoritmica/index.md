---
title: "Governamentalidade algorítmica e perspectivas de emancipação"
date: 2021-12-27
path: "/governamentalidade-algoritmica"
category: fichamento
author: Rafael Gonçalves
featuredImage: "./corinnevionnet.jpg"
srcInfo:  <a href="https://corinnevionnet.com/Photo-Opportunities">"Photo opportunities" por Corinne Vionnet</a>
published: true
tags:
 - modulação
 - Antoinette Rouvroy
 - Thomas Berns
 - governamentalidade
 - algoritmo
 - inteligência artificial
 - transindividual
 - rizoma
 - díspar

---

Fichamento do artigo "Governamentalidade algorítmica e perspectivas de emancipação: o díspar como condição de individuação pela relação?"[^1] de Antoinette Rouvroy e Thomas Berns.

[^1]: ROUVROY, A.; BERNS, T. Governamentalidade algorítmica e perspectivas de emancipação: o díspar como condição de individuação pela relação?. Revista ECO-Pós, [S. l.], v. 18, n. 2, p. 36–56, 2015. DOI: 10.29146/eco-pos.v18i2.2662. Disponível em: https://revistaecopos.eco.ufrj.br/eco_pos/article/view/2662. Acesso em: 16 dez. 2021.

# Análise de dados e inferência estatística como afastamento da noção de média ou normal

Mas seria mesmo? Ou seriam médias mais específicas (média homem-branco-rico, média mulher-branca-pobre, etc.), mais complexas, dividuais (imagens de rostos, padrões de cliques, etc. como "pedaços" metrificáveis e padronizáveis em uma média)? O que muda entre uma média (regressão linear?) e uma inferência estatística complexa (não-linear, não totalmente entendível ao humano, etc.)

> As novas oportunidades de agregação, análise e correlações estatísticas em meio a quantidades massivas de dados (os big data), afastando-nos das perspectivas estatísticas tradicionais do homem médio, parecem permitir “apreender” a “realidade social” como tal, de maneira direta e imanente, numa perspectiva emancipada de  toda  relação  à  “média”  ou  ao  “normal”  ou,  para  dizê-lo  de  outro  modo,  liberta  da  “norma”. 
> (p. 37)

# Novo regime de verdade digital encarnado numa multiplicidade de sistemas automáticos de modelização do "social"

> “Objetividade a-normativa”, ou mesmo “tele-objetividade” (Virilio, 2006, p.4), o novo regime de verdade digital se encarna numa multiplicidade de novos sistemas automáticos  de  modelização  do  “social”,  ao  mesmo  tempo  à  distância  e  em  tempo  real, acentuando a contextualização e a personalização automática das interações se-curitárias, sanitárias, administrativas, comerciais...
> (p. 37)

# Correlação no datamining reconstruiria singularidades pulverizadas sem relacioná-las a uma norma geral (mais afim com a ideia de regressão)

Sobre isso, me parece importante refletir que o digital é sempre uma redução em relação ao analógico. Assim, um tal "sistema [digital] de relações entre diversas medidas" não seria capaz de representar o todo analógico. Dessa perspectiva, tal sistema recairia sobre generalizações (A se relaciona com B, A' se reduz a A na representação digital, então A' seria dito em relação com B) de forma que existiriam normas (onde essas linhas analógicas quebram?).

> {5}: O _datamining_, articulado às finalidades de elaboração de perfis, reconstrói, seguindo uma lógica de correlação, os casos singulares  pulverizados  pelas  codificações  sem,  no  entanto,  relacioná-los  a  nenhuma  “norma”  geral,  mas  sobretudo  a  um  sistema  de  relações entre diversas medidas, irredutíveis a qualquer “média”. Sobre a distinção entre modelos de correlação e de regressão, ver Desrosières (1988).
> (p. 37)

# Caráter a-normativo da governamentalidade algorítmica

Normatividade contemporânea como normatividade adaptativa? Neste sentido não existiria média absolutamente, mas média como relação (A/B=cte), como função (f(X)), como modulação (Y = A*X).

> A crítica que nós desenvolvemos em re-lação à governamentalidade algorítmica não ignora, nem invalida em nada o ponto de vista dos estudos de ciência e tecnologia, mas que nos seja permitido concentrar nossa atenção sobre outra coisa que não os mecanismos de co-construção entre dispositivos  tecnológicos  e  atores  humanos.  Aqui,  propomos  simplesmente  que  o  _datamining_,  articulado  às  finalidades  de  elaboração  de  perfis  (quaisquer  que  sejam  os  aplicativos  envolvidos),  reconstrói,  seguindo  uma  lógica  de  correlação,  os  casos  singulares  pulverizados  pelas  condificações  sem,  no  entanto,  relacioná-los  a  nenhuma  norma geral,
> mas  somente  a  um  sistema  de  relações,  eminentemente  evolutivas,  entre  diversas  medidas, irredutíveis a qualquer média. Esta emancipação em relação a toda forma de média associa-se, notadamente, ao caráter autodidata destes dispositivos e pode ser considerada como essencial à ação normativa contemporânea.
> (p. 37-8)

# No datamining a estatística não serviria para gerar um espaço público nem privado, mas uma colonização do público pelo privado hipertrofiado (radicalizaçao de opiniões, economia da atenção, ...)

> Os  usos  particulares da estatística implicados nas operações de _datamining_, posto que não se ancoram  mais  em  qualquer  convenção,  permitem  escapar  desse  perigo,  mas,  como  veremos  mais  adiante,  não  são  nem  por  isso  geradores  de  espaço  público,  nem  do  contrário: sob a aparência de “personalização” das ofertas de informação, de serviços e de produtos, é sobretudo uma colonização do espaço público por uma esfera privada hipertrofiada  que  devemos  investigar  na  era  da  governamentalidade  algorítmica,  a  ponto de temermos que os novos modos de filtragem da informação levem a formas de imunização informacionais favoráveis a uma radicalização das opiniões e ao desa-parecimento da experiência comum (Sustein, 2009), sem mesmo evocar a tendência à  captação  sistemática  de  toda  parcela  de  atenção  humana  disponível  em  proveito  de interesses privados (a economia da atenção), em vez de contribuir para o debate democrático e o interesse geral. 
> (p. 38)

# Disparação (Deleuze sobre a individuação em Simondon)

> {7}:  “Gilbert Simondon mostrou (...) que a individuação supõe, em primeiro lugar, um estado metaestável, isto é, a existência de uma “disparação” como duas ordens de grandeza ou duas escalas de realidade heterogêneas, pelo menos, entre as quais os potenciais se repartem. Esse estado pré-individual não carece, todavia, de singularidades: os pontos relevantes ou singulares são definidos pela 
> existência e pela repartição dos potenciais. Aparece, assim, um campo “problemático” objetivo, determinado pela distância entre ordens heterogêneas. A individuação surge como o ato de solução de um tal problema ou, o que dá no mesmo, como a atualização do potencial e o estabelecimento de comunicação entre os díspares” (Deleuze, 1968, p. 317).
> (p. 38-9)

# Dataveillence - coleta automatizada de dados não classificados (disponibilizados por empresas, Estados, indivíduos, etc.)

> O  primeiro  momento  é  o  da  coleta  e  da  conservação  automatizada  de  quantidade  massiva  de  dados  não  classificados,  o  que  se  pode  chamar  dataveillance,  constitu-tiva do big data. Efetivamente, os dados estão disponíveis em quantidades massivas, provenientes de fontes diversas. Os governos os coletam para fins de segurança, con-trole, gestão dos recursos, otimização das despesas...; as empresas privadas recolhem quantidades de dados para fins de marketing e publicidade, de individualização das ofertas, de melhorias de sua gestão de estoques ou de suas ofertas de serviço, enfim, com vistas a aumentar sua eficácia comercial e, portanto, seus lucros...; os cientistas coletam os dados para fins de aquisição e de aperfeiçoamento de conhecimentos...; os próprios indivíduos compartilham benevolamente “seus” dados nas redes sociais, blogs, listas de e-mail... e todos esses dados são conservados sob uma forma eletrônica, em “armazéns de dados” de capacidades de estocagem virtualmente ilimitadas e po-tencialmente acessíveis a todo momento a partir de qualquer computador conectado à internet, qualquer que seja o lugar do globo onde se encontra. 
> (p. 39)

# Aparência de não-intencionalidade na coleta automatizada de dados

> O fato de que esses dados sejam coletados e conservados o máximo possível de forma automática,quer eles  sejam  desvinculados  de  todo  conhecimento  verdadeiro  das  finalidades  alme-jadas por esta coleta de informação, isto é, dos usos aos quais eles darão lugar uma vez  correlacionados  a  outros  dados,  quer  eles  consistam  em  informações  que  são  mais abandonadas que cedidas, traços deixados e não dados transmitidos, mas sem aparecer, apesar disso, como “roubados”, quer eles apareçam também como absoluta-mente quaisquer e dispersos, tudo isto dá lugar a um esvaziamento ou, no mínimo, a um ocultamentode toda finalidade e a uma minimização da implicação do sujeito e, portanto, do consentimento que pode ser dado a esta comunicação de informações: parecemo-nos nos mover aqui para o mais longe de toda forma de intencionalidade.
> (p. 39)

# Comportamentalismo digital - criação segmentada de dados "brutos" que não fazem sentido coletivamente a partir do real

> Esses dados aparecem assim constitutivos de um “comportamentalismo” digital gene-ralizado (Rouvroy, 2013a) uma vez que eles exprimem nem mais, nem menos que as múltiplas facetas do real, desdobrando-o em sua totalidade, mas de maneira perfeita-mente  segmentada,  sem  fazer  sentido  coletivamente,  senão  como  desdobramento  do real.
> (p. 39)

> Este nos parece ser o fenômeno mais novo: quer se trate de conservar o traço de uma compra, de um deslocamento, do uso de uma palavra ou de uma língua, cada elemento é reconduzido a sua natureza mais bruta, isto é, ser ao mesmo tempo abs-traído  do  contexto  no  qual  apareceu  e  reduzido  a  “dado”.
> (p. 39)

# Caráter objetivo, não-intencional e pouco subjetivo dos dados

> Um  dado  não  é  mais  que  um  sinal  expurgado  de  toda  significação  própria  –  e  certamente  é  por  conta  disso  que  nós  toleramos  deixar  esses  traços,  mas  é  também  o  que  parece  assegurar  sua  pretensão à mais perfeita objetividade: tão heterogêneos, tão pouco intencionados, tão materiais e tão pouco subjetivos, tais dados não podem mentir!
> (p. 39)

> O que é interessante é o fato de que tais dados têm, como principal característica, serem perfeitamente  anódinos,  poderem  permanecer  anônimos  e  serem  não-controláveis.
> (p. 39)

# Inofensividade e objetividade dos dados como evitação da subjetividade

> Por esta razão, e ao mesmo tempo, não repugnamos a possibilidade de abandoná-los, uma vez que eles não fazem sentido (conquanto eles não sejam correlacionados, ao menos), eles são bem menos intrusivos que uma carta de fidelidade e eles parecem não mentir, isto é, podem ser considerados como perfeitamente objetivos! Esta ino-fensividade e esta objetividade são ambas devidas a uma espécie de evitação da subjetividade.
> (p. 40)

# Datamining - tratamento automatizado de dados (dos quais emergem -- são produzidas -- correlações como forma de conhecimento)

> O  segundo  momento  é  aquele  do  datamining  propriamente  dito,  a  saber,  o  trata-mento automatizado destas quantidades massivas de dados de modo a fazer emergir correlações sutis entre eles. O que nos parece fundamental notar aqui é o fato de que nós nos encontramos, assim, diante de uma produção de saber (saberes estatísticos constituídos de simples correlações) a partir de informações não-classificadas e, por-tanto, perfeitamente heterogêneas, a produção de saber estando automatizada, isto é, solicitando apenas um mínimo de intervenção humana e, sobretudo, dispensando toda forma de hipótese prévia (como era o caso com a estatística tradicional que “verificava”  uma  hipótese),  isto  é,  evitando  novamente  toda  forma  de  subjetividade.
> (p. 40)

# Machine learning como produção de hipóteses a partir de dados

Ou mais propriamente dito, criação de correlações das quais é possível realizar inferências

> O  próprio daquilo a que chamamos machine learning é, em resumo, tornar diretamente possível a produção de hipótese a partir dos próprios dados.
> (p. 40)

# Aparente objetividade absoluta (livre de intervenção subjetiva)

> Desta maneira, nós nos encontramos novamente diante da ideia de um saber cuja objetividade poderia parecer absoluta, uma vez que estaria afastado de toda intervenção subjetiva (de toda formulação  de  hipótese,  de  toda  triagem  entre  o  que  é  pertinente  e  o  que  seria  so-mente  “ruído”,  etc.).
> (p. 40)

# Normas parecem emergir do real

Há um ciclo de naturalização (atribuição de objetividade) aos resultados algoritmos que passam a justificar uma reincidência das normas que "emergem" desses dados -- que, por sua vez, criam a realidade social de onde os dados são extraídos. Mas como Pasquinelli e Joler argumentam, em todos os momentos há vieses incorporados (por exemplo discriminações históricas presentes nos dados "brutos"), de forma que naturalizar os algoritmos é conservar o _status quo_. Ou seja, ao contrário de: real -> inferência algoritmica -> normas naturais; há: produção do real -> inferência algoritmica -> reafirmação do real produzido.

> As  normas  parecem  emergir  diretamente  do  próprio  real.
> (p. 40)

# Correlação e 'causação'

> Essas  normas  ou  esses  “saberes”  não  são,  contudo,  constituídos  “apenas”  de  correlações8, o que não é em si um problema, se não o esquecermos; é a condição própria de um “etos” científico e um “etos” político conservar uma dúvida, entreter uma desconfian-ça quanto à suficiência das correlações, manter a distinção entre correlação e causa, desconfiar  dos  “efeitos”  autoperformativos  das  correlações  (sua  capacidade  retroati-va), evitar que as decisões produzindo efeitos jurídicos em relação a pessoas ou as a-fetando de maneira significativa não sejam tomadas com base somente no único fun-damento  de  um  tratamento  de  dados  automatizado9  e  considerar  que  o  próprio  da  política (notadamente a preocupação de uma mutualização dos riscos) é recusar agir com base apenas em correlações. Parece importante lembrar disto diante da evolução rumo a um mundo que parece, cada vez mais, funcionar como se fosse constituído ele mesmo de correlações, como se estas fossem o que bastasse estabelecer para assegu-rar seu bom funcionamento10. 
> (p. 40)

# Substituição de saberes já criados (ciência) por uma lógica (objetiva) de correlações

> {8}: Pode-se citar aqui C. Anderson, redator-chefe da Wired, em “L’âge des Petabits”: “É um mundo no qual as quantidades massivas de dados e as matemáticas aplicadas substituem todas as outras ferramentas que poderiam ser utilizadas. Adeus a todas as teorias sobre  os  comportamentos  humanos,  da  linguística  a  sociologia.  Esqueçam  a  taxonomia,  a  ontologia  e  a  psicologia.  Quem  pode  saber por que as pessoas fazem o que fazem? O fato é que o fazem e que nós podemos traça-lo e medi-lo com uma fidelidade sem precedente. Se tivermos dados suficientes, os números falam deles mesmos” (citado em Cardon, 2012).
> (p. 40)

# Insuficiência das leis atuais contra perfilamento algoritmico

> {9}: Notemos que o regime jurídico europeu de proteção dos dados de caráter pessoal protege explicitamente os indivíduos contra as decisões que seriam tomadas a seu respeito com base unicamente no fundamento de um tratamento automatizado de dados (ver o artigo 15 da diretiva 95/46/CE). Porém, as garantias oferecidas pela diretiva europeia só se aplicam caso os tratamentos automatiza-dos concirnam dados de caráter pessoal, isto é, dados que se relacionam a pessoas identificadas ou identificáveis. Ora, a elaboração algorítmica de perfis pode muito bem “funcionar” com dados anônimos.
> (p. 40)

# Informação individual x informação de perfil

> Para  compreender  bem  em  que  consiste  a  elaboração  algorítmica  de  perfis  da  qual  falamos aqui, é necessário perceber a diferença crucial existente entre, por um lado, 
> a _informação  ao  nível  individual_,  a  qual  é,  mais  frequentemente,  observável  ou  perceptível pelo indivíduo ao qual ela se relaciona e, por outro lado, o _saber produzido no  nível  da  elaboração  de  perfis_,  que  não  é  o  mais  frequentemente  disponível  para  os indivíduos nem perceptível por eles, mas que lhes é, apesar disso, _aplicado_ de ma-neira a inferir deles um saber ou previsões probabilistas quanto às suas preferências, intenções e propensões, que não seriam, de outra forma, manifestas (Otterlo, 2013). 
> (p. 40-1)

# Inferência - ação sobre os comportamentos (uso dos saberes probabilísticos e estatísticos a fim de antecipar comportamentos individuais). Modulação?

> O terceiro momento é aquele do uso desses saberes probabilistas e estatísticos para fins  de  antecipação  dos  comportamentos  individuais,  que  são  associados  a  perfis  definidos a partir da base de correlações descobertas por _datamining_.
> (p. 41)

> Este momento de aplicação da norma aos comportamentos individuais, cujos exemplos os mais evi-dentes são perceptíveis nas esferas as mais diversas da existência humana (obtenção de  um  certo  crédito,  decisão  de  intervenção  cirúrgica,  tarifação  de  um  contrato  de  seguro, sugestão de compras direcionadas em sites de venda online), nos preocupa menos aqui, senão para notar, em primeiro lugar, que a eficácia preditiva será tanto maior  quanto  for  fruto  da  agregação  de  dados  massivos,  isto  é,  de  dados  que  estão  “simplesmente”  à  altura  da  diversidade  do  próprio  real11;  e  em  segundo  lugar,  que  essa ação por antecipação sobre os comportamentos individuais poderia, no futuro, sempre  se  limitar  mais  a  uma  intervenção  sobre  seu  ambiente,  forçosamente,  uma  vez que o ambiente é ele mesmo reativo e inteligente, isto é, ele próprio recolhe da-dos  em  tempo  real  pela  multiplicação  de  captores,  transmite-os  e  os  trabalha  para  se adaptar sem cessar a necessidades e perigos específicos, o que já é, no mínimo, o caso  daquela  parte  importante  da  vida  humana  durante  a  qual  os  indivíduos  estão  conectados.
> (p. 41)

# Ação se torna atuar sobre o meio a partir de antecipações para tornar desobediências improváveis

Neste sentido a modulação agiria diretamente sobre o meio e indiretamente sobre o indivíduo?

> Desta maneira, novamente evita-se toda forma de restrição direta sobre o indivíduo para preferir tornar, no próprio nível de seu ambiente, sua desobediência (ou  certas  formas  de  marginalidade)  sempre  mais  improváveis  (na  medida  em  que  estas teriam sempre já sido antecipadas).
> (p. 41)

# Perfilamento adaptável a cada indivíduo (afastado de uma média)

Contra a noção de um perfilamento ultraindividual, eu acredito que faça mais sentido pensar uma multiplicidade de perfilamentos dividuais, no sentido de que existiriam várias médias para indivíduos parciais que, se não se comportam como homem-médio no indivíduo, o fazem dividualmente.

> Em terceiro lugar, o perfil “ligado” ao com-portamento de um indivíduo poderia ser-lhe adaptado de maneira perfeitamente efi-caz pela multiplicação das correlações empregadas, a ponto de parecer evitar o uso de toda categoria discriminante e de poder mesmo levar em conta o que há de mais particular  em  cada  indivíduo,  ainda  mais  afastado  dos  grandes  números  e  médias.
> (p. 41)

# Normatividade aparentemente "democrática" (algoritmos isentos de categorias de classe, raça, etc.)

Relação com uma fixação moderna com a "Verdade", ou no caso, o "verdadeiro Real" substrato objetivo das interações humanas (subjetivas). Se o real é construído socialmente, não há normatividade democrática por natureza, existem normatividades em disputa política (científica-política se pensar na noção de ciência com política em Latour).

> Enfim, encontramo-nos diante da possibilidade de uma normatividade, em aparência, perfeitamente “democrática”, desprovida de referência a classes e categorias gerais – a cegueira dos algoritmos em relação às categorizações (sociais, políticas, religiosas, étnicas, de gênero...) socialmente experimentadas é, de resto, o argumento recorrente que  brandem  aqueles  que  são  favoráveis  ao  seu  uso  no  lugar  da  avaliação  humana  (nos aeroportos principalmente) (Zarsky, 2011).
> (p. 41)

# Não extinguir o anômalo, mas evitar o imprevisível (controlar?)

Sociedade de controle ~ modulação x sociedade disciplinar ~ molde

> Não se trata mais de excluir o que sai da média, mas de evitar o imprevisível, de tal modo que cada um seja verdadeiramente si mesmo.
> (p. 41)

# Eficácia baseada na confusão entre as etapas do processo

> Conforme  enunciado,  os  três  momentos  descritos  se  confundem  e  seu  funcionamento  normativo  é  tão  mais  potente  e  processual  conquanto  eles  se  alimentem
> mutuamente (mascarando ainda mais as finalidades, afastando ainda mais toda possibilidade  de  intencionalidade,  adaptando-se  ainda  mais  a  nossa  própria  realidade,  etc.).
> (p. 41-2)

# Governamentalidade algorítmica como racionalidade normativa (aparentemente anormativa, apolítica) instituída sobre a coleta e análise automatizada de dados visando modelizar, antecipar e afetar (por antecipação) comportamentos

> Por governamentalidade algorítmica, nós designamos, a partir daí, globalmente um  certo  tipo  de  racionalidade  (a)normativa  ou  (a)política  que  repousa  sobre  a  co-leta, agregação e análise automatizada de dados em quantidade massiva de modo a modelizar, antecipar e afetar, por antecipação, os comportamentos possíveis. 
> (p. 42)

# Deslocamentos aparentes do regime estatístico ao regime algoritmico. Individualização da estatística. Evitação de uma prática estatística tirânica (adesão tácita dos indivíduos).

Aparente individualização da estatística (homem médio -> perfil dinâmico)

> Se nos referimos à base geral do pensamento estatístico12, os deslocamentos aparentes, que seriam produzidos atualmente pela passagem do governo estatístico ao governo al-gorítmico e que dariam sentido a um fenômeno de rarefação dos processos de sub-jetivação, são, portanto, os seguintes: antes de tudo, uma aparente individualização da estatística (com a antinomia evidente que se exprime assim), a qual não transitaria mais (ou não pareceria mais transitar) por referências ao homem médio, para dar lugar à ideia de que seria possível tornar-se a si mesmo seu próprio perfil automaticamente atribuído e evolutivo em tempo real. 
> (p. 42)

Aparente democratização (assunção de concordância, adesão tácita x práticas tirânicas)

> Em seguida, uma preocupação elevada em evitar o perigo de uma prática estatística tirânica que reduziria o objeto estatístico a “gado”, zelando para que esta prática estatística se desenvolva como se nossa concordância estivesse dada, uma vez que é na medida em que somos cada um único que o modo de governo pelos algoritmos pretende se dirigir a cada um, através de seu perfil. 
> (p. 42)

> Em vez  de  um  acordo,  ou  mesmo  um  consentimento,  aquilo  com  que  lidamos  vem  da  adesão automática a uma normatividade tão imanente como aquela da própria vida; a prática estatística contemporânea incluiria, portanto, nela mesma, a expressão da a-desão tácita dos indivíduos.
> (p. 42)

> Donde um possível declínio da reflexividade subjetivante e o distanciamento das ocasiões de contestação das produções de “saber” fundadas no _datamining_ e na elaboração de perfis.
> (p. 42)

# Dados infraindividuais -> modelos supraindividuais

infraindividual ~ dividual (?)

> A governamentalidade algorítmica não pro-duz qualquer subjetivação, ela contorna e evita os sujeitos humanos reflexivos, ela se alimenta de dados “infraindividuais” insignificantes neles mesmos, para criar modelos de  comportamento  ou  perfis  supraindividuais  sem  jamais  interpelar  o  sujeito,  sem  jamais convocá-lo a dar-se conta por si mesmo daquilo que ele é, nem daquilo que ele poderia se tornar.
> (p. 42)

# Viralidade. Realimentação da falha (não gera crise). Maior importância dos positivos positivos do que dos falsos positivos.

> O momento de reflexividade, de crítica, de recalcitrância, necessário para que haja subjetivação parece, incessantemente, complicar-se e ser adiado (Rouvroy, 2011). É que a governamentalidade algorítmica, por sua perfeita adaptação ao “tempo real”, sua “viralidade” (quanto mais dela se serve, mais o sistema algorítmico se refina e se aperfeiçoa, uma vez que toda interação entre o sistema e o mundo se traduz por um registro de dados digitais, um enriquecimento correlativo da “base estatística” e uma melhoria das performances dos algoritmos), sua plasticidade, torna a própria noção de “falha” insignificante: a “falha” não pode, em outros termos, colocar o sistema em “crise”, ela é imediatamente reassimilada a fim de refinar ainda mais os modelos ou perfis de comportamento. Por outro lado, seguindo o objetivo do aplica-tivo que é feito de dispositivos algorítmicos – por exemplo, a prevenção das fraudes, do  crime,  do  terrorismo  –  os  “falso  positivos”  não  serão  nunca  interpretados  como  “falhas”  uma  vez  que  o  sistema  segue  uma  lógica  de  rastreamento  mais  do  que  de  diagnóstico: o objetivo é não deixar escapar qualquer positivo verdadeiro, qualquer que seja a taxa de falsos positivos
> (p. 42)

# Governamentalidade algoritmica como dispositivo de segurança (Foucault)

> > a regulação de um meio no qual não se trata tanto de fixar os limites, as fron-teiras, no qual não se trata tanto de determinar as posições, mas sobretudo, es-sencialmente,  de  permitir,  de  garantir,  de  assegurar  as  circulações:  circulações  de  pessoas,  circulação  de  mercadorias,  circulação  do  ar,  etc.  (Foucault,  2004,  p.31)
> (p. 43) 


> {13}: Assim como sua forma disciplinar, para retomar as modelizações foucaultianas do poder. Deste ponto de vista, nós nos situaría-mos  aqui  na  terceira  modelização  do  poder  analisada  por  Foucault,  a  que  analisa  os  dispositivos  de  segurança  numa  perspectiva  essencialmente  regulatória.  A  evolução  aqui  descrita  consistiria  em  estabelecer  neste  terceiro  modelo  do  poder  –  o  modelo  dos  dispositivos de segurança – novas rupturas. O princípio dos dispositivos de segurança “é não tomar nem o ponto de vista do que é impedido, nem o ponto de vista do que é obrigatório, mas recuar suficientemente para que se possa apreender o ponto em que as coisas vão se produzir, sejam elas desejáveis ou não. (...) A lei proíbe, a disciplina prescreve e a segurança, sem proibir nem prescrever, (...) tem essencialmente, por função, responder a uma realidade de modo que essa resposta anule essa realidade a que ela responde – anule ou limite ou freie ou regule. É essa regulação no elemento da realidade que é (...) fundamental nos dispositivos de segurança” (Foucault, 2004, p. 48-49).a regulação de um meio no qual não se trata tanto de fixar os limites, as fron-teiras, no qual não se trata tanto de determinar as posições, mas sobretudo, es-sencialmente,  de  permitir,  de  garantir,  de  assegurar  as  circulações:  circulações  de  pessoas,  circulação  de  mercadorias,  circulação  do  ar,  etc.  (Foucault,  2004,  p.31)
> (p. 43)

# "Poder" através de multiplos perfis (não corpo físico ou consciência moral)

> Que as “presas” do poder sejam digitais, mais que físicas, não significa, de modo algum, que os indivíduos sejam reduzíveis ontologicamente, existencialmente, a redes de da-dos recombináveis por aparelhos, nem que eles estejam totalmente sob o domínio de seus aparelhos. Significa simplesmente que, quaisquer que sejam, por outro lado, suas capacidades  de  entendimento,  de  vontade,  de  expressão,  não  é  mais  através  destas  capacidades que eles são interpelados pelo “poder”, mas, em vez disso, através de seus “perfis” (de fraudador potencial, de consumidor, de terrorista potencial, de aluno com forte potencial...).
> (p. 43)

# Tecnologia ajustada a cada um (Google)

> Como  disse  Eric  Schmidt,  Diretor  Geral  do  Google:  “Nós  sabemos,  em  linhas  gerais,
> quem vocês são, o que lhes interessa, quem são seus amigos (isto é, conhecemos seu “cardume”). A tecnologia será tão boa que será difícil para as pessoas ver ou consumir alguma coisa que não foi, em alguma medida, ajustada para elas” (isso quer dizer que uma previsão aparentemente individualizada seria possível).
> (p. 43-4)

# Adaptar o desejo do indivíduo ao produto, e não o contrário

> Essa forma de individualização se assemelha, de fato, a uma hipersegmentação e a uma hiperplasticidade das ofertas comerciais muito mais do que a uma consideração global das necessidades e desejos próprios a cada pessoa. Ao contrário, certamente o objetivo não é tanto adap-tar a oferta aos desejos espontâneos (se ao menos algo assim existir) dos indivíduos, mas, em vez disso, adaptar os desejos dos indivíduos à oferta, adaptando as estraté-gias de venda (a maneira de apresentar o produto, de fixar seu preço...) em proveito de  cada  um.
> (p. 44)

# Dynamic pricing

> Assim,  as  estratégias  de  _dynamic  pricing_  ou  de  adaptação  do  preço  de  certos serviços ou certas mercadorias de acordo com a _willingness to pay_ de cada con-sumidor  potencial  já  estariam  em  curso  em  alguns  sites  de  venda  online  de  viagens  aéreas.
> (p. 44)

# Exemplo: companhias aereas e perfil de "viajante cativo"

> Um exemplo bastante trivial: conecte-se no site de uma companhia aérea cujo nome não falaremos (a companhia Y) e se informe sobre os preços de uma pas-sagem de avião para Pisa, com saída de Bruxelas, partindo em três dias. Digamos que eles coloquem um preço de 180 euros. Achando o preço um pouco caro demais, você vai  ao  site  de  uma  outra  companhia  (a  companhia  Z)  ou  você  se  informa  em  outros  lugares  na  internet  para  encontrar  uma  passagem  menos  cara.  Supomos  que  você  não obtenha resultados. Você, então, retornará ao site da companhia Y e lá – surpresa! – você se dá conta de que o preço da passagem aumentou 50 euros num espaço de meia-hora apenas, o tempo em que você fazia sua pequena pesquisa. É simplesmente porque um perfil “viajante cativo” lhe foi atribuído: foi detectado, de acordo com seu percurso  pela  internet  e  a  data  de  partida  desejada,  que  você  precisava  verdadeira-mente dessa passagem de avião e que você estaria, então, disposto a gastar 50 euros a  mais  para  obter  essa  passagem,  posto  que  você  teria  a  impressão  de  que,  se  você  não  a  comprasse  logo,  o  preço  só  subiria.  Se,  no  lugar  de  reagir  “logicamente”  com-prando  o  mais  rápido  a  passagem,  você  mudasse  de  computador,  de  endereço  IP  e  entrasse novamente no site da companhia aérea, sua passagem lhe custaria 180 euros em  vez  de  230  euros.  Explicação?  O  primeiro  reflexo,  com  o  qual  o  vendedor  conta,  é  aquele  de  comprar  o  mais  cedo  possível,  seguindo  o  “alerta”  que  está  lançado:  o  preço aumenta, e rápido. Aqui neste caso, as consequências são relativamente triviais. Mas vê-se bem, nesse exemplo, que, em vez de respeitar escrupulosamente os dese-jos individuais de cada consumidor singular, trata-se, ao contrário, e baseando-se na detecção automática de certas propensões (de compra), da detecção da (in)elasticid-ade da demanda individual em relação a uma variação de preço; trata-se de suscitar o ato de compra no modo da resposta-reflexo a um estímulo de alerta que provoca um curto-circuito na reflexividade individual e na formação do desejo singular.
> (p. 44)

# Desejo subjetivo e reflexivo -> regulação objetiva e maquínica das condutas

Modulação como esse processo de captura do desejo (da subjetivação, da reflexividade) em prol de um funcionamento regulatório (minimização de erro), mecânico e previsível (?)

> Trata-se, portanto, de produzir a passagem ao ato sem formação nem formulação de desejo. O governo algorítmico parece, por esta razão, assinar a conclusão de um processo de dissipação das condições espaciais, temporais e linguísticas da subjetivação e da individuação em benefício de uma regulação objetiva, operacional das condutas possíveis, e isso a partir de “dados brutos”, neles mesmos a-significantes, e cujo tratamento  estatístico  visa,  antes  de  tudo,  acelerar  os  fluxos  –  poupando  toda  forma  de  “desvio”  ou  de  “suspensão  reflexiva”  subjetiva  entre  os  “estímulos”  e  suas  “respostas-reflexo”. Que aquilo que “corre” dessa forma seja a-significante não tem mais qualquer importância
> (p. 44)

# Servidão maquínica (Guattari)

> {14}:  Ao contrário, mesmo que o que « corre » seja a-significante é precisamente o que permite “a servidão maquínica”: “Há um incon-sciente maquínico molecular que vem de sistemas de codificação, sistemas automáticos, sistemas de moldagens, sistemas de em-préstimos, etc., que não colocam em jogo nem os canais semióticos, nem os fenômenos de subjetivação de relações sujeito/objeto, 
> nem  os  fenômenos  de  consciência;  que  colocam  em  jogo  o  que  eu  chamo  de  fenômenos  de  servidão  maquínica,  onde  funções,  órgãos entram diretamente em interação com sistemas maquínicos, sistemas semióticos. O exemplo que uso sempre é aquele da condução de um automóvel em estado de devaneio. Tudo funciona fora da consciência, todos os reflexos, pensamos em outra coisa e, no limite, chegamos a dormir; e depois, há um sinal semiótico de despertador que faz recobrar a consciência de uma só vez e reinjeta canais significantes. Há, portanto, um inconsciente de servidão maquínica” (Guattari, 1980).
> (p. 44-5)

# Aparente saturação a-significante do universo digital

> Graças ao fato de que os sinais digitais “podem ser calculados quantitativamente,  qualquer  que  se  seja  seu  significado”  (Eco  apud  Genosko,  2008),  tudo  se passa como se o significado não fosse mais absolutamente necessário, como se o universo estivesse já – independentemente de toda interpretação – saturado de sen-tido, como se não fosse mais, a partir de agora, necessário religar-nos uns aos outros pela  linguagem  significante,  nem  por  qualquer  transcrição  simbólica,  institucional,  convencional. Os dispositivos da governamentalidade algorítmica completam, assim, ao mesmo tempo, a emancipação dos significantes em relação aos significados (digi-talização,  recombinações  algorítmicas  dos  perfis)  e  a  substituição  dos  significantes  pelos  significados  (produção  da  realidade  em  contato  com  o  mundo  –  o  único  real  que “conta”, para a governamentalidade algorítmica, é o real digital) (Rouvroy, 2013b).
> (p. 44-5)

# Proletarização como perda do saber ante a máquina

Sobre isso, me parece que não é a máquina que absorve o saber (mito do robô em Simondon). Quem, então? Engenheiros? Donos?

> > A  proletarização  é  historicamente  a  perda  do  saber  do  trabalhador  frente  a  máquina que absorveu esse saber. Hoje, a proletarização é a padronização dos comportamentos  através  do  marketing  e  dos  serviços  e  a  mecanização  dos  espíritos  pela  exteriorização  dos  saberes  em  sistemas  tais  que  esses  “espíritos”  não sabem mais nada desses aparelhos de tratamento de informação de modo que  só  estabelecem  parâmetros:  é  precisamente  o  que  mostra  a  matematiza-ção  eletrônica  da  decisão  financeira.  Ora,  isso  afeta  todo  o  mundo:  emprega-dos, médicos, idealizadores, intelectuais, dirigentes. Cada vez mais, engenheiros participam  de  processos  técnicos  cujo  funcionamento  eles  ignoram,  mas  que  arruínam o mundo. (Stiegler, 2011)
> (p. 45)

# Semióticas a-significantes ~ servidão maquínica (Lazaratto)

> > Se  as  semióticas  significantes  têm  uma  função  de  alienação  subjetiva,  de  “as-sujeitamento  social”,  as  semióticas  a-significantes  possuem  uma  função  de  “servidão  maquínica”.  As  semióticas  a-significantes  operam  uma  sincronização  e uma modulação dos componentes pré-individuais e pré-verbais da subjetivi-dade, engendrando afetos, percepções, emoções, etc., como as peças, os com-ponentes, os elementos de uma máquina (servidão maquínica). Nós podemos funcionar todos como os componentes de input/output de máquinas semióti-cas, como simples retransmissores de televisão ou de internet, que fazem passar e/ou  impedem  a  passagem  da  informação,  da  comunicação,  dos  afetos.  Dife-rente das semióticas significantes, as semióticas a-significantes não conhecem nem as pessoas, nem os papeis, nem os sujeitos. (...) No primeiro caso, o sistema fala e faz falar. Ele indexa e dobra a multiplicidade das semióticas pré-significan-tes  e  simbólicas  sobre  a  linguagem,  sobre  os  canais  linguísticos,  privilegiando  suas  funções  representativas.  Enquanto  que,  no  segundo  caso,  o  sistema  não  produz discurso, não fala, mas funciona, coloca em movimento, conectando-se diretamente ao “sistema nervoso, ao cérebro, à memória, etc.”, ativando relações afetivas, transitivas, transindividuais dificilmente atribuíveis a um sujeito, a um indivíduo, a um eu. (Lazaratto, 2006) 
> (p. 45)

# Dupla estatística que produz ("automaticamente") dificuldade para subjetivação

Dupla estatística: captura automática de dados; correlação automatizada dos dados

> Sem considerá-lo como vão, nós queremos destacar aqui a indiferença desse “governo algorítmico” para os indivíduos, já que o mesmo se contenta em se interessar e em controlar nossa “dupla estatística”, isto é, os cruzamentos de correlações, produzidos de maneira automatizada, e com base em quantidades massivas de dados, estas constituídas  ou  coletadas  “automaticamente”.  Em  suma,  quem  nós  somos  grosso  modo,  para  retomar  a  citação  de  Eric  Schmidt,  não  é  mais,  de  modo  algum,  nós  mesmos  (seres singulares). E é justamente esse o problema, problema que, como nós veremos, acentuaria sobretudo uma rarefação dos processos e ocasiões de subjetivação, uma dificuldade de tornar-se sujeito, e não tanto um fenômeno de “dessubjetivação” ou de risco de extinção do indivíduo.
> (p. 46)

# 1 - Dificuldade de produzir um sujeito algorítmico que reflete sobre si e pensa como tal (consentimento)

> O  que  se  constata a princípio é uma dificuldade de produzir um sujeito algorítmico que reflete sobre si e se pensa como tal. Primeiro, como nós vimos, o consentimento do sujeito é  fraco  quando  ele  transmite  informação  (esses  dados  que  podem  com  frequência  serem utilizados mesmo permanecendo anônimos... assim como poderiam não mais sê-lo  na  medida  em  que  seu  anonimato  não  teria  mais  sentido),  não  que  esta  seria  “roubada” dele, o que lhe permitiria se opor, se constituir como sujeito resistente con-tra um tal roubo. Mas em vez disso, assistimos a um enfraquecimento considerável do caráter “deliberado” das divulgações de informação – o mais frequente, informações triviais, anódinas, segmentadas, descontextualizadas -, desses “traços” cuja trajetória e cujos usos subsequentes são, para o “sujeito”, imprevisíveis e incontroláveis, mesmo se  o  desenvolvimento  de  ferramentas  técnicas  que  permitem  aos  “usuários”  de  ser-viços de informática controlar melhor “seus” dados consiste, hoje em dia, em um ob-jeto  de  investimentos  de  pesquisa  consideráveis.  
> (p. 46)

# 2 - Produção aparentemente automática/objetiva/espontânea de "saberes"

> Em  seguida,  do  ponto  de  vista  do  tratamento dessas informações, constatamos que os “saberes” produzidos têm, como principal  característica,  a  de  parecer  emergir  diretamente  da  massa  de  dados,  sem  que  a  hipótese  a  qual  conduz  a  esses  “saberes”  lhes  seja  preexistente:  as  hipóteses  são  elas  mesmas  “geradas”  a  partir  da  dos  dados.
> (p. 46)

# 3 - Ação normativa sobre e pelo ambiente (modula indiretamente o indivíduo)

>  Ao  final,  a  ação  normativa  decorrente do processo estatístico sempre poderá equivaler mais a uma ação sobre e, as-sim, pelo ambiente, e menos a uma ação sobre o indivíduo em si. A ação desse último não é mais influenciada por confrontação direta com uma norma exterior – lei, média, definição de normalidade -, mas suas possibilidades são organizadas no próprio seio de seu ambiente. 
>  (p. 46-7)

# Força/perigo da generalização das práticas estatísticas no caráter autonomo/indiferente acerca do indivíduo

> Por conta desses três aspectos, a força bem como o perigo da generalização das práti-cas  estatísticas  à  qual  nós  assistimos  residiriam  não  em  seu  caráter  individual,  mas,  pelo  contrário,  em  sua  autonomia  ou  mesmo  em  sua  indiferença  para  com  o  indi-víduo.
> (p. 47)

> Nosso problema, para exprimi-lo da forma mais explícita, não é ser privado do que consideraríamos como nos sendo próprio ou ser forçado a ceder informações que atentariam contra a nossa vida privada ou a nossa liberdade, mas decorre fundamen-talmente do fato de que nossa dupla estatística é demasiadamente separada de nós, que não temos “relação” com ela, mesmo que as ações normativas contemporâneas se bastem dessa dupla estatística para serem eficazes.
> (p. 47)

# Perda da noção de projeto (no espaço social) como móvel, verificável, falível

Governamentalidade algorítmica como governo invisível em que a falha é incorporada na modelização de forma que se tem uma homogeneização e uma não necessidade de escolha por parte de indivíduos

> Em contrapartida, o que nos parece menos ultrapassável, de modo a desenhar assim uma  verdadeira  ruptura,  é  a  aparição  de  possibilidades  de  saberes  que  não  pressu-poriam mais a expressão de qualquer hipótese e que determinariam, por isso, o desa-parecimento, ao menos em parte do espaço social, da ideia de projeto16. Dessa forma, não se trata tanto de lamentar a perda da ideia de projeto entendido como aplicável ou verificável, mas sobretudo como móvel, isto é, precisamente como podendo pas-sar por fracassos e fazer, sobre esta base, história, sendo incessantemente retomado e transformado.
> (p. 47)

> Com o governo algorítmico, tenderíamos a con-siderar a vida social como a vida orgânica, mas considerando esta como se as adapta-ções que aí se desenvolvem não surgissem mais de deslocamentos e de perdas, como se  elas  não  pudessem  mais,  a  partir  deste  momento,  produzir  qualquer  crise,  nem  interrupção, nem devessem mais exigir qualquer comparecimento nem provação dos sujeitos, nem as próprias normas.
> (p. 48)

> {16}: Para  este  ponto  desprovido  de  projetos,  a  governamentalidade  algorítmica  apresenta,  talvez,  uma  versão  radical  do  governo  pelo objetivo, no sentido em que Laurent Thévenot (2012) o compreende: “No governo pelo objetivo, a autoridade legítima é certa-mente deslocada e distribuída nas coisas, tornando difícil sua apreensão e seu questionamento uma vez que ela se impõe em nome do realismo e perde sua visibilidade política”
> (p. 47)

# Poder situado no futuro (antecipação e correção de falhas futuras). O sujeito apenas deslizaria no real.

> O  campo  de  ação  deste  “poder”  não  está  situado  no  presente,  mas  no  futuro.  Essa  forma de governo trata essencialmente daquilo que poderia acontecer, das propen-sões mais do que das ações realizadas, à diferença da repressão penal ou das regras da  responsabilidade  civil,  por  exemplo,  que  só  concernem  às  infrações  que  teriam  sido cometidas ou que estariam sendo cometidas (em caso de flagrante) ou aos danos que  teriam  sido  causados.  Mais  ativamente,  o  governo  algorítmico  não  apenas  per-cebe o possível no presente, produzindo uma “realidade aumentada”, uma atualidade dotada  de  uma  “memória  do  futuro”,  mas  também  dá  consistência  ao  sonho  de  um  “acaso”sistematizado:  nosso  real  teria  se  tornado  o  possível,  nossas  normas  querem  antecipar, corretamente e de maneira imanente, o possível, o melhor meio sendo cer-tamente apresentar-nos um possível que nos corresponda e para o qual os sujeitos só precisariam deslizar.
> (p. 48)

# Na governamentalidade algoritmica, some-se o debate público sobre as normas e a possibilidade de desobediência

> A diferença em relação à normatividade jurídico-discursiva deve ser aqui sublinhada: lá onde essa normatividade estava dada, de maneira discursiva e pública, antes de toda ação sobre os comportamentos, os quais estavam, portanto, restringidos  por  ela  (embora  conservassem,  sob  risco  de  sanção,  a  possibilidade  de  não a obedecer), a normatividade estatística é precisamente o que não é nunca dado previamente, e que resiste a toda discursividade, é o que é incessantemente restringi-do pelos próprios comportamentos e que, paradoxalmente, parece tornar impossível toda forma de desobediência
> (p. 48)

# Aparência de objetividade e inofensibilidade

> O resultado é que, se permanecermos numa perspec-tiva  individualista,  liberal,  a  ação  sobre  os  comportamentos,  o  que  nós  chamamos  “governo  algorítmico”,  aparece,  ao  mesmo  tempo,  como  fundamentalmente  inofen-siva e como perfeitamente objetiva, posto que fundada sobre uma realidade anterior a toda manifestação de entendimento ou de vontade subjetivos, individuais ou coleti-vos, uma realidade que, paradoxalmente, parece tanto mais confiável e objetiva quan-to  mais  ela  provoca  a  abstração  de  nosso  entendimento,  mas  alimentando  o  sonho  de um governo perfeitamente democrático. 
> (p. 48)

# GA como ideal de desaparição do projeto de governar o real (mas governar a partir do real)

> Se era necessário re-situar esse movimento em uma perspectiva ampla, e resistindo desta vez à perspectiva da pura novidade (a qual só teria sentido em relação ao mo-
> delo jurídico-discursivo), devemos constatar que esse governo algorítmico aprofunda ainda  o  ideal  liberal  de  uma  aparente  desaparição  do  próprio  projeto  de  governar:  como mostramos em outros trabalhos (BERNS, 2009), não se trata mais de governar o real, mas de governar a partir do real. A evolução tecnológico-política aqui descrita leva a termo essa tendência19, ao ponto de que não (querer) ser governado poderia, a partir de agora, equivaler a não querer a si mesmo (e isso sem significar, no entanto, que nossa intimidade teria sido violada).
> (p. 48-9)

# Governamentalidade algorítmica como prática preemptiva (x preventiva) que cria desejos de consumo (constrói a realidade)

> A inofensividade, a “passividade” do governo algorítmico é apenas aparente: o governo algorítmico “cria” uma realidade ao menos tanto quanto ele a registra. Ele suscita “necessidades” ou desejos de consumo, mas, desta maneira, despolitiza  os  critérios  de  acesso  a  certos  lugares,  bens  ou  serviços;  ele  desvaloriza  a  política  (uma  vez  que  não  haveria  mais  nada  a  decidir,  a  resolver  em  situações  de  incerteza, posto que estas são antecipadamente desarmadas); o governo algorítmico dispensa as instituições, o debate público; ele substitui a prevenção (em proveito ape-nas da preempção), etc.
> (p. 48)

# Governo das (sobre as) relações

>  Nossa hipótese é de que o objeto – que não chega, portanto, a tornar-se  sujeito  –  do  governo  algorítmico  são  precisamente  as  relações:  os  dados  transmitidos  são  relações20  e  apenas  subsistem  enquanto  relações;  os  conhecimen-tos  gerados  são  relações  de  relações;  e  as  ações  normativas  que  daí  decorrem  são  ações sobre as relações (ou ambientes) referidas às relações de relações. É, portanto, na medida em que seria, na própria realidade de suas práticas visando à organização do possível, um governo das relações, que nós queremos agora tentar circunscrever a eventual novidade deste governo algorítmico.
>  (p. 49)

> {19}: Pelas mesmas razões de outras práticas do governo contemporâneo, como a relação ou a avaliação. Ver Berns (2011, 2012).20 A palavra “relação”, entendida aqui em seu sentido mais bruto e menos comum, pelo qual nós qualificamos o dado, serve-nos apenas para atestar uma operação que liga A e B, sendo capaz de ignorar o que está por trás dos termos assim ligados. Conforme mostraremos, toda a força do governo algorítmico reside, afinal, em sua capacidade de “monadologizar” essa relação, de tal modo que essa relação não consiga precisamente apreender o devir que seria próprio à relacionalidade.
> (p. 49)

> A incitação a abordar a governamentalidade algorítmica sob a perspectiva simondoniana provém do fato de que esse modo de governo parece não ter mais, por apoio e  por  alvo,  os  sujeitos,  mas  as  relações  enquanto  sendo  anteriores  aos  seus  termos,  isto  é,  não  apenas  as  relações  sociais,  intersubjetivas  na  medida  em  que  constroem  os  indivíduos,  de  modo  que  todo  indivíduo  seria  considerado  como  a  soma  dessas  relações,  mas  sobretudo  as    relações  elas  mesmas,  independentemente  de  toda  in-dividuação  simples  e  linear:  as  relações  enquanto  permanecem  inatribuíveis  aos  in-divíduos  que  elas  vinculam,  no  sentido,  assim,  de  que  a  “relacionalidade”  subsistiria  também para além dos indivíduos que as relações ligam. 
> (p. 49-50)

# Aparente compatibilidade entre os conceitos de transindividual e rizoma com a governamentalidade algoritmica

> {22}: O leitor entenderá que o alvo de nossa crítica não é a teoria simondoniana da individuação transindividual nem a perspectiva rizomática  deleuziana-guattariana,  que  a  governamentalidade  algorítmica  só  incorpora  em  aparência.  O  alvo  de  nossa  crítica  é,  justamente, a aparência de compatibilidade da governamentalidade algorítmica com essas teorias e perspectivas emancipatórias, mesmo  quando  a  governamentalidade  algorítmica  tendesse  mais  a  impedir  tanto  os  processos  de  individuação  transindividual  quanto a abertura às novas significações trazidas pelas relações entre entidades “díspares”.
> (p. 51)

# Consequências da metafísica das relações de Simondon: há devir quando há incompatibilidade entre termos, quando há um sistema em individuação (não dois termos já individuados)

> as isso significa então que, por um lado, a relação (a qual ranqueia o ser, excede ou transborda sempre aquilo que ela liga) não se reduz jamais a uma socialidade interindividual e que se ten-ta pensá-la o mais longe quanto possível em sua primazia ontológica: “a relação não brota entre dois termos que já seriam indivíduos”, mas ela é “a ressonância interna de um sistema de individuação” (Simondon, 2005, p.2925). Por outro lado, isso significa que o campo pré-individual – no qual os processos de individuação devem estar inscritospara serem pensados como processos que se desenvolvem conservando sempre esta dimensão pré-individual anterior aos seus movimentos de diferenciação – concebe-se como potencialmente metaestável, isto é, deve-se pensar seu equilíbrio como po-dendo  ser  rompido  por  uma  modificação  interna  ao  sistema,  mesmo  mínima.  Esta  não-estabilidade do campo pré-individual é inerente à possibilidade de uma forma-ção  por  diferenciação;  ela  é,  assim,  a  própria  condição  de  um  pensamento  que  não  cai no paralogismo, o qual consiste em pressupor e mesmo em já individuar sempre o princípio daquilo cuja causa o pensamento procura. Em outras palavras, se há devir, é apenas na medida em que há incompatibilidades entre ordens de grandezas, entre realidades dissimétricas.
> (p. 51)

# O indivíduo como relação em Simondon afirma o devir como realidade, não o indivíduo como relativo a uma medida

> Destas  operações  ou  processos  emanam  indivíduos  e  meios,  indivíduos  associa-dos  a  meios  (o  indivíduo  sendo  a  “realidade  de  uma  relação  metaestável”)  que  são  reais  e  tão  reais  uns  quanto  os  outros.  O  indivíduo  como  relação,  como  rela-tivo  a  um  meio,  é  real,  quer  dizer,  o  relativo  é  real,  ele  é  o  próprio  real.  A  relação,  e  o  indivíduo  como  relações,  não  estão,  portanto,  de  modo  algum,  em  uma  per-spectiva  que  poderíamos  qualificar  de  subjetivista,  a  expressão  de  uma  medida  à  qual  eles  seriam,  por  isso,  relativos  a  ponto  de  perder  sua  realidade:  eles  são  a  re-alidade do devir. Pela mesma razão, o meio associado a um indivíduo é tudo exceto
> sua  redução  à  medida,  isto  é,  à  probabilidade  de  aparecimento  do  indivíduo26.
> (p. 51-2)

# Governamentalidade algoritmica reduz o indivíduo à medida (enquanto continua a afirmar uma fixidez do sujeito)

> Paradoxalmente, ao probabilizar a totalidade da realidade (que parecer tornar-se, enquanto tal, o suporte da ação estatística) e ao parecer dessubjetivar essa perspectiva probabilística (a qual não mais se preocupa com hipóteses prévias), enfim, ao conferir-se  a  possibilidade  de  governar  os  comportamentos  sem  se  ocupar  diretamente  com os indivíduos para se contentar em governar a partir de uma expressão estatística da realidade, que conseguiria substituir a realidade (a perspectiva de um comportamentalismo digital), o governo algorítmico continua a atribuir valores absolutos ao indivíduo  (mesmo  que  ele  seja  abordado  “indiretamente”,  como  aquilo  que  as  relações  permitem  evitar)  e,  ao  mesmo  tempo,  o  “desrealiza”  no  sentido  em  que  o  indivíduo não é nada mais que algo relativo às séries de medidas, as quais, elas mesmas, servem de realidade e semque por isso, no entanto, apareça o caráter subjetivo dessas medidas. As relações sobre as quais se desdobra o governo algorítmico são medidas que, por sua própria capacidade de aparecer com a expressão não mediada e não subjetiva da realidade, isto é, por sua aparente objetividade, tornam ainda mais relativo – e menos real – tudo que advém em função delas e mesmo por elas: o que resulta é apenas relativo a uma série de medida que substitui a realidade. 
> (p. 52)

# Governanentalidade algoritmica como essencialização (ou estadificação, monodologisação) das relações. O devir enquanto movimento é capturado como coisa.

> Em outras palavras, as  relações  e  suas  medidas,  por  sua  capacidade  de  aparecer  como  desconectadas  de toda subjetividade, tornam tanto o real quanto o próprio indivíduo relativos. Mas isto,  considerado  à  luz  do  pensamento  simondoniano,  surge  como  o  fruto  de  uma  inversão: se anteriormente, segundo a metafísica da substância e do indivíduo, toda apreensão ou medida do meio de um indivíduo apareciam sempre como insuficien-tes, posto que demasiadamente subjetivas, impedindo, desta maneira, de alcançar a realidade  do  indivíduo  em  sua  individuação,  essa  insuficiência  (em  conjunto  com  a  diferença ontológica que ela revelava entre o indivíduo e seu meio) seria, doravante, resolvida  tornando  o  próprio  indivíduo  inteiramente  relativo  às  medidas  considera-das,  elas  mesmas,  como  emancipadas  de  toda  subjetividade,  ainda  que  elas  sejam  apenas medidas. Poderíamos até chegar a dizer, aproveitando sempre este confronto entre uma prática de governo e o pensamento simondoniano, que esta prática, con-centrando-se nas relações, consegue “monadologisá-las”, transformá-las em estados, mesmo em “status”, como se as relações fossem, elas mesmas, indivíduos, isto é, sua condiçãoperde  o  que  se  tratava  de  pensar  com  Simondon,  a  saber,  o  devir  em  ação  numa realidade metaestável.
> (p. 52)

# Devir-mônada da relação no big data (inibe a apreensão de uma realidade metaestável)

> É esse devir-mônada da relação que nós constatávamos ao considerar que os dados do _big data_ subsistem apenas como séries de relações, que os saberes gerados com base nisso consistem em religar relações e que as ações normativas que daí decorrem (agindo  sobre  as  relações  depois  de  tê-las  referido  a  relações  de  relações)  excluem,  precisamente,  a  possibilidade  de  uma  realidade  metaestável  no  seio  da  qual  se  inscreveria um devir.
> (p. 52-3)

# Governamentalidade algoritmica, ao evitar a falha, inibe a característica de disparação, encerrando o "real" estatístico em si mesmo

> “O que define essencialmente um sistema metaestável é a existência de uma ‘dispa-ração’, ao menos de duas ordens de grandeza, de duas escalas de realidade díspares, entre  as  quais  não  há  ainda  comunicação  interativa”,  escreve  Deleuze  (2002),  leitor  de  Simondon.  Ora,  essa  evitação  da  falha  ou  do  desvio  opera  como  negação  desta  “disparação”.  A  governamentalidade  algorítmica  apresenta  uma  forma  de  totaliza-ção, de encerramento do “real” estatístico sobre si mesmo, de redução da potência ao provável,  de  indistinção  entre  os  planos  de  imanência  (ou  de  consistência)  e  de  or-ganização (ou de transcendência), e constitui a representação digital do globo imu-nitário, de uma atualidade pura (Lagrandé, 2011), expurgada, de modo preemptivo, de toda forma de potência de porvir, de toda dimensão “outra”, de toda virtualidade (Rouvroy,  2011). 
> (p. 53)

# Impedimento da falha (pela preempção) impede a crise e, por isso, o político

>  Esse  “impedimento  da  falha”  da  modelização  digital  dos  possíveis  – pela preempção dos possíveis ou pelo registro e inscrição automática de toda “ir-regularidade”  nos  processos  de  refinamento  dos  “modelos”,  “padrões”  ou  perfis  (no  caso dos sistemas algorítmicos autodidatas) – retira do que poderia surgir do mundo em sua dissimetria relativa à realidade (aqui, o que lhe substitui é o corpo estatístico) sua potência de interrupção, de colocar em crise.
>  (p. 53)

> {27}: Novamente, é necessário apontar, aqui, o fato de que a crise, esse momento que convoca a decidir na incerteza, é precisamente o momento do político: “A autoridade legítima foi deslocada e distribuída nas coisas, dificultando sua apreensão e seu question-amento, uma vez que ela se impõe em nome do realismo e perde sua visibilidade política. A crítica se encontra paralisada porque ela parece ultrapassada e tornada caduca. A referência à objetividade, frequentemente acompanhada da invocação da transparên-cia da informação, não retoma uma exigência maior da deliberação democrática? ” (Thévenot, 2012). 
> (p. 53)

# Aparente similaridade entre governamentalidade algoritmica e o rizoma. Noção descritiva do rizoma x noção estratégico-política em D&G.

> Dando-se por espaço uma topologia horizontal de pura  superfície,  dispensando  toda  profundidade,  toda  verticalidade,  toda  estrutura  hierarquizada,  todo  projeto  e  toda  projeção,  a  governamentalidade  algorítmica,  assim  como  a  estratégia  rizomática,  não  se  interessa  nem  pelo  _sujeito_,  nem  pelos  _indivíduos_. Somente contam as relações entre os dados, que são apenas fragmentos infraindividuais,  reflexos  parciais  e  impessoais  de  existências  cotidianas  que  o  dat-amining permite correlacionar a um nível supraindividual, mas que não define qual-quer  ultrapassagem  do  indivíduo,  qualquer  povo,  portanto.  Na  era  dos  _Big  Data_  e  da governamentalidade algorítmica, a metáfora do rizoma parece ter adquirido _um estatuto propriamente descritivo ou diagnóstico_: nós somos hoje confrontados com a atualização “material”, poderia se dizer, do rizoma. O metabolismo do “corpo estatísti-co” – pelo qual se interessa a governamentalidade algorítmica, esse corpo estatístico incomensurável pelos corpos vivos, socialmente e fisicamente experimentados, con-sistentes, para além da simples aglomeração de elementos, de uma consistência que significa, ao mesmo tempo, que este corpo permaneça unido e que ele seja suscetível ao acontecimento (Rouvroy& Berns, 2009, 2010) – lembra singularmente as caracte-rísticas ou princípios rizomáticos enunciados por Gilles Deleuze e Félix Guattari. 
> (p. 53-4)

# Noção de relação sem alteridade na GA

> Em primeiro lugar, o que acontece com uma relacionalidade que não seria mais “fisica-mente habitada” por nenhuma alteridade? Na governamentalidade algorítmica, _cara sujeito é, ele mesmo, uma multidão, mas ele é múltiplo sem alteridade_, fragmentado em  quantidades  de  perfis  que  se  relacionam,  todos,  a  “ele  mesmo”,  às  suas  propen-sões,  aos  seus  desejos  presumidos,  suas  oportunidades  e  seus  riscos.  Uma  relação  –  fosse  ela  uma  cena  esvaziada  de  sujeitos  –  não  deve  sempre  ser  “povoada”,  fosse  por um “povo que falta” [evocado por Deleuze (1987, 1990)], um povo em projeto? A “relação” não implica, no mínimo, uma coletividade de mais de um, naquilo em que ela é a condição de uma dissimetria?
> (p. 54)

# O desejo nos precede na GA

Acredito que menos relevante seja a precedência do desejo em relação ao sujeito, mas sim da onde vem, no caso o desejo que circula nesses circuitos tem a ver com vigilância, controle, lucro, estereotipia (um desejo de manutenção acima de tudo).

>Em  segundo  lugar,  o  que  acontece  com  o  caráter  emancipador  de  uma  perspectiva  transindividual  ou  rizomática  _quando  os  desejos  que  aí  se  movem  nos  precedem_? Essa primazia cronológica da oferta personalizada em função de propensões não ex-pressas  pelo  sujeito  não  viria  sempre  já  determinar  e  estabilizar  os  processos  de  in-dividuação desde o estado pré-individual? Esses novos usos da estatística que são o _datamining_ e  a  elaboração  de  perfis  não  nos  reduzem  à  impotência  diante  das  nor-mas imanentes/produzidas da governamentalidade algorítmica?
> (p. 54)

# Relação não tem devir específico na GA

A relação não parece nem ter a ver com devir na GA (?), se pensarmos que são fragmentos individuais e sem um verdadeiro movimento. Pode existir mobimento no perfil, mas as relações monodologisadas em dados já foram capturados de qualquer devir.

> Em  terceiro  lugar,  o  que  acontece  com  o  caráter  emancipador  de  uma  perspectiva  transindividual ou rizomática _quando a relação não é mais trazida por nenhum devir específico_ (devir-sujeito,  devir-povo,  etc.),  _isto  é,  quando  ele  não  pode  relatar  mais  nada_,  uma  vez  que,  precisamente,  o  alvo,  no  sentido  do  que  essa  nova  maneira  de  governar por algoritmos insiste em excluir, é “o que poderia advir” e que não se teria previsto posto que fruto de disparações, isto é, a parte de incerteza, de virtualidade, de potencialidade radical que faz dos seres humanos processos livres para se projetar, relatar-se, tornar-se sujeitos, individuar-se, seguindo trajetórias relativamente e relacionalmente  abertas?  Poderia  se  dizer  que,  sim,  a  perspectiva  é  “emancipadora”  no  sentido de que ela faz tábula rasa das antigas hierarquias (no sentido mais amplo...o “homem  normal”  ou  o  “homem  médio”  que  ocupa  justamente  um  lugar  nessa  hierarquia), mas ela não é emancipadora no escopo de qualquer devir, qualquer projeto, qualquer objetivo. Há, portanto, realmente uma forma de “liberação”, mas que não é a liberdade no sentido “forte”. O regime de verdade digital (ou o comportamentalismo digital)  não  ameaça,  hoje,  solapar  as  próprias  bases  da  emancipação,  esvaziando  as  noções de crítica e de projeto (Rouvroy, 2013), e mesmo de comum?
> (p. 54)

# O comum pressupõe heterogeneidade. Ausência do comum na GA

> Sem chegar ainda a resolver essas questões, tratava-se, para nós, de mostrar que, mais do que retornar a aproximações personológicas (cujo individualismo possessivo dos regimes  jurídicos  de  proteção  de  dados  é  extremamente  exemplar),  que  seriam  tão  ineficazes quanto mal fundados, a aposta fundamental – o que restaria a salvar como recurso  antecedente  a  todo  “sujeito”,  a  toda  individuação  e  como  constitutivo  desta 
> última  –  é  “o  comum”,  entendido  aqui  como  esse  “entre”,  esse  lugar  de  presença  no  qual  os  seres  se  dirigem  e  se  relatam  uns  aos  outros  em  todas  as  suas  dissimetrias,  suas “disparações”. Nós queríamos mostrar também que a existência desse “comum” é, portanto, tributária não de uma homogeneização, de um encerramento do real sobre ele  mesmo,  mas,  pelo  contrário,  de  uma  heterogeneidade  das  ordens  de  grandeza,  de uma multiplicidade dos regimes de existência, enfim, de escalas de realidade dís-pares. Dito de outro modo, o comum necessita e pressupõe a não-coincidência, pois é a partir desta que os processos de individuação ocorrem, no momento em que ela nos obriga a nos dirigir uns aos outros. Inversamente, o governo das relações, repousando sobre o esvaziamento de toda forma de disparidade, “monadologisa” as relações de tal modo que estas não relatam mais nada e não expressam mais nenhum comum.
> (p. 54-5)
