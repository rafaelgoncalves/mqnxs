---
title: Anarquismo e infância
category: fichamento
path: "/anarquismo-infancia"
author: Rafael Gonçalves
featuredImage: "../../images/Banksy_03.jpg"
srcInfo: Retirado de <a href="https://www.banksy.co.uk/in.asp">Banksy</a>
date: 2020-12-30
tags: [Emma Goldman, Elisée Reclus, anarquismo, infância, educação, fichamento]
published: true
---


Trechos de dois textos de autoraes anarquistas sobre a infância.

## O futuro de nossos filhos - Elisée Reclus [^1]

> Pouco importa que a criança compreenda ou não; é necessário que decore um formulário qualquer escrito de antemão.

> Depois do absurdo alfabeto que lhe faz pronunciar as palavras de maneira diferente do modo como as escreve, e que acostuma previamente a todas as tolices que lhe são ensinadas, veem as regras gramaticais que recita de memória, em seguida as bárbaras nomenclaturas a que dão o nome de geografia, e ainda por cima o relato de crimes reais, conhecidos com o nome de história

> Se se deseja educar uma geração livre é mister começar por destruir as prisões chamadas colégios e liceus!

> Tenhamos a firme resolução de fazer deles homens livres - nós, que ainda não temos da liberdade senão uma vaga esperança.

## A criança e seus inimigos - Emma Goldman [^2]

> Todas as instituições hoje, a família, o Estado, os códigos morais veem em uma personalidade forte, bonita e intransigente um inimigo mortal; portanto, dedicam-se a limitar as emoções humanas e a originalidade do pensamento em uma camisa de força desde a primeira infância; o a moldar todo ser humano de acordo com um padrão; não como uma individualidade completa, mas como um escravo paciente, um autômato profissional, um cidadão pagador de impostos ou um moralista honesto
> (p. 83)

> [As perguntas das crianças] são respondidas de forma limitada, convencional e ridícula, baseado quase sempre em falsidades; e quando deseja contemplar as maravilhas do mundo, com olhos arregalados, imaginários e inocentes, aqueles ao seu redor rapdamente fecham as portas e janelas, mantendo a delicada planta humana em uma atmosfera de estufa, onde não pode respirar nem crescer livremente.
> (p. 84)

> O ideal do pedagogo mediano não é criar um ser original, bem-equilibrado e completo; em vez disso, busca que sua atuação pedagógica resulte em autômatos de carne e osso, para ,elhor se encaixar na esteira da sociedade e no vazio e embrutecimento de nossas vidas.
> (p. 85)

> "Fatos e dados", como são chamados, formam um amontoado de informações, talvez o suficiente para sustentar toda a forma de autoritarismo e criar o respeito necessário à importância da posse de bens, mas na realidade são uma grande desvantagem para a verdadeira compreensão da alma humana e seu lugar no mundo.
> (p. 85)

> As mudanças são essência da vida, variações e a contínua inovação. (p. 85)

> Eles [os pais e as mães] se apegam a ideia de que a criança é uma mera parte de si - uma ideia tão falsa quanto prejudicial, e que apenas aumenta a incompreensão da alma infantil, da suposta necessidade de escravizá-la e subordiná-la.
> (p. 87)

> Logo, confronta a dolorosa realidade de que está aqui apenas para servir como matéria inanimada, para ser moldado por seus pais e responsáveis, cuja autoridade, por si só, lhe dá forma.
> (p. 87)

> Não compreendo como os pais esperam que suas crianças desenvolvam espíGritos confiantes e independentes, quando fazem de tudo para restringi-los de realizar ações que possibilitam a criação das qualidades e caráter que diferenciam seus filhos de si mesmos, ao que atingiram pela virtude de ideias novas e revigorantes das quais são eminentemente portadoras.
> (p. 88)

> Uma árvore jovem e delicada se podada e cortada pelo jardineiro para adquirir uma forma artificial nunca alcançará a beleza e altura majestosa caso fosse permitida crescer naturalmente em liberdade.
> (p. 88)

> Os sentimentos de amor e ternura na jovem planta se tornam vulgares e grosseiros por meio de estupidez dos que a cercam, de modo que tudo que é bom e belo ou é totalmente esmagado ou oculto nos abismos mais profundos, como um grande pecado que não ousa ver a luz.
> (p. 88)

> Pelo contrário, sufocam a bela voz da primavera, da nova vida de beleza e esplendor do amor; colocam o longo e magro dedo da autoridade sobre os lábios macios e não permitem sua abertura para a canção prateada do crescimento individual, da beleza do caráter, da força do amor e da relação humana, que por si só fazem a vida valer a pena.
> (p. 88-89)

> Ainda assim esses pais imaginam estar fazendo o melhor para as suas crianças, e pelo que vejo, alguns realmente o fazem; mas o seu melhor resulta na morte e decadência absoluta do broto em formação. Afinal, estão reproduzindo a mesma relação de poder que de seus mestres políticos, econômicos, sociais e morais, ao suprimir pela força toda tentativa independente de compreender os males sociais e todo esforço sincero para a abolição desses males; não conseguem compreender que qualer método empregado serve para intensificar o deseko pela liberdade e aprofundar o zelo por sua luta.
> (p. 89)

> A compulsão desperta resistência, todos os pais e professores deveriam sabê-los. (p. 89)

> Pais radicais, embora emancipados da crença de propriedade, ainda se apegam tenazmente à noção de que são os donos da criança e que têm o direito de exercer sua autoridade sobre ela.
> (p. 89)

> Mas a impressionante mente infantil percebe cedo que as vidas de seus pais estão em contradição com as ideias que representam; como o bom cristão que reza fervorozamente no domingo, mas continua quebrando os mandamentos do Senhor durante o resto da semanal, o pai radical denuncia a deus, o sacerdócio, a Igreja, o governo, a autoridade doméstica, mas continua agindo conforme a condição que abomina.
> (p. 89-90)

> A criança alimentada com ideias unilaterais, prontas e fixas, logo se cansa de repetir as crenças de seus pais, e se lança em busca de novas sensações, por mais insignificantes e superficial que sejam, a mente humana não pode suportar a mesmice e a monotonia.
> (p. 90)

> Eles [casos de filhos de pais radicais que escolhem vidas conservadoras] são a maior prova de que um espírito independente sempre resistirá a qualquer força externa e alheia exercida sobre os corações e mentes humanas.
> (p. 91)

> Se a educação deve significar qualquer coisa, deve insistir no livre crescimento e desenvolvimento das forças e tendências inatas da criança. Só assim podemos esperar um indivíduo livre e também eventualmente uma comunidade livre, que impossibilite a interferência e a coerção do crescimento humano.
> (p. 91)


[^1]: RECLUS, E. O futuro de nossas crianças. 1894.

[^2]: GOLDMAN, E. A criança e seus inimigos. **Educação**. Biblioteca Terra Livre. 2019.
