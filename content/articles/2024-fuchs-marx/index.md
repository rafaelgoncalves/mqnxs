---
title: "Marx como sociólogo da tecnologia"
date: 2024-03-28
path: "/marx-soc-tecnologia"
category: fichamento
author: Rafael Gonçalves
featuredImage: "./tear.jpg"
srcInfo: <a href="https://commons.wikimedia.org/wiki/File:Powerloom_weaving_in_1835.jpg">Ilustração de tecelagem em tear mecânico</a>
# featuredImage: "./fuchs.jpg"
# srcInfo: <a href="https://commons.wikimedia.org/wiki/File:Christian_Fuchs_(Gesellschaftswissenschaftler).jpg">Christian Fuchs</a>
tags:
    - sociologia da tecnologia
    - Karl Marx
    - Christian Fuchs
    - maquinaria

published: true
---

Fichamento do artigo _Rereading Marx as critical sociologist of technology_[^1] de Christian Fuchs.

[^1]: FUCHS, Christian. Reareading Marx as critical sociologist of technology. _In_: **Rereading Marx in the age of digital capitalism**. London. Pluto Press. 2019. pp. 30-53.

# Objetivo do texto: propor uma releitura do conceito de maquinaria e do papel da tecnologia no capitalismo

> The task of this chapter is to reread Marx’s concept and critical sociology of technology by tracing the development of his notion of machinery and the role of technology in capitalism in his works from the 1840s up until the publication of Capital Volume 1 in 1867. 
> (p. 30)

# Visão dialética da tecnologia em Marx. Evita determinismo tecnológico e construção social da tecnologia. Não é tecnofílico nem tecnofóbico. Análise da tecnologia em uma dialética da exploração e da liberação.

> Karl Marx saw technology as an important feature of capitalist society. His analysis of technology, or what he also refers to as means of production, fixed constant capital and machinery, is dialectical in several
> respects:
> 
> 1. By analysing technology based on a dialectic of technology and society, Marx avoids both technological determinism and social construction of technology.
> 2. Marx is neither a techno-optimist nor a techno-pessimist, but stresses that technology in an antagonistic society has antagonistic effects: There is in most cases not just one impact of technology on society, but several ones that contradict each other.
> 3. Marx analyses technology based on a dialectic of exploitation and liberation: In capitalism, technology is a means of relative surplus-value production and control. At the same time, it advances the contradiction between the productive forces and the relations of production so that germ forms of a commons-based society emerge that cannot be realised within capitalism and within private property relations form one of the factors contributing to economic crises. As a consequence, liberation from capital requires both the fundamental transformation of society and the re-design of technology.
> (p. 30)

# Capítulo 15 do Capital: Maquinaria e grande indústria

> ‘Machinery and Large-Scale Industry’ is with 147 printed pages out of a total of 995 pages (in the Penguin edition, including the appendix ‘Results of the Immediate Process of Production’) the longest chapter in Capital Volume I (Marx 1867: 492–639). It comprises 15 per cent of the total length of the first volume of Marx’s main work. The chapter consists of ten sections that focus on the development of machinery (15.1), machinery and value (15.2), machinery’s impacts on labour in capitalism (15.3), the machine and the factory (15.4), machinery and class struggle (15.5), the question labour replaced by machinery can be compensated by new jobs created by the use of new technology (15.6), machinery and the attraction and repulsion of labour (15.7), modern industry’s transformation of earlier forms of labour (15.8), technology and legislation (15.9), and modern industry’s transformation of agriculture (15.10).
> (p. 31)

> Chapter 15 grounds a critical theory of technology by analysing technology’s role in capitalism as contradictory means of production that acts as means of relative surplus-value production and control and ripens the antagonism between the productive forces and the relations of production. The chapter shows that Marx’s theory is not abstract, but grounded in an analysis of empirical reality. Marx quotes from and analyses factory inspectors’ reports in order to ground his theoretical categories in workers’ everyday reality. Furthermore, concreteness is achieved by relating modern technology to working class struggles for the reduction of the working day and aspects of labour legislation. One can learn from chapter 15 that technology’s effects on society are not pre-given, but are subject to class struggles.
> (p. 31)

# Maquinaria como unidade do mecanismo motor, do mecanismo de transmissão e a ferramenta da máquina de trabalho [?]. Meio de baratear a produção e produzir mais-valor.

> Marx starts the analysis in chapter 15 at the abstract level by defining machinery as the unit of the motor mechanism, the transmitting mechanism and the tool or working machine (Marx 1867: 494). But the abstract is at the same time concrete: Marx already in the chapter’s first paragraph of the first section makes clear that machinery’s essence has developed historically together with capitalism so that ‘machinery is intended to cheapen commodities’ and ‘is a means for producing surplus-value’ (Marx 1867: 492). 
> (p. 31-2)

# Dois movimentos na tecnologia: dialetica da produção de capital (+) e dialética da destruição do capitalismo (-)

> He shows that the means of production does not just produce a dialectic of the production of capital and the production of communist potentials, but that this dialectic is mediated through a negative dialectic, in which technology acts as a means of destruction that advances capitalism’s antagonisms, including exploitation, unemployment, precarious labour and crisis potentials. Marx therefore ends chapter 15 by stressing modern technology’s role in the ‘process of destruction’ that results in ‘simultaneously undermining the original sources of all wealth – the soil and the worker’ (Marx 1867: 638).
> (p. 32)

# Fragmento sobre máquinas no Grundrisse

> The Fragment of Machines is a section in the Grundrisse’s sixth and seventh notebooks (Marx 1857/58: 690–714). Given that the Grundrisse was Capital’s first draft, we can consider the Fragment as a draft of Capital Volume I’s chapter 15. Pier Aldo Rovatti’s 1973 article ‘The Critique of Fetishism in Marx’s Grundrisse’ contains the earliest traceable use of the term ‘Fragment of Machines’ in English (Rovatti 1973).
> (p. 32)

# Relação com o movimento autonomista

> But the very term ‘Fragment of Machines’ stems from Renato Solmi’s 1964 translation into Italian that was published under the title ‘Frammento sulle macchine’ in the journal Quaderni Rossi. The journal formed one of the foundations of autonomist Marxism, an interpretation of Marx that gives special attention to class struggle and the role of technology and knowledge. Consequently, the Fragment has played an important role in autonomist Marxism (see, e.g., Hardt & Negri 2000: section 1.2; Negri 1991: chapter 7; Vercellone 2007; Virno 1996).
> (p. 32)

# Intelecto geral (_general intelect_) e economia da informação

> In the Fragment, Marx introduces the notion of the general intellect (Marx 1857/58: 706), by which he anticipated the emergence of an information economy as the result of the development of the productive forces.
> (p. 32)

# Antagonismo entre tempo e mais-valor na máquina. Papel da tecnologia na crise capitalista e na formação de uma sociedade comunista.

> Furthermore, he analyses the capitalist antagonism between necessary labour-time and surplus labour-time. On the one hand, Marx thereby makes clear that machinery is a factor that advances capitalist crises and working class precarity. On the other hand, he in a manner that is clearer than in other works argues that modern technology helps in creating the foundations of communism because its reduction of necessary labour-time through the increase of productivity enables a society, in which the ‘measure of wealth is then not any longer, in any way, labour time, but rather disposable time’ (Marx 1857/58: 708)
> (p. 32)

> Roman Rosdolsky in his analysis of the Grundrisse highlights that Marx in the elaboration of the concept of the general intellect stresses modern technology’s prospects for the working class’ ‘future liberation’ by the ‘radical reduction of working time’, a development that, however, can naturally ‘only be realised in a communist society; but capital – against its will – presses forward in this direction!’ (Rosdolsky 1977: 243).
> (p. 33)

# Aspecto positivo e negativo da tecnologia em ambos os textos

> But under capitalist conditions, technology’s productivity increases backfire and thereby deepen exploitation and advance crisis because capital ‘presses to reduce labour time to a minimum, while it posits labour time, on the other side, as sole measure and source of wealth’ (Marx 1857/58: 366). Modern technology’s communist potentials are certainly worked out much clearer in the Fragment than in Capital Volume I’s chapter 15. So the Fragment goes in a certain respect beyond chapter 15. At the same time, chapter 15 is historically much more concrete and follows technology’s contradictions into the experience of the working class’ everyday struggles. So chapter 15 goes beyond the Fragment just like the Fragment goes beyond chapter 15. 
> (p. 33)

# Redução do humano a estatuto de máquina pelo Capitalismo (nos manuscritos de Paris). Disso, é possível entender máquina como concorrente do humano.

> In the _Economic and Philosophic Manuscripts of 1844_, Marx argues that capitalism reduces humans to the status of machines: Capital depresses the worker ‘spiritually and physically to the condition of a machine’ (MECW 3: 237–8). ‘The machine accommodates itself to the weakness of the human being in order to make the _weak_ human being into a machine’ (MECW 3: 308). Marx here takes a humanist perspective and stresses that capitalism is inhumane and treats workers just like inanimate matter, as things and toil that can be used and abused. Capital denies workers their humanity. However, an aspect that is only briefly mentioned here and there is how the capitalist use of machines shapes working conditions. Marx: ‘Since the worker has sunk to the level of a machine, he can be confronted by the machine as competitor’ (MECW 3: 238).
> (p. 33)

# Redução da mão de obra e aumento da produção de mais-valor com a introdução da máquina em Engels (e Marx)

> In 1844, Engels published a series of three articles under the title ‘The Condition of England’. In the second one, he argues: ‘Since the application of the steam-engine and of metal cylinders in printing, one man does the work of two hundred’ (MECW 3: 482). So Engels here grasps the phenomenon of technological productivity increase. But there is no special theoretical vocabulary for this phenomenon. Later, Marx introduced the notion of machine as fixed constant capital that is a tool of relative surplus-value production. In his 1845 book The Condition of the Working-Class in England, Engels starts the analysis with the observation that the ‘history of the proletariat in England begins with the second half of the last century, with the invention of the steam-engine and of machinery for working cotton’ (MECW 4: 307). Engels also describes the negative consequences of machine use under capitalist conditions: ‘Every improvement in machinery throws workers out of employment’ (MECW 4: 429). In The German Ideology (written in 1845/46), Marx and Engels argue that machinery plays a role in the international division of labour: A machine invented in England deprives ‘countless workers of bread in India and China’ (MECW 5: 51). A more specialised vocabulary for characterising technology’s role in capitalism is still missing in these works.
> (p. 34)

# Noção dialética do papel da tecnologia em Marx (contra Proudhon): ao mesmo tempo existe um potencial de negar a divisão de trabalho, mas no capitalismo, sua introdução aumenta a divisão de trabalho.

> As a consequence, the division of labour is for Proudhon an eternal feature of society that in capitalism has a good and a bad side, whereas for Marx the division of labour is an expression of class relations that need to be politically sublated through class struggles in order to humanise society. For Proudhon, ‘the concentration of the instruments of labour is the negation of the division of labour’ (MECW 6: 187). So Proudhon stresses only the one side of machines, whereas Marx in The Poverty of Philosophy stresses the dialectical character of modern machines. They simultaneously have a repressive reality and emancipatory potentials. The dialectical character of modern machines has to do with the fact that in capitalism they are embedded into the class relationship between capital and labour: ‘The modern workshop, which is based on the application of machinery, is a social production relation, an economic category’ (MECW 6: 183). ‘In short, with the introduction of machinery the division of labour inside society has increased, the task of the worker inside the workshop has been simplified, capital has been concentrated, the human being has been further dismembered’ (MECW 6: 188). The dialectical character of technology is a key feature of Marx’s analysis of technology. ‘Discovered’ in his early philosophical works, he applied this principle in his later works to the historical and theoretical analysis of technology.
> (p. 34-5)

# Uso da máquina no capitalismo torna o trabalhador em apêndice da máquina (Manifesto)

> In the Manifesto of the Communist Party, published in February 1848, Marx and Engels stress how the capitalist application of machinery has radically transformed the production process so that large-scale industry emerged. ‘Owing to the extensive use of machinery and to division of labour, the work of the proletarians has lost all individual character, and, consequently, all charm for the workman. He becomes an appendage of the machine’ (MECW 6: 490–1). Marx and Engels also stress the embeddedness of the capitalist application of technology into the antagonism of productive forces and relations of production – ‘the revolt of modern productive forces against modern conditions of production’ (MECW 6: 489) that creates capitalist crises. Already in The German Ideology, Marx and Engels spoke of the development that ‘an earlier form of intercourse, which has become a fetter, is replaced by a new one corresponding to the more developed productive forces and, hence, to the advanced mode of the self-activity of individuals – a form which in its turn becomes a fetter and is then replaced by another’ (MECW 5: 82). The term ‘forms of intercourse’ (_Verkehrsform_) was later replaced by the category of the relations of production (_Produktionsverhältnisse_).
> (p. 35)

# Dialética das novas tecnologias de comunicação e da globalização da produção e circulação

> Marx and Engels also stress that ‘modern industry’ creates ‘improved means of communication’ that ‘place the workers of different localities in contact with one another (MECW 6: 493). There is a dialectic of modern communication technologies and the globalisation of production and circulation. Communication technologies shape and are shaped by transformations of society’s space-time relations. Ten years later, Marx pinpointed this insight in the _Grundrisse_ in the following way: ‘Capital by its nature drives beyond every spatial barrier. Thus the creation of the physical conditions of exchange – of the means of communication and transport – the annihilation of space by time – becomes an extraordinary necessity for it’ (Marx 1857/58: 524).
> (p. 35-6)

# Crítica da teoria da compensação: trabalhadores especializados substituídos por máquinas (e trabalhadores não especializados) não conseguem emprego em outras áreas necessariamente.

> In 1849, Marx published the pamphlet _Wage Labour and Capital_. Continuing his earlier established analysis, he argues in this work that the capitalist use of machinery throws ‘the hand workers onto the streets in masses’ and results in the replacement of ‘skilled workers by unskilled’ ones (MECW 9: 226). For the first time, Marx in this essay critically questions the compensation theory of labour that says that ‘the workers rendered superfluous by machinery find _new_ branches of employment’ (MECW 9: 226).
> (p. 36)

# A análise dialética da tecnologia feita por Marx e Engels em 1840s não deixa explícito o papel da tecnologia no comunismo (apesar de podermos supor seu papel quando se diz em 'aumento da produtividade')

> So by the end of the 1840s, Marx and Engels had established a dialectical analysis of technology that stressed the antagonistic and class character of machinery’s use in capitalism. They point out that capitalist technology is a means of increasing productivity and a means of control, rationalisation, globalisation and exploitation and is embedded into the antagonism between productive forces and relations of production. In the 1840s, Marx made clear that communism requires the abolition of private property and the division of labour, but he did not establish a clear understanding of the role of technology as one of the foundations of communism. Such a detailed analysis had to wait until Marx’s work on the _Grundrisse_ in the late 1850s.
> (p. 36)

# Trabalho necessário e 'mais-trabalho' (?) nos Grundrisse

> In the Grundrisse, Marx in 1857/58 introduced the notion of surplus-value and along with it the concepts of necessary labour-time and surplus labour-time:
> 
> > The increase in the productive force of living labour increases the _value_ of capital (or diminishes the value of the worker) not because it increases the quantity of products or use values created by the same labour – the productive force of labour is its natural force – but rather because it diminishes _necessary_ labour, hence, in the same relation as it diminishes the former, it creates _surplus labour_ or, what amounts to the same thing, surplus value; because the surplus value which capital obtains through the production process consists only of the excess of surplus labour over _necessary labour_. The increase in productive force can increase surplus labour – i.e. the excess of labour objectified in capital as product over the labour objectified in the exchange value of the working day – only to the extent that it diminishes the relation of _necessary labour to surplus labour_, and only in the proportion in which it diminishes this relation. Surplus value is exactly equal to surplus labour; the increase of the one [is] exactly measured by the diminution of _necessary labour_. (Marx 1857/58: 339)
> 
> Marx here describes a close relationship of surplus-value and technology in capitalism: Capital strives to increase unpaid labour-time in order to maximise profit. An important means for doing so is to increase productivity by making use of labour-saving technologies. As a consequence, the proportion of unpaid labour-time (surplus labour-time) increases and the proportion of the paid labour-time (necessary labour-time) decreases. The introduction of the category of surplus-value necessitates the distinction between necessary labour-time and surplus labour-time
> (p. 38-9)

# Capital constante e variável nos Grundrisse. Tecnologia como capital constante.

> In the _Grundrisse_, Marx also introduces the distinction between constant and variable capital. He speaks of the ‘division of capital into a constant part – raw material and instrument with an antediluvian existence before labour – and a variable part, that is, the necessary goods exchangeable for living labour capacity’ (Marx 1857/58: 454). The value of constant capital does not increase, it is ‘invariable value’ (Marx 1857/58: 379) that is used up and transferred to the commodity in the capitalist production process. Human labour-power in contrast is put to work in the production process as labour that creates novel goods, new commodities and thereby new value. It therefore has a dynamic, variable character.
> (p. 39)

# Capital fixo e circulante nos Grundrisse. Tecnologia como capital fixo.

> Furthermore, Marx introduces the distinction between fixed and circulating capital in the _Grundrisse_. Machinery is fixed capital because it is fixed in the production process for a longer time. It is not used up in the production of one commodity, but is used repeatedly as means for the production of many commodities over a longer time period. ‘[F]ixed capital, once it has entered the production process, remains in it’ (Marx 1857/58: 680). ‘“Fixed capital” serves over and over again for the same, operation’ (Marx 1857/58: 717). ‘Fixed capital in its developed form hence only returns in a cycle of years which embraces a series of turnovers of circulating capital’ (Marx 1857/58: 721). 
> (p. 39)

# Mais-valor relativo e mais-valor absoluto nos Grundrisse. Invenção tecnológica como produtora de mais-valor relativo.

> Whereas absolute surplus-value production has to do with primitive accumulation, the formal creation of wage-labour as legal relation and the creation of new realms of wage-labour (Marx 1857/58: 769–70), relative surplus-value production implies the transformation of the production process by new forms of technology and organisation. So in the _Grundrisse_, Marx establishes for the first time a clear relationship between the capitalist use of technology and the category of relative surplus-value production.
> (p. 40)

# Alienação, intelecto geral e a formação de uma economia do conhecimento no capitalismo

> In the _Grundrisse_, Marx argues like in earlier works that capitalist technology alienates the worker. He, for example, writes that the ‘_automatic system of machinery_’ and the capitalist use of science ‘acts upon’ the worker ‘as an alien power’ (Marx 1857/58: 692–3). A novel theoretical element is that Marx introduces the notion of the general intellect in the _Grundrisse_, by which he anticipated the emergence of an information economy:
> 
> > The development of fixed capital indicates to what degree general social knowledge has become a _direct force of production_, and to what degree, then, the conditions of the process of social life itself have come under the control of the general intellect and been transformed in accordance with it. (Marx 1857/58: 706)
> 
> The basic argument is that capitalism requires the development of new technologies for increasing productivity. By doing so, it also introduces an increasing level of science and knowledge labour into the production process so that at a certain point of time the increase in quantity of science, knowledge and technology in production turns into a new quality – knowledge becomes a ‘direct force of production’, the knowledge economy emerges.
> (p. 40)

# Desenvolvimento tecnológico como condição para o comunismo

> Marx expresses nowhere clearer than in the _Grundrisse_ that communism requires highly productive technologies in order to abolish wage-labour and enable a society that is built around freely determined activities beyond compulsion and necessity. One of the manifold passages in the _Grundrisse_, where he expresses this fact, is the following one:
> 
> Real economy – saving – consists of the saving of labour time (minimum (and minimization) of production costs); but this saving identical with development of the productive force. … The saving of labour time [is] equal to an increase of free time, i.e. time for the full development of the individual, which in turn reacts back upon the productive power of labour as itself the greatest productive power. (Marx 1857/58: 711)
> (p. 40-1)

# Subsunção formal e real nos manuscritos economicos de 61-63. Subsunção real como transformação do processo de produção pela ciência e tecnologia.

>  In the _Economic Manuscripts of 1861–63_, Marx introduces the concepts of the formal and real subsumption of capital under labour:
>  
>  > Historically, in fact, at the start of its formation, we see capital take under its control (subsume under itself) not only the labour process in general but the specific actual labour processes as it finds them available in the existing technology, and in the form in which they have developed on the basis of non-capitalist relations of production. It finds in existence the actual production process – the particular mode of production – and at the beginning it only subsumes it _formally_, without making any changes in its specific technological character. Only in the course of its development does capital not only formally subsume the labour process but transform it, give the very mode of production a new shape and thus first create the mode of production peculiar to it. … This _formal_ subsumption of the labour process, the assumption of control over it by capital, consists in the worker’s subjection as worker to the supervision and therefore to the command of capital or the capitalist. Capital becomes command over labour. (MECW 30: 92–3)
>  
>  (p. 42)

> Formal subsumption means that wage-labour relations are imposed on particular forms of labour without transforming the mode of production. Real subsumption in contrast means a qualitative change of the mode of production so that more radical organisational and technological changes take place. Marx speaks of formal and real subsumption as ‘_two separate forms of capitalist production_’ (MECW 34: 95). Formal and real subsumption for Marx correspond to forms of capitalist production that are based on absolute and relative surplus-value production: ‘I call the form which rests on absolute surplus value the _formal subsumption of labour under capital_. … The real subsumption of labour under capital is developed in all the forms which produce relative, as opposed to absolute, surplus value’ (MECW 34: 95, 105). 
> (p. 43)

> In real subsumption, science and technology transform the production process qualitatively:
> 
> With the real subsumption of labour under capital, all the CHANGES we have discussed take place in the technological process, the labour process, and at the same time there are changes in the relation of the worker to his own production and to capital – and finally, the development of the productive power of labour takes place, in that the productive forces of social labour are developed, and only at that point does the application of natural forces on a large scale, of science and of machinery, to direct production become possible. (MECW 34: 106)
> (p. 43)

# Cooperação, divisão de trabalho na manufatura e maquinaria como 3 formas de produção de mais-valor relativo nos manuscritos 61-63

> Co-operation has for Marx a general and a specific meaning. In general, it refers to ‘_the collective labour of many workers_’ (MECW 30: 255). Its more specific meaning is the ‘_agglomeration, heaping up of many workers in the same area_ (in one place), all working _at the same time_’ (MECW 30: 256). An example is that workers, who individually assemble toys from their homes, are amassed in a toy assemblage factory so that a foreman monitors them and speeds up the labour process. Marx here also speaks of ‘simple cooperation’ (MECW 30: 259). The second type is the division of labour within one workshop, the ‘division of labour in the manufacture of a commodity’ (MECW 30: 267) – the manufacture as a ‘specific _mode of production_’ (MECW 30: 268). In toy assemblage, this means that, for example, the first worker assembles the tail of a toy dog in the first step to the dog’s body, the second the head, the third the left ear, the fourth the right ear, the fifth the snout, etc. The third type is the use of machinery. ‘The purpose of machinery, speaking quite generally, is to lessen the value, therefore the price, of the commodity, to cheapen it, i.e. to shorten the labour time necessary for the production of a commodity’ (MECW 30: 318). 
> (p. 43-4)

> These three sections form a direct draft of _Capital Volume I_’s fourth part ‘The Production of Relative Surplus-Value’, where the distinction of the three methods of relative surplus-value production is present in the form of three separate chapters that correspond to the three sections in the 1861–63 _Economic Manuscripts_: Chapter 13 Co-operation (Marx 1867: 439–54), chapter 14: The Division of Labour and Manufacture (Marx 1867: 455–91), chapter 15: Machinery and Large-Scale Industry (Marx 1867: 492–639). 
> (p. 44)

# Economia do 'conhecimento científico' (~gerneral intellect) nos manuscritos 61-3

> In the _Economic Manuscripts of 1861–63_, Marx also repeats the _Grundrisse_’s assumption that in capitalist development, knowledge becomes an immediate force of production:
> 
> > The employment of the NATURAL AGENTS – their incorporation so to speak into capital – coincides with the development of _scientific knowledge_ as an independent factor in the production process. In the same way as the production process becomes an _application of scientific knowledge_, so, conversely, does science become a factor, a function so to speak, of the production process. (MECW 34: 32)
> 
> Marx here does not use the term general intellect, but instead speaks of ‘scientific knowledge’ (MECW 34: 32), ‘[s]cience’ (MECW 34: 34) and ‘the general intellectual product of social development’ (MECW 34: 126). 
> (p. 44)

# Subsunção real (e produção de mais valor relativo) no capitalismo. Importância da tecnologia.

> In the _Results_ [[ _of the Immediate Process of Production_, 1863]], Marx again takes up the question of the formal and real subsumption of labour under capital and points out the importance of machinery as method of relative surplus-value production in the real subsumption of labour under capital:
> 
> > The general features of the _formal subsumption_ remain, viz. the _direct subordination of the labour process to capital_, irrespective of the state of its technological development. But on this foundation there now arises a technologically and otherwise _specific mode of production – capitalist production_ – which transforms the nature _of the labour process and its actual conditions_. Only when that happens do we witness the _real subsumption of labour under capital_. … The real subsumption of labour under capital is developed in all the forms evolved by relative, as opposed to absolute surplus-value. With the real subsumption of labour under capital a complete (and constantly repeated) revolution takes place in the mode of production, in the productivity of the workers and in the relations between workers and capitalists. (Marx 1867: 1034–5) 
> (p. 45-6)

# Desenvolvimentos das noções de subsunção formal e real em Negri e Hardt

> Michael Hardt and Toni Negri have developed the concepts of the formal and real subsumption of labour under capital into the concepts of the formal and real subsumption of society under capital (Hardt & Negri 2009: 230, 2017: 178–82; Negri 1991: 131, 142). Formal subsumption means that non-capitalist relations and spheres play a formal role within capitalism. So, for example, housework is not organised as wage-labour but reproduces labour-power and is thereby productive, value-generating labour that (re-)creates the value of the commodity of labour-power. In the real subsumption of society under labour, formerly non-capitalist relations become directly commodified. Examples are the commodification of land, culture, nature, community, social relations, communication, the digital commons, etc.
> (p. 46)

# Acumulação por desposseção (?) em Harvey (comodificação de quase tudo)

> David Harvey uses the term accumulation by dispossession for the phenomenon that Hardt/Negri term the real subsumption of society under capital. Accumulation by dispossession means the commodification of (almost) everything via privatisation, financialisation, the management and manipulation of crises, and state redistributions (Harvey 2005: 160–5). Accumulation by dispossession is for Harvey ongoing primitive accumulation of capital. ‘All the features of primitive accumulation that Marx mentions have remained powerfully present within capitalism’s historical geography up until now. … Privatization (e.g. of social housing, telecommunications, transportation, water, etc. in Britain) has, in recent years, opened up vast fields for overaccumulated capital to seize upon’ (Harvey 2003: 145, 149).
> (p. 46)

# Diferenças entre a ênfase em acumulação em Harvey x subsunção em Negri/Hardt

> Whereas Harvey uses primitive accumulation for characterising the ongoing capitalist process of accumulation by dispossession based on Rosa Luxemburg, Hardt and Negri (2017: chapter 11) characterise primitive accumulation as capitalism’s first development phase (that was followed by the phase of manufacture/large-scale industry and the phase of social production) and formal and real subsumption as ongoing processes. ‘_Marx’s concept of “formal subsumption” provides a richer framework than primitive accumulation insofar as it reveals geographical and temporal differences and discontinuities by focusing on changes in production processes_’ (Hardt & Negri 2017: 180). 
> (p. 46)

# Maquinaria capitalista como capital fixo e constante e meio de produção de mais-valor relativo no Capital

> _Capital Volume 1_’s chapter 15 ‘Machinery and Large-Scale Industry’ is with 147 pages and ten sections not just _Capital_’s longest chapter, but also Marx’s most detailed exposition of technology’s role in capitalism (see Fuchs 2016d: chapter 15 for a detailed discussion). Marx defines capitalist machinery as fixed constant capital that is a means of relative surplus-value production.
> 
> > Machinery produces relative surplus-value, not only by directly reducing the value of labour-power, and indirectly cheapening it by cheapening the commodities that enter into its reproduction, but also, when it is first introduced sporadically into an industry, by converting the labour employed by the owner of that machinery into labour of a higher degree, by raising the social value of the article produced above its individual value, and thus enabling the capitalist to replace the value of a day’s labour-power by a smaller portion of the value of a day’s product. (Marx 1867: 530)
> (p. 47)

# Posição da tecnologia (em si x capitalista). As mesmas tecnologias não poderiam ser aplicadas no comunismo.

> Marx stresses the dialectical, contradictory character of modern technology: It is as such a means for creating free time and wealth for all, but under capitalist conditions is a means for the exploitation and control of the workers that is embedded into the antagonism of productive forces and relations of production that generates crises. Marx builds his analysis of technology in capitalism on Hegel’s dialectic of essence (technology-as-such) and existence (technology-in-capitalism):
> 
> > The contradictions and antagonisms inseparable from the capitalist application of machinery do not exist, they say, because they do not arise out of machinery as such, but out of its capitalist application! Therefore, since machinery in itself shortens the hours of labour, but when employed by capital it lengthens them; since in itself it lightens labour, but when employed by capital it heightens its intensity; since in itself it is a victory of man over the forces of nature but in the hands of capital it makes man the slave of those forces; since in itself it increases the wealth of the producers, but in the hands of capital it makes them into paupers, the bourgeois economist simply states that the contemplation of machinery in itself demonstrates with exactitude that all these evident contradictions are a mere semblance, present in everyday reality, but not existing in themselves, and therefore having no theoretical existence either. (Marx 1867: 568–9)
> (p. 48)

> Marx remarks in _Capital_ that the application of machinery would ‘be entirely different in a communist society from what it is in bourgeois society’ (Marx 1867: 515, footnote 33), whereby he indicates that one cannot simply apply old technologies without changes in a communist society, but that technology has to be re-designed along with the transformation of society
> (p. 48)

# O papel da tecnologia nas lutas de classe

> In chapter 15, Marx also gives particular attention to machinery in the context of class struggles. Misery, precarity, crises and unemployment caused by capital and the capitalist use of technology result in ‘periodic revolts of the working class against the autocracy of capital’ (Marx 1867: 562). But machinery is also ‘the most powerful weapon for suppressing strikes’ (Marx 1867: 562).
> (p. 48)

# Tecnologia como produção e como comunicação

> Marx distinguishes between the technology as means of production (see this chapter) and means of communication (see Chapter 4). Today, this distinction has become blurred. The networked computer is a convergence technology that is a means of communication and a means of production and a digital machine that enables the production, distribution and consumption of information with one and the same technology The computer is a universal machine, it is not just a medium of communication but also an instrument of production for the production of digital goods, information (e.g. user-generated content), communication and social relations.
> (p. 51-2)

# Síntese da teoria crítica marxiana da tecnologia

> Taken together, we can identify the following elements of Marx’s critical theory of technology:
> 
> - _Dehumanisation_: Capitalism dehumanises individuals by treating them like dead things, resources and machines.
> - _Alienation_: The capitalist use of machines is embedded into the alienation of the workers so that they become appendages to the machine. Capitalist technology has a class and alienated character.
> - _Fixed constant capital_: In capitalism, technology is fixed constant capital and one of the means of relative surplus-value production and control.
> - _Relative surplus-value production_: Co-operation, the division of labour and machinery are three important methods of relative surplus-value production.
> - _The real subsumption of labour under capital_: The distinction of formal and real subsumption of labour under capital discern among two forms of capitalist production. In the second one, technology plays a crucial role as means of relative surplus-value production that qualitatively transforms the production process.
> - _The antagonism of the productive forces and the relations of production_: The capitalist use of technology is embedded into and advances the contradiction between the productive forces and the relations of production that is one of the sources of capitalist crises. Modern technology creates an antagonism between necessary labour-time and surplus labour-time that creates one of the foundations of communism and well-rounded individuality, but within capitalist class relations is one of the sources of crisis, precarious labour, unemployment, overwork and the uneven distribution of labour-time. 
> - _The general intellect_: The development of modern technology in the context of capitalism-s drive to increase productivity results at a specific point of time in the emergence of an information economy, in which the general intellect - science and knowledge in production - has become a direct productive force.
> - _The division of labour_: Capitalist technology is embedded into the class division of labour that results in divisions such as the international division of labour, the gender division of labour, the urban and rural division of labour, the division of labour within one unit of production, the division of labour between labour and management, the division of labour between mental and manual labour, etc.
> - _Social problems_: The capitalist use of machinery contributes to social problems such as overwork, unemployment, stress, workplace injuries, precarious labour, work surveillance, etc.
> - _Technology and class struggles_: Technology does not determine society, but is rather embedded into class struggles. Technology is not the cause, but a means and result of social and societal change. The application of modern technology is contested and its impacts are subject to the outcome of class struggles.
> - _Contradictions of technology, the dialectic of technology and society_: Technology in capitalism has contradictory effects on the economy and society.
> - _Technology and communism_: Communism requires highly productive technologies in order to abolish wage-labour and enable a post-scarcity society that is built around freely determined activities beyond compulsion and necessity.
> (p. 50-1)

![Carater contraditório da tecnologia](./images/tecnologia-marx.png)

# Carater dialético da tecnologia. Nem tecnofilia, nem tecnofobia. Comunismo digital como resultado de uma prática política.

> In the age of digital capitalism, we can learn from Marx’s critical theory of technology that mobile phones, the Internet, social media, data, robotics, artificial intelligence, digital automation and other digital technologies are not evil as such, but at the same time also do not automatically bring about a better society. Digital communism requires democratic and participatory forms of digital media that enable the reduction of necessary labour-time to a minimum, a maximum of free time and a democratic public sphere. Digital media need to be shaped in democratic ways together and embedded into the creation of a participatory democracy in order to form the foundation of a truly democratic society. This means that we require the fundamental transformation of digital capitalism via social struggles and radical reforms so that a participatory digital democracy, a society of the digital and social commons, is not just a hope, but an active hope that we long for not just in our dreams, but also in our political practices.
> (p. 53)
