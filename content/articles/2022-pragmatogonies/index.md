---
title: "Pragmatogonias"
date: 2022-01-24
path: "/pragmatogonias"
category: fichamento
author: Rafael Gonçalves
published: true
tags:
 - Bruno Latour
 - não-humanos
 - tecnociência
 - sociotécnica
featuredImage: "./figure2.png"
srcInfo: Figura 2 no artigo citado (Latour)

---

Fichamento do texto _Pragmatogonies: a mythical account of how humans and non-humans swap properties_[^1] de Bruno Latour.

[^1]: LATOUR, B. Pragmatogonies: A Mythical Account of How Humans and Nonhumans Swap Properties. **American Behavioral Scientist**, v. 37, n. 6, p. 791–808, 1 maio 1994. 

# Princípio de simetria de Bloor

> The study of science and technology has been deeply modified in the last 20
> years through the use of what has been called a _principle of symmetry_ (Bloor,
> 1991). Truth and falsity, efficiency and irrationality, profitability and waste have
> been treated in the same terms instead of being partitioned in two incompatible
> realms. Instead of extracting the three sisters—truth, efficiency, and profitabil-
> ity—from the messy social world, they became mixed into social practice as
> intimately as possible.
> (p. 791)

# Problema nas ciências sociais. Solução: princípio de simetria entre humanos e não humanos.

> Very quickly, however, it appeared that the social theory
> that had been used to study rationality as well as irrationality in a symmetrical
> fashion was deeply flawed because it had been devised in contraposition to the
> world of objects. This birth defect made very difficult the use of the resources
> of the social sciences to study the natural world.
> (p. 791)

> To get out of this difficulty, it has been necessary to define a generalized
> principle of symmetry, not between rational and irrational behavior, but between
> humans and nonhumans (Latour, 1987). The apparent difficulty of this new
> principle—which is nothing but an extension of the first one—is that it seems
> to blur the boundaries between the human subject and the nonhuman object.
> (p. 791)

# Ação técnica como delegração: mobiliza temporalidades, espacialidades e agências diversas

> First, let me define technical action as the form of _delegation_ that allows us
> to mobilize in an interaction movements which have been executed earlier,
> farther away, and by other actants, as though they are still present and available
> to us now. Without the presence of the past, the presence of the far away, the
> presence of nonhuman characters, we would be limited, precisely, to interac-
> tions, to what we can manage to do, right now, with our own social skills, like
> the Machiavellian baboons I have just introduced.
> (p. 792)

# Técnica como socialização de não-humanos (em oposição a mitologia do Homo faber que descreve técnica como acesso imediato a objetividade)

> Second, this definition of technical action does not imply any _Homo faber_
> mythology as if we had through techniques some sort of privileged, unmediated,
> unsocialized access to objective matter and natural forces. Objects, matter, force,
> and nature are latecomers and cannot be used as our starting point. The tradi-
> tional definition of techniques as the imposition of a form consciously planned
> in advance onto some shapeless matter should be replaced by a much more
> oblique, although more accurate, definition as the socialization of non-humans
> (Latour & Lemonnier, 1994).
> (p. 792-3)

# Coletivos modernos: impossibilidade de diferenciar um órgão coletivo, um artefato e um sujeito

> Third, the most important consequence of criticizing the _Homo faber_ myth,
> is that when we exchange properties with nonhumans through technical delega-
> tion, we enter into a complex transaction, which is visible in contemporary
> collectives as well as in traditonal ones. If anything, as I have shown elsewhere
> (Latour, 1993b), what we call _modern_ collectives are not the ones in which
> society and technology are finally divorced from each other, but those in which
> relations are so intimate, transactions so many, and mediations so convoluted,
> that there is no longer any plausible way to differentiate for good a collective
> body, an artifact and a subject.
> (p. 793)

# Crítica da visão da Teoria Crítica: tecnologia como relação social de dominação

> If artifacts are social relations, then
> why on earth has society to pass through them to inscribe itself onto something
> else? Why not inscribe itself directly? After all, the artifacts count for nothing;
> they are just there to transport domination, exclusion, and power, conducting
> them like electricity along a wire. I know the answer critical theory will give.
> By going through the medium of artifacts, power and domination hide them-
> selves under the guise of natural and objective forces. They appear naturalized
> or objectified or reified. Do you see how critical theory functions? First it uses
> a tautology: Technology is nothing but social relations. Then it adds a conspiracy
> theory: Society is hiding itself behind the fetish of techniques.
> (p. 793)

# Sociedade é construída tanto por humanos como por não-humanos

> Society is not stable enough to inscribe itself
> onto anything. On the contrary, most of the features of social order — scale,
> asymmetry, durability, power, division of labor, role distribution, and hierarchy — are impossible even to define without bringing in socialized nonhumans.
> Yes, society is constructed, but not just _socially_ constructed.
> (p. 793)

> So every
> activity suspends the easy commonsense idea that humans speak and act. Every
> activity implies a generalized principle of symmetry or, at the least, offers an
> ambiguous mythology that disputes the unique position of humans. We find
> exactly the same uncertainty with techniques, where we have a human action
> ending up in the action of a nonhuman. So who eventually is responsible for the
> action? Both. The responsibility has to be shared, symmetry restored, and the
> role of humanity shifted sideways from being the sole transcendant cause to that
> of _mediating mediators_.
> (p. 794)

# Pragmatogonia: uma genealogia da troca de propriedades entre humanos e não-humanos

> Unfortunately, I am going to take a totally unreasonable and speculative path.
> Instead of describing sociotechnical networks, I am going to attempt a genealogy
> of the swapping of properties between humans and nonhumans. Because it will
> be as unreasonable, implausible, and unempirical as the cosmogonies of the past,
> and because it will retrace the metamorphosis of the object, I will call it, after
> Serres (1987), a _pragmatogony_.
> (p. 794)

> If I succeed in giving some space for the imagination, this would
> mean that we are not forever stuck with the boring alternation of humans to
> nonhumans and back. It would be possible to imagine a space, that will later be
> studied empirically, in which we could observe the swapping of properties
> without always having to start from a priori definitions of humanity.
> (p. 795)

# 11 - Ecologia política: necessidade de atribuir direitos à não-humanos (políticas das coisas)

> Nowadays, when we look above
> our heads, we watch a sociopolitical imbroglio, because, for instance, the
> depletion of the ozone layer brings together a scientific controversy, a political
> dispute between North and South, and gigantic strategic moves inside industry.
> The idea of a political representation of nonhumans seems not only plausible
> but necessary, although it would have seemed ludicrous or even indecent a few
> years ago.
> (p. 796)


> In the new crossover we bring the property learned
> through this prior experiment in large-scale management of the planet to bear
> on the political system. The new hybrid remains a nonhuman, but not only does
> it lose its material, objective, and rational character, it also takes up some of the
> properties of citizenship. It has rights, it should be protected, it cannot be
> enslaved. To use Michel Serres’s (1990) phrase, we should replace the social
> contract by the “natural contract.” I will call this first layer of meaning — last in
> chronological order — political ecology. We now understand that we must literally and not just symbolically manage the planet and practice the politics of
> things.
> (p. 796-7)

# 10 - Tecnologia: construção da ciência, de organizações, da indústria (objetos-instituições)

> We again encounter a crossover here, but it is of a different sort and goes in
> a different direction, although it can also be called sociotechnical. For the
> scientist I interviewed, there was no question of granting any sort of rights of
> citizenship to the yeast. For him, the yeast is a strictly material entity. The
> industrial laboratory, where new modes of organization of labor elicit com-
> pletely new features of the nonhumans, would be a good definition of what a
> technology is in the English sense of the word: that is, a fusion of science,
> organization, and industry.
> (p. 797)

> In technology, the forms of coordination learned
> through networks of power (see below) are extended to disarticulate entities not
> only on a much larger scale but also in a much more intimate way. Although
> yeast had, for millenia, already been put to work by the old brewing industry,
> the yeast now distributed through the networks of 30 European laboratories to
> have its genome mapped is humanized and socialized in the much more literal
> sense of becoming a code, a book, a program of action, compatible with our
> ways of coding, counting, and reading, and no longer retaining any of its material
> quality, its outsiderness. It has been swallowed within the collective.
> (p. 797)

> Through
> technology, socialness is shared with nonhumans in an almost promiscuous way,
> automatons being endowed with some sort of primitive speech, intelligence,
> foresight, self-control, discipline. They have no rights, to be sure, as in the
> eleventh meaning, but they are much more than material entities: They are
> complicated organizations.
> (p. 797-8)

# 9 - Redes de poder: emergência de malhas de energia, transporte e comunicação a partir de propriedades não-humanas (rearticulações extensivas)

Rede de poder (Hughes), corporação global (Chandler).

> This is not to say that what I call organizations and networks are purely social,
> because they are themselves recapitulating nine former crossovers of humans
> and nonhumans. Alfred Chandler and Thomas Hughes have each shown the
> simultaneous extension of what the former calls the global corporation (Chandler,
> 1990) and the latter calls networks of power (Hughes, 1983). Here again, I could
> talk of a sociotechnical imbroglio and replace the dualist paradigm by the
> seamless web of technical and social factors so beautifully deployed by Hughes.
> (p. 798)

> The extension of networks of power in the electrical industry, in telecommu-
> nications, in transportation, are impossible to imagine without the massive
> mobilization of material entities. The reason why Tom Hughes’ (1983) book is
> so exemplary for the field of science studies lies in his ability to show how a
> technical invention—that of electric lighting—is brought to bear by Edison on
> a mode of organization, of management, of law, that creates a corporation
> without much precedent because its scope and scale, to use Chandler’s title, are
> directly related to the physical properties of the electrical networks. Not that
> Hughes in any way talks of a material infrastructure triggering changes in the
> social superstructure. On the contrary, his networks of power are complete
> hybrids, but hybrids of a peculiar sort: They lend their nonhuman qualities to
> what were until then weak, local, and scattered corporate bodies. Management
> of large masses of electrons, clients, power stations, subsidiaries, meters, and
> dispatching rooms takes on the formal and universal character of scientific law.
> (p. 798)

# Genealogia do aprendizado de propriedades ontológicas: naturalização do social e cosialização do não-humano

> But the point of my little genealogy is to be able to identify inside the seamless
> web the properties which are borrowed from the social world to socialize the
> nonhumans, and, vice versa, from the nonhumans to naturalize and expand
> the social realm. For each layer of meaning, everything happens as if we were
> learning, through our contact with one side, _ontological properties_, which are
> then reimported to the other side, generating new, completely unexpected
> effects.
> (p. 798)

# Matéria é composta de intersecções humano-não-humano e também possui uma genealogia

> The principle of my genealogy,
> however, is that whenever we talk of matter as a given, we are in fact considering
> a package of multiple layers of former crossovers between social and natural
> elements so that what we take as primitive and pure terms are belated and mixed
> ones. Just by retracing the most recent three steps, we can already see that matter
> is vastly different depending on the different layers I have called political
> ecology, technology, or networks of power. Far from being a primitive term,
> always immutable in contrast to a fast changing society, matter has a genealogy
> too, and nonhumans can in no way be limited to their material definition, which,
> on the contrary, we should be able to retrace.
> (p. 799)

# 8 - Indústria: autonomização do trabalho através da composição de não-humanos (automação)

> The extraordinary feat that I will call _industry_ is to grant nonhumans the
> possibility of being related to one another in an assembly of actants that we call
> a machine or an automaton, which is endowed with some sort of autonomy and
> which is submitted to regular laws that can be measured through instruments
> and accounting procedures. From tools held in the hands of human workers, we
> shift to an assembly of machines where tools are related _to one another_, creating
> a massive array of labor and material relations in the new factories that Marx
> has forcefully described as so many circles of Dante’s _Inferno_.
> (p. 799)

Automação não como alienação e desumanização, mas como socialização.

> The paradox of this stage of the relations between humans and nonhumans
> is that it is seen as alienation, dehumanization, as if this was the first time that
> poor and exploited human weakness was confronted with an all-powerful
> objective force. However, to relate nonhumans together in an assembly of
> machines, ruled by laws, and accounted for by instruments, is still to grant them
> some sort of social life. Indeed, the whole modernist project consists of creating
> that peculiar hybrid: A fabricated nonhuman that has nothing of the character of
> society and politics, but that builds the body politic all the more effectively
> because it seems completely estranged from humanity (Latour, 1993b).
> (p. 799)

> We treat it as m-echanistic.
> forgetting that mechanism is one half of the modern definition of society.
> (p. 799)

# 7 - A Megamáquina: organização humana em larga escala como protótipo da máquina (gestão em larga escala)

> Lewis Mumford has made, in a series of beautiful books,
> the intriguing suggestion that megamachines are the templates on which ma-
> chines were then constructed (Mumford, 1966, 1986). First comes the megama-
> chine, that is, the organization of large numbers of humans through chains of
> command, deliberate planning, and accounting procedures. This change of scale
> through the imperial machinery of legal commands is what has first to be
> invented. The local interactions of humans are now extended through the large,
> stratified, externalized body politic, which can keep track of many nested
> subprograms of action through the invention of such intellectual techniques as
> writing, counting, and accounting.
> (p. 800)

> According to Mumford, before having any
> notion of wheels, gears, works, and movements, you first need to have set up
> the very possibility of a large-scale organization. Large-scale management is the
> template for large-scale technologies. Then and only then, by substituting some
> but not all of its subprograms by nonhumans, may you generate machinery and
> factories, industries and automatons. The nonhumans, in this view, enter the
> organization and take up the role of obedient servant which has already been
> rehearsed for centuries by humans enrolled in the imperial megamachine.
> Nonhumans are the understudies of human servants.
> (p. 800)

> Before being able to delegate action to
> nonhumans, and before being able to relate nonhumans to one another in an
> automaton, you first need to be able to nest many subprograms of action into
> one another without losing track of them. Management, in a way, always
> precedes the expansion of material techniques. Or rather, if we want to keep with
> the logic of my story, every time we learn something about the management of
> humans, we shift this new knowledge to the nonhumans, endowing them with
> more and more organizational properties.
> (p. 800)

# Transferência de propriedades humanas para não-humanos e vice-versa

> This is how we could interpret the even-numbered episodes I have recounted
> so far: Industry transfers to nonhumans the management of people learned in
> the imperial megamachine, just as technologies do for the large-scale manage-
> ment learned through networks of power. And if we recapitulate the odd-
> numbered episodes, we see the opposite process at work: What has been learned
> from the nonhumans is then reimported to reconfigure people, as that which
> happens in networks of power and political ecology.
> (p. 800)

# 6 - Ecologia internalizada: socialização de animais, plantas e materiais (reificação)

> How can we
> define domestication and agriculture better than by considering it as the granting
> of socialness and intimacy to nonhuman actants? I will call this process inter-
> nalized ecology; where so many animals, plants, and materials are submitted to
> such an intense socialization, re-education, and reconfiguration, that they
> change shapes, functions, and even genetic makeup (Kent, 1989).
> (p. 801)

> The result of this shift of characters is a
> human-made landscape (gardens, villages, and cities); a development so radical
> that it completely changed what is meant by social and material life.
> (p. 801)

# 5 - Sociedade: reificação nos artefatos de interações individuais (domesticação)

Durkheim: social a priori que age sobre indivíduos

> In the Durkheimian interpretation, a society is what precedes individual
> action, what lasts much longer than any interaction, what dominates us, the
> reality _in which_ we are born, live, and die. Society is this corporate body that is
> so overarching that it socializes us, the humans, giving us a role, a shape, and a
> function; yes, it domesticates us by teaching us how to behave and to conform.
> It is externalized, it is reified, it is more real than ourselves. The origin of all
> religions and sacred rituals, for Durkheim, are nothing but the return, through
> figures and myths, of what is transcendant over any individual interaction:
> society.
> (p. 802)

Garfinkel: social a posteriori como resultado de interações individuais

> And yet we build our society solely through interactions. No matter how
> many roles and functions we have been disciplined into, we still repair the social
> fabric out of our own knowledge and ethnomethods. Durkheim may be right,
> but so is Garfinkel.
> (p. 802)

Mediação técnica como forma duradoura de interações passadas

> Instead, the solution may simply be found in yet
> another incarnation of the word sociotechnical. We are not alone in our interac-
> tions. We also bring the long-lasting influence of all the actions which we, or
> others, have taken in the past through technical mediation.
> (p. 802)

> What Durkheim
> mistook for the effect of a sui generis social order is simply the effect of having
> brought so many techniques to bear on our social relations. From them, we
> learned what it was to last longer, to be spread over space and time, to occupy
> arole, to be dispatched into a function. By reimporting this competence into the
> definition of society, we learned how to reify it, to make it stand independently
> of fast-moving interactions. And indeed, we learned how to delegate to this
> externalized body even the task of delegating us into roles and functions. Yes,
> society exists for real, but no, it is not socially constructed. Even in this, the most
> primitive concept of all social theory, nonhumans proliferate rendering it impossible to recognize a “pure” society.
> (p. 802)

# 4 - Técnicas: extração e combinação de não-humanos (externalização)

> As we learn from archaeologists, techniques
> imply articulated subprograms of action which are spreading in space and time
> (Leroi-Gourhan, 1964).
> (p. 803)

> In other words, they imply not a society, which is a later
> hybrid, but some sort of social organization to hold together nonhumans extracted from very different seasons, matters, and places. A bow and arrow, a
> hammer, a net, a piece of clothing, are made of many different bits and pieces
> which have to be recombined in a time and space sequence bearing no relation
> to their natural settings.
> (p. 803)

> So, techniques are what happened to tools and nonhu-man actants when they were processed by a form of social organization that
> allowed them to be extracted, recombined, and socialized. Even the simplest
> techniques are sociotechnical. Even at this primitive layer of meaning we cannot
> separate forms of organization from technical practices.
> (p. 803)

# 3 - Complicação social: uso de não-humanos para estabilizar relações sociais (articulação)

> Shirley Strum and I have called this third layer of meaning _social complication_
> (Strum & Latour, 1987). Complex interactions are now marked and traced by
> nonhumans brought to bear on social relations. Why would the enrollment of
> nonhumans be of any use? Because they can stabilize social negotiations. At this
> stage, nonhumans offer an extraordinary feature: They are at once pliable and
> durable; they can be shaped very fast, but, once shaped, they last much longer
> than the interaction that has fabricated them. Social interactions, on the other
> hand, are extremely labile and transitory. More exactly, they are either negotiable
> but transient, or, if they are encoded for instance in the genetic makeup, they are
> extremely durable but impossible to easily renegotiate.
> (p. 803)

> By bringing in nonhumans, the contradiction of durability and negotiability is solved. It is now
> possible to trace interactions, to blackbox them, to recombine highly compli-
> cated tasks, to nest subprograms one into another. What was impossible for
> highly complex social animals to do becomes possible when prehumans transfer
> the use of tools not to gain access to food, but to trace, fix, underline, and
> materialize their interactions. The social realm, although still made only of
> interactions, becomes visible and gains some durability through its own tracers.
> (p. 803)

# 2 - O kit básico de ferramentas: extensão de propriedades sociais na forma de ferramentas (flexibilidade, durabilidade)

> Even the basic tool kit, this epitome of the Homo faber myth, cannot be
> accounted for by a sudden access to objective matter, to the obduracy of stones,
> straw, and wood. What is a tool, then, in my genealogy? It is the extension of
> social tools to nonhumans! Remember the complex social negotiation that
> Machiavellian baboons, chimpanzees, gorillas, and vervets are supposed to
> enter, according to primatologists (Byrne & Whiten, 1988)? They have few
> techniques, to be sure, but are perfectly able, as Hans Kummer (1993) has shown,
> to devise social tools through the manipulation and modification of one another
> in their complex strategies (De Waal, 1982; Strum, 1987). If you grant the
> prehumans of my own mythology at least the same kind of social complexity
> (see below), you may generate tools simply by shifting this ability, through a
> crossover, to nonhumans. Just treat pieces of stone and wood as social partners
> and modify them so that you can act on another. Prehuman tool use, in contrast
> to the ad hoc use of implements by primates to fulfill a task, would then be the
> extension of a skill rehearsed in the realm of social interaction. Even the most
> primitive tools already require some sort of social life, but one which is very
> different from my earlier episodes (later episodes in terms of the mythical history
> recounted here).
> (p. 805)


# Não existem 2 genealogias (infraestrutura material e superestrutura social). Mas uma genealogia que engloba negociações do que seria uma história sociotécnica

> Many archeologists try to go
> straight from what I will call the _basic tool kit_ to techniques as if they were
> directly related by a sort of Darwinian evolution of tools into composite tools.
> In the humblest flints some archaeologists are ready to see the first inceptions
> of techniques, of industry, of technology as if a direct route linked stones and
> nuclear plants.
> (p. 803-4)

> There are no two parallel histories, the first for the technical infrastructure and
> the other for the social superstructure, but only one sociotechnical history. There
> are not two parallel histories, one for the function and the other for the style, one
> for the material world and the other for symbolic representation. At every stage,
> according to my pragmatogony, it is through the commerce with nonhumans
> that the necessary social skills and properties are learned, and it is only by
> reimporting those skills back to the nonhumans that they are made to do different
> things and play different roles.
> (p. 804)

# 1 - Complexidade social: interações sociais pré-humanas? (ferramentas sociais)

> We are now back to the point where I started: to the sitcom of Clairborne,
> Crook, and Sharman; to the Machiavellian intelligence of primates, engaged in
> Garfinkelian interactions so as to repair the constantly decaying social order,
> manipulating one another to survive in groups of many conspecifics who are
> constantly interfering with one another. We are back to what Strum (1987) calls
> social complexity. I could go further and show you that even this “primitive
> term” is no freer from contact with nonhumans than any of the later stages, but
> I will spare you the rest of my pragmatogony, the rest of this mad pursuit into
> the logical origin of society and techniques.
> (p. 805)

# A abordagem sociotécnica apresentada escapa do dualismo humano / não-humano

> Even if this very speculative genealogy is entirely false, it shows, at the very
> least, that it is perfectly possible to imagine an alternative to the dualist paradigm
> I have criticized so much. We are not forever stuck in the boring alternation
> between two different substances, one made of objects and matter and the other
> of subjects and symbols. We are not forever limited to “not only, but also” types
> of explanation. According to my origin myth, it is impossible even to conceive
> of an artifact that does not incorporate social relations, or to define a social
> structure without the integration of nonhumans into it. Every human interaction
> is sociotechnical.
> (p. 805-6)

> Second, and more importantly, it is no longer true to say that once we abandon
> the dichotomy between society and techniques, we are simply faced with a
> seamless web of factors (Hughes, 1986) in which everything is included in
> everything else and vice versa, as so many of my critics like to argue (Collins &
> Yearley, 1992; Schaffer, 1991). On the contrary, the properties of humans and
> nonhumans cannot be swapped haphazardly. Not only does there exist a strict
> order in the acquisition of properties, but for each of the layers I have peeled
> away, the meaning of the word sociotechnical may be clarified by considering
> the crossover: what has been learned from the nonhumans and reimported back
> onto the social link, what has been rehearsed in the social realm and exported
> back to the nonhumans. Nonhumans too have a history. They are not material
> objects or constraints. Sociotechnical, is different from sociotechnicalg or socio-
> (p. 806)

# Existe uma alternância entre humano e não-humano, mas ela não tem a ver com humanismo ou objetividade

> Third, it should be clear from Figure 2 that there is a sense, nonetheless, in
> which the old dualism was right. We do indeed have to alternate between the
> state of social relations and the state of nonhuman relations, but this is not the
> same as alternating between humanism and objectivity. The mistake of the
> dualist paradigm comes from its definition of humanism. The very shape of
> humans, our very body, is already made in large part of sociotechnical negotia-
> tions and artifacts. So, considering the human as that which must be protected
> (p. 806)

# Nós somos animais sociotécnicos (mediação entre humano e não-humano)

> We are sociotechnical animals. We are never
> limited to social ties. We are never faced with objects. Where should we position
> humanity, then? Humanity should be positioned in the crossover, in the middle
> column of Figure 2, as the very possibility of mediating between different
> mediators.
> (p. 806)

# Pragmatogonia mostra uma diferença de escala (e não de separação entre natureza e culura). Tese de Jamais fomos modernos. Não existe grande divisão entre coletivos tradicionais e modernos.

> Fourth, in the pragmatogony I have attempted in this article, I reasoned as if
> we alternated from the social to the nonhuman repertoire, always through the
> same move: When we wanted to understand how an object comes to the
> collective, we looked at what type of social relevance with which it had first to
> be endowed, and when we wanted to understand how a social interaction could
> sustain a durable social link, we looked for those nonhumans which could lend
> their properties so as to render the social order more durable. This meant
> retracing the creation of a collective by the enroliment of nonhumans. I wanted
> to demonstrate that it is possible to pay respect to technical mediation without
> using the dualist paradigm, without inventing those two artifacts, a society, on
> the one hand, and an objective world, on the other. But scale is another feature
> of that movement. Ateach of the 11 moves I have retraced, amuch larger number
> of humans are mixed with a much larger number of nonhumans, to the point
> where, today, the whole planet is internalized in the making of our politics, of
> our legal system, and, soon, of our morality.
> (p. 806-7)


> The illusion of modernity was to believe that the more we grew the more
> distant objectivity and subjectivity would become, thus creating a future radi-
> cally different from our past. After the paradigm shift in our conception of
> science and technology, we now know that this will never be the case, indeed
> that this has _never been_ the case.
> (p. 807)

> Objectivity and subjectivity are not opposed,
> they grow together, and they grow irreversibly together, thus breaking the great
> divide between so-called traditional and modern collectives.
> (p. 807)

# Coletivo como emaranhado de sociedade e mundo objetivo

> 5 - I use the word collective as a substantive to mean the tangle (as conventionally understood) of the society (humans-among-themselves) and the objective world (things-in-themselves).
> (p. 807)
