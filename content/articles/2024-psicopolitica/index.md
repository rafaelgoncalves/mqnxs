---
title: Psicopolítica
tags:
    - Byung-Chul Han
    - neoliberalismo
    - capitalismo
    - saúde mental
    - big data
    - neoliberalismo
    - capitalismo da emoção
    - poder
    - fichamento

path: "/psicopolitica"
date: 2024-05-14
category: fichamento
featuredImage: "han.png"
srcInfo: <a href="https://brasil.elpais.com/brasil/2018/02/07/cultura/1517989873_086219.html">Byung-Chul Han</a>
published: true

---

Fichamento de Psicopolítica[^1] de Byung-Chul Han.

[^1]: Han, Byung-Chul. **Psicopolítica**: O neoliberalismo e as novas técnicas de poder. Editora Ayiné, Belo Horizonte-MG. 10a Edição. 2023.

# Do humano como sujeito ao humano como projeto

> Hoje, acreditamos que não somos sujeitos submissos, mas projetos livres, que se esboçam e se reinventam incessante mente. A passagem do sujeito ao projeto é acompanhada pelo sentimento de liberdade. E esse mesmo projeto já não se mostra tanto como uma figura de coerção, mas sim como uma forma mais eficiente de subjetivação e sujeição. **O «eu» como projeto, que acreditava ter se libertado das coerções externas e das restrições impostas por outros, submete-se agora a coações internas, na forma de obrigações de desempenho e otimização.**
> (p. 9)

# Crise da liberdade (em Marx): liberdade individual = liberdade do capital

> O capital se multiplica enquanto competimos livremente uns com os outros. A liberdade individual é uma servidão na medida em que é tomada pelo capital para sua própria multiplicação. **Assim, o capital explora a liberdade do indivíduo para se reproduzir: «Na livre concorrência, não são os indivíduos que são liberados, mas o capital». A liberdade do capital se realiza por meio da liberdade individual.** Des sa maneira, o indivíduo livre é rebaixado a **órgão genital** do capital. **A liberdade individual concede ao capital uma subjetividade «automática», que o incita à reprodução ativa.**  Assim, o capital «pare» continuamente «filhotes». A liberda de individual, que atualmente assume uma forma excessiva, é nada mais nada menos do que o excesso do próprio capital.
> (p. 13)

# Panóptico digital

> Com fins disciplinares, os internos do pan-óptico ben- thaminiano eram isolados uns dos outros, de modo que não conversassem. Os internos do pan-óptico digital, por sua vez, comunicam-se intensivamente e expõem-se por von tade própria. Participam assim, ativamente, da construção do pan-óptico digital. A sociedade digital de controle faz uso intensivo da liberdade. Ela só é possível graças à autorrevelação e à autoexposição voluntárias. O Grande Irmão digital repassa, por assim dizer, seu trabalho aos internos. **Assim, a entrega dos dados não acontece por coação, mas a partir de uma necessidade interna. Aí reside a eficiência do pan-óptico digital.**
> (p. 19)

# Transparência como dispositivo neoliberal

> **A transparência também é reivindicada em nome da liber dadede informação. Na verdade, ela não é nada mais do que um dispositivo neoliberal.** Ela vira tudo violentamente para fora, para que possa produzir informação. **Nos modos atuais de produção imaterial, mais informação e mais comunicação significam mais produtividade, aceleração e crescimento.** A informação é uma positividade que, por carecer de interioridade, pode circular independente do contexto. Isso permite que a circulação de informações seja acelerada à vontade.
> (p. 19-20)

# Dispositivo da transparência -> conformidade

> **Uma conformidade total é outra consequência do dispositivo da transparência. A supressão de divergências faz parte da economia da transparência.** A conexão e a comunicação totais já possuem em si um efeito nivelador. Geram um efeito de conformidade, **como se cada um vigiasse o outro até mes mo antes de qualquer vigilancia e controle através de serviços secretos.** O que ocorre hoje é urna vigilancia sem vigilancia.  **A comunicação é aplainada como que por moderadores in visíveis e rebaixada à condição de consenso. Essa vigilancia primaria e intrínseca é muito mais problemática do que a vigi lancia secundaria e extrínseca dos serviços secretos.**
> (p. 20-1)

# Psicopolítica digital e big data como instrumento psicopolítico. Vigilância passiva > controle ativo.

> Hoje, caminhamos para a **era da psicopolítica digital, que avança da vigilância passiva ao controle ativo, empurrando-nos, assim, para uma nova crise da liberdade: até a vontade própria é atingida.** **Os big data são um instrumento psicopolítico muito eficiente,** que permite alcançar um conhecimento abrangente sobre as dinâmicas da comunicação social. **Trata-se de um conhecimento de dominação que permi te intervir na psique e que pode influenciá-la em um nível pré-reflexivo.**
> (p. 23)

# Big data -> controle do futuro

> A abertura do futuro é constitutiva para a liberdade de ação. **Contudo, os big data tornam possíveis prognósticos so bre o comportamento humano. Dessa maneira, o futuro se torna previsível e controlável.** A psicopolítica digital transforma a negatividade da decisão livre na positividade de um estado de coisas. A própria pessoa se positiviza em coisa, que é quantificável, mensurável e controlável. Nenhuma coisa porém é livre: todavia, é mais transparente do que uma pessoa. **Os big data anunciam o fim da pessoa e do livre-arbítrio.**
> (p. 23)

# Smartphone como objeto de devoção digital

> Devoto significa submisso. **O smartphone é um objeto digital de devoção. Mais ainda, é o objeto de devoção do digital por excelência.** Como aparato de subjetivação, funciona como o rosário, e a comparação pode ser estendida ao seu manuseio. Ambos envolvem autocontrole e exame de si. A dominação aumenta sua eficiência na medida em que delega a vigilância a cada um dos indivíduos. **O curtir é o amém di gital. Quando clicamos nele, subordinamo-nos ao contexto de dominação. O smartphone não é apenas um aparelho de monitoramento eficaz, mas também um confessionário móvel. O Facebook é a igreja ou a sinagoga (que literal mente significa «assembléia») do digital.**
> (p. 24)

# Big data como psicograma (individual, coletivo e do inconsciente)

> A biopolítica é a técnica de governança da socieda de disciplinar, mas é totalmente inadequada para o regime neoliberal, que, antes de tudo, explora a psique. A biopolítica, que usa as estatísticas demográficas, não possui acesso ao psíquico. Ela não fornece um psicograma da população. A demografía não é uma psicografia; não explora a psique. **Aí reside a diferença entre a estatística e o big data. A partir do big data é possível extrair não apenas o psicograma individual, mas o psicograma coletivo, e quem sabe até o psicograma do inconsciente.** Isso permitiría expor e explorar a psique até o inconsciente.
> (p. 35-6)

# Psicopolítica em Alexandra Rau (p. 38, n. 2)

# Psicopoder em Stiegler (p. 41)

# Técnica em Foucault (de dominação e do eu) (p. 44, n. 12)

# Choque a partir de Naomi Klein (dr. Ewen Cameron e Milton Friedman) > psicopolítica

> O eletrochoque deve sua eficácia à paralisia e à aniquilação dos conteúdos psíquicos. A negatividade é sua essência. Por sua vez, a psicopolítica neoliberal é dominada pela positividade. Em vez de usar ameaças negativas, ela trabalha com estímulos positivos. Não aplica nenhum «remédio amargo», e sim o curtir.
> (p. 56)

# Novo objetivo do poder: futuro x passado

> Esse Estado de vigilancia orwelliana, com suas teletelas e as suas cámaras de tortura, diferencia-se fundamentalmente do pan-óptico digital (com a internet, os smartphones e o Google glass), que é dominado pela aparência de liberdade e comunicação ilimitadas. **Nesse pan-óptico não se é torturado, se é tuítado ou postado. Não há nenhum Ministério da Verdade. A transparência e a informação substituem a verdade. O novo objetivo do poder não consiste na adminis tração do passado, mas no controle psicopolítico do futuro.  A técnica de poder do regime neoliberal não é proibi tiva, protetora ou repressiva, mas prospectiva, permissiva e projetiva. O consumo não se reprime, só se maximiza. É gerada não uma escassez, mas uma abundância, um excesso de positividade. Somos todos compelidos a comunicar e a consumir.** O princípio de negatividade, que ainda define o Estado de vigilância de Orwell, cede lugar ao de positividade. As necessidades não são suprimidas, mas estimuladas.  Em vez de confissões extorquidas, há exposição voluntária.  O smartphone substitui a câmara de tortura. O Grande Irmão tem agora um rosto amável, A eficiência da sua vigi lância está em sua amabilidade.
> (p. 60-1)

# Emoção [e afeto] (subjetivo e performativo) x disposição [e sentimento] (objetivo e representativo)

> Um lugar, por exemplo, pode propagar uma disposição amigável. E algo bem ob jetivo. **Uma emoção amigável ou um afeto amigável não existem. A disposição não é nem intencional nem performativa. E algo em que alguém se encontra. Representa um estado de espirito. Por isso, é estática e constelativa, enquanto a emoção é dinâmica e performativa.** Não é o de onde (Wbrin) do estado de ânimo, mas o para onde (Wohin) que caracteriza a emoção. E o sentimento é constituído pelo para que (Wofür).
> (p. 66)

# Capitalismo da emoção: exploração neoliberal da subjetividade livre

> Illouz {autora de _Intimidades congeladas_} claramente ignora que a conjuntura atual emoção se deve, em última instância, ao neoliberalismo.  **O regime neoliberal emprega as emoções como recursos para alcançar mais produtividade e desempenho. A partir de certo nível de produção, a racionalidade, que representa o médium da sociedade disciplinar, atinge seus limites. Ela é percebida como uma restrição, uma inibição. De repente, a racionalidade atua de forma rígida e inflexível. Em seu lugar, entra em cena a emocionalidade, que está associada ao sentimento de liberdade que acompanha o livre desdo bramento individual. Ser livre significa deixar as emoções correrem livres. O capitalismo da emoção faz uso da li berdade.** A emoção é celebrada como expressão da subje tividade livre. A técnica neoliberal de poder explora essa subjetividade livre.
> (p. 69)

# Capacidade de influenciar o invivíduo no nível pré-reflexivo pela performatividade da emoção

> **As emoções são performativas no sentido de que evocam certas ações: como tendencia, representam a base energética ou mesmo sensível da ação.** As emoções são controladas pelo sistema límbico, no qual também se assentam os im pulsos. Eles formam o nível pré-reflexivo, semiconsciente e corporalmente impulsivo da ação, do qual frequentemente não se tem consciência de forma expressa. **A psicopolítica neoliberal se ocupa da emoção para influenciar ações sobre esse nível pré-reflexivo. Através da emoção, as pessoas são profundamente atingidas. Assim, ela representa um meio muito eficiente de controle psicopolítico do indivíduo.**
> (p. 72)

# Gamificação como captura do ócio pelo capital

> O «ócio como tempo para atividades mais elevadas» transforma seu possuidor em «outro sujeito», que possui mais força produtiva do que o sujeito que apenas trabalha. O tempo livre como «tempo para o desenvolvimento pleno do indivíduo» colabora para a «produção de capital fixo». Assim, o conhecimento é ca pitalizado. Para usar termos atuais, o aumento do tempo de ócio multiplica o capital humano. O ócio, que possibilitaria.  uma atividade casual e sem finalidade, é tomado pelo capital.
> (p. 75)

# Jogo x trabalho (como dois modos de pensamento)

> **Existem dois modos de pensamento: o que trabalha e o que joga.** Tanto o pensamento de Hegel como o de Marx é regido pelo princípio do trabalho. Da mesma maneira, O ser e o tempo de Heidegger também é devedor do trabalho. O «Dasein» em seu «cuidado» [Sorge] ou «angústia" [Angst] não joga. Somente o último Heidegger descobre o jogo com base na «serenidade». Assim, ele interpreta o próprio mundo como jogo. Ele investiga o «aberto de campo de jogo que mal pressentimos e que mal levamos em conta». **O «espaço-de-jogo-temporal remete a um espaço-tempo livre de qualquer forma de trabalho. E um espaço-aconte-cimento, no qual a psicologia como meio de subjetivação é completamente superada.**
> (p. 79)

# Vigilância digital aperspectivista

> **A vigilância digital é mais eficiente porque é aperspectivista.** Ela é livre de limitações perspectivistas que são caracte rísticas da óptica analógica. **A óptica digital possibilita a vi gilância a partir de qualquer ângulo. Assim, elimina pontos cegos.** Em contraste com a óptica analógica e perspectivista, a óptica digital pode espiar até a psique.
> (p. 82)

# Dadoísmo como segundo iluminismo (estatística > dados transparentes)

> **O dataísmo surge com a ênfase em um segundo Iluminismo.** No primeiro Iluminismo, acreditava-se que a estatística seria capaz de libertar o conhecimento do teor mitológico; por isso, a estatística foi festejada com euforia pelo primeiro Iluminismo. A luz da estatística, Voltaire almejava uma his tória que fosse separada da mitologia. De acordo com ele, a estatística seria um «objeto de curiosidade para quem quer ler a história como cidadão e como filósofo».
> (p. 83-4)

> **A transparencia é a palavra-chave para o segundo Iluminismo. Os dados são um médium transparente:** são, como tam bém se pode 1er no artigo do New York Times, urna «lente transparente e confiável». **O imperativo do segundo Iluminismo é: tudo deve se tornar dados e informação. Esse totalitarismo ou fetichismo dos dados marca o segundo Iluminismo. O dataísmo, que acredita que qualquer ideologia pode ser deixada para trás, é em si mesmo uma ideologia: conduz a um totalitarismo digital.** Assim, é necessário urçi terceiro Iluminismo, que nos ilumine mostrando que o Iluminismo digital se converte em servidão.
> (p. 84-5)

# Sexualização de dados (digitus > phallus)

> Atualmente, **os números e os dados nao são apenas absolutizados, mas também sexualizados e fetichizados.** O Quantified Self autoconhecimento através dos números, é praticado, por exemplo, a partir de uma energia libidinosa.  **O dataísmo desenvolve características libidinais, chegando a traços pornográficos. Os dataístas copulam com dados. As sim, fala-se entrementes de «datassexuais». Eles seriam «implacavelmente digitais» e considerariam os dados «sexy». O digitus se aproxima do phallus.**
> (p. 87)

# Quantified self (p. 88-90)

# Aproximação entre câmera de vídeo (inconsciente óptico) e big data (inconsciente digital)

> Poderia se estabelecer uma analogia entre os big data e a câmera de cinema. **Como uma lupa digital, o data-mining ampliaria as ações humanas e revelaria, por trás do espaço de ação estruturado pela consciência, um campo de ação estruturado de maneira inconsciente. A microfísica dos big data tornaria visíveis actomes, isto é, microações que escapariam à consciência. Os big data também poderiam promover padrões coletivos de comportamento dos quais não seríamos conscientes como indivíduos.** Com isso, o inconsciente co letivo ficaria acessível. **Analogamente ao «inconsciente óptico», a inter-relação microfísica ou micropsíquica também podería ser chamada de inconsciente digital.** A psicopolítica digital seria então capaz de aproveitar o comportamento das massas em um nivel que escapa à consciência.
> (p. 94)

# Romantismo x estatística (big data do século 17)

> **A euforia atual em torno dos big data é muito parecida com aquela em torno da estatística do século XVIII, que, porém, diminuiu rapidamente. A estatística corresponde, com efeito, aos big data da época.** Logo surgiu uma resistência contra ela, especialmente por parte do Romantismo.  A abominação da média e da normalidade é o afeto fundamental desse movimento. O singular, o improvável e o re pentino se opõem ao estatisticamente provável. **O Romantismo cultivou o peculiar, o anormal e o extremo contra a normalidade estatística.**
> (p. 107)

# Limite do big data (similar à estatística) à predição do futuro

> **Da transparência surge uma pressão por conformidade que elimina o outro, o estranho, o desviante.** Os big data tornam visíveis sobretudo os padrões comportamentais coletivos. O próprio dataísmo reforça o crescer tornando-se iguais. **O data-mining não é, em princípio, distinto da estatística. As correlações que ele ex põe mostram o estatisticamente provável. São calculados os valores médios estatísticos. Assim, os big data não têm ne nhum acesso àquilo que é único. Eles são completamente cegos ao acontecimento.** Não é o estatisticamente provável, mas **o improvável, o singular, o acontecimento que determinará a história, o futuro humano.** Por isso, os big data tam bém são cegos ao futuro.
> (p. 109)

# Idiotismo (criativo) x inteligência (sistêmica)

> **Inteligência significa escolher entre (do latim inter-legere). Ela não é completamente livre, na medida em que está presa a um entre determinado pelo sistema.** Não tem nenhum acesso ao fora, porque só tem a escolha entre opções dentro de um sistema. Portanto, não é de fato uma livre escolha, mas uma seleção de ofertas dispostas pelo sistema. A inteligência segue a lógica de um sistema. Ela é imanentemente sistêmica. Cada respectivo sistema define sua respectiva inteligência. Logo, a inteligência não possui nenhum acesso ao inteiramente Outro. **Ela habita o horizontal enquanto o idiota toca o vertical na medida em que abandona a inteligência, ou seja, o sistema predominante: «O interior da idiotice é delicado e transparente como a asa de uma libélula; ele cintila com a inteligência superada».**
> (p. 120-1)

