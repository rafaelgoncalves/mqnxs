---
title: Colonialismo de dados
tags:
    - Nick Couldry
    - Ulises Mejias
    - Aníbal Quijano
    - colonialismo de dados
    - pós-colonialismo
    - big data
    - tecnologias da informação
    - fichamento

date: 2023-05-16
category: fichamento
path: "/colonialismo-de-dados"
featuredImage: "couldry-mejias.png"
srcInfo: <a href="http://www.nickcouldry.org/">Nick Couldry</a> e <a href="https://ulisesmejias.com/">Ulises Mejias</a>
published: true
---

Fichamento do artigo "Colonialismo de dados"[^1] de Couldry e Mejias.

[^1]: COULDRY, N.; MEJIAS, U. A. Data Colonialism: Rethinking Big Data’s Relation to the Contemporary Subject. **Television & New Media**, v. 20, n. 4, p. 336–349, 1 maio 2019.

# Abstract

> We are often told that data are the new oil. But unlike oil, **data are not a substance found in nature. It must be appropriated.** **The capture and processing of social data unfolds through a process we call data relations, which ensures the “natural” conversion of daily life into a data stream.** The result is nothing less than a new social order, based on continuous tracking, and offering unprecedented new opportunities for social discrimination and behavioral influence. **We propose that this process is best understood through the history of colonialism.** Thus, data relations enact a new form of data colonialism, normalizing the exploitation of human beings through data, just as historic colonialism appropriated territory and resources and ruled subjects for profit. **Data colonialism paves the way for a new stage of capitalism whose outlines we only glimpse: the capitalization of life without limit.**
> (p. 336)

# Colonialismo de dados como neocolonialismo (nem metáfora, nem continuação do colonialismo simplesmente). Pensar Big Data a partir do Sul.

> This article foregrounds the importance of resisting such developments and so speaks from “the South” within Milan and Treré’s (2019) expanded notion as developed in this issue. But it does so by refashioning the very term—colonialism—whose historical forms generated the position of the “South” originally. For what is missing in the insightful recent accounts of Big Data is a wider frame to make sense of the whole social process under way. **That frame is colonialism, used here not as a mere metaphor,1 nor as an echo or simple continuation of historic forms of territorial colonialism,2 but to refer to a new form of colonialism distinctive of the twenty-first century: data colonialism.**
> (p. 336-7)

# Colonialismo de dados como combinação de práticas predatórias de extração com métodos de quantificação abstratas. Colonialismo de dados como precondição de um novo estágio do capitalismo emq ue a apropriação da vida humana [apenas? ou da vida em seu sentido mais amplo?] será central. [Mas a apropriação de vida já não é central? Pensar em Laymert sobre informação digital e genética].

> **Data colonialism combines the predatory extractive practices of historical colonialism with the abstract quantification methods of computing.** Understanding Big Data from the Global South means understanding capitalism’s current dependence on this new type of appropriation that works at every point in space where people or things are attached to today’s infrastructures of connection. The scale of this transformation means that it is premature to map the forms of capitalism that will emerge from it on a global scale. **Just as historical colonialism over the long-run provided the essential preconditions for the emergence of industrial capitalism, so over time, we can expect that data colonialism will provide the preconditions for a new stage of capitalism that as yet we can barely imagine, but for which the appropriation of human life through data will be central.** Right now, the priority is not to speculate about that eventual stage of capitalism, but to resist the data colonialism that is under way. This is how we understand Big Data from the South.
> (p. 337)

# Relações de dado como maneira através da qual a VIDA SOCIAL se torna um recurso para extração pelo capital. Dois polos de poder do colonialismo de dados: EUA e China.

> **Through what we call “data relations” (new types of human relations which enable the extraction of data for commodification), social life all over the globe becomes an “open” resource for extraction that is somehow “just there” for capital.** These global flows of data are as expansive as historic colonialism’s appropriation of land, resources, and bodies, although the epicenter has somewhat shifted. **Data colonialism involves not one pole of colonial power (“the West”), but at least two: the United States and China.** This complicates our notion of the geography of the Global South, a concept which until now helped situate resistance and disidentification along geographic divisions between former colonizers and colonized. Instead, the new data colonialism works both externally—on a global scale—and internally on its own home populations. The elites of data colonialism (think of Facebook) benefit from colonization in both dimensions, and North–South, East–West divisions no longer matter in the same way.
> (p. 337)

# Divergências com a posição autonomista sobre a fábrica social (Lazzarato, Negri, Hardt e outros) e a extensão de exploração de trabalho para a esfera da vida cotidiana.

> It is important to acknowledge both the apparent similarities and the significant differences between our argument and the many preceding critical arguments about Big Data. Today’s exposure of daily life to capitalist forces of datafication might seem similar, first, to arguments from four or five decades ago that **the capitalist organization of work had extended from the factory to the whole of society: the well-known Autonomist notion of the “social factory” (Gill and Pratt 2008; Terranova 2000; Tronti 1966; more recently, Hardt and Negri 2017 [[_Assembly_]]; Lazzarato 2014 [[_Signs and machines_]]). Indeed, much important work has been done more recently on the actual exploitation of quasi-labor or playbor, for example, on digital platforms (Fuchs 2017; Scholz 2013), which in turn have their roots often in an alternative strand of Marxist analysis (Smythe 1977).** But **our argument is not concerned here specifically with the exploitation of labor. Nor are we claiming that everyday life is now governed and managed _as if_ it was labor.** The weakness of the latter position has been noted even by those sympathetic to the Autonomist position (Ross 2013, 26) who have stressed the importance of unpaid work not only to today’s capitalism, but to Marx’s original model of capitalism (cf. Moore 2015, 69-71).
> (p. 337-8)

> A factor that has hampered our understanding of the scale of change, and its analogies to earlier forms of colonialism (not just capitalism) is the common suggestion that recent developments of capitalism had already largely been anticipated by Autonomism and its many followers. Certainly, they pointed in the direction of a _general_ intensification of social life’s orientation toward capitalism, but they were rather vague as to the mechanisms, except when they relied on the idea that the structure and norms _of work_ somehow expanded out into social life. As Gill and Pratt (2008, 7, emphasis added) put it, “from [the social factory] perspective _labour_ is deterritorialized, dispersed and decentralized so that ‘the whole society is placed at the disposal of profit’” (Negri 1989, 79). Marazzi (2008, 50, emphasis added) analogously argues that “today the capitalist organization _of work_ aims to . . . fuse work and worker, to put _to work_ the entire lives of workers.” **But that gives us no grip at all on data colonialism, which appropriates life as raw material whether or not it is actually labor, or even labor-like.**
> (p. 338)

# Big Data como apropriação ou extração de recursos. Colonialismo de dados como quadro amplo onde esses processos acontecem.

> **More helpful are approaches which see what is going on with data as a form of fundamental appropriation (Greene and Joseph 2015; Thatcher et al. 2016, drawing on Harvey 2004) or extraction (Mezzadra and Neilson 2017) of resources.** This appropriation is certainly complex. It is not simply a matter of harvesting a natural resource: first, **life needs to be configured so as to generate such a resource** (we come to the contribution of platforms later); second, **data about one individual’s actions or properties at one moment needs to be combined with data about other actions, moments, and properties to generate valuable relations between data points** (Arvidsson 2016; Thatcher et al. 2016, 995). A great deal of important work in the past half-decade has explained how exactly this happens and the details of how human actors are drawn into relations that extract and manage such data.3 **But what is still lacking, we suggest, is a broader frame for grasping the whole transformation of which such data appropriation via digital platforms is just one part. That frame we suggest is data colonialism.**
> (p. 338)

> In response [[ to the autonomist social-factory perspective ]], we must recover a site of resistance which confronts the new and distinctive appropriations of twenty-first-century capitalism, as practices of data processing are played out across a global terrain of differentiated exploitation (Arora 2016). **Our temporal scale for appreciating these developments should be not so much the past half-century of socializing capitalism, especially in Europe, but the centuries-long global cycle of colonialism’s long intertwining with capitalism.** Just as industrial capitalism would not have happened without the prior appropriation under historical colonialism of vast territories, their “natural” resources and bodies, so **we are today witnessing the first stage of another long-term double development: the colonial appropriation of life in general and its annexation to capital, through various mechanisms of which one is the digital platform.** **The platform, we argue, _produces_ the social for capital, that is, a form of “social” that is ready for appropriation and exploitation for value as data, when combined with other data similarly appropriated.** Rather than an expansion of the labor process, the better analogy is with the appropriation of physical nature within processes of capitalist production (Moore 2015). But because this appropriation makes us all _subjects_ of capital in distinctive new ways, the most useful overall framing here is of a new phase of colonialism that is deeply intertwined with the long-term development of capitalism.
> (p. 338-9)

# Para que a captura de dados pessoais seja possível, racionalidades extrativistas devem ser naturalizadas e normalizadas, o fluxo do dia-a-dia deve ser representado em uma forma capturável

> Personal data of many sorts is appropriated for ends which are not themselves “personal.” By personal data we mean data of actual or potential relevance to persons, whether collected from them or from other persons or things. **For personal data to be freely available for appropriation, it must first be treated as a natural resource, a resource that is _just there_. Extractive rationalities need to be naturalized or normalized, and, even more fundamentally, the flow of everyday life must be reconfigured and represented in a form that enables its capture _as_ data.**
> (p. 339)

# Dado pessoal como construção (socialidade construída)

> Jason Moore (2015) argues that capitalism historically depended on the availability of cheap nature: natural resources that are abundant, easy to appropriate from their rightful owners, and whose depletion is seen as unproblematic, **but whose “availability to capital” itself had to be constructed through elaborate means of marketization.** So too with what we now call **“personal data,” but which is the outcome, not the precondition or prior target, of a newly “computed sociality”** (Alaimo and Kallinikos 2016). That is the underlying reason why **there cannot be raw data (Gitelman 2013): because what is “given” must first be configured for “capture” (Kitchin and Dodge 2011).** Natural resources were and are not cheap per se, but legal and philosophical frameworks were established to rationalize them as such, on the basis that they were “just there.” Only later did the costs to humanity of treating natural resources this way come to be appreciated. But, as Julie Cohen (2018) points out, the legal fiction that land inhabited for millennia (such as the territory now known as Australia) was _terra nullius_ or “no man’s land” in English law, and thus available for exploitation without legal interference, has its strong parallels today.
> (p. 339-40)

# Dado como novo petróleo como trabalho ideológico

> The apparent naturalness of data colonialism’s appropriations relies also on a large amount of ideological work, just as historic colonialism did. Consider the business cliché that data are “the new oil,” lost to humanity until corporations appropriate it for some purpose. This rests on the construction of data as a “raw material” with natural _value_, as the World Economic Forum (WEF) claims: “personal data will be the new ‘oil’—a valuable resource of the 21st century . . . becoming a new type of raw material that’s on par with capital and labour” (WEF 2011, 5, 7). Through this discursive move, the links of data back to a prior process of data collection (i.e., appropriation) are obscured.
> (p. 340)

# Data exhaust como noção de que não há posse sobre os dados

> A blurring is achieved metaphorically through the common idea that data are “merely” the “exhaust” exuded by people’s lives, and so not capable of being owned by anyone (United Nations [UN] 2012, 9).
> (p. 340)

# Outras racionalidades extrativistas [que não a ideológica?]: social, prática e política

> To accomplish the appropriation of personal data, data colonialism relies on other _extractive rationalities_ as well. **There is, as many critics have noted (Fuchs 2017; Scholz 2013)—and this is what we share with earlier debates—a social rationality that treats much of the labor that contributes to data extraction as value-less, as “just sharing.” There is also a practical rationality that frames corporations as the only ones with the power and capacity to process (and thus appropriate) data. Simultaneously, a political rationality operates to position society as the natural beneficiary of corporations’ extractive efforts, just as humanity was supposed to benefit from historical colonialism as a “civilizational” project.**
> (p. 340)

# Setor de quantificação do social como conglomerados dos EUA e da China que participam dos processos de análise e corretagem [brokerage] de dados

> **The principal actors in data colonialism can collectively be called the _social quantification sector_, corporations involved in capturing everyday social acts and translating them into quantifiable data which is analyzed and used for the generation of profit.** Firms like Amazon, Apple, Facebook, and Google in “the West,” [[ ~ GAFAM ou FAAMG ]] and Baidu, Alibaba, and Tencent in China, are its most well-known players. The social quantification sector includes both big and small hardware and software manufacturers, developers of social media platforms, and firms dedicated to data analysis and brokerage. **The latter, a largely unregulated part of the economy, specialize in collecting information from medical, financial, criminal, and other records for categorizing individuals through algorithmic means.** Data brokers package and sell those lists to advertisers and other users such as governments and law enforcement agencies.
> (p. 340)

# Plataformas digitais como meios tecnológicos de produção de dados [a partir de informação?]. [Pensar que, a partir de Tarde/Latour, social pode ser extendido para além do humano]. (1)

> First, colonial history helps us see the emergence of digital platforms as more than business invention, or even new forms of economic control through multisided markets (Cohen 2018; Gillespie 2010; Rieder and Sire 2014). **Digital platforms are the technological means that produce a new type of “social” for capital: that is, the social in a form that can be continuously tracked, captured, sorted, and counted for value as “data.”** Platforms are a key means whereby the general domain of everyday life, much of it until now outside the formal scope of economic relations, can be caught within the net of marketization. Arvidsson (2016) analyzes well the technical methods of extracting value from platform data as a form of financialization, but the more basic move is **the appropriation of the social itself.** **As the purpose of “social” media platforms is to encourage ever more of our activities and inner thoughts to occur on platforms, there is, in principle, no limit to the appropriation, as what is appropriated is what is increasingly constructed to be ready for appropriation.**
> (p. 341)

# Apropriação da vida para o capital nas logísticas centradas em dado em todas as áreas de produção humana [e não humana?] (2)

> But social media platforms’ capture of data for commodification and value extraction is only one form that the annexation of life to capital takes under data colonialism. **A second [[ form of annexation of life to capital ]] is the immense growth of data-driven logistics in all areas of human production (Cowen 2014), whether or not for profit.** Whereas logistics itself emerged as the management of goods’ movement within global supply chains, the general “logic” of logistics treats all production on any scale and involving any combination of human and nonhuman parties, as ready for management through data. This incorporates continuous data collection and large-scale data-processing into many areas of work that previously were managed very differently. Important as the growth of quasi-labor on and around digital platforms is, and important as various forms of underpaid labor are (e.g., for small tasks on Amazon’s “Mechanical Turk” program or other areas of the gig or sharing economy), **they are only part of the broader growth of data-driven logistical management across all work, which constitutes the second way in which life is being appropriated for capital.**
> (p. 341 )

# Autogestão de atividades para extração de dados seja de forma voluntária ou como requisito contratual (3)

> The third distinct way in which human life is being appropriated through new social relations is where **individuals track their own activities for data extraction, sometimes voluntarily, but often (this is the overlap with the second area) as a requirement of their labor or other important contractual commitments, such as insurance or social security (Levy 2015).** As recent critical data studies have shown (Eubanks 2018; O’Neil 2016), such forms of self-data collection provide the basis for new forms of discrimination and inequality.
> (p. 342)

# Expansão da apropriação para toda esfera da vida social, capitalização da vida

> Taken together, **these transformations represent something approaching the appropriation for capital of the whole domain of social life, and much of individual life too, an appropriation that constitutes the colonial moment of contemporary capitalism.** A key point to appreciate, however, is that the social relations that make this possible are very often not labor relations in the strict sense of the word. **Human life itself is being progressively capitalized in new ways whose detail, extent, and precision go far beyond earlier general predictions of the capitalization of life.**
> (p. 342)

# Entendimento a partir de uma leitura pós-colonial de Marx. Força de abstração da mercadoria como característica fundamental do capitalismo.

> This wider colonial appropriation and its social means can be better understood by drawing on the insights of Marx. Our use of Marx here is not orthodox Marxism, but starts rather from the postcolonial debates about the limits of Marxism’s interpretation of global history and its relative neglect of colonialism and slavery (Williams 1994).
> (p. 342)

> Thus, what unleashes commodification’s transformative effects on the social is the fact that labor (the everyday activity that has gone on since the beginning of time) acquires under industrial capitalism an abstract dimension (Postone 1998, 7). This reinterpretation of Marx for a “post-Marxist world” (as Postone puts it) emphasizes that **it is the abstracting force of the commodity, the possibility of transforming life process into “things” with value, that is, the fundamental characteristic of capitalism.** This makes even clearer our divergence from the Autonomist analysis, as a critical argument from Marx no longer needs to proceed primarily by exposing how labor or quasi-labor is exploited.
> (p. 342)

# Dado como abstração da vida humana. Relações de dado como relações sociais mercantilizadas.

> This insight from Marx’s social theory enables us to think about the new role of commodification in relations organized around data production and consumption. **“Data” are abstractions from the processes of human life.** Data are not abstracted from us automatically, but through social relations to which at some point, even if retrospectively, we are assumed to have consented. Meanwhile, **data are being increasingly commodified.** Industrial capitalism, according to Marx, changed society by transforming the universal human activity of labor into a social form with an abstract dimension (via the commodification of labor). **Today, data colonialism is changing society by transforming _human life_ into a new abstracted social form that is also ripe for commodification: data. The means for this transformation are—to reiterate—not labor relations, but, more broadly, commodified social relations, or, more succinctly, data relations.**
> (p. 343)

# Interações sociais cotidianas contribuem para criação de mais-valor como fatores de produção

O texto tem um aspecto um pouco fatalista ao cogitar a incorporação de toda vida pelo capital. Da mesma forma que o capital encontra novos terrenos para a acumulação primitiva, todo processo de captura também cria novas possibilidades para resistência e para linhas de fuga. Por mais difícil que possa parecer, a _imaginação_ política - ligado a uma tentativa permanente de concretizá-la - me parece um outro lado importante da _crítica_ no que diz respeito à transformação social. Pensar em Flusser ou Haraway em relação ao _jogo_.

> Put another way, **ordinary social interaction has come to contribute to surplus value as a _factor of production_, just like seed or manure (the examples are Marx’s).** But what human beings do when they are tracked and when data are extracted from them during social interactions is not a new type of labor, although it can be appropriated, abstracted, and commodified all the same. The implications of this therefore extend beyond labor to many other aspects of life which until now were not regarded as economic “relations” at all, but come to be incorporated within a vastly expanded production process. These new types of social relations implicate human beings in processes of data extraction, but in ways that do not _prima facie_ seem extractive. That is the key point: the audacious yet largely disguised corporate attempt to incorporate _all of life_, whether or not conceived by those doing it as “production,” into an expanded process for the generation of surplus value. **The extraction of data from bodies, things, and systems create new possibilities for managing _everything_. This is the new and distinctive role of platforms and other environments of routine data extraction. If successful, this transformation will leave no discernable “outside” to capitalist production: everyday life will have become directly incorporated into the capitalist process of production.**
> (p. 343)

# Tentativa de captura total em termos de dados

> Data colonialism means that new social relations (data relations, which generate raw inputs to information processing) become a key means whereby new forms of economic value are created. The value of those extractive processes depends on the comprehensiveness of the data generated. Nothing should be excluded.
> (p. 343-4)

> Before the Internet, as Bruce Schneier (2015, 27–8) notes, data sources about social life were limited to company customer records, responses to direct marketing, credit bureau data, and government public records (we might add insurance company data on their insured). Now, as already noted, **a vast and varied social quantification sector operating within a complex web of data processing functions extract data from everyday life at a depth far exceeding that found in earlier forms of social organization.**
> (p. 344)

# Dataficação ~ gamificação

> There are multiple forms of encouragement into data relations: from the rituals of routine self-tracking on platforms where we count our followers or likes,4 to exemplary performance (the Olympic athletes who tell us how they track themselves). **These “soft” nudges toward datafication converge in marketers’ favored strategy of gamification.** Gamification is an industry “term of art” based, as Julie Cohen (2016, 207–14) notes, on a “very specific behavioural model” that incentivizes “sharing” and loyalty to processes of data extraction from which new “social knowledge” can be generated.
> (p. 344)

# Duplos de dados [data doubles]

> Data scholars call these sets of data points our “data doubles.”5 Management theorists Alaimo and Kallinikos (2016) note, in an analysis of retail fashion platforms, that **data doubles are the building blocks for new “social objects”: data-rich constructs arranged in complex categories which corporations can target and influence.** Media platforms like Netflix are based on the structuring of content production and marketing around the data doubles produced through relentless data harvesting and processing, all of which suggests customization and convenience for the user.
> (p. 344)

> But there is nothing comforting about this. Even though the new social knowledge is produced through operations that bypass human beings, **it is actual human beings, not “doubles,” who are tethered to the discriminations that such knowledge generates.** It is a real person who gets offered a favorable price in the supermarket, an opportunity for social housing, or a legal penalty, all based on algorithmic reasoning.
> (p. 344)

# Fluxos máquina-para-máquina na produção de conhecimento social, o exemplo do IoT.

> Human inputs are only part of the territory that data colonialism seeks to annex to human capital. **Machine-to-machine connections significantly deepen the new web of social knowledge production.** Consider the fast-growing “Internet of Things.” The goal is clear: to install into every tool for human living the capacity to _continuously and autonomously_ collect and transmit data within privately controlled systems of uncertain security.
> (p. 344)

# Colonização do eu [self] pelas práticas digitais. Ameaça sobre as condições básicas de autonomia humana.

> In the hollowed out social world of data colonialism, **data practices invade the space of the self by making tracking a permanent feature of life, expanding and deepening the basis on which human beings can exploit each other.** The bare reality of the self as a self6 comes to be at stake. It is the minimal integrity of human life that must be protected. This reality, which each subject can recognize in each other, cannot be traded away without endangering the basic conditions of human autonomy.
> (p. 344-5)

# Capitalismo como ponto de referência subjetiva e fechamento das próprias possibilidades de transformação

> Underlying such variations, **capitalism affirms the uniquely identifiable _reference-point_ on which all notions of the self rest; it is this “self” whose market potential gets traded in proxy form.** But, at the same time, capitalism erodes a core element in the _content_ of that identified self whose continuity (and change) we value through time. **By installing automated surveillance into the space of the self, we risk losing the very thing that constitutes us as selves at all, that is, the open-ended space where we continuously transform over time.** What needs defending is _not_) individualistic self-rule, but rather the minimal, socially grounded integrity of the self without which we do not recognize ourselves _and others_ as selves at all.
> (p. 345)

# Defesa de uma integridade mínima do eu para qualquer filosofia emancipatória. Vida rastreável como vida despossuída [dispossessed].

> This minimal integrity of the self is valued by all philosophical traditions. The integrity of the self as the entity that can make and reflect on choices in a complex world is essential to all Western liberal notions of freedom.7 **But the self’s minimal integrity is essential to a philosophy of liberation intended to operate beyond the dominance of Western models of power,** such as that of Argentinian-Mexican philosopher Enrique Dussel (1985) which emphasizes the “natural substantivity of a person,” the basic “fact that each individual is distinct and not merely different”?8 The expansion of data colonialism is a problem for all human subjects, indeed for human development as such. **A continuously trackable life is a dispossessed life, no matter how one looks at it. Recognizing this dispossession is the start of resistance to data colonialism.**
> (p. 345)

# Pensamento decolonial em Quijano e sua validade para pensar a situação atual

> At this point, we can learn much by recalling the vision of the late Peruvian sociologist Aníbal Quijano. To Quijano, **the point was not merely to move past the colonial through the “postcolonial,” but to challenge fundamentally colonialism’s legitimacy through “decolonial” ways of thinking.** Quijano focused on the legacy of historic colonialism, but his diagnosis is of enormous importance for addressing data colonialism too. What decolonial thinking in particular can help us grasp is that colonialism—whether in its historic or new form—can only be opposed effectively if it is attacked at its core: **the underlying rationality that enables continuous appropriation to seem natural, necessary and somehow an enhancement of, not a violence to, human development.**
> (p. 345)

# Crítica de Quijano sobre a reivindicação de absoluta universalidade no pensamento colonial

> **What must be abandoned is the claim to absolute universality that Quijano sees as characteristic of European modernity, and which we find reproduced in data colonialism, and its logics of universal data extraction and management of human beings through data.** The force of Quijano’s point remains, even when, in the bipolar world of U.S.–China data domination, colonialism starts to operate well beyond the confines of European traditions.
> (p. 346)

# Visão de totalidade no colonialismo de dados, supressão de visões alternativas (entendidas como resistência)

> We reach here the core of what is wrong with data colonialism’s new order: its vision of totality. Yet the logic of Big Data is not the only vision of human order that is possible:
>
> > Outside the “West,” virtually in all known cultures . . . all systematic production of knowledge is associated with a perspective of totality. But in those cultures, the perspective of totality in knowledge includes the acknowledgement of the heterogeneity of all reality; of the irreducible, contradictory character of the latter; of the legitimacy, i.e., the desirability of the diverse character of the components of all reality—and therefore, of the social. The [better, alternative] idea of social totality, then, not only does not deny, but depends on the historical diversity and heterogeneity of society, of every society. In other words, it not only does not deny, but it requires the idea of an “other”—diverse, different. (Quijano 2007, 177, emphasis added)
>
> **It is exactly this alternative vision of order and totality that datafication denies, as it categorizes subjects and builds societies toward total algorithmic control. And it is exactly this alternative vision that resistance to data colonialism must affirm.**
> (p. 346)

# Visão alternativa ao colonialismo de dados passa por entendê-lo como um processo colonial

> The practical starting-point for resistance to data colonialism is a vision that, until twenty years ago, would have been indisputable, but now, strangely, appears counterintuitive to many. **This vision rejects the idea that the continuous collection of data from human beings is natural, let alone rational; and so rejects the idea that the results of data processing are a naturally occurring form of social knowledge, rather than a commercially motivated form of extraction that advances particular economic and/or governance interests.** Rejecting data colonialism does not mean rejecting data collection and use in all its forms. **But it does mean rejecting the form of resource appropriation and accompanying social order that most contemporary data practice represents. A useful first step is to name such practice as the colonial process that it surely is.**
> (p. 346)
