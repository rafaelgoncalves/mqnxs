---
title: "Deleuze e o virtual"
date: 2024-05-13
path: "/deleuze-virtual"
category: fichamento
author: Rafael Gonçalves
featuredImage: "deleuze.jpg"
srcInfo: <a href="https://machinedeleuze.wordpress.com/2021/05/26/gilles-deleuze-um-misticismo-ateu-por-rene-scherer-2/">Gilles Deleuze</a>
tags: 
 - Gilles Deleuze
 - Éric Alliez
 - David Hume
 - Henri Bergson
 - Friedrich Nietzsche
 - Baruch Espinosa
 - Immanuel Kant
 - Gottfried Leibniz
 - individuação
 - cristal
 - virtual
 - plano de imanência
 - tempo

published: true
---

Fichamento dos textos "Deleuze filosofia virtual" de Éric Alliez e "O atual e o virtual" de Gilles Deleuze (anexo)[^1]

[^1]: Alliez, Éric. **Deleuze: filosofia virtual.** Editora 34, São Paulo, SP. 1996.

# Gilles Deleuze: uma vida filosófica

> Versão modificada da palestra que encerrou a homenagem organizada pelo _Colégio Internacional de Estudos Filosóficos Transdisciplinares_ em 5 de dezembro de 1995, no Rio de Janeiro (Centro Cultural Banco do Brasil): "Gilles Deleuze: uma vida filosófica".

# Deleuzianismo

> O que pretendo aqui fazer, sumariamente, é montar e desmontar um paradoxo com o qual de um modo ou de outro se haverá defrontado todo leitor, amador ou experimentado, de Gilles Deleuze. Pois se é incontestável que os estudos monográficos sobre Hume, Bergson, Nietzsche, Kant ou Espinosa propõem uma verdadeira gênese do pensamento deleuziano, não é menos verdade que a relação de _duplicação_ que Deleuze haverá mantido com a história da filosofia -- ver o Prólogo sempre citado de _Difference et répétition_: "Seria preciso que a resenha em história da filosofia atuasse como um verdadeiro duplo, e que comportasse a modificação máxima própria do duplo" -- acaba por semear confusão, não sobre a identidade filosófica de seu pensamento (uma "filosofia da diferença" segundo a definição mais genérica; ou, mais rigorosamente, uma "filosofia do acontecimento"), _mas quanto à prática e à realidade dessa filosofia que não tem de resto outra questão que não a do pensamento e das imagens do pensamento que a animam.
> (p. 11)

> {...} **sob que condições é possível afirmar que o _discurso indireto livre_ a que recorre Deleuze para constituir o espaço diferencial de sua obra** -- "como um muro de pedras livres, não cimentadas, onde cada elemento vale por si mesmo, e todavia em relação aos outros" ou "um _patchwork_ de continuação infinita, de ligação múltipla" ("Bartleby ou la formule") -- **é criador de um pensamento novo e de uma nova imagem do pensamento: o _deleuzianismo_?**
> (p. 12)

# Interpretar Deleuze como aquele que apreende puros estados intensivos nos clássicos de suas monografias

> A segunda {maneira}, prática ou empírica, consiste em **apreender nas monografias filosóficas aquilo que Deleuze não faz voltar nem seleciona _como puros estados intensivos da força anônima do pensamento senão para afirmar a transmutação da filosofia enquanto tal._** Quandoem nome da anárquica diferença a filosofia empreende a exclusão de todos os princípios transcendentes quepode haver encontrado em sua história para se adaptar às Formas de Deus, do Mundo e do Eu [Moi] (centro, esfera e círculo: “tríplice condição para não se po-der pensar o acontecimento”2); **quando a filosofia afirma a imanência como a única condição que lhe permite recriar seus conceitos como “as próprias coisas, masas coisas em estado livre e selvagem, para além dos‘predicados antropológicos’”.**
> (p. 13)

> A optar por este segundo método, em que se trata **menos de potencializar as filosofias (formalizando-as) do que de virtualizá-las (e atualizá-las), consoante uma “troca perpétua entre o virtual e o atual” que define oplano de imanência enquanto tal**, dever-se-á necessariamente partir, por razões que não são apenas de cronologia, do encontro de Deleuze com o empirismo. Afinal, como o faria um autor de _science-fiction_, **o empirista não trata precisamente o conceito “como obje-to de um encontro, como um aqui-e-agora, ou antes como um _Erewhon_ de onde saem, inesgotáveis, os ‘aqui’e os ‘agora’ sempre novos, diversamente distribuídos”** — conforme escreve Deleuze no mesmo Prólogo? O Empirista, ou o grande Experimentador.
(p. 14-5)

# Hume (p. 17-9)

> PROPOSIÇÃO I: _A filosofia deve constituir-se comoteoria do que fazemos, não como a teoria do que é, poiso pensamento só diz o que é ao dizer o que faz: re-cons-truir a imanência substituindo as unidades abstrataspor multiplicidades concretas, o É de unificação peloE enquanto processo ou devir (uma multiplicidade paracada coisa, um mundo de fragmentos não-totalizáveiscomunicando-se através de relações exteriores)_
> (p. 19)

# Bergson (p. 20-3)

> ROPOSIÇÃO II: _A filosofia é indissociável de umateoria das multiplicidades intensivas à medida que aintuição enquanto método é um método antidialéticode busca e de afirmação da diferença no jogo do atuale do virtual._
> (p. 23)

# Nietzsche (p. 24-5)

> PROPOSIÇÃO III: _Se a afirmação do múltiplo é aproposição especulativa e a alegria do diverso a pro-posição prática, é preciso afirmar a filosofia como essepensamento nômade que cria conceitos como manei-ras de ser e modos de existência._
> (p. 25)

# Espinosa (p. 26-7)

> PROPOSIÇÃO IV: _Ética do Ser-Pensamento, ética dasrelações que opõem as potências da vida às doutrinasdo juízo, a filosofia é uma onto-etologia à medida queseus conceitos formam mundos possíveis e acontecimen-tos presos no movimento de um infinito virtual-real._
> (p. 27)

# Kant (p. 28-32)

> ESCÓLIO {Comentário, explicação gramatical ou crítica sobre os autores antigos, particularmente da Grécia} I: _De um ponto de vista filosófico, a história da filosofia só vale à medida que começa a intro-duzir tempo filosófico no tempo da história. Questãode devires que extraem a história de si mesma, histó-ria universal de um princípio de razão contingente, elapoderá assim ser concebida como o meio onde se nego-cia o cruzamento necessário da filosofia com a históriatout court, todavia também com as ciências e as artes._
> (p. 32)

# Leibniz (p. 33-5)

> ESCÓLIO 2: _Não há história filosófica da filosofiasem que se desenvolvam as filosofias virtuais que dra-matizam um jogo de conceitos como expressão do jo-go do mundo. Ela não tem tal ou qual filosofia comoobjeto, mas ponto de vista, pura efetividade que com-preende sua efetuação real, (trans-)histórica, como ainflexão primeira e a dobradura original de uma idea-lidade em si mesma inseparável de uma variação infi-nita. Tal é o fundamento do perspectivismo deleuziano:a Dobra como operador do Múltiplo, que do pontodessa imanência singulariza a individuação do pensa-mento em cada uma das dobras do mundo._
> (p. 35)

# Filosofia virtual em Deleuze

> Como uma aranha sempre refazendo sua teia, **Deleuze extrai, seleciona de cada um de “seus” filósofosum universo virtual de conceitos que dobra sobre um mundo real de forças, de maneira que eles constitui-rão os únicos “sujeitos” de sua filosofia** (princípio altruísta de toda leitura generosa, já que nunca se é tãobem servido quanto por seus outros), destinados a serem investidos como heterônimos, intercessores, personagens conceituais que entram em ressonância num teatro multiplicado onde a dança das máscaras leva apotência do falso a um grau que se efetua não maisna forma (é o falsário) mas na transformação: “Mistério de Ariadne segundo Nietzsche”, eis o profundo nietzscheísmo de Deleuze em seu uso dos nomes próprios, nos quais imprime um verdadeiro devir-conceito. **A heterogênese e a transmutação deleuziana dão-se assim como (ou melhor: dão-nos) a ontogênese de uma filosofia-mundo que investe o plano de imanência ou de univocidade como campo de experiência radical de uma sobrefísica livre de toda Forma, crítica de todas as formas essenciais, substanciais ou funcionais, crítica de todas as formas de transcendência (inclusive em sua última figura, fenomenológica, quando chega o momento de pensar a transcendência no interior do imanente), pela qual haverá “passado” toda a história da filosofia em sua colocação em variação contínua.** Daí o caráter único da afirmação deleuziana da filosofia como sistema. Com efeito, “o sistema não deve apenas estar em perpétua heterogeneidade, ele deve ser uma heterogênese, o que, parece-me,nunca foi tentado” (grifo meu)
> (p. 37-8)

> Toda uma pragmática do conceito como ser real, volume absoluto, superfície auto-portadora, cristaliza-ção e coalescência, dobra do cérebro sobre si mesmo, micro-cérebro..., toda uma “maquínica” do pensamento será assim mobilizada para fazer o múltiplo (pois “é preciso um método que efetivamente o faça”), tomar como sujeito o virtual (“a atualização do virtual é asingularidade”) e **responder enfim à questão “o que é a filosofia?” (“a filosofia é a teoria das multiplicidades”) — quando chega a velhice, e a hora de falar concretamente, no ponto singular onde o conceito e a criação se reportam um ao outro na grande identidade EXPRESSIONISMO = CONSTRUTIVISMO.**
> (p. 40)

---

# Filosofia como teoria das multiplicidades: objeto atual (partículas, percepções) e imagem virtual (efêmeros, lembranças)[^2]

[^2]: Gilles Deleuze. "O atual e o virtual". Em: Éric Alliez. **Deleuze: Filosofia Virtual** (trad. Heloísa B.S. Rocha). Editora 34, São Paulo, SP. 1996. pp.47-57.

> **A filosofia é a teoria das multiplicidades. Toda multiplicidade implica elementos atuais e elementos virtuais. Não há objeto puramente atual. Todo atual rodeia-se de uma névoa de imagens virtuais.** Essa névoa eleva-se de circuitos coexistentes mais ou menos extensos, sobre os quais se distribuem e correm as ima-gens virtuais. É assim que uma partícula atual emite eabsorve virtuais mais ou menos próximos, de diferentes ordens. **Eles são ditos virtuais à medida que sua emissão e absorção, sua criação e destruição acontecem num tempo menor do que o mínimo de tempo contínuo pensável, e à medida que essa brevidade os mantém, conseqüentemente, sob um princípio de incerteza ou de indeterminação.** Todo atual rodeia-se de círculos sempre renovados de virtualidades, cada umdeles emitindo um outro, e todos rodeando e reagin-do sobre o atual (“no centro da nuvem do virtual está ainda um virtual de ordem mais elevada... cada partícula virtual rodeia-se de seu cosmo virtual, e cada uma por sua vez faz o mesmo indefinidamente...”). Em virtude da identidade dramática dos dinamismos, uma percepção é como uma partícula: **uma percepção atual rodeia-se de uma nebulosidade de imagens virtuais que se distribuem sobre circuitos moventes cada vez mais distantes, cada vez mais amplos, que se fazem e se desfazem. São lembranças de ordens diferentes: diz-se serem imagens virtuais à medida que sua velocidade ou sua brevidade as mantém aqui sob um princípio de inconsciência.**
> (p. 49-50)

# Relação entre o atual e o virtual. O plano de imanência. Singularidade (virtual) e individualidade (atual).

> As imagens virtuais são tão pouco separáveis doobjeto atual quanto este daquelas. **As imagens virtuais reagem portanto sobre o atual. Desse ponto de vista, elas medem, no conjunto dos círculos ou em cada círculo, um continuum, um spatium determinado em cada caso por um máximo de tempo pensável.** A esses círculos mais ou menos extensos de imagens virtuais correspondem camadas mais ou menos profundas do objeto atual. **Estes formam o impulso total do objeto: camadas elas mesmas virtuais, e nas quais o objeto atualse torna por sua vez virtual. Objeto e imagem são ambos aqui virtuais, e constituem o plano de imanência onde se dissolve o objeto atual.** Mas o atual passou assim por um processo de atualização que afeta tanto a imagem quanto o objeto. **O continuum de imagens virtuais é fragmentado, o spatium é recortado conforme decomposições regulares ou irregulares do tempo. E o impulso total do objeto virtual quebra-se em forças que correspondem ao continuum parcial, em velocidades que percorrem o spatium recortado. O virtual nunca é independente das singularidades que o recortam e dividem-no no plano de imanência.** Como mostrou Leibniz, a força é um virtual em curso de atualização, tanto quanto o espaço no qual ela se desloca. O plano divide-se então numa multiplicidade de planos, segundo os cortes do continuum e as divisões do impulso que marcam uma atualização dos virtuais. **Mas todos os planos formam apenas um único, segundo a via que leva ao virtual. O plano de imanência compreende a um só tempo o virtual e sua atualização, sem que possa haver aí limite assimilável entre os dois. O atual é o complemento ou o produto, o objeto da atualização, mas esta não tem por sujeito senão o virtual. A atualização pertence ao virtual.** A atualização do virtual é a **singularidade**, ao passo que o próprio atual é a **individualidade** constituída. O atual cai para forado plano como fruto, ao passo que a atualização o reporta ao plano como àquilo que reconverte o objeto em sujeito.
> (p. 50-1)

# Desdobramento expansivo da virtualidade a partir do atual x aproximação atual-virtual (cristal). Atualização (singularização) x cristalização (individuação).

> Consideramos até o momento o caso em que um atual rodeia-se de outras virtualidades cada vez mais extensas, cada vez mais longínquas e diversas: **umapartícula cria efêmeros, uma percepção evoca lembranças**. Mas o movimento inverso também se impõe: **quando os círculos se estreitam, e o virtual aproxima-se do atual para dele distinguir-se cada vez menos. Atinge-se um circuito interior que reúne tão somente o objeto atual e sua imagem virtual: uma partícula atual tem seu duplo virtual, que dela se afasta muito pouco; a percepção atual tem sua própria lembrança como uma espécie de duplo imediato, consecutivo ou mesmo simultâneo.** Com efeito, como mostrava Bergson, a lembrança não é uma imagem atual que se formaria após o objeto percebido, mas a imagem virtual que coexiste com a percepção atual do objeto. **A lembrança é a imagem virtual contemporânea ao objeto atual, seu duplo, sua “imagem no espelho”.** Há também coalescência e cisão, ou antes **oscilação, perpétua troca entre o objeto atual e sua imagem virtual: a imagem virtual não pára de tornar-se atual, como num espelho que se apossa do personagem, tragando-o e deixando-lhe, por sua vez apenas uma virtualidade, à maneira d’_A dama de Xangai_ {filme}.** A imagem virtual absorve toda a atualidade do personagem, ao mesmo tempo que o personagem atual nada mais é que uma virtualidade. **Essa troca perpétua entre o virtual e o atual define um cristal.** É sobre o plano de imanência que aparecem os cristais. O atual e o virtual coexistem, e entram num estreito circuito que nos reconduz constantemente de uma outro. **Não é mais uma singularização, mas uma individuação como processo, o atual e seu virtual. Não é mais uma atualização, mas uma cristalização.** A pura virtualidade não tem mais que se atualizar, uma vez queé estritamente correlativa ao atual com o qual formao menor circuito. Não há mais inassinalabilidade doatual e do virtual, mas indiscernibilidade entre os doistermos que se intercambiam.  
> (p. 53-4)

# Atual e virtual no Tempo

> Objeto atual e imagem virtual, objeto tornado virtual e imagem tornada atual: são essas as figuras quejá aparecem na óptica elementar. Mas, em todos os casos, **a distinção entre o virtual e o atual corresponde à cisão mais fundamental do Tempo, quando ele avança diferenciando-se segundo duas grandes vias: fazer passar o presente e conservar o passado.** O presente é um dado variável medido por um tempo contínuo, isto é, por um suposto movimento numa única direção: o presente passa à medida que esse tempo se esgota. **É o presente que passa, que define o atual. Mas o virtual aparece por seu lado num tempo menor doque aquele que mede o mínimo de movimento numa direção única. Eis por que o virtual é “efêmero”. Mas é também no virtual que o passado se conserva, já queo efêmero não cessa de continuar no “menor” seguinte, que remete a uma mudança de direção. O tempo menor do que o mínimo de tempo contínuo pensável numa direção é também o mais longo tempo, mais longo do que o máximo de tempo contínuo pensável em todas as direções.** O presente passa (em sua escala), ao passo que o efêmero conserva e conserva-se (na sua escala). Os virtuais comunicam-se imediatamente por cima do atual que os separa. **Os dois aspectos do tempo, a imagem atual do presente que passa e a imagem virtual do passado que se conserva, distinguem-se na atualização, tendo simultaneamente um limite inassinalável, mas intercambiam-se na cristalização até setornarem indiscerníveis, cada um apropriando-se dopapel do outro.**
> (p. 54-5)

# Duas escalas do virtual (como outras coisas em vastos circuitos e como próprio virtual do atual nos menores circuitos). Indivíduo constituído (pontos ordinários) x individuação em ato (pontos relevantes)

> A relação do atual com o virtual constitui sempreum circuito, mas de duas maneiras: **ora o atual remetea virtuais como a outras coisas em vastos circuitos, onde o virtual se atualiza, ora o atual remete ao virtual como a seu próprio virtual, nos menores circuitos onde o virtual cristaliza com o atual. O plano de imanência contém a um só tempo a atualização como relação do virtual com outros termos, e mesmo o atual como termo com o qual o virtual se intercambia.** Em todos os casos, a relação do atual com o virtual não é a que se pode estabelecer entre dois atuais. Os atuais implicam indivíduos já constituídos, e determinaçõespor pontos ordinários; ao passo que **a relação entre o atual e o virtual forma uma individuação em ato ouuma singularização por pontos relevantes a serem determinados em cada caso.**
> (p. 55-6)
