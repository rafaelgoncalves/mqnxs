---
title: "MEOT: Apresentação (1958)"
date: 2021-05-22
category: fichamento
path: "/apresentacao-simondon"
tags: ["MEOT", "Gilbert Simondon", "cultura", "objetos técnicos", "fichamento"]
featuredImage: "../../images/gears.jpg"
srcInfo: Gears por <a href="https://www.flickr.com/photos/29722415@N07/">Marvin Hooley</a>
published: true
---

Fichamento da apresentação de 1958 presente na edição brasileira de MEOT[^1].

## Objetivo do livro: introduzir na cultura um conhecimento sobre os objetos técnicos (OT)

> O livro intitulado _Do modo de existência dos objetos técnicos_ almeja introduzir na cultura u conhecimento adequado dos objetos técnicos, considerados em três níveis: elementos, indivíduos e conjuntos.
> (p. 39)

## Relação mitológica (alienação) x relação verdadeira (conscientização)

> Em nossa civilização, há um hiato entre as atitudes suscitadas no homem pelo objeto técnico e a verdadeira natureza desses objetos.
> (p. 39)

> Dessa relação inadequada e confusa resulta, no comprador, no construtor e no operador, um onjunto de valorizações e desvalorizações mitológicas.
> (p. 39)

> Para substituir essa relação inadequada por uma relação verdadeira, é preciso haver uma conscientização do modo de existência dos objetos técnicos.
> (p. 39)

## 3 etapas da conscientização

> A primeira busca apreender a gênese dos objetos técnicos: [**eles não devem ser vistos como seres artificiais**]. O sentido de sua evolução é uma concretiação. Um objeto técnico primitivo é um sistema abstrato de funcionamentos parciais isolados, sem uma base comum de existência, sem reciprocidade causal, sem ressonância interna. Um objeto técnico aperfeiçoado é um objeto individualizado, no qual cada estrutura é plurifuncional, sobredeterminada; cada estrutura existe nele não apenas como órgão, mas como corpo, como meio, como base para as outras estruturas.
> (p. 39)

Progresso como relaxação e não continuidade

> O verdadeiro progresso dos objetos técnicos se efetua por um esquema de relaxação, não de continuidade: há uma conservação da tecnicidade como informação através dos sucessivos ciclos evolutivos.
> (p. 40)

> A segunda etapa considera a relação entre o homem e o objeto técnico, no nível do indivíduo, de um lado, e no dos conjuntos, de outro. O modo de acesso do indivíduo ao objeto técnico é _menor_ ou _maior_. O modo menor é aquele que convém ao conhecimento da ferramenta e do instrumento; é primitivo, porém adequado a esse nível de existência da tecnicidade na forma de ferramentas ou instrumentos; faz do homem um portador de ferramentas, de acordo com uma aprendizagem concreta, uma espécie de simbiose instintiva do homem com o objeto técnico usado em determinado meio, segundo a intuição e o conhecimento implícito, quase inato. O modo maior supõe uma conscientização dos esquemas de funcionamento: é politécnico. A _Encyclopédie_ de Diderot e d'Alembert ilustra a passagem do modo menor para o modo maior.
> (p. 40)

> No _nível dos conjunto_, a consciência que o grupo adquire de sua relação com os objetos técnicos traduz-se pelas diversas modalidades da ideia de progresso, que são os juízos de valor formulados pelo grupo sobre o poder[**?**] que esses objetos possuem de fazer o grupo evoluir: o progresso otimista do século XVIII corresponde a uma conscientização da melhora dos elementos; o progresso pessimista e dramático do século XIX corresponde à substituição do indivíduo humano portador de ferramentas pelo indivíduo-máquina [41] bem como à inquietalçao que resulta dessa frustralção. Por último, resta elaborar uma nova ideia de progresso que corresponda à descoberta da tecnicidade no nível dos conjuntos de nossa época, graças a um aprofundamento da teoria da informação e da comunicação: a verdadeira natureza do homem não é ser portador de ferramentas - e, portanto, concorrente da máquina -, mas inventor de objetos técnicos capazes de resolver problemas de compatibilidade entre as máquinas num conjunto.
> (p. 40-41)

Compatibilidade, margem de indeterminação e máquina aberta.

> Mais do que governá-las, ele as compatibiliza, é agente e tradutor de informações de máquina para máquina, intervindo na margem de indeterminação contida no funcionamento da máquina aberta, capaz de receber informação. O homem constrói a significação das trocas de informação entre máquinas.
> (p. 41)

Não existe máquina de todas as máquinas (automatismo puro).

> O automatismo puro, que exclui o homem e imita o ser vivo, é um mito não correspondente ao mais alto nível possível de tecnicidade: não existe a máquina de todas as máquinas.
> (p. 41)

Rompimento do mundo mágico primitivo: tecnicidade, religiosidade e estética.

> Por fim, a terceira fase da conscientização substitui o objeto técnico _no conjunto do real_ , procurando conhecê-lo em sua essência, segundo uma gênese da tecnicidade. A hipótese básica da doutrina filosófica usada consiste em supor a existência de um modo primitivo de relação do homem com o mundo, que é o modo mágico: de uma ruptura interna dessa relação saem duas fases simultâneas e opostas - a fase técnica e a fase religiosa; a tecnicidade é a mobilização das funções figurais, o levantamento dos pontos-chave da relação do homem com o mundo; a religiosidade refere-se, ao contrário, ao respeito pelas funções de fundo: é o apego à totalidade em seu fundo. _Essa relação defasada [42] do homem com o mundo recebe uma mediação imperfeita da atividade estética_ : o pensamento estético conserva uma nostalgia da relação primitiva do homem com o mundo, é o neutro entre fases opostas.
> (p. 41-2)

Caráter parcial da neutralidade (capacidade de mediação) do objeto estético, por conta de sua concretude-funcionalidade.

> Mas seu caráter concreto de construtor de objetos limita seu poder de mediação, pois o objeto estético perde sua neutralidade e, por conseguinte, seu poder de mediação, ao procurar tornar-se funcional ou sagrado.
> (p. 42)

Pensamento filosófico como neutro (primitivo e elaborado).

> Somente no nível do pensamento que é, a um tempo, o mais primitivo e o mais elaborado de todos - o pensamento filosófico - pode intervir uma mediação verdadeiramente _neutra_ , _equilibrada_ , por ser _completa_ entre fases opostas.
> (p. 42)

[^1]: SIMONDON, G. Apresentação (1958). **Do modo de existência dos objetos técnicos**. Editora Contraponto, 2020. 
