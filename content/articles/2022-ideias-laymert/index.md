---
title: Por uma sociologia não-autocrática das máquinas
date: 2024-05-06
path: "/sociologia-nao-autocratica-das-maquinas"
category: fichamento
author: Rafael Gonçalves
featuredImage: "./ideias.jpg"
srcInfo: <a href="https://periodicos.sbu.unicamp.br/ojs/index.php/ideias/issue/view/1887">Capa Ideias 13 (00)</a>
tags:
- capitalismo
- cibernética
- tecnologia
- sociologia
- Laymert Garcia dos Santos
- Gilbert Simondon

published: true
---

Fichamento de "Por uma sociologia não autocrática das máquinas"[^1] de Laymert Garcia dos Santos.

[^1]: Santos, Laymert Garcia dos. 2022. “Por Uma Sociologia não autocrática Das máquinas”. _Ideias 13 (00)_. Campinas, SP:e022026. https://doi.org/10.20396/ideias.v13i00.8671667.

# Resumo

> Versão condensada, editada e revisada por Laymert Garcia dos Santos, de anotações realizadas por Pedro P. Ferreira em sala de aula, em um notebook, durante as três primeiras aulas (realizadas nos dias 02, 16 e 30 de março de 2011) da última disciplina que Laymert ministrou no Programa de Pós-Graduação em Sociologia (PPGS) do IFCH/Unicamp: a “SO141 – Tópicos Especiais em Trabalho, Cultura e Ambiente II”, que ele dedicou exclusivamente às ideias de Gilbert Simondon, em especial à sua filosofia da individuação técnica e coletiva.

# Simondon

> Para mim, Simondon ajuda a entender o mundo em que vivemos, e também como vivemos nele. Muitos filósofos importantes leram Simondon (entre eles Herbert Marcuse, Gilles Deleuze e Jean-Fran-çois Lyotard). O pensamento de Simondon é absolutamente neces-sário para a Sociologia contemporânea, e a sua importância para as Ciências Humanas e Sociais contemporâneas só cresce.
> (p. 2)

# A máquina, entre o humano e o não humano. Sociedade de humanos e não-humanos. [Maquínica].

> O que isso importa para as Ciências Humanas? Os sociólogos pensam que vivemos em uma sociedade de humanos. Simondon, no entanto, sabia que não vivemos numa sociedade de humanos, mas  sim  de  humanos  com  máquinas;  sabia  que  é  preciso  olhar,  não  para  os  humanos  em  oposição  aos  não-humanos,  mas  para  as  maquinações  que  passam  de  um  para  o  outro.  **O  fato  de  que  podemos pensar em maquinações diferentes para humanos e para não-humanos não significa que não possamos pensar, ao mesmo tempo,  num  nível  comum  a  ambos.  Por  isso,  para  entender  a  sociedade contemporânea, é preciso pesquisar a relação humano-máquina  (incluindo  aí  as  relações  humano-humano,  humano-máquina,  máquina-humano  e  máquina-máquina).**  Não  interessa,  porém,  fazer  a  crítica  das  Ciências  Humanas,  bater  em  cachorro  morto.  O  interesse  não  está  mais  em  explicações  (sobre  o  que  já  aconteceu), mas sim em implicações (sobre o que está por vir).
> (p. 2)

# MEOT e ILFI

> Como conceber, então, as Ciências Humanas após a morte do Homem? Seria preciso pensar em conjunto os modos de existência da  máquina  e  do  ser  humano.  A  disciplina  não  se  prenderá,  portanto, aos moldes das Ciências Humanas, pois propõe pensar o  deslocamento  do  pensamento  pós  virada  cibernética.  Vamos  trabalhar  sobretudo  com  as  duas  obras  principais  de  Simondon  (2005  e  2008):  _Du  mode  d’existence  des  objets  techniques_  [MEOT]  e _L’individuation  à  la  lumière  des  notions  de  forme  et  d’individuation_ [ILFI]. **São obras fundamentais, pois abordam a individuação no ser físico, no ser vivo e no ser humano, e pensam o modo de existência dos  humanos  e  das  máquinas  a  partir  de  uma  noção  inovadora  de  informação.**  O  fato  de  que  atualmente  tudo  é  cibernetizado,  digitalizado, destaca ainda mais a centralidade contemporânea da noção de informação.
> (p. 3)

# Objeto técnico entre o abstrato (pensado) e o concreto (efetivo). Modo próprio de leitura meta-objetiva.

> O  objeto  técnico  tem  uma  exis-tência  real,  mas  esta  existência  é,  ao  mesmo  tempo,  concreta  e  pensada;  é  um  pensamento  concretizado.  **A  técnica  é  a  interface  entre  o  abstrato  e  o  concreto;**  o  pensamento  está  dos  dois  lados.  Minha  sensação,  ao  ler  Simondon,  é  de  me  deparar,  ao  mesmo  tempo, com uma coisa muito concreta e muito abstrata. Como? **Ora,   a   própria   máquina   é   abstrata   e   concreta,   simultânea   e sucessiva, existe dentro de mim (na minha cabeça) e fora de mim (no  mundo).  O  que  existe  objetivamente,  lá  fora,  só  existe  pois  foi  pensado.**  Para  pensar  toda  a  complexidade  objetiva  da  técnica **é preciso pensar sobre esse pensamento, que é meta-objetivo; é preciso pensar sobre o modo de pensar sobre o objeto.** Então, é comum, na leitura de Simondon, você começar a ler uma frase, mas depois precisar recomeçar, por não ter ficado concreto para você. Aí está o obstáculo.
> (p. 3-4)

# Contexto: anos 50 -> pensar a informação (importância hoje)

> É  importante  lembrar  que  Simondon  escreveu  nos  anos  1950.  Desde  então,  o  processo  se  complexificou  infinitamente. Quando  Simondon  escreveu,  a  eletrônica  ainda  estava  restrita  ao  laboratório, não estava na vida cotidiana. Isso torna seu pensamento desatualizado?  É  interessante  que  a  resposta  a  esta  pergunta seja  negativa.  Como  ainda  podemos  nos  interessar  pelo  que  ele  escreveu? Ora, o fato é que ele ajuda a pensar inclusive aquilo que veio depois dele. Por um lado, existe uma enorme questão política em  jogo  (escravização).  Por  outro  lado,  estamos  atualmente  num  mundo  da  informação,  e  Simondon  pensou  justamente  o  mundo  da informação (e não o mundo da forma), esse substrato comum ao ser físico, ao ser vivo, ao ser psicossocial e ao ser técnico.
> (p. 4)

> Se  estamos  na  terceira  revolução  industrial,  precisamos  pensar  as  relações  entre  técnicas  e  tecnologias.  **A  sistematização  abstrata  dos  conhecimentos  e  das  técnicas  agenciados  hoje  já  fundam  uma  tecnologia  que  precisa  ser  pensada.** Eu  nunca  mais  tirei  Simondon  da  minha  cabeça  desde  1977,  pois  ele  pensou  sobre  os  problemas  que  são  os  nossos:  **o  papel  da  informação  na  individuação  de  todos  esses  tipos  de  seres.**  Daí  a  importância  contemporânea de Simondon para nós que vivemos em um mundo cada  vez  mais  controlado  por  meio  das  tecnologias  da  informa-ção. Ele percebeu que havia uma nova revolução em andamento. Ele   pensou   o   que   hoje   é   banal,   quando   ainda   não   existia.   É  uma  grande  capacidade  de  compreensão.  Espero  que  todos  se  apaixonem pelo pensamento simondoniano.
> (p. 4)

# Dificuldade: técnica

> Então,  digo  meio  brincando, que a discussão aqui será "técnica", pois precisaremos enfrentar as dificuldades para entender esse capítulo II da "Primei-ra Parte", no qual são desenvolvidas as categorias fundamentais de "elemento", "indivíduo" e "conjunto".
> (p. 5)

# Separação entre cultura e técnica

> A cultura costuma ser tão dissociada da técnica no Brasil, que são até dois Ministérios separados no governo. Gilberto Gil foi gênio por tentar aproximar os Ministérios da Cultura (MC) e da Tecno-logia  (MCT).  Depois  que  Gil  e  Juca  Ferreira  saíram  do  MC,  tudo  se  separou  novamente.3  Simondon  percebeu  como  a  concepção  francesa de cultura separa a técnica da cultura, e é importante no-tar  como  ele  voltou  para  a  _Encyclopédie_,  e  fez  uma  leitura  muito  especial dela, mostrando o seu papel e o seu valor político e social. **Simondon, na verdade, critica a especialização através da idéia de uma nova cultura.** Foi um corte com o Antigo Regime.
> (p. 5)

# Cultura técnica e alienação técnica

> Eu, por exemplo, não sei, e nem quero saber, como consertar o meu carro. Mas isso é ruim, pois estimula o automatismo. Esse Por uma sociologia não autocrática das máquinas|6|Idéias, Campinas, SP, v.13, 01-24, e022026, 2022era  o  conhecimento  que  a  criança  precisa  saber.  Saber  consertar  meu  carro  implicaria  em  conhecer  a  sua  mecânica,  mas  também  ter  a  sensibilidade  para  saber  o  que  eu  tenho  que  fazer.  No  meu  carro, quando eu escuto um barulhinho estranho, eu tento ignorar, aumento o som para não ouvi-lo, mesmo sabendo que mais para frente eu terei um problema ainda mais grave. Mas o que significa esse recalcamento do som que a máquina está fazendo, e portanto daquilo que ela está me dizendo? Eu não quero ouvi-la nunca, nem  quando  está  funcionando  (pois  deve  ser  silenciosa)  e  nem  quando estraga (pois finjo que o problema não existe). Eu nego a sua existência.
> (p. 5-6)

> No  objeto  estético,  damos  de  barato  que  é  preciso  conhe-cer  para  fruir.  Mas  quando  se  trata  do  objeto  técnico,  agimos  como  se  só  precisássemos  usar  o  objeto,  como  se  não  precisásse-mos  conhecê-lo. **Essa  dimensão  oculta  (não  pensada)  do  objeto  técnico  é  a  sua  ontologia,  o  seu  ser,  o  seu  movimento  interno,  a  direção do seu devir.** Ele só pode atender às necessidades humanas  (exteriores  a  ele)  se  elas  corresponderem  aos  seus  próprios  movimentos  internos.  Só  assim  ele  responde  e  atende.  **É  preciso  atentar  às  respostas  do  objeto  técnico  às  nossas  demandas.  Essas respostas são o resultado de uma história, de uma evolução.** Há uma relação entre a demanda humana externa e os movimen-tos internos do objeto técnico. Eu não posso pedir para um telefone dos anos 1950, que faça o que eu posso pedir para um _iPhone_. Ele não vai me responder. Isso não corresponde ao seu funcionamento interno.
> (p. 6-7)

> Os  exemplos  de  Simondon  são  centrais.  Além  disso,  **Simondon  extrai  pensamento  do  objeto  técnico**.  É  porque  ele  conhece  bem  o  modo  de  funcionamento,  e  as  transformações  nesse  modo  de  funcionamento,  que  ele  pode  passar  de  um  mecanismo  "x"  para  um "y", e dizer se tem ruptura ou continuidade, e enumerar toda uma série de nuanças e singularidades sobre essa passagem. É uma questão de falta de cultura de nossa parte, mas é também um efeito do aparente absurdo que é extrair pensamento do funcionamento. Isso, do ponto de vista metodológico, é fantástico.
> (p. 7)

> Esse pensamento não é, em termos simondonianos, _a priori_: **o pensamento é construído junto; não existe nada dado previamente; é da relação, do agenciamento (no  sentido  de  Gilles  Deleuze  e  Félix  Guattari),  da  articulação, que ele tira o pensamento.** Para fazer isso, ele precisa ter um olhar retrospectivo sobre a técnica anterior, olhando para trás, e **perceber o que é continuidade e o que é ruptura, quais são as categorias com as quais pensar o objeto técnico.** Ele extrai isso do próprio modo de existência, do funcionamento do objeto.
> (p. 7)

# Filosofia autocrática ocidental

> A  individuação  ocidental  é  a  da  ciência  autoritária.  Mas  os  problemas  gerados  pela  tecnologia  não  são  problemas  da  tecnologia,  e  sim  de  nossa  relação  com  ela.  Não  devemos  nos  oporàs  máquinas.  Esse  ressentimento  que  nutrimos  com  relação  às  máquinas  é  um  ponto  central,  como  prolegômeno  à  proposta  de  Simondon,  pois  mostra  como  ela  não  é  trivial,  não  é  fácil,  pois  é  anti-automática para nós. Para Philip K. Dick, por exemplo, em seu seu texto de não-ficção sobre a lógica dos andróides (Dick, 1976), o controle da máquina sobre o humano existe, mas devemos resistir. Donna Haraway (1991) está dizendo algo parecido, pois quer ser ciborgue, mas de oposição.
> (p. 7-8)

# Filosofia não-autocrática em Simondon

> Simondon  queria  construir  uma  filosofia  não-autocrática das máquinas. O filósofo notou que temos uma relação utilitária-instrumental  com  as  máquinas,  segundo  a  qual  elas  devem  estar  à  nossa  disposição. **Esta  é  uma  relação  marcada  pela  dominação,  do tipo senhor-escravo, sendo a máquina ora senhor (quando algo dá errado e ela nos domina), ora escravo (quando tudo dá certo e nós a dominamos).** No entanto, Simondon mostrou que, **encarar a máquina como escravo é a continuação da escravidão por outros meios.** O suposto problema das máquinas "_out of control_" ["fora de controle"]  (muito comum nas análises sociológicas e filosóficas) acaba legitimando a ideia de que precisamos dominá-las. Simondon afirmou que nossa relação com elas pode ser diferente, e muito melhor. **Para isso, é preciso construir uma filosofia não-autocrática das  máquinas,  através  do  estudo  dos  modos  de  existência  das  máquinas, dos humanos e de sua relação.**
> (p. 8)

> O importante para Simondon é a relação. **O seu pensamento é relacional e processual (apesar de falar em "essência"), e portanto desreifica o sujeito humano e o objeto tecnológico.** Ele relaciona arte (estética), tecnologia e cultura. Mas por que a estética é impor-tante para pensar o que está faltando na tecnologia? E por que é importante pensar isso? Ora, pois ocorre dominação.
> (p. 8)

> O  oposto  de  uma  relação de servidão seria uma que pensasse a especificidade do humano com relação à especificidade da máquina. Ou seja, todo o pensamento de Simondon gira em torno do problema do fantasma da dominação homem-máquina (um dominando o outro). Golem, Frankenstein,   Robocop,   toda   essa   linhagem   de   pensamento   segundo  a  qual:  ou  somos  dominados,  ou  devemos  dominar  as  máquinas.
> (p. 9)

> Esse é o nosso senso comum, Simondon não está exagerando.  Por  isso,  em  MEOT,  ele  fala  de  recalque,  que  é  diferente  de  repressão. **Na repressão você sabe que está sendo afetado, e sabe quem  está  afetando.  No  recalque  não.  É  da  natureza  do  recalque  que este seja ligado a algo desconhecido, obliterado, mas existente –  existente,  mas  inconsciente.  Simondon  está  falando  do  retorno  do  recalcado,  do  retorno  de  algo  que  existia,  que  estava  maquinando**  (no  sentido  de  Deleuze  e  Guattari),  produzindo  efeitos, mas que estava oculto. **A importância da distinção elemento-indivíduo-conjunto é trazer à tona o recalcado.** Temos uma relação mal resolvida  com  a  técnica.  Há  um  retorno  do  recalcado,  um  Iluminismo-Enciclopedismo,  no  Simondon,  mas  também  em  Sigmund  Freud.
> (p. 9)

> Se  nós  estamos  imitando  a  maquinação  da  máquina,  então  esse  é  um  problema  nosso  –  mais  ainda,  esse  é  "o"  nosso  problema  –,  não  das  máquinas.  **É  por  isso  que  precisamos  de  uma  filosofia  não-autocrática  das  máquinas.  Não  estamos  pensando suficientemente as maquinações extra-técnicas dos humanos, que interagem com as maquinações técnicas das máquinas.** Quem tem pensado sobre isso é o pessoal que pesquisa a questão da relação arte-tecnologia. Para um artista como Harun Farocki, por exemplo, a  questão  é  a  individuação  da  câmera  e  (junto  com)  a  nossa.  No  caso  dos  xamãs  yanomami,  por  sua  vez,  é  toda  uma  tecnologia  audiovisual  que  está  em  jogo.  Eles  podem  ser  chamados  de  simondonianos,   pois   querem   explorar-experimentar   o   virtual   enquanto tal, em lugar de sempre atualizar tudo aquilo do virtual a que eles tiverem acesso. **Simondon permite pensar essa diferença entre nós e eles: ver o xamã como um tecnólogo excepcional que acessa coisas que a ciência moderna nem desconfia que existem.**
> (p. 9)

# Computação no povo Tarabu (Bolívia)

> Quando você vê a mulher Jalq’a tecendo, nota que ela decide, na hora, o que vai tecer: a figura sai do fundo e volta; é altíssima computação, como se ela tivesse um software fabuloso na cabeça. É como o cartão perfurado do Jacquard – só que mais complexo, porque  neste  a  imagem  é  parada,  figura  e  fundo  são  destacados.  **Quando  não  há  essa  distinção,  há  um  vai-vem  constante:  a passagem da imagem está acontecendo na mão, na cabeça e no tecido, tudo junto no momento em que ela está sendo feita. Há aqui uma  complexidade  operatória.** Isso  estava  em  desaparecimento,  mas uma antropóloga chilena viabilizou um atelier para manter a prática, no qual pessoas podem comprar esses tecidos. A iniciativa foi  bem  sucedida,  e  é  interessante  notar  que,  diante  da  maior  independência econômica das mulheres (tradicionalmente, apenas as mulheres se dedicam a essa atividade), os homens começaram a tentar produzir estampas também, mas eles não conseguem juntar figura e fundo – fica tudo separado.
> (p. 10-1)

> O tecido Jalq’a é considerado artesanato; ninguém o conside-ra arte elaborada. Ademais, a tecelã terá que usar as mãos se quiser explicar  o  que  faz  –  ela  é  uma  artesã,  no  sentido  simondoniano.  No entanto, a questão da oposição entre um saber já teorizado e o saber do artesão (ligado diretamente ao conhecimento da matéria, operatório)  é  equivocada,  pois  não  é  oposição,  e  sim  modos  de  relação  com  a  técnica.  A  cultura  não  estava  separada  da  técnica  no  mundo  mágico. **É  isso  que  vai  fazer  Simondon  questionar  se   a   evolução   é   um   avanço. Nas   sociedades   originais-mági-cas – onde o primeiro tecnólogo é o xamã –, arte, técnica e cultura não estão separadas.  Assim, se os Yanomami seriam "crianças", na terminologia de Simondon, isso não é um julgamento de valor, e sim uma distinção conceitual. É de uma passagem do conhecimento operatório para um conhecimento teórico que se trata. O que veio antes não é pior do que o que veio depois. Tudo é fundamental.**
> (p. 11)

# Individuação-invenção x adaptação

> Há  uma  diferença  importante  entre  a  evolução  contínua  (adaptação)  e  a  evolução  descontínua  (individuação).  É  preciso  pensar na relação entre concretização e margem de indeterminação: **o  importante  na  concretização  do  objeto  técnico  é  transformar uma   tensão   negativa,   uma   incompatibilidade,   em   resolução.** Quando  isso  ocorre,  há  uma  mutação:  algo  que  era  incompatível  (forças   virtuais,   potenciais),   encontra   uma   solução   positiva.   É por aí, pela superação dos obstáculos, que as máquinas avançam e  evoluem. **Não  se  opõe  aqui  aperfeiçoamento  e  mutação  de  um  ser:  a  mutação  ocorre  quando  passamos  de  um  aperfeiçoamento  contínuo  para  um  aperfeiçoamento  descontínuo;  a  mutação  é  a  evolução.** Há  uma  linha  evolutiva,  mas  o  sentido  dessa  evolução  não é a adaptação darwiniana, **e sim a invenção**. O ato inventivo é fundamental para o ser técnico, pois este não existe na natureza, não existe sem aquele ato.
> (p. 12)

# Estados alterados de consciência

> Deleuze  fazia  muitas  experiências  com  estados  alterados  de consciência. Jean-Paul Sartre tomou anfetaminas para escrever _O ser e o nada_. Há toda uma linhagem de filósofos que só pensavam o que pensavam por conta dessas experiências. É comum também que surja um interesse pelo selvagem, pelo primitivo, como no caso do  Antonin  Artaud,  que  foi  tomar  peyote  com  índios  mexicanos.  O  interesse  pela  Arte  Bruta  (_art  brut_),  que  agora  voltou  a  crescer, também está ligado a isso. Esses agenciamentos aberrantes dizem alguma coisa sobre um plano da realidade que escapa à percepção habitual.
> (p. 12-3)

# Desvios de função

> Existem artistas que trabalham apenas com o desvio de função de máquinas: elas funcionam, mas de um jeito outro – fazem coisas absurdas. Jean Tinguely fez isso: pegou seu grande conhecimento de mecânica para fazer dele a não-produção. **Ele produziu não-produção, e o interessante é o barato das maquinações: é como se ele pegasse todos esses agenciamentos e os enlouquecesse.** Isso não é contrário ao que estamos estudando aqui: desvio de função é justamente a invenção, descontinuidade, reconfiguração do con-junto, um salto.
> (p. 13)

# Inventor: incompatibilidade -> coerência e consistência interna

> O  inventor  é  aquele  que  olha  para  um  incompatível,  vê  a  articulação de forças, e imagina uma alternativa, outra configuração, que promova consistência e coerência interna. **Mas o problema da invenção  embaralha  as  causalidades,  pois  o  objeto  concretizado  na  invenção  é  a  condição  de  sua  própria  concretização.  Todas  as  condições  para  uma  concretização  do  movimento  imanente  da técnica só estarão presentes quando surgir uma configuração que permita essa concretização; é uma conjunção.** O problema da técnica  não  é  técnico,  é  social  (num  sentido  amplo);  **as  condições  objetivas de concretização técnica são extratécnicas.**
> (p. 13)

# Buckminster-Fuller

> Seria  interessante  compararmos  a  metodologia  do  Simon-don,  para  pensar  a  evolução  dos  objetos  técnicos,  com  a  de  Ri-chard Buckminster-Fuller (1981), no livro _Critical path_ – livro este fundamental para pensar sobre tecnologia. Os dois estavam inte-ressados em ver essa evolução a partir da questão da informação, e  Buckminster-Fuller  também  se  interessou  profundamente  pela  cibernética. Mas Simondon foi um pensador, um filósofo, enquanto  Buckminster-Fuller  foi  um  inventor  que  fez  muitas  invenções,  algumas  que  só  estão  sendo  concretizadas  agora.  Buckminster-Fuller  sabia  que  cada  ramo  de  tecnologia  (linhagem  tecnológica)  tem um tempo de gestação, e que havia um hiato entre a invenção e  o  seu  "nascimento  social".  Ele  se  interessou  por  monitorar  esse  hiato, e percebeu que, na década de 70, tudo dispara. Não deve ser por  acaso  que  Buckminster-Fuller  e  Simondon  são  dessa  mesma  época, quando tudo dispara – **quando a informação se torna mais uma dimensão da matéria, quando a informação dispara a tecnologia.** O pensamento de Buckminster-Fuller é diferente, mas paralelo ao do Simondon. Acho que Simondon pensou de um jeito mais rigoroso, cartesiano, do que o Buckminster-Fuller. É legal ver como dois grandes pensadores da tecnologia pensaram esse movimento.
> (p. 14)

> Buckminster-Fuller  tem  ainda  um  lado  artista  que  é  fundamental  (_Critical  path_  era  livro  de  cabeceira  de  John  Cage).  Buckminster-Fuller teve uma crise aos 20 anos de idade – tentou se matar, ficou dois anos isolado estudando Albert Einstein e Leonardo da Vinci –, para depois influenciar toda a ciência contemporânea, como no caso das "Bucky balls" em nanotecnologia. Ele percebeu muito  cedo  que  teria  que  escolher  entre  fazer  dinheiro  ou  fazer  sentido. Ele escolheu fazer sentido, pois **se você quiser se revoltar contra  uma  ordem  social,  primeiro  precisa  saber  como  funciona  esta ordem.** Ele logo percebeu que, **para mudar as pessoas, é mais fácil  mudar  o  ambiente  no  qual  elas  estão,  do  que  a  consciência  delas.  Você  muda  a  cabeça  das  pessoas  mudando  o  ambiente;  a  transformação do ambiente transforma o ser humano. Quando ele descobriu isso, ele deixou de lado a política e focou na tecnologia.**
> (p. 14-5)

# O maestro (mecanólogo, sociólogo/psicólogo das máquinas) como mediador

> Aquele  que  pensa  "que  sociedade  é  essa  hoje"  é,  em  certa  medida, aquilo que Simondon chamou de "maestro", e também de "mecanólogo",  "sociólogo"  e  "psicólogo".  O  problema  do  maestro  não  é  conhecer  a  linguagem  dos  instrumentos  (apesar  de  isso  ser  geralmente  esperado).  **A  grande  diferença  entre  o  maestro  e  qualquer  outro  membro  da  orquestra  é  que,  em  lugar  de  tocar  um  instrumento,  ele  está  nos  entres,  em  meio  aos  que  tocam,  coordenando  esse  movimento  pelo  porvir,  pela  tendência:  ele  "puxa"  a  música;  ele  é  uma  pessoa  que  se  deixa  atravessar  por  todos  os  músicos;  ele  tem  que  se  responsabilizar  pelo  diálogo;  ele está captando o que cada um está fazendo; ele tem que estar, como o mecanólogo, ouvindo-vendo essa relação entre humanos e máquinas em todas as suas variantes humano-humano, humano-máquina   e   máquina-máquina.**  Isso   pode   parecer   banal,   mas   o que não me parece de forma alguma banal é **tirar o automatismo das nossas cabeças, isso que é condição para uma filosofia não autocrática  das  máquinas.**  Pois  podemos  entender  isso  aqui-agora,  mas  daqui  a  cinco  minutos  ter  uma  relação  autocrática  com  o  celular  ou  o  automóvel  –  ou  seja,  uma  relação  utilitária,  como  o  antigo  usava  o  escravo  –  na  qual  depois  de  usada,  a  máquina deve sair de cena.
> (p. 15)

# O processo de concretização (máquina primária > máquina concreta)

> É  preciso  pensar  a  ontologia  da  técnica,  o  modo  de  ser  do  objeto técnico, que é uma mistura de humano e natureza, humano e  não-humano.  Para  que  possamos  nos  entender  com  os  objetos  técnicos  –  e  _precisamos_ nos  entender  com  eles  –,  precisamos  compreender  que  eles  também  têm  uma  evolução,  e  que  ela  é  diferente  da  nossa. **Primeiro  existe  uma  analítica  da  máquina,  quando ela é pensada. Depois ela se concretiza, com sua evolução. Nesse  processo,  preciso  ouvir  a  máquina  –  tanto  na  medida  em  que  ela  concretiza  a  abstração,  quanto  na  medida  em  que  ela  me  diz  coisas  que  não  estavam  previstas  no  esquema  abstrato.  Ela  diz  coisas  que  não  estavam  previstas  no  meu  plano;  os  materiais  e as leis da natureza colocam para mim obstáculos e dificuldades. O  funcionamento  da  máquina  é  a  cobinação  entre  aquele  que  a  gente previu, e aquele que está acontecendo.**
> (p. 16)

> Primeiro a máquina é pensada; é primária; é "função + função + função..."; é uma organização das expectativas que eu tenho com relação  a  cada  parte  dessa  máquina,  pensada  separadamente,  como  elemento.  O  problema  aparece  quando  eu  junto  isso  tudo  em  um  todo  coerente.  **Um  _mero_  acúmulo  de  funções  não  é  uma  sinergia  de  funções.  A  máquina  sugere  modificações  do  plano, para que este se concretize. É preciso escutá-la. O técnico dialoga com a máquina, e não com um esquema abstrato.** O pensamento analítico inicial não escuta a máquina como indivíduo técnico, ele só é adequado ao nível do elemento. **É essa dimensão adicional aos elementos justapostos, esse n + 1 (os elementos mais algo, que é a sinergia),  que  permite  a  compreensão  da  máquina,  do  indivíduo  técnico.**  É  uma  só  concretização,  que  no  nível  dos  elementos  não  gera  problemas,  no  nível  dos  indivíduos  gera  problemas,  e  no  nível dos conjuntos permite uma possibilidade de resolução.
> (p. 16-7)

# Consistência ~ relação com o meio

> A  consistência  de um ser está ligada à positividade que ele estabelece com o seu meio.  Não  é  adaptação,  mas  troca.  Buckminster-Fuller  mostrou  muito bem que, se detonarmos o planeta, então vamos ser extintos; nossa  individuação  perderá  sua  consistência.  É  preciso  pensar  o  meio como meio técnico.
> (p. 17)

# O caso da mecatrônica (mecânica + eletrônica) e do cacau

> Uma   pesquisa   japonesa   dos   anos   1990   mostrou   que,   quando  há  essa  mutação-fusão,  como  na  mecatrônica  (mecânica  +  eletrônica),  ocorre  algo  do  tipo  1  +  1  =  3.  Além  dos  potenciais  anteriormente  existentes  da  mecânica  e  da  eletrônica,  surgem  potenciais antes inexistentes, próprios da mecatrônica. Com isso, a capacidade industrial de planejamento estratégico e identificação de concorrentes se complica, exigindo novas estratégias de espionagem industrial. Um bom exemplo disso é o caso da manteiga de cacau. Antes, o cacau era a matéria prima para este produto, de forma que a  indústria  sabia  o  universo  com  o  qual  tinha  que  se  preocupar.  No   entanto,   com   a   descoberta   de   diversos   outros   materiais   (derivados de petróleo, por exemplo) que poderiam desempenhar a mesma função do cacau na produção do produto, esse universo se complicou. Se eu sou produtor de manteiga de cacau, antes eu conhecia o mercado, sabia quais os setores que me influenciavam, meus  fornecedores,  concorrentes  etc.,  mas  agora  tenho  novos  problemas,  pois  a  concorrência  e  a  inovação  vêm  de  lugares  imprevistos. O tempo inteiro, tudo está em jogo; não é mais possível esperar  o  retorno  econômico  de  uma  nova  tecnologia  antes  de  lançar a próxima, e isso já no começo dos anos 1990. É o princípio do  surf:  surfar  ou  morrer.  Hoje  não  é  mais  possível  fazer  como  antes, e esperar o retorno financeiro de uma inovação para lançar a próxima. **Agora, eu preciso sempre lançar o último modelo, e é a propriedade intelectual que vem tornando possível a acumulação econômica   nesse   novo   contexto.   Podemos   dizer   que   existem   duas  velocidades,  a  econômica  e  a  tecnológica,  que  ultimamente  começaram a entrar em conflito: a propriedade intelectual vem forçando uma redução da velocidade tecnológica em benefício da econômica.**
> (p. 17-8)

# Exemplo das usinas nucleares (caso Fukushima)

> As  máquinas  concretizam  potências  imantentes;  elas  se  tornam  cada  vez  mais  aquilo  para  o  qual  elas  tendem  –  que  não  é  uma  simples  projeção  daquilo  que  queremos  que  elas  sejam,  mas sim os potenciais que elas podem realizar e que nunca vão se esgotar. **Elas evoluem conosco, mas nós não determinamos o futuro delas.** Isso  pode  ser  ilustrado  com  um  exemplo  contemporâneo.  Usinas de energia nuclear são projetadas para resistir a terremotos de  até  7  pontos  na  escala  Richter.  Ora,  o  terremoto  que  acaba  de  acontecer no Japão5 foi de 9 pontos. Termoreatores nucleares estão derretendo  e  a  contaminação  por  radioatividade  já  ameça  fugir  do controle. **Vemos que a natureza está respondendo, mas não do jeito esperado.** Vai ser necessária uma revisão de toda a questão da segurança  nuclear,  pois  todos  só  se  protegiam  até  terremotos  de  7  pontos.  Na  Califórnia,  existem  várias  empresas  de  energia nuclear que ficam sobre a falha tectônica. Se eu fosse californiano eu ficaria preocupado.
> (p. 18-9)

# Concretização: analítico > concreto

> **No   processo   de   concretização,   há   uma   passagem   do   modo  analítico  para  o  concreto,  na  qual  o  objeto  vai  ganhando  coerência.**  A  diferença  entre  o  artesanato  e  a  produção  industrial  é  útil  para  a  compreensão  desse  ponto.  **No  artesanato,  estamos  juntando  elementos  para  ver  como  realizar  um  objetivo.  Já  na  produção  industrial,  tais  elementos  ganham  uma  integração  na  qual eles passam a existir, não como uma soma de elementos, mas como  um  agenciamento  sinergético.** Assim,  enquanto  no  artesanato  havia  um  agregado  de  operações  isoladas  que  poderia  ser  desfeito sem comprometer essas mesmas operações, se tentarmos separar as operações que compõem a produção industrial não as encontraremos mais. **Essa consistência que o ser técnico concreto ganhou  é  irreversível;  ele  se  tornou  um  agregado  coerente  cujos  elementos não existem mais independentemente.**
> (p. 19)

# Concretização e artificialização

> Seria  útil  considerar  o  que  Simondon  diz  sobre  a  oposição  entre artificialização e concretização. **Artificialização é a desconstrução de uma individuação para outros fins; é uma perversão. É o caso da flor na estufa – quando, em lugar de naturalizar (como no caso da concretização das máquinas), você artificializa o organismo.** A ciência, por exemplo, descobriu que é mais fácil achar os princípios  ativos  de  plantas  já  conhecidas  por  populações  tradicionais  do  que  começar  do  zero  no  laboratório.  É  uma  perversão  para ganhar dinheiro. Condenar a técnica pela artificialização não é condenar a técnica, mas sim a artificialização
> (p. 19)

# O caso da engenharia genética

> No  caso  da  engenharia  genética,  por  exemplo,  é  preciso  assumir   que   existe   aqui   uma   confusão   sobre   se   a   semente   geneticamente  modificada  é  ainda  uma  "semente  como  antes", ou  se  ela  é  uma  coisa  "totalmente  nova",  que  antes  não  existia.  É semente ou é propriedade? Na hora de usar é preciso pagar por  aquilo  que  ela  tem  de  diferente  de  todas  as  outras  sementes,  algo  que  é  considerado  propriedade  particular.  Mas  na  hora  de vender é preciso convencer de que se trata de uma semente como qualquer  outra,  que  a  operação  é  a  mesma  de  sempre.  Ora,  a  sociedade  precisa  decidir  se  quer  eliminar  o  que  se  entende  por  semente e aceitar a engenharia genética, ou não. É preciso decidir, e não apenas fingir que é semente como sempre. **Por isso o caso da estufa, apresentado por Simondon, é central aqui: a individuação da  planta  foi  interrompida  e  tornada  dependente  do  humano.  O problema desse processo de artificialização de um ser natural é que a semente geneticamente modificada ameaça a existência da semente natural, algo que não ocorre no processo de concretização dos  objetos  técnicos.**  A  máquina  não  percebe:  ela  lê  mas  não  vê,    funciona  de  um  modo  diferente  do  humano.  Aqui  não  há  desconstrução do humano, mas sim construção da máquina. Isso é muito diferente da semente geneticamente modificada.
> (p. 19-20)

# Natureza e cultura (diferentes modos de existência)

> Há,  podemos  dizer,  uma  diferença  clara  entre  natureza  e  cultura. Apesar de existir um substrato comum entre o artificial e o natural, isso não significa que são a mesma coisa. São diferentes modos de existência, como nos casos do ser físico, do ser biológico ou do ser psicossocial. **Em sua obra sobre os diferentes modos de existência,  Étienne  Souriau  (2009)  considera  ainda  outros  modos  de  existência,  como  o  dos  fantasmas.  É  possível  pensar  o  ser  físico, o ser vivo, o ser humano, a máquina, os fantasmas... Como Simondon,  ele  pensa  então  o  "entre",  para  pensar  o  indivíduo.  O  humano  nunca  será  supérfluo,  mas  ele  muda  de  função,  de papel. O humano não é da mesma natureza do técnico. A evolução vital  é  paralela  à  técnica,  mas  não  há  analogia.  A  confusão  entre  essas duas evoluções foi justamente o erro da cibernética.**
> (p. 20)

# Técnica e mercadoria

> Simondon  permite  entender  o  que  é  exclusivo  do  ser  vivo,  o que é exclusivo do ser técnico, e o que é comum a ambos. **O que ambos  compartilham  é  a  informação,  e  esse  compartilhado  pode  ser  orientado  rumo  à  concretização,  ou  rumo  à  artificialização. O importante é ver a tecnicidade, e o papel da informação na concretização  dessa  tecnicidade;  e  isso  do  ponto  de  vista  humano  e não-humano. O problema não é a artificialização, mas sim as implicações não pensadas dessa artificialização.** Se é o capital que está  mandando  no  processo,  então  não  se  trata  mais  de  concretização técnica. Se a melhoria genética tem uma finalidade exterior, então não se trata de uma questão de coerência interna. **A finalidade precisa ser interna ao objeto técnico, e não externa. A crítica não deve ser dirigida à técnica, mas sim à relação entre as acelerações tecnológica e econômica capitalista, ao fato de que esta está comprometendo aquela. É o feitiço-fetiche da mercadoria.**
> (p. 20-1)

# Capital e tecnicidade (movimento software livre)

> **O  capitalismo  não  será  superado  sem  uma  conscientização  dos  modos  de  existência  dos  objetos  técnicos.  Aí  está,  evidentemente,  o  interesse  dos  movimentos  de  software  livre  nas  suas  diversas variantes, mais ou menos radicais. O software livre busca o movimento da tecnicidade, em oposição à lógica do capital.** As máquinas  não  podem  ser  tratadas  como  escravas,  o  que  foi  feito  tanto pelo capitalismo quanto pelo comunismo.
> (p. 21)

# Trabalho e invenção (Simondon, Marx e D&G)

> **Simondon tratou da  problemática  econômica  quando  diferenciou  trabalho  e  invenção. Podemos dizer que existem dois movimentos imanentes: o do capital e o da técnica.** Parece-me que Simondon trata apenas do movimento imanente da técnica, ao passo que Marx reificou o trabalho,  pois  não  se  interessou  pela  invenção.  **Se  quisermos  encontrar esses dois movimentos imanentes sendo bem trabalhados em conjunto, teremos que ler o "Tratado de nomadologia" de Deleuze e Guattari (1997).**
> (p. 21)

# Ser e devir (humano) (no capitalismo)

> Na  chave  processual  da  individuação,  **o  problema  não  é  o  que o humano é (seu "ser", sua "essência"), e sim o que ele setorna(seu "devir", seu "vir-a-ser"). Trata-se de um pensamento político, mas  que  exige  que  se  substitua  as  armas  do  velho  humanismo.**  Um exemplo das implicações políticas desta questão é a maneira como o programa neoliberal processa o _homo economicus_, fazendo da  vida  um  recurso  a  ser  investido.  Até  nas  relações  afetivas  é  preciso que haja retorno. Osvaldo J. López-Ruiz (2007) demonstrou isso numa pesquisa sobre o _ethos_ dos executivos contemporâneos, quando,  por  exemplo,  o  desejo  de  aprender  a  tomar  vinho  vem  já  ligado  à  possibilidade  de  convidar  o  chefe  para  um  jantar  e  ser  promovido  por  demonstrar  conhecimentos  enológicos.  **Tudo  é  investimento  e,  portanto,  envolve  uma  programação  (cálculo).  Então surge a questão: quando é que você individua efetivamente com seus próprios potenciais, e não com aqueles que são na verdade valorizados pelo sistema? Onde está a individuação e onde está a programação? Como saber se você está individuando, ou apenas fazendo  o  investimento  que  o  sistema  diz  que  você  precisa  fazer  para não ser um _loser_? São todas implicações políticas.**
> (p. 22)

# Possibilidades de uma individuação humano-máquina no capitalismo. Psicanálise -> neuro/cyber

> Em que medida o grau de controle no capitalismo contem-porâneo  abre  margens  de  individuação  humano-máquina?  Em que  medida  há  o  controle  e  em  que  medida  há  individuação? Em  que  medida  o  controle  transforma  a  própria  maneira  como  concebemos a psiquê humana? A psicanálise (as "psis")  está  em  decadência pois as "neuros" (ou "cybers") dizem que podem resolver todos os problemas neuroquimicamente. **A psicanálise enquadrava a economia libidinal do homem ocidental em determinados parâmetros,  mas  o  que  fundamenta  o  pensamento  freudiano  já  não  fundamenta  mais  a  compreensão  contemporânea  da  subjetividade.  Novas  práticas  reprodutivas  ligadas  a  novas  formas  de  sexualidade  e  de  agrupamento  familiar  tornaram  obsoleta  a  psicanálise.** Em meados do século XX a reprodução humana ainda não lidava com essas questões, e as transformações só aceleraram. No  entanto,  se  os  processos  de  individuação  se  transformaram  drasticamente, o pensamento sobre a individuação parece não ter acompanhado.
> (p. 23)

> Hamlet foi o primeiro a dizer, ainda no século XVI: "o tempo está fora do prumo". Nós continuamos nesta questão: ninguém tem tempo; há um abismo, uma tensão, entre o tempo social e o tempo humano,  e  lidar  bem  com  isso  é  raro.  Vinte  anos  atrás,  Heiner  Müller escreveu _Hamlet-Máquina_, misturando a questão de Hamlet com  a  questão  das  máquinas. **Segundo  Müller,  a  poesia  interessa pois vai mais depressa que a teoria, e isso também me interessa. Se  isso  é  obscuro,  é  preciso  notar  que  Müller  estava  escrevendo  no  escuro.  E  se  Shakespeare  antecipou  uma  questão  central  da  modernidade  com  Hamlet,  então  não  estaria  Müller  antecipando  uma nova questão? O que vai acontecer? Isso me interessa.**
> (p. 23)
