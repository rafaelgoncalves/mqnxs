---
title: Moralidade e tecnologia
author: Rafael Gonçalves
path: "/moralidade-e-tecnologia-Latour"
date: 2020-10-06
category: fichamento
published: true
featuredImage: "../../images/Red_traffic_light.jpg"
srcInfo: Another Red Light por <a href="https://www.flickr.com/photos/lenzmoser/32307397723/">::ErWin</a>
tags: ["Bruno Latour", "tecnociência", "moralidade", "fichamento"]
---

Pequeno fichamento do texto *Morality and technology*<sup>[1](#latour)</sup> de Bruno Latour.

Sobre a técnica preceder a humanidade, e a crítica à visão instrumentalista de Heidegger

>  Technologies belong to the human world in a modality other than that of instrumentality, efficiency or materiality.
> A being that was artificially torn away from such
> a dwelling, from this technical cradle, could in no way be a moral being,
> since it would have ceased to be human – and, besides, it would for a long
> time have ceased to exist.
> Technologies and moralities happen to be indissolubly mingled because, in both cases, the question of the relation of ends and means is profoundly problematized.
> ([1](#latour), p. 248)

Tecnologia como modo de existência, em oposição a território ou classe de objetos

> Technology is everywhere, since the term applies to a regime of enunciation, or, to put it another way, to a mode of existence, a particular form of
> exploring existence, a particular form of the exploration of being – in the
> midst of many others. If we are unable to distinguish between a technical
> object and a non-technical one, we should nevertheless be able to locate
> the dimension pertaining to technology in some entity. The regime of technology, if you wish, is different from another standpoint (scientific, artistic
> or moral) not in the way that a region of reality would differ from another,
> but in the way prepositions differ amongst themselves, in much the same
> way as ‘in’ is clearly distinguishable from ‘by’, although there is no particular domain of ‘in’ that we can separate from the territory ‘by’.
> ([1](#latour), p. 248)

Objetos técnicos são uma dobra de espaço, tempo, e actantes. Multiplicidade de temporalidades, terriotorios, atores.

> There is nothing less local, less contemporary, less brutal than a hammer, as soon as one begins to unfold what it
> sets in motion; there is nothing more local, more brutal and more durable
> than this same hammer as soon as one folds everything implicated in it.
> ([1](#latour), p. 249)

Tecnologia como ser-como-outro.

> We never tame technologies,
> not because we lack sufficiently powerful masters, not because technologies, once they have become ‘autonomous’, function according to their own
> impulse, not because, as Heidegger claims, they are the forgetting of Being
> in the form of mastery, but because they are a true form of mediation. Far
> from ignoring being-as-being in favour of pure domination, of pure hailing,
> the mediation of technology experiments with what must be called beingas-another
> ([1](#latour), p. 250)

O uso de uma tecnologia desvia nossas intenções: ao mudar os meios, nós mudamos o fim.

> If we fail to recognize how much the use
> of a technique, however simple, has displaced, translated, modified, or
> inflected the initial intention, it is simply because we have changed the end
> in changing the means, and because, through a slipping of the will, we have
> begun to wish something quite else from what we at first desired. If you want
> to keep your intentions straight, your plans inflexible, your programmes of
> action rigid, then do not pass through any form of technological life. The
> detour will translate, will betray, your most imperious desires.
> ([1](#latour),  p. 252)

Tecnologia é mais do que existencia material passiva, é mediação ativa.

> Yet the existence of a multiplicity of modes of exploration of
> being does not justify turning technical enunciation into a simple material
> domain on the surface of which always float symbols, values, judgements
> and tastes, since that habit would cause all mediations to gradually disappear. 
> ([1](#latour), p. 253)

Tecnologia é uma complexidade de traduções e desvios, onde não faria sentido falar nem em função (de forma fixa), nem em neutralidade.

>  The paradox of technology
> is that it is always praised for its functional utility, or always held in
> contempt because of its irritating neutrality, although it has never ceased to
> introduce a history of enfoldings, detours, drifts, openings and translations
> that abolish the idea of function as much as that of neutrality
> ([1](#latour), p. 255)

Moralidade está inscrita nas coisas, assim, antes de uma regra moral ser concretizada em leis e exigencias, ela já está presente nos objetos tecnológicos.

> Morality comes to
> rework precisely the same materials as does technology, but by extracting
> from each of them another form of alterity because its primary concern is
> the impossibility of their fitting into the mould of intermediaries. Well before
> we are able to translate the moral exigencies of tradition into obligations,
> they already lie inside that massive objectivity of mediations that forbid
> them being taken for ends for whoever and whatever else. In this sense,
> morality is from the beginning inscribed in the things which, thanks to it,
> oblige us to oblige them
> ([1](#latour), p. 257-258)


> Each of these modes of existence upsets in its own distinctive way the relations between means and
> ends: technology by dislocating the relations between entities in such a way
> that they open towards a series of new linkages that force the constant
> displacement of goals and multiply intermediary agents whose collective
> sliding forbids any mastery; morality, by constantly interrogating aggregates
> to make them express their own aims and prevent a too hasty agreement
> about the definitive distribution of those that will serve as means and those
> that will serve as ends. If one adds morality to technology, one is bound to
> notice, to make a pun, the end of the means.
> This gathering, this progressive composition
> of a common world, obliges us to return to another form of enunciation, this
> time a political one, which similarly aspires to recover its ontological dignity
> in order to escape from the state of abasement in which it had been cast by
> a scorn that has lasted even longer than that which technology has had to
> endure for so long.
> ([1](#latour), p. 258-259)


<br/>
<section class="footnotes" role="doc-endnotes">
<hr />
<ol>
<li id="latour" role="doc-endnote">
LATOUR, Bruno; VENN, Couze. Morality and technology. <em>Theory, culture & society</em>, v. 19, n. 5-6, p. 247-260, 2002.</li>
</ol>
</section>
