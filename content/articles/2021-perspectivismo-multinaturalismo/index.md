---
title: Perspectivismo e multinaturalismo
date: 2021-12-10
author: Rafael Gonçalves
path: "/perspectivismo-multinaturalismo"
category: fichamento
featuredImage: "./narciso.jpg"
srcInfo: Foto por <a href="https://www.flickr.com/photos/70626035@N00/10510653336/">jacinta lluch valero</a>
published: true
tags: 
- antropologia
- perspectivismo
- multinaturalismo
- Eduardo Viveiros de Castro
- Metafísicas canibais

---

Fichamento de dois capítulos da primeira parte do livro _Metafísicas Canibais_[^1] do antropólogo Eduardo Viveiros de Castro.

[^1]: DE CASTRO, Eduardo Viveiros, _Metafísicas canibais: elementos para uma antropologia pós-estrutural_, São Paulo: Ubu Editora, n-1 edições, 2018.

# Metafísica da predação e afinidade virtual nos ameríndios

> Os dois conceitos emergiram ao cabo de uma análise dos pressupostos cosmológicos da “metafísica da predação” que acabamos de evocar. Ocorre que essa metafísica, como se infere do resumo de Lévi-Strauss, tem sua expressão mais patente no alto rendimento especulativo das categorias sociológicas indígenas que denotam a aliança matrimonial, fenômeno que traduzimos em mais um outro conceito, o de _afinidade virtual_. A afinidade virtual é o esquematismo característico do que Deleuze chamaria a “estrutura Outrem” nos mundos ameríndios e, como tal, acha-se marcada indelevelmente pelo signo do canibalismo, um motivo onipresente na imaginação relacional dos habitantes destes mundos.
> (p. 33-4)

# Elementos da alter-antropologia indígena (transformação simétrica e inversa da antropologia ocidental)

> Perspectivismo interespecífico, multinaturalismo ontológico e alteridade canibal formam assim os três vértices de uma alter-antropologia indígena que é uma transformação simétrica e inversa da antropologia ocidental – simétrica também no sentido de Latour, e inversa também no sentido da _reverse anthropology de Wagner_.
> (p. 34)

# Origem na parábola levi-straussiana (experimento indígena sobre a corporeidade do europeu e experimento indígena sobre a intelectualidade do indígena)

> Este trabalho conjunto com Tânia Stolze Lima teve como ponto de partida, do meu lado, uma pequena epifania – a súbita percepção da ressonância entre os resultados de nossas investigações sobre as cosmopolíticas amazônicas, que afirmam uma multiplicidade perspectiva intrínseca ao real, e uma conhecida parábola de Lévi-Strauss sobre a conquista da América, relatada originalmente em Raça e história:
> 
> > Nas Antilhas, alguns anos após o descobrimento da América, enquanto os espanhóis despachavam comissões de inquérito para saber se os indígenas possuíam alma ou não, estes tratavam de submergir prisioneiros brancos, para verificar, com base numa longa e cuidadosa observação, se seus cadáveres apodreciam ou não. (Lévi-Strauss [1952] 2013: 364)
> 
> (p. 35)

> O autor viu nesse conflito de antropologias uma alegoria barroca do fato de que uma das manifestações típicas da natureza humana é a negação de sua própria generalidade. Uma avareza congênita, que impede a extensão dos predicados da humanidade à espécie como um todo, parece ser justamente um desses predicados. Em suma, o etnocentrismo é, como o bom senso – do qual seja talvez apenas a expressão aperceptiva –, a coisa do mundo mais bem compartilhada.
> (p. 35)

> O formato da lição lévi-straussiana é familiar, mas isso não a faz menos mordente. O favorecimento da própria humanidade às custas da humanidade do outro manifesta uma semelhança essencial com esse outro desprezado: como o outro do Mesmo (do europeu) se mostra ser o mesmo que o outro do Outro (do indígena), o Mesmo termina se mostrando, sem se dar conta, o mesmo que o Outro.
> (p. 35)

> A anedota claramente fascinou Lévi-Strauss, que iria recontá-la em Tristes trópicos. Mas ali ele introduz um toque irônico suplementar, marcando uma diferença antes que uma semelhança entre as partes, ao observar que em suas investigações etnoantropológicas, os europeus invocavam as ciências sociais, ao passo que os índios mostravam maior confiança nas ciências naturais; e que enquanto os primeiros perguntavam-se se os índios não seriam meros animais, os segundos se contentavam em suspeitar que os europeus pudessem ser deuses. “Dada a igual [mútua] ignorância”, conclui o autor, “a última atitude era mais digna de seres humanos” (1955a: 81-83). O que, se é realmente o caso, obriga-nos a concluir que, a despeito de uma igual ignorância a respeito do outro, o outro do Outro não era exatamente o mesmo que o outro do Mesmo. Talvez coubesse mesmo dizer que era seu exato oposto, não fosse o fato de que, nos mundos indígenas, a relação entre esses dois outros da humanidade, a animalidade e a divindade, é completamente outra que aquela que herdamos do cristianismo. O contraste retórico de Lévi-Strauss é eficaz por lançar mão de nossas hierarquias ontológicas, antes que das dos Taino.
> (p. 35-6)

> De qualquer maneira, foi uma meditação sobre esse desequilíbrio que conduziu à hipótese perspectivista segundo a qual os regimes ontológicos ameríndios divergem daqueles mais difundidos no Ocidente precisamente no que concerne às funções semióticas inversas atribuídas ao corpo e à alma. Para os espanhóis do incidente das Antilhas, a dimensão marcada era a alma; para os índios, era o corpo. Por outras palavras, os europeus nunca duvidaram de que os índios tivessem corpo (os animais também os têm); os índios nunca duvidaram de que os europeus tivessem alma (os animais e os espectros dos mortos também as têm). O etnocentrismo dos europeus consistia em duvidar que os corpos dos outros contivessem uma alma formalmente semelhante às que habitavam os seus próprios corpos; o etnocentrismo ameríndio, ao contrário, consistia em duvidar que outras almas ou espíritos fossem dotadas de um corpo materialmente semelhante aos corpos indígenas.
> (p. 36-7)

# A "alma" atual seriam "a cultura", "o simbólico", "a mente". Problema moderno: existe alma nos animais e nas máquinas?

> {6}: A velha “alma” recebeu nomes novos, agora ela avança mascarada (larvatus prodeo): chama-se-lhe “a cultura”, “o simbólico”, “a mente”. O problema teológico da alma alheia transmutou-se diretamente no quebra-cabeça filosófico conhecido como o “_problem of other minds_”, hoje na linha de frente das investigações neurotecnológicas sobre a consciência humana, sobre os fundamentos possíveis da atribuição da condição jurídica de “pessoa” a outros animais e, por fim, sobre a inteligência das máquinas (os deuses passaram a habitar os microprocessadores Intel). Nos dois últimos casos, trata-se de saber se certos animais não teriam, afinal, algo como uma alma ou consciência – talvez mesmo uma cultura –, e se certos sistemas materiais não-autopoiéticos, ou seja, desprovidos de um corpo orgânico (máquinas computacionais), podem se mostrar capazes de intencionalidade.
> (p. 37)

# Diferença das posições que ocupam Natureza e Cultura nas duas visões de mundo (ocidental e ameríndia)

> Nos termos da semiótica de Roy Wagner – melanesianista que logo viria a se revelar um mediador crucial para a teoria do perspectivismo ameríndio –, na onto-antropologia europeia o corpo pertenceria à dimensão do inato ou do espontâneo (a “natureza”), dimensão que é o resultado contrainventado de uma operação de simbolização “convencionalizante”, enquanto a alma seria a dimensão construída, fruto de uma simbolização “diferenciante” que “especifica e concretiza o mundo convencional ao traçar distinções radicais e delinear suas individualidades” (Wagner [1975] 2010: 86). Nos mundos indígenas, ao contrário, a alma “é experienciada como uma manifestação […] da ordem convencional implícita em todas as coisas”, ela “resume os aspectos em que seu possuidor é similar aos outros [seres], para além dos apectos em que ele difere deles (id. ibid.: 152); o corpo, ao contrário,7 pertenceria à esfera do que está sob a responsabilidade dos agentes, ele é uma das figuras fundamentais que é preciso construir contra o fundo inato e universal de uma “humanidade imanente” (id. ibid.: 142-51). Em poucas palavras, a práxis europeia consiste em “fazer almas” (e diferenciar culturas) a partir de um fundo corporal-material dado (a natureza); a práxis indígena, em “fazer corpos” (e diferenciar espécies) a partir de um continuum sócio-espiritual dado “desde sempre” – no mito, precisamente, como veremos.
> (p. 37-8)

# Modos de simbolização convencional e diferenciante em Wagner

> O sistema teórico de Wagner, conceitualmente denso e fortemente original, resiste a um resumo didático; exortamos o leitor a experimentar diretamente A invenção da cultura, que contém sua exposição mais elegante. Grosso modo, pode-se dizer que a semiótica wagneriana é uma teoria da práxis (humana e verossimilmente não-humana) que a concebe como consistindo na operação recíproca e recursiva de dois, e apenas dois, modos de simbolização: (1) o simbolismo convencional ou coletivizante (também: literal), em que os signos se organizam em contextos padronizados (domínios semânticos, linguagens formais etc.) que contrastam com um plano heterogêneo de “referentes”, isto é, em que são vistos como simbolizando algo de outro que eles mesmos; e (2) o simbolismo diferenciante ou inventivo (também: figurativo), modo em que o mundo de fenômenos “representado” pela simbolização convencional é apreendido como constituído de “símbolos que representam a si mesmos”, isto é, de eventos que se manifestam simultaneamente como símbolos e referentes, dissolvendo o contraste convencional.
> (p. 38-9)

# O real como efeito da simbolização diferenciante (?)

> É importante observar, em primeiro lugar, que o mundo dos referentes, o “real”, é definido por Wagner como um efeito semiótico: o outro do signo é um outro signo, dotado da capacidade singular de “representar a si mesmo”. O modo de existência das entidades atuais enquanto objetos ou “ocasiões” (Whitehead) é a tautegoria.
> (p. 39)

# Semiótica wagneriana é operação convencionalizante (?)

> Em segundo lugar, deve-se sublinhar que o contraste entre os dois modos é resultado de uma operação (e uma percepção) convencionalizante: a distinção entre invenção e convenção é ela própria convencional, mas ao mesmo tempo toda convenção é produzida por contrainvenção.
> (p. 39)

# Culturas coletivizadoras (contrainventa diferenciação motivadora) e culturas diferencializantes (contrainventam uma coletivização motivadora)

> As culturas (os macrossistemas humanos de convenções) se distinguem por aquilo que definem como pertencendo à esfera da responsabilidade dos agentes – o mundo do “construído” – e aquilo que pertence (porque é contraconstruído como pertencendo) ao mundo do “dado”, isto é, do não-construído: O cerne de todo e qualquer conjunto de convenções culturais é uma simples distinção quanto a que tipo de contextos – os não convencionalizados ou os da própria convenção – serão deliberadamente articulados no curso da ação humana e que tipo de contextos serão contrainventados como “motivação” sob a máscara convencional do “dado” ou do “inato”. É claro que, para qualquer conjunto de convenções dado, seja ele o de uma tribo, uma comunidade, uma “cultura” ou uma classe social, há apenas duas possibilidades: um povo que diferencia deliberadamente, sendo essa a forma de sua ação, irá invariavelmente contrainventar uma coletivização motivadora como “inata”, e um povo que coletiviza deliberadamente irá contrainventar uma diferenciação motivadora dessa mesma maneira. (Wagner [1975] 2010: 95)
> (p. 39-40)

# Perspectivismo como dupla torção (materialista e especulativa) do animismo (psicologista e positivista)

> O quiasma antropológico apontado por Lévi-Strauss no incidente das Antilhas correspondia muito bem a duas características singulares que começávamos a divisar na etnografia amazônica. Primeiro, ele vinha confirmar por uma via inesperada a operação onipresente de uma economia da corporalidade no seio mesmo de ontologias que estavam então sendo redefinidas e renomeadas – algo unilateralmente, como se vê – como animistas.
> (p. 40)

> Em seguida, ele permitia entrever algumas das implicações metateóricas desse estatuto não marcado ou genérico da dimensão virtual (a “alma”) dos existentes, premissa capital de uma poderosa estrutura intelectual indígena, capaz, _inter alia_, de contradescrever sua própria imagem projetada pela antropologia ocidental, e, por essa via, devolver-nos “uma imagem de nós mesmos na qual nós não nos reconhecemos”.
> '(p. 41)

> Foi a essa dupla torção materialista e especulativa imprimida à representação usual (psicologista e positivista) do “animismo” que chamamos “perspectivismo”, em virtude de suas analogias – construídas ao menos tanto quanto constatadas – com as teses filosóficas associadas a este rótulo, como as que se podem encontrar tanto em Leibniz como em Nietzsche, em Whitehead como em Deleuze.
> (p. 41-2)

# Visão ameríndia segundo a qual todo existente seria um centro potencial de intencionalidade (ponto de vista)

> Como diversos etnólogos já o haviam notado, mas quase todos muito de passagem, numerosos povos (talvez todos) do Novo Mundo compartilham de uma concepção segundo a qual o mundo é composto por uma multiplicidade de pontos de vista: todos os existentes são centros potenciais de intencionalidade, que apreendem os demais existentes segundo suas próprias e respectivas características ou potências.
> (p. 42)

> Os pressupostos e consequências dessa ideia são irredutíveis ao conceito corrente de relativismo que eles parecem, à primeira vista, evocar. Eles se dispõem, a bem dizer, em um eixo ortogonal à oposição entre relativismo e universalismo. Tal resistência do perspectivismo ameríndio aos termos de nossos debates epistemológicos ameaça seriamente a transportabilidade das partições ontológicas que os alimentam. É a conclusão a que chegaram muitos antropólogos (embora por diversos outros motivos), quando argumentaram que a distinção clássica entre Natureza e Cultura – artigo primeiro da Constituição da disciplina, em que ela faz seu voto de obediência à velha matriz metafísica ocidental – não pode ser utilizada para descrever dimensões ou domínios internos a cosmologias não ocidentais sem passar antes por uma crítica etnológica rigorosa.
> (p. 42)

# Multiculturalismo e multinaturalismo

> Tal crítica, no caso presente, impunha a redistribuição dos predicados subsumidos nas duas séries paradigmáticas da “Natureza” e da “Cultura”: universal e particular, objetivo e subjetivo, físico e moral, fato e valor, dado e instituído, necessidade e espontaneidade, imanência e transcendência, corpo e espírito, animalidade e humanidade etc. Esse reembaralhamento das cartas conceituais levou-me a sugerir a expressão “multinaturalismo” para designar um dos traços contrastivos do pensamento ameríndio em relação às cosmologias “multiculturalistas” modernas: enquanto estas se apoiam na implicação mútua entre unicidade da natureza e multiplicidade das culturas – a primeira garantida pela universalidade objetiva dos corpos e da substância, a segunda gerada pela particularidade subjetiva dos espíritos e dos significados –,10 a concepção ameríndia suporia, ao contrário, uma unidade do espírito e uma diversidade dos corpos. A “cultura” ou o sujeito seriam aqui a forma do universal, a “natureza” ou o objeto, a forma do particular.
> (p. 42-3)

# Na visão ameríndia, todo existente se ve como (potencialmente é) pessoa e ve outros existentes (que potencialmente são) conforme uma relação da predação

> Essa semelhança inclui um mesmo modo, que poderíamos chamar performativo, de apercepção: os animais e outros não-humanos dotados de alma “se veem como” pessoas, e portanto, em condições ou contextos determinados, “são” pessoas, isto é, são entidades complexas, com uma estrutura ontológica de dupla face (uma visível e outra invisível), existindo sob os modos pronominais do reflexivo e do recíproco e os modos relacionais do intencional e do coletivo. O que essas pessoas veem, entretanto – e que sorte de pessoas elas são –, constitui precisamente um dos problemas filosóficos mais sérios postos por e para o pensamento indígena.
> (p. 43-4)

> A semelhança das almas não implica a homogeneidade ou identidade do que essas almas exprimem ou percebem. O modo como os humanos veem os animais, os espíritos e outros personagens cósmicos é profundamente diferente do modo como esses seres os veem e se veem. Tipicamente – esta tautologia é como o grau zero do perspectivismo –, os humanos, em condições normais, veem os humanos como humanos e os animais como animais; quanto aos espíritos, ver estes seres usualmente invisíveis é um signo seguro de que as “condições” não são normais (doença, transe e outros estados alterados de consciência). Os animais predadores e os espíritos, por seu lado, veem os humanos como animais de presa, ao passo que os animais de presa veem os humanos como espíritos ou como animais predadores: “O ser humano se vê a si mesmo como tal. A Lua, a serpente, o jaguar e a Mãe da varíola o veem, contudo, como um tapir ou um queixada, que eles matam”, anota Baer (1994: 224) sobre os Matsiguenga da Amazônia peruana. Vendo-nos como não-humanos, é a si mesmos – a seus respectivos congêneres – que os animais e espíritos veem como humanos: eles se percebem como (ou se tornam) entes antropomorfos quando estão em suas próprias casas ou aldeias, e experimentam seus próprios hábitos e características sob uma aparência cultural – veem seu alimento como alimento humano (os jaguares veem o sangue como cerveja de milho, os urubus veem os vermes da carne podre como peixe assado etc.), seus atributos corporais (pelagem, plumas, garras, bicos etc.) como adornos ou instrumentos culturais, seu sistema social como organizado do mesmo modo que as instituições humanas (com chefes, xamãs, festas, ritos…).
> (p. 44-5)

> Algumas precisões são necessárias. O perspectivismo raramente se aplica a todos os animais (além de quase sempre englobar outros seres – no mínimo, os mortos); ele parece focalizar mais frequentemente espécies como os grandes predadores e carniceiros, tais como o jaguar, a anaconda, o urubu ou a harpia, bem como as presas típicas dos humanos, como os porcos selvagens, os macacos, os peixes, os veados ou o tapir. Com efeito, uma das dimensões básicas das inversões perspectivas diz respeito aos estatutos relativos e relacionais de predador e presa. A metafísica amazônica da predação é um contexto pragmático e teórico altamente propício ao perspectivismo. Isso dito, não há existente que não possa ser definido nos termos relacionais gerais de sua posição em uma escala relativa de potência predatória.
> (p. 45)

> Pois se nem todos os existentes são pessoas _de facto_, o ponto fundamental está em que nada impede (_de jure_) que qualquer espécie ou modo de ser o seja. Não se trata, em suma, de um problema de taxonomia, de classificação, de “etnociência”.11 Todos os animais e demais componentes do cosmos são intensivamente pessoas, virtualmente pessoas, porque qualquer um deles pode se revelar (se transformar em) uma pessoa.
> (p. 45-6)

# Perspectivismo não como possibilidade lógica, mas potencialidade ontológica

> Não se trata de uma mera possibilidade lógica, mas de potencialidade ontológica. A “personitude” e a “perspectividade” – a capacidade de ocupar um ponto de vista – são uma questão de grau, de contexto e de posição, antes que uma propriedade distintiva de tal ou qual espécie. Alguns não-humanos atualizam essa potencialidade de modo mais completo que outros; certos deles, aliás, manifestam-na com uma intensidade superior à de nossa espécie, e, neste sentido, são “mais humanos” que os humanos (Hallowell 1960: 69).
> (p. 46)

# Carater jurisprudencial e não normativo do perspectivismo

> Além disso, a questão possui uma qualidade _a posteriori_ essencial. A possibilidade de que um ser até então insignificante venha a se revelar (ao sonhador, ao doente, ao xamã) como um agente prosopomórfico capaz de afetar os negócios humanos está sempre aberta; no que concerne à personitude dos seres, a experiência, justamente, “pessoal” é mais decisiva que qualquer lista taxonômica ou dogma cosmológico. Não há registro civil, e, além disso, a ontologia indígena é essencialmente jurisprudencial, não um código normativo.
> (p. 46

# Etnocentrismo (negação interespecífica de humanidade) entre ameríndios

> Se nada impede que qualquer existente seja pensado como pessoa – isto é, como manifestação individual de uma multiplicidade biossocial –, nada tampouco impede que um outro coletivo humano não o seja. Esta, aliás, é a situação usual: a estranha “generosidade” que faz povos como os amazônicos verem seres humanos ocultos sob as formas mais improváveis, ou melhor, afirmarem que mesmo seres os mais improváveis são capazes de se verem como humanos, acompanha paradoxalmente o tão falado “etnocentrismo” desses mesmos povos, que negam a humanidade a seus congêneres, por vezes mesmo (ou sobretudo) a seus vizinhos mais próximos, na geografia como na história.
> (p. 46-7)

# Humanidade como posterior/inferior à noção de pessoalidade

> Esse fato de que a condição de pessoa (cuja forma aperceptiva universal é a anatomia e a etologia humanas) possa ser tanto “estendida” a outras espécies como “recusada” a outros coletivos de nossa espécie sugere, de saída, que o conceito de _pessoa_ – centro de intencionalidade constituído por uma diferença de potencial interna – é anterior e superior logicamente ao conceito de _humano_. A humanidade é a posição do congênere, o modo reflexivo do coletivo, e como tal é derivada em relação às posições primárias de predador ou presa, que envolvem necessariamente outros coletivos, outras multiplicidades pessoais em situação de alteridade perspectiva.
> (p. 47)

# A humanidade como suspensão deliberada de uma diferença predatória (semelhança como caso particular da diferença em que esta tende a zero)

> A semelhança ou congeneridade surge por suspensão deliberada, socialmente produzida, de uma diferença predatória dada; ela não a precede.13 É nisso precisamente em que consiste o processo do parentesco ameríndio: a “reprodução humana” é uma _estabilização intensiva da predação_, seu inacabamento deliberado, ao modo do famoso “platô contínuo de intensidade” proposto por Gregory Bateson como característico do ethos do povo de Bali,14 conceito que inspirou os “mil platôs” de Deleuze e Guattari.
> (p. 47-8)

> Em um outro texto de Lévi-Strauss que tratava, não por acaso, do canibalismo, essa ideia da semelhança como decaimento de um potencial de diferença recebeu uma formulação especialmente adequada ao perspectivismo ameríndio:
> 
> > O problema do canibalismo […] não consistiria mais em investigar o porquê do costume, mas, ao contrário, o modo pelo qual surgiu esse limite inferior da predação à qual talvez se ligue a vida social. (Lévi-Strauss 1984: 144, grifo nosso)
> 
> O que não é senão uma aplicação do preceito estruturalista clássico, segundo o qual “a semelhança não existe em si: ela não passa de um caso particular da diferença, aquele em que a diferença tende a zero” (Lévi-Strauss [1971] 2011: 35).
> (p. 48-9)

# Xamãs como capazes de assumir outras perspectivas (e se relacionar com outras espécies, as percebendo como "pessoas")

> O xamanismo ameríndio pode ser definido como a habilidade manifesta por certos indivíduos de cruzar deliberadamente as barreiras corporais entre as espécies e adotar a perspectiva de subjetividades “estrangeiras”, de modo a administrar as relações entre estas e os humanos. Vendo os seres não-humanos como estes se veem (como humanos), os xamãs são capazes de assumir o papel de interlocutores ativos no diálogo transespecífico; sobretudo, eles são capazes de voltar para contar a história, algo que os leigos dificilmente podem fazer.
> (p. 49)

# Xamanismo como política cósmica

> O encontro ou o intercâmbio de perspectivas é um processo perigoso, e uma arte política – uma diplomacia. Se o multiculturalismo ocidental é o relativismo como política pública (a prática complacente da tolerância), o perspectivismo xamânico ameríndio é o multinaturalismo como política cósmica (o exercício exigente da precaução).
> (p. 49-50)

# Epistemologia da subjetivação/personificação no xamã (versus objetivação moderna). Pessoa (intenção) x coisa.

> O xamanismo é um modo de agir que implica um modo de conhecer, ou antes, um certo ideal de conhecimento. Tal ideal está, sob certos aspectos, nas antípodas da epistemologia objetivista favorecida pela modernidade ocidental. Nesta última, a categoria do objeto fornece o telos: conhecer é “objetivar”; é poder distinguir no objeto o que lhe é intrínseco do que pertence ao sujeito cognoscente, e que, como tal, foi indevida e/ou inevitavelmente projetado no objeto. Conhecer, assim, é dessubjetivar, explicitar a parte do sujeito presente no objeto, de modo a reduzi-la a um mínimo ideal (ou a ampliá-la demonstrativamente em vista da obtenção de efeitos críticos espetaculares). Os sujeitos, tanto quanto os objetos, são concebidos como resultantes de processos de objetivação: o sujeito se constitui ou reconhece a si mesmo nos objetos que produz, e se conhece objetivamente quando consegue se ver “de fora”, como um “isso”. Nosso jogo epistemológico se chama objetivação; o que não foi objetivado permanece irreal e abstrato. A forma do Outro é a coisa.
> (p. 50)

> O xamanismo ameríndio é guiado pelo ideal inverso: conhecer é “personificar”, tomar o ponto de vista daquilo que deve ser conhecido. Ou antes, daquele; pois a questão é a de saber “o quem das coisas” (Guimarães Rosa), saber indispensável para responder com inteligência à questão do “por quê”. A forma do Outro é a pessoa. Poderíamos dizer que a personificação ou subjetivação xamânicas refletem uma propensão geral a universalizar a “atitude intencional” identificada por certos filósofos modernos da mente (ou filósofos da mente moderna) como um dos dispositivos inferenciais de base. Sendo mais precisos – visto que os índios são perfeitamente capazes de adotar as atitudes “física” e “funcional” (Dennett 1978) em sua vida cotidiana –, diríamos que estamos aqui diante de um ideal epistemológico que, longe de buscar reduzir a “intencionalidade ambiente” a zero a fim de atingir uma representação absolutamente objetiva do mundo, faz a aposta inversa: o conhecimento verdadeiro visa à revelação de um máximo de intencionalidade, por via de um processo de “abdução de agência” sistemático e deliberado.
> (p. 50-1)

# Xamanismo se preocupa com a intenção/ação (versus causalidade material objetivista)

> Dizíamos acima que o xamanismo era uma arte _política_. Dizemos, agora, que ele é uma _arte_ política. Pois a boa interpretação xamânica é aquela que consegue ver cada _evento_ como sendo, em verdade, uma _ação_, uma expressão de estados ou predicados intencionais de algum agente. O sucesso interpretativo é diretamente proporcional à ordem de intencionalidade que se consegue atribuir ao objeto ou noema. Um ente ou um estado de coisas que não se presta à subjetivação, ou seja, à determinação de sua relação social com aquele que conhece, é xamanisticamente insignificante – é um resíduo epistêmico, um “fator impessoal” resistente ao conhecimento preciso. Nossa epistemologia objetivista, desnecessário lembrar, toma o rumo oposto: ela considera a “atitude intencional” do senso comum como uma ficção cômoda, algo que adotamos quando o comportamento do objeto-alvo é complicado demais para ser decomposto em processos físicos elementares. Uma explicação científica exaustiva do mundo deve ser capaz de reduzir toda ação a uma cadeia de eventos causais, e estes a interações materialmente densas (sobretudo nada de “ação” à distância).
> (p. 51-2)

# Navalha de Occam x chocalho do xamã

> {17}: Referimo-nos aqui [[noema]] ao conceito de Dennett sobre a n-ordinalidade dos sistemas intencionais. Um sistema intencional de segunda ordem é aquele onde o observador atribui não apenas crenças, desejos e outras intenções ao objeto (primeira ordem), mas também crenças etc. sobre as outras crenças. A tese cognitivista mais aceita sustenta que apenas o _Homo sapiens_ exibe intencionalidade de ordem igual ou superior a dois. Observe-se que o princípio xamânico de “abdução de um máximo de agência” vai de encontro, evidentemente, aos dogmas da psicologia fisicalista: “Os psicólogos têm frequentemente recorrido ao princípio conhecido pelo nome de ‘cânon de parcimônia de Lloyd Morgan’, que pode ser visto como um caso particular da navalha de Occam. Esse princípio determina que se deve atribuir a um organismo o mínimo de inteligência, ou consciência, ou racionalidade suficientes para dar conta de seu comportamento” (Dennett 1978: 274). Efetivamente, o chocalho do xamã é um instrumento de tipo inteiramente diferente da navalha de Occam; esta pode servir para escrever artigos de lógica ou de psicologia, mas não é muito boa, por exemplo, para recuperar almas perdidas.
> (p. 51)

# Reduação a objeto na ciência moderna, redução ao sujeito no interpretação ameríndia

> Em suma, se no mundo naturalista da modernidade um sujeito é um objeto insuficientemente analisado, a convenção interpretativa ameríndia segue o princípio inverso: um objeto é um sujeito incompletamente interpretado.
> (p. 52)

> Aqui [[na interpretação ameríndia]], é preciso saber personificar, porque é preciso personificar para saber. O objeto da interpretação é a contrainterpretação do objeto. Pois este último deve ser expandido até atingir sua forma intencional plena – de espírito, de animal em sua face humana –, ou, no mínimo, ter sua relação com um sujeito demonstrada, isto é, ser determinado como algo que existe “na vizinhança” de um agente (Gell 1998).
> (p. 52)

# Na interpretação ameríndia, os objetos são "ações congeladas" (encarnação material de uma intencionalidade não-intencional)

> No que respeita a essa segunda opção, a ideia de que os agentes não-humanos percebem-se a si mesmos e a seu comportamento sob a forma da cultura humana desempenha um papel crucial. A tradução da “cultura” para os mundos das subjetividades extra-humanas tem como corolário a redefinição de vários eventos e objetos “naturais” como sendo índices a partir dos quais a agência social pode ser abduzida. O caso mais comum é a transformação de algo que, para os humanos, é um mero fato bruto, em um artefato ou comportamento altamente civilizados do ponto de vista de outra espécie: o que chamamos “sangue” é a “cerveja” do jaguar, o que tomamos como um barreiro lamacento os tapires experimentam como uma grande casa cerimonial, e assim por diante.
> (p. 52-3)

> Os artefatos possuem esta ontologia interessantemente ambígua: são coisas ou objetos, mas apontam necessariamente para uma pessoa ou sujeito, pois são como ações congeladas, encarnações materiais de uma intencionalidade não-material. E assim, o que uns chamam de “natureza” pode bem ser a “cultura” dos outros.
> (p. 53)

# Pressuposição antropomórfica (versus excepcionalidade antropocêntrica do humano)

> Essa pressuposição antropomórfica do mundo indígena contrasta incompativelmente com o obstinado esforço antropocêntrico de “construir” o humano como não-dado, como a essência mesmo do não-dado, esforço que transparece na filosofia ocidental, inclusive naquela de orientação mais, digamos, avançada (Sloterdijk 2000: 20-ss).
> (p. 54)

# Plano de imanência como passado "pré-histórico" absoluto presente no pensamento ameríndio (no mito)

> “Se é verdade que nós, homens modernos, temos o conceito, mas perdemos de vista o plano de imanência […]” (D. & G. [1991] 1997: 135). Tudo o que precede não é senão o desenvolvimento, por assim dizer dedutivo, efetuado pela prática teórica indígena, da intuição instauradora da mitologia do continente, a saber, aquela que postula a preexistência absoluta de um meio literalmente pré-histórico – algo como o “passado absoluto” de que falam alguns filósofos, aquele passado que nunca foi presente e que portanto nunca passou, enquanto o presente não cessa de passar – definido pela interpenetrabilidade ontológica e a coacessibilidade epistemológica de todos os “insistentes” que povoam e constituem esse meio, e que são os modelos e padrões dos existentes atuais.
> (p. 55)

> Como nos ensinam as Mitológicas, o mito indígena, enquanto atualização narrativa do plano de imanência, articula-se privilegiadamente em torno das causas e consequências da especiação – a investidura em uma corporalidade característica – dos personagens ou actantes que povoam esse plano, todos concebidos como compartilhando de uma condição geral instável na qual aspectos humanos e não-humanos se acham inextricavelmente emaranhados:
> 
> > Gostaria de fazer-lhe uma pergunta simples: que é um mito? [pergunta Didier Eribon] – Não é uma pergunta simples, é exatamente o contrário […]. Se você interrogar um índio americano, seriam muitas as chances de que a resposta fosse esta: uma história do tempo em que os homens e os animais ainda não eram diferentes. (Lévi-Strauss & Eribon [1988] 2005: 195-96)
> (p. 55-6)

# Discurso mítico como atualização de condição pré-cosmológica virtual (mistura entre corpo e espírito)

> A definição é de fato profunda: aprofundemo-la, então, ainda que em uma direção um pouco diferente da que Lévi-Strauss tinha em mente em sua resposta. O discurso mítico consiste em um registro do movimento de atualização do presente estado de coisas a partir de uma condição pré-cosmológica virtual dotada de perfeita transparência – um “caosmos” onde as dimensões corporal e espiritual dos seres ainda não se ocultavam reciprocamente. Esse pré-cosmos, muito longe de exibir uma “identidade” primordial entre humanos e não-humanos, como se costuma caracterizá-lo, é percorrido por uma diferença infinita, ainda que (ou justamente porque) interna a cada personagem ou agente, ao contrário das diferenças finitas e externas que constituem as espécies e as qualidades do mundo atual.
> (p. 56)

# Regime de multiplicidade qualitativa (mito não como história/processo, mas como metamorfose -- figuração do devir)

> Donde o regime de multiplicidade qualitativa próprio do mito: a questão de saber se o jaguar mítico, por exemplo, é um bloco de afetos humanos em forma de jaguar ou um bloco de afetos felinos em forma de humano é indecidível, pois a “metamorfose” mítica é um acontecimento, uma mudança não-espacial: uma superposição intensiva de estados heterogêneos, antes que uma transposição extensiva de estados homogêneos. Mito não é história porque metamorfose não é processo, “ainda não era” processo e “jamais será” processo; a metamorfose é anterior e exterior ao processo do processo – ela é uma figura (uma figuração) do devir.
> (p. 56)

# Mito como descrição do momento de passagem pré-cosmológico/mistura para cosmológico/separado (transparência/espírito e opacidade/corpo)

> A linha geral traçada pelo discurso mítico descreve, assim, a laminação instantânea dos fluxos pré-cosmológicos de indiscernibilidade ao ingressarem no processo cosmológico: doravante, as dimensões humana e felina dos jaguares (e dos humanos) funcionarão alternadamente como fundo e forma potenciais uma para a outra. A transparência originária ou _complicatio_ infinita se bifurca ou se explica, a partir de então, na invisibilidade (as almas humanas e os espíritos animais) e na opacidade (o corpo humano e as “roupas” somáticas animais) que marcam a constituição de todos os seres mundanos.
> (p. 56-7)

# Permanece o fundo de virtualidade (divisão é relativa e reversível)

> Essa invisibilidade e opacidade são, entretanto, relativas e reversíveis, uma vez que o fundo de virtualidade é indestrutível ou inesgotável (os grandes rituais indígenas de recriação do mundo são justamente os dispositivos de contraefetuação desse fundo indestrutível).
> (p. 57)

# Espíritos (modo virtual e infinito/autodiferente intensivamente) e existentes (modo atual e finito extensivamente)

O mito retrata personagens com diferenças internas infinitas.

> Esta “auto-”diferença é a propriedade característica dos agentes cosmológicos que designamos pela palavra “espíritos”; por isso, todos os seres míticos são concebidos como espíritos (e como xamãs), assim como, reciprocamente, todo existente atual ou “modo finito” pode se revelar agora, porque foi antes, um espírito, uma vez que sua razão de ser encontra-se relatada no mito.
> (p. 57)

# Ontologia comandada por uma diferença intensiva fluente (transformação/relação/intervalo) que se atualiza (forma/termos/ser)

> Em suma, o mito propõe um regime ontológico comandado por uma diferença intensiva fluente, que incide sobre cada ponto de um contínuo heterogêneo, onde a transformação é anterior à forma, a relação é superior aos termos, e o intervalo é interior ao ser. Cada ser mítico, sendo pura virtualidade, “já era antes” o que “iria ser depois”, e por isso não é, pois não permanece sendo, nada de atualmente determinado.
> (p. 58)

# Passagem do contínuo heterogêneo pré-cosmológico ao discreto homogêneo atual (grande tema da antropologia estruturalista). Espíritos como evidência de uma virtualidade mítica ainda presente.

> Em contrapartida, as diferenças extensivas introduzidas pela especiação (_lato sensu_) pós-mítica – a passagem do “contínuo” ao “discreto” que constitui o grande (mi)tema da antropologia estruturalista – cristalizam blocos molares de identidade interna infinita (cada espécie é internamente homogênea, seus membros são idêntica e indiferentemente representativos da espécie enquanto tal). Esses blocos estão separados por intervalos externos, quantizáveis e mensuráveis, uma vez que as diferenças entre as espécies são sistemas finitos de correlação, de proporção e de permutação de caracteres de mesma ordem e natureza.
> (p. 58-9)

> O contínuo heterogêneo do mundo pré-cosmológico dá assim lugar a um discreto homogêneo, nos termos do qual cada ser é só o que é, e só o é por não ser o que não é. Mas esses seres a que damos desajeitadamente o nome de “espíritos” são o testemunho de que nem todas as virtualidades foram atualizadas, e que o turbulento fluxo mítico continua a rugir surdamente por debaixo das tranquilas descontinuidades aparentes entre os tipos e espécies.
> (p. 59)

# Mito como discurso absoluto (diferenças entre pontos de vista são anuladas e amplificadas). Meio pré-objetivo e pré-subjetivo.

> O perspectivismo ameríndio conhece então no mito um lugar geométrico onde a diferença entre os pontos de vista é ao mesmo tempo anulada e exacerbada. Nesse discurso absoluto, cada espécie de ser aparece aos outros seres como aparece para si mesma – como humana –, e entretanto age como se já manifestando sua natureza distintiva e definitiva de animal, planta ou espírito. Ponto de fuga universal do perspectivismo, o mito fala de um estado do ser onde os corpos e os nomes, as almas e as ações, o eu e o outro se interpenetram, mergulhados em um mesmo meio pré-subjetivo e pré-objetivo.
> (p. 59)

# Especiação a partir da humanidade

> A passagem não é um processo de diferenciação do humano a partir do animal, como na vulgata evolucionista ocidental. _A condição original comum aos humanos e animais não é a animalidade, mas a humanidade._ A grande divisão mítica mostra menos a cultura se distinguindo da natureza que a natureza se afastando da cultura: os mitos contam como os animais perderam atributos herdados ou mantidos pelos humanos. Os não-humanos são ex-humanos, e não os humanos os ex-não-humanos.
> (p. 60)

> Assim, se nossa antropologia popular vê a humanidade como erguida sobre alicerces animais normalmente ocultos pela cultura – tendo outrora sido “completamente” animais, permanecemos, “no fundo”, animais –, o pensamento indígena conclui ao contrário que, tendo outrora sido humanos, os animais e outros existentes cósmicos continuam a sê-lo, mesmo que de uma maneira não evidente para nós.
> (p. 60)

# Fundo pré-cosmológico (são todos diferentes de si mesmos).

> O que o perspectivismo afirma, enfim, não é tanto a ideia de que os animais são “no fundo” semelhantes aos humanos, mas sim a de que eles, como os humanos, são outra coisa “no fundo”: eles têm, em outras palavras, um “fundo”, um “outro lado”; são diferentes de _si mesmos_.
> (p. 61)

# Perspectivismo coloca a diferença humano/não-humano para o interior de cada existente. Nem animismo, nem totemismo.

> Nem animismo – que afirmaria uma semelhança substancial ou analógica entre animais e humanos –, nem totemismo – que afirma uma semelhança formal ou homológica entre diferenças intra-humanas e diferenças interespecíficas –, o perspectivismo afirma uma diferença intensiva que traz a diferença humano/ não-humano _para o interior de cada existente_.
> (p. 61)

> Com isso, cada existente se encontra como que separado de si mesmo e tornado semelhante aos demais apenas sob a dupla condição subtrativa dessa comum autosseparação e de uma estrita complementaridade, pois se todos os modos do existente são humanos para si mesmos, nenhum é humano para (ou semelhante a) nenhum outro: a humanidade é “reciprocamente” _reflexiva_ (o jaguar é um homem para o jaguar, o queixada é um homem para o queixada), mas não pode ser _mútua_ (no momento em que o jaguar é um homem, o queixada não o é, e vice-versa).
> (p. 61-2)

> Esse parece-me ser, em última análise, o sentido da ideia de “alma” nas ontologias indígenas. Se todos os seres têm alma, nenhum deles, ninguém, coincide consigo mesmo. Se tudo pode ser humano, então nada é humano inequivocamente. A humanidade de “fundo” torna problemática a humanidade de “forma”, ou de “figura”. As súbitas inversões entre fundo e forma ameaçam constantemente o instável mundo transformacional ameríndio.
> (p. 62)

# Perspectivismo não como pluralidade de representações sobre uma natureza, mas como multiplicidade de naturezas atreladas a uma representação

> A teoria perspectivista ameríndia está de fato supondo uma pluralidade de _representações_ sobre o mesmo mundo, como pressupõe Århem? Basta considerar o que dizem as etnografias para perceber que é o exato inverso que se passa: todos os seres veem (“representam”) o mundo da _mesma maneira_ – o que muda é o mundo que eles veem. Os animais utilizam as mesmas “categorias” e “valores” que os humanos: seus mundos giram em torno da caça e da pesca, da cozinha e das bebidas fermentadas, das primas cruzadas e da guerra, dos ritos de iniciação, dos xamãs, chefes, espíritos etc. Se a Lua, as cobras e os jaguares veem os humanos como tapires ou queixadas, é porque, como nós, esses perigosos predadores comem tapires e queixadas, comida própria de humanos. Só poderia ser assim, pois, sendo humanos entre si mesmos, “em seu departamento”, os não-humanos veem as coisas como os humanos as veem – isto é, como nós humanos as vemos em _nosso_ departamento. Mas as coisas _que eles veem_, quando veem _como nós_ vemos, são _outras_: o que para nós é sangue, para os jaguares é cerveja; o que para as almas dos mortos é um cadáver podre, para nós é mandioca fermentando; o que vemos como um barreiro lamacento, para os tapires é uma grande casa cerimonial, e assim por diante.
> (p. 63-4)

# O perspectivismo é multinaturalismo, porque uma perspectiva não é uma representação

> O relativismo cultural, um “multiculturalismo”, supõe uma diversidade de representações subjetivas e parciais, incidentes sobre uma natureza externa, una e total, indiferente à representação. Os ameríndios propõem o oposto: de um lado, uma unidade representativa puramente pronominal – é humano quem ocupa vicariamente a posição de sujeito cosmológico; todo existente pode ser pensado como pensante (“isto existe, logo isto pensa”), isto é, como “ativado” ou “agenciado” por um ponto de vista –; do outro lado, uma radical diversidade real ou objetiva. O perspectivismo é um multinaturalismo, pois uma perspectiva não é uma representação.
> (p. 65)

# O ponto de vista cria o sujeito, não o objeto.

> {9}: O ponto de vista cria, não o objeto, como diria Saussure, mas o sujeito mesmo. “É esse o fundamento do perspectivismo. Este não significa uma dependência em face de um sujeito definido previamente: ao contrário, será sujeito aquele que vier ao ponto de vista […]” (Deleuze 1988: 27).
>  (p. 65)

# O ponto de vista está no corpo. A representação está no espírito.

> Uma perspectiva não é uma representação porque as representações são propriedades do espírito, mas o _ponto de vista está no corpo_. Ser capaz de ocupar o ponto de vista é sem dúvida uma potência da alma, e os não-humanos são sujeitos na medida em que têm (ou são) um espírito; mas a diferença entre os pontos de vista – e um ponto de vista não é senão diferença – não está na alma. Esta, formalmente idêntica através das espécies, só enxerga a mesma coisa em toda parte; a diferença deve então ser dada pela especificidade dos corpos.
> (p. 65-6)


# A diferença de ponto de vista deve ter a ver com uma diferença no corpo (afetos, afecções, ethos)

> Os animais veem da mesma forma que nós coisas diversas do que vemos porque seus corpos são diferentes dos nossos. Não estou me referindo a diferenças de fisiologia – quanto a isso, os ameríndios reconhecem uma uniformidade básica dos corpos –, mas aos afetos que atravessam cada espécie de corpo, as afecções ou encontros de que ele é capaz (para evocarmos a distinção espinosista), suas potências e disposições: o que ele come, como se move, como se comunica, onde vive, se é gregário ou solitário, tímido ou agressivo…
> (p. 66)

> O que estamos chamando de “corpo”, portanto, não é uma fisiologia distintiva ou uma anatomia característica; é um conjunto de maneiras ou modos de ser que constituem um _habitus_, um _ethos_, um etograma. Entre a subjetividade formal das almas e a materialidade substancial dos organismos, há esse plano central que é o corpo como feixe de afetos e capacidades, e que é a origem das perspectivas. Longe do essencialismo espiritual do relativismo, o perspectivismo é um _maneirismo corporal_.
> (p. 66)

# Regime de multiplicidade, não de identidade. Existe sangue|cerveja, não um x que seria a coisa-em-si.

Visão rizomática (acentrada) ~ relação com disjunção inclusiva (síntese disjuntiva) em Deleuze.

>  O que existe na multinatureza não são entidades autoidênticas diferentemente percebidas, mas multiplicidades imediatamente relacionais do tipo sangue|cerveja. Só existe o _limite_ entre o sangue e a cerveja, a rigor; a borda por onde essas duas substâncias “afins” comunicam e divergem.10 Não há, enfim, um x que seja sangue para uma espécie e cerveja para outra; há, desde o início, um sangue | cerveja que é uma das singularidades ou afecções características da multiplicidade humano | jaguar. A semelhança afirmada entre humanos e jaguares ao fazer com que ambos bebam “cerveja” não está lá senão para que melhor se perceba o que faz a diferença entre humanos e jaguares. “Estamos em uma língua ou em outra – não há mais supralíngua [_arrière-langue_] como não há supramundo [_arrière-monde_]” (Jullien 2008: 135). Efetivamente, estamos no sangue ou na cerveja, ninguém bebe a bebida-em-si; mas no mundo indígena, toda cerveja tem um travo de sangue, e vice-versa.
> (p. 67)

# Corpo como instrumento de disjunção referencial. O problema do perspectivismo não passa pelo da representação (do uno real/verdadeiro), mas o da equivocidade (~equívoco) presente na diferença de perspectivas.

> Assim, a posse de uma alma similar implica a posse de conceitos análogos por parte de todos os existentes. O que muda quando se passa de uma espécie existente a outra é a referência destes conceitos: o corpo é o sítio e o instrumento da disjunção referencial entre os “discursos” (os semiogramas) de cada espécie. O problema do perspectivismo ameríndio não é o de encontrar a referência comum (o planeta Vênus, digamos) a duas representações diferentes (“Estrela Matutina” e “Estrela Vespertina”, então), mas, ao contrário, o de contornar o equívoco que consistiria em imaginar que quando o jaguar diz “cerveja de mandioca” ele esteja se referindo à mesma coisa que nós, apenas porque ele “quer dizer” a mesma coisa que nós.
> (p. 67-8)

# Perspectivismo como epistemologia constante (mesmas representações) e ontologias variáveis (múltiplas referências)

> Em outras palavras, o perspectivismo supõe uma epistemologia constante e ontologias variáveis: mesmas representações, mas outros objetos; sentido único, mas referências múltiplas.
> (p. 68)

# Tradução perspectivista foca em não perder de vista a diferença oculta nos homônimos equívocos que conectam-separam a língua das espécies

> O propósito da tradução perspectivista – uma das principais tarefas dos xamãs – não é portanto o de encontrar um _sinônimo_ (uma representação correferencial) em nossa língua conceitual humana para as representações que outras espécies utilizam para falar de uma mesma coisa “lá fora”; o propósito, ao contrário, é não perder de vista a diferença oculta dentro dos _homônimos_ equívocos que conectam-separam nossa língua e a das outras espécies.
> (p. 68)

# Caridade interpretativa da antropologia ocidental x multiplicidade referencial da alter-antropologia ameríndia (?)

> Se nossa antropologia ocidental está fundada no princípio de caridade interpretativa (a boa vontade do pensador, sua tolerância diante da humanidade incoativa ou atrofiada do outro), que afirma uma sinonímia natural entre as culturas humanas, a alter-antropologia ameríndia afirma, muito ao contrário, uma homonímia contranatural entre o discurso das espécies vivas, a qual está na origem de toda sorte de equívocos fatais. Este então é o equivalente ameríndio de nosso princípio de precaução: um mundo composto de uma “indefinidade” de focos acesos de intencionalidade não pode senão conter uma boa dose de más intenções.
> (p. 68)

# O multinaturalismo afirma a variação como natureza

> A noção de multinaturalismo se mostra útil, neste sentido, por seu caráter paradoxal: nosso macroconceito de “Natureza” não admite um verdadeiro plural e parece sempre pedir uma inicial maiúscula; isso nos leva espontaneamente a perceber o solecismo ontológico contido na ideia de “(várias) naturezas”, e portanto a realizar o deslocamento corretivo que ela impõe.
> (p. 69)

> Parafraseando a conhecida passagem de Deleuze sobre o relativismo (1988: 30), diríamos então que o multinaturalismo amazônico não afirma uma variedade de naturezas, mas a naturalidade da variação, a variação como natureza.
> (p. 69)

> A inversão da fórmula ocidental do multiculturalismo incide não apenas sobre os _termos_ (natureza e cultura) em sua determinação respectiva pelas _funções_ de unidade e diversidade, mas igualmente sobre as posições de “termo” e de “função”. Os leitores antropólogos reconhecerão aqui, bem entendido, a fórmula canônica do mito de Lévi-Strauss ([1955b] 1958: 252-53): o multinaturalismo perspectivista é uma transformação em dupla torção do multiculturalismo ocidental. Ele assinala o cruzamento de um umbral ou limite, um limiar semiótico-histórico que é um limiar de tradutibilidade e de equivocidade; um limiar, justamente, de transformação perspectiva.
> (p. 69)

