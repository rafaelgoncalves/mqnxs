---
title: Jamais fomos modernos
tags:
 - Bruno Latour
 - modernidade
 - amoderno
 - antropologia
 - antropologia simétrica
 - não-humanos
 - humanidade
 - natureza
 - cultura
 - naturezas-cultura
 - redes sociotécnicas

date: 2024-03-26
category: fichamento
path: "/jamais-fomos-modernos"
featuredImage: "latour.jpg"
srcInfo: <a href="https://commons.wikimedia.org/wiki/File:Bruno_Latour_(4381542885).jpg">Bruno Latour</a>
published: true
---

Fichamento do ensaio _Jamais fomos modernos_ de Bruno Latour[^1].

[^1]: Latour, Bruno. 2019 [1994]. _Jamais fomos modernos: ensaio de antropologia simétrica_. São Paulo: Editora 34.  4a Ed.

# Proliferação dos híbridos [quase-objetos, quase-sujeitos]

> Multiplicam-se os artigos híbridos que delineiam tramas de ciência, política, economia, direito, religião, técnica, ficção. Se a leitura do jornal diário é a reza do homem moderno, quão estranho é o homem que hoje reza lendo estes assuntos confusos. Toda a cultura e toda a natureza são diariamente reviradas aí.
> (p. 10)

> Contudo, ninguém parece estar preocupado. […] Não misturemos o céu e a terra, o global e o local, o humano e o inumano. […] O navio está sem rumo: à esquerda o conhecimento das coisas, à direita o interesse, o poder e a política dos homens.
> (p. 10-1)

# Crítica amparada na Naturza, Sociedade ou Discurso [Changeaux, Bordieu e Derrida]

(p. 12-4)

# Crise da crítica, "o miraculoso ano de 1989", dupla falência do naturalismo (crise climática) e do socialismo (queda do muro): modernos, antimodernos e pós-modernos

(p. 18-9)

# O que é um moderno?

> A modernidade possui tantos sentidos quantos forem os pensadores ou jornalistas. Ainda assim, todas as definições apontam, de uma forma ou de outra, para a **passagem do tempo**. Através do adjetivo moderno, assinalamos **um novo regime, uma aceleração, uma ruptura, uma revolução do tempo**. Quando as palavras “moderno”, “modernização” e “modernidade” aparecem, definimos, por contraste, um passado arcaico e estável. Além disso, a palavra encontra-se sempre colocada em meio a uma polêmica, em uma briga onde há ganhadores e perdedores, os Antigos e os Modernos. **“Moderno”, portanto, é duas vezes assimétrico: assinala uma ruptura na passagem regular do tempo; assinala um combate no qual hä vencedores e vencidos.**
> (p.  20)

# Modernos: purificação e mediação/tradução

> A hipótese deste ensaio — trata-se de uma hipótese e também de um ensaio — é que a palavra “moderno” designa dois corjuntos de práticas totalmente diferentes que, para permanecerem eficazes, devem permanecer distintas, mas que recentemente deixaram de sé-lo. O **primeiro conjunto de prâticas cria, por “traduçäo”, misturas entre gêneros de seres completamente novos, hibridos de natureza e cultura. O segundo cria, por “purificação”, duas zonas ontolôgicas inteiramente distintas, a dos humanos, de um lado, e a dos não-humanos, de outro.** Sem o primeiro conjunto, as prâticas de purificaçäo seriam vazias ou supérfluas. Sem o segundo, o trabalho da traduçäo seria freado, limitado ou mesmo interditado. **O primeiro conjunto corresponde aquilo que chamei de redes, o segundo ao que chamei de critica**. O primeiro, por exemplo, conectaria em uma cadeia continua a quimica da alta atmosfera, as estratégias cientificas e industriais, as preocupaçôes dos chefes de Estado, as angustias dos ecologistas; o segundo estabeleceria uma partiçäo entre um mundo natural que sempre esteve aqui, uma sociedade com interesses e questôes previsiveis e estaveis, e um discurso independente tanto da referência quanto da sociedade. 
> (p. 20-1)

# Jamais fomos modernos

> Enquanto considerarmos separadamente estas práticas, seremos realmente modernos, ou seja, estaremos aderindo sinceramente ao projeto da purificação crítica, ainda que este se desenvolva somente através da proliferação dos híbridos. **À partir do momento em que desviamos nossa atenção simultaneamente para o trabalho de purificação e o de hibridação {mediação, tradução}, deixamos instantaneamente de ser modernos, nosso futuro começa a mudar**. Ao mesmo tempo, **deixamos de ter sido modernos**, no pretérito, pois tomamos consciéncia, retrospectivamente, de que os dois conjuntos de praticas estiveram operando desde sempre no periodo histérico que se encerra. Nosso passado começa a mudar. Enfim, **se jamais tivéssemos sido modernos**, pelo menos näo da forma como a critica nos narra, as relaçôes tormentosas que estabelecemos com as outras naturezas-culturas seriam transformadas. O relativismo, a dominaçäo, o imperialismo, a mâ fé, o sincretismo seriam todos explicados de outra forma, modificando entäoQual o laço existente entre o trabalho de tradução ou de mediação e o de purificação? Esta é a questão que eu gostaria de esclarecer. À hipôtese, ainda muito grosseira, é que a segunda possibilitou a primeira; quanto  a antropologia comparada. 
> (p. 21)

# Questões tratadas no ensaio

> **{1} Qual o laço existente entre o trabalho de tradução ou de mediação e o de purificação?** Esta é a questão que eu gostaria de esclarecer. À hipôtese, ainda muito grosseira, é que **a segunda possibilitou a primeira; quanto mais nos proibimos de pensar os hibridos, mais seu cruzamento se torna possivel**; este é o paradoxo dos modernos que esta situaçäo excepcional €m que nos encontramos nos permite enfim captar. **{2} À segunda questäo diz respeito aos pré-modernos, às outras naturezas-culturas. À hipôtese, também demasiado ampla, é que, ao se dedicar a pensar os hibridos, eles näo permitiram sua proliferação.** É esta diferença que nos permitiria explicar a Grande Separaçäo entre Nós e Eles, e que permitiria resolver finalmente a insolúvel questäo do relativismo. **{3} À terceira questäo diz respeito à crise atual: se a modernidade foi assim täo eficaz em seu trabalho de separaçäo e de proliferaçäo, por que ela está enfraquecendo hoje, nos impedindo de sermos modernos de fato? Daí a ûltima questäo {4} que é também a mais dificil: se deixamos de ser modernos, se näo podemos mais separar o trabalho de proliferaçäo e o trabalho de purificaçäo, o que iremos nos tornar?** Como desejar as Luzes sem a modernidade? A hipôtese, também por de- masiado enorme, é de que serâ preciso reduzir a marcha, curvar e regular a proliferaçäo dos monstros através da representaçäo oficial de sua exis- téncia. Seria necessdria uma outra democracia? Uma democracia estendi- da às coisas? Para responder a estas perguntas, deverei distinguir entre os pré-modernos, os modernos, e mesmo entre os pés-modernos aquilo que cles têm de durâvel e o que têm de fatal. Perguntas demais, bem o sei, para um ensaio que näo tem outra desculpa que näo sua brevidade. Nietzsche dizia, sobre os grandes problemas, que eram como os banhos frios: é preciso entrar râpido e sair da mesma forma.  
> (p. 21-2)


# Dupla separação

```
          Deus barrado
                |
          2a separação
                |
Sujeito---1a separacao--Objeto
Sociedade               Não-humano
Humano
```
(p. 23)

# Boyle, o método empírico dos _matter of facts_ e a criação da Natureza

Shapin e Schaffeur

(p. 26-9)

# Hobbes, o método dedutivo e a criação da Sociedade

(p. 30-2)

# Não-humanos a partir de Boyle

> Eis que intervém, na escrita de Boyle, um novo ator reconhecido pela nova Constituição: **corpos inertes, incapazes de vontade e de preconceito, mais capazes de mostrar, de assinar, de escrever e de rabiscar sobre os instrumentos de laboratório testemunhos dignos de fé. Estes não-humanos, privados de alma, mas aos quais é atribuído um sentido, chegam a ser mais confiáveis que o comum dos mortais, aos quais é atribuída uma vontade, mas que não possuem a capacidade de indicar, de forma confiável, os fenômenos.** De acordo com a constituição, em caso de dúvida, mais vale apelar aos não-humanos para refutar os humanos. Dotados de seus novos poderes semióticos, aqueles irão contribuir para uma nova forma de texto, o artigo de ciência experimental, híbrido entre o estilo milenar da exegese biblica — até então aplicado exclusivamente às Escrituras e aos classicos — e o novo instrumento que produz novas inscriçôes. À partir de entäo, será em torno da bomba de ar em seu espaço fechado, e a respeito do comportamento dotado de sentido dos näo-humanos, que as testemunhas iräo continuar seus debates.
> (p. 36-7)

# Rede científica

> Ao seguirem a reprodução de cada protótipo de bomba de ar através da Europa e a transformação progressiva de um equipamento custoso, pouco confiável e atravancante em uma caixa preta de baixo custo, que aos poucos se torna um equipamento comum em todos os laboratórios, os autores {Shapin e Schaffeur} **trazem a aplicaçäo universal de uma lei física de volta ao interior de uma rede de práticas padronizadas. Evidentemente, a interpretaçäo da elasticidade do ar dada por Boyle se propaga, mas se propaga exatamente com a mesma velocidade que a comunidade dos experimentadores e seus equipamentos se desenvolvem. Nenhuma ciéncia pode sair da rede de sua pratica. O peso do ar certamente continua a ser um universal, mas um universal em rede.** Graças à extensäo desta, as competências e o equipamento podem tornar-se suficientemente rotineiros para que a produçäo do vâcuo torne-se täo invisivel quanto o ar que respiramos, mas universal como antigamente, nunca. 
> (p. 37-8)

# Representação política e científica

> Em seus debates comuns, os descendentes de Hobbes e de Boyle nos fornecem os recursos que usamos até hoje: **de um lado, a força social, o poder; do outro, a força natural, o mecanismo. De um lado, o sujeito de direito; do outro, o objeto da ciéncia. Os porta-vozes politicos iräo representar a multidäo implicante e calculadora dos cidadäos; 0s porta-vozes cientificos irão de agora em diante representar a multidäo muda e material dos objetos.** Os primeiros traduzem aqueles que 0s enviam, que näo saberiam como falar todos ao mesmo tempo; os segundos traduzem aqueles que representam, que säo mudos de nascimento. Os primeiros podem trair, os segundos também. No século XVII, a simetria ainda é visivel, os porta-vozes ainda disputam entre si, acusando-se mutuamente de multiplicar as fontes de conflito. Basta apenas um pequeno esforço para que sua origem comum torne-se invisivel, para que sé haja um porta-voz do lado dos homens, para que a mediaçäo dos cientistas torne-se invisivel. Em breve à palavra “representaçäo” tomara dois sentidos diferentes, dependendo de estarmos falando de eleitos ou de coisas. 
> (p. 43-4)

# A constituição moderna

![Figura 2](./jfm2.png)

> Três vezes a transcendência e três vezes a imanência em uma tabela que fecha todas as possibilidades. Nós não criamos a natureza; nós criamos a sociedade; nós criamos a natureza: nós não criamos a sociedade; nós não criamos nem uma nem outra, Deus criou tudo; Deus não criou nada, nós criamos tudo.
> (p. 49)

> A modernidade não tem nada a ver com a invenção do humanismo, com a irrupção das ciências, com a laicizaçäo da sociedade, ou com a mecanização do mundo. **Ela é a produção conjunta destas três duplas de transcendência e imanência, através de uma longa história da qual apresentei apenas uma etapa por intermédio das figuras de Hobbes e Boyle. O ponto essencial desta Constituição moderna é o de tornar invisível, impensável, irrepresentável o trabalho de mediação que constrói os híbridos.**
> (p. 50)

> É nesta dupla linguagem que reside a potência crítica dos modernos: podem mobilizar a natureza no seio das relações sociais, ao mesmo tempo em que a mantêm infinitamente distante dos homens; são livres para construir e desconstruir sua sociedade, ao mesmo tempo em que tornam suas leis inevitáveis, necessárias e absolutas.
> (p. 53)

# A prudência dos pré-modernos

> **O duaiismo natureza/sociedade é indispensável aos modernos para que possam, justamente, aumentar a escala dos mistos entre objetos e sujeitos**. Os pré-modernos, por no fundo serem todos monistas na constituiçäo de suas naturezas-culturas, se acreditarmos no que dizem os antropólogos (Lévi-Strauss, 1952), se proibem, pelo contririo, de praticar aquilo que suas representaçôes aparentemente permitiriam. “O indigena é um capitalizador lógico”, disse Lévi-Strauss, “ele refaz, sem cessar, os laços, redobra incansavelmente sobre si mesmos todos os aspectos do real, sejam eles fisicos, sociais ou mentais” (Lévi-Strauss, 1962, p.353). **Ao saturar com conceitos os mistos de divino, humano e natural, limitam a expansäo prâtica destes mistos. É a impossibilidade de mudar a ordem social sem modificar a ordem natural — e inversamente — que obriga os pré-modernos, desde sempre, a ter uma grande prudência.**
> (p. 58)

# Pós-modernos, não-modernos e antimodernos

(p. 60-66)

# Incomensurabilidade (Kant, Hegel/dialética, Bachelard/fenomenologia, pré-pós-modernos (Changeaux, Lacan e Habermas), Lyotard/pós-modernismo)

(p. 67-78)

# Virada semiótica (Greimas, Eco, Barthes, Derrida)

(p. 79-82)

# Isolamento do Ser e dos entes (Heiddeger) (?)

(p. 82-84)

# Temporalidade moderna

> Já que tudo aquilo que acontece é para sempre eliminado, **os modernos têm realmente a sensação de uma flecha irreversível do tempo, de uma capitalização, de um progresso.** Mas como esta temporalidade é imposta a um regime temporal que corre de forma totalmente diversa, os sintomas de um desentendimento se multiplicam. Como Nietzsche havia observado, os modernos têm a doença da história. Querem guardar tudo, datar tudo, por- que pensam ter rompido definitivamente com seu passado. **Quanto mais revoluçôes eles acumulam, mais eles conservam; quanto mais capitalizam, mais colocam no museu. À destruição maniaca é paga simetricamente por uma conservaçäo também maniaca.**
> (p. 86)

> À temporalidade moderna nada tem de “judaico-cristã” e, felizmen- te, nada tem de durável também. É uma projeção do Império do Centro sobre uma linha transformada em flecha através da separaçäo brutal entre aqui- lo que näo tem histéria mas que ainda assim emerge na histéria — as coi- sas da natureza — e aquilo que nunca deixa a histéria — os trabalhos e as paixôes do homem. À assimetria entre natureza e cultura torna-se entäo uma assimetria entre passado e futuro. O passado era a confusäo entre as coisas e os homens; o futuro, aquilo que näo os confundira mais. À moderniza- ¢ao consiste em sair sempre de uma idade de trevas que misturava as neces- sidades da sociedade com a verdade cientifica para entrar em uma nova idade que irá, finalmente, distinguir de forma clara entre aquilo que pertence à natureza intemporal e aquilo que vem dos humanos. O tempo moderno provém de uma superposição da diferença entre o passado e o futuro com esta outra diferença, mais importante, entre a mediação e a purificação. O presente é traçado por uma série de rupturas radicais, as revoluções, que formam engrenagens irreversiveis para impedir-nos, para sempre, de vol- tar atrás. Em si mesma, esta linha é tão vazia quanto a escansão de um metrônomo. É sobre ela, entretanto, que os modernos iräo projetar a mul- tiplicação dos quase-objetos e traçar, graças a eles, duas séries de progres- são: uma para cima, o progresso; outra para baixo, a decadência. 
> (p. 89-90)

# Contrarrevolução copernicana

![Figura 8](./jfm8.png)

> Dou o nome de **contrarrevolução copernicana a esta inversäo da inversäo. Ou antes este deslizamento dos extremos rumo ao centro e para baixo, que faz girar tanto o objeto quanto o sujeito em torno da prática dos quase-objetos e dos mediadores.** Näo precisamos apoiar nossas explicaçôes nestas duas formas puras, o objeto ou o sujeito-sociedade, já que elas säo, ao contrario, **resultados parciais e purificados da pratica central, a única que nos interessa.** Säo produto do _craking_ purificador, e näo sua matéria prima. À natureza gira, de fato, mas näo ao redor do sujeito-sociedade. Ela gira em torno do coletivo produtor de coisas e de homens. O sujeito gira, de fato, mas não em torno da natureza. Ele é obtido a partir do coletivo produtor de homens e de coisas. O Império do Centro se encontra, enfim, representado. Âs naturezas e sociedades säo os seus satélites. 
> (p. 99)

# Ontologias de geometria variável

![Figura 9](./jfm9.png)

> A ontologia dos mediadores, portanto, possui uma geometria variável. O que Sartre dizia dos humanos, que sua existência precede sua essência, é válido para todos os actantes, a elasticidade do ar, a sociedade, a matéria e a consciência. Não temos que escolher entre o vácuo nº 5, realidade da natureza exterior cuja essência não depende de nenhum humano, e o vácuo nº 4, representação que os pensadores ocidentais levaram séculos para definir. Ou antes, só poderemos escolher entre os dois quando houverem ambos sido estabilizados. Não podemos afirmar se o vácuo nº 1, muito instável no laboratório de Boyle, é natural ou social, mas apenas que ocorre artificialmente no laboratório. O vácuo nº 2 pode ser um artefato fabricado pela mao do homem, a menos que se transmute em vácuo nº 3, que começa a tornar-se uma reação que escapa aos homens. **O que é o vácuo, então? Nenhuma destas posições. _A essência do vácuo é a trajetória que liga todas elas. Em outras palavras, a elasticidade do ar possui uma histéria_. Cada um dos actantes possui uma assinatura única no espago desdobrado por esta trajetôria.** Para tracá-los, não precisamos construir nenhuma hipótese sobre a esséncia da natureza ou a da sociedade. Basta superpor todas estas assinaturas para obter a forma que os modernos chamam erroneamente, para resumir e purificar, de “natureza” e “sociedade”.
> (p. 108-9)

> Agora podemos compreender melhor o paradoxo dos modernos. Uma vez que utilizavam ao mesmo tempo o trabalho de mediação e o de purificação, mas representavam apenas o segundo, eles jogavam ao mesmo tempo com a transcendência e com a imanência das duas instâncias da natureza e da sociedade. O que resultava em quatro recursos contraditórios, que lhes permitiam fazer tudo e qualquer coisa. **Ora, se traçarmos o mapa das variedades ontológicas, iremos perceber que não há quatro regiôes, mas somente três. À dupla transcendência da natureza, de um lado, e da sociedade, do outro, corresponde às essências estabilizadas. Em compensação, a imanência das naturezas-naturantes e dos coletivos corresponde a uma mesma e única região, a da instabilidade dos eventos, a do trabalho de mediação.** (p. 110)
> 
# Primeiro princípio de simetria (Bloor)

> Este é o primeiro princípio de simetria, que abalou os estudos sobre as ciéncias e as técnicas, ao **exigir que o erro e a verdade fossem tratados da mesma forma** (Bloor, 1982). Até então, a sociologia do conhecimento só explicava, através de uma grande quantidade de fatores sociais, os desvios em relaçäo à trajetória retilinea da razäo. O erro podia ser explicado socialmente, mas a verdade continuava a ser sua propria explicação. Era possivel analisar a crença em discos voadores, mas não o conhecimento dos buracos negros; era possivel analisar as ilusões da parapsicologia, mas nao o saber dos psicôlogos; os erros de Spencer, mas não as certezas de Darwin. Fatores sociais do mesmo tipo náo podiam ser igualmente aplicados aos dois. Nestes dois pesos, duas medidas, encontramos a antiga divisäo da antropologia entre ciéncias — impossiveis de estudar — e etnociéncias — possiveis de estudar.
> (p. 116)

> O princípio de simetria reestabelece, pelo contrário, a continuidade, a historicidade e, vale lembrar, a Justica. Bloor é o anti-Canguilhem, da mesma forma que Serres é o anti-Bachelard, o que, por sinal, explica a total incompreensáo, na França, tanto da sociologia das ciéncias quanto da antropologia de Serres (Bowker e Latour, 1987). “O único mito puro é a idéia de uma ciéncia purificada de qualquer mito”, escreve este último ao romper com a epistemologia (Serres, 1974, p.259).
> (p. 117)

# O segundo princípio de simetria, ou o princípio de simetria generalizado (Callon)

![Figura 10](./jfm10.png)

> Para que a antropologia se torne simétrica, portanto, não basta que acoplemos a ela o primeiro princípio de simetria — que só dá cabo das injustiças mais óbvias da epistemologia. É preciso que a antropologia absorva aquilo que Michel Callon chama de princípio de simetria generalizada: **o antropólogo deve estar situado no ponto médio, de onde pode acompanhar, ao mesmo tempo, a atribuição de propriedades não humanas e de propriedades humanas** (Callon, 1986). Não lhe é permitido usar a realidade exterior para explicar a sociedade, nem tampouco usar os jogos de poder para dar conta daquilo que molda a realidade externa. Também não lhe é permitido alternar entre o realismo natural e o realismo sociológico, usando “não apenas” a natureza, “mas também” a sociedade, a fim de conservar as duas assimetrias iniciais, ao mesmo tempo em que dissimula as fraquezas de uma sob as fraquezas da outra (Latour, 1989a).
> (p. 120-1)

# As duas grandes divisões (natureza/cultura [humanos/não-humanos] --> "Nós"/"eles" [Ocidente/Outro])

![Figura 11](./jfm11.png)

> A Grande Divisão interior explica, portanto, a Grande Divisão exterior: apenas nós diferenciamos de forma absoluta entre a natureza e a cultura, entre a ciência e a sociedade, enquanto que todos os outros, sejam eles chineses ou ameríndios, zandés ou barouyas, não podem separar de fato aquilo que é conhecimento do que é sociedade, o que é signo do que é coisa, o que vem da natureza como ela realmente é daquilo que suas culturas requerem. Não importa o que eles fizerem, por mais adaptados, regrados e funcionais que possam ser, permanecerão eternamente cegos por esta confusão, prisioneiros tanto do social quanto da linguagem. Não importa o que nós façamos, por mais criminosos ou imperialistas que sejamos, escapamos da prisão do social ou da linguagem e temos acesso às próprias coisas através de uma porta de saída providencial, a do conhecimento científico. A partição interior dos não-humanos define uma segunda partição, desta vez externa, através da qual os modernos são separados dos pré-modernos. Nas culturas Deles, a natureza e a sociedade, os signos e as coisas são quase coextensivos. Em Nossa cultura, ninguém mais deve poder misturar as preocupações sociais e o acesso às coisas em si.
> (p. 125)

# Naturezas-culturas

> Mas se efetuarmos a superposição desses dois lugares — aquele que, sem maiores esforços, o etnólogo ocupa para estudar as culturas e aquele que, a muito custo, definimos para estudar nossa cultura —, a antropologia comparada torna-se possível, ou mesmo simples. Ela não mais compara as culturas colocando a sua de lado, como se esta possuísse, por um espantoso privilégio, a natureza universal. **Ela compara naturezas-culturas.** Seriam estas realmente comparáveis? Semelhantes? Iguais? Talvez agora possamos resolver a insolúvel questáo do relativismo.
> (p. 121)

> Aquilo que a razão complica, as redes explicam. A peculiaridade dos ocidentais foi a de ter imposto, através da Constituição, a separação total dos humanos e dos não-humanos — Grande Divisão interior — tendo assim criado artificialmente o choque dos outros. “Como alguém pode ser persa?” Como é possível que alguém não veja uma diferença radical entre a natureza universal e a cultura relativa? **Mas a própria noção de cultura é um artefato criado por nosso afastamento da natureza. Ora, não existem nem culturas — diferentes ou universais — nem uma natureza universal. Existem apenas naturezas-culturas, as quais constituem a única base possível para comparações.** A partir do momento em que levamos em conta tanto as práticas de mediação quanto as práticas de purificação, percebemos que nem bem os modernos separam os humanos dos não-humanos nem bem os “outros” superpõem totalmente os signos e as coisas (Guille-Escuret, 1989).
> (p. 129-30)

# O terceiro princípio de simetria (Latour)

> Suponhamos que, tendo voltado definitivamente dos trópicos, a anropologia decida ocupar uma posicáo triplamente simétrica: explica com s mesmos termos as verdades e os erros — é o primeiro principio de sietria; estuda a0 mesmo tempo a produção dos humanos e dos näo-humanos — é o princípio de simetria generalizada; **finalmente, ocupa uma posição intermediária entre os terrenos tradicionais e os novos, porque suspende toda e qualquer afirmação a respeito daquilo que distinguiria os ocidentais dos Outros.** É verdade, ela perde o exotismo, mas ganha novos terrenos que lhe permitirão estudar o dispositivo central de todos os coletivos, até mesmo os nossos. Ela perde sua ligação exclusiva com as culturas — ou com as dimensões culturais —, mas ganha as naturezas, o que tem um valor inestimável. As duas posições que situei desde o início deste ensaio — aquela que o etnólogo ocupava sem fazer esforço, e aquela que o analista das ciências pesquisava com tanta dificuldade — podem agora ser superpostas. A analise das redes estende a mão à antropologia e lhe oferece a posição central que havia preparado para ela.
> (p. 129)

# Relativismo absoluto, relativismo cultural, universalismo particular e relativismo relativista (antropologia simétrica)

![Figura 12](./jfm12.png)

> Posso agora comparar as formas de relativismo seguindo o critério de elas levarem ou näo em conta a construção das naturezas. O **relativismo absoluto** supõe culturas separadas e incomensuráveis que nenhuma hierarquia seria capaz de ordenar, É inútil falar sobre ele, uma vez que ele coloca a natureza entre parénteses. No que diz respeito ao **relativismo cultural**, mais sutil, a natureza entra em cena, mas para existir ela não supôe nenhuma sociedade, nenhuma construcáo, nenhuma mobilizaçäo, nenhuma rede. Trata-se portanto da natureza revista e corrigida pela epistemologia, para a qual a prática cientifica continua fora do jogo. Para esta tradiçäo, as culturas estáo repartidas como diversos pontos de vista mais ou menos precisos sobre esta natureza única. Algumas sociedades a enxergam “em uma nuvem”, outras em uma névoa espessa, outras em tempo claro. Os racionalistas irão insistir nos aspectos comuns de todos estes pontos de vista, os relativistas na deformação irresistível imposta pelas estruturas sociais a todas as percepções (Hollis e Lukes, 1982). Os primeiros serão derrotados se pudermos mostrar que as culturas não superpõem suas categoriais; os segundos ficarão enfraauecidos se nudermos provar aue elas se superpõem (Brown, 1976).
> (p. 130)

> **Na prática, portanto, assim que a natureza entra em jogo sem estar ligada a uma cultura em particular, há sempre um terceiro modelo que empregamos por debaixo dos panos, que é o do universalismo que eu chamaria de “particular”. Uma das sociedades — sempre a nossa — define o quadro geral da natureza em relação ao qual as outras estarão situadas.** É a solução de Lévi-Strauss, que distinguia entre uma sociedade ocidental com acesso à natureza e a própria natureza, miraculosamente conhecida por nossa sociedade. À primeira metade deste argumento permite o relativismo modesto — nós somos apenas uma cultura entre outras —, mas a segunda permite o retorno sub-reptício do universalismo arrogante — continuamos a ser absolutamente diferentes. Não há qualquer contradição, no entanto, aos olhos de Lévi-Strauss, entre as duas metades, já que, justamente, nossa Constituição, e apenas ela, permite distinguir uma sociedade A composta por humanos e uma sociedade A’ composta por nio-humanos e para sempre afastada da primeira! **A contradição só é aparente, hoje, aos olhos da antropologia simétrica. Este último modelo é o fundo comum dos dois outros, o que quer que digam os relativistas, que nunca relativizam nada além das culturas.**
> (p. 130-1)

# Antropologia simétrica (diferenciar as natutezas-culturas pelo tamanho ~ proliferação de híbridos)

> Isto porque o objetivo do princípio de simetria não é apenas o de estabelecer a igualdade — esta é apenas o meio de regular a balança no ponto zero — **mas também o de gravar as diferenças, ou seja, no fim das contas, as assimetrias, e o de compreender os meios práticos que permitem aos coletivos dominarem outros coletivos. Ainda que sejam semelhantes pela coproduçäo, todos os coletivos diferem pelo tamanho.** No comeco da pesagem, uma central nuclear, um buraco na camada de ozónio, uma rede de satélites, um aglomerado de galáxias não säo mais pesados do que uma fogueira de gravetos, o céu que pode cair sobre nossa cabeça, uma genealogia, uma carroça, espiritos visiveis no céu, ou uma cosmogonia. **Estes quase-objetos, com suas trajetórias hesitantes, traçam ao mesmo tempo formas da natureza e formas de sociedades. Mas no fim da medição, o primeiro lote traça um coletivo totalmente diferente do segundo. Também estas diferenças devem ser reconhecidas.**
> (p. 134)

> _Um número muito maior de objetos exige muito mais sujeitos. Muito mais subjetividade requer muito mais objetividade._ Se desejamos Hobbes e seus descendentes, precisamos de Boyle e de seus descendentes. Se desejamos o Leviatã, precisamos da bomba de vácuo. É isto que permite respeitar ao mesmo tempo as diferenças (as volutas têm, de fato, dimensões diferentes) e as semelhanças (todos os coletivos misturam da mesma forma as entidades humanas e não-humanas). Os relativistas, que tentam nivelar todas as culturas, transformando-as em codificações igualmente arbitrárias de um mundo natural cuja produção não é explicada, não conseguem respeitar os esforços que os coletivos fazem para dominar uns aos outros. Por outro lado, os universalistas são incapazes de compreender a fraternidade profunda dos coletivos, uma vez que são obrigados a oferecer o acesso à natureza apenas aos ocidentais e a trancar todos os outros em sociedades das quais eles só escaparão caso se tornem cientistas. modernos e ocidentalizados.
> (p. 135)

> As ciências e as técnicas não são notáveis por serem verdadeiras ou eficazes — estas propriedades lhes são fornecidas por acréscimo e por razões outras que não as dos epistemólogos (Latour, 1989a) —, **mas sim porque multiplicam os não-humanos envolvidos na construção dos coletivos e porque tornam mais íntima a comunidade que formamos com estes seres.** É a extensäo da espiral, a amplitude dos envolvimentos que irá suscitar, a distáncia cada vez maior onde irá recrutar estes seres que caracterizam as ciéncias modernas e náo algum corte epistemológico que romperia de uma vez por todas com seu passado pré-científico. Os saberes e os poderes modernos näo säo diferentes porque escapam á tirania do social, mas porque acrescentam muito mais híbridos a fim de recompor o laço social e de aumentar ainda mais sua escala.
> (p. 135)

# O golpe de Arquimedes: a ciência como política executada por outros meios (paráfrase de Clausewitz)

> O ponto de Arquimedes não deve ser procurado no primeiro momento, mas sim na conjunção dos dois: como fazer política através de novos meios que subitamente tornaram-se comensuráveis com ela, ao mesmo tempo em que é negada qualquer ligação entre atividades absolutamente incomensuráveis? O balanço é positivo, em dois sentidos: Hieron defende Siracusa com a ajuda de máquinas que podem ser dimensionadas, o coletivo aumenta proporcionalmente, mas a origem desta variação de escala, desta comensurabilidade, irá desaparecer para sempre, deixando o Olimpo das ciências como uma fonte de forças novas, sempre disponíveis, nunca visiveis. **Sim, a ciência é de fato a politica executada por outros meios, os quais só têm força porque permanecem radicalmente outros.**
> (p. 138)

# Especificidade dos modernos: recusa a existência de híbridos (excepcionalismo moderno)

> **Os modernos de fato diferem dos pré-modernos porque se recusam a pensar os quase-objetos como tais.** Os híbridos representam para eles o horror que deve ser evitado a qualquer custo através de uma purificação incessante e maniaca. Por si mesma, esta diferença na representação constitucional importaria muito pouco, uma vez que não seria suficiente para separar os modernos dos outros. Haveria tantos coletivos quantas fossem as representações. Mas a máquina de criar diferenças é ativada por esta recusa de pensar os quase-objetos, porque ela gera a proliferação inédita de um certo tipo de ser: _o objeto construtor do social, uma vez expulso do mundo social, atribuido a um mundo transcendente que no entanto não é divino, e que produz, por contraste, um sujeito flutuante portador de direito e de moralidade._ A bomba de vácuo de Boyle, os micróbios de Pasteur, a polia composta de Arquimedes são objetos deste tipo. **Estes novos não-humanos possuem propriedades miraculosas, uma vez que são ao mesmo tempo sociais e não-sociais, produtores de naturezas e construtores de sujeitos. São os _tricksters_ da antropologia comparada.** Através desta brecha, as ciências e as técnicas irão irromper de forma tão misteriosa na sociedade que este milagre vai forçar os ocidentais a se pensarem como sendo totalmente diferentes dos outros. **O primeiro milagre gera um segundo — por que os outros não fazem o mesmo? — e depois um terceiro — por que nés somos tão excepcionais? É esta caracteristica que irá engendrar, em cascata, todas as pequenas diferenças, as quais serão recolhidas, resumidas e amplificadas pela grande narrativa do Ocidental radicalmente a parte de todas as culturas.**
> (p. 140)

# Etnologia como medida medidora. Relacionalismo ou relativismo relativista.

> Os universalistas definiam uma única hierarquia. Os relativistas absolutos tornavam todas elas iguais. **Os relativistas relativistas, mais modestos porém mais empíricos, mostram os instrumentos e as cadeias que foram usadas para criar assimetrias e igualdades, hierarquias e diferenças** (Callon, 1991). Os mundos só parecem comensuráveis ou incomensuraveis aqueles que ficam presos às medidas medidas. Porém, todas as medidas, tanto na ciéncia rigida quanto na ciéncia flexivel, são sempre medidas medidoras e estas constréem uma comensurabilidade que não existia antes que fossem desenvolvidas. Nenhuma coisa é, por si só, redutivel ou irredutivel a qualquer outra. Nunca por si mesma, mas sempre por intermédio de uma outra que a mede e transfere esta medida a coisa. [...] **A etnologia é uma destas medidas medidoras que soluciona na prática a questão do relativismo ao construir, dia após dia, uma certa comensurabilidade. Se a questão do relativismo for insolúvel, o relativismo relativista ou, de forma mais elegante, o relacionismo, não oferece nenhuma dificuldade a priori. Se deixarmos de ser totalmente modernos, ele irá tornar-se um dos recursos essenciais para relacionar os coletivos, que tentaremos não mais modernizar. Servirá de organon para a negociaçäo planetária sobre os universais relativos que estamos construindo aos poucos.**
> (p. 141-2)

# Uma rede extensa é local em todos os pontos (p. 146)

> Para falar de forma vulgar de um assunto que foi idolatrado demais, os fatos científicos são como peixes congelados: nunca devem ficar fora do congelador, por um instante que seja. **O universal em rede produz os mesmos efeitos do que o universal absoluto, mas já não possui as mesmas causas fantásticas. É possível comprovar “em todos os lugares” a gravitação, mas com o custo da extensão relativa das redes de medidas e de interpretação. A elasticidade do ar pode ser verificada em toda parte, mas somente quando estamos conectados a uma bomba de vácuo que se disseminou pela Europa graças às múltiplas transformações dos experimentadores.** Tentem comprovar o mais simples dos fatos, a menor lei, a mais humilde constante, sem antes conectar-se às diversas redes metrológicas, aos laboratérios, aos instrumentos. O teorema de Pitágoras ou a constante de Planck se estendem às escolas e aos foguetes, às mâquinas e aos instrumentos, mas não saem de seus mundos, assim como os achuar náo saem de suas aldeias (Latour, 1989a, capítulo VI).
> (p. 149)

# O que há são redes práticas entre o micro e o macro teóricos

> Ou permanecem no “micro” e nos contextos interpessoais, ou então passam subitamente para um nível “macro” e só lidam, segundo eles, com racionalidades descontextualizadas e despersonalizadas. O mito e a burocracia sem alma e sem agente, assim como o do mercado puro e perfeito, apresenta a imagem simétrica aquela do mito das leis cientificas viversais.
> (p. 151)

> Contudo, **existe um fio de Ariadne que nos permitiria passar continuamente do local ao global, do humano ao não-humano. É o da rede de práticas e de instrumentos, de documentos e traduções.** Uma organização, um mercado, uma instituição não são objetos supra-lunares feitos de uma matéria diferente daquela de nossas relações locais sub-lunares. A única diferença vem do fato de que os primeiros são compostos por híbridos e, para sua descrição, precisam mobilizar um grande número de objetos. O capitalismo de Fernand Braudel ou de Marx não é o capitalismo total dos marxistas (Braudel, 1979). É um labirinto de redes um pouco longas que envolvem, de forma incompleta, um mundo a partir de pontos que se transformam em centros de cálculo ou de lucro.
> (p. 152)

> Assim como os adjetivos natural e social designam representaçôes do coletivo que, em si, nada tem de natural ou de social, as palavras local e global possibilitam pontos de vista sobre redes que não säo, por natureza, nem locais nem globais, mas que säo mais ou menos longas e mais ou menos conectadas.
> (p. 153)

# Encontrar as pequenas causas para grandes efeitos

> Da mesma forma como não devemos permitir à verdade científica e à eficácia técnica, ainda por cima, a transcendência, também total, e a racionalidade — também absoluta. Tanto para os crimes quanto para o domínio, tanto para os capitalismos quanto para as ciências, devemos compreender as coisas banais, as pequenas causas e seus grandes efeitos (Arendt, 1963; Meyer, 1990).
> (p. 157)

# A paralisia da totalização -> triagem pelas redes

> O que fazer, então, com estas superfícies lisas e preenchidas, com estas totalidades absolutas? Bem, virar todas elas pelo avesso, subvertêlas, revolucioná-las. Que belo paradoxo! Por seu espirito crítico, os modernos inventaram ao mesmo tempo o sistema total, a revolução total para acabar com ele, e a impossibilidade igualmente total de realizar esta revolução, impossibilidade que os desespera absolutamente! Não é esta a causa de muitos dos crimes de que nos acusamos? **Ao levar em conta a Constituição ao invés do trabalho de tradução, os críticos imaginaram que estávamos realmente incapacitados para compromissos, para experimentar, para misturar e para triar.** À partir das frágeis redes heterogêneas que formam os coletivos desde sempre, eles elaboraram totalidades homogêneas que não poderíamos tocar sem que, com isso, as revolucionássemos totalmente. E como esta subversão era impossível, mas eles tentaram fazêla assim mesmo, foram passando de um crime a outro. Como este _Noli me tangere_ dos totalizadores ainda seria capaz de passar como uma prova de moralidade? A crença em uma modernidade radical e total levaria, portanto, à imoralidade?
> (p. 158)

# Comum na ideida de que jamais fomos modernos

> Para nós, as técnicas não são novas, e nem modernas no sentido mais banal da palavra, mas sim coisas que desde sempre fazem parte de nosso mundo. Mais que qualquer outra, nossa geração as digeriu, integrou, ou mesmo humanizou. Isto porque somos os primeiros a não acreditar mais nem nas virtudes nem nos perigos das ciências e das técnicas; somos os primeiros a partilhar seus vícios e virtudes sem neles ver o céu ou o inferno, assim como talvez nos seja mais fácil pesquisar suas causas sem ter que apelar para o fardo do homem branco, para a fatalidade do capitalismo, para o destino europeu, para a história do Ser ou da racionalidade universal. Talvez nos seja mais fácil, hoje, abandonar a crença em nossa própria estranheza. **Não somos exóticos, mas sim comuns. O que, conseqüentemente, também faz com que os outros deixem de ser exóticos. São como nós, jamais deixaram de ser nossos irmãos. Não devemos acrescentar o crime de nos acreditarmos radicalmente diferentes a todos os outros que já cometemos.**
> (p. 159)

# Imanência x transcendência -> transcendência sem oposto (delegação)

> Onde estamos, então? Em que iremos recair? Enquanto nos colocarmos esta pergunta, é certo que estaremos no mundo moderno, obcecados com a construção de uma imanéncia (immanere: residir em) ou na desconstrução de alguma outra. Permanecemos ainda, para usar um termo do vocabulário antigo, na metafísica. Ora, ao percorrer estas redes, não encontramos nada que seja particularmente homogêneo. Permanecemos, antes, em uma infra-física. Nós somos imanentes, então, textos entre outros textos, sociedade entre outras sociedades, entes entre os entes?
> (p. 160-161)

> Também não, uma vez que, se ao invés de ligarmos os pobres fenômenos às amarras sólidas da natureza e da sociedade, deixarmos que os mediadores produzam as naturezas e as sociedades, teremos invertido o sentido das transcendências modernizadoras. Naturezas e sociedades transformam-se nos produtos relativos da história. **Portanto, não recaímos apenas na imanência, uma vez que as redes não estão mergulhadas em nenhum fluido.** Não precisamos encontrar um éter misterioso para que elas se propaguem. Não precisamos preencher os vazios. É a concepção dos termos transcendência e imanência que se encontra modificada pelo retorno dos modernos ao não-moderno. Quem disse que a transcendência deveria possuir um oposto? Nós somos, nós permanecemos, _nós jamais abandonamos a trancendência, ou seja, a manutenção na presença através da mediação do envio._
> (p. 161)

> As outras culturas sempre se chocaram contra os modernos devido ao aspecto difuso de suas forças ativas ou espirituais. Elas jamais colocavam em jogo matérias puras ou forças mecánicas puras. Os espiritos e os agentes, os deuses e os ancestrais estavam misturados a tudo. Em comparação, para eles o mundo moderno parecia desencantado, esvaziado de seus mistérios, dominados pelas forças homogêneas da imanência pura à qual apenas nós, humanos, impúnhamos alguma dimensão simbólica e para além das quais existia, talvez, a transcendéncia do Deus suprimido. **Ora, se não hä imanéncia, se hâ somente redes, agentes, actantes, o desencanto se tornaria impossivel.** Não somos nés que acrescentamos arbitrariamente a “dimensäo simbélica” a forças puramente materiais. **Assim como nós, estas também são transcendentes, ativas, agitadas e espirituais. O acesso à natureza não é mais imediato do que à sociedade ou ao Deus suprimido. No lugar do jogo sutil dos modernos entre trés entidades, cada uma das quais era ao mesmo tempo transcendente e imanente, obtemos uma única proliferação de transcendências. Termo polémico inventado para fazer face à pretensa invasão da imanéncia, o sentido da palavra deve ser modificado caso nao haja mais oposicáo.**
> (p. 161-2)

# Delegação

> **Chamo de delegação esta transcendência sem oposto.** A enunciação, ou a delegação ou o envio de mensagem ou de mensageiro permite continuar em presença, ou seja, existir. Quando abandonamos o mundo moderno, não recaímos sobre alguém ou sobre alguma coisa, **não recaímos sobre uma essência, mas sim sobre um processo, sobre um movimento, uma passagem, literalmente, um passe, no sentido que esta palavra tem nos jogos de bola.** Partimos de uma existência contínua e arriscada — contínua porque é arriscada — e não de uma essência; partimos da colocação em presença e não da permanência. Partimos do _vinculum_ em si, da passagem e da relação, aceitando como ponto de partida apenas aqueles seres saídos desta relação ao mesmo tempo coletiva, real e discursiva. Não partimos dos homens, este retardatário, nem da linguagem, mais tardia ainda. O mundo dos sentidos e o mundo do ser são um único e mesmo mundo, o da tradução, da substituição, da delegação, do passe. Diremos, sobre qualquer outra definição de uma esséncia, que ela é “desprovida de sentido”, desprovida de meios para manter-se em presença, para durar. **Toda duração, toda dureza, toda permanéncia deverá ser paga por seus mediadores. É esta exploraçäo de uma transcendência sem oposto que torna nosso mundo täo pouco moderno, com todos seus nûncios, mediadores, delegados, fetiches, mâquinas, estatuetas, instrumentos, representantes, anjos, tenentes, porta-palavras e querubins.** Que mundo é este que nos obriga a levar em conta, ao mesmo tempo e de uma só vez a natureza das coisas, as técnicas, as ciéncias, os seres ficcionais, as economias e os inconscientes? **É justamente nosso mundo. O qual deixou de ser moderno depois que substituímos cada uma das essências por mediadores, delegados e tradutores que lhe dão sentido.** É por isso que ainda não somos capazes de reconhecêlo. Ele parece antiquado com todos esses delegados, anjos e tenentes. Ao mesmo tempo não se parece muito com as culturas estudadas pelos etnólogos, uma vez que estes nunca realizaram o trabalho simétrico de convocar delegados, mediadores e tradutores para sua casa, para seu próprio coletivo. A antropologia foi toda feita com base na ciéncia, ou com base na sociedade, ou com base na linguagem, alternando sempre entre o universalismo e o relativismo cultural, e no fim das contas nos dizia bem pouco tanto sobre “Eles” quando sobre “Nós”.
> (p. 162-3)

# Conservar as potências dos pré, pós e modernos

> Mas a triagem parece impossível e mesmo contraditória já que o dimensionamento dos coletivos depende do silêncio mantido em relação aos quase-objetos. Como conservar o tamanho, a pesquisa, a proliferação, e ao mesmo tempo tornar explicitos os hibridos? Este é, entretanto, o amálgama que procuro: **manter a colocaçäo em natureza e a colocaçäo em sociedade que permitem a mudança de tamanho através da criação de uma verdade exterior e de um sujeito de direito, sem com isso ignorar o trabalho continuo de construção conjunta das ciéncias e das sociedades.** Usar os pré-modernos para pensar os hibridos, mas conservando, dos modernos, o resultado final do trabalho de purificaçäo, ou seja, a colocaçäo em caixa-preta de uma natureza exterior claramente distinta dos sujeitos. Seguir de forma contínua o gradiente que leva das existéncias instáveis as esséncias estabilizadas — e inversamente. **Obter o trabalho de purificaçäo, mas como caso particular do trabalho de mediaçäo.** Manter todas as vantagens do dualismo dos modernos sem seus inconvenientes — a clandestinidade dos quase-objetos; conservar todas as vantagens do monismo dos pré-modernos sem sofrer suas limitaçôes —, a restricio de tamanho devido a confusäo durável entre saberes e poderes.
> (p. 168)

![Figura 14](./jfm14.png)

# Uma definição amoderna de humano: permutador, mediador

> Onde estão os Mounier das máquinas, os Lévinas dos animais, os Ricoeur dos fatos? O humano, como podemos compreender agora, só pode ser captado e preservado se devolvermos a ele esta outra metade de si mesmo, a parte das coisas. Enquanto o humanismo for feito por contraste com o objeto abandonado à epistemologia, não compreenderemos nem o humano, nem o näo-humano.
> (p. 171)

> Se o humano não possui uma forma estável, isso não quer dizer que não tenha nenhuma forma. **Se, ao invés de o ligarmos a um dos pólos da Constituição, nós o aproximarmos do meio, ele mesmo se torna o mediador e o permutador.** O humano não é um dos pólos da Constituição que se oporia aos não-humanos. **As duas expressões de humanos ou de não-humanos são resultados tardios que não bastam mais para designar a outra dimensão.** A escala de valores não consiste em fazer deslizar a definição do humano ao longo da linha horizontal que conecta o pólo do objeto ao do sujeito, mas sim em fazê-la deslizar ao longo da dimensão vertical que define o mundo não moderno. **Caso seu trabalho de mediação seja revelado, ele toma forma humana. Caso encoberto, iremos falar de inumanidade, ainda que se trate da consciência ou da pessoa moral.** À expressão “antropomórico” subestima nossa humanidade, em muito. **Deveríamos falar em morismo. Nele se entrecruzam os tecnomorfismos, os zoomorfismos, os fisimorismos, os ideomorfismos, os teomorfismos, os sociomorfismos, os psicoorfismos. São suas alianças e suas trocas, como um todo, que definem o ntropos. Uma boa definição para ele seria a de permutador ou recombinador de morfismos. Quanto mais próximo desta repartição, mais humano ele será.** Quanto mais distante, mais ele irá tomar formas múltiplas nas quais sua humanidade rapidamente torna-se impossível de discernir, ainda que suas formas sejam as da pessoa, do indivíduo ou do eu. Quando tentamos isolar sua forma daquelas que ele mistura, não o protegemos — nós o perdemos.
> (p. 172-3)

> Os humanistas modernos são redutores, já que tentam relacionar a ação com determinadas potências apenas, transformando o resto do mundo em meros intermediários ou simples forças mudas. Quando redistribuímos a ação entre todos os mediadores **perdemos, é verdade, a forma reduzida do homem, mas ganhamos uma outra, que devemos chamar de irreduzida. O humano está no próprio ato de delegação, no passe, no arremesso, na troca contínua das formas.** É claro que ele não é uma coisa, mas as coisas também não são coisas. É claro que ele não é uma mercadoria, mas as mercadorias também não são mercadorias. É claro que ele não é uma máquina, mas aqueles que já viram as máquinas sabem quão pouco maquinais elas são. Claro que ele não pertence a este mundo, mas também este mundo não pertence a este mundo. Claro que ele não está em Deus, mas qual a relação existente entre o Deus do alto e aquele que deveríamos dizer de baixo? **O humanismo só pode manter-se dividindo-se entre todos os seus enviados. A natureza humana consiste no conjunto de seus delegados e de seus representantes, de suas figuras e de seus mensageiros. Este universal do qual falamos, simétrico, vale muito bem o outro, duplamente assimétrico, dos modernos. Esta nova posição, deslocada em relação à do sujeito/sociedade, deve agora ser garantida por emendas na Constituição.**
> (p. 174)

# A constituição não-moderna

> A natureza e a sociedade não são dois polos distintos, mas uma só e mesma produção de sociedades-naturezas, de coletivos.
> (p. 175)

> Mas, ora, podemos conservar as duas primeiras garantias da antiga Constituição sem manter a duplicidade, hoje visível, de sua terceira garantia. A transcendência da natureza, sua objetividade, ou a imanência da sociedade, sua subjetividade, provêm ambas do trabalho de mediação sem contudo depender de uma separaçäo entre elas, como faz crer a Constituição dos modernos. **O trabalho de colocaçäo em natureza ou de colocaçäo em sociedade vem da conclusäo durável e irreversivel do trabalho comum de delegaçäo e de traduçäo. No fim das contas, há de fato uma natureza que náo criamos, e uma sociedade que podemos mudar, há fatos científicos indiscutíveis e sujeitos de direito, mas estes tornam-se conseqüência de uma prática continuamente visível, ao invés de serem, como para os modernos, as causas longínquas e opostas de uma prática invisivel que os contradiz.**
> (p. 176)

![Figura 15](./jfm15.png)

# O parlamento das coisas

> Se retomarmos as duas representações enquanto uma dúvida dupla quanto à fidelidade dos mandatários, então o Parlamento das coisas estará definido. Em seu recinto encontra-se recomposta a continuidade do coletivo. Não há mais verdades nuas, mas também não há mais cidadãos nus. Os mediadores dispõe de todo o espaço. As Luzes encontraram enfim seu lugar. As naturezas estão presentes, mas com seus representantes, os cientistas, que falam em seu nome. As sociedades estão presentes, mas com os objetos que as sustentam desde sempre. Pouco nos importa que um dos mandatários fale do buraco de ozônio, que um outro represente as indústrias químicas, um terceiro represente os operários destas mesmas indústrias químicas, um quarto os eleitores, um quinto a meteorologia das regiões polares, que um outro fale em nome do Estado; pouco nos importa, contanto que eles se pronunciem todos sobre a mesma coisa, sobre este quaseobjeto que criaram juntos, este objeto-discurso-natureza-sociedade cujas novas propriedades espantam a todos e cuja rede se estende de minha geladeira à Antártida passando pela química, pelo direito, pelo Estado, pela economia e pelos satélites. Os imbróglios e as redes que não possuíam um lugar possuem agora todo o espaço. São eles que é preciso representar, é em torno deles que se reúne, de agora em diante, o Parlamento das coisas. “A pedra rejeitada pelos construtores tornou-se a pedra angular.”
> (p. 181)
