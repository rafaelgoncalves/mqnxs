---
title: A modulação algorítmica de comportamento e suas categorias operativas a partir das patentes da Facebook Inc.
tags:
    - Débora Machado
    - Facebook
    - patentes
    - modulação
    - fichamento

date: 2023-06-12
category: fichamento
path: "/modulacao-algoritmica"
featuredImage: "fb.jpg"
srcInfo: <a href="https://commons.wikimedia.org/wiki/File:Facebook_(26405898387).jpg">Facebook conceptual phone</a>
published: true
---

Fichamento do artigo "A modulação algorítmica de comportamento e suas categorias operativas a partir das patentes da Facebook Inc."[^1] de Débora Machado, que propõe categorias de modulação de comportamento a partir de análise de patentes da Facebook.

[^1]: MACHADO, D. F. A modulação algorítmica de comportamento e suas categorias operativas a partir das patentes da Facebook Inc. **Revista Eletrônica Internacional de Economia Política da Informação**, da Comunicação e da Cultura, v. 22, n. 2, p. 97–111, 24 maio 2020. 

# Abstract

> Esta pesquisa parte dos objetivos de entender as implicações sociais do uso de sistemas algorítmicos pelas plataformas de mídias sociais, e analisar as dinâmicas e motivações para a criação desses sistemas, para assim compreender o que muda em uma comunicação cada vez mais mediada e modulada por agentes não humanos. A partir da análise de pedidos de patentes requeridos pela empresa Facebook Inc. nos últimos quatro anos, foi possível identificar tecnologias que são úteis à modulação de comportamento. A análise de patentes mostra-se como uma alternativa interessante para explorar os processos algorítmicos e entender alguns mecanismos da plataforma de mídia social que costumam ser ofuscados e de difícil acesso.

# Objetivo da pesquisa

> A pesquisa apresentada neste artigo tem como objetivo **destrinchar parte do funcionamento dos processos algorítmicos** de uma plataforma específica, o Facebook, para **identificar uma consequência específica de seu uso**, a modulação algorítmica de comportamento, a partir de suas patentes.
> (p. 99)

# Modulação algorítmica como controle e orientação de comportamento via dados e algoritmos

> Entende-se como **modulação algorítmica uma forma de controle e orientação de comportamento possibilitada por processos algorítmicos, que opera a partir da coleta massiva de dados para direcionar condutas, a atenção ou o comportamento de pessoas ou perfis** (SOUZA; AVELINO; SILVEIRA, 2018), e que atualmente é **intrínseca ao funcionamento e modelo de negócio das plataformas de mídias sociais** (MACHADO, 2018).
> (p. 100)

# Objeto de análise: patentes de modulação de comportamento

> Portanto, foi feito um recorte na busca de patentes para analisar apenas pedidos de patente referentes a tecnologias identificadas como moduladoras ou úteis à modulação de comportamento.
> (p. 100)

> Dessa forma, foi feita uma análise dos pedidos de patente depositados pela Facebook Inc. entre 2014 e 2018 para identificar tecnologias desenvolvidas pela empresa que possibilitassem a orientação do comportamento dos usuários da plataforma.
> (p. 100)

# Resultado: categorias operativas (operatórias?) de modulação

> A partir dessa análise, foi possível determinar seis categorias operativas que nos ajudam a compreender as diferenças e similitudes das invenções úteis à modulação de comportamento.
> (p. 100)

# Artigo referente a projeto de mestrado

> Este artigo é oriundo de uma dissertação de mestrado defendida pela autora em agosto de 2019 no Programa de Pós-Graduação em Ciências Humanas e Sociais (PCHS) da Universidade Federal do ABC (MACHADO, 2019).
> (p. 100)

# Método: análise de patentes

> Para a presente pesquisa a metodologia utilizada foi a análise de patentes. O método se mostrou como uma alternativa interessante para investigar os processos algorítmicos e as dinâmicas de funcionamento das plataformas de mídias sociais. Analisamos as patentes de uma única empresa, a Facebook Inc., e os documentos foram obtidos a partir da base de dados do United States Patent and Trademark Office (USPTO). Foi feita uma busca por pedidos de patentes que tenham sido depositados entre 1º de janeiro de 2014 e 31 de dezembro de 2017. Para a primeira etapa da análise utilizamos os primeiros mil entre os 3.097 resultados gerados pela busca. Foi feita a leitura dos títulos e resumos das patentes e foram selecionadas para a segunda etapa apenas as patentes referentes às funcionalidades da plataforma de mídia social Facebook e suas aplicações na versão mobile, excluindo documentos referentes a outros serviços da empresa, como o WhatsApp e o Instagram. O resultado foi um total de 430 pedidos de patentes que passaram por uma segunda leitura e, por fim, **foram selecionadas as patentes referentes a sistemas úteis à modulação de comportamento dos usuários, resultando em 41 pedidos de patentes que foram enquadrados em seis categorias operativas: Recomendação que leva à ação; Decidindo pelo usuário; Predição e inferência; Atribuindo valor à interação; Análise de emoções; Possibilidades para a coleta de dados**.
> (p. 100-1)

# Categorias operativas de modulação

> Essas categorias referem-se à principal forma de operação de cada uma dessas patentes que pode torná-las úteis à modulação de comportamento. Entre as patentes classificadas na categoria “Recomendação que leva à ação” estão sistemas e processos algorítmicos de recomendação que possuem entre as suas finalidades a função de orientar o usuário a agir de alguma forma que seja vantajosa para a plataforma. Na categoria “Decidindo pelo usuário” estão as patentes correspondentes a sistemas que auxiliam o Facebook a cortar etapas no processo de tomada de decisão do usuário. No grupo “Predição e inferência” estão as patentes nas quais a análise preditiva ocupa uma função essencial para o seu funcionamento. Entre as classificadas como “Atribuindo valor à interação” estão as que propõem inovações para aprimorar a maneira pela qual se atribui valor a cada interação do usuário na plataforma. Na categoria “Análise de emoções” estão as patentes referentes a tecnologias que buscam coletar e analisar informações sobre as emoções e os sentimentos dos usuários. Por fim, entre as classificadas como “Possibilidades para a coleta de dados”, são apresentadas as patentes que têm sistemas e métodos, entre suas principais inovações, para a coleta e análise de dados dos usuários (MACHADO, 2019).
> (p. 101)

# M1 - Recomendação que leva à ação

> Entre as 41 patentes selecionadas como moduladoras de comportamento ou úteis à modulação, 27 apresentam a recomendação como uma de suas principais características operativas. A área mais visível da plataforma Facebook, independentemente do dispositivo utilizado, o _News Feed_ (_Feed_ de Notícias), tem seu funcionamento baseado na recomendação de conteúdo.
> (p. 101)

> Mas seria todo sistema de recomendação algorítmica um tipo de modulação de comportamento? Embora entendamos que, ao definir a visibilidade ou a invisibilidade de uma publicação, a plataforma tem o poder de direcionar o olhar do usuário e formatar opiniões (MACHADO; SOUZA; SILVEIRA, 2018), com esta pesquisa observamos que **os sistemas de recomendação algorítmica mais úteis à modulação são aqueles que induzem ou direcionam o usuário a uma ação específica**.
> (p. 101-2)

Exemplo

> Se em alguns textos das patentes a intenção de sugerir ou incentivar uma ação ao usuário é sutil, em outros essa intencionalidade é bem clara. É o caso do pedido de patente denominado Methods and systems for optimizing messages to users of a social network4 (ARQUETTE et al., 2017), que descreve uma invenção que funcionaria no processo de envio de mensagens ao usuário, em formato de e-mail ou notificação, para “otimizar mensagens enviadas para um usuário de uma rede social”5 (ARQUETTE et al., 2017, p. 1, tradução nossa). Parte dessa otimização refere-se à criação de um modelo do usuário
> 
> > que pode ser usado para antecipar ou prever se uma mensagem enviada em determinado momento e uma mensagem que inclui conteúdo específico serão eficazes em obter uma resposta ou reação desejada do usuário. A resposta desejada pode incluir qualquer ação ou ações, como clicar em um link na mensagem, entrar no sistema de rede social, entrar em um grupo, responder a uma solicitação de amizade, enviar um convite para um amigo, fornecer _feedback_, visualizar conteúdo na rede social, comentar um conteúdo publicado no sistema de rede social etc.6 (ARQUETTE et al., 2017, p. 6, tradução nossa)
> 
> (p. 102-3)

Aprendizado de máquina ~ recomendação

> O texto afirma que, “em geral, as mensagens podem ser enviadas ao usuário para incentivá-lo a participar ou aumentar a participação em qualquer aspecto do sistema de rede social”7 (ARQUETTE et al., 2017, p. 5, tradução nossa). **A patente também aponta que o sistema inclui a implementação de ferramentas de aprendizado de máquina, que seriam treinadas com base nas respostas desse usuário às mensagens e notificações, para prever se a mensagem otimizada será efetiva em “extrair uma resposta ou reação desejada do usuário”** (ARQUETTE et al., 2017, p. 6, tradução nossa). **O verbo _to encourage_, traduzido para o português como incentivar, aparece em diversas patentes de recomendação**.
> (p. 103)

# M2 - Decidindo pelo usuário

> Essa categoria também engloba tecnologias de recomendação de ações e conteúdos, no entanto **elas se diferenciam das apresentadas na categoria anterior por demonstrarem uma ênfase maior na tentativa de encurtar os passos no processo de escolha e tomada de decisão do usuário na plataforma, mostrando-se como tecnologias facilitadoras que otimizam o tempo e minimizam os esforços**.
> (p. 104)

Exemplo

> Em algumas patentes, **a empresa afirma que ações que envolvam a digitação de muitas palavras podem ser incômodas e “desencorajar os usuários do uso do sistema de rede social, em parte devido à inconveniência de ter de digitar um número significativo de caracteres”** (LINDSAY; RAJARAM, 2017, p. 1, tradução nossa). **Essa justificativa foi recorrente nos sistemas que autocompletam frases**. É o caso do pedido de patente _Determining phrase objects based on received user input context information_, que altera a interface de composição de mensagens e publicações para oferecer sugestões de combinações de palavras que podem otimizar a escrita do usuário. Para definir quais termos serão sugeridos, uma série de análises de contexto, localização, perfil e sentimento do usuário é executada de modo que a escolha seja tão precisa que, assim que o usuário digitar “ice”, o sistema saiba que ele quer escrever “chocolate ice cream” e não apenas “ice cream”, por exemplo. A partir da ilustração é possível notar que, **caso o usuário não queira utilizar as sugestões e sim terminar de digitar a palavra, é necessário clicar em um botão para que o teclado seja exibido**.
> (p. 104)

> Segundo a empresa, essa simplificação na composição da mensagem aumenta a interação do usuário na plataforma:
> 
> > Para incentivar as interações dos usuários, os sistemas de redes sociais se esforçam para simplificar as interações entre o usuário e o sistema de redes sociais. Por exemplo, um sistema de rede social fornece a seus usuários interfaces que buscam reduzir o número de cliques para acessar uma página, reduzir o número de caracteres que um usuário insere para fornecer conteúdo ao sistema e reduzir a desordem ao apresentar conteúdo. Simplificar as interações do usuário com o sistema de rede social aumenta a probabilidade de os usuários continuarem a utilizá-lo.11 (LINDSAY; RAJARAM, 2017, p. 1, tradução nossa)
> 
> (p. 105)

# M3 - Predição, antecipação e inferência

> Para David Savat (2013, p. 41, tradução nossa), “a disciplina visa produzir manifestações externas específicas de comportamento, ao passo que a modulação visa a predizê-las”. As análises preditivas estão presentes em diversas tecnologias de recomendação da plataforma. No entanto, **as patentes inseridas nessa categoria possuem a predição como uma de suas principais funções operativas**. Nota-se que **a maioria das patentes identificadas com essa característica operativa se referem a tecnologias para conteúdo impulsionado, como anúncios**. Em 2018, o mercado publicitário foi responsável por 98% da receita da Facebook Inc. (CLEMENT, 2019).
> (p. 105)

Exemplo

> O pedido de patente _Predicting latent metrics about user interactions with content based on combination of predicted user interactions with the content_ (ZELDIN et al., 2016) descreve **uma tecnologia que prevê as interações que o usuário terá com um conteúdo, inclusive as que serão realizadas em certo intervalo de tempo após ser impactado por ele**. A análise nos mostra as possibilidades de predição de comportamento com base na coleta de dados comportamentais de um usuário na plataforma, fornecendo exemplos de diferentes modelos utilizados para definir a probabilidade de um usuário interagir com determinado conteúdo.
> (p. 105)

> O sistema proposto deseja prever não apenas interações realizadas dentro da plataforma, como compartilhamentos e curtidas, mas também **ações externas que ocorram em determinado tempo após o usuário ter tido contato com o conteúdo, como o acesso a um site ou a uma loja de uma marca relacionada à publicação, por exemplo**.
> (p. 106)

# M4 - Atribuindo valor à interação

Similaridade com a ideia de KPI (key performance indicators)? Exemplo: lifetime value.

> Juntamente com o aumento dos conteúdos pagos na plataforma, surgiu a necessidade de se **criarem novas métricas e formas de quantificar o efeito e o valor que deve ser pago por cada interação de um usuário com determinado conteúdo**.
> (p. 106)

Exemplo

> O pedido de patente Quantifying Social Influence17 apresenta uma tecnologia que define um coeficiente de influência social a partir do conteúdo compartilhado por um usuário e da interação de sua “audiência” com esse conteúdo para assim poder “executar um tipo de ação com base no coeficiente de influência social do usuário18” (AGARWAL, 2016, p. 1, tradução nossa).
> (p. 106)

> Segundo o texto da patente, **ações podem ser executadas de forma automatizada pela plataforma de mídia social para aumentar, manter ou diminuir o coeficiente de influência social do usuário**. Como exemplo de fatores relacionados a resultados de ações que podem ocorrer na plataforma ou “externos aos sistemas de rede social, como as ações no mundo real”19, e que seriam determinados como “fatores de sucesso”20, estão a “participação em eventos, o rendimento em publicidade ou vendas, resultados eleitorais e assim por diante”21 (AGARWAL, 2016, p. 12, tradução nossa). A patente também afirma que um algoritmo que pontua a influência social pode ser criado para **identificar quais comportamentos do usuário estão correlacionados ao aumento ou à diminuição de sua influência social e, a partir dessa informação, “ajustar-se para produzir aumentos ou diminuições na pontuação de influência social correlacionados com os resultados desejados”** (AGARWAL, 2016, p. 1, tradução nossa).
> (p. 106-7)

# M5 - Análise de emoções

> Durante a pesquisa foi possível identificar interesse por parte da empresa em desenvolver e patentear sistemas capazes de **coletar e inferir emoções e sentimentos dos usuários, e em usá-los como informações relevantes para a personalização, a recomendação e o ranqueamento de conteúdo**. Das 41 patentes selecionadas como úteis à modulação de comportamento, seis citam a análise de sentimentos ou emoções como parte de seu funcionamento (DONOHUE, 2015; KAZI et al., 2016a, 2016b; LINDSAY; RAJARAM, 2017; NAVEH, 2015; WONG et al., 2016).
> (p. 107)

Exemplo

> Um sentimento específico, o tédio, é o foco da patente _Presenting additional content items to a social networking system user based on receiving an indication of boredom_ (YU; WANG, 2016). **O sistema utiliza sensores como a câmera frontal para rastrear a posição do olhar de uma pessoa e, cruzando com outros dados, identificar se o usuário está interessado no conteúdo que está vendo no momento ou não**.
> (p. 107)

> O texto da patente apresenta um dispositivo que possui a finalidade de **instituir um “nível de tédio” para cada usuário que utiliza a plataforma por determinado tempo e não interage com as publicações**. Esse tipo de orientação de comportamento ocorre para “encorajar a interação do usuário com o conteúdo apresentado via _Feed_ de Notícias”24 (YU; WANG, 2016, p. 2, tradução nossa). Ademais, o texto afirma que **pode aumentar o preço ou incluir custos adicionais para anúncios que apareçam pela segunda vez no _News Feed_ do usuário**, em uma nova posição, por causa do nível de tédio identificado na sua atividade. Entende-se que esse preço é cobrado, entre outros fatores, **pelo fato de que prender a atenção de alguém que até então não demonstrou interesse em nada é uma ação valiosa**.
> (p. 107)

# M6 - Possibilidades para a coleta de dados

> A pesquisa apresentou uma parcela significativa de pedidos de patente que apresentam, além das finalidades específicas descritas até aqui, novas possibilidades para a coleta de dados. Algumas das patentes inseridas nessa categoria operativa repetem-se em outras, mas **são reapresentadas com ênfase nos métodos e sensores utilizados para coletar dados do usuário**. O pedido de patente _Ranking of news feed in a mobile device based on local signals_ (MARRA et al., 2016b), por exemplo, propõe um sistema de ranqueamento de publicações no _Feed_ de Notícias que, de início, não apresenta qualquer grande inovação. Mas, para que ele funcione corretamente, é anexado à tecnologia o aperfeiçoamento na captação e na análise de dados por meio de sensores de um dispositivo móvel. O produto dessa análise são os sinais locais. Entre os dados de sensores coletados incluem-se os dados de “câmera, acelerômetro, giroscópio, sensor de luz ambiente, microfone, sensor de impressão digital, sensor de pressão, sensor de proximidade e similares”26 (MARRA et al., 2016b, p. 5, tradução nossa).
> (p. 107-8)

# Conclusão e perspectivas futuras

> A análise das patentes da Facebook Inc. permitiu que a hipótese de que a empresa desenvolve tecnologias com a proposta de modular o comportamento dos seus usuários fosse confirmada e possibilitou uma observação aprofundada sobre a dinâmica de funcionamento tanto de tecnologias moduladoras de comportamento quanto de outras invenções úteis ao processo de modulação. Espera-se que, a partir dos apontamentos apresentados aqui, outras pesquisadoras se interessem na busca de novas metodologias que abranjam as possibilidades de investigação de processos e sistemas propositalmente ofuscados pelas empresas detentoras das grandes plataformas digitais utilizadas atualmente.
> (p. 109)
