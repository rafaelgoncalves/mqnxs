---
title: Manifesto ciborgue
tags:
    - Donna Haraway
    - ciborgue
    - tecnociência
    - materialismo semiótico
    - naturezacultura
    - tecnologias da informação
    - fichamento
    - manifesto
    - pós-humanismo
    - feminismo
    - marxismo
    - psicanálise
    - feminismo radical
    - pós-modernidade
    - ficção cientifica
    - microeletrônica

date: 2023-04-22
category: fichamento
path: "/manifesto-ciborgue"
featuredImage: "lampert.jpg"
srcInfo: <a href="https://www.moma.org/collection/works/101856">Kangaroo (2006) - Nicolas Lampert</a>
published: true
---

Fichamento de Manifesto Ciborgue[^1], de Donna Haraway, publicado originalmente em 1985 na revista _Socialist Review_.

[^1]: HARAWAY, D. Manifesto ciborgue: ciência, tecnologia e feminismo-socialista no final do século XX. Em: TADEU, T. (Org.). **Antropologia do ciborgue: As vertigens do pós-humano.** Tradução: Tomaz Tadeu. 2. ed. Belo Horizonte: Autêntica Editora, 2009. p. 33–118. [_A Cyborg anifesto: Science, Technology, and Socialist-Feminism in the Late Twentieth Century_, 1991].

# Ciborgue como mito político (feminista, socialista e materialista)

> Este ensaio1 é um esforço para construir um mito político, pleno de ironia, que seja fiel ao feminismo, ao socialismo e ao materialismo. 2 Um mito que poderá ser, talvez, mais fiel – na medida em a blasfêmia possa sê-lo – do que uma adoração ou uma identificação reverente.
> (p. 35)

# Blasfêmia como antídoto para a moral interna e insistência na comunidade. Ironia como método.

> A blasfêmia nos protege da maioria moral interna, ao mesmo tempo em que insiste na necessidade da comunidade. Blasfêmia não é apostasia. A ironia tem a ver com contradições que não se resolvem – ainda que dialeticamente – em totalidades mais amplas: **ela [[ a ironia ]] tem a ver com a tensão de manter juntas coisas incompatíveis porque todas são necessárias e verdadeiras. A ironia tem a ver com o humor e o jogo sério. Ela constitui também uma estratégia retórica e um método político que eu gostaria de ver mais respeitados no feminismo socialista.** No centro de minha fé irônica, de minha blasfêmia, está a imagem do ciborgue.
> (p. 35)

# O ciborgue como organismo cibernético (cybernetic organism). O ciborgue como fato e ficção [imaginação política]. A experiência das mulheres como fato e ficção.

> **Um ciborgue é um organismo cibernético, um híbrido de máquina e organismo, uma criatura de realidade social e também uma criatura de ficção. Realidade social significa relações sociais vividas, significa nossa construção política mais importante, significa uma ficção capaz de mudar o mundo**. Os movimentos internacionais de mulheres têm construído aquilo que se pode chamar de “experiência das mulheres”. Essa experiência é tanto uma ficção quanto um fato do tipo mais crucial, mais político.  **A libertação depende da construção da consciência da opressão, depende de sua imaginativa apreensão e, portanto, da consciência e da apreensão da possibilidade.** O ciborgue é uma matéria de ficção e também de experiência vivida – **uma experiência que muda aquilo que conta como experiência feminina no final do século XX**. Trata-se de uma luta de vida e morte, mas a fronteira entre a ficção científica e a realidade social é uma ilusão ótica.
> (p. 36)

# Ciborgue na ficção científica e na medicina moderna. Sexo-ciborgue como profilaxia ao heterossexismo.

> A ficção científica contemporânea está cheia de ciborgues – criaturas que são simultaneamente animal e máquina, que habitam mundos que são, de forma ambígua, tanto naturais quanto fabricados. **A medicina moderna também está cheia de ciborgues, de junções entre organismo e máquina, cada qual concebido como um dispositivo codificado, em uma intimidade e com um poder que nunca, antes, existiu na história da sexualidade.** [[ Exemplos são a vacina, os hormônios artificiais, o marca-passo, a bomba de infusão de insulina ]]. **O sexo-ciborgue restabelece, em alguma medida, a admirável complexidade replicativa das samambaias e dos invertebrados – esses magníficos seres orgânicos que podem ser vistos como uma profilaxia contra o heterossexismo.** O processo de replicação dos ciborgues está desvinculado do processo de reprodução orgânica.
> (p. 36)

# Produção e guerra como processos ciborguianos. C3I (comando-controle-comunicação-inteligência).

>  A produção moderna parece um sonho de colonização ciborguiana, um sonho que faz com que, comparativamente o pesadelo do taylorismo pareça idílico. Além disso, a guerra moderna é uma orgia ciborguiana, **codificada por meio da sigla C3I (comando-controle-comunicação-inteligência) – um item de 84 bilhões de dólares no orçamento militar.
> (p. 36-7)

# Ciborgue como mapa social e corporal, como recurso imaginativo capaz de acoplamentos. ~Biopolítica foucaultiana.

> Estou argumentando em favor do ciborgue como uma ficção que mapeia nossa realidade social e corporal e também como um recurso imaginativo que pode sugerir alguns frutíferos acoplamentos. O conceito de biopolítica de Michel Foucault não passa de uma débil premonição da política-ciborgue – uma política que nos permite vislumbrar um campo muito mais aberto.
> (p. 37)

# Final do século XX, somos todos híbridos, quimeras, ciborgues. Ciborgue como ontologia contemporânea.

> No final do século XX, neste nosso tempo, um tempo mítico, **somos todos quimeras, híbridos – teóricos e fabricados – de máquina e organismo; somos, em suma, ciborgues.** **O ciborgue é nossa ontologia;** ele determina nossa política. O ciborgue **é uma imagem condensada tanto da imaginação quanto da realidade material: esses dois centros, conjugados, estruturam qualquer possibilidade de transformação histórica.**
> (p. 37)

# Guerra de fronteiras. Em favor do prazer da confusão e da responsabilidade da construção. Por um mundo sem gênese e sem fim.

> **As coisas que estão em jogo nessa guerra de fronteiras são os territórios da produção, da reprodução e da imaginação.** Este ensaio é **um argumento em favor do prazer da confusão de fronteiras, bem como em favor da _responsabilidade_ em sua construção.** É também um esforço de contribuição para a teoria e para a cultura socialista-feminista, de uma forma pósmodernista, não naturalista, **na tradição utópica de se imaginar um mundo sem gênero, que será talvez um mundo sem gênese, mas, talvez, também, um mundo sem fim. A encarnação ciborguiana está fora da história da salvação.**
> (p. 37)

# Ciborgue como não-edípico (nem édipo, nem pós-édipo, [nem anti-édipo?])

> Ela tampouco obedece a um calendário edípico, no qual as terríveis clivagens de gênero seriam curadas por meio de uma utopia simbiótica oral ou de um apocalipse pós-edípico. Como argumenta Zoe Sofoulis em _Lacklein_ (seu ensaio, inédito, sobre Jacques Lacan, Melanie Klein e a cultura nuclear), **os mais terríveis e talvez mais promissores monstros dos mundos ciborguianos estão corporificados em narrativas não edípicas, obedecendo a uma lógica de repressão diferente, a qual, em nome de nossa sobrevivência, precisamos compreender.**
> (p. 38)

# Ciborgue como criatura pós-gênero. Nem um estado original (pré-édipo), nem uma totalidade teleológica (final). [~está no meio (não na média).]

> **O ciborgue é uma criatura de um mundo pós-gênero:** ele não tem qualquer compromisso com a bissexualidade, com a simbiose pré-edípica, com o trabalho não alienado. **O ciborgue não tem qualquer fascínio por uma totalidade orgânica que pudesse ser obtida por meio da apropriação última de todos os poderes das respectivas partes**, as quais se combinariam, então, em uma unidade maior. Em certo sentido, **o ciborgue não é parte de qualquer narrativa que faça apelo a um estado original, de uma “narrativa de origem”, no sentido ocidental, o que constitui uma ironia “final”, uma vez que o ciborgue é também o telos apocalíptico dos crescentes processos de dominação ocidental que postulam uma subjetivação abstrata, que prefiguram um eu último, libertado, afinal, de toda dependência – um homem no espaço.**
> (p. 38)

# Sobre o marxismo e a psicanálise dependerem de uma unidade original. O ciborgue como subversão [partir da diferença?].

> As **narrativas de origem, no sentido “ocidental”, humanista, dependem do mito da unidade original, da ideia de plenitude, da exultação e do terror, representados pela mãe fálica da qual todos os humanos devem se separar – uma tarefa atribuída ao desenvolvimento individual e à história, esses gêmeos e potentes mitos tão fortemente inscritos, para nós, na psicanálise e no marxismo.** Hilary Klein argumenta que **tanto o marxismo quanto a psicanálise, por meio dos conceitos de trabalho, individuação e formação de gênero, dependem da narrativa da unidade original, a partir da qual a diferença deve ser produzida e arregimentada, num drama de dominação crescente da mulher/natureza.** O ciborgue pula o estágio da unidade original, da identificação com a natureza, no sentido ocidental. Essa é sua promessa ilegítima, aquela que pode levar à subversão da teleologia que o concebe como guerra nas estrelas.
> (p. 38-9)

# Refuta da distinção Natureza/Cultura no mito do ciborgue [~Lator, _Nous n'avons jamais été modernes_, 1991]

> Não mais estruturado pela polaridade do público e do privado, o ciborgue define uma pólis tecnológica baseada, em parte, numa revolução das relações sociais do _oikos_ – a unidade doméstica. **Com o ciborgue, a natureza e a cultura são reestruturadas: uma não pode mais ser o objeto de apropriação ou de incorporação pela outra.** Em um mundo de ciborgues, as relações para se construir totalidades a partir das respectivas partes, incluindo as da polaridade e da dominação hierárquica, são questionadas.
> (p. 39)

# Ciborgues como filhos ilegítimos do militarismo, do capitalismo patriarcal e do socialismo de Estado, mas têm uma inclinação por uma frente unida sem partido de vanguarda [socialismo libertário?]

> Os ciborgues não são reverentes; eles não conservam qualquer memória do cosmo: por isso, não pensam em recompô-lo. **Eles desconfiam de qualquer holismo, mas anseiam por conexão – eles parecem ter uma inclinação natural por uma política de frente unida, mas sem o partido de vanguarda.** O principal problema com os ciborgues é, obviamente, que **eles são filhos ilegítimos do militarismo e do capitalismo patriarcal, isso para não mencionar o socialismo de estado. Mas os filhos ilegítimos são, com frequência, extremamente infiéis às suas origens. Seus pais são, afinal, dispensáveis.**
> (p. 40)

# Humano--animal

> Na cultura científica estadunidense do final do século XX, a fronteira entre o humano e o animal está completamente rompida. **Caíram as últimas fortalezas da defesa do privilégio da singularidade [humana] – a linguagem, o uso de instrumentos, o comportamento social, os eventos mentais; nada disso estabelece, realmente, de forma convincente, a separação entre o humano e o animal.** Muitas pessoas nem sequer sentem mais a necessidade dessa separação; muitas correntes da cultura feminista afirmam o prazer da conexão entre o humano e outras criaturas vivas. **Os movimentos em favor dos direitos dos animais não constituem negações irracionais da singularidade humana: eles são um lúcido reconhecimento das conexões que contribuem para diminuir a distância entre a natureza e a cultura.**
> (p. 40)

# Organismo--máquina [deslocamentos entre o vivo e o inerte para o interior tanto de organismos, como de máquinas. ~Viveiros de Castro]

> A segunda distinção sujeita a vazamentos é aquela entre o animal-humano (organismo), de um lado e a máquina, de outro. **As máquinas pré-cibernéticas podiam ser vistas como habitadas por um espírito: havia sempre o espectro do fantasma na máquina.** Esse dualismo estruturou a disputa entre o materialismo e o idealismo, a qual **foi resolvida por um rebento dialético que foi chamado, dependendo do gosto, de espírito ou de história. Mas, basicamente, nessa perspectiva, as máquinas não eram vistas como tendo movimento próprio, como se autoconstruindo, como sendo autônomas.** Elas não podiam realizar o sonho do homem; só podiam arremedá-lo. Elas não eram o homem, um autor para si próprio, mas apenas uma caricatura daquele sonho reprodutivo masculinista. Pensar que elas podiam ser outra coisa era uma paranoia. Agora já não estamos assim tão seguros. **As máquinas do final do século XX tornaram completamente ambígua a diferença entre o natural e o artificial, entre a mente e o corpo, entre aquilo que se autocria e aquilo que é externamente criado, podendo-se dizer o mesmo de muitas outras distinções que se costumavam aplicar aos organismos e às máquinas. Nossas máquinas são perturbadoramente vivas e nós mesmos assustadoramente inertes.**
> (p. 41-2)

# Crítica à textualização [construtivismo social? virada linguística?]

> A determinação tecnológica não é o único espaço ideológico aberto pelas reconceptualizações que veem a máquina e o organismo como textos codificados, textos por meio dos quais nos engajamos no jogo de escrever e ler o mundo. **A “textualização” de tudo, na teoria pós-estruturalista e na teoria pós-modernista, tem sido condenada pelos marxistas e pelas feministas socialistas, que desconfiam do desprezo utópico que essas teorias devotam às relações de dominação vividas,** desprezo que está na base do “jogo” da leitura arbitrária por elas postulada.
> (p. 42)

# Entre a totalidade e o puro relativismo, uma política ciborgue

> É certamente verdadeiro que as estratégias pós-modernistas, tal como o meu mito do ciborgue, subvertem uma quantidade imensa de totalidades orgânicas (por exemplo, o poema, a cultura primitiva, o organismo biológico). Em suma, a certeza daquilo que conta como natureza – uma fonte de _insight_ e uma promessa de inocência – é abalada, provavelmente de forma fatal. **Perde-se a autoria/autoridade transcendente da interpretação e com ela a ontologia que fundamentava a epistemologia “ocidental”. Mas a alternativa não é o cinismo ou a falta de fé, sto é, alguma versão de uma existência abstrata, como as teorias do determinismo tecnológico, que substituem o “homem” pela “máquina” ou a “ação política significativa” pelo “texto”. Saber o que os ciborgues serão é uma questão radical; respondê-la é uma questão de sobrevivência. Tanto os chimpanzés quanto os artefatos têm uma política. Por que não a teríamos nós?** (de Waal, 1982; Winner, 1980)
> (p. 42-3)

# Sobre a pós-modernidade e as mudanças advindas com ela

> 5: Um argumento provocativo e abrangente sobre a política e a teoria do “pós-modernismo” é o de **Fredric Jameson (1984), que argumenta que o pós-modernismo não é uma opção, um estilo entre outros, mas uma categoria cultural que exige uma reinvenção radical da política de esquerda a partir de seu interior; não existe mais nenhuma posição exterior que dê sentido à confortante ficção de que é possível manter uma certa distância crítica.** Jameson também deixa claro por que não se pode ser a favor ou contra o pós-modernismo, o que seria um gesto moralista. **Minha posição é a de que as feministas (outras pessoas e outros grupos também) precisam de uma contínua reinvenção cultural, de uma crítica pós-modernista e de um materialismo histórico: nesse cenário, só uma ciborgue pode ter alguma chance. A velha dominação do patriarcado capitalista branco parece, agora, nostalgicamente inocente: eles normalizaram a heterogeneidade, ao fazer classificações como aquelas de homem e mulher, de branco e negro, por exemplo. O “capitalismo avançado” e o pós-modernismo liberaram a heterogeneidade, deixando-nos sem nenhuma norma. O resultado é que nós nos tornamos achatados, sem subjetividade, pois a subjetividade exige profundidade, mesmo que seja uma profundidade pouco amigável e afogadora.** É hora de escrever A morte da clínica. Os métodos da clínica exigem corpos e trabalhos; nós temos textos e superfícies. **Nossas dominações não funcionam mais por meio da medicalização e da normalização; elas funcionam por meio de redes, do redesenho da comunicação, da administração do estresse. A normalização cede lugar à automação, à absoluta redundância.** Os livros de Michel Foucault – _O nascimento da clínica_, _História da sexualidade_ e _Vigiar e punir_ – descrevem uma forma particular de poder em seu momento de implosão. **O discurso da biopolítica cede lugar, agora, ao jargão técnico, à linguagem do substantivo partido e recombinado; as multinacionais não deixam nenhum nome intacto.** Esses são seus nomes, constantes de uma lista feita a partir de um número da revista Science: Tech-Knowledge, Genentech, Allergen, Hybritech, Compupro, Genen-cor, Syntex, Allelix, Agrigenetics Corp., Syntro, Codon, Repligen, MicroAngelo from Scion Corp., Percom Data, Inter Systems, Cyborg Corp., Statcom Corp., Intertec. **Se é verdade que somos aprisionados pela linguagem, então, a fuga dessa prisão exige poetas da linguagem, exige um tipo de enzima cultural que seja capaz de interromper o código; a heteroglossia ciborguiana é uma das formas de política cultural radical. Para exemplos de poesia-ciborgue, ver Perloff (1984); Fraser (1984). Para exemplos de escrita-ciborgue modernista/pós-modernista feminista, ver HOW(ever), 871 Corbett Ave, San Francisco, CA 94131.**
> (p. 102-3)

# Físico--não-físico. A miniaturização como poder. Fluidez (eletromagnetismo) do ciborgue.

> A terceira distinção é um subconjunto da segunda: **a fronteira entre o físico e o não físico é muito imprecisa para nós**. Os livros populares de Física que se centralizam nas consequências da teoria quântica e no princípio da indeterminação são uma espécie de equivalente científico popular da literatura cor-derosa dos romances baratos, servindo como marcadores de uma mudança radical na heterossexualidade branca americana: eles erram na interpretação, mas acertam no problema. Os dispositivos microeletrônicos são, tipicamente, as máquinas modernas: eles estão em toda parte e são invisíveis. **A maquinaria moderna é um deus irreverente e ascendente, arremedando a ubiquidade e a espiritualidade do Pai. O chip de silício é uma superfície de escrita; ele está esculpido em escalas moleculares, sendo perturbado apenas pelo ruído atômico – a interferência suprema nas partituras nucleares.** A escrita, o poder e a tecnologia são velhos parceiros nas narrativas de origem da civilização, típicas do Ocidente, mas a miniaturização mudou nossa percepção sobre a tecnologia. **A miniaturização acaba significando poder; o pequeno não é belo: tal como ocorre com os mísseis ele é, sobretudo, perigoso.** Contrastem os aparelhos de TV dos anos 50 ou as câmeras dos anos 70 com as TVs de pulso ou com as câmeras de vídeo que cabem na palma da mão. **Nossas melhores máquinas são feitas de raios de sol; elas são, todas, leves e limpas porque não passam de sinais, de ondas eletromagnéticas, de uma secção do espectro.** Além disso, essas máquinas são eminentemente portáteis, móveis – um fragmento da imensa dor humana que é infligida cotidianamente em Detroit ou Cingapura. **As pessoas estão longe de serem assim tão fluidas, pois elas são, ao mesmo tempo, materiais e opacas. Os ciborgues, em troca, são éter, quintessência.**
> (p. 43-4)

> É precisamente a ubiquidade e a invisibilidade dos ciborgues que faz com que essas minúsculas e leves máquinas sejam tão mortais. **Eles são – tanto política quanto materialmente – difíceis de ver. Eles têm a ver com a consciência – ou com sua simulação. 6 Eles são significantes flutuantes, movimentando-se em caminhões através da Europa: eles só podem ser bloqueados pelas bruxarias daquelas que são capazes de interpretar as redes ciborguianas de poder – as deslocadas e pouco naturais mulheres de Greenham – e não pelos velhos sindicalistas militantes das políticas masculinistas cujos clientes naturais dependem dos empregos da indústria militar.** Em última instância, a ciência “mais dura” tem a ver com o domínio da maior confusão de fronteiras – **o domínio do número puro, do espírito puro, o C3I, a criptografia e a preservação de poderosos segredos.** As novas máquinas são tão limpas e leves!
> (p. 44)

> **As doenças evocadas por essas máquinas limpas “não passam” de minúsculas mudanças no código de um antígeno do sistema imunológico, “não passam” da experiência do estresse.**
> (p. 44)

# Capitalismo avançado como simulacro

> 6: Baudrillard (1983), Jameson (1984, p. 66) observa que a definição de simulacro, dada por Platão, é a de cópia para a qual não existe nenhum original, isto é, o mundo do capitalismo avançado, da pura troca. Veja o número especial de Discourse (nº 9, 1987), sobre tecnologia (“A cibernética, a ecologia e a imaginação pós-moderna”)
> (p. 103)

# Sobre a tecnofobia em movimentos marxista e feminista

> Do livro _One-dimensional man_ (MarCuse , 1964) ao livro _The Death of Nature_ (MerChant , 1980), **os recursos analíticos desenvolvidos pelas pessoas progressistas insistem no argumento de que a técnica envolve, necessariamente, dominação; como resposta, elas apelam em favor de um imaginário corpo orgânico que possa organizar nossa resistência.**
> (p. 45)

# Sobre as possibilidades de dominação e de liberação a partir do ciborgue. Valorização de um pluralismo perspectivo.

> De uma certa perspectiva, um mundo de ciborgues significa a imposição final de uma grade de controle sobre o planeta; significa a abstração final corporificada no apocalipse da Guerra nas Estrelas – uma guerra travada em nome da defesa; significa a apropriação final dos corpos das mulheres numa orgia guerreira masculinista (sofia, 1984). De uma outra perspectiva, **um mundo de ciborgues pode significar realidades sociais e corporais vividas, nas quais as pessoas não temam sua estreita afinidade com animais e máquinas, que não temam identidades permanentemente parciais e posições contraditórias.** **A luta política consiste em ver a partir de ambas as perspectivas ao mesmo tempo, porque cada uma delas revela tanto dominações quanto possibilidades que seriam inimagináveis a partir do outro ponto de vista. Uma visão única produz ilusões piores do que uma visão dupla ou do que a visão de um monstro de múltiplas cabeças.**
> (p. 46)

# Contra uma noção essencialista de "mulher" [e de raça, e de classe]. Afinidade ao invés de identidade.

> Tem-se tornado difícil nomear nosso feminismo por um único adjetivo – ou até mesmo insistir na utilização desse nome, sob qualquer circunstância. A consciência da exclusão que é produzida por meio do ato de nomeação é aguda. As identidades parecem contraditórias, parciais e estratégicas. **Depois do reconhecimento, arduamente conquistado, de que o gênero, a raça e a classe são social e historicamente constituídos, esses elementos não podem mais formar a base da crença em uma unidade “essencial”. Não existe nada no fato de ser “mulher” que naturalmente una as mulheres. Não existe nem mesmo uma tal situação – “ser” mulher. Trata-se, ela própria, de uma categoria altamente complexa, construída por meio de discursos científicos sexuais e de outras práticas sociais questionáveis.** A consciência de classe, de raça ou de gênero é uma conquista que nos foi imposta pela terrível experiência histórica das realidades sociais contraditórias do capitalismo, do colonialismo e do patriarcado. **E quem é esse “nós” que é enunciado em minha própria retórica? Quais são as identidades que fundamentam esse mito político tão potente chamado “nós” e o que pode motivar o nosso envolvimento nessa comunidade? A existência de uma dolorosa fragmentação entre as feministas (para não dizer “entre as mulheres”), ao longo de cada fissura possível, tem feito com que o conceito de mulher se torne escorregadio: ele acaba funcionando como uma desculpa para a matriz das dominações que as mulheres exercem umas sobre as outras.** Para mim – e para muitas outras mulheres que partilham de uma localização histórica similar (corpos brancos, de classe média profissional, femininos, de esquerda, estadunidense, de meia-idade) – as fontes dessa crise de identidade política são são incontáveis. A história recente de grande parte da esquerda e do feminismo estadunidense tem sido construída a partir das respostas a esse tipo de crise – respostas que são dadas por meio de infindáveis cisões e de buscas por uma nova unidade essencial. **Mas existe também um reconhecimento crescente de uma outra resposta: aquela que se dá por meio da coalizão – a afinidade em vez da identidade.**
> (p. 47-8)

# Consciência de oposição [de mulheres negras] em Chela Sandoval

> Chela Sandoval (s.d., 1984) discute, a partir da história da formação da nova voz política representada pelas mulheres de cor [[ no original: _women of colour_ ]], um novo modelo de identidade política que ela chama de “consciência de oposição”. **Esse modelo baseia-se naquela capacidade de analisar as redes de poder que já foi demonstrada por aquelas pessoas às quais foi negada a participação nas categorias sociais da raça, do sexo ou da classe.** A identidade “mulheres de cor” – um nome contestado em suas origens por aquelas pessoas que ele deveria incorporar – produz não apenas uma consciência histórica que **assinala o colapso sistemático de todos os signos de Homem nas tradições “ocidentais”, mas também, a partir da outridade, da diferença e da especificidade, uma espécie de identidade pós-modernista.** Independentemente do que possa ser dito sobre outros possíveis pós-modernismos, essa identidade pós-modernista é plenamente política. **A “consciência de oposição” de Sandoval tem a ver com localizações contraditórias e calendários heterocrônicos e não com relativismos e pluralismos.**
> (p. 48)

> **Sandoval enfatiza que não existe nenhum critério essencialista que permita identificar quem é uma mulher de cor. Ela observa que a definição desse grupo tem sido feita por meio de uma consciente apropriação da negação. Por exemplo, uma chicana ou uma mulher estadunidense negra não pode falar como uma mulher (em geral) ou como uma pessoa negra ou como um chicano. Assim, ela está no degrau mais baixo de uma hierarquia de identidades negativas, excluída até mesmo daquelas categorias oprimidas privilegiadas constituídas por “mulheres e negros”, categorias que reivindicam o feito de terem realizado importantes revoluções.** A categoria “mulher” nega todas as mulheres não brancas; a categoria “negro” nega todas as pessoas não negras, bem como todas as mulheres negras. **Mas tampouco existe qualquer coisa que se possa chamar de “ela”, tampouco existe qualquer singularidade; o que existe é um mar de diferenças entre os diversos grupos de mulheres estadunidenses que têm afirmado sua identidade histórica como mulheres estadunidenses de cor.** Essa identidade assinala um espaço construído de forma autoconsciente. Sua capacidade de ação não pode ter como base qualquer identificação supostamente natural: **sua base é a coalizão consciente, a afinidade, o parentesco político.** Diferentemente da identidade “mulher” de algumas correntes do movimento das mulheres brancas estadunidenses, não existe, aqui, qualquer naturalização de uma suposta matriz identitária: **essa identidade é o produto do poder da consciência de oposição.**
> (p. 48-9)

> Sandoval argumenta que as “mulheres de cor” têm a chance de **construir uma eficaz unidade política que não reproduza os sujeitos revolucionários imperializantes e totalizantes dos marxismos e feminismos anteriores** – movimentos teóricos e políticos que têm sido incapazes de responder às consequências da desordenada polifonia surgida do processo de descolonização.
> (p. 49-50)

# Críticas da taxonomização [sectarismo? apagamento de vertentes menores?] do movimento feminista em King

> Katie King, por sua vez, tem discutido os limites do processo de identificação e da estratégia político-poética da construção de identidade que faz parte do ato de ler o “poema”, esse núcleo gerador do feminismo cultural. King critica a persistente tendência, entre as feministas contemporâneas de diferentes “momentos” ou “versões” da prática feminista, a taxonomizar o movimento das mulheres, tendência que faz com que as nossas próprias tendências políticas pareçam ser o telos da totalidade. **Essas taxonomias tendem a refazer a história feminista, de modo que essa história pareça ser uma luta ideológica entre categorias coerentes e temporalmente contínuas – especialmente entre aquelas unidades típicas conhecidas como feminismo radical, feminismo liberal e feminismo socialista-feminista. Todos os outros feminismos ou são incorporados ou são marginalizados, em geral por meio da construção de uma ontologia e de uma epistemologia explícitas.** As taxonomias do feminismo produzem epistemologias que acabam por policiar qualquer posição que se desvie da experiência oficial das mulheres. Obviamente, a “cultura das mulheres”, tal como a cultura das mulheres de cor, é criada, de forma consciente, pelos mecanismos que estimulam a afinidade, destacando-se os rituais da poesia, da música e de certas formas de prática acadêmica. A política da raça e a política da cultura estão, nos movimentos das mulheres dos Estados Unidos, estreitamente interligadas. **Aprender como forjar uma unidade poético-política que não reproduza uma lógica da apropriação, da incorporação e da identificação taxonômica – esta é a contribuição de King e de Sandoval.**
> (p. 50-1)

# Alternativas à unidade-por-meio-da-dominação ou da unidade-por-meio-da-incorporação

> **A luta teórica e prática contra a unidade-por-meio-da-dominação ou contra a unidade-por-meio-da-incorporação implode, ironicamente, não apenas as justificações para o patriarcado, o colonialismo, o humanismo, o positivismo, o essencialismo, o cientificismo e outros “ismos”, mas também todos os apelos em favor de um estado orgânico ou natural.** Penso que os feminismos radicais e socialistas-marxistas têm implodido também suas/nossas próprias estratégias epistemológicas e que isso constitui um passo valioso para se imaginar possíveis unidades políticas. Resta saber se existe alguma “epistemologia”, no sentido ocidental, que possa nos ajudar na tarefa de **construir afinidades eficazes.**
> (p. 51)

# Sobre a negação de uma categoria identitária natural ou total

> As mulheres brancas, incluindo as feministas socialistas, descobriram a não inocência da categoria “mulher” (isto é, foram forçadas, aos pontapés e aos gritos, a se darem conta disso). Essa consciência muda a geografia de todas as categorias anteriores; ela as desnatura, da mesma forma que o calor desnatura uma proteína frágil. **As feministas-ciborgue têm que argumentar que “nós” não queremos mais nenhuma matriz identitária natural e que nenhuma construção é uma totalidade.**
> (p. 52)

# Recusa do pós-modernismo na psicanálise e na construção de epistemologias

> 16: O papel central das torias de relações de objeto da Psicanálise e outras tendências universalizantes na teorização da reprodução, do trabalho feminino relacionado ao cuidado dos filhos e da maternidade, presentes em muitas abordagens epistemológicas, demonstra a resistência de seus autores ou de suas autoras àquilo que estou chamando de “pós-modernismo”. Para mim, tanto as tendências universalizantes quanto essas teorias psicanalíticas tornam difícil uma análise do “lugar da mulher no circuito integrado”, levando a dificuldades sistemáticas na teorização sobre a construção das relações sociais e da vida social em termos de gênero. O argumento da posição feminista tem sido desenvolvido por: Flax (1983), Harding (1986), Harding e Hintikka (1983), Hartsock (1983a, b), O’Brien (1981), Rose (1983), Smith (1974, 1979). Sobre as teorias que repensam as teorias do materialismo feminista e sobre as posições feministas desenvolvidas em resposta à essa crítica, ver Harding (1986, p. 163-96), Hartsock (1987) e H. Rose (1986).
> (p. 105)

# O feminismo-marxista/socialista enfatiza a responsabilidade cotidiana de mulheres na construção de unidades, a despeito da operação essencializadora da estrutura ontológica do trabalho

> Em uma fiel filiação, a aliança com as estratégias analíticas básicas do marxismo permitiu que o feminismo-socialista avançasse. **A principal conquista tanto dos feminismos marxistas quanto dos feminismos socialistas foi a de ampliar a categoria “trabalho” para acomodar aquilo que (algumas) mulheres faziam, mesmo quando a relação assalariada estava subordinada a uma visão mais abrangente do trabalho sob o patriarcado capitalista. Em particular, o trabalho das mulheres na casa e a atividade das mulheres, em geral, como mães (isto é, a reprodução no sentido socialistafeminista), foram introduzidos na teoria com base em uma analogia com o conceito marxiano de trabalho.** A unidade das mulheres, aqui, repousa em uma epistemologia que se baseia na estrutura ontológica do “trabalho”. **O feminismo-marxista/ socialista não “naturaliza” a unidade; trata-se de uma possível onquista que se baseia em uma possível posição enraizada nas relações sociais. A operação essencializadora está na estrutura ontológica do trabalho ou na estrutura de seu análogo (a atividade das mulheres).16 A herança do humanismo marxiano com seu eu eminentemente ocidental é o que, para mim, constitui a dificuldade. A contribuição dessas formulações está na ênfase da responsabilidade cotidiana de mulheres reais na construção de unidades e não em sua naturalização.**
> (p. 53-4)

# O reducionismo na teoria feminista de MacKinnon (~feminismo radical?)

> A reescrita da história do polimórfico campo chamado “feminismo radical” é apenas um dos efeitos da teoria de MacKinnon. O efeito principal é a produção de uma teoria da experiência e da identidade das mulheres que representa uma espécie de apocalipse para todas as perspectivas revolucionárias. **Isto é, a totalização inerente a essa fábula do feminismo radical atinge sua finalidade – a unidade política das mulheres – ao impor a experiência do não-ser radical e seu testemunho.** Para o feminismo marxista/socialista, a consciência é uma conquista e não um fato natural. A teoria de MacKinnon elimina, na verdade, algumas das dificuldades inerentes à concepção humanista do sujeito revolucionário, mas ao custo de um reducionismo radical.
> (p. 54)

> 17: Cometo um erro argumentativo de categoria ao “modificar” as posições de MacKinnon com o qualificativo “radical”, produzindo, assim, eu própria, um reducionismo relativamente a uma escrita que é extremamente heterogênea e que não utiliza, explicitamente, aquele rótulo. Meu erro de categoria foi causado por uma solicitação para escrever um ensaio, para a revista Socialist Review, a partir de uma posição taxonômica – o feminismo-socialista – que tem, ela própria, uma história heterogênea. Uma crítica inspirada em MacKinnon, mas sem o seu reducionismo e com uma elegante análise feminista do paradoxal conservadorismo de Foucault em questões de violência sexual (estupro), é a feita por Lauretis (1985; ver também 1986, p. 1-19). Uma análise sócio-histórica teoricamente feminista da violência familiar, que insiste na complexidade do papel ativo das mulheres, dos homens e das crianças, sem perder de vista as estruturas materiais da dominação masculina, bem como das dominações de raça e de classe, é a feita por Gordon (1988).
> (p. 105)

# Similaridades entre a abordagem marxista e o feminismo radical[?]

> Perversamente [[ considerando-se a recusa de MacKinnon a relacionar feminismo (sexo/gênero) com marxismo (classe) ]], **a apropriação sexual ainda tem, nesse feminismo, o _status_ epistemológico do trabalho, isto é, o trabalho é o ponto a partir do qual uma análise capaz de contribuir para mudar o mundo deve fluir. Mas a objetificação sexual e não a alienação é a consequência da estrutura de sexo/gênero. No domínio do conhecimento, o resultado da objetificação sexual é a ilusão e a abstração.** Entretanto, a mulher não é simplesmente alienada de seu produto: **em um sentido profundo, ela não existe como sujeito, nem mesmo como sujeito potencial, uma vez que ela deve sua existência como mulher à apropriação sexual.** Ser constituída pelo desejo de um outro não é a mesma coisa que ser alienada por meio da separação violenta do produto de seu próprio trabalho.
> (p. 55)

> A teoria da experiência desenvolvida por MacKinnon é extremamente totalizadora. Ela não marginaliza a autoridade da fala e da ação política de qualquer outra mulher; ela as elimina. **Trata-se de uma totalização que produz aquilo que o próprio patriarcado ocidental não conseguiu – o sentimento de que as mulheres não existem a não ser como produto do desejo dos homens.**
> (p. 55-6)

> Em minha taxonomia, que como em qualquer outra, é uma reinscrição da história, o feminismo radical pode acomodar qualquer atividade feminina identificada pelas feministas socialistas como form,a de trabalho apenas se essa atividade puder, de alguma forma, ser sexualizada. **A reprodução tem diferentes conotações para as duas tendências – para uma, ela está enraizada no trabalho; para a outra, no sexo; ambas chamam as consequências da dominação e o desconhecimento da realidade social e pessoal de “falsa consciência”.**
> (p. 56)

> _Feminismo socialista_
>  - estrutura de classe // trabalho assalariado // alienação
>  - trabalho – por analogia: reprodução; por extensão: sexo; por acréscimo: raça
> 
> _Feminismo radical:_
>  - estrutura de gênero // apropriação sexual // objetificação
>  - sexo – por analogia: trabalho; por extensão: reprodução; por acréscimo: raça
> 
> (p. 57)

# Ausência de questionamentos sobre raça no feminismo racial e marxista

>  Cada uma delas [[ autoras feministas ocidentais ]] tentou anexar outras formas de dominação, expandindo suas categorias básicas por meio de analogias, de simples listagens ou de acréscimos. **Uma das principais e devastadoras consequências disso é a existência de um silêncio constrangedor, entre as radicais brancas e as feministas socialistas, sobre a questão da raça. A história e o polivocalismo desaparecem em meio às taxonomias políticas que tentam instituir genealogias. Não há nenhum espaço estrutural para a raça (ou para muita coisa mais) em teorias que pretendem apresentar a construção da categoria “mulher” e do grupo social “mulheres” como um todo unificado ou totalizável.**
> (p. 57)

# "Epistemologia" como "conhecer a diferença (?)

> A teórica francesa, Julia Kristeva, afirma, em outro contexto, que as mulheres surgiram como um grupo histórico após a Segunda Guerra Mundial, juntamente com outros grupos como, por exemplo, a juventude. Sua datas são duvidosas; mas estamos agora acostumados a lembrar que, como objetos de conhecimento e como atores históricos, a “raça” nem sempre existiu, a “classe” tem uma gênese histórica e os “homossexuais” são bastante recentes. Não é por acaso que o sistema simbólico da família do homem – e, portanto, a essência da mulher – entra em colapso no mesmo momento em que as redes de conexão entre as pessoas no planeta se tornam, de forma sem precedentes, múltiplas, pregnantes e complexas. [[ ... ]] Mas ao nos tornarmos conscientes de nossos fracassos, arriscamos cair em uma diferença ilimitada, desistindo da complicada tarefa de realizar conexões parciais, reais. Algumas diferenças são lúdicas; outras são polos de sistemas históricos mundiais de dominação. “Epistemologia” significa conhecer a diferença.
> (p. 57-8)

# A informática da dominação

> Estamos em meio à mudança: **de uma sociedade
> industrial, orgânica, para um sistema polimorfo, informacional;
> de uma situação de “só trabalho” para uma situação de “só la-
> zer”**. Trata-se de um jogo mortal. Simultaneamente materiais e
> ideológicas, as dicotomias aí envolvidas podem ser expressas por
> meio do seguinte quadro, que resume **a transição das velhas e
> confortáveis dominações hierárquicas para as novas e assustadoras
> redes que chamei de “informática da dominação”**:
> 
> |   |   |
> |---|---|
> | Representação                   | Simulação |
> | Romance burguês, realismo       | Ficção científica, pós-modernismo |
> | Organismo                       | Componente biótico |
> | Profundidade, integridade       | Superfície, fronteira |
> | Calor                           | Ruído |
> | Biologia como prática clínica   | Biologia como inscrição |
> | Fisiologia                      | Engenharia de comunicação |
> | Pequeno grupo                   | Subsistema |
> | Perfeição                       | Otimização |
> | Eugenia                         | Controle populacional |
> | Decadência, Montanha mágica     | Obsolescência, Choque do futuro |
> | Higiene                         | Administração do estresse |
> | Microbiologia, tuberculose      | Imunologia, AIDS |
> | Divisão orgânica do trabalho    | Ergonomia/cibernética do trabalho |
> | Especialização funcional        | Construção modular |
> | Reprodução                      | Replicação |
> | Especialização do papel social com base no sexo orgânico | Estratégias genéticas otimizadas |
> | Determinismo biológico          | Inércia evolucionária, restrições |
> | Ecologia comunitária            | Ecosistema |
> | Cadeia racial do ser            |Neo-imperialismo, humanismo das Nações Unidas |
> | Administração científica na casa/fábrica | Fábrica global/Trabalho feito em casa por meio das tecnologias eletrônicas |
> | Família/Mercado/Fábrica         | Mulheres no circuito integrado |
> | Salário-família                 | Valor comparável |
> | Público/Privado                 | Cidadania do tipo “ciborgue” |
> | Natureza/Cultura                | Campos de diferença |
> | Cooperação                      | Reforço na comunicação |
> | Freud                           | Lacan |
> | Sexo                            | Engenharia genética |
> | Trabalho                        | Robótica |
> | Mente                           | Inteligência artificial |
> | Segunda Guerra Mundial          | Guerra nas estrelas |
> | Patriarcado capitalista branco  | Informática da dominação |
> 
> (p. 59-60)

# Confusão em relação ao termo "natural", alargamento do que se caracteriza um componente boótico (essência x projeto)

> Essa lista sugere diversas coisas interessantes. 18 Em primeiro lugar, os objetos situados no lado direito não podem ser compreendidos como “naturais”, o que nos impede de compreender como naturais também os objetos do lado esquerdo. Não podemos voltar ao passado – ideológica ou materialmente. Não se trata apenas de que “deus” está morto: a “deusa” também está. Ou, se quisermos, podemos vê-los, a ambos, como revivificados nos mundos das políticas microeletrônica e biotecnológica.
> (p. 60)

> Em relação a objetos tais como componentes bióticos, devemos pensar não em termos de propriedades essenciais, mas em termos de projeto, restrições de fronteira, taxas de fluxo, lógica de sistemas, custos para se reduzir as restrições. **A reprodução sexual é um tipo, entre muitos, de estratégia reprodutiva, com custos e benefícios que são uma função do ambiente sistêmico.**
> (p. 60-1)

> As ideologias da reprodução sexual não poderão mais, de forma razoável, apelar a concepções sobre sexo e sobre papéis sexuais, com o argumento de que constituiriam aspectos orgânicos de objetos naturais tais como organismos e famílias. Um tal raciocínio será desmascarado como sendo irracional: **os executivos das grandes corporações que leem Playboy e as feministas radicais que são contra a pornografia formarão, ironicamente, um estranho par no desmascaramento conjunto do irracionalismo.**
> (p. 61)

# Semiologias ciborguianas (?)

> **Em termos ideológicos, o racismo e o colonialismo expressam-se, agora, em uma linguagem que fala em desenvolvimento e subdesenvolvimento, em graus e níveis de modernização.** Pode-se pensar qualquer objeto ou pessoa em termos de desmontagem e remontagem; não existe nenhuma arquitetura “natural” que determine como um sistema deva ser planejado. Os centros financeiros de todas as cidades do mundo, bem como as zonas de processamento de exportação e de livre comércio, proclamam este fato elementar do “capitalismo tardio”: **o universo inteiro dos objetos que podem ser cientificamente conhecidos deve ser formulado como um problema de engenharia de comunicação (para os administradores) ou como uma teoria do texto (para aqueles que possam oferecer resistência). Trata-se, em ambos os casos, de semiologias ciborguianas.**
> (p. 61-2)

# Caráter probabilístico-estatístico do controle

> As estratégias de controle irão se concentrar nas condições e nas interfaces de fronteira, bem como nas taxas de fluxo entre fronteiras, e não na suposta integridade de objetos supostamente naturais. A “integridade” ou a “sinceridade” do eu ocidental cede lugar a procedimentos decisórios e a sistemas especializados. Por exemplo, as estratégias de controle aplicadas às capacidades das mulheres para dar à luz a novos seres humanos serão desenvolvidas em uma linguagem que se expressará em termos de controle populacional e de maximização da realização de objetivos, concebendo-se esses últimos como um processo individual de tomada de decisão. **As estratégias de controle serão formuladas em termos de taxas, custos de restrição, graus de liberdade. Os seres humanos, da mesma forma que qualquer outro componente ou subsistema, deverão ser situados em uma arquitetura de sistema cujos modos de operação básicos serão probabilísticos, estatístico.**
> (p. 62)

# Dessacralização da vida (~homogeneização ante à noção de informação em Simondon e Laymert). Troca -> tradução.

> **Nenhum objeto, nenhum espaço, nenhum corpo é, em si, sagrado; qualquer componente pode entrar em uma relação de interface com qualquer outro desde que se possa construir o padrão e o código apropriados, que sejam capazes de processar sinais por meio de uma linguagem comum [[ ~informação ]].** **A troca, nesse mundo, transcende à tradução universal** efetuada pelos mercados capitalistas, tão bem analisada por Marx. Nesse universo, a patologia privilegiada, uma patologia que afeta todos os tipos de componentes, é o estresse – um colapso nas comunicações (hoGness, 1983). O ciborgue não está sujeito à biopolítica de Foucault; o ciborgue simula a política, uma característica que oferece um campo muito mais potente de atividades.
> (p. 62-3)

# Situação das mulheres ~ integração/exploração na informática dadominação (sistema de produção/reprodução e comunicação)

> As dicotomias entre mente e corpo, animal e humano, organismo e máquina, público e privado, natureza e cultura, homens e mulheres, primitivo e civilizado estão, todas, ideologicamente em questão. **A situação real das mulheres é definida por sua integração/ exploração em um sistema mundial de produção/reprodução e comunicação que se pode chamar de “informática da dominação”. A casa, o local de trabalho, o mercado, a arena pública, o próprio corpo, todos esses locais podem ser dispersados e entrar em relações de interface, sob formas quase infinitas e polimórficas, com grandes consequências para as mulheres e outros grupos – consequências que são, elas próprias, muito diferentes para as diferentes pessoas, o que faz com que seja difícil imaginar fortes movimentos internacionais de oposição, embora eles sejam essenciais para a sobrevivência.**
> (p. 63)

# Relações bilaterais entre social (como contexto e como projeto) e tecnológico (como ferramenta e como mito)

> As tecnologias e os discursos científicos podem ser parcialmente compreendidos como formalizações, isto é, **como momentos congelados das fluidas interações sociais que as constituem, mas eles devem ser vistos também como instrumentos para a imposição de significados.** A fronteira entre ferramenta e mito, instrumento e conceito, sistemas históricos de relações sociais e anatomias históricas dos corpos possíveis (incluindo objetos de conhecimento) é permeável. **Na verdade, o mito e a ferramenta são mutuamente constituídos.**
> (p. 64)

# Codificação como problema comum à biologia e ciências da comunicação

> Além disso, as ciências da comunicação e as biologias modernas são construídas por uma operação comum – _a tradução do mundo em termos de um problema de codificação_, isto é, a busca de uma linguagem comum na qual toda a resistência ao controle instrumental desaparece e toda a heterogeneidade pode ser submetida à desmontagem, à remontagem, ao investimento e à troca.
> (p. 64)

# Tradução na cibernética

> Em cada caso [[ de tecnologias cibernéticas ]], a solução para as questões-chave repousa em uma teoria da linguagem e do controle; **a operação-chave consiste em determinar as taxas, as direções e as probabilidades do fluxo de uma quantidade chamada informação. O mundo é subdividido por fronteiras diferencialmente permeáveis à informação.**
> (p. 64-5)

# Informação como condição da tradução universal

> **A informação é apenas aquele tipo de elemento quantificável (unidade, base da unidade) que permite uma tradução universal e, assim, um poder universal sem interferências, isto é, aquilo que se chama de “comunicação eficaz”**. A maior ameaça a esse poder é constituída pela interrupção da comunicação. Qualquer colapso do sistema é uma função do estresse. **Os elementos fundamentais dessa tecnologia podem ser condensados na metáfora C 3 I (comando-controle-comunicação-inteligência) – o símbolo dos militares para sua teoria de operações.**
> (p. 65)

# Tradução na biologia

> Nas biologias modernas, a tradução do mundo em termos de um problema de codificação pode ser ilustrada pela biologia molecular, pela ecologia, pela teoria evolucionária sociobiológica e pela imunobiologia. Nesses campos, **o organismo é traduzido em termos de problemas de codificação genética e de leitura de códigos.** A biotecnologia – uma tecnologia da escrita – orienta a pesquisa em geral.19 **Em um certo sentido, os organismos deixaram de existir como objetos de conhecimento, cedendo lugar a componentes bióticos, isto é, tipos especiais de dispositivos de processamento de informação.**
> (p. 65)

> A biologia é, nesse caso, uma espécie de criptografia. A pesquisa é necessariamente uma espécie de atividade de inteligência.
> (p. 65)

# A eletrônica (material) como base técnica dos simulacros

> As tecnologias da comunicação dependem da eletrônica. Os estados modernos, as corporações multinacionais, o poder militar, os aparatos do estado de bem-estar, os sistemas de satélite, os processos políticos, a fabricação de nossas imaginações, os sistemas de controle do trabalho, as construções médicas de nossos corpos, a pornografia comercial, a divisão internacional do trabalho e o evangelismo religioso dependem, estreitamente, da eletrônica. A microeletrônica é a base técnica dos simulacros, isto é, de cópias sem originais.
> (p. 66)

# A microeletrônica como centro dos processos informatizados contemporâneos

> **A microeletrônica está no centro do processo que faz a tradução do trabalho em termos de robótica e de processamento de texto, do sexo em termos de engenharia genética e de tecnologias reprodutivas e da mente em termos de inteligência artificial e de procedimentos de decisão.** As novas biotecnologias têm a ver com mais coisas do que simplesmente reprodução humana. Como uma poderosa ciência da engenharia para redesenhar materiais e processos, a biologia tem implicações revolucionárias para a indústria, talvez mais óbvias hoje em áreas como fermentação, agricultura e energia. **As ciências da comunicação e a biologia caracterizam-se como construções de objetos tecnonaturais de conhecimento, nas quais a diferença entre máquina e organismo torna-se totalmente borrada; a mente, o corpo e o instrumento mantêm, entre si, uma relação de grande intimidade.** A organização material “multinacional” da produção e reprodução da vida cotidiana, de um lado, e a organização simbólica da produção e reprodução da cultura e da imaginação, de outro, parecem estar igualmente implicadas nesse processo. **As imagens que supõem uma manutenção das fronteiras entre a base e a superestrutura, o público e o privado ou o material e o ideal nunca pareceram tão frágeis.**
> (p. 67)

# Mulheres no circuito integrado

> Tenho utilizado o conceito – inventado por Rachel Grossman (1980) – de **“mulheres no circuito integrado”, para nomear a situação das mulheres em um mundo tão intimamente reestruturado por meio das relações sociais da ciência e da tecnologia.** 20 Utilizei a circunlocução “as relações sociais da ciência e da tecnologia” para indicar que não estamos lidando com um determinismo tecnológico, mas com um sistema histórico que depende de relações estruturadas entre as pessoas. Mas a frase deveria também indicar que a ciência e a tecnologia fornecem fontes renovadas de poder, que nós precisamos de fontes renovadas de análise e de ação política (latour, 1984). Alguns dos rearranjos das dinâmicas da raça, do sexo e da classe, enraizados nas relações sociais propiciadas pela cultura high-tech, podem tornar o feminismo-socialista mais relevante para uma política progressista eficaz.
> (p. 67)

# "Nova revolução industrial", nova classe trabalhadora e novos tipos de exploração da mullher trabalhadora [~sociedades de controle, virada cibernética ?]

> **A “Nova Revolução Industrial” está produzindo uma nova classe trabalhadora mundial, bem como novas sexualidades e etnicidades.** A extrema mobilidade do capital e a nova divisão internacional do trabalho estão interligadas com a emergência de novas coletividades e com o enfraquecimento dos agrupamentos familiares. Esses acontecimentos não são neutros em termos de gênero nem em termos de raça. Nas sociedades industriais avançadas, os homens brancos têm se tornado vulneráveis, de uma maneira nova, à perda permanente do emprego, enquanto as mulheres não têm perdido seus empregos na mesma proporção que os homens. Não se trata simplesmente do fato de que as mulheres dos países do Terceiro Mundo são a força de trabalho preferida das multinacionais dos setores de processamento de exportação, particularmente do setor eletrônico, cuja produção está baseada na ciência. **O quadro é mais sistemático e envolve reprodução, sexualidade, cultura, consumo e produção. No paradigmático _Sillicon Valley_, muitas mulheres têm suas vidas estruturadas em torno de empregos baseados na eletrônica e suas realidades íntimas incluem monogamia heterossexual em série, cuidado infantil negociado, distância da família ampliada ou da maior parte das formas tradicionais de comunidade, uma grande probabilidade de uma vida solitária e uma extrema vulnerabilidade econômica à medida que envelhecem.** A diversidade étnica e racial das mulheres do _Sillicon Valley_ forma um microcosmo de diferenças conflitantes na cultura, na família, na religião, na educação e na linguagem.
> (p. 68)

# Economia do trabalho caseiro para Richard Gordon

> Richard Gordon chamou essa nova situação de “economia do trabalho caseiro”.21 Embora ele inclua o fenômeno do trabalho caseiro propriamente dito, que está emergindo em conexão com a linha de montagem do setor eletrônico, Gordon quer nomear, com **a expressão “economia do trabalho caseiro”, uma reestruturação do trabalho que, de forma geral, tem as características anteriormente atribuídas a trabalhos femininos, trabalhos que são feitos, estritamente, por mulheres.**
> (p. 69)

> Ser feminizado significa: **tornar-se extremamente vulnerável; capaz de ser desmontado, remontado, explorado como uma força de trabalho de reserva; que as pessoas envolvidas são vistas menos como trabalhadores/ as e mais como servos/as; sujeito a arranjos do tempo em que a pessoa ora está empregada num trabalho assalariado ora não, num infeliz arremedo da ideia de redução do dia de trabalho; levar uma vida que sempre beira a ser obscena, deslocada e reduzível ao sexo.**
> (p. 69)

> Em vez disso [[ desqualificação em larga escala de trabalhadoras previamente privilegiadas ]], o conceito quer indicar que **a fábrica, a casa e o mercado estão integrados em uma nova escala e que os lugares das mulheres são cruciais** – e precisam ser analisados pelas diferenças existentes entre as mulheres e pelos significados das relações existentes entre homens e mulheres, em várias situações.
> (p. 69)

> **A economia do trabalho caseiro, considerada como uma estrutura organizacional capitalista mundial, torna-se possível por meio das novas tecnologias, embora não seja causada por ela.** O êxito do ataque contra os empregos relativamente privilegiados dos trabalhadores masculinos sindicalizados – em grande parte brancos – está ligado à **capacidade que têm as novas tecnologias de comunicação de integrar e controlar os trabalhadores, apesar de sua grande dispersão e descentralização.** As consequências das novas tecnologias são sentidas pelas mulheres tanto na perda do salário-família (masculino) – quando elas chegaram a ter acesso a esse privilégio dos brancos – quanto no caráter de seus próprios empregos, os quais estão se tornando capital-intensivo como, por exemplo, no trabalho de escritório e na enfermagem.
> (p. 70)

# Novos arranjos econômicos ~ decadência do estado de bem-estar social -> feminização da pobreza

> Os novos arranjos econômicos e tecnológicos estão relacionados também à decadência do estado do bem-estar e à consequente intensificação da pressão sobre as mulheres para que assumam o sustento da vida cotidiana tanto para si próprias quanto para os homens, crianças e pessoas mais velhas. **A feminização da pobreza – gerada pelo desmantelamento do estado de bem-estar, pela economia do trabalho caseiro, na qual empregos estáveis são a exceção, e sustentada pela expectativa de que os salários das mulheres não serão igualados aos salários masculinos – tornou-se um grande problema.** O fato de que um número crescente de lares são chefiados por mulheres está relacionado à raça, à classe ou à sexualidade. A generalização desse processo deveria levar à construção de coalizões entre as mulheres, organizadas em torno de várias questões. O fato de que o sustento da vida cotidiana cabe às mulheres como parte de sua forçada condição de mães não é nenhuma novidade; **o que é novidade é a integração de seu trabalho à economia capitalista global e a uma economia que progressivamente se torna centrada em torno da guerra.**
> (p. 70-1)

> No quadro das três principais fases do capitalismo (comercial/industrial inicial, monopolista, multinacional) ligadas, respectivamente, ao nacionalismo, ao imperialismo e ao multinacionalismo e relacionadas, também respectivamente, aos três períodos estéticos dominantes de Jameson (realismo, modernismo e pós-modernismo), eu argumentaria que formas específicas de famílias relacionam-se dialeticamente com aquelas formas de capitalismo e com as correspondentes formas políticas e culturais mencionadas. **Embora vividas de forma problemática e desigual, as formas ideais dessas famílias podem ser esquematizadas como: 1) a família nuclear patriarcal, estruturada pela dicotomia entre o público e o privado e acompanhada pela ideologia burguesa branca de separação entre a esfera pública e a privada e pelo feminismo burguês anglo-americano do século XIX; 2) a família moderna mediada (ou imposta) pelo estado de bem-estar e por instituições como o salário-família, com um florescimento de ideologias heterossexuais a-feministas, incluindo suas versões críticas desenvolvidas em Greenwich Village, em torno da Primeira Guerra Mundial; e 3) a “família” da economia do trabalho caseiro, caracterizada por sua contraditória estrutura de casas chefiadas por mulheres, pela explosão dos feminismos e pela paradoxal intensificação e erosão do próprio gênero.**
> (p. 71-2)

| Fase do capitalismo | organização de poder | regime estético | forma familiar |
| --- | --- | ---| --- |
| comercial/industrial inicial | nacionalismo | realismo | família nuclear patriarcal |
| monopolista | imperialismo | modernismo | família moderna mediada pelo Estado |
| multinacional | multinacionalismo | pós-modernismo | "família" da economia do trabalho caseiro |

> À medida que a robótica e as tecnologias que lhe são relacionadas expulsam os homens do emprego nos países “desenvolvidos” e tornam mais difícil gerar empregos masculinos nos países “em desenvolvimento” do Terceiro Mundo e à medida que o escritório automatizado se torna a regra mesmo em países com reserva de trabalhadores, a feminização do trabalho intensifica-se.
> [[...]]
> Um número maior de mulheres e homens ver-se-á frente a situações similares, o que fará com que alianças que atravessem o gênero e a raça, formadas em torno das questões ligadas à sustentação básica da vida (com ou sem empregos), se tornem necessárias e não apenas desejáveis.
> (p. 72)

# Privatização (militarização, ideologias de direita e redefinição das concepções de propriedade) em Petchesky

> As novas tecnologias parecem estar profundamente envolvidas naquelas formas de “privatização” analisadas por Ros Petchesky (1981), nas quais se combinam, de forma sinergética, o processo de militarização, as ideologias e as políticas públicas sobre questões de família, desenvolvidas pela direita, e as redefinições das concepções de propriedade (empresarial e estatal), a qual passa a ser vista como exclusivamente privada.
> (p. 73)

# TIs e erradicação da vida pública

> **As novas tecnologias de comunicação são fundamentais para a erradicação da “vida pública” de todas as pessoas. Isso facilita o florescimento de uma instituição militar high-tech permanente, com prejuízos culturais e econômicos para a maioria das pessoas, mas especialmente para as mulheres.** 
> (p. 73)

# Videogames ~ competição e militarização.

> **Tecnologias como videogames e aparelhos de televisão extremamente miniaturizados parecem cruciais para a produção de formas modernas de “vida privada”. A cultura dos videogames é fortemente orientada para a competição individual e para a guerra espacial.** Desenvolve-se, aqui, em conexão com a dinâmica de gênero, uma imaginação high-tech, uma imaginação que pode contemplar a possibilidade da destruição do planeta, permitindo, como se fosse uma ficção científica, que se escape às suas consequências
> (p. 73)

# Sexualidade ~ instrumentalidade [~Dardot e Laval sobre dispositivos de desempenho e gozo]

> As novas tecnologias afetam as relações sociais tanto da sexualidade quanto da reprodução, e nem sempre da mesma forma. **Os estreitos vínculos entre a sexualidade e a instrumentalidade – uma visão sobre o corpo que o concebe como uma espécie de máquina de maximização da satisfação e da utilidade privadas – são descritos de forma admirável, nas histórias sociobiológicas sobre origem que enfatizam o cálculo genético e descrevem a inevitável dialética da dominação entre os papéis sexuais feminino e masculino.**
> (p. 74)

# Visualização e intervenção do corpo da mulher com as tecnologias contemporâneas

> Essas histórias sociobiológicas baseiam-se em uma visão _high-tech_ do corpo – uma visão que o concebe como um componente biótico ou como um sistema cibernético de comunicação. **Uma das mais importantes transformações da situação reprodutiva das mulheres dá-se no campo médico, no qual as fronteiras de seus corpos se tornam permeáveis, de uma nova forma, à “visualização” e à “intervenção” das novas tecnologias.** Obviamente, saber quem controla a interpretação das fronteiras corporais na hermenêutica médica é uma questão feminista importantíssima.
> (p. 74)

# Rede ideológica [~Chiapelo & Boltanski ?]

> Se foi, alguma vez, possível caracterizar ideologicamente as vidas das mulheres por meio da distinção entre os domínios público e privado, uma distinção que era sugerida por imagens de uma vida operária dividida entre a fábrica e a casa; de uma vida burguesa dividida entre o mercado e a casa; de uma vida de gênero dividida entre os domínios pessoal e político, não é suficiente, agora, nem mesmo mostrar como ambos os termos dessas dicotomias se constroem mutuamente na prática e na teoria. **Prefiro a imagem de uma rede ideológica – o que sugere uma profusão de espaços e identidades e a permeabilidade das fronteiras no corpo pessoal e no corpo político. A ideia de “rede” evoca tanto uma prática feminista quanto uma estratégia empresarial multinacional – tecer é uma atividade para ciborgues oposicionistas.**
> (p. 76)

> **Entretanto, não há nenhum “lugar” [[ das mulheres no circuito integrado: Casa, Mercado, Local de Trabalho Assalariado, Estado, Escola, Hospital-Clínica e Igreja. ]] para as mulheres nessas redes, apenas uma geometria da diferença e da contradição, crucial às identidades ciborguianas das mulheres.** Se aprendermos a interpretar essas redes de poder e de vida social, poderemos construir novas alianças e novas coalizões. Não há como ler a seguinte lista a partir de uma perspectiva identitária, a partir da perspectiva de um eu unitário. **O importante é a dispersão. A tarefa consiste em sobreviver na diáspora.**
> (p. 77)

# Informática da dominação como intensificação da insegurança e empobrecimento cultural, com um fracasso de redes de subsistência

> A única forma de caracterizar a informática da dominação é vê-la como uma **intensificação massiva da insegurança e do empobrecimento cultural, com um fracasso generalizado das redes de subsistência para os mais vulneráveis.**
> (p. 80)

# Dificuldades dos marxismos em compreender a relação dominação-(falsa)consciência [crítica à noção de ideologia e alienação ?]

> Por excelentes razões, **os marxismos veem melhor a dominação, mas têm dificuldades em compreender a falsa consciência e a cumplicidade das pessoas no processo de sua própria dominação, no capitalismo tardio.** É importante lembrar que o que se perde, com esses rearranjos, especialmente do ponto de vista das mulheres, está, com frequência, ligado a formas virulentas de opressão, as quais, em face da violência existente, são nostalgicamente naturalizadas. **A ambivalência para com as unidades rompidas por meio das culturas _high-tech_ exige que não classifiquemos a consciência entre, de um lado, uma “crítica lúcida, como fundamento de uma sólida epistemologia política” e, de outro, uma “consciência falsa e manipulada”, mas que tenhamos uma sutil compreensão dos prazeres, das experiências e dos poderes emergentes, os quais apresentam um forte potencial para mudar as regras do jogo.**
> (p. 81-2)

# Parcialidade do ponto de vista feminista [antecipação da tese desenvolvida em 'conhecimentos situados'?]. Crítica à noção de totalidade (como imperialista). Possibilidade de uma ciência feminista (~parcialidade).

> A **parcialidade permanente dos pontos de vista feministas tem consequências para nossas expectativas relativamente a formas de organização e participação políticas.** Para trabalhar direito, não temos necessidade de uma totalidade. **O sonho feminista sobre uma linguagem comum, como todos os sonhos sobre uma linguagem que seja perfeitamente verdadeira, sobre uma nomeação perfeitamente fiel da experiência, é um sonho totalizante e imperialista. Nesse sentido, em sua ânsia por resolver a contradição, também a dialética é uma linguagem de sonho.** Talvez possamos, ironicamente, aprender, a partir de nossas fusões com animais e máquinas, como não ser o Homem, essa corporificação do logos ocidental. **Do ponto de vista do prazer que se tem nessas potentes e interditadas fusões, tornadas inevitáveis pelas relações sociais da ciência e da tecnologia, talvez possa haver, de fato, uma ciência feminista.**
> (p. 83)

# Autoras de ficção científica feminista como teóricas do ciborgue

> Quero concluir com um mito sobre identidades e sobre fronteiras, o qual pode inspirar as imaginações políticas do final do século XX. Sou devedora, nessa história, a escritoras e escritores como Joanna Russ, Samuel R. Delany, John Varley, James Tiptree Jr. [pseudônimo de Alice Sheldon], Octavia Butler, Monique Wittig e Vonda McIntyre, que são nossos/ as contadores/as de histórias, explorando o que significa – em mundos high-tech – ser corporificado.28 São os/as teóricos/ as dos ciborgues. 
> (p. 83)

> 28: King (1984). Esta é uma uma lista abreviada da ficção científica feminista que está subjacente a temas deste ensaio: Octavia Butler, _Wild Seed, Mind of My Mind, Kindred, Survivor; Suzy McKee Charnas, Motherliness_; Samuel R. Delany, a série _Neverÿon_; Anne McCaffery, _The Ship Who Sang, Dinosaur Planet; Vonda Mclntyre, Superluminal, Dreamsnake_; Joanna Russ, _Adventures of Alix, The Female Man_; James Tiptree, Jr, _Star Songs of an old Primate, Up the Walls of the World; John Varley, Titan, Wizard, Demon._
> (p. 107)

# Feministas francesas sobre a importância do corpo [feminismo materialista francês?]

> Ao explorar concepções sobre fronteiras corporais e ordem social, a antropóloga Mary Douglas (1966, 1970) ajuda-nos a ter consciência sobre **quão fundamental é a imagística corporal para a visão de mundo e, dessa forma, para a linguagem política**. **As feministas francesas, como Luce Irigaray e Monique Wittig, apesar de todas as suas diferenças, sabem como escrever o corpo, como interligar erotismo, cosmologia e política, a partir da imagística da corporificação** e, especialmente para Wittig, a partir da imagística da fragmentação e da reconstituição de corpos.
> (p. 83-4)

> 29: Os feminismos franceses representam uma importante contribuição para a heteroglossia-ciborgue. Burke (1981); Irigaray (1977, 1979); Marks e de Courtivron (1980); Signs (outono de 1981); Wittig (1973); Duchen (1986). Para uma tradução para o inglês de algumas correntes do feminismo francófono, ver _Feminis Issues: A Journal of Feminist Social and Political Theory_, 1980.
> (p. 107-8)

# O feminismo radical e a oposição orgânico-tecnológico

> **Feministas radicais estadunidenses, como Susan Griffin, Audre Lorde e Adrienne Rich, têm afetado profundamente nossas imaginações políticas, mesmo que restringindo demasiadamente, talvez, aquilo que nós pensamos como sendo uma linguagem corporal e política amigável.** Elas insistem no orgânico, opondo-o ao tecnológico. **Mas seus sistemas simbólicos, bem como as perspectivas que lhe são relacionadas (o ecofeminismo e o paganismo feminista), repleto de organicismos, só podem ser compreendidos como – para usar os termos de Sandoval – ideologias de oposição adequadas ao final do século XX.** Elas simplesmente chocam qualquer pessoa que não esteja preocupada com as máquinas e a consciência do capitalismo tardio. Assim, elas são parte do mundo do ciborgue. Mas existem também grandes vantagens para as feministas em não abraçar explicitamente as possibilidades inerentes ao colapso das distinções nítidas entre organismo e máquina, bem como as distinções similares que estruturam o eu ocidental. **É a simultaneidade dos colapsos que rompe as matrizes de dominação e abre possibilidades geométricas.** O que pode ser aprendido a partir da poluição “tecnológica” política e pessoal?
> (p. 84)

# Sobre o papel da escrita para a colonização e sua contestação contemporânea

> Contrariamente aos estereótipos orientalistas do “primitivo oral”, o alfabetismo é uma marca especial das mulheres de cor, tendo sido adquirido pelas mulheres negras estadunidenses, bem como pelos homens, por meio de uma história na qual eles e elas arriscaram a vida para aprender e para ensinar a ler e a escrever. A escrita tem um significado especial para todos os grupos colonizados. **A escrita tem sido crucial para o mito ocidental da distinção entre culturas orais e escritas, entre mentalidades primitivas e civilizadas.** Mais recentemente, essas distinções têm sido desconstruídas por aquelas teorias pós-modernas que atacam o falogocentrismo do ocidente, com sua adoração do trabalho monoteísta, fálico, legitimizado e singular – o nome único e perfeito.31 **Disputas em torno dos significados da escrita são uma forma importante da luta política contemporânea. Liberar o jogo da escrita é uma coisa extremamente séria.** A poesia e as histórias das mulheres de cor estadunidenses dizem respeito, repetidamente, à escrita, ao acesso ao poder de significar; mas desta vez o poder não deve ser nem fálico nem inocente. **A escrita-ciborgue não tem a ver com a Queda, com a fantasia de uma totalidade que, “era-uma-vez”, existia antes da linguagem, antes da escrita, antes do Homem. A escrita-ciborgue tem a ver com o poder de sobreviver, não com base em uma inocência original, mas com base na tomada de posse dos mesmos instrumentos para marcar o mundo que as marcou como outras.**
> (p. 85-6)

> Os instrumentos são, com frequência, histórias recontadas, que invertem e deslocam os dualismos hierárquicos de identidades naturalizadas. **Ao recontar as histórias de origem, as autoras-ciborgue subvertem os mitos centrais de origem da cultura ocidental.** Temos, todas, sido colonizadas por esses mitos de origem, com sua ânsia por uma plenitude que seria realizada no apocalipse. **As histórias falogocêntricas de origem mais cruciais para as ciborgues feministas estão contidas nas tecnologias – tecnologias que escrevem o mundo, como a biotecnologia e a microeletrônica – da letra, da inscrição que têm, recentemente, textualizado nossos corpos como problemas de código sobre a grade do C3I.** As histórias feministas sobre ciborgues têm a tarefa de recodificar a comunicação e a inteligência a fim de subverter o comando e o controle.
> (p. 86-7)

> A linguagem de Moraga [[ _Loving in the war years_, 1983 ]] não é “inteira”; ela é autoconscientemente partida, uma quimera feita de uma mistura de inglês e espanhol, as línguas dos conquistadores. Mas é esse monstro quimérico, sem nenhuma reivindicação em favor de uma língua original existente antes da violação, que molda as identidades eróticas, competentes, potentes, das mulheres de cor. 
> (p. 87)

> A _Sister outsider_ [[ Audre Lorde, 1984 ]] sugere a possibilidade da sobrevivência do mundo não por causa de sua inocência, mas por causa de sua habilidade de viver nas fronteiras, de escrever sem o mito fundador da inteireza original, com seu inescapável apocalipse do retorno final a uma unidade mortal que o Homem tem imaginado como sendo a Mãe inocente e todo poderosa, libertada, no Fim, de uma outra espiral de apropriação, por seu filho
> (p. 88)

# A linguagem ciborgue preza pela plurivocidade, pela diferença, pelo ruído, pela poluição [ ~Viveiros no primeiro capítulo de _Metafísicas_ ]

> A escrita é, preeminentemente, a tecnologia dos ciborgues – superfícies gravadas do final do século XX. **A política do ciborgue é a luta pela linguagem, é a luta contra a comunicação perfeita, contra o código único que traduz todo significado de forma perfeita – o dogma central do falogocentrismo. É por isso que a política do ciborgue insiste no ruído e advoga a poluição, tirando prazer das ilegítimas fusões entre animal e máquina.** São esses acoplamentos que tornam o Homem e a Mulher extremamente problemáticos, subvertendo a estrutura do desejo, essa força que se imagina como sendo a que gera a linguagem e o gênero, subvertendo, assim também, a estrutura e os modos de reprodução da identidade “ocidental”, da natureza e da cultura, do espelho e do olho, do escravo e do senhor. **“Nós” não escolhemos, originalmente, ser ciborgues. A ideia de escolha está na base, de qualquer forma, da política liberal e da epistemologia que imaginam a reprodução dos indivíduos antes das replicações mais amplas de “textos”.**
> (p. 88-9)

# Análise imanente (x olho de deus transcendente). Contra a hierarquização de opressões.

> Libertadas da necessidade de basear a política em uma posição supostamente privilegiada com relação à experiência da opressão, incorporando, nesse processo, todas as outras dominações, podemos, da perspectiva dos ciborgues, vislumbrar possibilidades extremamente potentes. **Os feminismos e os marxismos têm dependido dos imperativos epistemológicos ocidentais para construir um sujeito revolucionário, a partir da perspectiva que supõe existir uma hierarquia entre diversos tipos de opressões e/ou a partir de uma posição latente de superioridade moral, de inocência e de uma maior proximidade com a natureza.** Sem poder mais contar com nenhum sonho original relativamente a uma linguagem comum, nem com uma simbiótica natural que prometa uma proteção da separação “masculina” hostil, estamos escritas no jogo de um texto que não tem nenhuma leitura finalmente privilegiada nem qualquer história de salvação. **Isso faz com que nos reconheçamos como plenamente implicadas no mundo, libertando-nos da necessidade de enraizar a política na identidade, em partidos de vanguarda, na pureza e na maternidade.** Despida da identidade, a raça bastarda ensina sobre o poder da margem e sobre a importância de uma mãe como Malinche. As mulheres de cor transformam-na, de uma mãe diabólica, nascida do medo masculinista, em uma mãe originalmente alfabetizada que ensina a sobrevivência.
> (p. 89)

# Ciborgues da vida real x mitos originários

> Mas existe um outro caminho para ter menos coisas em jogo na autonomia masculina, um caminho que não passa pela Mulher, pelo Primitivo, pelo Zero, pela Fase do Espelho e seu imaginário. Passa pelas mulheres e por outros ciborgues no tempo-presente, ilegítimos, não nascidos da Mulher, que recusam os recursos ideológicos da vitimização, de modo a ter uma vida real. [[...]] Esses ciborgues da vida real (por exemplo, as mulheres trabalhadoras de uma aldeia do sudeste asiático, nas empresas eletrônicas japonesas e estadunidenses descritas por Aihwa Ong) estão ativamente reescrevendo os textos de seus corpos e sociedades. A sobrevivência é o que está em questão nesse jogo de leituras.
> (p. 90)

# Dualismos como essenciais para práticas de dominação

> Para recapitular: **certos dualismos têm sido persistentes nas tradições ocidentais; eles têm sido essenciais à lógica e à prática da dominação sobre as mulheres, as pessoas de cor, a natureza, os trabalhadores, os animais – em suma, a dominação de todos aqueles que foram constituídos como outros e cuja tarefa consiste em espelhar o eu [dominante].** Estes são os mais importantes desses problemáticos dualismos: eu/outro, mente/ corpo, cultura/natureza, macho/fêmea, civilizado/primitivo, realidade/aparência, todo/parte, agente/instrumento, o que faz/o que é feito, ativo/passivo, certo/errado, verdade/ilusão, total/parcial, Deus/homem. O eu é o Um que não é dominado, que sabe isso por meio do trabalho do outro; o outro é o um que carrega o futuro, que sabe isso por meio da experiência da dominação, a qual desmente a autonomia do eu. **Ser o Um é ser autônomo, ser poderoso, ser Deus; mas ser o Um é ser uma ilusão e, assim, estar envolvido numa dialética de apocalipse com o outro. Por outro lado, ser o outro é ser múltiplo, sem fronteira clara, borrado, insubstancial. Um é muito pouco, mas dois [o outro] é demasiado.**
> (p. 91)

# Dissolução de dualismos na cultura _high-tech_

> A cultura _high-tech_ contesta – de forma intrigante – esses dualismos. Não está claro quem faz e quem é feito na relação entre o humano e a máquina. Não está claro o que é mente e o que é corpo em máquinas que funcionam de acordo com práticas de codificação. **Na medida em que nos conhecemos tanto no discurso formal (por exemplo, na biologia) quanto na prática cotidiana (por exemplo, na economia doméstica do circuito integrado), descobrimo-nos como sendo ciborgues, híbridos, mosaicos, quimeras.** Os organismos biológicos tornaram-se sistemas bióticos – dispositivos de comunicação como qualquer outro. **Não existe, em nosso conhecimento formal, nenhuma separação fundamental, ontológica, entre máquina e organismo, entre técnico e orgânico.** A replicante Rachel no filme Blade Runner, de Ridley Scott, destaca-se como a imagem do medo, do amor e da confusão da cultura-ciborgue.
> (p. 91)

# Ficção científica ciborgue

> Katie King observa como o prazer de **ler essas ficções não é, em geral, baseado na identificação.** As estudantes que encontraram Joanna Russ pela primeira vez, estudantes que aprenderam a ler escritores e escritoras modernistas como James Joyce ou Virginia Woolf sem problemas, não sabem o que fazer com _Adventures of Alyx_ ou _The female man_, nos quais **os personagens rejeitam a busca do leitor ou da leitora por uma inteireza inocente, ao mesmo tempo que admitem o desejo por buscas heroicas, por um erotismo exuberante e por uma política séria.**
> (p. 93)

# Por uma imagem positiva do ciborgue

> Essas são várias das consequências de se levar a sério a imagem dos ciborgues como sendo algo mais do que apenas nossos inimigos. **Nossos corpos são nossos eus; os corpos são mapas de poder e identidade. Os ciborgues não constituem exceção a isso.** O corpo do ciborgue não é inocente; ele não nasceu num Paraíso; ele não busca uma identidade unitária, não produzindo, assim, dualismos antagônicos sem fim (ou até que o mundo tenha fim). Ele assume a ironia como natural. Um é muito pouco, dois é apenas uma possibilidade. O intenso prazer na habilidade – na habilidade da máquina – deixa de ser um pecado para constituir um aspecto do processo de corporificação. **A máquina não é uma coisa a ser animada, idolatrada e dominada. A máquina coincide conosco, com nossos processos; ela é um aspecto de nossa corporificação. Podemos ser responsáveis pelas máquinas; elas não nos dominam ou nos ameaçam. Nós somos responsáveis pelas fronteiras; nós somos essas fronteiras.** Até agora (“era uma vez”), a corporificação feminina parecia ser dada, orgânica, necessária; a corporificação feminina parecia significar habilidades relacionadas à maternidade e às suas extensões metafóricas. Podíamos extrair intenso prazer das máquinas apenas ao custo de estarmos fora de lugar e mesmo assim com a desculpa de que se tratava, afinal, de uma atividade orgânica, apropriada às mulheres. **Ciborgues podem expressar de forma mais séria o aspecto – algumas vezes, parcial, fluido – do sexo e da corporificação sexual. O gênero pode não ser, afinal de contas, a identidade global, embora tenha uma intensa profundidade e amplitude históricas. [[?]]**
> (p. 96-7)

> O gênero ciborguiano é uma possibilidade local que executa uma vingança global. A raça, o gênero e o capital exigem uma teoria ciborguiana do todo e das partes. **Não existe nenhum impulso nos ciborgues para a produção de uma teoria total; o que existe é uma experiência íntima sobre fronteiras – sobre sua construção e desconstrução.** Existe um sistema de mito, esperando tornar-se uma linguagem política que se possa **constituir na base de uma forma de ver a ciência e a tecnologia e de contestar a informática da dominação** – a fim de poder agir de forma potente.
> (p. 98)

# Ciborgue ~regeneraqção (e não renascimento). Por um mundo sem gênero.

> Uma última imagem: os organismos e a política organicista, holística, dependem das metáforas do renascimento e, invariavelmente, arregimentam os recursos do sexo reprodutivo. **Sugiro que os ciborgues têm mais a ver com regeneração, desconfiando da matriz reprodutiva e de grande parte dos processos de nascimento.** Para as salamandras, a regeneração após uma lesão, tal como a perda de um membro, envolve um crescimento renovado da estrutura e uma restauração da função, com uma constante possibilidade de produção de elementos gêmeos ou outras produções topográficas estranhas no local da lesão. O membro renovado pode ser monstruoso, duplicado, potente. **Fomos todas lesadas, profundamente. Precisamos de regeneração, não de renascimento, e as possibilidades para nossa reconstituição incluem o sonho utópico da esperança de um mundo monstruoso, sem gênero.**
> (p. 98)

# Ciborgue: 1) crítica a noção de teoria universal, totalizante 2) recusar o anticientificismo e a tecnofobia. Sair dos dualismos (heteroglossia e não linguagem comum) [~monismo da diferença x informação como substrato homogêneo]

> A imagem do ciborgue pode ajudar a expressar dois argumentos cruciais deste ensaio. **Em primeiro lugar, a produção de uma teoria universal, totalizante, é um grande equívoco, que deixa de apreender – provavelmente sempre, mas certamente agora – a maior parte da realidade. Em segundo lugar, assumir a responsabilidade pelas relações sociais da ciência e da tecnologia significa recusar uma metafísica anticiência, uma demonologia da tecnologia e, assim, abraçar a habilidosa tarefa de reconstruir as fronteiras da vida cotidiana, em conexão parcial com os outros, em comunicação com todas as nossas partes.** Não se trata apenas da ideia de que a ciência e a tecnologia são possíveis meios de grande satisfação humana, bem como uma matriz de complexas dominações. **A imagem do ciborgue pode sugerir uma forma de saída do labirinto dos dualismos por meio dos quais temos explicado nossos corpos e nossos instrumentos para nós mesmas. Trata-se do sonho não de uma linguagem comum, mas de uma poderosa e herética heteroglossia.** Trata-se da imaginação de uma feminista falando em línguas36 [glossolalia] para incutir medo nos circuitos dos supersalvadores da direita. Significa tanto construir quanto destruir máquinas, identidades, categorias, relações, narrativas espaciais. Embora estejam envolvidas, ambas, numa dança em espiral, prefiro ser uma ciborgue a uma deusa.
> (p. 98-9)

# Referência que pode ser interessante

> WINNER, Langdon. Autonomous technology: technics out of control as a theme
> in political thought. Cambridge, MA: MIT Press, 1977.
