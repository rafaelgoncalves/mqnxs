---
title: Colonialismo digital
tags:
    - Michael Kwet
    - colonialismo digital
    - colonialismo
    - big data
    - tecnologias da informação
    - software livre
    - hardware aberto
    - neutralidade de rede
    - internet
    - áfrica do sul
    - fichamento

date: 2023-10-08
category: fichamento
path: "/colonialismo-digital"
featuredImage: "svilar.jpg"
srcInfo: <a href="https://roarmag.org/essays/digital-colonialism-the-evolution-of-american-empire/">Arte por Zoran Svilar</a>
published: true
---

Fichamento do artigo _Digital colonialism_[^1] [^2] do sociólogo Michael Kwet.

[^1]: KWET, M. Digital colonialism: US empire and the new imperialism in the Global South. **Race & Class**, v. 60, n. 4, p. 3–26, abr. 2019. 

[^2]: A numeração e o conteúdo diferem ligeiramente da versão publicada (não consegui encontrar a versão exata que utilizei para o fichamento).

# Abstract

> This paper **proposes a theoretical and conceptual framework explaining how the United States is reinventing colonialism in the Global South through the domination of digital technology.** **Drawing on South Africa as a case example, it argues that US multinationals exercise imperial control at the architecture level of the digital ecosystem: software, hardware, and network connectivity.** This gives rise to five related forms of domination. First, the monopoly power of multinational corporations is used for resource extraction through rent and surveillance, constituting a **new form of _economic domination_.** Second, by controlling the digital ecosystem, Big Tech corporations control computermediated experiences, giving them **direct power over political, economic, and cultural domains of life – a new form of _imperial control_.** Third, the centerpiece of surveillance capitalism, Big Data, violates the sanctity of privacy and concentrates economic power into the hands of US corporations – **a system of _global surveillance capitalism_.** Fourth, as a feature of surveillance capitalism, Global North intelligence agencies partner with their own corporations to conduct **mass and targeted surveillance in the Global South. This intensifies _imperial state surveillance_.** And fifth, US elites have persuaded most people that society must proceed according to its own ruling class conceptions of the digital world, **setting the foundation for _tech hegemony_.**


# Colonialismo digital e imperialismo sobre o sul global

> The secretive initiative marks a critical moment in history: many countries in the Global South are on the verge of extending computer devices and Internet connectivity to the poor majority. How will the Global South be impacted by the spread of digital technology? More importantly, should the Global South adopt the products and models of US tech giants, or should they think differently and pursue other options? Can the countries of the Global South shape their own digital destiny? **An insidious new phenomenon, digital colonialism, casts a shadow on the Global South. This structural form of domination is exercised through the centralised ownership and control of the three core pillars of the digital ecosystem: software, hardware, and network connectivity, which vests the United States with immense political, economic, and social power.** **As such, GAFAM (Google/Alphabet, Amazon, Facebook, Apple, and Microsoft) and other corporate giants, as well as state intelligence agencies like the National Security Agency (NSA), are the new imperialists in the international community. Assimilation into the tech products, models, and ideologies of foreign powers – led by the United States – constitutes a twenty-first century form of colonisation.**
> (p. 2)

# Situação atual na África do Sul pós-apartheid

> More than two decades into formal democracy, South Africa is struggling to overcome its apartheid past. Economic inequality has increased, and the country ranks among the most unequal in the world. Racial disparities are high with respect to income, wealth, employment, and education, while residential segregation has persisted. The ANC has delivered some modest services to the poor – including millions of cheap RDP (Reconstruction and Development Programme) houses, access to electricity, and small social welfare grants – yet poverty remains rampant. About 55 per cent of the population falls under the upper limit poverty line of less than $3 per day.4 Sixty-three per cent of Africans/blacks fall below the poverty line, compared to just under 1 per cent of whites.5 Given the outcomes of the neoliberal development path favoured by the ANC and the leading opposition party, the Democratic Alliance, some scholars are beginning to label South Africa a ‘neo-apartheid’ rather than a ‘post-apartheid’ society.
> (p. 3)

> Despite recent attention to new technologies, members of government, NGOs, business classes and intellectuals have provided little critique of what paths are available at the architectural level. Instead, **they have been aiming to ‘catch up’ with the North by seeking to place mainstream digital tech into every corner of society, while training South Africans in digital literacy for assimilation into US products.** The published record on digital tech in South Africa fails to critique Big Tech multinationals (e.g. GAFAM, Uber and Netflix) and their models for the digital society: Big Data, artificial intelligence, and machine learning; centralised cloud services; the gig economy; the spread of CCTV surveillance; as well as industry-specific trends, such as predictive analytics in private security, policing, education, finance, and employment.
> (p. 4)

# Tentativa de responder questões pouco olhadas (em relação ao uso de tecnologias hegemônicas)

> This article attempts to answer the questions entirely absent from public discourse; such as, **‘are cloud centres built by Amazon, Microsoft, and Google good for the country?’ or ‘which technologies best promote privacy rights, transparency, collaboration, and local development?’.**
> (p. 4 )

# Colonização de tecnologias digitais por corporações de Big Tech

> Today, a new form of corporate colonisation is taking place. Instead of the conquest of land, **Big Tech corporations are colonising digital technology.** The following functions are all dominated by a handful of US multinationals: **search engines (Google); web browsers (Google Chrome); smartphone and tablet operating systems (Google Android, Apple iOS); desktop and laptop operating systems (Microsoft Windows); office software (Microsoft Office, Google G Suite); cloud infrastructure and services (Amazon, Microsoft, Google, IBM); social networking platforms (Facebook, Twitter); transportation (Uber, Lyft); business networking (Microsoft LinkedIn); streaming video (Google YouTube, Netflix, Hulu); and online advertising (Google, Facebook)** – among others. **GAFAM now comprise the five wealthiest corporations in the world, with a combined market cap exceeding $3 trillion.** If South Africans integrate Big Tech products into their society, the United States will obtain enormous power over their economy and create technological dependencies that will lead to perpetual resource extraction.
> (p. 5 )

# Uber na África do Sul

> Uber has had devastating effects in Africa and beyond.12 The company takes around 25 per cent commission for each trip, in addition to hidden costs,13 leading to an outflow of revenue from the local economy to foreign coffers. Moreover, it is able to undercut local markets by offering artificially low prices: Uber can operate at a loss – to the tune of billions – thanks to funding from Wall Street and other wealthy investors.14 With the backing of corporate finance, it leverages predatory subsidies, network effects, Big Data analytics, and the deregulatory effects of its position as an ‘intermediary’ to stamp out competition and colonise the market. Within just two years, Uber sported a net worth of R1.65 billion ($125 million) inside South Africa.15
> (p. 6 )

# Google e Facebook na África do Sul

> In November 2017, Financial Mail’s Anton Harber wrote a feature story deeming Google and Facebook ‘the biggest threat to South African news media’.17 Google takes 70 per cent of local online advertising, while social media – led by Facebook – takes another 12 per cent. The major South African media groups are left with just 8 per cent of the pie. The Google and Facebook ‘nemesis’ is an expanding duopoly: the two take 77 per cent of online advertising spend in the US and captured virtually all the ad growth in 2016.18 If this continues, Harber exclaims, ‘the big two could have a devastating effect on the media’s role in defining democracy’.19
> (p. 7 )

# Implantação de infraestrutura do norte global no sul global visando dominação econômica e cultural (via tecnologias digitais)

> **Under digital colonialism, foreign powers, led by the US, are planting infrastructure in the Global South engineered for their own needs, enabling economic and cultural domination while imposing privatised forms of governance.** To accomplish this task, major corporations design digital technology to ensure their own dominance over critical functions in the tech ecosystem. **This allows them to accumulate profits from revenues derived from rent (in the form of intellectual property or access to infrastructure) and surveillance (in the form of Big Data). It also empowers them to exercise control over the flow of information (such as the distribution of news and streaming services), social activities (like social networking and cultural exchange), and a plethora of other political, social, economic and military functions mediated by their technologies.**
> (p. 8 )

# Código ~ dominação. Arquitetura de código ~ arquitetura física.

> The control of code is foundational to digital domination. In Code: and Other Laws of Cyberspace, **Lawrence Lessig famously argued that computer code shapes the rules, norms and behaviours of computer-mediated experiences in ways similar to architecture in physical space** (e.g. imperial railways designed for colonisation).21 **‘Code is law’ in the sense that it has the power to usurp legal, institutional and social norms impacting the political, economic and cultural domains of society.** This critical insight has been applied in fields like copyright, free speech regulation, Internet governance, blockchain, privacy, and even torts. **What has been missed, however, is how US dominance of code – and other forms of digital architecture – usurps other countries’ sovereignty.**
> (p. 9 )

# Software, hardware e conectividade de rede (?) como três pilares do poder digital

> Digital forms of power are linked through the three core pillars of the digital ecosystem: software, hardware and network connectivity.22 **(Software is the set of instructions that define and determine what your computer can do. Hardware is the physical equipment used for computer experiences. The network is the set of protocols and standards computers use to talk to each other, and the connections they make.)**
> (p. 9 )

# Software como lógica codificada (axiomática?) que permite experiências de usuário particulares

> **Software functions as the coded logic that constrains and enables particular user experiences.** For example, software determines rules and policies such as whether or not users can post a message anonymously at a website, or whether or not users can make a copy of a copyright-restricted file like an e-book. **The rules that a programmer codes into the software largely determine technological freedoms and shape users’ experiences using their devices.** Thus, software exerts a powerful influence on the behaviour, policies and freedoms of people using digital 
> (p. 9 )

> **Control over software is a source of digital domination primarily exercised through software licences and hardware ownership.** Free Software licences allow people to use, study, modify and share software as they see fit.23 **By contrast, non-free software licences grant a software designer control over users by precluding the ability to exercise those freedoms. With proprietary software, the human-readable source code is closed off to the public, and owners usually restrict the ability to use the software without paying.** In the case of Microsoft Windows, for example, the public must pay for the programme in order to use it, they cannot read the source code to understand how it works, they cannot change its behaviour by changing the code, and they cannot share a copy with others. Thus with proprietary licensing, Microsoft maintains absolute control over how the software works. The same goes for other proprietary apps, like Google Play or Adobe Photoshop.24 By design, non-free software provides the owner power over the user experience. It is authoritarian software.
> (p. 10 )

# Controle sobre hardware (SaaS/Cloud, propriedade centralisada, hardware concretizando restrições de software)

> **Control over hardware is a second source of digital domination. This can take at least three forms: software run on third-party servers, centralised ownership of hardware, or hardware designed to prevent users from changing the software.** In the first scenario, software is executed on someone else’s computer. As a result, users are dispossessed of their ability to control it. This is typically accomplished through Software as a Service (SaaS) in the cloud. For example, when you visit the Facebook website, the interface you are provided executes on third-party hardware (i.e. on Facebook’s cloud servers). Because users cannot change the code running on Facebook’s servers, they cannot get rid of the ‘like’ button or change the Facebook experience. ‘There is no cloud’, the saying goes, ‘just someone else’s computer’. **Corporations and other third parties design cloud services for remote control over the user experience.** This gives them immense power over individuals, groups and society.25 In the second scenario, people become dispossessed of hardware ownership itself. **With the rise of cloud computing, it is possible that hardware manufacturers will soon only offer low-powered, low-memory devices (similar to the terminals of the 1960s and 1970s) and computer processing and data storage will be primarily conducted in centralised clouds.** With end-users dispossessed of processing power and storage, software and data would be under the absolute control of the owners and operators of clouds.26 In the third scenario, **hardware is manufactured with locks that prevent users from changing the software on the devices. By locking down devices to a pre-determined set of software choices, the hardware manufacturer determines which software is allowed to run when you turn on your device.27** Thus, hardware restrictions can prevent the public from controlling their devices, granting device manufacturers power over users.
> (p. 10 )

# Controle sobre a conctividade de rede

> Control over network connectivity is a third source of digital domination. **Net neutrality regulation proposes that Internet traffic should be ‘neutral’ so that Internet Service Providers (ISPs) treat content flowing through their cables, cellular towers and satellites equally.** According to this philosophy, those who own the pipes are ‘common carriers’ and should almost never be allowed to manipulate the data that flows through them.28 This constrains the ability of wealthy media providers to pay for faster content delivery speeds than less wealthy providers (such as grassroots organisations, small businesses, and common people). More importantly, by treating traffic equally, net neutrality prevents network discrimination against various forms of traffic critical to civil rights and liberties. For example, the Tor browser facilitates anonymous Internet communications, but the use of the Tor network can be detected by ISPs and throttled (i.e. slowed to a crawl).29 Net neutrality prevents this form of discrimination and protects the end user’s freedom to utilise the Internet as they wish, without third-party favouritism, blocking, or throttling. Let us consider some concrete examples as to how software, hardware, and networks constitute sources of power and control related to social justice in the Global South.
> (p. 11 )

# DRM como exemplo de controle sobre o software

> One way to stop file sharing is to control software. The industry built Digital Rights Management (DRM) software, for example, to prevent copyright-restricted publications from playing on a user’s computer unless the user pays to access it first. This works well with proprietary software because users cannot remove the DRM. (However, if the DRM software is Free Software – which allows people the freedom to use, study, modify and share the software – people can remove the DRM code that locks the content.) Thus, industry is bolstered by proprietary software as a means to enforce copyright.
> (p. 12 )

# Streaming e nuvem como exemplo de controle sobre o hardware

> A second way to prevent sharing is to take control of the hardware. If, for example, people stop running software on their own devices, and instead run their computer experiences through centralised cloud servers, then cloud providers can determine their ‘access’ to copyrighted data. In this scenario, users cannot copy and trade media over the Internet because the data ‘streams’ to their device from a content owner’s platform (e.g. Netflix or Spotify) which provides media content through its servers. Thus, the widespread distribution of storage capacity and broadband Internet threatens the copyright monopoly.30
> (p. 13 )

# Controle de rede por ISP como exemplo de controle sobre conectividade de rede

> A third way to prevent sharing media is to control the network. People may own and control their software and hardware, but if they can be spied on by an ISP or government, then they can be fined or arrested for copyright infringement, or have their Internet connection throttled or terminated. People might use privacy protection technologies to conceal their content sharing – such as the Tor network or Virtual Private Networks (VPNs) – but this can be thwarted by ISPs throttling Tor or VPNs. In this scenario, control of the network (ISP discrimination) is used to make anonymous content sharing impractical. Thus, public control of the network threatens copyright enforcement.
> (p. 13 )

# Aspecto colonial: acumulação por multinacionais estadunidenses

> To bring this back to colonialism, **US multinationals have designed digital architecture which, in one way or another, allows them to accumulate vast fortunes based on rent or data extraction. In the case of copyright, control over software, hardware, or the Internet is used to protect the copyright monopoly in the name of intellectual property rights.** Given that the marginal cost of producing digital works is near-zero, prominent intellectuals and activists have challenged copyright paywalls in the interest of socioeconomic justice and out of concern that draconian technologies are needed to enforce digital forms of copyright.31 Free access to digital publications for all people on planet earth, irrespective of their wealth, could improve education, culture, equality, democracy, and innovation. **Western technology has been engineered to block free sharing, which impoverishes poor people’s ability to obtain knowledge and culture, and reduces communication between rich and poor.**
> (p. 13 )

# Colonialismo digital ~ propriedade intelectual e inovação (financiada pelo Estado)

> Intellectual property rights date back centuries, but **they became a fixation of the West in the late twentieth century as western corporations came to dominate intellectual property.** Despite strong IP protections, much of the development in the domain of technological innovation has been driven by the public sector. **The Internet, GPS, multi-touch screens, HTML, the Siri virtual personal assistant, LCD displays, microprocessors, RAM memory, hard disk drives, Google’s search algorithm, and lithium-ion batteries were all developed by public institutions or with a heavy dose of public funding.32** Additionally, 80 per cent of basic scientific research and development in the US is funded by government and non-profit sectors.33 Thus the foundational knowledge and technologies of the digital world have been largely driven by state-led research and development.
> (p. 14 )

# Propriedade intelectual como fenômeno recente (imposto pelos EUA)

> The expansion of copyright protections is also a recent phenomenon. The United States did not observe foreign copyrights until the end of the nineteenth century. For about a century prior, it printed copyrighted works produced by Europeans without paying licensing fees. **As its own domestic industry developed, the US changed its tune in order to protect its local content industry.** Initially, copyrights lasted just fourteen years, with a right of renewal for another fourteen years. **By the end of the twentieth century, the US muscled the international community into accepting lengthy copyright terms (life of the author plus several decades). While the US (and other countries) built up its knowledge economies without respecting foreign copyrights, today it seeks to ‘kick away the ladder’ and impose extensive copyright restrictions on the rest of the world.**
> (p. 14 )

> Countries in the **Global South have accepted these terms in large part because of the pressures exerted through international trade agreements and organisations. In particular, the Agreement on Trade-Related Aspects of Intellectual Property Rights (TRIPS), administered by the World Trade Organization (WTO), requires member countries to provide a number of intellectual property protections, including copyright and patents.** Developing countries have ratified TRIPS because it is a necessary prerequisite to membership in the WTO.34 In 2004, developing countries, including South Africa, proposed a development agenda at the World Intellectual Property Organization (WIPO) that would bridge the ‘knowledge gap’ and ‘digital divide’ through ‘public interest flexibilities’ with respect to intellectual property.35 While recommendations made in 2007 included special considerations for developing countries (e.g. technology transfer and increased research sharing), they are far from demanding the drastic weakening of intellectual property rights.36 If copyrights and patents are to be reduced or phased out, developing countries and developed world allies will have to push for a shift to government procurement of knowledge production.37
> (p. 15 )

# O caso Facebook Free Basics na África do Sul

> Facebook’s Free Basics service offers another case of how Big Tech corporations expand empire in the Global South. Free Basics offers a stripped-down version of free Internet services to people with little or no disposable income. **Facebook decides which content and websites the poor can access – while conveniently providing Facebook itself within the app. Free Basics is zero-rated by ISPs, meaning that data transfers inside the app are paid for by ISPs instead of their customers. The ISPs hope that the limited Internet experience will lead to paying customers who, having tasted a free sample, will purchase data for the full experience. Free Basics not only has Facebook playing Internet gatekeeper of the poor, it also violates net neutrality laws: zero-rated offerings place content providers on an unequal footing.** Several countries have terminated Free Basics, in part due to popular backlash.38 However, Internet.org has put over 100 million users from over sixty countries, including South Africa, into the Facebook platform, which channels them towards the Facebook ecosystem.
> (p. 15 )

# Google e facebook como gatekeepers informacionais

> **Integrating platforms like Facebook outside the US does more than drain local advertising revenue: it undermines various forms of local governance.** Seventy-five per cent of web publisher’s traffic now comes from Google (46 per cent) and Facebook (29 per cent).39 **Centralisation of services into their hands provides them with centralised control over communications – by way of code. These two firms filter search results and news feeds with proprietary black box algorithms, granting them enormous power to shape who sees what news.** Leftist outlets have published data suggesting that Google censors socialist views, while Facebook has been found to favour mainstream liberal media.40
> (p. 16 )

> Platforms also regulate freedom of speech and association.41 If an online social network detects certain keywords and forms of speech, it can censor it, or ban the user. **Moreover, it can prohibit the right to associate with others in the pursuit of social, political, economic, cultural and religious ends.** This has been carried out against Palestinians (e.g. with the removal of the page for the political party, Fatah), as well as the far Right.42
> (p. 16 )

# Capitalismo de vigilância como fenômeno histórico (e não recente)

> From a historical perspective, surveillance capitalism is nothing new. During the era of slavery, racial phenotypes were used by Europeans to identify fugitive slaves, while various surveillance tactics were used to police black bodies. In South Africa, colonists instituted pass laws and branded the skins of blacks and livestock to segregate the people and control black labour.
> (p. 17 )

> In the digital era, surveillance capitalism has been variously interpreted. In the mid-2000s, criticisms of Internet surveillance began to mount and, in 2014, the term ‘surveillance capitalism’ was coined by several prominent scholars in a special summer issue of Monthly Review, in which the authors focused on state-corporate surveillance, commercial exploitation, and Internet governance. Other scholars have devoted attention to issues like data monetisation and algorithmic discrimination.
> (p. 17 )

> Big Data is the central component of surveillance capitalism. **Corporations and states are collecting, storing and processing enormous centralised databases of information about the world’s netizens. This enables them to infer traits about people (such as their sexuality, religion, political affiliations and behavioural tendencies) that individuals do not disclose themselves.** The data is then used to manipulate individuals, groups and organisations for the interests of corporate profits and state power.
> (p. 17 )

> The Big Data society is a project for total surveillance of the human species. **Harnessing advanced statistics and artificial intelligence to make sense of enormous troves of data, corporations and governments seek God-like omniscience to manage the population.** Much of the data collection is made possible by centralisation in the cloud. The presence of Big Tech multinationals in the Global South extends the reach of surveillance capitalism to its inhabitants – with the US empire at the centre.
> (p. 18 )

# Inteligência no Big Data como extração de dados humanos via vigilância

> Because machines do not ‘think’, Big Data must derive its predictive accuracy from the richness of data that can be collected about individuals and groups. Given that massive amounts of data are needed, it generally requires mass surveillance.
> (p. 18 )

# Aspecto centralista/monopolista da economia de dados

> Additionally, those who have access to the most valuable types of data, coupled with vast quantities of it, have a supreme advantage over competitors. Facebook, for example, has access to over two billion people’s sensitive information – what they ‘like’, who they are friends with, who they talk to, where they travel physically, and so on. **Google dominates search engines, as well as data from its ad services and smartphone activity (via Google Android).** Amazon has unique and valuable commercial data, including the habits of their customers while they are shopping at Amazon.com (the market funnel), their entire purchase histories, plus whatever other data they capture or acquire. Few companies can amass these kinds of data sets.
> (p. 18 )

# Desigualdade competitiva no Sul Global

> It is nearly impossible for Global South firms to compete with these established giants, for a number of reasons. **First, network effects create considerable barriers to entry for competition.** With network effects, the more users in a network, the more valuable that network becomes. Online platforms tend to concentrate user bases because the more users they have, the better the network is. Just as people do not want to have fifty different telephones on fifty separate networks, they do not want to have fifty social media accounts. They also do not want to have many separate e-hailing services, search engines, e-commerce platforms, and so on. Second, economies of scale pose serious barriers to entry. **It is very expensive to run centralised social networks because cloud infrastructure is costly, quality products require teams of skilled programmers, data must be curated effectively, and the service must be monetised to cover those costs. Moreover, competitors include multi-billion dollar corporations, which already dominate the market, enjoy the benefit of network effects, have accumulated brand equity and trade secrets, and have the power to acquire smaller companies.**
> (p. 18 )

> **This leads to a dynamic where the largest sets of valuable data – such as social data (Facebook, Twitter), e-commerce (Amazon), and search (Google) – are dominated by a handful of ‘winners’ (multinationals).** Even corporations and countries within the Global North are beginning to express concerns about the data monopolisation tied to Big Data and AI.43 **However, simple reforms will not fix the problem, because the structural design of the ecosystem favours concentration. There is little reason to believe the Global South will produce viable competitors.**
> (p. 19 )

# Big Data ~ vigilância

> US.44 Under this arrangement, the overly capacious term ‘Big Data’ has been used to gloss over surveillance activity and power dynamics. **Applied to humans, Big Data is little more than a euphemism for surveillance.**
> (p. 19 )

# Capitalismo de vigilância ~ ferrovias coloniais

> **Like the railroads of empire, surveillance capitalists extract data out of the Global South, process it in the metropolitan centre, and spit back information services to colonial subjects, who cannot compete.** To make matters worse, with such large, pristine databases in the hands of the private sector, intelligence agencies, led by the United States, piggyback off US corporations for their own mass surveillance programmes. **As whistleblowers have revealed, most (if not all) of the large US tech giants are partnered to the National Security Agency.45 Global North domination of technical architecture thus enables another element of digital colonialism – state surveillance – rendering it even more problematic.**
> (p. 20 )

# EUA e apartheid na África do Sul

> American intelligence also targeted anti-apartheid activists. During the post-second world war period, the CIA supported white liberal anti-communist student programmes in the South African university system, and they likely helped arrest Nelson Mandela in 1962.50 In July 2018, thousands of declassified documents revealed FBI surveillance of Nelson Mandela and extensive investigations into the anti-apartheid movement.
> (p. 21 )

# PRISM e UPSTREAM como métodos de vigilância contemporâneos dos EUA

> US contributions to surveillance inside South Africa receded with the transition from apartheid to democracy. **With the spread of digital technology, however, its role has re-emerged. During the 2000s, a handful of whistleblowers revealed the existence of global mass and targeted surveillance programmes carried out by the US intelligence community.** A cache of National Security Agency documents leaked by Edward Snowden details many of these. The NSA utilises two primary methods for data collection: **partnerships with corporations (via the PRISM programme) and the tapping of the Internet backbone (via the UPSTREAM programme).** The extent of the NSA’s data collection can be estimated by the size of its storage facilities, such as its $2 billion, 25,000 square-foot facility in Bluffdale, Utah. According to NSA whistleblower William Binney, the facilities collect trillions of phone calls and emails, in addition to sources like banking and social networking.51
> (p. 21 )

> **The deployment of Big Tech products in the Global South extends the eyes and ears of foreign intelligence.** The US stranglehold over tech infrastructure, combined with a vast pool of resources, provides it with leverage over other countries. When countries like South Africa want information about a person of interest, they must apply through the Mutual Legal Assistance Treaty to access private information from social networking platforms like Twitter or Facebook. US spy agencies, by contrast, can demand access in the name of national security.
> (p. 22 )

# Colonialismo como ato de violência física e ideológica

> Colonialism was not just a physical act of aggression, it was an ideology formed to justify conquest and pacify resistance. In South Africa, Afrikaners appealed to select passages in the Bible to cast themselves as God’s chosen people for settling occupied land. During the nineteenth century, Europeans formulated the theory of biological race in service of capitalist exploitation. Britain’s Francis Galton played a key role: his theories of race were developed over a two-year trip to Southern Africa (extending from the Cape up to present-day Namibia), where he developed extreme disdain for Africans. Soon thereafter, **Galton coined the term ‘eugenics’ and improved the fingerprinting techniques introduced to South African police forces in 1900 by Sir Edward Henry. Galton went on to revolutionise the field of statistics, inventing the concepts of statistical correlation and regression to the mean, which were marshalled in service of social Darwinist ideologies of racial intelligence enthusiastically accepted by the British intellectual classes.53**
> (p. 23 )

> In the twenty-first century, Big Tech corporations have fashioned a new ‘Manifest Destiny’ for the digital age. **Western doctrines glorify Big Data, centralised clouds, proprietary systems, smart cities littered with surveillance, automation, predictive analytics, and similar inventions. Commentators may acknowledge potential deficiencies – the loss of privacy, job losses to machines, or algorithmic discrimination – but consider the core technologies an inevitable part of technological ‘progress’.** In South Africa, this narrative is delivered via World Economic Forum founder Klaus Schwab’s theory of the so-called55 ‘Fourth Industrial Revolution’ (4IR), which privileges the private sector and promotes the trending instruments of domination characterising digital capitalism. South African politicians, journalists and intellectuals (featured in the media) have internalised his doctrine. Scarcely an article or radio show discussing technology fails to mention the 4IR.
> (p. 24 )

# Escolhas tecnológicas na educação

> The importance of technology choices for schools cannot be overstated: **the specific technologies deployed will forge path dependencies by shaping the habits, preferences and knowledge base of the first tech generation from childhood.** **Education offers the ultimate breeding ground for Big Tech imperialism; product placement in schools can be used to capture emerging markets and tighten the stranglehold of Big Tech products, brands, models and ideology in the Global South.** The youth will be more likely to consume the products they receive in school as adults, while the future generation of tech developers will likely become developers of products for the ecosystems they grow up using: Microsoft, Google, Apple – or Free Software like GNU/Linux.
> (p. 24 )

# Resistência contra o colonialismo digital na África do Sul, papel do movimento software livre

> **Big Tech colonisation in South Africa can be countered with publicly owned and controlled technology built for freedom by design at the architectural level.** The Free Software Movement (FSM) has been at the forefront of this political struggle. **The FSM developed within the centre of empire in response to enclosure of the software commons, first through proprietary software and now through Internet centralisation. It has concentrated on developing forms of technology that grant control to individuals and communities for the purpose of individual and collective freedom.** During the 2000s, Free Software (also called Free and Open Source Software (FOSS)) was endorsed for public sector implementation across the Global South, including in South Africa. **The development and dispersion of the Free Software philosophy across the world resembles the development of socialism within Europe as a reaction to land enclosure and industrial exploitation, and its subsequent spread across liberation movements throughout the world.**
> (p. 25 )

> Impressed by the anti-possessive design of Free Software, Archbishop Desmond Tutu has endorsed the FSM. Introducing Stallman at the University of Western Cape in 2007, he stated: There are those who will take the fruits of the human mind and lock them up, dishing them out to us in meted amounts for a fee that locks most of our people out. And there are laws that are reserved for business reasons and changed to rob society of its own rights … Free Software and Open Source, Free and Open Resources for Education, new ways to create and share cultural artifacts such as music, writing, and art – all of these are changing the world for the better.58
> (p. 26 )

# Limites do software livre (hardware e protocolo). Cultura livre (software livre, hardware aberto e neutralidade na Internet).

> Free Software alone, however, cannot provide the freedom to control technology. In Die Gedanken Sind Frie, Columbia law professor Eben Moglen developed a framework that provides a more complete account of the digital ecosystem. According to Moglen, the three core pillars of the digital ecosystem must be arranged to prevent authoritarian forms of digital technology. **Software must be Free Software so that the public has the capacity to control its devices; hardware must be Free Hardware without digital locks and widely distributed in the hands of the people;59 and the Internet must be neutral and provide bandwidth for all people on equal terms.**
> (p. 26 )

> Moglen adds that the trio of **Free Software, Free Hardware and Free Spectrum (network connectivity) form the foundation for Free Culture, whereby anyone with a device and the Internet can freely access, produce and share published works.60** Taken together, the core pillars of the digital ecosystem are essential components which, by their very freedom and openness, empower the public – rather than states, corporations, or any other third parties – to exercise direct and collective control over the devices and ecosystem shaping their lives. In 2016, Edward Snowden echoed this sentiment, stating, ‘we’re very rapidly approaching a point in human history where we will need to seize the means of our communication’.61 Digital rights advocates argue that new technology is needed to decentralise the Internet, secure privacy, and subvert centralisation in the cloud.
> (p. 27 )

# Problemas da centralização da internet e alternativas livres descentralizadas

> **The present client-server network model has billions of users as clients at the edge requesting information from a small number of (predominantly corporate) servers in the centre who process, store and deliver information back to the clients. This architecture is problematic as it confers enormous power on corporations and states, which own and operate the clouds. In a global context, this model facilitates colonial dispossession.** As an antidote to this dilemma, the Free Software Movement is building decentralised networking alternatives. In February 2010, Eben Moglen and his colleagues launched the **FreedomBox project in reaction to cloud centralisation.** The project is designed to run a secure, personal server that protects privacy and provides infrastructure for communities to network their online activities without the need for centralised intermediaries. **On this model, users run the FreedomBox (Free and Open Source) software on a device in their home. They may install it on a small, inexpensive device plugged into the wall which operates as a personal cloud. It offers a wireless access point and a hard drive that stores users’ personal data, so they can access their information from any device over the Internet. It operates as a personal privacy protector: with the click of a button, users can enable Tor, and it will route their traffic through the Tor network to provide them with anonymity. It offers other services, such as private email and ad blocking. Crucially, FreedomBox allows the decentralised hosting of alternative platforms built for privacy, such as the GNU Social and Mastodon social networks, through either peer-to-peer or highly decentralised networks with servers based in local communities. On the FreedomBox model, each unit functions as a client and a server. The technology is explicitly designed to retrofit decentralisation into the Internet.62**
> (p. 27 )

> New decentralisation technologies have to be user-friendly with simple interfaces accessible to the masses; millions and then billions of people need to use them instead of surveillance services; devices must be affordable to the poor; ISPs must be prohibited from throttling technologies such as Bit Torrent and Tor; and sufficient funding is needed for development. Because these kinds of changes strike at the heart of the world’s empire, they require a strong movement driven from below. Popular participation, education, activism and legal innovation are critical to countering tech hegemony.66
> (p. 29 )

> New technologies are often viewed as something that ‘comes out’ on the market rather than products designed with particular values and power relations embedded in them. **From an engineering perspective, it does not have to be this way: digital technology can be owned and controlled by the popular classes. Discussions around tech should be holistic and address structural inequality, identity, culture, and politics.67** It is not enough to focus on US and European experiences when thinking about the digital world, as most discussions do in the North. Many countries in the Global South are digitising their societies, and the ecosystem must be viewed from a global perspective. **A paradigm shift is needed to change the focus from outcomes on the surface for Westerners (in domains like privacy and discrimination) to structural power at the technical architectural level within a global context.68**
> (p. 30 )
