---
title: Epistemologia de dados, colonialidade do poder e resistência
tags:
    - Paola Ricaurte Quijano
    - colonialidade
    - big data
    - dados
    - fichamento

path: "/epistemologias-de-dados-colonialidade-do-poder"
date: 2024-04-07
category: fichamento
featuredImage: "ricaurte.jpg"
srcInfo: <a href="https://commons.wikimedia.org/wiki/File:Paola_Ricaurte_Quijano_7.jpg">Paola Ricaurte</a>, <a href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>, via Wikimedia Commons
published: true
---

Fichamento do artigo "Data epistemologies, the coloniality of power and resistance"[^1] de Paola Ricaurte Quijano.

[^1]: Ricaurte, Paola. 2019. “Data Epistemologies, The Coloniality of Power, and Resistance”. Television & New Media 20(4):350–65. doi: 10.1177/1527476419831640.

# Abstract

> Data assemblages amplify historical forms of colonization through a complex arrangement of practices, materialities, territories, bodies, and subjectivities. **Data-centric epistemologies should be understood as an expression of the coloniality of power manifested as the violent imposition of ways of being, thinking, and feeling that leads to the expulsion of human beings from the social order, denies the existence of alternative worlds and epistemologies, and threatens life on Earth.** This article develops a theoretical model to analyze the coloniality of power through data and explores the multiple dimensions of coloniality as a framework for identifying ways of resisting data colonization. Finally, this article suggests possible alternative data epistemologies that are respectful of populations, cultural diversity, and environments.

# Big data como chão epistemológico da contemporaneidade

> Big data form the epistemological ground of our historical moment. We live under a new regime of knowledge production in which data processing through advanced statistics and prediction models informs decisions, actions, and relations. This knowledge regime requires data scientists, advanced computing capacity, and a vast amount of data to provide more accurate predictions for decision making in every field: security, public administration, finance, health, commerce, labor, climate, education, transport. Dominant discourses predict a near future in which a deep learning revolution and big data will optimize the capabilities of machine learning (ML) to solve the most complex tasks and foster economic growth. To accomplish this, the quality, diversity, and amount of collected data need to increase. **This epistemology, which represents a more complex evolution of the post-positivist paradigm, is based on three assumptions: (1) data reflects reality, (2) data analysis generates the most valuable and accurate knowledge, and (3) the results of data processing can be used to make better decisions about the world.** Nevertheless, all these assumptions should be contested and analyzed in a broader framework that considers how this form of knowledge production increases capital concentration (West 2017), surveillance (Zuboff 2015), and colonization (Couldry and Mejias 2018).
> (p. 1-2)

# Aspecto colonial da racionalidade baseada em dados

> We must also acknowledge the ways **this regime is rooted in a complex process of colonization through data by dispossession (Thatcher et al. 2016) and the capture of life (Couldry and Mejias 2018) at a supranational level.** In this article, I argue that this data-centric rationality should be understood as an expression of the coloniality of power (Mignolo 2014; Quijano 2000, 2007), manifested as the violent imposition of ways of being, thinking, and feeling that leads to the expulsion of human beings from the social order, denies the existence of alternative worlds and epistemologies (Escobar 2017; Santos 2009), and threatens life on Earth.
> (p. 2)

> **The regime of data colonialism includes the capture of data relations as defined by Couldry and Mejias (2018, 2) not only as “new types of human relations that enable the extraction of data for commodification” but also as the whole universe of human-object and object-object interactions that has emerged with the development of the “Internet of Things” (IoT), as well as biodata, and data from “non related to human-derived activities, such as those from energy, water, roads, infrastructure networks and natural resources” (Cordova et al. 2018).** This has led to new forms of colonization through data, grounded in material infrastructures and symbolic constructions that reinforce these practices. Data extraction, storage, processing, and analysis are part of a much broader process that is ripe for analysis through a decolonial lens.
> (p. 2)

# Extrativismo de dados

> Data centered economies foster extractive models of resource exploitation, the violation of human rights, cultural exclusion, and ecocide. **Data extractivism assumes that everything is a data source. In this view, life itself is nothing more than a continuous flow of data.** The pervasiveness of technologies and data regimes in all spheres of existence crowd out alternative forms of being, thinking, and sensing. **The commodification of life and the establishment of an order mediated by data relations limits the possibility of life outside the data regime: refusing to generate data means exclusion.**
> (p. 3)

# Modelo decolonial como alternativa ao modelo do extrativismo de dados

> In contrast to the compensation model, this article’s argument for decolonizing data follows the decolonial model established in the work of Quijano (2000, 2007), Mignolo (2014), and González Casanova (2006). It is possible to reframe the matrix of colonial power in terms of data colonialism as an epistemic order based on data. We can reimagine forms of data resistance as epistemic disobedience. **Decolonial thinking and epistemic disobedience demand the transformation of the structure of power by taking “control over labor/resources/product, over sex/resources/products, over authorities/ institutions/violence, and over intersubjectivity/knowledge/communication to the daily life of the people”** (Quijano 2000, 573). Colonialism and neocolonialism should be understood in terms of their effects on power relations across borders and their reproduction by technocrats, universities, and governments within borders of colonized countries. These forms of oppression should be seen through the lens of their effects on the bodies, affects, and territories of marginalized and multiethnic populations. We must identify how the multiple dimensions of colonization are interwoven and deployed as an internal, international, and transnational process (González Casanova 1980) that perpetuates exploitation, the extinction of alterity, and the diminishment of life on Earth.
> (p. 4)

> **The proposed analytical model approaches the coloniality of power through data as a complex socio-technical assemblage that articulates material infrastructures as well as biological, emotional, ecological, and symbolic dimensions that are generally ignored in theoretical debates.** While existing comprehensive models explain data assemblages (Kitchin 2014, 25), the coloniality of power framework assumes that the dimensions of knowing/being/sensing cannot be conceived independently and that data colonization implies violent forms of domination. **This understanding comprises a new ontology and epistemology that rejects Western rationality and dualism and the domination of alterity by any means.** The coloniality of power (Table 1) integrates the coloniality of knowing/being/sensing and can be deployed through interconnected subdimensions: the coloniality of economy, politics, knowledge, being, sensing, and nature. This model also considers the coloniality exerted through socio-technical sys- tems, the materiality on which data colonialism is built.
> (p. 4)

![Tabela 1](./ricaurte1.png)

# Colonizaćão de dados para além do ocidente

> The coloniality of power model enables us to reflect on non-Western contexts affected by this data-driven logic. **We must understand that governments and public institutions (including universities) act as central forces in the process of internal and international data colonization at the systemic level by (1) developing legal frameworks (2) designing public policy, (3) using artificial intelligence systems for public administration, (4) hiring technological services, (5) acquiring products for public administration and sur- veillance purposes, (6) implementing public policies and digital agendas, and (7) facilitating education and the development of labor forces.** Governments become the main clients of corporations (Table 2) that **offer artificial intelligence services for public decision making with corporate-owned data, the contracting of corporate services and the acquisition of products (cyberdefense, surveillance, telecom infrastructure, data centers, servers, IoT, smart cities, etc.), the implementation digital agendas (connectivity, hardware, software), and the development of education programs, curriculum, content, and agendas for labor force development (training, agreements).**
> (p. 8)

![Tabela 2](./ricaurte2.png)

# Recolonização de povos indígenas como "inclusão digital"

> The perspectives of indigenous nations and communities are rarely considered in debates surrounding digital agendas. Digital inclusion under this paradigm means connecting those who are still outside the scope of the data extraction system; that is the case for many indigenous communities in some Latin American countries. The consequences of data collection and the participation of marginalized groups in the digital economy has not been evaluated. **In many cases, indigenous populations are being recolonized through datafication and public policies regarding the implementation of digital literacy programs or digital citizenship policies. (Cordova 2018). In this way, populations are colonized not only on an individual and community level (through the use of Internet services, privative software and hardware, and the dependence to Western digital social networks for communication) but also at an institutional or systemic level, through the subordination of governments and institutions to the services of Western technology companies.** Added to existing structural violence, this results in simultaneous internal, international, and transnational colonization (González Casanova 1980)
> (p. 8)

# Mexico como exemplo

> Mexico lacks data about sensitive issues, and citizen initiatives try to fill this information gap. **The map of femicides is a good example of how data can be used to further justice and human rights, in this case honoring the memories of the thousands of women killed by the structures of economic and patriarchal violence.** The project demonstrates a critical reflection on power, gender violence, technology, and knowledge production through the use of categories endorsed by gender violence protocols that reflect the roots of structural violence, with an explicit decision to use free software and tools. This project demonstrates the value of citizen initiatives and the many challenges faced by those who work with sensitive data, data that relate to vulnerable communities, and data that would make visible the coloniality of power and the matrix of domination that conceals the continuity of economic, technological, political, physical, cognitive, and emotional forms of exclusion. At the same time, this initiative has proven it is possible to generate a social reaction and counternarratives that contribute to understanding the importance of the problem of gender violence. María’s story has been covered by various media outlets and has been exhibited in museums.
> (p. 11)


