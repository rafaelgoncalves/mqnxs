---
title: "Mediação algorítmica e aprendizado de máquina: defesa de dissertação de mestrado em sociologia"
tags:
    - Pedro Ferreira
    - Bárbara Geraldo de Castro
    - Henrique Parra
    - Rafael Evangelista
    - mediação técnica
    - mediação algorítmica
    - capitalismo
    - google
    - modulação
    - captura
    - imagem
    - imaginação
    - algoritmos
    - apresentação
    - patentes
    - inteligência artificial
    - aprendizado de máquina

path: "/mediacao-algoritmica"
date: 2025-02-08
category: apresentação
featuredImage: "cover.png"
srcInfo: Defesa de dissertaçao de mestrado em sociologia
published: true
---

Fala apresentada na ocasiao de defesa de dissertaçao de mestrado em sociologia[^1] "Mediaçao algoritmica e aprendizado de maquina" no Instituto de Filsofia e Ciências Humanas (IFCH) da Unicamp no dia 29 de janeiro de 2025. Abaixo segue a gravação do áudio da arguição.

[^1]: <https://www.laspa.slg.br/2025/01/27/mediacao-algoritmica-e-aprendizado-de-maquina/>


# Apresentação

Bom dia a todas, eu gostaria de agradecer a presença de vocês, e especialmente do meu orientador Pedro, e da Bárbara, Henrique e Rafael por aceitarem o convite para participarem da banca de minha defesa de dissertação de mestrado.

Como vocês sabem, eu vou apresentar rapidamente a minha dissertação, que tem o título _Mediação algorítmica e aprendizado de máquina: uma caracterização baseada em patentes da Google sobre técnicas de modulação de atividade_. Eu comecei o mestrado em 2023 e ele foi orientado por _Pedro Ferreira_ e financiado pela _FAPESP_. Quem quiser acessar o texto que eu enviei pra banca, é só acessar este link: <https://rafaelg.net.br/dissertacao.pdf>. 

![slide 00](./00.png)

Bem, como o próprio título já mostra, eu tentei desenvolver meu trabalho na intersecção entre duas questões: a primeira seria a de propor de _uma descrição sociotécnica do aprendizado de máquina_. O aprendizado de máquina é a principal técnica da inteligência artificial contemporânea que é baseada no processamento de grandes quantidades de dados. Então, eu queria contribuir para a apropriação sociológica dos debates recentes sobre inteligência artificial, um tema cada vez mais presente no nosso cotidiano coletivo. Mas eu também me interessei em uma questão talvez mais ampla: a da _agência dos algoritmos_. Ou seja, eu gostaria de participar de um debate preocupado em descrever como os algoritmos agem socialmente, no meu caso específico, os algoritmos de aprendizado de máquina. Eu queria resolver - pra mim mesmo inicialmente - como descrever o modo como os algoritmos são capazes de participar ativamente na transformação da realidade material e na criação de novos significados coletivos.

Pra explorar essas duas questões, eu parti de uma análise empírica de _patentes da Google_ sobre _algoritmos de modulação de atividade_, ou seja, algoritmos que visavam de alguma forma _afetar a atividade da usuária_.

Por conta disso, a própria estrutura da dissertação foi dividida em duas partes, além de uma introdução que apresenta a Google, as patentes de modulação de atividade e os métodos que utilizei durante a pesquisa. Vou fazer essa breve apresentação seguindo a mesma divisão.

## Tecnociência e capitalismo contemporâneo

![slide 01](./01.png)

Primeiramente, o motivo de eu ter escolhido a Google tem a ver com um entendimento de algumas autoras das ciências sociais de que ela tem uma posição central na aproximação recente entre desenvolvimento tecnocientífico e criação de novas formas de acumulação capitalista. A autora Shoshana Zuboff, por exemplo, escreve em 2015 que :

> A Google é considerada por muitos como a pioneira do big data e com a força desses feitos também foi pioneira na lógica de acumulação mais ampla que denomino de capitalismo de vigilância, da qual o big data é tanto uma condição quanto uma expressão (Zuboff 2018 [2015], 24-5).

![slide 02](./02.png)

Lembrando que o big data é outra expressão que está intimamente relacionada com os processos que chamamos de inteligência artificial e aprendizado de máquina.

Outros autores como Matteo Pasquineli, Christian Fuchs e Julian Assange também dão elementos pra entender sua centralidade política e econômica. Mas nessa apresentação vou me deter à Zuboff. O que me interessa da caracterização da Zuboff é não só a relação direta que ela traça da empresa com o desenvolvimento tecnocientífico, mas também o entendimento de que a dominação da Google excede o campo econômico. Dessa forma, sua análise está preocupada com:

> [A] lógica de acumulação atualmente institucionalizada [notoriamente pela Google] que produz agenciamentos em hiperescala de dados objetivos e subjetivos sobre indivíduos e seus habitat no intuito de conhecer, controlar e modificar comportamentos para produzir novas variedades de mercantilização, monetização e controle (Zuboff 2018 [2015]: 57). 

![slide 03](./03.png)
 
Esse ponto foi importante pra mim, pois os algoritmos que eu analiso visam justamente modular a atividade das usuárias, o que não só cria novas possibilidades de valorização, mas também de um controle que não é exclusivamente econômico, é o que a Zuboff chama de _excedente comportamental_, o que possibilita um poder preditivo sobre o sujeito. 

![slide 04](./04.png)

Partindo disso, eu escolhi utilizar patentes da Google como material da minha pesquisa. Pra chegar nas patentes eu utilizei a ferramenta _Base de dados de Texto Completo de Patentes_ (PatFT) do _Escritório de Patentes e Marcas registradas dos Estados Unidos_ (USPTO). Entretanto, no meio da pesquisa esta ferramenta foi substituída pela _Busca Pública de Patentes_ (PPubS) que é a que está mostrada no slide. O funcionamento de ambas é bem parecido. No caso da pesquisa eu busquei por: _todas as patentes da Google que continham o termo aprendizado de máquina no título ou no abstract_, o que resultou, ainda em junho de 2022, em 215 patentes. Dessas, eu analisei _nove_ que tinham relação direta com o fenômeno da _modulação de atividade_. 

![slide 05](./05.png)

As patentes são documentos jurídicos com descrições tecnológicas que servem ao mesmo tempo pra apresentar uma invenção e garantir pra quem patenteia o _direito de excluir que outras pessoas construam a invenção patenteada_. Ou seja, é um direito negativo, uma forma de propriedade intelectual que não garante que a invenção um dia vai ser construída. Por outro lado, elas ao menos dão uma pista do sentido que o desenvolvimento tecnológico está indo e na minha experiência foram uma fonte mais interessante pra análise sociológica de algoritmos do que artigos científicos, por exemplo. As _nove_ patentes que eu analisei estão mostradas na tela. Vou falar brevemente do que se trata cada uma delas:

USANDO FLUXOS DE DADOS E/OU CONSULTAS DE PESQUISA PARA DETERMINAR INFORMAÇÃO SOBRE EVENTOS EM DESENVOLVIMENTO: visa agregar e processar dados de celulares, computadores e instituições para, utilizando aprendizado de máquina, prever o desenvolvimento de eventos e notificar usuárias.

SUGESTÃO DE MENSAGENS: utiliza aprendizado de máquina para criar uma representação semântica de mensagens em um aplicativo de troca de mensagens, bem como sugerir palavras no momento da escrita de novas mensagens.

EXECUTANDO SUBTAREFA(S) PARA UMA AÇÃO PREVISTA EM RESPOSTA A UMA INTERAÇÃO SEPARADA DA USUÁRIA COM UM ASSISTENTE AUTOMÁTICO ANTES DA EXECUÇÃO DA AÇÃO PREVISTA: utiliza aprendizado de máquina para prever uma interação entre usuária e assistente automático para carregar dados e começar processos antes mesmo da interação acontecer.

USANDO APRENDIZADO DE MÁQUINA E OUTROS MODELOS PARA DETERMINAR A PREFERÊNCIA DE UMA USUÁRIA A CANCELAR UMA TRANSMISSÃO OU DOWNLOAD: visa treinar um modelo de aprendizado de máquina que seja capaz de decidir pela usuária qual download ou transmissão de mídia interromper no caso de muitos downloads simultâneos que ultrapassem um dado limite.

RESOLVENDO AUTOMATICAMENTE, COM ENTRADAS DE USUÁRIAS REDUZIDAS, UM CONJUNTO DE INSTÂNCIAS DE ATIVIDADES PARA UM GRUPO DE USUÁRIAS: apresenta técnicas para sugestão de atividades, locais e horários em uma conversa em grupo. O aprendizado de máquina é utilizado para inferir a sugestão e é treinado por meio da verificação se as usuárias executaram a atividade sugerida.

SISTEMAS E MÉTODOS PARA PRIORIZAÇÃO DE NOTIFICAÇÕES EM DISPOSITIVOS MÓVEIS: propõe substituir a ordem cronológica de apresentação de notificações em celulares para uma relacionada com a probabilidade de interação inferida via aprendizado de máquina. A patente prevê inclusive que algumas notificações nem sejam mostradas à usuária.

MÉTODO E APARATO UTILIZANDO DADOS DO ACELERÔMETRO PARA PROVIDENCIAR ANÚNCIOS MELHORES: propõe utilizar aprendizagem de máquina sobre dados do acelerômetro, um sensor de smartphone, pra tipificar o movimento da usuária e sugerir anúncios.

REORDENANDO RESULTADOS DE CONSULTA DE PESQUISA DE ACORDO COM FUNÇÕES DE DESEMPENHO PREVISTAS PARA O CONTEXTO DE BUSCA ESPECÍFICO: visa utilizar aprendizado de máquina para alterar os resultados de pesquisa para se adequarem à qualidade prevista para cada usuária específica.

SUGESTÃO DE AÇÕES BASEADA EM APRENDIZADO DE MÁQUINA: visa sugerir ações a serem executadas em um dispositivo pelo processamento via aprendizado de máquina de informação da tela ou do contexto da usuária.

## Uma cartografia do aprendizado de máquina

![slide 06](./06.png)

Vou pegar esta última patente como exemplo nesta apresentação. A informação que este método recebe é ou a informação da _tela do dispositivo_ ou o _contexto, obtido por sensores_, por exemplo. Com base nisso, o método apresenta uma sobreposição na tela, que a patente chama de _fatia [_slice_]_ , sugerindo uma determinada ação. 

![slide 07](./07.png)

Na primeira imagem, a usuária está utilizando um aplicativo de _troca de mensagens_ e a fatia sugere _criar um evento no calendário_ baseado no conteúdo da conversa.

Na segunda, a fatia _sugere correções gramaticais_ para um e-mail que _está sendo redigido pela usuária_.

Na terceira, a usuária está interagindo com _um aplicativo de transporte indisponível no seu local_ e a fatia sugere _a instalação ou troca para outro aplicativo de transporte_.

Na última, o _celular parece estar bloqueado_ e a fatia sugere _uma mudança de temperatura_, possivelmente em uma casa automatizada, provavelmente com base em sensores de temperatura, internet ou padrões comportamentais da usuária.

Esse é um exemplo de uma técnica de modulação de atividade. Em alguma medida, ela substitui ou altera a realização de atividades pela usuária. Vou ler um trecho dessa patente que ilustram o tipo de enunciado que eu utilizo nas minhas análises:

> Note também que a tarefa determinada pelas técnicas pode ser uma que o usuário provavelmente vai querer em seguida, apesar do usuário não necessariamente saber que é uma tarefa provável. Então, o usuário pode ainda não estar consciente de que ele quer criar um evento no calendário ou fazer uma chamada. O módulo de aprendizado de máquina pode determinar essa tarefa mesmo assim (Krishna et al. 2021: 12).

![slide 08](./08.png)

Nessa última frase fica bastante explícito, inclusive, a pretensão de modulação de atividade. Outros trechos que eu utilizei são bastante mais técnicos e descrevem com detalhes o funcionamento das tecnologias patenteadas.

Com base nessas descrições, eu criei uma _cartografia_ que visa caracterizar sócio-tecnicamente o processo de modulação de atividade por aprendizado de máquina. Essa cartografia tem três momentos principais, como na imagem apresentada no slide.

![slide 09](./09.png)

A **captura** descreve o processo de _captação de informação_ principalmente por sensores e pela interface de usuária, a _digitalização_ dessa informação, _criação de dados_ e o treinamento do modelo que _converte os dados em parâmetros numéricos_. Esses parâmetros criam uma _imagem_ no interior do modelo, o que as vezes é endoginamente chamada de _espaço latente_. A captura descreve uma _vigilância ativa_ que cria um excedente, a imagem, a partir da captação de informação e criação de dados.

A **imaginação** está centrada no processo propriamente dito de criação da _imagem_: durante o treinamento do modelo, os dados criados na captura se somam a outros dados públicos e privados que são utilizados para configurar o modelo de aprendizado de máquina. Esse treinamento _cria uma representação de processos_, que é o que eu chamo de _imagem_, que depois é utilizada como potencial para as capacidades do modelo de _inferência_ e _incidência informacional_. Por exemplo, o treinamento de um modelo para a sugestão de ações pode criar uma imagem de hábitos comportamentais. A partir disso o modelo é capaz de processar dados de sensores ou da tela de um smartphone e sugerir ações para a usuária.

Finalmente a **modulação** diz respeito à afetação da usuária. Minha análise mostrou que o aprendizado de máquina _afeta de forma indireta a usuária_: o modelo age sobretudo pela alteração da interface de usuária que, por sua vez, propicia a usuária a determinadas ações em detrimento de outras. A informação de saída propriamente dita são as classificações, predições, sugestões, gerações, etc. que afetam a atividade da usuária. É importante perceber que as próprias ações da usuária são realimentadas no modelo por meio da captura, de forma que podemos dizer que a usuária participa como um componente de processo de aprendizagem de máquina, similar ao regime que Deleuze e Guattari (2011-2012) chamaram de _servidão maquínica_.

Desse modo, uma importante conclusão do meu mestrado foi descrever o aprendizado de máquina como um processo sociotécnico que envolve essas três operações.

## Mediação algorítmica

![slide 10](./10.png)

Passando de um foco no funcionamento para a descrição de sua agência, uma outra preocupação do meu trabalho foi definir sociotecnicamente um algoritmo para então falar da sua ação social.

![slide 11](./11.png)

Nas patentes e em outros materiais da área de computação, há geralmente três sentidos distintos atribuídos ao termo _algoritmo_. Por vezes, ele é tomado como um conjunto de _regras lógicas_, muitas vezes comparado com uma _“receita de bolo”_. Outras vezes, ele é considerado como o conjunto de _operações materiais_ que acontece em um dispositivo quando um programa está sendo executado. E entre essas duas definições ainda, por vezes chamamos de algoritmo o _programa_ escrito em uma linguagem de programação específica, ou seja, as regras lógicas já codificadas, mas ainda não executadas. Um ponto intermediário entre sua descrição abstrata e sua operação concreta. De certa forma essa separação indica o foco em três momentos distintos: a concepção, a implementação e a execução de um algoritmo. O que há de comum nessas três definições é que o algoritmo é uma _operação intermediária que transforma entradas em saídas_. 

Sem perder de vista essas três definições, eu queria usar uma definição que fosse ao mesmo tempo _específica aos sistemas digitais_, portanto com um vínculo com sua materialidade, e _aberto o suficiente pra abarcar os principais usos do termo_. Assim, eu entendi que os algoritmos estão na intersecção de duas tendências tecnocientíficas: por um lado, a ideia de _programabilidade_ e, por outra, a _história da microeletrônica digital_. O que faz convergir ambas as tendências é o _computador digital_. Propus considerá-lo como o _indivíduo técnico_ da operação de computação (Simondon 2020). Desse modo, podemos entender os algoritmos como _unidades elementares da computação em um computador digital_, especificamente, eles são os atores responsáveis por mediar os processos informacionais no interior de um computador digital, relacionando entradas e saídas. Ou seja, considerei que **os algoritmos são os mediadores da informação digital**.

Em outros trabalhos sobre algoritmos, a agência deles é geralmente descrita como _vieses_. O viés é um _resultado do algoritmo que pode ser entendido como problemático do ponto de vista ético ou político_. Há muitos exemplos na mídia, como a dificuldade em classificar rostos de pessoas não brancas ou a tendência em traduzir palavras sempre para o masculino. Mas entendemos que o termo viés tem pelo menos duas limitações:

1. Esse termo nos remete à ideia de que o viés seria um erro, e que é possível e desejável desenviesar os algoritmos;
2. O foco geralmente recai em resultados individuais, que nem sempre levam em conta problemas mais estruturais de fundo.

Em outras palavras, acredito que essa descrição da agência algorítmica _ainda considera que a neutralidade é possível e desejável_. Desse modo, essa caracterização acaba limitando o horizonte dos trabalhos sobre a agência dos algorítmos, pois eles podem, no máximo, não produzir vieses, ao invés de pensar de forma mais ampla que sua agência poderia ser construtiva para mundos alternativos, mais justos e responsáveis. Além disso, as assimetrias sociais nem sempre são capturadas pela ideia de viés: um algoritmo pode ter uma acurácia muito grande em classificar gênero de rostos masculinos e femininos, por exemplo, mas se ele só considerar essas duas possibilidades, ele exclui identidades trans, por exemplo. Ou seja, ele participa bastante ativamente na construção de uma sociedade sexista e trans-excludente. (É isso o que mostra o trabalho “tons de gênero” de Joy Buolamwini e Tmnit Gebru (2018)).

Buscando fugir dessas limitações, propus descrever a agência algorítmica por outras vias. Por já entender os algoritmos como mediadores, parti do conceito de _mediação técnica_ de Bruno Latour (2018). Para Latour, há uma mediação técnica quando a _ação e o significado de outros momentos, de outros lugares e de outros agentes continuam a agir “aqui e agora”, mas na condição de que essa ação e esse significado sejam trocados, traduzidos, delegados ou descolados por e para um agente não-humano_. No nosso caso, esse agente não humano seria o computador digital. Dessa forma, a mediação técnica é uma espécie de agência do meio técnico.

Embora com suas especificidades, acreditamos que é isso o que ocorre quando há a execução de um algoritmo em um computador digital. Desse modo, escrevemos que “a agência dos algoritmos pode ser descrita como mediação algorítmica, um processo ou agenciamento que resulta em uma ação/significação e que é realizada pelo controle da informação digital em um sistema de computação” (Gonçalves 2024: 152). Em outras palavras, _uma mediação algorítmica é uma mediação técnica cujo meio é um computador digital_. Por conta da especificidade do computador digital, podemos dizer que sua ação ocorre por meio da mediação da informação digital pelos algoritmos.

![slide 12](./12.png)

A partir disso, localizamos três modos específicos que o aprendizado de máquina age, ou seja, três de suas mediações algorítmicas:

1. Por retirar sua capacidade dos dados do passado e por funcionar como um laço de realimentação, o aprendizado de máquina possui um **funcionamento conservador**. Ele tende a reproduzir em seus resultados o passado, como se o novo pudesse ser inferido do antigo. É o que Matteo Pasquinelli e Vladan Joler (2020) chamam de “regeneração do passado”. Um exemplo nas patentes analisado no capítulo quatro foi a sugestão sempre de um mesmo aplicativo de streaming de música para uma usuária, o que incentiva que a ela não mude seus padrões comportamentais.
2. Por conta de seu automatismo e de um funcionamento que visa uma minimização do erro, o aprendizado de máquina também propicia a **criação de estereótipos** (Gonçalves 2022). Um exemplo nas patentes é a sugestão de mensagens prontas na patente de sugestão de mensagens analisado no capítulo três. Se uma usuária deseja mudar sua linguagem, deve recusar as sugestões do método patenteado.
3. Mas, embora isso não seja explicitado nas patentes de forma isolada, o conservadorismo e a estereotipia se relacionam diretamente com a **reprodução de assimetrias sociais**. Relacionando nosso trabalho com outros casos, como os sistemas de classificação automática de gênero, é possível perceber que o aprendizado de máquina tem um papel bastante importante na concretização de casos de racismo e sexismo algorítmico. 

Um exemplo bastante perceptível da reprodução de assimetrias sociais por aprendizado de máquina são os conhecidos modelos gerativos. No exemplo do slide, é possível ver que a palavra _doutora [_doctor_]_ em inglês gera a maioria das imagens _aparentemente masculinas e principalmente de pessoas brancas_. A palavra _enfermeira [_nurse_]_, no mesmo gerador de imagens, gera a maior das imagens com a aparência de _mulheres brancas_.

![slide 13](./13.png)
![slide 14](./14.png)

Por fim, embora no caso do aprendizado de máquina as mediações mais evidentes e mais gerais sejam principalmente negativas de um ponto de vista social, reforçando assimetrias, uma conclusão importante pra mim no mestrado foi encontrar uma forma de descrever a agência algorítmica que não esteja limitada em apontar suas tendências prejudiciais. Desse modo, algo que eu vou levar para minhas próximas pesquisas é essa forma de descrever a ação social dos algoritmos como _mediação algorítmica_, buscando entender de que maneiras algoritmos controlam fluxos de informação digital. Eu acho que esse modo é interessante porque não reduz os algoritmos a ferramentas neutras que exclusivamente reproduzem as intenções de programadoras ou de usuárias, mas também não atribui a eles uma autonomia absoluta. Desse modo, eu acredito que a análise das mediações algorítmicas permite uma descrição mais precisa da parcela de ação que é realizada pelos algoritmos. Também acho que é um conceito que permite uma responsabilidade política (Haraway 2023), pois explicita sua conexão com outros atores sociais.

![slide 15](./15.png)

# Áudios da apresentação e da arguição da banca

Fala da abertura de Bárbara Geraldo de Castro e minha apresentação:

`audio: /audio/apresentacao.m4a`

Comentários e respostas à fala de Henrique Parra:

`audio: /audio/henrique.m4a`

Comentários e respostas à fala de Rafael Evangelista:

`audio: /audio/rafael.m4a`

Comentários finais de Pedro Ferreira antes da deliberação da banca:

`audio: /audio/pedro.m4a`

# Referências

Buolamwini, Joy, e Timnit Gebru. 2018. “Gender Shades: Intersectional Accuracy Disparities in Commercial Gender Classiﬁcation”. Em **Proceedings of Machine Learning Research**, 81:1–18.

Deleuze, Gilles, e Félix Guattari. 2011-2012. **Mil platôs: capitalismo e esquizofrenia 2**. 2º ed. 5 vols. São Paulo: Editora 34.

Gonçalves, Rafael. 2024. **Mediação algorítmica e aprendizado de máquina: uma caracterização baseada em patentes da Google sobre técnicas de modulação de atividade**. <https://rafaelg.net.br/dissertacao.pdf>.

Gonçalves, Rafael. 2022. “Automatismo ontem e hoje: reflexões sobre os limites da inteligência artificial a partir de Simondon”. **Ideias** 13 (junho):e022008. <https://doi.org/10.20396/ideias.v13i00.8668218>.

Haraway, Donna. 2023. **A reinvenção da natureza: símios, ciborgues e mulheres**. WMF Martins Fontes.

Krishna, Golden Gopal, Carl Magnus Borg, Miroslav Bojic, Henry Owen Newton-Dunn, Jacob M. Klinker, Mindy Pereira, Devin Mancuso, Daniel June Hyung Park, e Lily Sin. 2021. **Suggesting actions based on machine learning**. United States US10970096B2, filed 21 de setembro de 2020, e issued 6 de abril de 2021.

Latour, Bruno. 2017. **A esperança de pandora: ensaios sobre a realidade dos estudos científicos**. São Paulo: Editora Unesp.

Pasquinelli, Matteo, e Vladan Joler. 2021. “The Nooscope Manifested: AI as Instrument of Knowledge Extractivism”. **AI & SOCIETY** 36 (4): 1263–80. <https://doi.org/10.1007/s00146-020-01097-6>.

Silva, Tarcízio. 2022. **Racismo algorítmico: inteligência artificial e discriminação nas redes digitais**. Edições Sesc SP. <https://racismo-algoritmico.pubpub.org/>.

Simondon, Gilbert. 2020. **Do modo de existência dos objetos técnicos**. Traduzido por Vera Ribeiro. 1º ed. Rio de Janeiro: Editora Contraponto.

Zuboff, Shoshana. 2018 [2015]. “Big other: capitalismo de vigilância e perspectivas para uma civilização de informação”. Em: Fernanda Bruno, Bruno Cardoso, Marta Kanashiro, e Luciana Guilhon (eds.). **Tecnopolíticas de Vigilância: perspectivas da margem**. São Paulo: Boitempo, pp. 17–68.  [“Big Other: Surveillance Capitalism and the Prospects of an Information Civilization”. **Journal of Information Technology** 30 (1): 75–89. <https://doi.org/10.1057/jit.2015.5>].
