---
title: Os Comuns
date: 2021-06-07
path: "/comuns"
category: fichamento
featuredImage: "../../images/bica.jpg"
srcInfo: Foto por <a href="https://www.flickr.com/photos/mturdestinos/40156756164/in/photolist-241XZmf-nhzGDb-24bvSyU-23U9eG6-FAVspP-fu5dt7-bfzrvH-fkUii4-55fpWe-3MGe7M">Renato Soares/MTur</a>
tags: ["alternativas sistêmicas", "Christophe Aguinton", "commons", "fichamento"]
published: true
---

Fichamento de um capítulo (escrito por Christophe Aguinton) do livro _Alternativas sistémicas_[^1], organizado por Pablo Sólon, feito no contexto da disciplina "Introdução à Economia" cursada no IE/Unicamp no primeiro semestre de 2021.

[^1]: SOLÓN, P. **Alternativas sistémicas: Bien Vivir, decrecimiento, comunes, ecofeminismo, derechos de la Madre Tierra e desglobalización**. 2017. Disponível em: <https://systemicalternatives.org/>

## Comuns como relação social

> ¿Es correcto hablar de “bienes” comunes como si se tratara de recursos físicos, naturales o del conocimiento? O, por el contrario, ¿los comunes son un tipo de relación social, una forma de gestión social de diferentes elementos y procesos necesarios para la vida de una comunidad humana?
> (p. 1)

> Estas relaciones sociales de colaboración se dan en torno a algún tipo de elemento material, natural, digital o del conocimiento, pero lo que los hace ser “comunes” es la práctica de esa gestión comunitaria que les permite “cuidar” aquel elemento sobre el cual intervienen y, al mismo tiempo, reproducir y enriquecer sus formas de organización social.
> (p. 79)

> Los “comunes” son un modo particular de relación social con los bienes materiales o inmateriales. Elementos naturales como el agua o el aire existen como tales y sólo se convierten en comunes cuando una comunidad humana gestiona sus relaciones con estos elementos de manera colectiva, por ejemplo, la distribución de agua para riego en una comunidad.
> (p. 80)

## Origem na inglaterra medieval

> El origen de los “comunes” (commons en inglés) viene de la Inglaterra medieval, donde los agricultores tuvieron acceso a pastos y bosques de dominio señorial. La “Carta Magna”, impuesta por los barones ingleses al rey Juan en el año 1215, que definía las libertades de las cuales se beneficiaron los miembros del reino, fue modificada en 1225 incorporando un segundo texto denominado la “Carta de los bosques” (Bollier, 2015).
> (p. 80)

> Estos comunes británicos fueron cuestionados en los siglos XVI y XVIII por los propietarios de tierras que querían colocar cercas para el pastoreo de ovejas con la finalidad de abastecer a la industria textil en auge. Aunque esta realidad de los comunes británicos corresponde al contexto de la organización social y económica del medioevo, situaciones parecidas se han dado en varias sociedades pre-capitalistas de diferentes continentes y, de manera muy diversa y compleja, en formas de gestión de pueblos indígenas que ejercían la práctica de gestión de los “bienes comunes”.
> (p. 80)

## "El Gobierno de los Comunes: la evolución de las instituiciones para la acción colectiva"

Livro da cientista política Elinor Ostrom, prêmio Nobel de economia em 2009

> la “gobernanza policéntrica” basada en diseños complejos para manejos complejos en realidades complejas es la que subyace en la gestión de los comunes (Ostrom, 2010). Ostrom postula que las comunidades y la gente son capaces de generar sistemas de gestión sostenibles creando consensos sociales para la gestión de los llamados “recursos”. Bajo esta perspectiva, predomina la noción de abundancia -versus la de escasez- como sostén del paradigma de los comunes.
> (p. 82)

> En su trabajo, ella identifica ocho principios que caracterizan las estructuras de gestión de los comunes:
>  - Organizaciones con integrantes claramente definidos: se sabe cómo y por qué pertenecen al grupo.
>  - Coherencia de las reglas para la gestión de los comunes: sobre quiénes, cuándo y cuánto del bien común se puede “usar” o gestionar.
>  - Sistemas democráticos de elección colectiva de representantes.
>  - Sistemas de vigilancia: los encargados deben responder ante la organización.
>  - Sistemas de sanciones para los que falten a las reglas.
>  - Mecanismos para resolver conflictos.
>  - Reconocimiento mínimo de derechos de organización autónoma ante autoridades estatales o municipales.
>  - Las actividades alrededor del recurso de uso común se realizan desde las organizaciones interesadas.
> (p. 82-83)

Limitações dessa abordagem:

> El aporte de Ostrom no está exento de algunas críticas por su apreciación de la naturaleza como “recursos” que podrían ser gestionados por una colectividad humana, olvidándose que éstos son parte de ecosistemas y del sistema de la Tierra, que tienen sus propios ciclos vitales y que no pueden ser “gestionados” de manera antropocéntrica, si es que se pretende una sostenibilidad de esos ecosistemas. Esta realidad se hace muy visible cuando se analizan los comunes de varios pueblos indígenas que, desde su visión, consideran a la naturaleza como su hogar, como su madre y su base vital, a la cual no se proponen “gobernar” sino más bien convivir con ella y cuidarla.
> (p. 83)

## Os comuns e o neoliberalismo

> Los comunes han adquirido notoriedad porque son una respuesta a la privatización generalizada que promueve el actual proceso de globalización neoliberal.
> (p. 83)

## Bens públicos e bens comuns

> A diferencia de las anteriores propuestas, nos parece importante distinguir entre bienes públicos y bienes comunes para marcar la diferencia con las visiones de izquierda del siglo pasado, que sobretodo giraban alrededor de la oposición capitalismo versus socialismo. O en otros términos, propiedad privada de los medios de producción y ley de mercado, por un lado, versus, nacionalización de los medios de producción y planificación económica, por otro.
> (p. 84)
 
> En última instancia, la esfera pública incluye todo lo relacionado con el Estado, en el sentido más amplio. En cambio, los comunes son el espacio donde los interesados se implican de manera directa.
> (p. 85)

## Lógica ternária público-privado-comum ou quaternária público-privado-comum-natureza

> No existe una relación solamente binaria entre lo público y lo privado, entre el mercado y la planificación estatal. En la realidad, existe una relación ternaria entre lo público, lo privado y los comunes. Por ejemplo, en el mundo de la cartografía tenemos, por un lado, una multinacional privada con un cuasi-monopolio privado con “Google Maps” y “Google Street View”; por otro lado, tenemos agencias públicas de mapeo, la mayoría militares; y por último, están los “activistas de la web” que crearon otra opción denominada “OpenStreetMap”, que es un mapa de libre colaboración y acceso que tiene cada vez mayor éxito.
> (p. 85)

> También está la opinión de quienes señalan que en verdad hay una relación cuaternaria entre lo público, lo privado, los comunes y la naturaleza. Esta última tiene sus propios procesos de auto-regulación y una dinámica que tiene que ser tomada en cuenta a la hora de cualquier proceso de “gestión”.
> (p. 86)

## Free Software Foundation (FSF)

> A principios de la década de 1980, aparecieron los “softwares libres” que no estaban protegidos debido a que el “software”, es decir, los programas, no estaban sujetos a un cargo monetario separado del “hardware”, que eran los equipos de computación que vendía la industria de la informática.
> (p. 86)

> Esta transformación en la industria del software dio lugar a la creación, en 1985, de la FSF, la “Free Software Foundation” que sería la organización de apoyo para las licencias de software libre, entre las cuáles la más conocida es la “General Public Licence” (GPL10)[3]. Para Richard Stallman, fundador de la FSF, y el movimiento por el software libre, éste es “un bien común de la humanidad” que debe ser accesible a todos. Este es un movimiento que ha crecido constantemente hasta el punto que, en la actualidad, la mayor parte del software es libre o basado en software libre.
> (p. 86-87)

## Creative Commons

> En la década de los 2000, los comunes del conocimiento se expandieron a través de dos iniciativas. La primera fue la introducción de los Creative Commons[4] que son un conjunto de licencias que pueden liberar cualquier obra intelectual, foto, texto, música, etc. de los derechos de autor y de propiedad intelectual. Gilberto Gil, uno de los más grandes artistas brasileños y ministro de cultura en el gobierno de Lula, difundió varias de sus obras bajo la licencia Creative Commons.
> (p. 87)

## Open access initiaitive

> En respuesta a esta situación, y para permitir que las universidades de los países del sur, que a menudo carecen de los medios para pagar las suscripciones a revistas académicas caras, fue lanzado el “Open access initiative”[5], el año 2002 en Budapest.
> (p. 87)

## Guerras da água (Bolívia)

> Entre las muchas iniciativas que se han tomado para evitar la privatización de los comunes, y las que más éxito han alcanzado, son las vinculadas al agua. Los ejemplos más notables fueron las guerras del agua en Bolivia en las cuales se ganaron peleas en contra de la privatización en Cochabamba y El Alto. Otro ejemplo notable, es el referéndum en Italia en el cual prevaleció el SI al “agua como bien común”. Estas victorias, sin embargo, no han devenido en un incremento de la capacidad de la población para gestionar el agua como bien común. En Bolivia, la gestión pública del agua no produjo los resultados deseados, y la transición a una “gestión público-social”, que era el reclamo de los movimientos en contra de la privatización, encontró resistencia tanto de las autoridades como de las burocracias sindicales y la tecnocracia de las empresas públicas del agua. De esta manera, estos comunes que involucran a la naturaleza se quedaron a medio camino, como proyectos sin una concreción real.
> (p. 87-88)

## Cooperativas, fundos mutualistas, asociações e organizações ligadas a economia solidária

> Se trata de comunes muy diversos, como puede ser una cooperativa retomada por sus trabajadores después de un conflicto social o un banco mutualista con una cartera de miles de millones de euros. Estas estructuras pueden ser híbridas y tener diferentes dinámicas que las terminan alejando de los comunes. Sin embargo, es de señalar que las cooperativas fueron la primera respuesta del movimiento obrero y el movimiento socialista del siglo XIX como alternativa al capitalismo industrial, basado en la explotación y la alienación de los trabajadores.
> (p. 88)

## Diversidade e comuns

> Esta diversidad, complejidad y tensiones que existen entre los “comunes” lejos de debilitar su enfoque lo enriquece y nos obliga a partir de la realidad antes que de esquemas que, muchas veces, no abarcan las múltiples dimensiones de los procesos de gestión y de construcción de lo común que se dan por fuera de lo público y lo privado.
> (p. 89)

## 2 mitos da modernidade ocidental capitalista: soberania estatal e propriedade privada

> En el seno de la modernidad occidental y el capitalismo encontramos dos mitos: la soberanía ilimitada del Estado tal como lo define Hobbes en el Leviatán, y la fe en la institución de la propiedad que permite a John Locke vincular la propiedad privada y la prosperidad general.
> (p. 89)

> Estamos en una fase histórica donde el Estado renuncia a una serie de sus prerrogativas para defender a la propiedad privada, porque el Estado, al final de cuentas, es también un propietario. Entre la soberanía estatal y la soberanía de la propiedad privada no hay lugar para los comunes, que se rigen por un conjunto de derechos y obligaciones en virtud de otras lógicas.
> (p. 89)

## Commoning como "fazer em comum"

> Burns Weston y David Bollier van más allá de la simple descripción de los diferentes tipos de derechos y hacen hincapié en la importancia del “commoning”, que es el hecho de “hacer en común”, de actuar colectivamente para el desarrollo de los comunes. El “commoning” es la lógica que está presente en los diferentes tipos de comunes. El concepto “commoning” permite describir las prácticas que son similares en la gestión de los comunes, excluyendo aquellas que se realizan a través de la propiedad privada o que son asumidas por el Estado y sus instituciones. El concepto de “commonig” se basa en una cultura de cooperación y reciprocidad (Weston y Bollier, 2013).
> (p. 90)

## Pratica extrativista vs prática gerativa (comuns)

> Capra y Mattei desarrollan un enfoque innovador al distinguir una práctica extractivista de una generadora. El sistema jurídico actual se basa en una mentalidad extractivista que atomiza la sociedad en base a la noción de individualismo por la cual todo trato humano es reducible a una relación de propiedad. En cambio, el derecho al “commoning” es generador porque se basa en relaciones de cooperación, reproducción, acceso e inclusión y promueven nuevas prácticas bajo una lógica imaginativa para el desarrollo de los comunes, aquello que en América Latina se denomina el “pro común”.
> (p. 90)

## Cuidado como divisor comum (dos comuns)

>“Commoning” es un derecho generador que promueve conceptos claves para el funcionamiento de todos los comunes. Podemos entonces proponer un principio que una a todos los comunes: el “cuidado”.
> (p. 91)

## Propriedade coletiva > propriedade pública

> Al final del siglo XIX y principios del siglo XX empezó a desarrollarse otra visión. La propiedad colectiva se fue transformando en una propiedad pública bajo el control del Estado y las autoridades locales.
>
> - Al final del siglo XIX emergió un mundo totalmente nuevo
> debido a los aportes de la segunda revolución industrial, la
> aparición de la “gran empresa” bajo el modelo alemán, el
> desarrollo de redes técnicas – las vías férreas, la electricidad
> y el teléfono – mientras terminaba el primer período de
> globalización y se afirmaban las grandes potencias que se
> dividieron el mundo. En este contexto, la socialdemocracia
> y el movimiento comunista desarrollaron una visión del
> socialismo orientada a continuar con el avance de estas redes
> técnicas y de la gran industria bajo un Estado planificador.
> - En la misma época, las aspiraciones populares y las necesidades
> de una industria moderna convergieron para desarrollar un
> servicio público de educación gratuita y obligatoria, y sistemas
> de protección social para cubrir riesgos, enfermedades,
> accidentes de trabajo y la jubilación. Es entonces que apareció
> la idea de que los derechos universales no se limiten a los
> derechos democráticos tal y como estaban enunciados en la
> declaración de los Derechos del Hombre y el Ciudadano, de
> la revolución francesa de 1789, sino que incluyan “derechos
> positivos” como los derechos sociales y económicos (derecho
> a la educación, a la vivienda, etc.) que serían reconocidos, en
> 1948, por la Declaración Universal de los Derechos Humanos
> de las Naciones Unidas.
> (p. 92)

> En este contexto, los comunes del siglo XIX, heredados de las sociedades feudales y de las cooperativas obreras de producción, entraron en declive por dos cuestiones: por un lado, no correspondieron a los criterios de progreso y eficiencia que hicierion posible la gran empresa y la planificación estatal; y por otro lado, no permitieron pensar en derechos sociales de carácter universal.
> (p. 92-93)

## O Antropoceno e o retorno dos comuns

> Pasó casi un siglo para que la cuestión de los comunes regrese al centro de la discusión a través del movimiento altermundialista y de círculos académicos. Varias razones explican el regreso de los comunes: el balance negativo de experiencias de administración económica por el estado y el sector privado; el rechazo a las privatizaciones; la crisis de la idea de progreso como fue concebida a principios del siglo XX (inclusive por los estados socialistas que desarrollaron visiones industrialistas); y, finalmente, la aparición de nuevas categorías de comunes, como el conocimiento y la naturaleza. Los comunes de la naturaleza, que abarcan el clima, los océanos, la atmosfera y otros componentes del sistema de la Tierra, están amenazados por las actividades humanas en el actual período histórico, que se denomina el Antropoceno.
> (p. 93)

## Comuns e política: democracia direta, ecofeminismo, crise do clima, ...

> A la luz de la experiencia histórica, el interés más importante en la defensa y expansión del dominio de los comunes es que se constituyen en uno de los enfoques más favorables para ejercer el cuidado de lo común, a través de una real propiedad social y una democracia directa, que no se reduzca a un momento de lucha o de experiencia revolucionaria; o de una relación vital, no necesariamente de propiedad, sino de interdependencias y eco-dependencias, como lo sugieren las ecofeministas considerando los aportes de las reflexiones contemporáneas sobre la crisis del clima, la alimentación o el agua.
> (p. 94)

> Los comunes requieren de un involucramiento participativo y no solamente de la transformación de las estructuras de poder político. En esa medida, los comunes son un mecanismo para empezar a ejercer, en la práctica, alternativas de sociedad.
> (p. 94)

## Socialistas utópicos, cooperativas e os comuns

> Así, los comunes permiten restablecer una tradición del socialismo del siglo XIX, de Owen a Fourier, que coloca en el centro del proceso de emancipación las prácticas sociales a nivel de la educación, las cooperativas, la vida comunitaria, las relaciones entre hombres y mujeres, etc.
(p. 94)

## Internet como bem comum

Será mesmo? O que dizer sobre a estrutura hierarquica  da mesma (descentralizada, mas não distribuida)?

> Ciertas características permiten definir al internet como un bien común de la humanidad que es administrado por una comunidad técnica capaz de auto gestionarse y donde, al no ser manejada por los Estados, se generan constantemente diversos conflictos y debates. Estos conflictos se experimentan de dos maneras: 1) la intención de controlar el internet a través de mecanismos nacionales creados en nombre de la defensa de la propiedad intelectual, la lucha contra el terrorismo y la pedofilia, y 2) la propuesta de establecer un sistema interestatal que se encargue de gestionarlo. Estas cuestiones son centrales en las movilizaciones que se desarrollan en varios países del mundo contra el ACTA (del inglés Anti-Counterfeiting Trade Agreement, en español Acuerdo comercial anti-falsificación) y explican el surgimiento de nuevas corrientes políticas, como los partidos piratas, o nuevos movimientos sociales como el “Students for a Free Culture” (Estudiantes por una cultura libre) que tuvo un impacto importante en Estados Unidos, entre 2007 y 2010.
> (p. 95)

## Realidades híbridas (público-privado-comum)

> Las relaciones sociales que están en el centro de los comunes son contrarias a la lógica capitalista y a la gestión pública-estatal. Sin embargo, en la realidad existen una serie de interrelaciones y realidades híbridas porque los comunes no pueden escapar a la influencia de su entorno. Todo indica que el capitalismo no se hubiera podido desarrollar sin el Estado moderno. Esta interdependencia conlleva una influencia reciproca sobre los métodos de gestión, la organización del trabajo, la construcción de las instituciones en varios campos, la investigación, la educación, la innovación, la protección social, la gestión de mercados, etc.
> (p. 96)

## Cooperativas com estrutura capitalista

> En el ámbito de los comunes, una de las primeras transformaciones fue la adopción, por parte de cooperativas y mutuales, de estructuras muy similares a las de grandes grupos capitalistas. Hoy en día, las cooperativas agrícolas y los bancos mutualistas se encuentran a la vanguardia de ese proceso de transformación.
> (p. 96-97)

> Al mismo tiempo, estas cooperativas agrícolas adoptan cada vez más prácticas agroindustriales y formas de gestión muy parecidas a las multinacionales. El sector bancario vive un proceso similar. La diferencia entre los bancos mutualistas y los bancos privados es cada vez menor. Estas transformaciones se explican por tres elementos: primero, la globalización neoliberal que empuja a las cooperativas a asumir métodos de gestión que les permitan hacer frente a la competencia internacional; segundo, el distanciamiento de los cooperativistas “de base” que cada vez se involucran menos en la gestión y cuidado de sus cooperativas; y, finalmente, la exacerbada autonomía de los dirigentes de las cooperativas que promueven el distanciamiento de las bases y aceleran la integración de sus cooperativas con el mundo y las dinámicas de las multinacionales capitalistas.
> (p. 97)

## Internet, redes sociais, uberização (gig economy)

> Sin embargo, el ámbito digital de los comunes permite a grandes y poderosos actores beneficiarse de los efectos de la red y apuntar hacia un posible monopolio. Es el caso, entre otros, de las redes sociales como Facebook o Twitter, de las herramientas de trabajo de google y las plataformas colaborativas como Uber, Airbnb o Blablacar.
> (p. 97)

> Frente a estos tipos de “privatización”, dos formas de reacción se están desarrollando. La primera es apuntar a quienes trabajan en estas plataformas para que gocen de los mismos derechos de todo trabajador. Ese es el caso de Estados Unidos, donde hay iniciativas para que a los choferes de Uber se les otorgue el status de asalariados y así puedan acceder a los beneficios sociales que conlleva dicho estatus. La otra reacción, proveniente del mundo de los comunes, busca desarrollar alternativas “libres” de estos grandes consorcios de la economía digital: programas libres, plataformas realmente colaborativas basadas en la cultura del intercambio y sin ánimo de lucro, etc.
> (p. 98)

## Inserção da lógica dos comuns no público e privado

> Para terminar con la complejidad de los procesos de los bienes comunes y la hibridación, es importante tomar en cuenta el surgimiento y el desarrollo de una lógica de los comunes dentro los servicios públicos, en las instituciones y en las grandes empresas. Si la presencia de los padres de familia para el funcionamiento de los establecimientos escolares es considerada “antigua” y la intervención de los enfermos en los servicios de salud apareció con la epidemia del SIDA en los años ochenta; hoy, esta intervención, es facilitada por las herramientas digitales que permiten a los pacientes interconectarse, incidir sobre su tratamiento y acceder a medicamentos. Lo digital facilita también las iniciativas de control de las instituciones y de las empresas, gracias al accionar de colectivos de ciudadanos que filtran o sistematizan la información y hacen posible la publicación de información en blogs o páginas web.
> (p. 98)

## A prática do "cuidado" exige participação

> La práctica del “cuidado”, que está inmersa en los comunes, quiere decir implicarse, involucrarse y, por consecuencia, estar estrechamente vinculado a la gestión de los comunes. Las formas que adquiera esa intervención pueden ser muy variadas. Los casos de los grandes comunes como Wikipedia o del internet en sí mismo, son particularmente interesantes porque tienen funcionamientos similares a algunos movimientos de reciente aparición como los “Indignados” u “Occupy” que se basan en tres principios: participan quienes lo deseen, las decisiones se adoptan por consenso, y, los acuerdos alcanzados son socializadas al nivel local más amplio posible.
> (p. 98-99)

De forma contraditória ao que parece, participação não implica necessariamente transparência e debate político? Ao meu ver, me parece que a democracia direta participativa seria o mais próximo de uma "democracia real" que fosse efetiva, transparente e com amplo debate político. Um problema que surge é uma hierarquia entre quem está mais inserido ou menos na "participação". Como resolver isso? Sinto que a resposta não pode ser uma estrutura fixa, mas um movimento de constante atividade em busca do democrático, do político, do transparente, etc.

> Esta forma de funcionamiento, sin embargo, presenta ciertos problemas porque tiende a dejar de lado el debate político y no transparenta realmente los procesos de toma decisión que ocurren fuera de las grandes asambleas. Esto nos plantea una cuestión central para la gestión de comunes cada vez más amplios: ¿qué entendemos realmente por “democracia real”? ¿Cuáles son sus elementos constitutivos? ¿Cómo profundizar esa democracia real para que no acabe siendo cooptada o distorsionada por formas político partidarias o estatales?
> (p. 99)

> No sólo se trata de la democracia al interior de los comunes sino en relación al Estado. ¿Cuál debe ser la postura de los comunes frente al Estado? ¿Qué tipo de transformaciones deben producirse en el Estado desde la perspectiva de los comunes? ¿Es posible “comunizar” el Estado o por el contrario el más grande aporte de los comunes es crear una suerte de contrapoder al Estado preservando siempre un tipo de autonomía de los comunes frente al poder estatal? ¿Cómo combinar ambas estrategias de transformación radical del Estado y, al mismo tiempo, de construcción de un contrapoder de los comunes al Estado?
> (p. 99)

## Que visão de modernidade, prosperidade e futuro têm os comuns?

> Así mismo, es fundamental reflexionar sobre cuál es la visión de prosperidad, modernidad o futuro que tienen los comunes. En la actualidad toda forma de gestión privada, estatal y comunitaria responde a una cierta dinámica que implica un ahora y un mañana. ¿A dónde se quiere llegar con los comunes? ¿Hay una visión crítica compartida en el movimiento de los comunes sobre el desarrollo, el progreso, el productivismo y la modernidad? ¿No es este un tema esencial para el potenciamiento de los comunes en el siglo XXI?
> (p. 100)

## Qual a relação entre os comuns e a natureza?

> Por último, es fundamental reflexionar sobre la relación de los comunes con la naturaleza. Es decir, cómo se construye y se alienta la promoción y necesidad de comunes no antropocéntricos en el siglo XXI. Los comunes más antiguos practicados por pueblos indígenas no eran antropocéntricos. Estas relaciones eco-sociales, en un momento de crisis planetaria, son más necesarias que nunca, por lo que tienen que darse a una escala jamás antes vista. ¿Qué formas de gestión de los comunes son las más apropiadas en relación al clima, los océanos y los glaciares? Hasta ahora se han ensayado iniciativas inter-estatales que han sido poco efectivas. ¿Cómo construir formas de gestión para estos comunes de carácter planetario? ¿Cómo generar una conciencia global que realmente asuma el desafío de nuestro tiempo de “cuidar” la Tierra?
> (p. 100)
