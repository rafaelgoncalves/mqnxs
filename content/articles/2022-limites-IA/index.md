---
title: Sobre os limites da inteligência artificial
date: 2022-10-14
path: "/limites-IA"
category: fichamento
author: Rafael Gonçalves
featuredImage: "./cover.png"
srcInfo: Detalhe da capa do livro Tecnodiversidade
tags:
 - inteligência artificial
 - Yuk Hui
 - tecnodiversidade
 - cibernética
 - recursividade

published: true
---

Fichamento do texto "Sobre os limites da inteligência artificial"[^1] de Yuk Hui.

[^1]: HUI, Y. Os limites da inteligência artificial. Em: **Tecnodiversidade**. São Paulo: Ubu Editora, 2020. 

# Artificialidade da IA ~ esquematização

> A artificialidade da inteligência consiste basicamente em matéria esquematizada. Mas esse tipo de inteligência tende a se libertar das restrições inerentes à matéria a partir de uma reação cuja finalidade é essa mesma esquematização.
> (p. 159)

# Elán vital (Bergson) como força criativa

> O _élan_ vital não é algo que possa ser objetificado e reificado; ele consiste em uma força criativa que possibilita a exteriorização da inteligência na forma de ferramentas e a interiorização de ferramentas na forma de órgãos.
> (p. 160)

# Capacidade de mutação mais rápida na inteligência artificial que na humana

> Graças à invenção de ferramentas, a inteligência permite a complexificação do organismo por meio do acréscimo de órgãos exteriorizados. De outro lado, a inteligência que é exteriorizada por um processo de geometrização (isto é, uma forma estrita de esquematização), mesmo que ainda temporariamente confinada às propriedades físicas, também libera a si mesma da matéria, ou, de modo mais preciso, é capaz de realizar sua transferência de um material para outro – uma forma moderna de transubstanciação. **Esse processo confere à inteligência artificial a capacidade de produzir mutações mais rápidas e mais amplas do que aquelas ligadas à inteligência humana – um fato também reconhecido por Bergson**.
> (p. 160)

# Paradoxo da inteligência: consciência infeliz como incapacidade de incorporar as exteriorizações da inteligência

> Podemos encontrar aqui um paradoxo da inteligência. À medida que a inteligência se exterioriza de modo constante para que possa interiorizar seus produtos – como aquilo que Hegel chama de “astúcia da razão” –, pode acontecer, em um dado momento, que ocorra uma falha na reintegração da exteriorização, o que leva a inteligência a se sentir ameaçada por seus produtos e a se subordinar a eles – daí a “consciência infeliz” que hoje testemunhamos no pânico do desemprego em massa e da derrocada do elemento humano, que, por sua vez, levam ao ressurgimento de políticas reacionárias.
> (p. 160-1)

# Limites da inteligência artificial tem mais a ver com sua limitação a um paradigma específico de inteligência que suas limitações e capacidades técnicas

> Falar sobre os limites da inteligência artificial não significa mostrar quais são os pontos fracos da inteligência da máquina, o que ela não poderia, não pode e nunca poderá fazer. As máquinas são parte do processo evolutivo da espécie humana. São um dos aspectos da evolução que os seres humanos foram capazes de controlar, mas do qual estão gradualmente perdendo o controle. **Mostrar os limites da inteligência artificial não fará com que as máquinas possam ser controladas de novo, mas é algo que libertará a inteligência das máquinas dos vieses de certas ideias de inteligência** – e, desse modo, possibilitará a concepção de novas ecologias políticas e de economias políticas da inteligência das máquinas. Para que possamos fazê-lo, teremos de entender a história da inteligência das máquinas e aquilo que motiva e impede sua evolução.
> (p. 161)

# Imbricamento entre realidade técncia e sociedade humana. Recirprocidade entre matéria e espírito.

> Um erro comum está em não entender que a realidade técnica – termo usado por Gilbert Simondon – também inscreve em si a realidade humana, e isso não só porque a tecnologia é a concretização de esquemas mentais influenciados por estruturas sociais e políticas contidas na sociedade humana, mas também porque ambas são transformadas pela realidade técnica. **A sociedade humana é transformada por invenções técnicas, e essa transformação sempre vai além da intenção original dos esquemas mentais**. Matéria e espírito formam uma relação recíproca – não há materialismo sem espírito, assim como não há espiritualismo sem matéria –, e qualquer falha no reconhecimento dessa reciprocidade só nos levará à autoderrota.
> (p. 161-162)

# Foco do texto: mudança da inteligência maquínica de inferências mecânicas lineares para operações digitais recursivas

> A ruptura para a qual queremos voltar nossa atenção é a mudança da inteligência das máquinas de uma inferência mecânica linear para uma operação digital recursiva [[ entendida provisoriamente como reflexividade ]].
> (p. 162)

# Evolução das máquinas do cartesianismo ao organicismo

> Em _Recursivity and Contingency_ [Recursividade e contingência], buscamos recontar a história da evolução da inteligência das máquinas como uma transição do cartesianismo para o organicismo.
> (p. 162)

# Recursividade como nova causalidade surgida no século XX. Questiona a linearidade cartesiana e a dualidade mecanicismo/organicismo.

> O surgimento e a elaboração de uma nova causalidade no século XX – a que chamamos recursividade – fornecem uma base ampla para uma série de novas ideias cujo funcionamento se dá a partir de formas de raciocínio não lineares, entre as quais constam a cibernética, a teoria dos sistemas, a teoria da complexidade e a ecologia.
> (p. 163)

> Essa forma não linear de raciocínio liberta a inteligência das máquinas do confinamento à causalidade linear do cartesianismo e desafia a dualidade que dá sustentação às críticas formuladas desde o século XVIII – mais precisamente, a dualidade das diferenças irredutíveis entre mecanicismo e organicismo.
> (p. 163)

# Papel da mecânica estatística na cibernética

> 3: É relevante analisar como a mecânica estatística foi essencial para a cibernética de Wiener, que utiliza o método estatístico para aproximar o tempo entrópico irreversível e o tempo newtoniano reversível – o que mencionamos de passagem em “Máquina e ecologia”. Cf. também Suzanne Guerlac, _Thinking in Time: An Introduction to Henri Bergson_. Ithaca, NY: Cornell University Press, 2006, p. 32: “Alguns anos mais tarde (1876), Josef Loschmidt apresenta a seguinte questão a Boltzmann: se a entropia é um processo irreversível (um material frio não se aquece de maneira espontânea ao longo do tempo), como é possível derivá-la de um modelo que corresponde a leis reversíveis? Boltzmann respondeu a esse desafio com um artigo publicado em 1877 em que caracterizava a entropia em termos de probabilidades matemáticas por meio de uma análise estatística. Esse é o começo daquilo que viria a se tornar o campo da mecânica estatística. Quando traduziu a lei da entropia em termos de mecânica clássica, e então adaptou o que daí resultou à análise estatística, Boltzmann embaralhou as implicações da segunda lei da termodinâmica quanto à realidade do tempo fisiológico a que Bergson chama ‘duração real’”.
> (p. 163-4)

# A cibernética não teria superado a dualidade mecanicismo/vitalismo, mas sim assimilado a operação dos organismos (no mecanicismo?)

> O organicismo ainda está preso a leis inexoráveis, como no caso de uma parte condicionada por outras partes e pelo todo; o vitalismo enfatiza o livre-arbítrio, que resiste de modo constante à tendência em direção a uma sociedade fechada. Desse modo, não podemos afirmar que Wiener tenha superado inteiramente a dualidade entre mecanicismo e vitalismo – podemos afirmar, no máximo, que Wiener descobriu uma operação técnica capaz de assimilar o comportamento dos organismos.
> (p. 164-5)

# Cibernética (McCulloch e Pitts) como ponto de partida da IA

> Somos frequentemente lembrados dos trabalhos de Warren McCulloch e Walter Pitts – pesquisadores que participaram das Conferências Macy7 – quando nos referimos a redes neurais artificiais. McCulloch e Pitts apresentaram o primeiro modelo de uma rede neural baseada em princípios cibernéticos. Para eles, a atividade cerebral poderia ser vista como uma operação lógica realizada por neurônios. **Neurônios são operadores lógicos e de memória que atualizam seus estados individuais e o resultado geral de modo recursivo**. Em resumo, a cibernética também é o nosso ponto de partida, assim como o ponto de vista sob o qual poderemos analisar nossa situação atual.
> (p. 165)

# Origem da IA nas conferências Macy (cibernética) x conferência Dartmouth (IA simbólica)

> Esta interpretação da história é aparentemente diferente da narrativa clássica sobre a origem do termo “inteligência artificial”, que dizem ter sido criado durante a Conferência de Dartmouth de 1956, associado a cientistas e pesquisadores como Marvin Minsky, John McCarthy e Claude Shannon, entre outros. Essa narrativa clássica continua com o avanço no desenvolvimento de uma inteligência artificial simbólica fraca (ou o que John Haugeland chama de “boa e velha inteligência artificial”) para uma mais forte e, por fim, leva à fantasia dos dias de hoje de uma superinteligência.
> (p. 166)

# Juízo reflexionante em Kant ~ feedback na cibernética

> Esse ponto de vista nos deixa em grande medida indiferentes à ruptura epistemológica causada entre organicismo e mecanicismo que já havia sido antecipada por Immanuel Kant em _Crítica à faculdade do julgar_ e que acabou concretizada na cibernética. Nesse texto seminal, Kant desenvolveu aquilo a que ele se refere como o “juízo reflexionante” com o objetivo de descrever uma operação que não segue regras preestabelecidas. Diferentemente do “juízo determinativo”, que aplica o universal ao particular, o “juízo reflexionante” começa com o particular para chegar ao universal a partir da heurística do princípio regulativo – isto é, aquele que deriva suas próprias regras durante o avanço em direção a uma _finalidade_.
> (p. 166)

> A ideia mais genial de Kant é a de que o belo é _subjetivo-objetivo_, no sentido de que sua objetividade poderia ser alcançada por meio do sujeito, ainda que não de maneira demonstrável. Será apenas pelo juízo reflexionante que poderemos chegar à ideia universal e necessária do belo. É por essa razão que, no artigo “Epistémologie et cybernétique”, Gilbert Simondon defende que foi apenas na _Crítica à faculdade do julgar_ que Kant conseguiu lidar com a cibernética. Isso porque o juízo reflexionante, cuja teleologia não precisa ser fatalista, mostra certa afinidade com a noção de _feedback_.
> (p. 166-7)

# Causalidade circular nas máquinas ~ alma

> A cibernética com máquinas digitais recursivas caracteriza um ponto de inflexão na história em que a inteligência das máquinas supera o estereótipo do autônomo sem alma descrito por Descartes. A causalidade circular implementada nas máquinas parece sugerir um movimento análogo ao da alma: a alma é aquilo que se volta a si mesmo a fim de se autodeterminar. 
> (p. 169)

# Crítica de Dreyfus a Minsky

> Quando publicou sua série de textos sobre os limites da inteligência artificial nos anos 1970 – e mais notadamente o livro de 1972 _What Computers Cannot Do? A Critique of Artificial Reason_ [O que computadores não conseguem fazer? Uma crítica da razão artificial] –, Hubert Dreyfus acusou os cientistas de inteligência artificial, especialmente Marvin Minsky, de reduzir a cognição a uma “estrutura particular, a uma estrutura de conhecimento ou a uma estrutura-modelo”.
> (p. 171)

# Impasse da IA como impasse da metafísica ocidental

> Do ponto de vista da lógica, a crítica da “razão artificial” de Dreyfus poderia ser interpretada como uma crítica ao uso de um pensamento linear e mecanicista em detrimento de um pensamento recursivo e orgânico para a modelagem da cognição.
> Assim, Dreyfus chega à conclusão de que o impasse da inteligência artificial é também o impasse da metafísica ocidental; em oposição, o pensamento heideggeriano – uma tentativa de ir além da metafísica – forneceria uma alternativa, isto é, pode-se formular uma inteligência artificial heideggeriana.
> (p. 172)

# O mundo como o outro da cognição em Heidegger

> Desde o princípio, o mundo que Heidegger descreve é o Outro da cognição, e não pode ser a ela reduzido, já que a cognição só é possível graças ao mundo. O mundo e o conteúdo cognitivo podem ser vistos em termos da relação entre figura e fundo da _gestalt_. O mundo é constituído por uma totalidade complexa de referências, e a cognição depende delas para raciocinar. Ou, em outras palavras, **a cognição é parte do mundo, uma parte do todo**.
> (p. 173)

# Digitização e digitalização

> Contudo – e isso também será a chave para a reinterpretação dos parágrafos 17 e 18 de _Ser e tempo_ –, o mundo já não é mais o mundo fenomenológico que Heidegger descrevia. **Cada vez mais o mundo é capturado e reconstruído por dispositivos móveis e sensores. Esse é o processo da digitização e da digitalização**. Uma grande parte do mundo está encerrada em telas, ainda mais se considerarmos que nos dias de hoje é possível fazer praticamente tudo com aplicativos para telefone celular.
> (p. 173)

# Computação como redução do mundo

> Quando o mundo se torna, por assim dizer, um sistema técnico, o mundo que Heidegger descrevia como o solo para o firmamento da verdade – no sentido de _aletheia_ – é reduzido a conjuntos de dados que podem ser analisados a partir de parâmetros lógicos e calculados de forma aritmética – _mathesis universalis_. **O mundo perde o caráter incalculável; em outras palavras, deixa de ser fundacional para a epistemologia baseada na computação**. É também por isso que hoje pensamos que a inteligência artificial está se tornando cada vez mais poderosa e que a questão do mundo enfatizada tanto por Heidegger quanto por Dreyfus é cada vez menos importante – porque estamos vivendo em um mundo digitalizado, um mundo do _Gestell_. **O poder da inteligência artificial se baseia na redução do mundo a modelos computacionais**. O que estou dizendo parece ser uma crítica típica ao reducionismo; a história, no entanto, não é tão simples. O reducionismo não é necessariamente ruim – mas ele é ruim quando é considerado como a realidade toda, como acontecia no erro do mecanicismo cartesiano.
> (p. 174)

# Organologia como relação macanicismo-vitalismo

> A dualidade que Bergson estabelece entre o mecanicismo e o vitalismo – assim como outros pares dialéticos, como duração e espaço, matéria e espírito, ciência e metafísica –, que passa ao leitor a impressão errada de que o filósofo francês é um dualista, não é exatamente uma ruptura opositiva; **o que mais interessa a Bergson é a relação entre duas ideias**, ou, como ele escreveu, “prolongando-se duas delas até o ponto em que se cortem, chegar-se-á no entanto à própria verdade. O agrimensor mede a distância de um ponto inacessível visando-o alternadamente de dois pontos a que tem acesso. Achamos que esse método de verificação progressiva é o único que possa fazer avançar definitivamente a metafísica”. **Daí Gilles Deleuze escrever em Bergsonismo que “o dualismo, portanto, é apenas um momento que deve terminar na reformação de um monismo”**. Se “monismo” é ou não um termo apropriado, isso é outra questão, mas, para nossos propósitos aqui, **chamaremos essa relação entre dois polos de relação organológica**.
> (p. 176)

# Catástrofes como consequência da ilusão transcendental do cálculo

> O _élan_ vital é a própria vida, e o mecanicismo precisa reconhecer suas origens e retornar à vida. Essa também é a razão pela qual Bergson sugere que “a mecânica exigiria uma mística”. O mecanicismo buscava explicar a vida sem vida, enquanto Bergson procura levá-lo de volta para uma base ainda mais primordial – e, ao fazê-lo, as dualidades por ele estabelecidas são deixadas para trás. Uma volta ao _élan_ vital e ao mundo não é uma repetição do que Bergson e Heidegger já disseram; é reposicionar as tecnologias em realidades mais amplas para além do mundo calculável. **A forma como falamos de progresso desde o século XVIII é dominada pelo desejo de medir, calcular e dominar. No entanto, também testemunhamos catástrofes concebidas como formas de resistência da natureza ou da Terra. Essas catástrofes não resultaram de erros de cálculo, mas decorreram fundamentalmente da “ilusão transcendental” do cálculo**. O termo “não racional” que evocamos em “Variedades da experiência da arte” é um convite para pensar além do computável e, por isso, também para entender como o não racional considerado incalculável é articulado na arte e poderia fornecer um método para refletir sobre outras possibilidades de computação.
> (p. 177)

# 'Inteligência como computação' como uma definição entre outras

> Gostaria de voltar à primeira epígrafe deste texto, encontrada no artigo seminal de Marvin Minsky, “Steps Toward Artificial Intelligence” [Passos rumo à inteligência artificial], de 1961, em que o autor comenta sua abordagem da inteligência: “é claro que não existe uma teoria da ‘inteligência’ amplamente aceita; esta é nossa análise e pode ser que ela seja polêmica”. Em vez de afirmar que na verdade não sabemos exatamente o que é inteligência, **prefiro entender que Minsky nos convidava abertamente a problematizar e até mesmo a reinventar o conceito de inteligência**. Inteligência, na medida em que se refere a coisas concretizáveis por meio de aparatos digitais, quer dizer algo computável. O que significa ser computável, entretanto? Significa enumerável de forma recursiva. **O que é recursivamente enumerável se refere a apenas um tipo de inteligência dentre vários outros**. Ou, mais precisamente, se refere a apenas uma das tendências da inteligência, na linguagem de Bergson.
> (p. 178)

# Computação ~ maximização de decisões racionais (Leroi-Gourhan) ~ racionalidade geométrica (Bergson)

> Trata-se, ainda assim, de uma tendência técnica no sentido dado por André Leroi-Gourhan, já que baseada em princípios voltados à maximização de decisões racionais e à minimização de influência contingentes. Essa tendência técnica segue e é motivada por uma racionalidade geométrica que, para Bergson, também pode ser um obstáculo que a vida deve superar, pois capaz de tornar a inteligência alheia a si mesma e de promover o esquecimento de suas fundações.
> (p. 178)

# Superinteligência como expressão de um computacionalismo

> Hoje é necessário que contestemos a fantasia de ter uma superinteligência que será finalmente superior a todas as outras formas de inteligência e que um dia tomará o lugar do Estado. **A fantasia de uma superinteligência é a expressão de uma forma extrema de computacionalismo, de acordo com a qual o mundo é calculável e poderia ser esgotado através de operações matemáticas – e se revela, também, como a forma mais elevada de neutralização e despolitização via tecnologia**, como já analisado por Carl Schmitt.
> (p. 178-9)

# Definição antropocêntrica (~simbólico) de inteligência

> Plantas e bolores limosos podem nos permitir descobertas sobre os princípios organísmicos e, dessa forma, nos dar inspiração para o aprimoramento de algoritmos (como no caso da “computação natural”, um ramo da ciência da computação). **Ao utilizá-las, no entanto, subordinamos essas formas de vida à calculabilidade**. De outro lado, e no que concerne à inteligência humana – e aqui acompanhamos Bergson mais uma vez na afirmação de que a força dessa inteligência vem de sua capacidade de inventar ferramentas e símbolos inorgânicos –, o ser humano é em primeiro lugar um _animal symbolicum_, no sentido dado por Ernst Cassirer, ou um ser técnico, de acordo com Bernard Stiegler. **Talvez plantas e animais não sejam menos “inteligentes” que seres humanos, mas o uso que eles fazem de símbolos é bem menor que o dos seres humanos**. A inteligência assim definida está ligada ao mesmo processo de evolução como hominização.
> (p. 179)

# Pensamento chinês como exemplo de inteligência que engloba o incalculável

> Queremos usar o pensamento chinês como exemplo, mas apenas para investigar a noção de inteligência e ver como o _incalculável_ funciona nessa inteligência – o que é diferente do conceito de mundo de Heidegger ou do _élan_ vital de Bergson. Queremos enfatizar que tratamos apenas de um exemplo, já que a China e a Europa são somente frações dessa diversidade. Em chinês, “inteligência” é muitas vezes traduzida como _zhi hui_ (智慧) ou _zhi neng_ (智能) – o sentido literal do primeiro termo corresponde à “sabedoria”, enquanto o do segundo corresponde a “ser inteligente”, “ter a capacidade de racionar” ou “tornar-se sábio”. Sabemos que inteligência não significa sabedoria, já que sabedoria é algo muitas vezes atribuído a pensadores orientais – que não têm a Filosofia! Queremos perguntar o que realmente quer dizer “inteligência” no pensamento chinês.
> (p. 180)

# Mou Tsung-San sobre o papel da intuição intelectual (Kant) no pensamento chinês

> Quando leu _Crítica da razão pura_ de Kant, o filósofo novo confucionista Mou Tsung-San (1909–1995) ficou atônito e, ao mesmo tempo, sentiu-se iluminado, já que acreditava que a razão especulativa que Kant pretendia limitar era exatamente o que a filosofia chinesa busca cultivar. Em seu ambicioso livro _A intuição intelectual e a filosofia chinesa_, Mou tenta mostrar que, caso sigamos as definições sistêmicas de Kant para as operações e para os limites das faculdades do espírito que fundamentaram o conhecimento científico, então provavelmente perceberemos que a intuição intelectual que é excluída da ciência tem um papel central no pensamento chinês.
> (p. 180-1)

# Objetividade em Kant

> Uma vez que pretende ser válido segundo critérios objetivos, o conhecimento deve ter como base a cognição sensorial (os fenômenos). É óbvio que a especulação para além do fenômeno é sempre possível, já que esse ato em si está ligado à liberdade humana e que as pessoas podem sonhar; no entanto, um conhecimento desse tipo não passa de especulação, uma vez que não pode ser fundamentado e, por isso, está excluído do conhecimento científico. O númeno é, portanto, negativo, e um sentido positivo só será possível quando houver uma intuição intelectual que corresponda à sua apreensão. Kant rejeitava a possibilidade de seres humanos possuírem intuição intelectual já em sua primeira _Crítica_; em vez dela, ele insistia que os seres humanos só poderiam ter intuição sensorial. A intuição sensorial é o solo em que a razão trabalha e para além do qual poderia acabar se afogando no oceano.
> (p. 181)

# Intuição intelectual como elemento proeminente no confuncionismo, taoísmo e budismo para Mou

> **Segundo Mou, o elemento proeminente na síntese do confucionismo, do taoismo e do budismo – aquilo a que hoje chamamos pensamento chinês – é o cultivo de uma intuição intelectual que se insere para além do fenômeno e que o reúne ao númeno**. Para Mou, a intuição intelectual não é inata. Quando nascemos, não necessariamente a possuímos, ainda que tenhamos intuição sensorial. Essa é também a diferença entre Mou Tsung-San e Schelling (e também Fichte), já que essa intuição intelectual precisa ser desenvolvida e não se caracteriza como algo que já nos é dado desde o começo e que lança as bases da sistematização do conhecimento. Assim, a intuição intelectual de Mou não é exclusivamente nem _a priori_ nem _a posteriori_: ela não é exclusivamente _a priori_ porque difere da intuição sensorial oferecida a toda nossa espécie; também não é exclusivamente _a posteriori_ porque não se desenvolve inteiramente a partir da experiência, já que o seu exercício é o que diferencia os humanos de outros animais. As figuras ideais como o sábio no confucionismo, o zhengren (“a pessoa verdadeira”, literalmente) no taoismo e Buda no budismo representam pessoas que cultivaram a intuição intelectual.
> (p. 182)

# Intuição intelectual e cosmotécnica

> Mas o que exatamente é essa intuição intelectual e como podemos fazê-la funcionar a partir da leitura que Tsung-San faz do pensamento chinês? Corremos o risco de simplificar o raciocínio com a seguinte afirmação: **a intuição intelectual é a razão sintética que entende a relação entre o eu e outros seres (ou o cosmos) a partir da perspectiva de um sujeito moral, e não de um sujeito de conhecimento**. O sujeito moral e o sujeito de conhecimento são duas tendências do desenvolvimento humano. O sujeito moral é anterior ao sujeito de conhecimento. **Quando um sujeito de conhecimento olha para o mundo, ele procura compreendê-lo a partir de uma decomposição analítica; o sujeito moral, por sua vez, enxerga a intercorrelação das coisas a partir de uma razão sintética que sempre busca a unificação das ordens cósmica e moral** – e esse processo também é a base fundacional do conceito de cosmotécnica.
> (p. 182-3)

# Intuição intelectual e pensamento oriental

> Mas o que isso significa de fato? Em _A intuição intelectual e a filosofia chinesa_, e também em seu trabalho de maior maturidade, _O fenômeno e a coisa em si_, Mou Tsung-San tentou mostrar que a intuição intelectual é essencial ao confucionismo, ao taoismo e ao budismo. **Para Mou, a intuição intelectual está associada à criação (cosmogonia, por exemplo) e à metafísica moral (em oposição à metafísica dos costumes de Kant, cuja base é a capacidade de entendimento do sujeito)**.
> (p. 183)

> **Para Mou, a capacidade do coração (_xin_) de “conhecer a fronteira do céu” é precisamente a intuição intelectual**: algo que não se refere a um tipo de conhecimento determinado pelas intuições sensoriais e pela compreensão, mas uma iluminação completa que surge do _cheng ming_ do _xin_ moral universal, onipresente e infinito. Nessa iluminação total, os seres aparecem como coisas em si, e não como objetos de conhecimento. 
> (p. 184)


> Ao comentar a passagem seguinte do _Xi Ci_, em que se discute o uso de cascos de tartaruga e de milefólios em práticas divinatórias, Mou Tsung-San afirma:
> 
> > Ainda que o casco de tartaruga ou os milefólios não sejam providos de pensamento, podemos conhecer o mundo inteiro ao trabalhá-los e ao apresentar nossas questões a eles, desde que haja ressonâncias […]. Desse modo, o sentir que se volta ao conhecimento do mundo é como a sensação de todo o cosmos. A ideia de sentir todo o cosmos é expressa de maneira mais sólida no confucionismo pré-Quin – de fato, é a isso que Kant chamava intuição intelectual.28
> 
> Uma abordagem filosófica mais profunda seria necessária nesse ponto, mas o que Mou intui é que há uma forma de conhecimento para além dos fenômenos – e que essa é a origem da moral. E isso porque não podemos basear a moral em análises de dados, ou, de outro modo, teríamos uma axiomatização como a que temos ouvido sobre a ética da tecnologia de hoje. Quando falamos em ética da tecnologia, contudo, já pressupomos um tipo específico de sujeito de conhecimento e de razão e admitimos certa normatividade. Em vez de axiomatizar a moral, precisaremos voltar aos modos de conhecimento diferentes que ainda não foram considerados por engenheiros e acadêmicos que trabalham com a inteligência artificial.
> (p. 185)

# Reflexão final sobre a necessidade de uma noodiversidade (através de uma tecnodiversidade). Tecnodiversidade como descolonização.

> Se Mou Tsung-San estava certo ao dizer que a intuição intelectual, e não a razão analítica, se destaca no cerne do pensamento chinês, então vemos que há aqui uma diferença fundamental em termos de definições de inteligência. Essa diferença contribui para a tecnodiversidade do desenvolvimento tecnológico futuro. Isso não significa que estamos propondo que a inteligência das máquinas terá de desenvolver intuição intelectual, ainda que isso pudesse ser um experimento interessante – já que poderia apontar para uma “singularidade tecnológica” ou para uma “explosão da inteligência” de verdade (imaginem uma máquina que seja capaz de produzir o que ela mesma intui); em vez disso, **esta breve investigação é uma tentativa de mostrar que a inteligência não é limitada pelo cálculo ou pela análise dos fenômenos**; a inteligência, no que se inclui o meio / suporte técnico de que ela depende, deve ser ampliada em dois sentidos. **Em primeiro lugar, precisa ser reposicionada em realidades mais amplas que excedam a pura racionalidade e que considerem o não racional; em segundo lugar, a inteligência deve ser entendida em conjunto com seu suporte simbólico, que não pode ser excluído ou colocado em segundo plano (o que acontece com muita frequência quando se pensa que é possível “extrair” modelos de inteligência de bolores limosos ou de insetos)**. O desafio da inteligência artificial não está na construção de uma superinteligência, mas na facilitação de uma noodiversidade. E, para que a noodiversidade seja possível, precisaremos desenvolver uma tecnodiversidade. Também é assim que a cosmotécnica se diferencia da “virada ontológica” (que enxerga a cultura sob a perspectiva de uma natureza organísmica), já que sustentamos a hipótese de que **precisamos desenvolver com urgência uma tecnodiversidade como orientação para o futuro, como política de decolonização**. Seria, ao mesmo tempo, uma reconstrução das histórias da cosmotécnica que foram eclipsadas pela busca por uma história universal da tecnologia (e também por uma história universal da espécie humana) e um chamado à experimentação nas artes e nas tecnologias para o futuro. Mas, para que esses experimentos sejam possíveis, precisaremos de disciplinas e instituições dedicadas ao estudo da arte, da tecnologia e da filosofia que hoje ainda não temos – e é precisamente por causa dessa ausência que, juntos, devemos ousar pensar e agir.
> (p. 186-7)
