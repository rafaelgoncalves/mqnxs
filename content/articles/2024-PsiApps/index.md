---
title: "PsiApps: autonomia individual e mediação técnica"
tags:
    - Fernanda Bruno
    - Bruno Latour
    - mediação técnica
    - capitalismo
    - neoliberalismo
    - aplicativos
    - saúde mental
    - modulação
    - captura
    - google
    - facebook
    - algoritmos
    - fichamento

path: "/psiapps"
date: 2024-04-24
category: fichamento
featuredImage: "fig6.png"
srcInfo: <a href="https://www.reciis.icict.fiocruz.br/index.php/reciis/article/view/2205/2415">Fluxo de captura</a>
published: true
---

Fichamento do artigo "'Tudo por conta própria'"[^1] de Fernanda Bruno et al.

[^1]: Bruno, Fernanda Glória, Paula Cardoso Pereira, Anna Carolina Franco Bentes, Paulo Faltay, Mariana Antoun, Debora Dantas Pio da Costa, Helena Strecker, e Natássia Salgueiro Rocha. 2021. “‘Tudo por conta própria’: autonomia individual e mediação técnica em aplicativos de autocuidado psicológico”. **Revista Eletrônica de Comunicação, Informação & Inovação em Saúde** 15(1). doi: 10.29397/reciis.v15i1.2205.

# Resumo

> Este  artigo  discute  o  resultado  da  **análise  de  10  aplicativos  móveis  de  autocuidado  psicológico  utilizados  no Brasil, que denominamos PsiApps.** A análise se desdobra em duas camadas: uma ‘visível’, que envolve os discursos dos próprios aplicativos para descrever os problemas que visam solucionar, suas promessas e seus métodos; outra ‘invisível’, que inclui formas automatizadas de coleta e compartilhamento de dados dos  usuários  pelos  aplicativos.  **Veremos  como  a  ênfase  na  individualidade  e  na  autonomia  manifesta  na  primeira  camada  torna  opaca  uma  série  de  mediadores  presentes  na  segunda  camada,  em  grande  parte  invisíveis para o usuário.** O contraste entre a centralidade da agência individual promovida pelos discursos dos PsiApps e o caráter relacional da infraestrutura e do ecossistema de dados que os integram evidenciam as contradições da autonomia ofertada por esses aplicativos

# Economia psíquica dos algoritmos e PsiApps

> Esse  ecossistema  digital  entrelaça,  de  modo  singular,  corporações  de  tecnologia,  ciência  e  sociedade  em  **uma  nova  lógica  que  investe,  cada  vez  mais,  em  processos  algorítmicos  de  captura,  análise  e  uso  de  informações psíquicas e emocionais extraídas de grandes volumes de dados. Tal investimento tecnocientífico, econômico e social compõe os processos de uma Economia Psíquica dos Algoritmos** (BRUNO, 2018; BRU-NO; BENTES; FALTAY, 2019), da qual os aplicativos fazem parte. Interessados em compreender como as informações psicológicas e emocionais vêm sendo coletadas e usadas em aplicativos móveis, nossa análise tem como foco o que designamos de **‘aplicativos de autocuidado psicológico’ou, numa versão abreviada, PsiApps.** Esta designação provém da análise do modo como os próprios _apps_ enunciam o serviço que oferecem, bem como os estados de bem-estar psicológico que dizem promover.
> (p. 35)

# PsiApps

> **Os  PsiApps  abrangem  um  escopo  amplo  e  heterogêneo  de  serviços  e  funcionalidades  relacionadas  à  saúde mental: vão desde técnicas de meditação, passando por controle do sono ou de outras funções emocionais e fisiológicas, até a realização de testes psicológicos e terapia guiada.** Por um lado, esses apps têm o potencial de oferecer aos usuários informações e serviços, possibilitando acesso a dados em tempo real, orientações sobre condições clínicas e incentivo a hábitos benéficos à saúde psíquica, emocional e física. Por outro, **eles ampliam potencialmente a coleta de informações psicológicas e emocionais, bem como as predições que se pretendem extrair dessas coletas que são valiosas não apenas para fins comerciais, mas também para a pesquisa médica, clínica e científica.**
> (p. 35)

# Camada visível: autonomia individual (~neoliberalismo)

> Na primeira dimensão, identificamos que **boa parte dos aplicativos dirige-se a um ‘sujeito ansioso’, ao qual oferecem meios para cuidar de si mesmo, especialmente através de um automonitoramento que promete autoconhecimento.** O discurso identificado nessa camada **reverbera uma série de processos relacionados à racionalidade e à subjetividade neoliberais, que envolvem “a pressuposição de um self autônomo, livre  e  com  poder  de  escolha,  como  um  valor,  um  ideal  e  uma  meta”**  (ROSE,  2011,  p.  211).  Entendemos,  assim, que os PsiApps reproduzem e reforçam essa racionalidade, instigando o usuário ansioso a resolver “tudo por conta própria” (CÍNGULO, 2019) e a tratar o bem-estar emocional como uma questão de “simples escolha”  (CÍNGULO,  2019).  Como  veremos,  essa  oferta  de  autonomia  e  autocuidado  individualizados  e  individualizantes nos apps analisados – que inclui desde ideais de empreendedorismo individual a modelos clínicos e psicológicos de autocuidado, aproximando-se de um treinamento de si – está repleta de mediadores invisíveis (LATOUR, 2012).
> (p. 36)

# Camada invisível: captura (por meio de mediadores - TOCs e trackers)

> Na segunda camada, focalizaremos nesses mediadores invisíveis, evidenciando que o usuário não está apenas consigo mesmo nessa busca pelos estados emocionais prometidos; e tampouco está diante de uma ferramenta neutra. O usuário só está ‘por conta própria’ num sentido muito limitado do termo que, como veremos,  não  se  confunde  com  a  suposta  autonomia  e  o  controle  sobre  si  alegados  e  propagados  pelos  PsiApps. **Incorporados à própria materialidade e infraestrutura desses aplicativos, atuam uma série de mediadores pouco ou nada visíveis para o usuário: recursos das interfaces dos apps, mecanismos de coleta de dados, processos algorítmicos de análise e predição, entre outros.** Usando estratégias metodológicas para a abertura dessas ‘caixas-pretas’ (LATOUR, 2012) que são os apps, exploramos dois níveis dessa segunda camada:  **os  termos  de  uso  e  políticas  de  privacidade,  de  um  lado;  e,  de  outro,  os  rastreadores  (trackers) presentes nos aplicativos, buscando pistas sobre os atores e mediadores presentes no ecossistema de dados dos PsiApps e seus modelos de negócios.**
> (p. 36)

# Contradição entre as duas camadas dos PsiApps

> De um lado, tais _apps_ reverberam  processos  de  subjetivação  neoliberais,  que,  a  partir  de  modelos  psicológicos  e  terapêuticos  específicos, buscam responsabilizar os indivíduos na jornada do aprimoramento ininterrupto de si. De outro,  embora  reproduzam  um  discurso  de  suposta  neutralidade  e  autonomia,  os  PsiApps  contam  para  seu funcionamento com mediadores invisíveis, integrados às dinâmicas da economia digital, cada vez mais interessada em informações psicológicas e emocionais (BRUNO; BENTES; FALTAY, 2019).
> (p. 35)

# Objetivo do artigo: evidenciar e mapear

> Em  uma  perspectiva  teórico-metodológica  que  se  aproxima  dos  App  Studies  (GERLITZ  et  al.,  2019;  DIETER et  al.,  2019),  pretendemos  **evidenciar  e  mapear  a  condição  inerentemente  relacional,  híbrida  e  heterogênea  desses  dispositivos.**  Num  contexto  de  crise  e  de  extrema  vulnerabilidade,  é  fundamental  desvelar as relações inscritas em emergentes práticas de cuidado de si, discutindo e avaliando com cautela as ‘soluções’ ofertadas, uma vez que o acesso aos mediadores tradicionais ficou menos acessível durante a atual pandemia.
> (p. 36)

# Aplicativo (App) -- App e Software Studies

> O  que  tende  a  permanecer  pouco  evidente  é  o  fato  de  que  **aplicativos  são  pacotes de software que continuamente alimentam múltiplas conexões em uma infraestrutura multinível e  multiescala,  composta  por  uma  miríade  de  atores  heterogêneos  –  técnicos,  econômicos,  culturais,  científicos, epistemológicos etc.** Como os emergentes campos dos App Studies (GERLITZ et  al.,  2019;  DIETER et  al.,  2019)  e  dos  Software  Studies  (BRATTON,  2015)  vêm  demonstrando,  **aplicativos  jamais  são objetos autônomos e isolados, mas entidades relacionais, emaranhadas em infraestruturas de dados, modelos econômicos, conjuntos sociotécnicos, que operam em diferentes níveis.**
> (p. 37)

> De um ponto de vista sociotécnico, como viemos destacando, **um aplicativo é uma entidade relacional e  agenciada  de  camadas  de  software  (DIETER  et  al.,  2019;  BRATTON,  2015)  e  montada  em  tempo  de  execução** (GERLITZ et al., 2019), uma vez que tanto o aplicativo como seu usuário estão sempre conectados a uma miríade de objetos, serviços, dispositivos e infraestruturas, sem que esse usuário saiba exatamente quando, como, onde e com quem tais conexões são estabelecidas.
> (p. 45)

# Metodologia: abrir caixas-preta, 10 aplicativos na Google Play Store

(p. 37-8)

![Figura 1 - Aplicativos](./fig1.png)

# Tracker

> **Um _tracker_  é  um  _software_  cuja  tarefa  é  coletar  informações  a  partir  do  uso  de  um  aplicativo.  Ele  pode  informar  ao  desenvolvedor  como  o  usuário  utiliza  o  aplicativo  e  o  dispositivo  móvel,  e  geralmente  é  distribuído  pelas  empresas  no  formato  SDK  (Software  Development  Kit).**  Para  detectar  a  presença  desses rastreadores, utilizamos o serviço εxodus, que se apresenta como uma plataforma de auditoria de privacidade para aplicativos Android e que dá visibilidade à presença de trackers através do rastreamento de seus arquivos ou pacotes de instalação. A identificação e análise dos rastreadores presentes nos PsiApps nos permite, assim, rastrear o fluxo de dados não declarados pelos desenvolvedores em seus termos de uso e políticas de privacidade, assim como os mediadores ocultos e os indícios dos seus modelos de negócios.
> (p. 40)

# Ansiedade como principal "problema" (seguido de estresse, depressão, autoestima, foco e sono)

> **A respeito dos problemas, a ansiedade está no centro desses discursos, aparecendo em nove entre os dez aplicativos analisados, sendo que seis deles possuem a palavra ‘ansiedade’ em seus títulos ou subtítulos.** A ênfase na ansiedade reverbera dados apresentados em relatório da OMS (WHO, 2020), que mostra que o  Brasil  lidera  casos  de  ansiedade  no  mundo.  Entre  os  problemas  mais  frequentes  apresentados  pelos  PsiApps  estão  também  estresse,  depressão,  questões  relacionadas  à  autoestima,  à  atenção  e  ao  foco  e  ao  sono (Figura 2).
> (p. 40)

> Alguns dos apps oferecem, inclusive, suas próprias definições de ansiedade. O Querida Ansiedade afirma que  a  ansiedade  “é  uma  preocupação  exagerada  com  aquilo  que  está  por  vir”  (QUERIDA  ANSIEDADE,  2019). Uma segunda definição é apresentada pelo Monitor de Ansiedade e Humor: “Ansiedade, ânsia ou nervosismo é uma característica biológica do ser humano e animais, que antecede momentos de perigo real ou imaginário, marcada por sensações corporais desagradáveis” (MONITOR DE ANSIEDADE E HUMOR, 2019). **Em comum, os dois pontuam que a ansiedade é uma emoção normal e benéfica, sendo prejudicial, quando  se  torna  muito  recorrente  e  intensa.  Assim,  ambos  oferecem  meios  de  conviver  de  modo  mais  saudável com a ansiedade.**
> (p. 41)

> **Para os PsiApps, em geral, a ansiedade não deve ser eliminada, mas sim administrada com a ajuda de suas ferramentas.** Como enfatiza o Querida Ansiedade (cujo próprio título traz essa dimensão conciliatória), a ansiedade é “um alerta para a busca de uma vida mais autêntica” (QUERIDA ANSIEDADE, 2019). **Nesse sentido, a ansiedade seria o fator pelo qual os sujeitos seriam impulsionados a cuidar, gerir e controlar a si mesmos.**
> (p. 41)

# Ferramentas de autogestão e autocuidado

> Já em relação às ferramentas e aos métodos ofertados para auxiliar os usuários – a segunda categoria de  nossa  análise  da  camada  ‘visível’  –,  **boa  parte  dos  aplicativos  atua  numa  zona  nebulosa  entre  suporte  técnico, apoio clínico e autocuidado.** Essa indefinição é perceptível na descrição de alguns aplicativos: **de um lado, ancoram seus serviços em conhecimentos e técnicas científicas e clinicamente estabelecidas; de outro, ressaltam que não pretendem oferecer serviço psicoterapêutico propriamente dito e que visam sobretudo oferecer meios para que o próprio usuário cuide, monitore e conheça melhor a si mesmo.** Apenas dois dos dez  aplicativos  (Cíngulo  e  Sanvello)  oferecem  ferramentas  de  ‘terapia  guiada  e/ou  técnicas  terapêuticas’  enunciadas como clinicamente validadas.
> (p. 41)

> Entre  as  ferramentas  mais  frequentes  (Figura  3),  além  de  exercícios  de  relaxamento,  meditação  guiada  e  respiração,  estão  recursos  de  ‘automonitoramento’. **Encontradas  em  sete  dos  dez  aplicativos, tais  ferramentas  correspondem  a  técnicas  para  coleta  explícita  e  direta  de  informações  sobre  humor  e  estados  emocionais,  indicando  a  relevância  desse  tipo  de  dado  para  os  PsiApps.** Outras  funcionalidades  complementam o automonitoramento, como ferramentas de diário pessoal e espaço para escrita terapêutica, ou ainda relatórios estatísticos e gráficos, voltados para visualização das análises dos dados fornecidos pelos usuários.
> (p. 41-2)

> Em  geral,  as  ferramentas  de  automonitoramento  estão  associadas  à  possibilidade  de  aprimorar  o   autoconhecimento   e/ou   melhorar   a   si   mesmo. **Essa   possibilidade   de   autoconhecimento,   de   um   ‘aperfeiçoamento’ de si, digamos, é encontrada em seis dos dez apps como a principal promessa – terceira categoria de análise da camada ‘visível’.** **Há uma associação clara entre as práticas de automonitoramento – rastreamento e registro de aspectos psicológicos e comportamentais, tais como humor, emoções, ansiedade, ânimo,  atividades  e  outros  –  e  o  autoconhecimento,  anunciado  como  chave  para  cuidar  e  melhorar  a  si mesmo.** Como afirma o app  Querida  Ansiedade,  o  autoconhecimento  serve  para  transformar  “a  vilã”  ansiedade “em amiga” (QUERIDA ANSIEDADE, 2019). A  partir  do  automonitoramento  sistematizado  e  de  o
> (p. 42-3)

# Treinamento de si ~ neoliberalismo

> A  partir  do  automonitoramento  sistematizado  e  de  outros  exercícios  para  reduzir  a  ansiedade,  **o  autocuidado  promovido  pelos  PsiApps  se  aproxima  mais  de  um  treinamento  do  que  de  uma  hermenêutica  de  si,  característica  do  modo  clínico  terapêutico  moderno** (FOUCAULT,  2010).  Esse  treinamento  de  si  é  expresso,  por  exemplo,  na  descrição  do  app  Lojong,  que  se  entitula  “uma  academia  para  mente”  na  qual  “você aprende a treinar sua mente através de práticas de meditação” (LOJONG APP, 2019) e outros exercícios de relaxamento. Ou ainda, no discurso do app Fabulous: “iremos agir como seus treinadores da vida, construindo sua motivação para que você possa focar em desenvolver hábitos que reduzam problemas de saúde mental, como ansiedade, e melhorar sua produtividade diária” (FABULOUS, 2019).
> (p. 43)

> O  que  a  análise  de  tais  discursos  evidencia,  portanto,  é  que  **o  autocuidado,  concebido  como  um  ‘treinamento de si’, visa a um aperfeiçoamento, a partir de uma autogestão otimizada, monitorada e calculada que os apps proporcionariam.** A mensagem aos usuários é a de que ‘se conhecendo’, eles podem se aperfeiçoar, **o que reproduz e reforça aspectos de uma racionalidade neoliberal** (ROSE, 2011; FOUCAULT, 2010; EHRENBERG,  2010;  BROWN,  2006),  **centrada  em  soluções  individuais,  o  que  estimula  os  indivíduos  a  buscarem continuamente a otimização de seus recursos psicológicos.**
> (p. 43)

# Coaching e mindifulness como zonas de intersecção entre os discurso psi e neoliberal

> A influência da Psicologia Positiva e de suas técnicas de Coaching é facilmente percebida nos enunciados dos apps. **Entre os aplicativos que mencionam modelos terapêuticos, o Mindfulness, que envolve técnicas e exercícios de direcionamento da atenção, é o mais recorrente.** Traduzida para o português como ‘atenção plena’ ou ‘consciência plena’, o Mindfulness é uma técnica de meditação secular cujas práticas consistem em  dirigir  a  atenção  tanto  para  elementos  externos,  como  sons  e  imagens,  quanto  para  si  próprio,  como  a  respiração  e  as  sensações  corporais,  com  o  objetivo  de  manter  “afastados  os  demais  pensamentos  e  os  julgamentos” (MORAES, 2019, p. 234).
> (p. 43)

> V:  Segundo o Instituto Brasileiro de Coaching, o Coaching “é um processo, uma metodologia, um conjunto de competências e habilidades que podem ser aprendidas e desenvolvidas por absolutamente qualquer pessoa pra alcançar um objetivo na vida pessoal ou profissional, até 20 vezes mais rápido” (IBC, 2020). **Autores como Dardot e Laval vêm enfatizando a relação entre técnicas como o Coaching e a programação neurolinguística e a produção do sujeito neoliberal, uma vez que elas “têm como objetivo fortalecer o eu, adaptá-lo melhor à realidade, torná-lo mais operacional em situações difíceis”** (DARDOT; LAVAL, 2016, p. 333).
> (p. 43)

# McDonaldização do mindfulness e mercado do sujeito ansioso

> Para Ronald Purser (2019), aplicativos de atenção plena integram a ‘McDonaldização do Mindfulness’, uma vez que produzem uma técnica eficiente, escalável e quantificável e uma mercadoria globalizada e comercializável. Deste modo, junto com outros apps de controle do tempo e da atenção, **os PsiApps compõem um mercado do ‘sujeito ansioso’, no qual pessoas buscam soluções rápidas e individualizadas para dar conta de sua ansiedade e de seus efeitos negativos no trabalho, nos relacionamentos e na saúde mental.**
> (p. 43)

# Aparente autonomia do neosujeito, self empreendedor, sujeito empresarial

> Nesse processo, a figura do “sujeito empresarial” (DARDOT; LAVAL, 2016, p. 333) ou “self  empreendedor” (ROSE, 2011, p. 215) emerge como **aquele que se automonitora e quantifica ininterruptamente variações  de  humor,  sintomas  psicológicos  e  estados  emocionais  de  modo  a  calcular,  administrar  e  gerir  seus  problemas  subjetivos.**  E  é  nessa  retórica  que  se  apoiam  as  funcionalidades  desses  aplicativos,  uma  vez que eles ofereceriam, ao alcance do usuário, as ferramentas necessárias para ele “superar os problemas emocionais que mais atrapalham a sua vida” (CÍNGULO, 2019)
> (p. 44)

> Embora o discurso dos apps enfatize que o sujeito é responsável por seu sucesso (ou seu fracasso) e que ele é autônomo na conquista do seu bem-estar psíquico, numa espécie de “empreendedorismo psíquico” (FISHER, 2016, p. 137) **tais promessas só podem ser  alcançadas  se  os  usuários  estão  assistidos  pelas  ferramentas  dos  próprios  aplicativos,  que  envolvem  uma série de processos opacos aos olhos dos usuários, ferindo, assim, sua suposta autonomia.**
> (p. 44)

# Autocuidado ~ cuidado de si

> É  curioso  notar  como  essa  versão  tecnologicamente  assistida  do  autocuidado  opera  uma  série  de  inflexões neoliberais numa noção cujos sentidos históricos já foram tão distintos. Mais afastado de nós, o cuidado  de  si  se  constitui  na  Antiguidade  como  um  conjunto  de  técnicas  pelas  quais  a  vida  de  cada  um  é  tomada  como  objeto  de  conhecimento  ou  de  arte  (FOUCAULT,  2002).  Mais  recentemente,  a  noção  de  autocuidado tem uma trajetória médica e política que encontra uma de suas expressões mais potentes no feminismo negro entre os anos 1970 e 1980. O cuidado consigo ganha, nesse contexto, a força política de sobrevivência frente a instituições médicas racistas e sexistas, implicando ações tanto individuais quanto coletivas que envolviam, inclusive, a abertura de clínicas comunitárias (NELSON, 2011).
> (p. 44)

# Visão tecnoliberal do autocuidado

> Toda  essa  dimensão  política,  crítica  e  coletiva  é  esvaziada  na  versão  tecnoliberal  do  autocuidado  encarnada  pelos  PsiApps.  **Na  esteira  das  ferramentas  de  autoajuda,  a  retórica  neoliberal  do  autocuidado  adiciona uma atenção maior aos limites e às fragilidades individuais, buscando ao mesmo tempo aceitá-los e superá-los por meio de investimentos em si mesmo. Os PsiApps materializam, assim, o investimento e a responsabilidade individuais como o caminho terapêutico para lidar com problemas psíquicos e emocionais sem considerar fatores coletivos, socioculturais e contextuais implicados no sofrimento individual.**
> (p. 44-5)

# Privatização do stress em Mark Fischer

> Tal perspectiva está em consonância com o que Mark Fisher (2016) chama de “privatização do stress” (p.  125)  ao  abordar  a  saúde  mental  nas  sociedades  capitalistas  atuais.  **A  “privatização  do  stress”  (p.  125)  para  o  autor,  foi  parte  fundamental  do  projeto  de  destruição  do  conceito  de  público  e  do  qual  depende,  fundamentalmente, o conforto psíquico.** Em nossa análise, chama atenção como os aplicativos raramente vinculam os estados de ansiedade a dimensões que ultrapassam o indivíduo. Para eles: “A melhor forma de aprender a lidar com a ansiedade é conhecer a si mesmo” (QUERIDA ANSIEDADE, 2019).
> (p. 45)

# Terms of Consent (ToCs) -- Termos de uso e Políticas de privacidade

> Assim, para rastrear os fluxos de captura e compartilhamento de dados e os atores que compõem essa infraestrutura sociotécnica, **examinamos num primeiro momento os termos de uso e políticas de privacida-de, raramente lidos com atenção pelos usuários, mas centrais à delimitação da responsabilidade jurídica do aplicativo.** Notamos que **apesar de todos fazerem menção ao compartilhamento de dados com terceiros e de quase a totalidade admitir a coleta de informações pessoais e/ou de atividades do usuário (oito e nove apps, respectivamente), apenas quatro dos dez PsiApps são específicos sobre com quem compartilham os dados e apenas dois apps fazem referência ao tipo de dados que podem ser compartilhados.** Isso é especialmente crítico pelo caráter sensível dos dados aos quais esses aplicativos têm acesso, uma vez que disponibilizam ferramentas de registro, análise e monitoramento de informações sobre emoções, humores e estados psi-cológicos dos usuários. A coleta e o compartilhamento desse tipo de dados, no entanto, praticamente não é especificada. Somente a política de privacidade do Sanvello cita explicitamente a coleta de informações sobre “humor, objetivos e dados pessoais de saúde” (SANVELLO, 2020, tradução nossa)
> (p. 45-6)

# Finalidade (marketing)

> Quanto à finalidade da coleta de dados, **oito aplicativos alegam que os dados são utilizados para melhorar e personalizar a experiência do usuário e/ou aperfeiçoar os produtos e serviços ofertados. Sete aplicativos citam uso de dados para ferramentas de marketing comportamental ou personalização de anúncios. Alguns especificam finalidades analíticasvii,  como:  interpretações  através  da  análise  das  ações  e  preferências  do  usuário  (Meditopia),  medir  o  desempenho  de  iniciativas  de  mercado  (Cíngulo)  ou  gerar  relatórios  com  estatísticas  sobre  o  uso  dos  serviços  (Sanvello).**  Via  de  regra,  deixam  brechas  para  usos  não  explicitados  nas políticas de privacidade e nos termos de uso. **Com relação à possibilidade de uso dos dados para fins de pesquisa acadêmica ou científica, três aplicativos a mencionam: Cíngulo, Meditopia e Fabulous.** A Figura 4 apresenta uma síntese da análise sobre coleta, compartilhamento e uso de dados declarados pelos desenvolvedores.
> (p. 46)

![Figura 4 - Finalidades](./fig4.png)

# Captura de informações pessoais e gerais

> Informações  pessoais:  nome  completo,  e-mail,  telefone,  endereço,  gênero,  fotos  subidas  ao  app,  data  de nascimento, documento de identificação, dados de acesso em redes sociais. O Sanvello menciona, além desses, que coleta dados referentes a planos de saúde e outros dados pessoais de saúde.
> (p. 46)

> Informações gerais ou de atividade: endereço IP, dispositivo, tipo e versão de browser, páginas visitadas dentro do app, quando e por quanto tempo, quais as funcionalidades mais populares, entre outras, obtidas de modo automatizado, a partir de rastros de interação e que não identificam o usuário pessoalmente.
> (p. 46)

# Trackers: anúncio e perfilamento (pela Facebook e Google)

> Como descrito no tópico 2, além da análise dos termos de uso e políticas de privacidade, examinamos a presença de trackers nos arquivos de instalação dos aplicativos. No âmbito dos AppStudies, **investigar os trackers – literalmente rastrear os rastreadores – é um método exploratório que expõe atores e conexões que  costumam  permanecer  opacos  na  rede  que  constitui  a  economia  de  dados  em  que  os  aplicativos  se capitalizam.**  Além  disso,  tal  rastreamento  permite  detectar  os  tipos  de  trackers  mais  recorrentes  nos  PsiApps e a quem pertencem, dando indícios mais tangíveis acerca dos seus modelos de negócios.
> (p. 46-7)

> Os trackers podem ser agrupados em diferentes categorias, a partir do tipo de informação que coletam. Neste artigo, utilizamos as categorias sugeridas pela própria ferramenta εxodus (2020): Relatório de erros, Analíticos, Criadores de perfil, Identificação, Anúncios, Localização. (tradução nossa). **Entre essas categorias, dois grupos se destacaram: os ‘trackers de anúncios’ e os ‘trackers analíticos e de perfilização’, correspondentes a 33,3% e 44%, respectivamente, dos 75 trackers identificados. Enquanto os primeiros são  utilizados  para  veicular  automaticamente  anúncios  segmentados  por  públicos-alvo,  gerando  receita  com sua aplicação; o segundo grupo se destina a capturar automaticamente uma série de eventos e dados do usuário, visando a construção de perfis, extração de métricas, segmentação de audiência, análise de comportamento dos usuários, relatórios de desempenho do aplicativo, correlação de eventos etc.** (GOOGLE FIREBASE, 2020).
> (p. 47)

> Quem são os terceiros que os termos de uso e políticas de  privacidade  tratam  de  modo  tão  vago?  **O  levantamento  realizado  via  εxodus  mostrou  a  presença massiva do Facebook e do Google nesse circuito de captura, compartilhamento e análise de dados, o que nos  fornece  mais  um  dado  importante  sobre  os  atores  e  as  relações  que  compõem  essa  infraestrutura  que viemos descrevendo. Na inspeção de nove aplicativosviii, os trackers dessas empresas correspondem, juntos, a 66,6% do total da utilização de trackers.** No infográfico a seguir (Figura 5) é possível visualizar o mapeamento realizado, detalhando os trackers e as permissões de cada aplicativo, bem como a recorrência de cada tracker.
> (p. 47)

![Figura 5 - Trackers](./fig5.png)

# Processo de captura: cíclico, recursivo e performativo

> Um último aspecto a destacar sobre essa infraestrutura multinível e multiescala é o caráter altamente cíclico, recursivo e performativo de seu funcionamento: **os dados capturados, analisados, perfilizados e segmentados são continuamente utilizados para testar, modificar e atualizar os aplicativos, gerando novos dados para análise num ‘circuito de retroalimentação’ contínuo, para utilizar um termo caro à cibernética** (WIENER, 2017; HUI, 2019). Essa performatividade da função recursiva (LURY; DAY, 2019) no ecossistema dos aplicativos é um traço relacionado tanto ao próprio modo de existência dos objetos digitais modulados por algoritmos (HUY, 2016) quanto ao modelo de plataformização (HELMOND, 2015) – que está na base das demandas por contínuas atualizações e inovações do mercado da tecnologia (NIEBORG; POELL, 2018). No infográfico a seguir (Figura 6), ilustramos de modo sintético os fluxos de captura, compartilhamento e análise de dados por meio dos trackers, as múltiplas camadas, os atores e as conexões que compõem a infraestrutura que viemos descrevendo, bem como o caráter cíclico desse processo.
> (p. 48)

![Figura 6 - O processo de captura](./fig6.png)

# Mediadores

> Como se pode ver, o autocuidado, que supostamente colocaria o indivíduo no centro do controle, conta na verdade  com  uma  série  de  mediadores,  humanos  e  não  humanos,  que  são  em  grande  parte  invisíveis  para  o  usuário.  Ainda  que  o  app  seja  visto  pelo  usuário  como  um  mediador  em  sua  busca  por  autoconhecimento  e  autocuidado, os muitos mediadores encapsulados no app escapam ao seu conhecimento e controle.
> (p. 49)

> É sobretudo a opacidade dessa cadeia de mediadores que faz com que **os PsiApps pareçam tecnicamente neutros para os usuários, atuando como “mediações que produzem experiência de não mediação”** (CESARINO, 2019, p. 13). O aplicativo e sua suposta neutralidade técnica anuncia-se como um dispositivo perfeito para que cada um monitore, conheça e cuide de si mesmo. Afinal, trata-se de uma máquina que se alimenta de ‘nós mesmos’ (hábitos, condutas e informações que fornecemos) e que nos devolve uma imagem, um grafo, uma visualização de nosso humor, nossa rotina, nosso nível de ansiedade, nossas noites de sono ou de insônia.
> (p. 50)

# Heterogeneidade e assimetria da rede e do processo

> Ao mesmo tempo, vimos que a infraestrutura por onde trafegam os dados gerados pelo uso do app compõe uma rede heterogênea de relações dinâmicas entre atores, processos, protocolos, finalidades e apropriações, que, mais uma vez, são opacas para os usuários. **Apesar de ser parte fundamental dessa infraestrutura composta por atores heterogêneos, o usuário, à diferença dos outros atores que o acompanham e monitoram, desfruta de modo muito limitado da dimensão relacional agenciada nessa rede, uma vez que não participa dela de forma minimamente  simétrica.**  Já  os  atores  que  têm  acesso  privilegiado  à  infraestrutura  e  ao  ecossistema  de  dados  materializados na camada ‘invisível’ conseguem, de fato, tirar grande proveito dessa relação.
> (p. 49)

# Relacionalidade dos dados capturados (perda de autonomia)

> **A  dimensão  relacional  é,  assim,  quase  que  inteiramente  expropriada  por  tais  atores,  que  se  alimentam  dos dados relacionais e produzem perfis, valor, pesquisas científicas, testes e atualizações do produto etc. –que  constituem-se  na  verdadeira  condição  para  a  possibilidade  das  ofertas  de  autonomia  da  camada  ‘visível’.**  O  sujeito  tem  sua  participação  extremamente  limitada  nesse  processo,  uma  vez  que  em  grande  medida  nem  se  percebe  em  tal  relação,  tendo  sua  agência  e  margem  de  negociação  bastante  reduzidas.  **É  justamente  essa  limitação e assimetria que o impede de conhecer não só a rede da qual faz parte, mas as implicações de sua falta de autonomia em relação ao ecossistema automatizado.**
> (p. 49)

> Como  alguns  autores  vêm  destacando,  o  saber  associado  à  atual  economia  de  dados  **visa  menos  um  conhecimento individualizado e aprofundado da personalidade de indivíduos específicos do que um conhecimento de correlações que revela padrões supraindividuais ou interindividuais que permitam, entre outras finalidades, gerar predições em larga escala** (BRUNO; BENTES; FALTAY, 2019; ROUVROY; BERNS, 2015). Ou  seja,  apesar  de  depender  da  contínua  produção,  captura  e  compartilhamento  de  dados  pessoais,  **a  dimensão individual é também no ecossistema de dados, em certa medida, ilusória ou ao menos parcial, ainda que essencial, uma vez que é a dimensão relacional aquela de fato visada por esse modelo de conhecimento e poder.**
> (p. 49)

# Não-neutralidade e economia de dados

> No  entanto,  sabe-se  que  “a  Ferramenta  Neutra  sob  controle  completamente  humano”  é  um  mito  (LATOUR,  1994,  p.  32,  tradução  nossa).  Como  vimos,  também  não  são  nada  neutros  os  problemas  visados  pelos  apps,  assim  como  as  ferramentas  que  disponibilizam  e  as  soluções  que  prometem.  Embora frequentemente busquem legitimar seus métodos com ‘bases científicas’, **os PsiApps são fruto de uma  série  de  perspectivas  sobre  o  psiquismo,  o  comportamento  e  as  emoções,  bem  como  sobre  suas  disfunções e ‘aprimoramentos’. Da mesma maneira, os tipos de dados, os hábitos, as emoções e estados psíquicos monitorados e coletados são frutos de escolhas baseadas em abordagens específicas e voltadas para finalidades particulares, as quais não são claras para o usuário, mas não são neutras. Ou seja, os PsiApps estão inseridos numa rede bastante complexa de influências e matrizes sociotécnicas, científicas, econômicas etc.** Confrontar os discursos e modelos terapêuticos que colocam o indivíduo no controle do seu  autocuidado  com  a  materialidade  e  heterogeneidade  dos  atores  e  conexões  agenciados  na  camada  invisível evidenciou não apenas como os aplicativos móveis são parte da infraestrutura de uma poderosa **economia de dados** que se torna central para o capitalismo contemporâneo, mas também permitiu identificar processos ligados a contextos sociais, políticos e culturais mais amplos. Não por acaso, **a defesa do fim dos mediadores tradicionais atravessa o funcionamento neoliberal da sociedade** (CESARINO, 2019). Se o indivíduo deve ser o único responsável por sua autorrealização, quanto menos mediadores existirem entre ele e a conquista de suas metas, melhor.
> (p. 50)

# Escolhas técnicas e epistemológicas subjacentes aos PsiApps

> Além disso, o predomínio de técnicas de automonitoramento, que traduzem em dados humores e estados psíquicos são mais um indício do modo de funcionamento da mediação técnica desses dispositivos: eles sugerem como certos modelos terapêuticos podem ser mais adequados não somente à proposta de tratamento rápido, autônomo e flexível, mas às próprias operações técnicas dos aplicativos, sobretudo as algorítmicas. **Noutros termos, o privilégio dado a certas ferramentas pelos PsiApps não é apenas indicativo dos modelos subjetivos e epistemológicos que estão em sua base, mas de quais modelos terapêuticos geram dados legíveis e operacionalizáveis algoritmicamente pela mediação técnica.**
> (p. 50-1)

# Autonomia individual -> economia de dados baseado na captura

> Portanto,  nessa  jornada  de  autoconhecimento  e  autocuidado  marcadamente  neoliberal,  o  usuário  está  longe  de  alcançar  efetivamente  o  modelo  de  autonomia  tão  propagado  pelos  PsiApps.  Vimos  que  os  dados  gerados  por  ele  são  analisados  e  utilizados  por  uma  série  de  outros  humanos  e  máquinas,  e  geralmente compartilhados com terceiros, alimentando um modelo de negócios e de conhecimento dominado pelas grandes corporações de tecnologia. **Um ecossistema que visa cada vez mais nossos dados psíquicos e emocionais, mas sobre os quais pouco ou nada sabemos. O promulgado autocuidado é, assim, acompanhado  de  uma  cadeia  complexa,  heterogênea,  recursiva  e  dinâmica  de  atores  humanos  e  não  humanos,  constituída  por  protocolos  tecnológicos,  plataformas,  instituições,  corporações,  dispositivos  móveis,  interfaces,  bancos  de  dados,  algoritmos,  trackers,  cientistas  de  dados,  desenvolvedores,  bem  como modelos psicológicos, terapêuticos e de negócios.** Problematizar tais questões não significa tomar o usuário por ingênuo. Trata-se, contudo, de enfatizar as implicações da assimetria materializada nessa rede sociotécnica entre o usuário e os atores que possuem acesso privilegiado ao ecossistema de dados.
> (p. 51)
