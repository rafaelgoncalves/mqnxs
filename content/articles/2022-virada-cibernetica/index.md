---
title: A informação após a virada cibernética
date: 2022-05-19
path: "/virada-cibernetica"
category: fichamento
author: Rafael Gonçalves
featuredImage: "./laymert.jpg"
srcInfo: <a href="https://commons.wikimedia.org/wiki/File:Laymert_dos_Santos_(3972103243).jpg">Laymert Garcia dos Santos</a>
tags:
- capitalismo
- cibernética
- tecnologia
- brasil
- Laymert Garcia dos Santos
- esquerda
published: true
---

Fichamento de fala de Laymert Garcia dos Santos entitulada _A informação após a virada cibernética_[^1] sobre a fusão entre desenvolvimentos capitalista e tecnocientífico que o autor chamou de "virada cibernética".

[^1]: GARCIA DOS SANTOS, L. A informação após a virada cibernética. In: **Revolução tecnológica, internet e socialismo**. Socialismo em discussão. 1a. ed. São Paulo, SP, Brasil: Editora Fundação Perseu Abramo, 2003. p. 9–34. 

# Objetivo do texto: problematizar a relação entre política e tecnologia pela esquerda contemporânea

> Aceitando o pressuposto de que há uma revolução tecnológica e de que é preciso pensar sua articulação com a luta pelo socialismo, minha intervenção não pretende trazer respostas e soluções, mas sim problematizar as relações entre tecnologia e política na sociedade contemporânea e apontar questões que, a meu ver, não estão sendo tratadas com a devida atenção pela esquerda brasileira.
> (p. 9)

# Virada cultura e pós-modernidade a partir de 1970s

> Desde a década de 1970, mas principalmente nos anos mais recentes, tem se firmado a tese segundo a qual o capitalismo estaria se transfigurando ao incorporar a dimensão da cultura ao processo de produção e até mesmo ao fazer dela o motor da acumulação. De certo modo, grande parte da discussão que se trava nas ciências sociais sobre a questão da globalização, mas também da chamada pósmodernidade, gira em torno daquilo que Frederic Jameson denominou “a virada cultural” 2 , isto é, a transformação geral da própria cultura com a reestruturação social do capitalismo tardio enquanto sistema.
> (p. 10)

> Nessa perspectiva, se quisermos compreender a sociedade atual, precisaremos entender como a cultura vem sendo colonizada pelo capital e como tal colonização afeta a política, as lutas de resistência e os anseios de emancipação. Seguindo a trilha de Jameson e de outros autores, Jeremy Rifkin, um arguto farejador das tendências econômicas contemporâneas, argumenta que o capitalismo global não só é “baseado no conhecimento”, mas também, e principalmente, que ele, ao canibalizar as culturas, todas as culturas, ameaça as próprias bases das sociedades ao dissolver a diversidade cultural do planeta por meio de uma instrumentalização cada vez mais intensa e acelerada.
> (p. 10)

# Limitação da ideia de virada cultural em relação ao papel que ocupam as tecnologias da informação

> A discussão sobre a virada cultural operada pelo capitalismo contemporâneo é muito instigante. Tendo porém a achar que ela tem um alcance limitado por não considerar a centralidade das tecnologias da informação no processo em curso – mesmo conferindo importância a essas tecnologias, o pensamento sobre a virada cultural vê o papel destas como apenas _um_ dos fatores da transformação. 
> (p. 10)

# Virada cibernética: tecnociência como motor da acumulação capitalista contemporânea

> Minha hipótese, portanto, é que para perceber o mundo que está sendo construído não basta compreender a plena incorporação da cultura ao sistema de mercado. Mais importante do que a transformação desta em mercadoria parece ser a “virada cibernética”, que selou a aliança entre o capital e a ciência e a tecnologia, e conferiu à tecnociência a função de motor de uma acumulação que vai tomar todo o mundo existente como matéria-prima à disposição do trabalho tecnocientífico.
> (p. 10-11)

# Tecnologias da informação para além das mídias comunicacionais (codificação e digitalização do mundo)

> Entretanto é preciso deixar claro que a mídia, tal como existe hoje, é apenas um segmento, ainda que relevante, da virada cibernética; esta implica muito mais do que o simples uso dos meios eletrônicos para transmitir informações. Hoje não se pode mais pensar a questão dos meios eletrônicos segundo os mesmos parâmetros de outrora – as concepções que viam os meios como o quarto Poder, como dispositivos passíveis de democratização da cultura, como porta-vozes da opinião pública, como veículos que podem contribuir para o aperfeiçoamento democrático. Estas concepções precisam ser reconsideradas dentro do campo maior que é definido pelo alcance e pela abrangência da noção tecnocientífica de informação. Do mesmo modo, discussões como a democratização da informática e da inte rnet não podem se limitar à exaltação ou à crítica dos novos meios. Isso porque as tecnologias da informação extrapolam imensamente o campo de atuação da mídia e das novas mídias, pois operam – em todos os campos – a codificação e a digitalização do mundo ao manipularem a realidade informacional que permeia a matéria inerte, o ser vivo e o objeto técnico.
> (p. 11-2)

# Origem da virada cibernética na mudança tecnocientífica no final da 2a GM

1948: Publicação do livro _Cybernetics_ de Norbert Wiener e do artigo _A Mathematical Theory of Communication_ de Claude Shannon.

> Antes de marcar uma “revolução” na sociedade contemporânea, a virada cibernética foi uma mudança que se operou desde o final da Segunda Guerra Mundial no campo da ciência e da tecnologia.
> (p. 12)

> Comentando a importância da publicação do livro de Norbert Wiener, _Cybernetics, Theory of Control and Communications in the Animal and the Machine_, o filósofo Gilbert Simondon observa:
> > Enquanto a especialização científica impedia as possibilidades de comunicação, nem que fosse por causa de linguagens diferentes entre especialistas de diferentes ciências, a cibernética, em contrapartida, resultava de vários homens trabalhando em equipe e tentando entender a linguagem uns dos outros. [...] a presença de médicos, de físicos e de matemáticos eminentíssimos nessa equipe mostrava que se produzia no campo das ciências algo que sem dúvida não havia existido desde Newton pois [...] Newton pode ser considerado o último homem de ciência a haver coberto todo o campo da reflexão objetiva. [...] Com efeito, historicamente, a cibernética surgiu como algo novo, querendo instituir uma síntese4
> (p. 12)

> Tal centralidade se devia ao fato de o conceito de informação ser válido nos campos da física, da biologia e da tecnologia. Com a palavra, Simondon:
> > Seria preciso definir uma noção que fosse válida para pensar a individuação na natureza física tanto quanto na natureza viva, e em seguida para definir a diferenciação interna do ser vivo que prolonga sua individuação separando as funções vitais em fisiológicas e psíquicas. Ora, se retomamos o paradigma da tomada de forma tecnológica, encontramos uma noção que parece poder passar de uma ordem de realidade a outra, em razão de seu caráter puramente operatório, não vinculado a esta ou àquela matéria, e definindo-se unicamente em relação a um regime energético e estrutural: a noção de informação.
> (p. 13)

# Informação para G. Bateson: "uma diferença que faz a diferença"

> Definida como a singularidade real por meio da qual uma energia potencial se atualiza, através da qual uma incompatibilidade é superada, a informação, segundo a formulação luminosa de Gregory Bateson, é “uma diferença que faz a diferença”. 
> (p. 13)

# Informação como componente de um campo pré-individual e imanente a partir da qual a matéria inerte é gerada e tornada consistente. Pré-individual como campo consistente do ser.

> Ocorre que tanto na física quanto na biologia e na tecnologia a informação atua nessa realidade pré-individual, intermediária, que o filósofo denomina “o centro consistente do ser”; essa realidade natural pré-vital tanto quanto pré-física a partir da qual a vida e a matéria inerte são geradas e tornam-se consistentes. Ora, a possibilidade de se conceber um substrato comum à matéria inerte, ao ser vivo e ao objeto técnico apaga progressivamente as fronteiras estabelecidas pela sociedade moderna entre natureza e cultura. Mais ainda: tudo se passa como se houvesse um plano de realidade em que matéria e espírito humano pudessem se encontrar e se comunicar não como realidades exteriores postas em contato, mas como sistemas que passam a se integrar num processo de resolução que é imanente ao próprio plano.
> (p. 13)

> Se a técnica é veículo de uma ação que vai do homem ao universo e de uma informação que vai do universo ao homem, é fator de resolução de um diálogo intenso; o que conta é a interação, o caráter produtivo do agenciamento, e não as partes preexistentes. Na base da virada cibernética encontra-se, assim, a capacidade do homem de “falar” a linguagem do “centro consistente do ser”.
> (p. 13-4)

# Donna Haraway sobre informação como o que possibilita codificação e tradução universais

> Donna Haraway havia observado que as ciências das comunicações e a biologia moderna compartilham o mesmo ímpeto de traduzir o mundo num problema de codificação, de buscar uma linguagem comum na qual desapareça qualquer resistência ao controle instrumental e na qual toda heterogeneidade possa ser submetida a decomposição, recomposição, investimento e troca. “O mundo”, escreve Haraway, “é subdividido por fronteiras diferencialmente permeáveis à informação. Esta nada mais é do que um tipo de elemento quantificável (unidade, base de unidade) que permite uma tradução universal e, portanto, um poder instrumental desabrido” 6.
> (p. 14)

# Virada cibernética como movimento que permite o controle tecnocientífico do mundo por meio da informação

> É preciso, portanto, perceber a virada cibernética como esse “movimento comum” que se dá no campo da ciência e da técnica, a partir do qual se instaura a possibilidade de abrir totalmente o mundo ao controle tecnocientífico por meio da informação. Mas é evidente que essa possibilidade inaugurada dentro dos laboratórios não se circunscreve a eles.
> (p. 14)

# Hermínio Martins sobre estados de natureza/cultura cibernéticos

> A virada cibernética não é apenas mudança na lógica da técnica: a perspectiva de uma dominação irrestrita da natureza pelo homem, inclusive da natureza humana, leva a tecnociência a erigir como referência máxima o “estado de natureza cibernético” e o “estado de cultura cibernético”. Com a palavra, o sociólogo Hermínio Martins:
> > No “estado de natureza cibernético”, a “natureza” é naturezacomo-informação. Ou seja, o pressuposto é que a natureza se encontra totalmente disponível aos processos de recuperação, processamento e armazenamento de informação, possibilitados pela máquina universal, ou _machina machinarum_, o computador eletrônico digital, programável, multiusos e de alto rendimento. [...] Se estamos já a viver dentro do horizonte do “estado de natureza cibernético”, possível de sumariar adequadamente como “natureza-como-informação”, podemos também dizer que estamos a moldar e a ser moldados, cada vez mais, por aquilo a que podemos chamar por analogia “estado de cultura cibernético”, quando a cultura se torna cultura-como-informação. Isso é óbvio sobretudo no caso da cultura cognitiva paradigmática, a ciência natural ou tecnociência, embora se deva notar que durante várias décadas esta cibernização da ciência esteve quase totalmente confinada à ciência militar (durante a Guerra Fria). [...] Diz-se hoje que, resultante de papéis cada vez mais numerosos [...], se fez muito mais do que juntar uma frente tecnológica adicional ao _instrumentarium_ da investigação científica, pelo menos nas ciências físicas e da vida. Em vez disso, parece mais apropriado falar de nada menos do que a emergência de uma terceira forma de ciência, como tem sido sugerido por alguns investigadores7.
> (p. 14-5)

# Efemeralização e miniaturização na tecnociência e sua relação com o capitalismo contemporâneo

> A virada cibernética não se circunscreveu aos laboratórios. Desde a década de 1970 começamos a sentir os seus efeitos no plano social. Richard Buckminster-Fuller, que acompanhou sistematicamente a evolução tecnológica do século XX desde a década de 1920 até meados dos anos 1980, descobriu que toda tecnologia tinha um tempo de gestação, mas também que tal tempo estava encurtando cada vez mais, o que evidenciava uma aceleração crescente. Entretanto, a partir da década de 1970, a evolução tecnológica dispara, caracterizando um movimento que Fuller chama de efemeralização, isto é, aceleração da aceleração, aceleração exponencial que faz com que as transformações comecem a se precipitar. Por outro lado, a esse fenômeno se somava a descoberta da miniaturização, isto é, a possibilidade de fazer mais com menos: mais com menos trabalho, menos energia e menos matéria-prima. Se conjugarmos tudo isso com a chegada da informática na vida cotidiana, veremos que tecnicamente estavam dadas as condições para uma grande transformação da sociedade capitalista.
> (p. 15-6)

~Gilles Deleuze sobre o surfe como esporte modulatório (_Post-scriptum..._)

> A inovação conduziu então a produção industrial a uma verdadeira mutação que afetou inclusive a lógica dos investimentos nas empresas de ponta: a partir de meados da década de 1980 o princípio do retorno do capital começou a deixar de comandar o processo de substituição de tecnologias e passou a prevalecer o princípio do surfe: há ondas tecnológicas e as empresas têm que surfar – não há mais tempo para esperar o retorno do capital investido, as próprias ondas tecnológicas exigem que se esteja na crista da onda para não morrer.
> (p. 16)

# Aliança entre tecnociência e capital numa ordem planetária. Animais humanos e não-humanos como fonte de informação genética. Trabalho flexível assentado no processamento de informação digital. Saber como reelaboração, reprocessamento, reprogramação e recombinação de conhecimentos já existentes.

> Ninguém ignora que o desenvolvimento tecnológico encontra-se na base da globalização. Mas poucos a concebem como o fruto de uma aliança entre o capital e a tecnociência que se estende ao nível planetário ao mesmo tempo que consagra a inovação tecnológica como instrumento de supremacia econômica e política. Nos países do Norte a tecnologização da sociedade é intensa. À reordenação e reprogramação do processo de trabalho em todos os setores, tornada possível pela digitalização crescente dos circuitos de produção, circulação e consumo, veio associar-se a recombinação da vida, tornada possível pela decifração do código genético e pelos avanços da biotecnologia. Naqueles países, tudo se passa como se uma nova era estivesse se abrindo ou, mais do que isso, como se tudo fosse passível de questionamento, como se até mesmo a evolução natural das espécies, inclusive a humana, tivesse chegado a seu estado terminal e a história tivesse sido “zerada”, tratandose, agora, de reconstruir o mundo sobre novas bases. Quando percebemos que na ótica do biotecnólogo uma planta, um animal e até mesmo o ser humano reduz-se a um pacote de informações – porque o que interessa é o agenciamento das suas informações genéticas –, realizamos melhor a mudança de perspectiva. Por outro lado, a noção de trabalho e até mesmo a de produção de conhecimento também são profundamente alteradas, agora não pela informação genética, mas pela digital. O trabalho vai inscrever-se no software dos computadores, cuja capacidade de processar a informação na própria cadeia da produção lhe confere uma flexibilidade até então desconhecida. Finalmente, o próprio saber é transformado: cada vez mais é reelaboração, reprocessamento, reprogramação e recombinação de conhecimentos já existentes.
> (p. 17)

# Informação transforma o mundo em uma inesgotável base de dados

> Em suma: a informação enquanto diferença que faz a diferença reconfigura o trabalho, o conhecimento e a vida, enquanto a virada cibernética transforma o mundo num inesgotável banco de dados.
> (p. 17)

# Mudança de foco no capitalismo para o controle de processos e não para os produtos. Mais pelas potências (virtual) que pelas atualizações. O papel das simulações e prospecções (e modelizações, e modulações, etc.)

~prospecção em Simondon (_Imaginação e invenção_)

~controle em Deleuze (_Post-scriptum..._)

~modelização, antecipação em Rouvroy e Berns (_Governamentalidade algorítmica..._)

> Em toda parte, e sempre que possível, o capitalismo de ponta passa a interessarse mais pelo controle dos processos do que dos produtos, mais pelas potências, virtualidades e performances do que pelas coisas mesmas. O capital, e antes de tudo o capital financeiro, começa a deslocar-se para campo do virtual, voltando-se para uma economia futura cujo comportamento é analisado por meio de simulações cada vez mais complexas. Tal tendência não se limita porém ao mercado financeiro; em muitos outros setores a prospecção passa a preponderar.
> (p. 17-8)

# Capitalismo contemporâneo visa o controle sobre a realidade virtual, mais do que a apropriação de produtos da realidade atual

> Ora, é possível compreender todo esse deslocamento por meio da importância ascendente da informação, tal como é aqui entendida. Com efeito, como germe que atualiza a potência do virtual, ela é o operador da passagem de uma dimensão da realidade para outra, se lembrarmos que a dimensão atual da realidade é a dimensão do existente, ao passo que a dimensão virtual é a do que existe enquanto potência. Assim, é a informação que permite ao capital global e à tecnociência passarem da dimensão atual da realidade para a sua dimensão virtual. Agora se torna possível investir sobre toda criação, inclusive a criação da vida. Sabemos que por meio da privatização das telecomunicações, da colonização das redes e do próximo loteamento do campo eletromagnético, o capital global busca controlar o acesso e a exploração do ciberespaço; mas nos esquecemos de que a ambição maior da nova economia é assenhorearse da dimensão virtual da realidade, e não apenas da dimensão da realidade virtual, do ciberespaço, como tem sido observado.
> (p. 18)

# Ambição capitalista de apropriação do futuro

> Se tivermos em mente que a dimensão virtual da realidade começa a ser mais importante em termos econômicos do que a sua dimensão atual, teremos uma idéia melhor do sentido da corrida tecnológica. Aliado à tecnociência, o capitalismo tem a ambição de apropriar-se do futuro.
> (p. 18)

# Instrumentalização que visa observar o mundo como matéria-prima informacional a ser processado por tecnologias que lhe agregam valor. O papel da propriedade intelectual.

> Levando a instrumentalização ao extremo, a virada cibernética permite que a tecnociência considere tudo o que existe ou existiu como matéria-prima a ser processada por uma tecnologia que lhe agrega valor. Tal possibilidade abriu para a apropriação capitalis ta um horizonte e um campo de atuação insuspeitos: o plano molecular do finito ilimitado no qual, lembrando Deleuze, um número finito de componentes produz uma diversidade praticamente ilimitada de combinações. Se o mundo é um banco de dados, a atividade valorizada é aquela que nele garimpa informações passíveis de serem traduzidas em novas configurações e apresentadas como inovações. Não é difícil perceber, então, que tanto para a tecnociência como para o capital global a preocupação primeira consistiu em encontrar uma formulação jurídica que lhes permitisse assegurar o acesso e o controle da informação nos dois extremos, isto é, no plano molecular em que ela se encontra, mas também no plano global, no mercado mundial em que ela será explorada depois de reprogramada.
> (p. 19)

> Os sistemas de propriedade intelectual foram a saída jurídica encontrada para a proteção da inovação fundada na manipulação da informação genética ou digital. Mas para que isso ocorresse foi preciso transferir para esse terreno o regime de patentes, que vigorava na esfera industrial e selava as relações entre o direito e a ciência, protegendo a propriedade de artefatos e máquinas, isto é, das coisas que não existiam na natureza inanimada. Ora, tal transferência não se configura como continuação ou simples desdobramento de um monopólio temporário de exploração concedido aos inventores desde o século XIX ; na verdade, ela é muito mais do que isso.
> (p. 19)

> Os direitos de propriedade intelectual consagram a dessacralização total da vida, ao legitimarem a apropriação, a exploração e a monopolização de seus componentes.
> (p. 19)

# Mudanças do sistema de patentes de propriedade intelectual industrial para propriedade intelectual informacional

> De onde viemos? Para onde vamos?, se pergunta o jurista Bernard Edelman. E responde:
> 
> > Até o primeiro terço do século XX , o direito estava em paz com as ciências e as técnicas e nada vinha perturbar esse idílio. A maneira pela qual considerava o “ser vivo”, fosse ele vegetal, animal ou humano, como uma totalidade não apropriável [...], correspondia idealmente ao modelo técnico-científico da natureza. Em outras palavras, as categorias jurídicas estavam ajustadas ao saber técnico. Se o homem era o senhor da natureza, ainda não era o seu “possuidor” ou, mais precisamente, seu “proprietário”. A técnica da patente exprimia perfeitamente o domínio do homem sobre uma natureza inanimada.
> 
> Onde estamos e para onde vamos?
> 
> > A intrusão do ser vivo no campo da patente a partir dos anos 1930 é uma revolução jurídica. E para entender direito tal revolução é preciso destacar um duplo fenômeno: o da modificação profunda do papel da patente e as condições jurídicas que permitiram que o ser vivo fosse patenteável.
> (p. 19-20)

> Não é o caso de acompanhar aqui o longo caminho percorrido por essa revolução, que começa com uma proteção jurídica específica para as plantas por meio do Plant Act de 1930 nos Estados Unidos, abarca os microorganismos em 1980 com o caso Chakrabarty, estende-se aos animais no final da mesma década e, finalmente, chega ao homem, com o caso Moore. Interessa, porém, sublinhar que para o jurista foi-se pouco a pouco elaborando um modelo jurídico que, partindo de uma concepção sagrada do ser vivo, desembocou numa concepção instrumental e até mesmo industrial.
> (p. 20)

# Proteção de propriedade x não-proteção de valores de uso, da vida

> É interessante notar que enquanto se arrastam ao longo dos anos as iniciativas jurídicas para proteger o acesso aos recursos genéticos e ao conhecimento, às inovações e práticas das comunidades tradicionais e das populações indígenas a eles associados, o acesso às inovações tecnocientíficas já se encontra mundialmente protegido pelo regime de propriedade intelectual, tal como podemos ler nos acordos GATT-Trips 12 , da Organização Mundial de Comércio [ OMC ]. Trips protege o valor informacional dos produtos e processos manipulados pela biotecnologia e pela tecnologia da informação; mas não pode proteger outros valores, como os valores de uso modernos e tradicionais, e nem o valor da vida, porque eles não cabem no sistema.
> (p. 21)

# Temas que passam despercebidos pela esquerda contemporânea

> E no entanto toda essa transformação que a ênfase na propriedade intelectual acarreta no regime de propriedade e até mesmo no objeto a ser apropriado é muito pouco discutida pelos críticos do capitalismo. Passa despercebido o caráter intrinsecamente predatório de uma cultura e de uma sociedade que começaram a considerar legítimas e justas tanto a redução dos seres vivos à condição de matéria-prima sem valor quanto a pretensão do biotecnólogo de reivindicar para sua atividade “inventiva” a exclusividade da geração de valor. Passa despercebida a desqualificação sumária do “trabalho” da natureza e de todo tipo de trabalho humano, em todas as culturas e sociedades, exceto o trabalho tecnocientífico.
> (p. 21)

# Aceleração tencológica e a noção de "progresso" ainda são tidos como algo positivo

> Ora, se todos nós podemos acompanhar até pelos jornais que a aceleração tecnológica e a aceleração econômica do capitalismo global se fundem num só movimento, nem sempre fica claro o sentido da opção pela máxima aceleração. Os sociólogos, os economistas, mas também os políticos parecem não se dar conta dos efeitos colaterais que a velocidade máxima pode produzir nas relações sociais. De certo modo, continua intacto o mito do século XIX segundo o qual o progresso só traz benefícios e bem-estar, cabendo aos democratas lutar pela sua universalização. E, porque o mito do progresso continua intocado, as forças progressistas não discutem politicamente a tecnologia.
> (p. 22)

# Nazi-fascismo como aceleração total. Tecnicização da vida. Minorias como inimigos (potência de autonomia, freios à aceleração).

~estado suicidário, guerra total em D&G (_Mil platôs 3, 5_)

> Comentando a tese de Ernest Jünger segundo a qual a estratégia do genocídio dos nazistas teria privado a Wehrmacht dos meios de transporte necessários à vitória, [Heiner] Müller observa:
>
> > Ele não entendeu que a doutrina militar dos nazistas repousava sobre o conceito estratégico de aceleração total. O problema não era a Wehrmacht derrotar o Exército Vermelho ou Rommel derrotar Montgomery. Isso era apenas o aspecto superficial, o teatro da guerra. Ao contrário, sua realidade era totalmente econômica e tecnológica. Tratava-se de experimentar a tecnologia, de introduzir a tecnologia no cotidiano, de tecnicizar a vida. Toda tentativa de aceleração total encontra nas minorias seu principal adversário. Pois as minorias sempre representam algo autônomo; elas são um obstáculo à aceleração. As minorias são freios. Daí nasce a necessidade de aniquilá-las, pois elas persistem em sua velocidade própria.
> (p. 22-3)

# Eugenia fascista e engenharia genética como faces de um programa de seleção no capitalismo contemporâneo

> A observação de Müller importa porque aponta o princípio da seleção como um critério novo e interessantíssimo para se pensar a problemática da exclusão e da inclusão no capitalismo global – o que os economistas e políticos estão eufemisticamente chamando de “linha divisória digital” (_digital divide_). Exigida pela aceleração econômica e tecnológica total em curso, a seleção seria um modo de “processar” as categorias sociais e as populações em dois registros. No primeiro, trata-se de neutralizar aquelas que se excluíram ou foram excluídas do movimento total, seja porque o recusavam e a ele resistiam, seja porque se mostraram incapazes de acompanhá-lo, tornando-se então “descartáveis”, para usar as palavras do subcomandante Marcos. No segundo, trata-se de favorecer e estimular aquelas categorias e populações que podem conferir a máxima eficácia à ordem econômica e tecnocientífica, segundo os parâmetros da aceleração total. Assim, Auschwitz seria o emblema negativo da seleção, enquanto a nova eugenia que se constitui com a engenharia genética, a sociobiologia e o neodarwinismo seriam o positivo.
> (p. 23)

# Auschwitz como "altar do capitalismo": valorização da eficiência e do progresso em detrimento da vida

> Como Zygmunt Bauman14 , Müller não vê Auschwitz como um desvio, uma exceção, um surto de irracionalismo, ou mesmo como uma regressão à barbárie, à maneira de Adorno. Para ele, o campo de concentração significa o altar do capitalismo, o último estágio das Luzes e o modelo de base da sociedade tecnológica. As expressões são evidentemente polêmicas, quase inadmissíveis, soando como provocação; mas convém meditar sobre elas, pois sugerem aspectos extremos até então despercebidos da lógica social dominante. Auschwitz seria o altar do capitalismo porque ali o homem é sacrificado em nome do progresso, porque o critério da máxima racionalidade reduz o homem ao seu valor de matéria-prima; seria o último estágio das Luzes, como a realização plena do cálculo por ela inaugurado; e seria, enfim, o modelo de base da sociedade tecnológica porque o extermínio em escala industrial consagra até mesmo na morte a busca de funcionalidade e eficiência, princípios fundamentais do sistema técnico moderno.
> (p. 24)

# Susan George e o relatório Lugano (seleção na contemporaneidade)

> Ora, o comentário do teatrólogo adquire uma inteligibilidade perturbadora quando relacionado com o livro de Susan George _O relatório Lugano_, onde se constata que a lógica do extermínio está maquinando a estratégia neoliberal em curso. Pois se Müller sabia que a estratégia nazista de aceleração total obedecia ao princípio da seleção, isto é, do direito do mais forte, George sabe que a estratégia neoliberal repousa sobre esse mesmo princípio, ao colocar a mesma questão totalitária: quem tem o direito de sobreviver, quem está condenado a desaparecer.
> (p. 24)

> Uma vez colocada a questão da redução de população, o relatório passa a discutir as estratégias para “resolver” o problema dos excluídos por meio do que Müller chama de “limpeza social”. De saída os sistemas genocidas como o Holocausto são considerados estratégias ruins por várias razões: apóiam-se em enorme burocracia, são caros demais e ineficientes, conferem demasiado poder e responsabilidade ao Estado, não passam despercebidos, atraem a ruína e o opróbrio a seus autores. Diz o relatório:
> 
> > O modelo de Auschwitz é o contrário do que precisamos para atingir o objetivo. [...] A seleção das “vítimas” não deve ser responsabilidade de ninguém, senão das próprias “vítimas”. Elas selecionarão a si mesmas a partir de critérios de incompetência, de inaptidão, de pobreza, de ignorância, de preguiça, de criminalidade e assim por diante; numa palavra, elas encontrar-se-ão no grupo dos perdedores 18
> 
> É preciso, portanto, reduzir o número de descartáveis e, para tanto, o relatório propõe uma atualização concertada dos flagelos configurados pelos quatro cavaleiros do Apocalipse: a Conquista, a Guerra, a Fome e a Peste. Vistos nesta perspectiva, os conflitos regionais, as crises, as epidemias e os desmanches que assolam as economias e sociedades do Terceiro Mundo adquirem uma inteligibilidade espantosa, até então irreconhecível. Mas a produção de destruição não tem apenas inspiração bíblica: há também estratégias que nem são João de Patmos nem Thomas Malthus poderiam conceber, porque são preventivas e dependem da política e da tecnologia do século XX – aqui têm lugar os inibidores de reprodução, como as esterilizações em massa, a contracepção forçada etc.
> (p. 27-8)

# Sobre a existência de uma divisa digital e da posição do Brasil no polo negativo

> Ora, nesse contexto é preciso admitir: o problema não é que vai se criar uma linha divisória digital, como parece pensar o presidente do Banco Central, Armínio Fraga 19 . Muito ao contrário, é porque ela já foi criada e continua se fortalecendo permanentemente que o futuro do Brasil está comprometido.
> (p. 29)

# Sobre uma posição de "sociedade pós-catástrofe" do Brasil

> Ora, não por acaso, a construção de um Brasil moderno é uma verdadeira obsessão que atravessa a sociedade inteira, está em todas as mentes e encontra a adesão de todos os setores e classes sociais. Pode-se discordar da maneira como tal construção deve ser feita, mas sua necessidade parece inquestionável. Entretanto cabe perguntar: seria ainda possível construir uma nação? O Brasil continua se vendo como país do futuro e talvez esta seja a razão por que os brasileiros não têm olhos para perceber a ruína moderna que está se construindo. Foi preciso que um sociólogo alemão (Robert Kurz, em seu livro _O colapso da modernização_) nos mostrasse que o esforço desenvolvimentista do Terceiro Mundo não pode mais trazer a prometida modernização da sociedade para que, com ela, descobríssemos que o projeto de futuro já ficou para trás e que vivemos numa “sociedade pós-catástrofe”, em que predomina a dinâmica do desmoronamento. Atentos então aos sinais da catástrofe – aumento do desemprego, da violência e da miséria; desindustrialização e endividamento; desmontagem das instituições e serviços públicos; recuo da presença do Estado nas diferentes regiões; degradação ambiental; devastação na Amazônia e invasão de terras indígenas; desestruturação urbana; papel crescente do tráfico de drogas e do crime organizado na vida das cidades –, atentos principalmente à conversão de parcelas cada vez maiores da população em não-pessoas sociais, isto é, “sujeitos monetários sem dinheiro”, (...)
> (p. 30-1)

# Sobre a impossibilidade de um desenvolvimento ilimitado tecnocientífico e de uma "crise da sociedade de trabalho" em que o mundo todo sofreria os efeitos de uma lógica capitalista da exclusão de parcelas cada vez maiores do mercado de trabalho em Kurz

> Robert Kurz considera que o colapso da modernização explicitou-se primeiro nas sociedades do Terceiro Mundo, mas não se restringe a elas. Segundo ele, a própria derrocada do socialismo no Leste e os problemas enfrentados pelos países do Primeiro Mundo são parte de um mesmo processo que ele denomina “crise da sociedade de trabalho”. Não cabe aqui discutir as teses de Kurz, mas convém lembrar que, em seu entender, a causa da crise é a mesma em todas as partes: pela primeira vez na história o sistema capitalista, agora globalizado, passa a excluir em vez de incluir parcelas cada vez maiores da força de trabalho. Isso porque a concorrência no mercado mundial e o casamento da tecnociência com o capital globalizado impõem um padrão de produtividade tão alto que a própria lógica do sistema acaba tornando-o destrutivo e talvez até mesmo autodestrutivo. Ora, não deixa de ser irônico e paradoxal pensar que justamente quando o capitalismo parece triunfar no mundo inteiro ele precisa entrar em guerra com todas as sociedades e todas as culturas porque a estratégia da aceleração total funde, num só e único movimento, uma racionalidade tecnocientífica que recusa qualquer limitação ao seu desenvolvimento e uma racionalidade econômica que rejeita até mesmo a idéia de qualquer limite para o capital.
> (p. 32-3)

# Sobre uma luta pela vida, o papel da tecnologia nisso e da importância do estudo da tecnociência para a esquerda

> A luta não terminou. Para além das ideologias, para além até mesmo das questões de classe, agora o conflito concerne à própria vida, isto é, à sua defesa ou à sua apropriação. Estão aí o MST [Movimento dos Trabalhadores Rurais Sem Terra] e Chiapas para nos lembrar cotidianamente disso. Ora, se a luta é pela vida, tem que ser incorporada a questão da tecnociência e das novas tecnologias. Várias vezes já me disseram que, enquanto sociólogo da tecnologia, eu me ocupo de questões que não são relevantes para a maioria do povo brasileiro porque esta não tem acesso ao caderno escolar, quanto mais ao computador. Mas defendo a idéia de que é preciso sim discutir politicamente a tecnologia e conhecer as opções tecnológicas possíveis para evitar que elas nos sejam apresentadas como inexoráveis e enfiadas por nossa goela abaixo. Dentro da esquerda, precisamos deixar de lado a ingenuidade quanto ao papel progressista da tecnociência no capitalismo contemporâneo.
> (p. 33)
