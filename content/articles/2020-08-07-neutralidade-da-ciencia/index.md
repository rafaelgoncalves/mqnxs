---
title: Neutralidade da ciência
author: Rafael Gonçalves
date: 2020-08-07
path: "/neutralidade-da-ciencia-Dagnino"
category: fichamento
featuredImage: "../../images/Little_boy.jpg"
srcInfo: Little boy retirado de <a href="https://commons.wikimedia.org/wiki/File:Little_boy.jpg">Wikimedia</a>
published: true
tags: ["Renato Dagnino", "tecnociência", "neutralidade", "fichamento"]
---

No livro *Neutralidade da ciência e determinismo tecnológico*<sup>[[1]](#dagnino)</sup>, Dagnino propõe uma interessante taxonomia das abordagens de estudo da tecnociência. Nela, há primeiramente uma separação entre o "foco" que é dado: C&T ou sociedade, e depois há ainda uma subdivisão entre dois grandes campos em cada uma das super-categorias, totalizando assim 4 categorias de abordagens: neutralidade da ciência, determinismo tecnológico, tese fraca da não-neutralidade e tese forte da não-neutralidade. Neste post, reproduzo alguns trechos e faço alguns comentários sobre a primeira.

### Abordagem com foco na C&T

<blockquote>
De acordo com esta abordagem, a C&T é entendida como infensa ao contexto sociopolítico, como possuindo um desenvolvimento linear em busca da verdade, endogenamente determinado, universal e inexorável, ao longo do qual apenas existe a diferença entre uma tecnologia mais avançada (de ponta, mais eficiente, mais recente) e menos avançada (obsoleta, ineficiente, ultrapassada). É uma concepção evolucionista, uma espécie de darwinismo tecnológico, uma vez que a história é reduzida a um processo em que sobrevivem as tecnologias mais aptas, mais eficientes, mais produtivas.
(<a href="#dagnino">1</a>, p. 35)
</blockquote>

Nesta categoria, há uma autonomia da tecnociência em relação ao "social". A ciência é vista como um processo desinteressado de busca sincera da verdade e a tecnologia - vista ainda como aplicação do saber científico - como produção linear e necessária em direção à maior eficiência. A ideia de progresso inexorável está fortemente vinculada com essa visão.

<blockquote>
C&T seria um assunto técnico e não político; haveria uma barreira virtual que protegeria o ambiente de produção científico-tecnológica do contexto-social, político e econômico. Barreira esta que impediria que os interesses dos atores sociais envolvidos no desenvolvimento da C&T pudessem determinar a trajetória de inovação.
(<a href="#dagnino">1</a>, p. 35)
</blockquote>

É possível perceber que esta forma de pensar C&T questiona quase que imediatamente a cientificidade das humanidades. Ao instaurar a ciência como progresso linear e necessário, qual seria o lugar das múltiplas teorias sociais que nem sempre são compatíveis e na mesma direção? Nem sempre é fácil metrificar algum parâmetro de veracidade absolutamente aceito, e isso é ainda mais forte nas ciências humanas, tanto pela complexidade dos sistemas tratados (sociedades, seres humanos, psicologia, etc.), como pela maior proximidade que os objetos estudados têm em relação ao campo social, econômico, político (proximidade que esse modelo de ciência tenta afastar).

### Ciência e religião

<blockquote>
A idéia de neutralidade do conhecimento científico tem sua origem nas próprias condições de seu surgimento como tal, a partir do século XV, como uma oposição ao conhecimento (ou pensamento) religioso. Este sim era considerado como claramente não neutro, uma vez que tinha por objetivo intervir na realidade social por meio dos fiéis, a ponto de pretender a sua transformação, e converter ou dar combate aos adeptos de outras crenças.
(<a href="#dagnino">1</a>, p. 37)
</blockquote>

Assim, ao mesmo tempo em que o campo do saber científico se funda, é garantida uma separação entre a ciência autônoma e desinteressada como puramente racional e a religião interessada e inseparável do contexto social, econômico, cultural.

<blockquote>
Para muitos, ciência e religião compartilharam o mesmo objetivo: a verdade. A diferença seria que a ciência admite só a autoridade da razão e da experiência, a Palavra da Razão, enquanto que a religião só aceita a Palavra de Deus. A diferença seria a forma como avaliam a verdade e a falsidade. A ciência o faria por meio de argumentos racionais e procedimentos empíricos, conferindo à sua verdade um <em>status</em> privilegiado obtido pela aplicação de um método de certificação, um procedimento racional de justificação.
(<a href="#dagnino">1</a>, p. 38)
</blockquote>

A razão e experiência se tornam juízes absolutos da verdade. Ao substituir Deus pelo "deus Logos" e a fé religiosa pela "fé na razão" e na empiria, a ciência se torna o novo elemento transcendente explicador de todas as coisa.

### Ciência como verdade

<blockquote>
O primado positivista de que a subjetividade devia ser contida dentro dos limites da objetividade e sua tentativa de reproduzir a realidade "assim como ela é" dá força à crença de que a ciência é a expressão de uma verdade absoluta.
(<a href="#dagnino">1</a>, p. 38)
</blockquote>

É natural que, ao conferir à razão estatuto de juiz absoluto das coisas, a subjetividade é colocada com sumetida à sua lógica, pois tudo deve ser explicado nos termos do método científico.

<blockquote>
Nessa visão, o mundo dos fatos seria explicado mediante estruturas, relações, processos e leis subjacentes sem que qualquer juízo de valor intermidiasse a essa explicação.
(<a href="#dagnino">1</a>, p. 38)
</blockquote>

### Neutralidade e determinismo

Se a ciência (e a tecnologia dela derivada) é considerada neutra, seu desenvolvimento é necessário e não está passível de disputa. Esse argumento é potente porque engloba em si mesmo, de forma auto-referente, a ideia de que as coisas não poderiam ser de outra forma.

<blockquote>
A ideia de neutralidade parte de um juízo fundacional difuso, ao mesmo tempo descritivo e normativo, mas abarcante e potente, de que a C&T não se relaciona com o contexto do qual é gerada. Mais do que isso, que permanecer dele sempre isolada é um objetivo e uma regra da "boa ciência". E, finalmente, que ela pode de fato ser isolada.
(<a href="#dagnino">1</a>, p. 38)
</blockquote>

Assim, o método supõe uma separação absoluta entre sociedade e tecnociência, ao mesmo tempo que promove essa separação.

<blockquote>
Ao entender o ambiente de produção científico-tecnológica como separado do contexto social, político e econômico, essa idéia torna impossível a percepção de que os interesses dos atores sociais de alguma forma envolvidos com o desenvolvimento da C&T possam determinar a sua trajetória.
(<a href="#dagnino">1</a>, p. 38)
</blockquote>

<blockquote>
Essa idéia leva à impossibilidade de desenvolvimentos alternativos da C&T que coabitam em um mesmo ambiente. Ou seja, só existe uma única C&T "verdadeira".
(<a href="#dagnino">1</a>, p. 38)
</blockquote>

Colocando-se a tecnociência como neutra, seu desenvolvimento se torna necessário, e então instaura-se um determinismo da ciência e da tecnologia.

<blockquote>
Assim, as contradições se resolveriam naturalmente através de caminhos iluminados pela própria ciência, com novos conhecimentos e técnicas que superariam racionalmente os antigos, sem que se colocassem em questão a ação e os interesses dos atores sociais no processo inovativo.
(<a href="#dagnino">1</a>, p. 39)
</blockquote>

### Natureza, tecnociência e finalidade

<blockquote>
Essa percepção de senso comum, de que o presente é melhor que o passado e que conduzirá a um futuro ainda melhor, em busca de uma finalidade imanente a ser alcançada, está em evidente consonância com a idéia de neutralidade.
(<a href="#dagnino">1</a>, p. 39)
</blockquote>

Essa é uma visão *teleológica* da Natureza. Existe uma finalidade, um caminho a ser percorrido, um lugar a se chegar.

<blockquote>
O desenvolvimento da C&T seria, no plano do conhecimento, manifestação de uma realidade assim percebida. Seria o resultado de seu progressivo desenvolvimento, da contínua descoberta da verdade e, por isso, único, universal, e coerente com o progresso.
(<a href="#dagnino">1</a>, p. 39)
</blockquote>

### Tecnociência e o desenvolvimento da sociedade

A percepção de que os estágios posteriores da sociedade, alavancados pelo desenvolvimento da tecnociência, seriam necessariamente melhores aponta para uma utopia tecnológica.

<blockquote>
Ela entende, igualmente que conhecimentos criados e utilizados por diferentes civilizações poderiam ser apropriados para finalidades quaisquer, e por atores sociais diferentes, a qualquer tempo. Mais do que isso, supõe que a acumulação pura e simples de conhecimentos científico-tecnológicos seria suficiente para garantir o progresso econômico e social a todos. A C&T teria uma apropriação universal, seria um "patrimônio da Humanidade". Em consequencia, uma trajetória de qualidade e "exelência acadêmica" imposta à produção científica e a eficiência e a produtividade da tecnologia, avaliadas geralmente por critérios quantitativos, levariam ao desenvolvimento social.
(<a href="#dagnino">1</a>, p. 39)
</blockquote>

Essa visão - somada à de que as ciências exatas, médicas e tecnológicas seriam mais ciência do que as ciências humanas, os estudos da linguagem e das artes - parece justificar a ideia de que um investimento massivo nas primeiras seriam suficientes para um progresso efetivo da sociedade, [tema que eu já comentei em outro momento](/blog/por-que-incentivar-pesquisas-nas-humanidades-e-nas-artes.html).

<blockquote>
Ela também ensinaria as pessoas a pensar racionalmente, o que levaria ao "comportamento racional" em todas as esferas da atividade. Graças à ciência, a humanidade, ao livrar-se da política, implantaria o domínio da lógica e da razão, em substituição ao da emoção e da paixão, o que faria com que as próprias questões sociais e políticas pudessem ser tratadas de maneira científica, eliminando as disputas irracionais animadas por interesses políticos, e produziria uma sociedade cada vez melhor.
(<a href="#dagnino">1</a>, p. 40)
</blockquote>

### A razão moderna

<blockquote>
O cientificismo compartilha com o positivismo a convicção de que todos os processos - sociais ou físicos - podem ser analisados, entendidos, coisificados, mediante uma colocação científica para encontrar uma solução objetiva e politicamente neutra.
(<a href="#dagnino">1</a>, p. 41)
</blockquote>

Colocada dessa forma, a ideia de ciência moderna que separaria sociedade de um lado e natureza do outro, é considerada a maneira correta de explicar o mundo. Não só dando bases sólidas para se apoiar, mas também conferindo uma superioridade etnocêntrica aos seus criadores em relação às outras culturas - que não praticariam a mesma separação moderna.

<blockquote>
A idéia de modernidade tão cara ao positivismo, é tida como racional na medida em que suas fundações cognitivas - ciência e tecnologia - eram superiores às de qualquer sociedade anterior. De acordo com ele, a racionalidade seria universal, independentemente de condições sociais e históricas. Questionar essa visão não era apenas desafiar a legitimidade da ideia moderna, baseada na separação das esferas da vida social que nas sociedades anteriores se mantiveram indiferenciadas, mas enfraquecer o único ponto de vista confiável a partir do qual se poderiam fazer julgamentos sobre o mundo.
(<a href="#dagnino">1</a>, p. 41)
</blockquote>

### Instrumentalismo

A racionalidade e a eficiência passa a ser a medida não só de questões técnicas, mas também de qualquer âmbito da vida social.

<blockquote>
A concordância em relação a metas é sempre difícil. Mas sendo a eficiência um valor universal, ela seria especialmente adequada para gerar acordos racionais.
(<a href="#dagnino">1</a>, p. 42)
</blockquote>

Solução de todos os problemas, ou distopia tecnocrática?

### Teoria substantiva da tecnologia

<blockquote>
Filósofos como Heidegger, críticos das sociedades "distópicas" em que o progresso técnico é visto como um aumento da eficiência neutro a ponto de converter-se num novo estilo de vida, propõem o que Feenberg (1999b) chama de Teoria Substantiva da tecnologia. Eles rejeitam a noção de que a tecnologia é neutra e apontam que ela é uma estrutura cultural que encarna valores próprios, particulares.
(<a href="#dagnino">1</a>, p. 43)
</blockquote>

### Ciência-conhecimento, ciência-atividade

<blockquote>
Essas características, que dizem respeito à ciência como conhecimento, não poderiam ser estendidas à ciência como uma atividade institucionalizada. Nesse caso, ela seria sempre permeável aos valores e interesses socias e não poderia ser neutra. Essa distinção leva a que se possa postular um caráter não-neutro à ciência como atividade e manter a objetividade científica e o ideal de compromisso com a honestidade intelectual em relação às teorias e outras expressões do saber. Isto é, que a ciência não pode ser neutra como atividade, mas que deve sê-lo como saber.
(<a href="#dagnino">1</a>, p. 43)
</blockquote>

Sobre essa abordagem, como decidir onde o conhecimento termina e a prática começa? Me parece que a barreira nesse caso é nebulosa e necessitaria de um arbítrio subjetivo.

### Sociologia da Ciência (Merton)

A Sociologia da Ciência surge nos EUA como reposta à Sociologia do Conhecimento, que conferia um caráter social não só à prática científica, mas também ao seu conteúdo.

<blockquote>
[A] ciência é entendida não como um processo individual, mas social (no sentido do coletivo), que envolve algum tipo de interação entre seus protagonistas e que se desenvolve no interior de uma organização (instituição).
(<a href="#dagnino">1</a>, p. 44-45)
</blockquote>

Essa abordagem estabelece um lugar à sociologia nos estudos de C&T, ao colocar que existem atores e instituições envolvidas no processo tecnocientífico. Assim, passa a ser papel da sociologia estudar a forma do fazer tecnocientífico, sem entretanto questionar os aspectos relativos ao seu conteúdo.

<blockquote>
O método e a disposição do cientista em despir-se de juízos de valor seriam a garantia de que a ciência se manteria infensa às influências políticas e sociais, que seus resultados seriam universais, que poderiam ser apropriados por qualquer sociedade, que seriam cumulativos; que a ciência estaria em permanente evolução.
(<a href="#dagnino">1</a>, p. 45)
</blockquote>

Portanto, se puder ser garantida a separação entre as esferas natural-objetiva e social-subjetiva, será garantido o caráter neutro e absoluto da ciência-conhecimento.

<blockquote>
Mas a suposta neutralidade defendida por Merton e uma confusão entre o normativo (o que deveria ser) e o descritivo (o que é) terminam dificultando aos cientistas a percepção de que as influências "externas" são inevitáveis. E isso, ao reforçar o determinismo científico-tecnológico e a inviabilidade de construção de alternativas, favorece a instrumentalização da C&T no capitalismo como um mero mecanismo de acumulação do capital.
(<a href="#dagnino">1</a>, p. 46)
</blockquote>

### Compromisso método-autonomia (Hugh Lacey)

<blockquote>
A autonomia seria ao mesmo tempo um compromisso com a sociedade e uma proposta política. A ciência seria decidida pelos cientista, que deveriam usar os recursos que a sociedade disponibilizaria para suas pesquisas na busca da verdade, sempre entendida como neutra.
(<a href="#dagnino">1</a>, p. 47)
</blockquote>


Essa autonomia, em conjunto com um método rigoroso, permitiriam um avanço puro da ciência e da busca pela verdade. 

<blockquote>
Ao garantir o crescimento do conhecimento científico, que se daria sempre pelo caminho da neutralidade e da imparcialidade, asseguradas pelo método e pelas práticas controladas inerentes à ciência, a autonomia daria livre curso à sua própria dinâmica interna. Garantiria à ciência (e aos cientistas) a prerrogativa de definir seus próprios problemas, de fazer suas próprias perguntas, identificar suas próprias prioridades de pesquisa, permitindo sua busca incessante por desvendar as leis da ordem subjacente ao mundo dos fatos, impedindo qualquer intrusão do mundo dos valores e dos interesses externos que, inevitavelmente, retardariam essa busca.
(<a href="#dagnino">1</a>, p. 48)
</blockquote>

### Autonomia e interesse

<blockquote>
Na outra ponta, mas funcionando na mesma direção, a menção constante a situações em que a ciência se subordina a valores e interesses externos -  que vão desde o episódio de Lysenko e a estúpida teimosia dos criacionistas até o comprometimento com o segredo industrial necessário à geração do lucro ou com os objetivos militares - reforça a idéia da autonomia como condição para progresso da ciência.
(<a href="#dagnino">1</a>, p. 49)
</blockquote>

### Programa Forte de Edimburgo

<blockquote>
Os desdobramentos posteriores à contribuição de Merton tiveram um marco importante no surgimento do Programa Forte de Edimburgo, em que autores como Bloom, Barner e Woolgar voltam a focar as questões relativas ao conteúdo do conhecimento tentando explicar como este - e o próprio conceito de verdade - era influenciado pela interação e pela negociação entre os atores que o produziam.
(<a href="#dagnino">1</a>, p. 50)
</blockquote>

Essa abordagem reestabelece um lugar ao sociólogo que seria incumbido de estudar não só a prática tecnocientífica, mas também possíveis "vazamentos" que alterariam seu conteúdo.

### A Nova Sociologia do Conhecimento

<blockquote>
A hipótese construtivista defendida pela Nova Sociologia do Conhecimento, de que os processos cognitivos e os processos sociais devem ser analisados em conjunto, de modo que possam ser estabelecidas as possíveis relações entre eles, não chega a levar a um abandono da questão central que nos ocupa: a idéia de neutralidade da ciência.
(<a href="#dagnino">1</a>, p. 51)
</blockquote>

<br/>
<section class="footnotes" role="doc-endnotes">
<hr />
<ol>
<li id="dagnino" role="doc-endnote">DAGNINO, Renato. <em>Neutralidade da ciência e determinismo tecnológico: um debate sobre a tecnociência</em>. Unicamp, 2008.</li>
</ol>
</section>
