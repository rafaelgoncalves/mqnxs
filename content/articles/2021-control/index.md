---
title: Post-scriptum sobre as sociedades de controle
date: 2021-10-09
path: "/sociedades-de-controle"
category: fichamento
tags: [Gilles Deleuze, modulação, fichamento, controle, capitalismo contemporâneo, dividual, analógico, digital, trabalho]
featuredImage: "controller.jpg"
srcInfo: <a href="https://commons.wikimedia.org/wiki/File:Minimoog_Voyager_(left_panel).jpg">Retirado de wikimedia</a>
published: true
---

Fichamento de "Post-scriptum sobre as sociedades de controle"[^1] escrito por Gilles Deleuze em 1990.

[^1]: DELEUZE, Gilles, Post scriptum sobre as sociedades de controle. In. *Conversações*., Rio de Janeiro: Editora 34, 1992, p. 219–226.

# Sociedades disciplinares

> Foucault situou as sociedades disciplinares nos séculos VIII e XIX; atingem seu apogeu no início do século XX.
> Elas procedem à organização dos grandes meios de confinamento. O indivíduo não cessa de passar de um espaço
> fechado a outro, cada um com suas leis: primeiro a família, depois a escola ("você não está mais na sua família"),
> depois a caserna ("você não está mais na escola"), depois a fábrica, de vez em quando o hospital, eventualmente a
> prisão, que é o meio de confinamento por excelência. É a prisão que serve de modelo analógico: a heroína de
> Europa 51 pode exclamar, ao ver operários, "pensei estar vendo condenados...".
> (p. 219)

# Crise das sociedades disciplinares

> Encontramo-nos numa crise generalizada de todos os meios de confinamento, prisão, hospital, fábrica, escola,
> família. A família é um "interior", em crise como qualquer outro interior, escolar, profissional, etc. Os ministros
> competentes não param de anunciar reformas supostamente necessárias. Reformar a escola, reformar a indústria,
> o hospital, o exército, a prisão; mas todos sabem que essas instituições estão condenadas, num prazo mais ou
> menos longo. Trata-se apenas de gerir sua agonia e ocupar as pessoas, até a instalação das novas forças que se
> anunciam.
> (p. 220)

# As sociedades de controle 

> São as _sociedades de controle_ que estão substituindo as sociedades disciplinares. "Controle" é o nome
> que Burroughs propõe para designar o novo monstro, e que Foucault reconhece como nosso futuro próximo. Paul
> Virillo também analisa sem parar as formas ultra rápidas de controle ao ar livre, que substituem as antigas
> disciplinas que operavam na duração de um sistema fechado. Não cabe invocar produções farmacêuticas
> extraordinárias, formações nucleares, manipulações genéticas, ainda que elas sejam destinadas a intervir no novo
> processo. Não se deve perguntar qual é o regime mais duro, ou o mais tolerável, pois é em cada um deles que se
> enfrentam as liberações e as sujeições. Por exemplo, na crise do hospital como meio de confinamento, a
> setorização, os hospitais-dia, o atendimento a domicílio puderam marcar de início novas liberdades, mas também
> passaram a integrar mecanismos de controle que rivalizam com os mais duros confinamentos. Não cabe temer ou
> esperar, mas buscar novas armas.
> (p. 220)

# Confinamento (analógico)

> Os diferentes internatos ou meios de confinamento pelos quais passa o indivíduo são variáveis independentes:
> supõe-se que a cada vez ele recomece do zero, e a linguagem comum a todos esses meios existe, mas é _analógica_.
> (p. 210-221)

# Controle (numérico/digital)

> Ao passo que os diferentes modos de controle, os controlatos, são variações inseparáveis, formando um sistema
> de geometria variável cuja linguagem é _numérica_ (o que não quer dizer necessariamente binária).
> (p. 221)

# Molde e modulação

> Os
> confinamentos são moldes, distintas moldagens, mas os controles são uma modulação, como uma moldagem
> auto-deformante que mudasse continuamente, a cada instante, ou como uma peneira cujas malhas mudassem de
> um ponto a outro.
> (p. 221)

# Fabrica e empresa

> Isto se vê claramente na questão dos salários: a fábrica era um corpo que levava suas forças
> internas a um ponto de equilíbrio, o mais alto possível para a produção, o mais baixo possível para os salários;
> mas numa sociedade de controle a empresa substituiu a fábrica, e a empresa é uma alma, um gás. Sem dúvida a
> fábrica já conhecia o sistema de prêmios mas a empresa se esforça mais profundamente em impor uma
> modulação para cada salário, num estado de perpétua metaestabilidade, que passa por desafios, concursos e
> colóquios extremamente cômicos. Se os jogos de televisão mais idiotas têm tanto sucesso é porque exprimem
> adequadamente a situação de empresa.
>  (p. 221)

# Corpo trabalhista e rivalidade

> A fábrica constituía os indivíduos em um só corpo, para a dupla
> vantagem do patronato que vigiava cada elemento na massa, e dos sindicatos que mobilizavam uma massa de
> resistência; mas a empresa introduz o tempo todo uma rivalidade inexpiável como sã emulação, excelente
> motivação que contrapõe os indivíduos entre si e atravessa cada um, dividindo-o em si mesmo.
> (p. 221)

# Modulação por mérito na escola-empresa

> O princípio
> modulador do "salário por mérito" tenta a própria Educação nacional: com efeito, assim como a empresa substitui
> a fábrica, a formação permanente tende a substituir a escola, e o controle contínuo substitui o exame. Este é o
> meio mais garantido de entregar a escola à empresa.
> (p. 221)

# Nunca se termina nada nas sociedades de controle

> Nas sociedades de disciplina não se parava de recomeçar (da escola à caserna, da caserna à fábrica), enquanto nas
> sociedades de controle nunca se termina nada, a empresa, a formação, o serviço sendo os estados metaestáveis e
> coexistentes de uma mesma modulação, como que de um deformador universal.
> (p. 222)

# Assinatura e matrícula na sociedade disciplinar

> As sociedades disciplinares têm dois
> pólos: a assinatura que indica o indivíduo, e o número de matrícula que indica sua posição numa massa. É que as
> disciplinas nunca viram incompatibilidade entre os dois, e é ao mesmo tempo que o poder é massificante e
> individuante, isto é, constitui num corpo único aqueles sobre os quais se exerce, e molda a individualidade de
> cada membro do corpo (Foucault via a origem desse duplo cuidado no poder pastoral do sacerdote - o rebanho e
> cada um dos animais - mas o poder civil, por sua vez, iria converter-se em "pastor" laico por outros meios).
> (p. 222)

# Cifra nas sociedades de controle

> Nas
> sociedades de controle, ao contrário, o essencial não é mais uma assinatura e nem um número, mas uma cifra: a
> cifra é uma senha, ao passo que as sociedades disciplinares são reguladas por palavras de ordem (tanto do ponto
> de vista da integração quanto da resistência). A linguagem numérica do controle é feita de cifras, que marcam o
> acesso à informação, ou a rejeição. Não se está mais diante do par massa-indivíduo. Os indivíduos tornaram-se
> "dividuais", divisíveis, e as massas tornaram-se amostras, dados, mercados ou "bancos".
> (p. 222)

# Dinheiro lastrado na sociedade disciplinar e não-lastrado na de controle

>  É o dinheiro que talvez
> melhor exprima a distinção entre as duas sociedades, visto que a disciplina sempre se referiu a moedas cunhadas
> em ouro - que servia de medida padrão -, ao passo que o controle remete a trocas flutuantes, modulações que
> fazem intervir como cifra uma percentagem de diferentes amostras de moeda. A velha toupeira monetária é o
> animal dos meios de confinamento, mas a serpente o é das sociedades de controle.
> (p. 222)

# Humano discontínuo x ondulatório

> Passamos de um animal a
> outro, da toupeira à serpente, no regime em que vivemos, mas também na nossa maneira de viver e nas nossas
> relações com outrem. O homem da disciplina era um produtor descontínuo de energia, mas o homem do
> controleé antes ondulatório, funcionando em órbita, num feixe contínuo.
> (p. 222-223)

# Determinação social das máquinas

> É fácil fazer corresponder a cada sociedade certos tipos de máquina, não porque as máquinas sejam
> determinantes, mas porque elas exprimem as formas sociais capazes de lhes darem nascimento e utilizá-las.
> (p. 223)

# Máquinas de cada sociedade

> As
> antigas sociedades de soberania manejavam máquinas simples, alavancas, roldanas, relógios; mas as sociedades
> disciplinares recentes tinham por equipamento máquinas energéticas, com o perigo passivo da entropia e o
> perigo ativo da sabotagem; as sociedades de controle operam por máquinas de uma terceira espécie, máquinas de
> informática e computadores, cujo perigo passivo é a interferência, e o ativo a pirataria e a introdução de vírus.
> (p. 223)

# Mutação do capitalismo

> Não é uma evolução tecnológica sem ser, mais profundamente, uma mutação do capitalismo. É uma mutação já
> bem conhecida que pode ser resumida assim: o capitalismo do século XIX é de concentração, para a produção, e
> de propriedade. Por conseguinte, erige a fábrica como meio de confinamento, o capitalista sendo o proprietário
> dos meios de produção, mas também eventualmente proprietário de outros espaços concebidos por analogia (a
> casa familiar do operário, a escola). Quanto ao mercado, é conquistado ora por especialização, ora por
> colonização, ora por redução dos custos de produção.
> (p. 223)

> Mas atualmente o capitalismo não é mais dirigido para a
> produção, relegada com frequência à periferia do Terceiro Mundo, mesmo sob as formas complexas do têxtil, da
> metalurgia ou do petróleo. É um capitalismo de sobre-produção. Não compra mais matéria-prima e já não vende
> produtos acabados: compra produtos acabados, ou monta peças destacadas. O que ele quer vender são serviços, e
> o que quer comprar são ações. Já não é um capitalismo dirigido para a produção, mas para o produto, isto é, para
> a venda ou para o mercado. Por isso ele é essencialmente dispersivo, e a fábrica cedeu lugar à empresa. A família,
> a escola, o exército, a fábrica não são mais espaços analógicos distintos que convergem para um proprietário,
> Estado ou potência privada, mas são agora figuras cifradas, deformáveis e transformáveis, de uma mesma
> empresa que só tem gerentes. Até a arte abandonou os espaços fechados para entrar nos circuitos abertos do
> banco. As conquistas de mercado se fazem por tomada de controle e não mais por formação de disciplina, por
> fixação de cotações mais do que por redução de custos, por transformação do produto mais do que por
> especialização da produção. A corrupção ganha aí uma nova potência. O serviço de vendas tornou-se o centro ou
> a "alma" da empresa. Informam-nos que as empresas têm uma alma, o que é efetivamente a notícia mais
> terrificante do mundo. O marketing é agora o instrumento de controle social, e forma a raça impudente dos
> nossos senhores.
> (p. 223-224)

# Humano confinado x humano endividado

> O controle é de curto prazo e de rotação rápida, mas também contínuo e ilimitado, ao passo que
> a disciplina era de longa duração, infinita e descontínua. O homem não é mais o homem confinado, mas o
> homem endividado. É verdade que o capitalismo manteve como constante a extrema miséria de três quartos da
> humanidade, pobres demais para a dívida, numerosos demais para o confinamento: o controle não só terá que
> enfrentar a dissipação das fronteiras, mas também a expliosão dos guetos e favelas.
> (p. 224)

# Governamentabilidade algoritmica (modulação universal)

> Não há necessidade de ficção científica para se conceber um mecanismo de controle que dê, a cada instante, a
> posição de um elemento em espaço aberto, animal numa reserva, homem numa empresa (coleira eletrônica). Félix
> Guattari imaginou uma cidade onde cada um pudesse deixar seu apartamento, sua rua, seu bairro, graças a um
> cartão eletrônico (dividual) que abriria as barreiras; mas o cartão poderia também ser recusado em tal dia, ou
> entre tal e tal hora; o que conta não é a barreira, mas o computador que detecta a posição de cada um, lícita ou ilícita, e opera uma modulação universal.
> (p. 224-225)

# Crise das instituições

> O que conta é que estamos no início de alguma coisa. No regime
> das prisões: a busca de penas "substitutivas", ao menos para a pequena delinqüência, e a utilização de coleiras
> eletrônicas que obrigam o condenado a ficar em casa em certas horas. No regime das escolas: as formas de
> controle contínuo, avaliação contínua, e a ação da formação permanente sobre a escola, o abandono
> correspondente de qualquer pesquisa na Universidade, a introdução da "empresa" em todos os níveis de
> escolaridade. No regime dos hospitais: a nova medicina "sem médico nem doente", que resgata doentes
> potenciais e sujeitos a risco, o que de modo algum demonstra um progresso em direção à individuação, como se
> diz, mas substitui o corpo individual ou numérico pela cifra de uma matéria "dividual" a ser controlada. No
> regime da empresa: as novas maneiras de tratar o dinheiro, os produtos e os homens, que já não passam pela
> antiga forma-fábrica. São exemplos frágeis, mas que permitiriam compreender melhor o que se entende por crise
> das instituições, isto é, a implantação progressiva e dispersa de um novo regime de dominação.
> (p. 225)

# Questões levantadas

> Uma das
> questões mais importantes diria respeito à inaptidão dos sindicatos: ligados, por toda sua história, à luta contra
> disciplinas ou nos meios de confinamento, conseguirão adaptar-se ou cederão o lugar a novas formas de
> resistência contra as sociedades de controle? Será que já se pode apreender esboços dessas formas por vir,
> capazes de combater as alegrias do marketing? Muitos jovens pedem estranhamente para serem "motivados", e
> solicitam novos estágios e formação permanente; cabe a eles descobrir a que estão sendo levados a servir, assim
> como seus antecessores descobriram, não sem dor, a finalidade das disciplinas. Os anéis de uma serpente são
> ainda mais complicados que os buracos de uma toupeira.
> (p. 225-226)
