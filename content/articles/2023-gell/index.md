---
title: Tecnologia e magia
tags:
    - Alfred Gell
    - tecnologia
    - técnica
    - agência
    - antropologia
    - tecnologia de encantamento
    - fichamento

date: 2023-05-04
category: fichamento
path: "/tecnologia-e-magia"
featuredImage: "gell.jpg"
srcInfo: <a href="https://www.youtube.com/watch?v=_NhgCyGdT4c">Alfred Gell</a>
published: true
---

Fichamento de "Technology and magic"[^1] de Alfred Gell.

[^1]: GELL, Alfred. Technology and Magic. **Anthropology Today** 4(2):6-9. 1988.

# Desenvolvimento tecnológico sem precedentes pelo humano (apesar de não ser um monopólio desta espécie - vide uso de ferramentas por chimpanzés)

> ‘Technological’ capabilities are one of the distinguishing features of our species, and have been since a very early stage in evolution, if not from the very beginning. It is no longer possible to claim ‘tool using’ as an uniquely ‘human’ characteristic, because there are distinct tooluse traditions among apes, especially chimpanzees, and rather more rudimentary examples of tool-use among some other species as well. Human beings, however, have elaborated ‘technological’ means of realizing their intentions to an unprecedented degree. But what is ‘technology’? and how does it articulate to the other species characteristics we possess?
> (p. 6)

# Problema mal colocado: tecnologia como meio de subsistência (conseguir comida, por exemplo). Tecnologia não é oposto a magia, também não é idêntico a ferramenta, nem esta com atividades de subsistência.

> The answers which have been suggested to this question have suffered from a bias arising from the misconceived notion that the obtaining of subsistence necessities from the environment is the basic problem which technology enables us to surmount. Technology is identified with ‘tools’ and ‘tools’ with artefacts, like axes and scrapers, which are presumed to have been imported in the ‘food quest’. This ‘food quest’ has been imagined as a serious, life-or-death, business, and the employment of technology as an equally ‘serious’ affair. _Homo technologicus_ is a rational, sensible, creature, not a mythopoeic or religious one, which he only becomes once he abandons the search for “technical’ solutions to his problems and takes off into the realms of fantasy and empty speculation.
> (p. 6)

> But this opposition between the technical and the magical is without foundation. Technology is inadequately understood if it is simply identified with tool-use, and tool-use is inadequately understood if it is identified with subsistence activity.
> (p. 6)

# Tecnologia como formas de relação social que fazem com que seja necessária a produção, a distribuição e o consumo de bens e serviços por meio de processos 'técnicos'.

> At the very minimum, technology not only consists of the artefacts which are employed as tools, but also includes the sum total of the kinds of knowledge which make possible the invention, making and use of tools. But this is not all. ‘Knowledge’ does not exist except in a certain social context. Technology is coterminous with the various networks of social relationships which allow for the transmission of technical knowledge, and provide the necessary conditions for cooperation between individuals in technical activity. But one cannot stop even at this point, because the objectives of technical production are themselves shaped by the social context. Technology, in the widest sense, is those forms of social relationships which make it socially necessary to produce, distribute and consume goods and services using ‘techcal' processes.
> (p. 6) 

# Técnica como invenção (ingenious). Técnica como "circuitividade": ponte entre elementos dados e um resultado esperado.

> What distinguishes ‘technique’ from non-technique is a certain degree of _circuitousness_ in the achievement of any given objective. It is not so much that technique has to be learned, as that technique has to be ingenious. Techniques form a bridge, sometimes only a simple one, sometimes a very complicated one, between a set of ‘given’ elements (the body, some raw materials, some environmental features) and a goal-state which is to be realized making use of these givens. The given elements are rearranged in an intelligent way so that their causal properties are exploited to bring about a result which is improbable except in the light of this particular intervention.
> (p. 6)

# Meios técnicos são formas indiretas de conseguir um resultado desejado. Grau de tecnicalidade como proporcional ao número e complexidade de passos para se obter o resultado. Tecnologia como sistema.

> Technical means are roundabout means of securing some desired result. The degree of technicality is proportional to the number and complexity of the steps which link the initial givens to the final goal which is to be achieved. Tools, as extensions of the body which have to be prepared before they can be used, are an important category of elements which ‘intervene’ between a goal and its realization. But not less ‘technical’ are those bodily skills which have to be acquired before a tool can be used to good effect. Some tools, such as a baseball bat, are exceptionally rudimentary, but require a prolonged (i.e. circuitous) learning-process, in appropriate learning settings, before they can be deployed to much purpose. Highly ‘technical’ processes combine many elements, artefacts, skills, rules of procedure, in an claborate sequence of purposes or sub-goals, each of which must be attained in due order before the final result can be achieved. It is this elaborate structure of intervening steps, the steps which enable one to obtain result X, in order to obtain Y, in order to (finally) obtain Z, which constitute technology as a ‘system’.
> (p. 6)

# Tecnologias de produção: formas indiretas de garantir 'coisas' que nós pensamos que precisamos

> The _ﬁrst_ of these technical systems, which can be called the ‘Technology of Production’, comprises technology as it has been conventionally understood, i.e. roundabout ways of securing the ‘stuff’ we think we need; food, shelter, clothing, manufactures of all kinds. I would include here the production of signals, i.e. communication. This is relatively uncontroversial and no more need be said about it at this point.
> (p. 6-7)

# Tecnologias de reprodução: tecnologias de parentesco (kinship)

> The _second_ of these technical systems I call the ‘Technology of Reproduction’. This technical system is more controversial, in that under this heading I would include most of what conventional anthropology designates by the word ‘kinship’.
> (p. 7)

> It must occur to anyone, nonetheless, who makes the comparison between human and animal societies, that human societies go to extreme lengths to secure specific patterns of matings and births. Once infants are born, their care and socialization is conducted in a technically elaborated way, making use of special devices such as cradles, slings, swaddling-boards, etc., and later on, toy weapons, special educational paraphernalia and institutions, and so on. The reproduction of society is the consequence of a vast amount of very skilled manipulation on the part of those with interests at stake in the process. Human beings are bred and reared under controlled conditions which are technically managed, so as to produce precisely those individuals for whom social provision has been made.
> (p. 7)

# Tecnologias de parentesco em outros animais e a domesticação como tecnologia de reprodução. Humanos como animais autodomesticados.

> Of course, animals also engage in purposive action in order to intervene in reproductive processes, securing and defending mates, succouring their young, and so forth. Sometimes they seem to be quite cunning about it. I do not want to draw any hard and fast line between human and animal kinship here. But what I would suggest is that the really telling analogies between human and animal Kinship systems are not to be found among wild populations of animal species, but among domesticated animals, such as horses and dogs, whose breeding behaviour, and social learning, human beings have learned to control using many of the same techniques as human beings use on themselves, with very much the same goals in view, We are (self-) domesticated animals; our animal analogues are the other domesticated animals.
> (p. 7)

> Domesticated varieties of animals are biddable, docile, creatures, because we have made them so. And so are we. The vaunted human attributes of teachability, flexibility—a kind of permanent childlike acceptance-are traits which have been evolved, not in the course of mighty struggles against the hostile forces of nature, but adapting to the demand for a more and more ‘domesticable’ human being. This is the phenotype which has been awarded maximum reproductive opportunities, and which now predominates, not because it has been ‘selected’ by nature, but because it selected itself. 
> (p. 7)

# Tecnologias de reprodução dependem de outras técnicas

> The patterns of social arrangements which we identify as ‘kinship systems” are a set of technical strategies for managing our reproductive destiny via an elaborate sequence of purposes. Accordingly, the whole domain of kinship has to be understood primarily as a technology, just as one would see horse-breeding and horse-breaking, or dog-breeding and dog-training, as ‘technical’ accomplishments. But how do we secure the acquiescence of horses and dogs in our intentions, apart from special breeding programmes, so as to secure a supply of tractable animals? Evidently, it is by exploiting natural biases in horse and dog psychology; in other words, by the artful use of whips, sugar-lumps, smacks, caresses, etc., all of which we can deliver because we possess hands, and know how to use them on animals all the better because we continually use them on one another.
> (p. 7)

# Tecnologias de encantamento como formas de controlar pensamentos e ações de outros seres humanos (~manipulação psicológica)

> Here we enter the domain of the _third_ of our three technologies, which I will call the “Technology of Enchantment’. Human beings entrap animals in the mesh of human purposes using an array of psychological techniques, but these are primitive by comparison with the psychological weapons which human beings use to exert control over the thoughts and actions of other human beings. The technology of enchantment is the most sophisticated that we possess.
> (p. 7)

> Under this heading I place all those technical strategies, especially art, music, dances, rhetoric, gifts, etc., which human beings employ in order to secure the acquiescence of other people in their intentions or projects. These technical strategies—which are, of course, practised reciprocally-exploit innate or derived psychological biases so as to enchant the other person and cause him/her to perceive social reality in a way favourable to the social interests of the enchanter. It is widely agreed that characteristically human ‘intelligence’ evolved, not in response to the need to develop superior survival strategies, but in response to the complexity of human social life, which is intense, multiplex, and very fateful for the individual. Superior intelligence manifests itself in the technical strategies of enchantment, upon which the mediation of social life depends. The manipulation of desire, terror, wonder, cupidity, fantasy, vanity, an inexhaustible list of human passions, offers an equally inexhaustible field for the expression of technical ingenuity.

# Magia como aspecto da tecnologia. Magia como simbólica (em oposição à tecnologia, demonstrativamente causal). Aspecto magico da tecnologia como um problema.
 
> Magic is, or was, clearly an aspect of each of the three technologies I have identified, i.e. the technologies of production, reproduction, and psychological manipulation, or ‘enchantment’. But magic is different from these technologies, each of which involves the exploitation of the causal properties of things and the psychological dispositions of people, which are numbered, of course, among their causal properties. Whereas magic is ‘symbolic’. Naturally, in stating this, I am conscious that there has been a prolonged debate about magic, and that not everybody agrees that magic is ‘symbolic’ at all; since it can be interpreted as an attempt to employ spirits or quasi-physical magical powers to intervene (causally) in nature. There is abundant native testimony to support this view, which is often the correct one to take from the standpoint of cultural interpretation, since nothing prevents people from holding at least some mistaken causal beliefs. However, from an observer’s point of view, there is a distinction, in that efficacious technical strategies demonstrably exploit the causal properties of things in the sequence of purposes, whereas magic does not. The evolutionary survival value of the magical aspects of technical strategies is, therefore, a genuine problem.
> (p. 7)
 
# Pensamento mágico como formalizador/codificador de características estruturais de uma atividade técnica

> I take the view that ‘magic’ as an adjunct to technical procedures persists because it serves ‘symbolic’ ends, that is to say, cognitive ones. Magical thought formalizes and codifies the structural features of technical activity, imposing on it a framework of organization which regulates each successive stage in a complex process.
> (p. 7-8)

> If one examines a magical formula, it is often seen that a spell or a prayer does little more than identify the activity which is being engaged in and defines a criterion for ‘success’ in it. ‘_Now I am planting this garden. Let it be so productive that I will not be able to harvest all of it. Amen_’. Such a spell is meaningless by itself, and it only fulfils its technical role in the context of a magical system in which each and every gardening procedure is accompanied by a similar spell, so that the whole sequence of spells constitutes a complete cognitive plan of ‘gardening’
> (p. 8)

# Mágia como comentário sobre atividade técnica (brincadeira)

> Magic consists of a symbolic ‘commentary’ on technical strategies in production, reproduction, and psychological manipulation. I suggest that magic derives from play. When children play, they provide a continuous stream of commentary on their own behaviour. This commentary frames their actions, divides it up into segments, defines momentary goals, and so on. It seems that this superimposed organizational format both guides imaginative play as it proceeds, and also provides a means of internalizing it and recalling it, as well as raw materials for subsequent exercises in innovation and recombination, using previously accumulated materials in new configurations. Not only does the basic format of children’s play-commentary (now I am doing this, now I am doing that, and now this will happen...) irresistibly recall the format of spells, but the relation between reality and commentary in play and in magic-making remain essentially akin; since the play-commentary invariably idealizes the situation, going beyond the frontiers of the merely real. When a child asserts that he is an aeroplane (with arms extended, and the appropriate sound effects and swooping movements) the commentary inserts the ideal in the real, as something which can be evoked, but not realized. But the unrealizable transformation of child into aeroplane, while never actually confused with reality, does nonetheless set the ultimate goal towards which play can be oriented, and in the light of which it is intelligible and meaningful.
> (p. 8)

> The same is true of magic, which sets an ideal standard, not to be approached in reality, towards which practical technical action can nonetheless be oriented.
> (p. 8)

# Inovação na brincadeira (contínuo) e na tecnologia (tentativas de realizar fatos técnicos até então tidos como mágicos)

> There is another feature which play and technology share. Technology develops through a process of innovation, usually one which involves the re-combination and re-deployment of a set of existing elements or procedures towards the attainment of new objectives. Play also demonstrates innovativeness—in fact, it does so continuously, whereas innovation in technology is a slower and more difficult process. Innovation in technology does not usually arise as the result of the application of systematic thought to the task of supplying some obvious technical ‘need’, since there is no reason for members of any societies to feel ‘needs’ in addition to the ones they already know how to fulfil. Technology, however, does change, and with changes in technology, new needs come into existence. The source of this mutability, and the tendency towards ever-increasing elaboration in technology must, I think, be attributed, not to material necessity, but to the cognitive role of ‘magical’ ideas in providing the orienting framework within which technical activity takes place. Technical innovations occur, not as the result of attempts to supply wants, but in the course of attempts to realize technical feats heretofore considered ‘magical’.
> (p. 8)

# Exemplo: pescaria com pipa como mito  (técnica ~ magia)

> This fishing technique exemplifies perfectly the concept of roundaboutness which I have emphasized al ready. But it also suggests very strongly the element of fantasy which brings technical ideas to fruition. Indeed if one encountered ‘kite-fishing’ as a myth, rather thar as a practice, it would be perfectly susceptible to LéviStraussian myth-analysis. There are three elements: firstly, the spider’s web, which comes from dark places inside the earth (caves); secondly, the kite, which is a birc which flies in the sky, and finally, there is the fish whick swims in the water. These three mythemes are broughi into conjunction and their contradictions are resolved ir a final image, the ‘fish with its jaws stuck together’ jusl like Asdiwal, stuck half-way up a mountain and turned to stone. One does not have to be a structuralist aficionado in order to concede that here a magical, mythopoeic. story can be realized as a ‘practical’ technique for catching fish.
> (p. 8)

# Magia como tecnologia ideal (sem custo de oportunidade)

> This leads me to one further observation on the relation between magic and technology. I have so far described magic as an ‘ideal’ technology which orients practical technology and codifies technical procedures at the cognitive-symbolic level. But what would be the characteristics of an ‘ideal’ technology? An ‘ideal’ technical procedure is one which can be practiced with zero opportunity costs. Practical technical procedures, however efficient, always do ‘cost’ something, not necessarily on money terms but in terms of missed opportunities to devote time, effort and resources to alternative goals, or alternative methods of achieving the same goal. The defining feature of ‘magic’ as an ideal technology is that it is ‘costless’ in terms of the kind of drudgery, hazards and investments which actual technical activity inevitably requires. Production ‘by magic’ is production minus the disadvantageous side-effects, such as struggle, effort, etc.
> (p. 8-9)

# Exemplo de jardins descritos por Malinowski. A efetividade da tecnologia não-mágica é que permite uma versão idealizada da tecnologia (imaginativamente convincente)

> Of course, real gardens are not quite so spectacular, though the ever-presence of these images of an ideal garden must be a major factor in focusing gardeners’ minds on taking all practical steps to ensure that their gardens are better than they might otherwise be. However, if one considers the litanies of the garden magician a little more closely, one realizes that the garden being celebrated with so much fine language is, in effect, not a garden situated in some never-never land, but the garden which is actually present, which is mentioned and itemized in very minute, concrete, detail. For instance, each of the twenty-odd kinds of post or stick which is used to train yam creepers is listed, as are all the different cultigens, and all their different kinds of shoots and leaves, and so on. It is apparent that the real garden and its real productivity are what motivates the imaginary construction of the magical garden. It is because nonmagical technology is effective, up to a point, that the idealized version of technology which is embodied in magical discourse is imaginatively compelling.
> (p. 9)

> In other words, it is technology which sustains magic, even as magic inspires fresh technical efforts. The magical apotheosis of ideal, costless, production, is to be attained technically, because magical production is only a very flattering image of the production which is actually achievable by technical means. Hence, in practice, the pursuit of technical efficiency through intelligent effort coincides with the pursuit of the ideal of ‘costless’ production adumbrated in magical discourse. And this observation can lead to a conclusion concerning the fate of magic in modern societies, which no longer acknowledge magic specifically, yet are dominated by technology as never before.
> (p. 9)

# Magia na contemporaneidade: anúncios comerciais, ficção científica, outros comentadores simbólicos da tecnologia. Magia = tecnologia.

> What has happened to magic? It has not disappeared, but has become more diverse and difficult to identify. One form it takes, as Malinowski himself suggested, is advertising. The flattering images of commodities purveyed in advertising coincide exactly with the equally flattering images with which magic invests its objects. But just as magical thinking provides the spur to technological development, so also advertising, by inserting commodities in a mythologized universe, in which all kinds of possibilities are open, provides the inspiration for the invention of new consumer items. Advertising does not only serve to entice consumers to buy particular items; in effect, it guides the whole process of design and 1nanufacture from start to finish, since it provides the idealized image to which the finished product must conform. Besides advertising itself, there is a wide range of imagery which provides a symbolic commentary on the processes and activities which are carried on in the technological domain. The imagination of technological culture gives rise to genres such as science fiction and idealized popular science, towards which practising scientists and technologists have frequently ambivalent feelings, but to which, consciously or unconsciously, they perforce succumb in the process of orienting themselves towards their social milieu and giving meaning to their activities. The propagandists, image-makers and ideologues of technological culture are its magicians, and if they do not lay claim to supernatural powers, it is only because technology itself has become so powerful that they have no need to do so. And if we no longer recognize magic explicitly, it is because technology and magic, for us, are one and the same.
> (p. 9)
