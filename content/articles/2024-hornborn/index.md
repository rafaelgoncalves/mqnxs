---
title: "A ecologia política do tecnoceno"
date: 2024-05-23
path: "/ecologia-politica-tecnoceno"
category: fichamento
author: Rafael Gonçalves
featuredImage: "hornborg.jpg"
srcInfo: <a href="https://www.keg.lu.se/en/alf-hornborg">Alf Hornborg</a>
tags:
  - Alf Hornborg
  - Bruno Latour
  - tecnoceno
  - ecologia política
  - economia
  - antropoceno
  - sistema-mundo
  - ecologia
  - natureza
  - teoria ator-rede
  - fichamento

published: true
---

Fichamento do texto "A ecologia política do tecnoceno: revelando troca ecologicamente desigual no sistema-mundo" de Alf Hornborg[^1].

[^1]: HORNBORG, Alf, A. (2015). The Political Ecology of the Technocene: Uncovering Ecologically Unequal Exchange in the World-System. In C. Hamilton, C. Bonneuil, & F. Gemenne (Eds.), **The Anthropocene and the Global Environmental Crisis: Rethinking Modernity in a New Epoch** (pp. 57-69). Routledge. 

# Convergência entre c. naturais e sociais (pós-cartesianas) na ideia de Antropoceno

> The currently unfolding **discourse on the Anthropocene represents a convergence of Earth system natural science and what I will refer to as post-Cartesian social science.** Both fields suggest that the Enlightenment distinction between Nature and Society is obsolete. Now that humanity is recognised as a geological force, the story goes, we must rethink not only the relations between natural and social sciences but also history, modernity, and the very idea of the human. Indeed, the increasingly inextricable interfusion of nature and human society is incontrovertible, as evidenced not only by climate change but also by several other kinds of anthropogenic transformations of ecosystems.
> (p. 57)

# Ciências sociais pós-cartesianas (Latour, Haraway, Descola, Escobar, ...) sobre a distinção entre Natureza e Cultura

> For decades having believed these circumstances to be self-evident, however, I am surprised by the intensity and also the character of the philosophical import that is currently attributed to them. The theoretical implications of the interfusion of Nature and Society, and the imperative of transdisciplinary approaches to human–environmental relations, were prominent in social-science agendas already in the 1990s (for example, Narain and Agarwal 1991; Haraway 1991; Croll and Parkin 1992; Latour 1993; Descola and Pálsson 1996; Peet and Watts 1996; Escobar 1999). **Fields such as environmental anthropology, political ecology, development studies, and science-and-technology studies (STS) were attempting to deconstruct the Nature/Society distinction more than twenty years ago.** Rather than embroil ourselves in increasingly obscure deliberations on the possible philosophical implications of this shift, **it should now be incumbent on social scientists to try to be as clear as possible about the societal and not least political issues that it raises.**
> (p. 57)

# Questão do texto

> The questions I wish to address in this chapter are: **In what sense should the idea of the Anthropocene change our understanding of human–environmental relations, history, and modernity?** If post-Cartesian perspectives can help us grasp climate change, **how can they simultaneously illuminate the history of technology and development? Do they imply a complete dissolution of the categories of Nature and Society, or merely their reconceptualisation? Is the notion of the Anthropocene an adequate designation for the current period? What are the prospects for humanity surviving the planetary transformations that it has set in motion?**
> (p. 57-8)

# Críticas à vinculação efetiva e analítica de não-distinção entre Natureza e Cultura

> A prominent role of science seems to be to represent technological progress as ‘natural’, as if capitalist expansion was founded exclusively on innovative discoveries of the ‘nature’ of things, and as if the social organisation of exchange had nothing to do with it. Constrained by our Cartesian categories, we are prompted by the materiality of technology to classify it as belonging to Nature rather than to Society. The post-Cartesian solution to this predicament would be to abandon the categories of ‘nature’ and ‘society’ altogether. Philippe Descola (2013, 82), for instance, rhetorically asks, ‘where does nature stop and culture begin’ in an increasingly anthropogenic biosphere. **But to acknowledge that Nature and Society are inextricably intertwined all around us – in our bodies, our landscapes, our technologies – does not give us reason to abandon an analytical distinction between aspects or factors deriving from the organisation of human society, on the one hand, and those deriving from principles and regularities intrinsic to the pre-human universe, on the other.** For example, the future of fossil-fuel capitalism no doubt hinges on the relation between the market price of oil and the Second Law of Thermodynamics, but I cannot imagine that we have anything to gain from dissolving the analytical distinction between the logic of the world market and the laws of thermodynamics.
> (p. 58)

[Acredito que o ponto dos cientistas sociais "pós-cartesianos" é insistir que os fatos científicos são construídos em uma realidade que é "material-semiótica" (Haraway) e não são puros reflexos (nem imediatos, nem puramente culturais) de leis naturais. Nesse sentido, se não há vantagem em abolir a distinção, o tensionamento destas gera novas questões, por exemplo a Haraway ao pensar gênero como agente material-semiótico, ou Latour sobre pensar os híbridos]

> **Regardless of how we represent them, the laws of thermodynamics have been in operation as long as there has been a universe {em termos Latourianos, são fatos científicos construídos que resistem provas de força e por isso são eficazes e verdadeiros}**, billions of years before the origins of human societies. They are an undeniably ‘natural’ aspect of human existence that pervade everything we do, and yet have not been, and cannot be, the least altered by human activity. In contrast, modes of human social organisation such as markets are ephemeral constructs that can be fundamentally transformed by political decisions or the vicissitudes of history {embora, por outro lado, se sustente por meio de instituições e objetos objetivos, que faz com que a efemeridade e transformabilidade sejam relativas e conjecturais}. Yes, thermodynamics and markets are intertwined in fossil-fuel capitalism, but this is no reason to deny that the former belongs to Nature and the latter to Society.
> (p. 58)

[Não acredito que, para Latour, por exemplo, o objetivo seja tornar as noções de natureza e sociedade obsoletas, mas mostrar sua natureza variável, histórica e construída. Por isso a purificação é uma forma de mediação entre outras em _Jamais fomos modernos_. Deste ponto de vista, é parte do método chegar às noções de natureza e cultura em cada caso concreto, como consequência a posteriori de relações híbridas.]

> If the categories of Nature and Society are obsolete, as it is currently fashionable to propose, this only applies to images of Nature and Society as bounded, distinct realms of reality. At the risk of being unfair to Descartes, I shall follow the convention of referring to such distinctions as examples of ‘Cartesian dualism’. It seems trivial to observe that such bounded, distinct realms do not exist (who would today object?), **but it remains justified to identify the logic of natural and societal phenomena separately, prior to demonstrating how they interact in practice.**
> (p. 59)

# Abordagem macrossociológica para o estudo sociotécnico (x microssociológico da TAR)

> In consequence with the abandonment of Cartesian dualism in our approach to anthropogenic transformations of the biosphere, we have no less reason to **reconsider human economies and technologies as similarly hybrid phenomena interlacing biophysical resources, cultural perceptions, and global power structures.** Such insights deserve to be pursued not only at the micro-level of our interaction as individuals with specific artefacts, as advocated by Actor Network Theory (ANT), but **more importantly at the macro-level, where the global assemblages of artefacts that I have called ‘technomass’ (Hornborg 2001) indeed are the very stuff of a highly inequitable world-system.** It is in this global sense that the social dimensions of technology are the most interesting. By viewing it as a system-wide totality, we can detect how global power relations are delegated to, and buttressed by, technology.
> (p. 59)

# Desigualdade inerente ao processo de desenvolvimento tecnológico na revolução industrial

> It would thus be highly misleading to conceive of the anthropos starring in the Anthropocene narrative as the human species (Malm and Hornborg 2014). **‘Humanity’ as a collective has never been an agent of history, and the technological fruits of the Industrial Revolution continue to be very unevenly accessible to different segments of world society.** This uneven distribution of modern, fossil-fuel technology is in fact a condition for its very existence. The promises it held out to humanity were illusory all along: the affluence of high-tech modernity cannot be universalised, because it is predicated on a global division of labour that is geared precisely to huge price and wage differences between populations. What we have understood as technological innovation is an index of unequal exchange.
> (p. 60)

> The conditions of technological innovation were radically transformed in the late eighteenth century. We usually think that the decisive factors were engineering science and the adoption of fossil fuels, but none of this would have been possible without the global social processes that made the relative prices of labour and resources on the world market prerequisite to ‘technological progress’ in Europe.
> (p. 60)

> Ingenuity is a necessary but not _sufficient_ condition for modern technological ‘progress’. Global price relations are systematically excluded from our definition of technology, even though, by organising asymmetric resource flows, they are crucial for its very existence. Much as inexpensive labour and land in colonial cotton plantations were fundamental to the Industrial Revolution (cf. Hornborg 2011, Chapter 5), it remains essential for high-tech society that prices of oil and other resources are manageable.
> (p. 60)

# Dualismo cartesiano como raiz do fetichismo tecnológico

> **I am convinced that Cartesian dualism is at the root of the difficulties we are having in perceiving our technological fetishism.** A tenacious illusion of Enlightenment thought is that a boundary can be drawn between material forms and the relations that generate them, and that it is only the latter that can be contested, negotiated, and transformed. **I think this kind of distinction – the reification of _things_ – is more problematic than the distinction between ‘natural’ and ‘societal’ aspects. It is the very essence of capitalist fetishism.** The ‘moderns’ generally perceive tangible objects as given, and as separate from the invisible networks of relations in which they are embedded. Such distinctions alienate humans from non-human nature as well as from the products of their labour, because both are perceived as categories of autonomous objects rather than as manifestations of relations.
> (p. 60-1)

> **The density of distribution of technologies that are ultimately dependent on fossil fuels by and large coincides with that of purchasing power. These technologies are an index of capital accumulation, privileged resource consumption, and the displacement of both work and environmental loads.** After more than two hundred years, we still tend to imagine ‘technological progress’ as nothing but the magic wand of ingenuity that, with no necessary political or moral implications elsewhere, will solve our local problems of sustainability. Universities throughout the world reproduce this illusion by entrenching the academic division of labour between faculties of engineering and faculties of economics. **But globalised technological systems essentially represent an unequal exchange of embodied labour and land in the world-system (Hornborg 2011). The world-view of modern economics, the emergence of which accompanied the Industrial Revolution in the hub of the British Empire, systematically obscures the asymmetric exchange of biophysical resources on which industrialisation rests. This disjunction between exchange values and physics is as much a condition for modern technology as engineering.**
> (p. 61)

# O termo Antropoceno não captura o aspecto desigual da acumulação econômica e desenvolvimento tecnológico

> The uneven accumulation of technomass visible on satellite photos of night-time lights proceeds by means of a simple algorithm: the more fossil fuels and other resources it has dissipated today, the more it will afford to dissipate tomorrow.  **This account of our entry into the Anthropocene does not refer to the biological properties of the species _Homo sapiens_, but to a specific form of social organisation that emerged very recently in human history, as a strategy of one segment of humanity to dominate the remainder.** This form of social organisation continues to be propelled by the interests not of our species, but of a social category (Malm and Hornborg 2014).
> (p. 61)

> **The dominant Anthropocene narrative, of course, does recognise that climate change derives from human activities, but these activities are then viewed as expressions of innate traits of our species {?}.** Rather than examine their societal and political drivers as factors that can be transformed, the narrative tends to represent them as natural and inevitable features of our biology. **But phenomena such as worldviews, property relations, and power structures are _social_ phenomena. They are beyond the horizons of natural science, because they require analytical tools that natural scientists are not provided with. [[Como separar o social e o natural/tecnológico quando o semiótico/cultural se expressa no material e vice-e-versa?]]**
> (p. 61-2)

# Crítica ao especiecentrismo do termo Antropoceno [embora invoque um antropocentrismo para justificar a excepcionalidade do humano]

> This is not to deny that human organisms are uniquely equipped to develop capitalism. Our semiotic capacity for abstract representation and language, which had enormous survival value for hundreds of thousands of years of hunting and gathering, finally generated general-purpose money and the globalised economy, which in turn made the Industrial Revolution feasible. **The world-systemic events of the eighteenth century were products of a global history of increasing interconnectedness (and inequality) ultimately founded on the human capacity for abstract representation.** The big question is whether this capacity will be of any use in redesigning our global economy for survival. **To challenge the species-centrism of the Anthropocene narrative is to make two important points that are often disregarded by natural scientists: (1) the incentives, benefits, and negative repercussions of industrialisation are very unevenly distributed among social categories within the human species; and (2) there is nothing biologically inevitable about the institutions and forms of social organisation that we know as capitalism.**
> (p. 62)

# Tecnoceno como era geológica fundada na tecnologia moderna pós sex XVIII (o que incluiria aspectos biofísicos e socio-culturais)

> Dipesh Chakrabarty (2009) correctly observes that we now need to integrate the history of our species with the history of capital, but he does not provide us with any feasible suggestions on how to proceed. **He is completely silent on how our biological capacity for abstract representation (as in language and other semiotic systems) is prerequisite to the very idea of money, and how money was in turn prerequisite to the Industrial Revolution that inaugurated the Anthropocene.**  But it is precisely through this chain of events that studies of natural and human history, while each reserving its specific arsenal of concepts and methods, can be integrated. **Modern technology is the pivot of both, because it implicates both biophysical and socio-cultural dimensions of our increasingly globalised history. Rather than imply that climate change is the inexorable consequence of the emergence of _Homo sapiens_, as suggested by the notion of the ‘Anthropocene’, I would thus prefer that the geological epoch inaugurated in the late eighteenth century be named the _Technocene_.**
> (p. 62)

# Distinção entre pré-modernidade e modernidade. Diferença entre objetos técnicos com e sem mediação da percepção humana [como distinguir de fato? há mediação humana em todo objeto técnico construído]. Tecnologia como magia contemporânea.

> A focus on how artefacts are employed in the construction of human societies [[ as in Latour & Strum ]] addresses a phenomenon that is specific to the human _species_, but it can also illuminate the specificity of the social condition that we know as _modernity_. **The distinction between pre-modern and modern political economies hinges on the different roles of human perceptions in the two contexts. In pre-industrial societies, where political economy is about the social organisation of human muscle power, people have to be persuaded to exert themselves for the benefit of those in power.  The operation of modern technology, however, locally appears to be independent of human perceptions.** As we consider the role of artefacts in different human societies, a central question is: If material objects are mobilised as agents in systems of socio-ecological relations, what is the difference between their capacity to operate _without_ the mediation of human perceptions, on the one hand, and their capacity to operate by _means_ of such mediation, on the other? **In other words, how do we distinguish between technology and magic?**
> (p. 63)

> **I will argue that technology is our own version of magic.** I define ‘magic’ as a category of social persuasion mediated by human perceptions but represented as independent of human consciousness. In this sense, I agree with Latour (1993) that ‘modernity’ is not a decisive break with ‘pre-modern’ ontologies. **The Enlightenment demystification of pre-modern magic and ‘superstition’ was not a final purge of valid, objective knowledge, but a _provisional_ and _politically_ positioned one.** Its understanding of the nature of economic growth and ‘technological progress’ has been a successful instrument of expansion for core regions of the world-system for over three centuries, but the multiple crises currently faced by global society are an indication of the approaching bankruptcy of this worldview. **On the other hand, I continue to believe that even the illusions of capitalist modernity can be exposed through rational analysis, and I suppose that, in this sense, I continue to have faith in the Enlightenment. We can use the same Reason that gave us modern technology to show that this very technology is a particular kind of magic.**
> (p. 63)

# Tipologia tecnológica: tecnologia local (e.g.: chave), magia local (e.g.: moeda), tecnologia/magia global (e.g.: máquina elétrica)

> A conclusion from what I have argued so far is that there are three fundamental categories of artefacts, defined by the specific ways in which they are delegated agency. The first is **local, non-globalised technology,** which operates without the mediation of either human perceptions or exchange rates. It is exemplified by keys. The second is **‘local magic’,** which operates by means of human perceptions, exemplified by coins. The third is **globalised technology,** which locally operates without the mediation of human perceptions, but globally relies on exchange rates that are dependent on human strategies and intentions. It could also be called ‘global magic’, and can be exemplified by machines propelled by fossil fuels or electricity. **Globalised technology is ‘magic’ in the sense that it is a specific way of exerting power over other people while concealing the extent to which this power is mediated by human perceptions.**
> (p. 64)

# Economia política, vertente pós-estruturalista (Escobar e Latour) como "relativista" e menos confrontatória do sistema capitalista

> The discourse on **political ecology emerged in the early 1970s as an ambition within several human sciences to relate local ecological dilemmas, primarily in what was then known as the ‘Third World’, to global political economy.** Over the decades, two main lineages of research can be discerned: **one acknowledging an objective Nature and a set of actors contesting each other’s claims to resources, the other inspired by poststructuralist theory to deconstruct images of Nature as well as the identities and claims of the actors (Escobar 1999)**. Disarmed by its own relativism, the latter, constructivist approach has predictably yielded fewer substantive challenges to capitalist extractivism. Pursuing constructivism ever deeper into philosophical opacity, the ‘political ecology’ of Bruno Latour (2004) has been criticised for even more radically disarming political criticism (for example, Wilding 2010; Söderberg and Netzén 2010; Hornborg 2014). **The emphasis of the constructivist wing of science-and-technology studies on microsociological case studies of individual actors has gone hand-in-hand with a rebuttal of more inclusive socio-economic power structures like ‘capitalism’ or even ‘society’ (Söderberg and Netzén 2010, 100–2).**
> (p. 64)

# Noção antropocentrica de sociedade

> **Following my previous argument, however, we must maintain that only _societies_ – organised assemblages of interacting human beings – negotiate meanings, generate relations of unequal exchange, and enable people to exert power over each other.** Of course, all these social relations are stabilised through the recruitment of non-human components into their networks, and, of course, they are to a large measure shaped by the specific features of these non-human components, but **the driving forces and the glue that reproduce them are irreducibly _social_ in the sense that they hinge on the incentives, intentions and agency of interacting human subjects.**
> (p. 64-5)

# Reformulação da micro/ecologia política em termos macro/econômicos

> A post-Cartesian understanding of the Industrial Revolution should fundamentally reframe the discourse on political ecology. Rather than dream of advanced technological solutions to problems of ecological sustainability, **we would recognise most modern technologies as social strategies for _displacing_ problems (labour as well as environmental loads) to areas where labour and envi- ronmental degradation are less expensive.** Instead of technological utopianism, this radical reconceptualisation of technology should prompt us to critically consider the role of general-purpose money in orchestrating asymmetric transfers of labour power and natural resources in the world-system.
> (p. 65)

# Foco no arranjo economico/sociocultural específico que promoveu a industrializaçào

> Our capacity for abstract representation was pre-requisite to capitalism, but **only by means of the specific sociocultural institution of general-purpose money. It was through the globalised circulation of general- purpose money that all the ingredients of the Industrial Revolution – American fields, African slaves, cotton fibre, British workers, coal, cotton textiles, and so on – were transformed into commensurable and interchangeable commodities.** The generalised commoditisation of all this human time and natural space, which made industrialisation possible, is not an inexorable consequence of the human capacity for representation. **If the economic strategies generating globalisation and industrialisation are root causes of the perilous prospects of climate change, it should be theoretically possible to avert this threat by modifying the conditions of economic rationality.** It should be feasible, in principle, to organise a monetary system that restricts the interchangeability of products to specific spheres of exchange through the use of special-purpose currencies. This is not to suggest that certain kinds of exchanges should be prohibited, but that **the options available to individual actors should encourage transactions that substantially reduce the consumption of fossil fuels and other practices contributing to environmental degradation.**
> (p. 65-6)

# Sugestão: criação de uma moeda paralela local [~moeda do banco Palmas e outros bancos comunitários]

> To make this suggestion more tangible, let us imagine that a nation-state seriously wishes to reduce the long-distance transport required to provision its citizens. **It could achieve this goal by establishing a special currency, a certain amount of which is provided to its citizens on a monthly basis as a tax-free basic income, but which can only be used to buy goods and services originating within a certain geographical distance from the point of purchase.** It would serve as a ‘complementary currency’ in the sense that it did not replace conventional money, only provide an alternative to it. **The new currency would tend to circulate within localised circuits of exchange, encouraging the growth of an informal sector alongside the conventional economy.** The amount provided to each citizen or household would correspond to basic requirements for survival. The extent to which people wished to continue earning conventional, general-purpose money to enable additional consumption would be a matter of personal choice. A likely scenario would be that most people decided to divide their time between working in the formal and informal sectors. What is beyond doubt is that people would tend to use the new currency at least to procure basic necessities such as food, repairs, carpentry and so on, as this would leave more of their conventional income for other kinds of expenditures. Local farmers and other entrepreneurs would be encouraged to accept (tax-free) payment in the new currency for two reasons: they would be able to use some of it to pay for local labour, services, and goods; and, they would be offered the option of converting some of it into conventional money, through the authorities, at beneficial rates. **Once in operation, this system would undoubtedly radically reduce the demand for long-distance transport, which is one of the main drivers of climate change** (Hornborg 2013b). In the long run, it would not only be more sustainable – reducing energy use, greenhouse gas emissions and waste, while enhancing local cooperation, biodiversity and resilience – but also reduce public expenses for transport infrastructure, environmental protection, health services and social security.
> (p. 66)

# Ameaças ao business-as-usual: colapso financeiro ou ecológico

> For that vocabulary/cosmology to collapse, business as usual must itself be under threat. At least two such plausible threats can be envisaged, perhaps in conjunction: **a definitive collapse of the global financial system, a breakdown ultimately geared to the rising costs of shrinking supplies of energy and other resources; and, a global ecological crisis such as that evoked by the notion of planetary boundaries.** When either or both of these scenarios can no longer be disregarded, reforms that currently seem highly improbable, such as the one advocated here, may be considered in a new light.
> (p. 67)
