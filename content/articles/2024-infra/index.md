---
title: Tecnologia, infraestruturas e redes feministas
tags:
    - Débora Prado de Oliveira
    - Daniela Camila de Araújo
    - Marta Mourão Kanasiro
    - colonialismo
    - androcentrismo
    - infraestrutura
    - feminismo
    - baobáxia
    - autonomia
    - linguagem
    - segurança
    - fichamento

path: "/tecnologia-infraestruturas-e-redes-feministas"
date: 2024-08-09
category: fichamento
featuredImage: "fuxico.png"
srcInfo: '<a href="https://www.marialab.org/fuxico/">Arte da Fuxico: rede autônoma feminista (MariaLab)</a>'
published: true

---

Fichamento do artigo "Tecnologia, infraestruturas e redes feministas: potências no processo de ruptura com o legado colonial e androcêntrico"[^1].

[^1]: OLIVEIRA, Débora Prado de; ARAÚJO, Daniela Camila de; KANASHIRO, Marta Mourão. Tecnologias, infraestruturas e redes feministas: potências no processo de ruptura com o legado colonial e androcêntrico. **Cadernos Pagu**, v. 59, p. e205903, 2021.  

# Resumo

> **Autonomia, linguagem e segurança** são as três categorias fundamentais que neste artigo desvelam a noção de **tecnologia feminista** que vem sendo construída em movimentos feministas da América Latina. **Os diferentes sentidos mobilizados nessas categorias são observados a partir da atuação de coletivas latino-americanas de mulheres, pessoas trans e não binárias voltadas para produção e uso das tecnologias de informação e comunicação, redes autônomas e infraestruturas.** Nesse cenário, tecnologia feminista é uma perspectiva construída por esses movimentos engajados em um debate tecnopolítico na interface com conhecimentos sobre internet, autonomia, infraestruturas digitais e segurança da informação para ativistas. A partir de pesquisas recentes desenvolvidas pelas autoras, este artigo apresenta e analisa a constituição dessa perspectiva e sua contribuição para a construção de alianças com as tecnologias e redes sociotécnicas que sejam divergentes do legado colonial e androcêntrico.

# Representatividade e protagonismo -> desestabilização e ressignificação

> Nesse sentido, compreendemos que há neste campo um reconhecimento da importância da representatividade, mas há também uma forte proposta de ir além na medida em que o chamado por envolver mais mulheres, pessoas trans e não binárias no desenho de tecnologias emerge como algo que será determinante para promover alianças que sejam capazes de desestabilizar escolhas naturalizadas e não verbalizadas e desafiar a reprodução de padrões e normas em experiência com infraestruturas e redes digitais. Ou seja, **as experiências com tecnologias feministas, mais do que convidar corpos e grupos sociais que sofrem forças de afastamento da interação social com tecnologias a reconhecer e assumir seu protagonismo nesse campo, buscam mobilizar experiências, interesses e necessidades não normativos e incorporá-los em redes sociotécnicas.** Nesse processo, conceitos como redes comunitárias, infraestruturas tecnológicas e autonomia, por exemplo, são revisitados em processos que buscam manter as definições abertas à possibilidade de serem reconstruídas, reapropriadas e ressignificadas a partir das múltiplas condições vividas, como veremos mais adiante. Seguindo este movimento que emerge das experiências acompanhadas, mais do que buscar alcançar conceitos definitivos do que seriam tecnologias, infraestruturas e redes feministas, nossa proposta neste artigo é olhar para as desestabilizações e diferenciações que emergem quando as perspectivas feministas {5} são mobilizadas a partir do universo alcançado em nossas pesquisas, caracterizado por tensões e deslocamentos.
> (p. 4-5)

# Linguagem (feminino como gênero neutro)

> 1: Doravente, o termo „coletivo‟ será utilizado neste artigo como substantivo feminino, tal como ocorre entre alguns grupos de mulheres que informaram nossas pesquisas. „Coletivo‟ é usado comumente como substantivo masculino para designar novas formas de mobilização e organização de grupos em prol de diversos direitos. **A opção das autoras está em consonância com Isabelle Stengers e Philipe Pignare (2011) e busca caminhar junto a essas mulheres e suas palavras como antídotos.** O mesmo ocorre com a substituição da palavra servidores por servidoras, e outros termos geralmente utilizados no masculino. Débora Oliveira (2019) observa que utilizar substantivos femininos é um mecanismo adotado pelos grupos pesquisados para apontar a estrutura sexista da nossa linguagem e como ela pode transmitir a ideia de que alguns assuntos são masculinos, como aqueles relacionados às tecnologias.
> (p. 4-5)

# Situacionalidade

> 2: Considerando o compromisso engajado que assumimos a partir de epistemologias feministas, a partir dos processos de pesquisa, salientamos que Daniela Araújo estudou o movimento feminista na cultura hacker e como resultado desta aproximação passou a integrar o núcleo de organização da MARIALAB em 2017 e Débora Oliveira passou a colaborar em iniciativas da MARIALAB / VEDETAS em São Paulo/SP. Marta Mourão Kanashiro é pesquisadora e ativista na área de tecnologia e vigilância desde 2001, é membra fundadora da Rede Latino-Americana de Estudos sobre Vigilância, Tecnologia e Sociedade (Rede Lavits) e acompanha as atividades de grupos feministas voltados aos temas da rede, como é o caso da coletiva MARIALAB dentre outras.
> (p. 5)

# Material e métodos

> pesquisa de campo de ambas se estruturou a partir de participações em encontros e eventos, alguns exclusivos para mulheres, pessoas transgênero e não binárias que contemplavam experimentações com servidoras3, redes4 autônomas e comunitárias5, e aprendizados sobre segurança da informação e vigilância. Também compõem as fontes desta pesquisa uma série de documentos e publicações online que informam as articulações {7} tecnopolíticas e os saberes produzidos por esses grupos. A partir dessas fontes, neste artigo abordaremos discussões relativas a três aspectos distintivos no campo das tecnologias feministas: a proposta de autonomia coletiva, a ativação do imaginário por meio de outras linguagens e narrativas e o tensionamento de uma noção universal de segurança.
> (p. 6-7)

# Exemplos de iniciativas feministas mapeadas pelo relatório Latin America in a Glimpse – Gênero, feminismo e internet na América Latina (2017), organizado pela Derechos Digitales e a Association for Progressive Communication (APC)

> Com relação às infraestruturas feministas, o relatório descreve mais detidamente iniciativas no Brasil, Argentina e México. VEDETAS e CL4NDESTINA, por exemplo, são dois projetos brasileiros mencionados. VEDETAS tem como proposta promover espaços físicos e digitais exclusivos para mulheres visando o aprendizado e intercâmbio de conhecimentos sobre tecnologias, associados ao desenvolvimento de infraestruturas de {9} redes e servidoras. CL4NDESTINA é uma servidora ativista feminista que oferece hospedagem para sites de coletivas, organizações e movimentos sociais feministas baseados na América Latina. KÉFIR10, por sua vez, é um projeto situado na Argentina e no México e se define como uma “cooperativa transfeminista de tecnologias livres”, cuja aposta está no desenvolvimento de comunidades digitais enquanto um “ecossistema” para o desenvolvimento e aprendizagem de ferramentas para comunicação online, tecnologias livres, desenvolvimento web e segurança digital.
> (p. 8-9)

# Tecnologia feminista para a Rede Autônoma Feminista

> As experiências aqui mobilizadas parecem propor um passo adiante, uma vez que, ao tensionar as normas naturalizadas nesse campo, também trazem a proposta de criação de tecnologias por e para esses grupos sociais, buscando fortalecer mutuamente tecnologias e ações políticas daqueles que escapam às normas hegemônicas. Nas formulações conjuntas de VEDETAS, KÉFIR e PERIFÉRICAS no site Rede Autônoma Feminista15, essa proposta transformadora é expressa da seguinte forma:
> 
> > **Uma das definições de tecnologia feminista é a aplicação de conhecimentos da ciência para apoiar a causa feminista, sendo que esses conhecimentos são desenvolvidos e mantidos por mulheres que desafiam o status do sistema patriarcal normativo, propondo novas formas de politizar o debate sobre tecnologia e seus usos. É uma tecnologia política, constituída através da autonomia da mulher.**
> 
> (p. 10)

# Materialidade infraestrutural e corpórea

> É importante notar ainda que neste campo a invisibilidade das infraestruturas tecnológicas é aproximada à invisibilidade do trabalho, das relações e ainda das desigualdades associadas aos corpos e os problemas específicos que certos grupos sociais enfrentam. **É um chamado pelo reconhecimento de materialidades – das infraestruturas e dos corpos – acompanhado de uma elaboração política a partir do feminismo interseccional.**
> (p. 11)

# Interseccionalidade

> 17: A professora de direito norte-americana Kimberlé Williams Crenshaw (2002), ao se debruçar sobre uma teoria crítica de raça, conceitua a interseccionalidade ao apontar como a materialização de sistemas de diferença prejudicava o acesso de mulheres negras a direitos civis e humanos, impondo limites e riscos estruturais. A autora advoga que a experiência de mulheres negras não pode ser capturada nem só pela perspectiva de raça, nem só pela de gêneros sem que se incorra em um apagamento. Ou seja, desde uma perspectiva interseccional é preciso a combinação para abordar as múltiplas exclusões ou privilégios estruturais que atravessam sujeitos e grupos que não podem ser reduzidos a apenas uma lente. O reconhecimento das diferenças é associado ainda a um movimento em busca de transformações, conforme aponta Patricia Hill Collins (2017:12), que ressalta que Crenshaw “está claramente defendendo a interseccionalidade como uma construção de justiça social, e não como uma teoria da verdade desvinculada das preocupações de justiça social. Ao fazer uma revisão da ampla literatura produzida nesse campo, Piscitelli (2009) aponta que no feminismo interseccional a prática política das mulheres é localizada e pensada a partir de intersecções: “nas suas reformulações, o conceito de gênero requer pensar não apenas nas distinções entre homens e mulheres, entre masculino e feminino, mas em como as construções de masculinidade e feminilidade são criadas na articulação com outras diferenças de raça, classe social, nacionalidade, idade; e como essas noções se embaralham e misturam no corpo de todas as pessoas, inclusive aquelas que, como intersexos, travestis e transexuais, não se deixam classificar de maneira linear como apenas homens ou mulheres”.
> (p. 11)

# Ampliação da noção de infraestruturas sociotécnicas para incorporar categorias feministas, como as noções de consentimento, escuta, cuidado e autonomia

> Este movimento de expansão está presente no manifesto digital #Do aco a pele18, redigido a quatro maos numa colaboracao entre ativistas do rasil e Mexico, assinado por “Nanda de VEDETAS, servidora transhackfeminista do Brasil e Nadège da cooperativa feminista de tecnologias livres KÉFIR”, em que as autoras **apontam que a “materialidade eletrônica pode ser um portal para o aprendizado e para a transgressão”.** Está presente também na definição de Sophie Toupin e Alexandra Hache de um modo mais amplo:
>
> Um dos principais elementos constitutivos das infraestruturas feministas autônomas está no conceito de auto-organização já praticado por muitos movimentos sociais que entendem a questão da autonomia como um desejo por liberdade, auto-valorização e ajuda mútua. Além disso, entendemos **o termo infraestrutura tecnológica de forma expansiva, englobando hardware, software e aplicativos, mas também design participativo, espaços {12} seguros e solidariedades sociais** (Toupin S., Hache, A, 2015:23).
>
> Essa perspectiva de infraestrutura, apontam as autoras, passa pela criação de espaços físicos seguros que permitam às mulheres, pessoas trans e não binárias se reunirem e o desejo de instaurar processos de aprendizado conjunto. Exemplo disso é a acolhida de mães e crianças em atividades realizadas pelas coletivas, considerando que esta é uma maneira de incluir essas mulheres em suas especificidades e vivências. Também é parte dessa abordagem, a criação de servidoras feministas19, como mostram as experiências realizadas pelas VEDETAS, CL4NDESTINAS e KÉFIR.
> (p. 12-3)

# Adjetivo feminista ~ não-neutralidade

> Ao acrescentar a palavra feministas às infraestruturas e propor a perspectiva interseccional ou de solidariedades sociais, esses grupos colocam a não neutralidade das tecnologias e dos aparatos que servem ao funcionamento da internet em primeiro plano. Ao mesmo tempo, frisam que a maior parte das escolhas e relações tecnopolíticas por trás desses aparatos não atendem às necessidades de grupos que são atingidos pelas desigualdades estruturais, como as de gênero, raça, etnia e classe. Ou seja, **acrescentar essa palavra se torna uma forma não só de apontar a política por trás das tecnologias e infraestruturas, mas ao mesmo tempo, indicar a quem sua suposta neutralidade vem servindo, {14} afirmando que não são as mulheres, muito menos os grupos que são atravessados por múltiplas desigualdades no sentido apontado pela perspectiva interseccional.** Classificar essas tecnologias como feministas enuncia ainda que a perspectiva política adotada neste campo será engajada com uma agenda de transformações e direitos sociais. Outro ponto que aparece conectado, nesse sentido, é uma proposta de **alianças na diversidade, considerando que essas alianças não sejam só de resistência, mas também de criação – no sentido de ativar novos paradigmas e experimentações que transcendam estruturas de pensamento, práticas e relações que reproduzam a hierarquização característica das perspectivas colonialista, capitalista, androcentrica e ocidental** (Haraway, 1995, 2004; Ribeiro, 2017).
> (p. 13-4)

# AUTONOMIA tecnológica

> **O projeto autonomista foi considerado para muitas autoras e ativistas como aspecto fundamental do que constitui a {15} identidade do movimento feminista latino-americano**, principalmente se considerarmos que a ascensão destes movimentos se dá em meio à ditadura militar que se instalou em inúmeros países da região na década de 70 (Alvarez, 2014). O ativismo feminista contemporâneo, especialmente entre **a parcela do movimento que habitualmente tem sido denominada de feminismos jovens (Facchini e França, 2011; Alvarez, 2014), é marcado por noções como descentralizar, autogerir e colaborar em rede, e por um retorno aos ideais do movimento feminista autonomista, porém sob novos significados relacionados aos corpos, às instituições e aos espaços de poder.** Alvarez (2014) correlaciona essa retomada com a pluralização dos feminismos, expandindo os campos heterogêneos e policêntricos que se cruzam com outros movimentos sociais e outras interseccionalidades.
> (p. 14-5)

> 20: A expressão “projeto autonomista” vincula-se neste texto às reivindicações de autonomia tecnológica presentes entre as ativistas que compuseram essa pesquisa. Neste universo, **é amplo o debate sobre as tensões entre autonomia e soberania tecnológica.** Ainda que explorar essas questões fuja ao escopo deste artigo, vale mencionar que não se deve compreender aqui a autonomia como alternativa à soberania tecnológica. Neste sentido, as autoras deste texto entendem que **o Estado deve possibilitar e incentivar a tecnologia nacional, especialmente, quando se trata de países em desenvolvimento.** Parte do embate entre autonomia e soberania se deve à recusa da regulação e atuação do Estado, como uma das inúmeras facetas da cultura digital. Dentre as muitas vertentes {15} neste cenário, há aquela que vincula a perspectiva de colaboração em rede e autonomia tecnológica (dentre muitos outros aspectos) à ideais libertários conectados ao empreendedorismo, à tecnofilia, e às propostas neoliberais que dominam o Vale do Silício (Barbrook, Cameron, 1995; Coleman, Golub, 2008; Evangelista, 2011; Pessoa, 2017). Este não é o sentido que sobressai nas experiências ativistas aqui apresentadas, que também pode ser conferido na percepção de Bravo (2017) exposta adiante neste texto.
> (p. 11-2)

# Corresponsabilização e experimentação coletiva

> Essa perspectiva de autonomia coletiva é baseada numa ideia de corresponsabilidade e ajuda a extrapolar a divisão _humano/máquina_, reforçando que também é parte das infraestruturas o trabalho, a energia e criação humanas. Em outra coluna de diálogo entre a APC e a cooperativa KÉFIR o trabalho de pessoas que permeia o desenho e o funcionamento das infraestruturas é colocado em primeiro plano por Nadège:
>
> Você não apenas configura uma máquina e é isso. É investigação, manutenção, criação, desenvolvimento de conteúdo, advocacy, conscientização, estar em diferentes tipos de espaços, cruzando lutas e ativismos, dando suporte... É um processo contínuo.
>
> (p. 16)

> A proposta, assim, não é buscar na tecnologia uma solução para desigualdades estruturais e históricas por _design_, mas **promover o encontro e a troca entre diferentes conhecimentos para ativar a experimentação, passível de erros e de limites, e a busca por novos possíveis**. A ideia de autonomia, nesse sentido, é mobilizada não como uma expectativa de ruptura total em relação a atores e processos hegemônicos, nem pensada de forma individual, mas proposta como **uma construção que acontece de forma coletiva.**
> (p. 17)

# Alternativas infernais (no capitalismo) por Stengers e Pignarre

> 23: “Stengers e Pignarre chamam de „alternativas infernais‟ o “conjunto de situações que parecem não deixar outra escolha senão a resignação”, por um lado, ou conduz, por outro lado, a realização de uma “denúncia sonora”, que é impotente na medida que conclui de forma genérica que “todo o „sistema‟ que tem que ser destruído”, paralisando também a ação (Pignarre; Stengers, 2011:24). A noção de alternativas infernais, assim, carrega a hipótese de que o modo de funcionamento do capitalismo pressupõe um sufocamento da ação política e que sua perpetuação é sustentada pela limitação das alternativas possíveis e pela imposição de falsas escolhas que levam a uma narrativa de sacrifícios necessários e de resignação” (Prado, Araujo, Kanashiro, 2020).
> (p. 17)

# Soberania -> autonomia, autodeterminação tecnológicas (territorialização e contextualização local)

> A ativista feminista Loreto Bravo (2017), ao escrever sobre redes de telefonia celular autônomas e comunitárias dos povos originários de Oaxaca, propõe uma **ponte ético-política entre a comunidade hacker e as comunidades locais para avançar de uma concepção de soberania tecnológica para conceitos de autonomia e autodeterminação que também aciona coletividades.** A principal ruptura aqui seria **não pensar nem nos moldes da soberania associada ao desenvolvimento dos Estados-nações, considerando seu papel no processo de colonização, nem pensar em termos das liberdades individuais, numa tradição mais liberal, mas deixar o fluxo aberto para a significação local e para abordagens mais coletivas.** Nesse sentido, além da coletividade e interdependência, {18} a territorialização e a contextualização local também são constitutivas da noção de autonomia.
> (p. 17-8)

# Exemplos, rede de telecomunicações em Oxaca e Baobáxia da Rede Mocambos

> Como mencionamos anteriormente, as experiências feministas em torno das redes e infraestruturas autônomas não são as únicas que podem ativar novos possíveis. A experiência dos povos de Oaxaca resgatada por Loreto Bravo ou, no Brasil, da rede Mocambos25, uma colaboração entre quilombos, que, entre suas ações, mantém a Baobáxia26, um “repositório multimídia projetado para operar em comunidades rurais com nenhuma ou pouca internet”, mostram a potência de outros encontros com redes e infraestruturas autônomas que não partem dos feminismos.
> (p. 18)

> 25: Projetada e desenvolvida pela Rede Mocambos, uma colaboração entre quilombos brasileiros, a Baobáxia é uma rede que funciona com ou sem a internet e que carrega um repositório multimídia. Sobre a Rede Mocambos ver Tozzi, Vicenzo (2010): Redes federadas eventualmente conectadas. Arquitetura e prototipo para a rede Mocambos [https://livrozilla.com/doc/326548/redes-federadas-eventualmente-conectadas - acesso em 20/11/2018].
> (p. 18)

> 26: Sobre a Baobáxia, consultar: http://www.mocambos.net/tambor/pt/baobaxia, http://media.mocambos.net/baobaxia/doc/Apresentacao/#/7 e https://wiki.mocambos.net/index.php/NPDD/Baobáxia
> (p. 18)

# LINGUAGEM tensionada em relação a norma gramatical e aos vieses de gênero

> Nesse movimento de abrir o imaginário e as práticas sociotécnicas, outra infraestrutura hackeada nas experiências feministas é a linguagem. Um dos primeiros aspectos observados, logo nos primeiros encontros que tivemos com estes grupos em nossas pesquisas, em particular nos minicursos promovidos pelas {19} VEDETAS, foi o **tensionamento da norma gramatical e os vieses de gênero que ela implica. Termos como „servidoras‟ e „roteadoras‟ são usados na conjugação feminina, uma forma de questionar a noção de que o campo de infraestruturas técnicas seria um setor de domínio masculino – ou seja, é uma forma também de não limitar o imaginário coletivo e nossas próprias noções pelos conceitos que a linguagem molda, ou mais além, um modo a “desenfeitiçar” as máquinas.**
> (p. 18-9)

> O esforço de tradução e associação com termos em português ou espanhol é outro trabalho em progresso, assim como a elaboração de manuais e cartilhas autorais, inteiramente na língua materna. Tradicionalmente, os materiais técnicos disponíveis estão em língua inglesa. **Reconhecer o impedimento que o idioma muitas vezes acarreta denota uma compreensão de que a exclusão acontece por múltiplas desigualdades.**
> (p. 19)

# Nomes e memória feminista

> Nesse sentido, um princípio importante que experiências como a parceria entre VEDETAS, PERIFÉRICAS e KÉFIR apontam é **a importância de ativar narrativas locais, o que passa pela linguagem, pelos nomes e por uma reivindicação da memória, sobretudo de resgatar saberes e práticas de mulheres e de grupos que, muitas vezes, sofrem uma força de silenciamento em dinâmicas que tentam impor a inexorabilidade de futuros.**
> (p. 19)

> Além da flexão no feminino, nas configurações de rede, por exemplo, os nomes dos dispositivos também eram pensados para representar mulheres nos módulos do laboratório VEDETAS. O próprio nome VEDETAS resgata uma história, que é sempre apresentada nos momentos coletivos como os minicursos:
> 
> Vedeta é o nome de estruturas tipo casinhas que ficavam nas praias, de onde era feita a vigilância da costa. Durante a Guerra de Independência da Bahia, no início do século XIX, uma negra ex-escrava chamada Maria Felipa tomou a Ilha de Itaparica de assalto. Durante algumas semanas, sua tropa feminina esteve em vigília nessas casinhas, derrubando embarcações portuguesas. **As mulheres da tropa ficaram conhecidas como vedetas, e são bem populares no imaginário popular de Itaparica, associada {20} ao canto de capoeira Maria Doze Homens. Maria seria a Maria Felipa, que teria derrubado 12 homens de uma vez.**
>
> (p. 19-20)

> Do mesmo modo, **os serviços disponibilizados pela Servidora VEDETAS recebem nomes de mulheres consideradas pioneiras em suas áreas de atuação.** O _etherpad_, plataforma para edição de textos que pode ser utilizado simultaneamente por diversas pessoas, recebeu o nome de Antonieta, em homenagem a Antonieta de Barros, primeira mulher negra parlamentar no Brasil em 1934. O _ethercalc_, planilha editável colaborativamente, recebeu o nome de Eveliyn, cientista formada em matemática em 1945 e que foi uma das pioneiras entre as mulheres negras que trabalharam na IBM e na NASA.
> (p. 20)

# Uso de metáforas

> **A proposta aqui {no uso de metáforas para aproximar tecnologias digitais e referências do cotidiano das mulheres} é a de ativar outras formas de redes e tecnologias que operam localmente e que não são necessariamente digitais para tentar construir uma ponte entre diferentes saberes.** Exemplo desta aproximação é a Fuxico, dispositivo móvel e autônomo, que cria uma rede sem fio desconectada da Internet, com o objetivo de compartilhar conteúdo digital em rede local e de forma completamente anônima (Zanolli et al, 2018). **Criada a partir de uma remixagem do projeto de software livre Piratebox (caixa-pirata), a Fuxico estabelece uma ressignificação deste dispositivo ao substituir a {21} identidade visual e o nome caixa-pirata pela imagem de um fuxico, técnica artesanal com retalhos de tecido, datada do período colonial, muito comum no interior do nordeste brasileiro.**  Cada trouxinha de tecido é costurada a muitas outras para a confecção de peças de vestuário e decoração, em uma prática realizada por mulheres que se reúnem em torno desse fazer para um espaço de troca e conversa. **Ao fazer esta associação, a Fuxico quer acionar o sentido do compartilhamento de saberes entre mulheres em um espaço íntimo e seguro como objetivo principal do projeto.**
> (p. 20-1)

> Os conceitos expressados aqui se aproximam das reflexões trazidas por Maffia (2005), que, ao formular sua crítica às ciências hegemônicas, revela que a produção de „verdades‟ é baseada em falsas noções de objetividade e neutralidade, que requisitam o uso de linguagem literal e a exclusão da emoção, considerando, assim, que as metáforas, longe de ter valor para o conhecimento, criam obstáculos para o sentido. A vivência dos minicursos VEDETAS, por exemplo, é exatamente a oposta, as metáforas são valorizadas e, muitas vezes, aperfeiçoadas pelas participantes, que propõem outras mais eficientes ou formulam suas próprias comparações para testar um conhecimento apreendido ou conectá-lo com um conhecimento anterior. A verdade aqui aparece, portanto, no sentido proposto por Maffia (2005), de que será verdadeiro aquilo que for legitimado por diferentes perspectivas, sendo, portanto, um sentido que não é acabado, mas que pode ser renegociado.
> (p. 21)

# SEGURANÇA digital feminista

> Enquanto alguns grupos podem se preocupar, especialmente, com a vigilância de aparatos de repressão do governo, como feministas que lidam com a questão do aborto no Brasil; outras podem estar sob risco a partir do conflito de grandes interesses econômicos, como acontece com algumas comunidades impactadas por mineradoras, por exemplo. Outros grupos ainda podem estar sob risco de ataque de seus pares, como mulheres que sofrem violências na rede e fora dela de parceiros ou ex- parceiros ou de pessoas de sua convivência que carregam valores misóginos, LGBTQIA+fóbicos ou racistas, por exemplo. Os ataques podem ser direcionados ainda a propagar o discurso de ódio numa tentativa de promover a autocensura e silenciar vozes diversas. É preciso considerar, enfim, que **mesmo num pequeno grupo, respostas para perguntas como – o que faz você se sentir seguro ou inseguro? – podem variar muitíssimo e que as medidas de segurança devem ser contextualizadas.**
> (p. 22)

> Com isso, a segurança não é pensada numa camada de ferramentas e ações individuais a serem acionadas no uso de determinadas tecnologias, mas **atrelada a uma construção coletiva, {23} que passa pela prática do cuidado mútuo e pela construção e desconstrução do que é coletivamente considerado aceitável ou não.** Também por um **compromisso de compartilhamento de saberes que possam ajudar a informar decisões, considerando possibilidades e limites em contextos específicos,** sem acionar narrativas generalizadas sobre perigos inevitáveis que podem conduzir ao medo, considerando que **preservar o bem-estar das pessoas envolvidas numa experiência compartilhada é também uma medida de segurança, pensada de forma mais ampla, e de autocuidado coletivo.**
> (p. 22-3)

> As tecnologias como tratadas pelas coletivas abrem espaço a novos aspectos por estabelecerem exatamente uma perspectiva feminista sobre a relação com as TICs. A concepção de segurança {26} não está restrita aos artefatos tecnológicos em si ou a práticas em ambiente online, mas se expande para pensar a segurança desde os espaços de aprendizagem e construção destas tecnologias. Essa não é uma perspectiva comum com coletivos que lidam com privacidade e segurança das TICs, mas uma construção que abarca o ponto de partida de mulheres, pessoas trans e não binárias, suas necessidades singulares, e que altera profundamente esses processos.
> (p. 25-6)

> Ainda que voltem-se para questões relativas a regulamentação, governança e advocacy, e criem alianças nessas direções, **as ações mais correntes, dos grupos aqui pesquisados, são aquelas em que o foco incide mais fortemente no compartilhamento de conhecimentos e desenvolvimento de ferramentas e habilidades para que as ativistas comecem a aderir às práticas de segurança, construir novas práticas e ampliar a compreensão sobre as tecnologias de que fazem uso,** reconhecendo os interesses políticos e econômicos que as sustentam.
> (p. 26)

# Espaços seguros (que não incluem homens cis, ou pessoas brancas, por exemplo)

> Outro dispositivo acionado nesse sentido é a **criação de espaços seguros – os encontros exclusivos para determinados grupos sociais, como só mulheres ou só de mulheres negras.** Nas oficinas das VEDETAS, por exemplo, buscando criar um ambiente de fala seguro, homens cis não podem participar. Em eventos mistos, nos quais há algum espaço dedicado à discussão de gênero, como o Espaço Ada na Cryptorave, ainda que não seja possível limitar a presença de homens cisgênero, **busca-se priorizar o lugar de fala das mulheres na coordenação das atividades, o que por vezes gerou conflitos quando essa premissa não foi observada.**
> (p. 23)

> Quando consideramos que **as universalizações que atravessam lugares de fala como a branquitude e a cis-heteronormatividade podem fomentar a sensação de que certos espaços não devem ser ocupados por mulheres, pessoas trans e negras, por exemplo, os espaços exclusivos acabam sendo {24} extremamente importantes para, por alguns momentos, remover fisicamente algumas camadas de hierarquização.** Os recortes raciais e socioeconômicos aplicados na constituição destes espaços seguros são algumas práticas que refletem a posição interseccional assumida pelas coletivas. Critérios como a autodeclaração de raça/cor, o local de residência e as condições econômicas das participantes são levados em conta no momento de inscrição e seleção para oficinas e cursos realizados pela MariaLab, seja dentro do projeto VEDETAS ou fora dele.
> (p. 23-4)

# Cryptorave

> 32: A Cryptorave é um evento com duração de 24 horas, realizado anualmente desde 2014 e organizado de forma voluntária e colaborativa por coletivos ativistas sediados no Estado de São Paulo. É inspirada na Cryptoparty, uma iniciativa global e descentralizada para a realização de eventos que discutem a vigilância e a segurança na rede e introduzem noções básicas de criptografia. Em 2015, a Cryptorave passou a concentrar as atividades que continham a discussão sobre gênero no espaço denominado Ada Lovelace, em homenagem à matemática britânica da primeira metade do século XIX, considerada como a primeira programadora da história. A Coletiva Hackerfeminista MariaLab foi uma das precursoras na organização das atividades programadas para o Espaço Ada.  Atualmente, os debates sobre gênero estão mais disseminados nas diversas trilhas da Cryptorave e inclui também um espaço em homenagem a Chelsea Manning.
> (p. 23)

# Infraestruturas feministas

> O campo das infraestruturas feministas é uma forma de realizar um chamado para que sejamos protagonistas da construção de tecnologias que queremos desde outras perspectivas políticas, alianças, desenfeitiçamentos e interesses. **Infraestruturas feministas, portanto, não se definem apenas por materialidades eletrônicas feitas por mulheres, mas carregam o compromisso com um esforço ativo de repensar o espaço, os pactos, a linguagem e as referências, as relações entre pessoas e grupos e mesmo entre humanos e máquinas a partir de outras perspectivas, que resgatem a memória e saberes locais e indaguem universalizações.**
> (p. 27)

> A partir das experiências das coletivas é possível pensar que as infraestruturas feministas trazem uma reivindicação de materialidade, de heterogeneidade e de proposta de aliança na diversidade entre grupos não hegemônicos que confrontam processos de homogeneização da atividade humana e não humana em bancos de dados digitais e confrontam simultaneamente disputas de poder que atravessam a internet e que também operam fora dela. É importante destacar que **a proposta das infraestruturas feministas não é operar apenas na construção de contraposições e resistências, mas também no sentido de resgatar o exercício da utopia, da imaginação e da criação a partir do encontro entre múltiplos grupos e corpos.** Mais do que resistências a processos hegemônicos, o que se coloca em perspectiva aqui é **a capacidade de insistir em existir, em recusar o apagamento, e em ativar outras vivências e materialidades {28} coletivamente para manter a criação tecnológica aberta ao que não está previsto, não foi programado e calculado ou mesmo para aquilo que já existe em termos de invenção técnica, de saberes e de modos de viver, mas que sofre investidas de invisibilidade.**
> (p. 27-8)
