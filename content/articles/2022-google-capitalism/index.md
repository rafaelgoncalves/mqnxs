---
title: "Capitalismo da Google"
date: 2022-08-05
path: "/capitalismo-google"
category: fichamento
author: Rafael Gonçalves
featuredImage: "./googleplex.jpg"
srcInfo: <a href="https://pt.wikipedia.org/wiki/Googleplex#/media/Ficheiro:Google_Campus,_Mountain_View,_CA.jpg">Googleplex, sede da Google em Mountain View, CA</a>
tags:
    - capitalismo
    - google
    - Karl Marx
    - Chistian Fuchs
    - economia
    - prosumidor
    - commons

published: true
---

Fichamento do artigo _Google Capitalism_[^1] de Christian Fuchs.

[^1]: FUCHS, C. Google Capitalism. **tripleC: Communication, Capitalism & Critique. Open Access Journal for a Global Sustainable Information Society**, v. 10, n. 1, p. 42–48, 30 jan. 2012. Disponível em: <https://www.triple-c.at/index.php/tripleC/article/view/304>

# Abstract

> This paper analyzes Google’s political economy. In section 2, Google’s cycle of capital accumulation is explained and the role of surveillance in Google’s form of capital accumulation is explained. In section 3, the discussion if Google is “evil” is taken up. Based on Dallas Smythe’s concept of the audience commodity, the role of the notion of Internet prosumer commodification is stressed.
> (p. 42)

# Noção de prosumidor, usuários como commodity de audiência, commodity de prosumidores de internet

> Alvin Toffler (1980) introduced the notion of the prosumer in the early 1980s. It means the “progressive blurring of the line that separates producer from consumer” (Toffler 1980, 267). Dallas Smythe (1981/2006) suggests that in the case of media advertisement models, the audience is sold as a commodity to advertisers: “Because audience power is produced, sold, purchased and consumed, it commands a price and is a commodity. […] You audience members contribute your unpaid work time and in exchange you receive the program material and the explicit advertisements” (Smythe 1981/2006, 233, 238). With the rise of user-generated content, free access social networking platforms, and other free access platforms that yield profit by online advertisement – a development subsumed under categories such as web 2.0, social software, social media and social networking sites – the web seems to come close to accumulation strategies employed by the capital on traditional mass media like TV or radio. The users who google, upload photos, and images, write wall posting and comments, send mail to their contacts, accumulate friends or browse other profiles on Facebook, constitute an audience commodity that is sold to advertisers. The difference between the audience commodity on traditional mass media and on the Internet is that, in the latter case, the users are also content producers; there is user-generated content, the users engage in permanent creative activity, communication, community building, and content-production. That the users are more active on the Internet than in the reception of TV or radio content is due to the decentralized structure of the Internet, which allows many-to-many communication. Due to the permanent activity of the recipients and their status as prosumers, we can say that in the case of Facebook and the Internet the audience commodity is an Internet prosumer commodity (Fuchs 2010).
> (p. 43)

# Duplo papel da google na comodificação de prosumidores na internet: exploração de usuários criadores de conteúdo (pela indexação) e condução de trabalho não pago por usuários de serviços.

> Google relates to Internet prosumer commodification in two ways: **On the one hand it indexes user-generated content that is uploaded to the web and thereby acts as a meta-exploiter of all user-generated content producers. Without user-generated content by unpaid users, Google could not perform keyword searches. Therefore Google exploits all users, who create World Wide Web (WWW) content**. **On the other hand users employ Google services and thereby conduct unpaid productive surplus-value generating labour**. Such labour includes for example: searching for a keyword on Google, sending an e-mail via GMail, uploading or searching for a video on YouTube, searching for a book on Google Print, looking for a location on Google Maps or Google Earths, creating a document on GoogleDocs, maintaining or reading a blog on Blogger/Blogspot, uploading images to Picassa, translating a sentence with Google Translate, etc. **Google generates and stores data about the usage of these services in order to enable targeted advertising. It sells these data to
> advertising clients, who then provide advertisements that are targeted to the activities, searches, contents and interests of the users of Google services. Google engages in the economic surveillance of user data and user activities, thereby commodifies and infinitely exploits users and sells users and their data as Internet prosumer commodity to advertising clients in order to generate money profit**. Google is the ultimate economic surveillance machine and the ultimate user-exploitation machine. It instrumentalizes all users and all of their data for creating profit.
> (p. 43-4)

 > Google users are double objects of commodification: 1) they and their data are Internet prosumer commodities themselves, 2) through this commodification their consciousness becomes, while online, permanently exposed to commodity logic in the form of advertisements. Most online time is advertising time served by Google or other online advertising companies.
 > (p. 44)

# Ciclo de acumulação de capital da Google

![Ciclo de acumulação de capital da Google](./google-accumulation.png)

> Figure 2 shows the process of capital accumulation on Google. Google invests money (M) for buying capital: technologies (server space, computers, organizational infrastructure, etc.) and labour power (paid Google employees). These are the constant and variable capital outlays. The Google employees make use of the fixed capital in order to produce (P1) Google services (like Google Search, YouTube, GMail). Google services are no commodities; they are not sold to users, but rather provided to users without payment. Free access provision and a large number of services allow Google to attract many users and to collect a lot of data about their searches. The Google search, Google’s core service, is powered by the unpaid work of all those, who create web pages and web content that are indexed by Google. They are unpaid by Google, although Google uses their content for making money. The Google services and the unpaid labour of web content creators is the combined foundation for the exploitation of the Google users. They engage in different unpaid work activities (searching, e-mailing, creating documents, blogging, reading blogs, uploading videos or images, watching videos or images, etc.) (P2). Thereby a new commodity C’ is created, the Google prosumer commodity. **It is created by the unpaid work of Google users and WWW content creators and consists of a multitude of data about user interests and activities. Google exploits Google users and WWW content producers because their work that serves Google’s capital accumulation is fully unpaid. Google in processes of economic surveillance collects a multitude of data about usage behaviour and users’ interests**. The Google prosumer commodity C’ is sold to advertising clients (the process C’ – M’): **Google attains money (M’) from advertising clients, who in return can use the data of the Google prosumer commodity they have purchased in order to present targeted advertisements to Google users**. Google thereby increases its invested money M by a profit p: M’ = M + p. p is partly reinvested and partly paid as dividend to Google stockowners.
> (p. 44-5)

# Os serviços da Google não são commodities, mas os dados de usuários sim

> For Marx (1867), the profit rate is the relation of profit to investment costs: p = s / (c + v) = surplus value / (constant capital (= fixed costs) + variable capital (= wages)). If Internet users become productive web 2.0 prosumers, then in terms of Marxian class theory this means that they become productive labourers, who produce surplus value and are exploited by capital because for Marx productive labour generates surplus value (Fuchs 2010). **Therefore not merely those who are employed by Internet corporations like Google for programming, updating, and maintaining the softand hardware, performing marketing activities, etc., are exploited surplus value producers, but also the users and prosumers, who engage in the production of user-generated content and data (like search queries on Google). Google does not pay the users for the production of content and transaction data. Google’s accumulation strategy is to give them free access to services and platforms, let them produce content and data, and to accumulate a large number of prosumers that are sold as a commodity to third-party advertisers. Not a product is sold to the users, but the users and their data are sold as a commodity to advertisers. Google’s services are not commodities. They are free of charge. The commodity that Google sells is not Google services (like its search engine), but the users and their data**. The golden rule of the capitalist Internet economy is that the more users a platform has, the higher the advertising rates can be set. The productive labour time that is exploited by Google on the one hand involves the labour time of the paid employees and on the other hand all of the time that is spent online at Google services by the users. For the first type of knowledge labour, Google pays salaries. **The second type of knowledge is produced completely for free (without payment)**. There are neither variable nor constant investment costs.
> (p. 45)

# Cálculo do lucro para a estratégia de acumulação da Google

> The formula fo the profit rate needs to be transformed for this accumulation strategy:
> 
> p = s / (c + v1 + v2)
> 
> s: surplus value, c : constant capital, v1: wages paid to fixed employees, v2: wages paid to users
> (p. 45)

> The typical situation is that v2 => 0 and that v2 substitutes v1 (v1 => v2=0). If the production of content (web content that is indexed by Google) and data (search keywords, data generated by the use of Google services) and the time spent online were carried out by paid employees, Google’s variable costs would rise and profits would therefore decrease. **This shows that prosumer activity in a capitalist society can be interpreted as the outsourcing of productive labour to users, who work completely for free and help maximizing the rate of exploitation (e = s / v = surplus value / variable capital) so that profits can be raised and new media capital may be accumulated. This situation is one of infinite exploitation of the users**. Capitalist prosumption is an extreme form of exploitation, in which the prosumers work completely for free. Google infinitely exploits its users and the producers of web content that is indexed on Google.
> (p. 45)

# Papel importante da vigilância

> Surveillance of user data is an important part of Google’s operations. It is, however, subsumed under Google’s political economy, i.e. Google engages in user surveillance for the end of capital accumulation. Google surveillance is therefore a form of economic surveillance.
> (p. 45)

> Google’s general terms of services (http://www.google.com/accounts/TOS, version from April 16 2008) apply to all of its services. It thereby enables the economic surveillance of a diverse multitude of user data that is collected from various services and user activities for the purpose of targeted advertising: “Some of the Services are supported by advertising revenue and may display advertisements and promotions. These advertisements may be targeted to the content of information stored on the Services, queries made through the Services or other information”.
> (p. 45)

> In its privacy policy (http://www.google.com/intl/en/privacy/privacy-policy.html, version from October 3, 2010), Google specifies that the company “may collect the following types of information”: **personal registration information, cookies that store “user preferences”, log information (requests, interactions with a service, IP address, browser type, browser language, date and time of requests, cookies that uniquely identify a user), user communications, location data, unique application number**. Google says that it is using Cookies for “improving search results and ad selection”, which is only a euphemism for saying that Google sells user data for advertising purposes. “Google also uses cookies in its advertising services to help advertisers and publishers serve and manage ads across the web and on Google services”. To “serve and manage ads” means to exploit user data for economic purposes.
> (p. 45-6)

> The combination of Google’s terms of service and its privacy policy allows and legally enables the collection of a multitude of user data for the purpose of targeted advertising. These self-defined Google rules, in which users have no say and which are characteristic for privacy self-regulation, enable economic surveillance.
> (p. 46)

> Many Internet corporations avoid opt-in advertising solutions because such mechanisms can drastically reduce the potential number of users participating in advertising. **That Google helps advertisers to “serve and manage ads across the web” means that Google uses the DoubleClick server for collecting user behaviour data from all over the WWW and using this data for targeted advertising**. Google’s exploitation of users is not only limited to its own sites, its surveillance process is networked, spreads and tries to reach all over the WWW.
> (p. 46)

> The analysis shows that **Google makes use of privacy self-regulation for formulating privacy policies and terms of service that enable the large-scale economic surveillance of users for the purpose of capital accumulation**. Advertising clients of Google, who use Google AdWords, are able to target ads for example by country, exact location of users and distance from a certain location, language users speak, the type of device used: (desktop/laptop computer, mobile device (specifiable)), the mobile phone operator used (specifiable), gender, or age group (data source: http://adwords.google.com).
> (p. 46)

# Contradições da Google e a ética de "não ser mau": Google como algo perverso dentro das relações capitalistas, mas com potencial positivo fora delas

A análise de Fuchs não ignora a agência inerente às tecnologias? O único problema do Google Docs é estar inserido em relações capitalistas? Ou as próprias técnicas mobilizadas nessas tecnologias produzem o capitalismo tal como existe na contemporaneidade? Ex: sugestão de palavras como redução da autonomia do usuário.

> Google sees itself as “a company that does good things for the world” (Page & Brin, cited in: Jarvis 2009, 99). One of its mottos is: “Don’t be evil”. “You can make money without doing evil” (http://www.google.com/corporate/tenthings.html) is one of the slogans of Google’s philosophy. One should go beyond one-sided assessments of Google and think dialectically: Google is at the same time the best and the worst that has ever happened on the Internet. It is evil like the figure of Satan and good like the figure of God. It is the dialectical Good Evil. Google is part of the best Internet practices because it services can enhance and support the everyday life of humans. It can help them to find and organize information, to access public information, to communicate with others and to co-operate with others. Google has the potential to greatly advance the cognition, communication and co-operation of humans in society. Google is a manifestation of the productive and socializing forces of the Internet. **The problem is not the technologies provided by Google, but the capitalist relations of production, in which these technologies are organized. The problem is that Google for providing its services necessarily has to exploit users and to engage in the surveillance and commodification of user-oriented data**.
> (p. 46)

> At the level of the technological productive forces, we see that Google advances socialization, the co-operative and common character of the online-productive forces: Google tools are available for free, Google Documents allows the collaborative creation of documents; GMail, Blogger, and Buzz enable social networking and communication, YouTube supports sharing videos, Google Scholar and Google Books help better access worldwide academic knowledge, etc. **These are all applications that can give great benefits to humans. But at the level of the relations of production, Google is a profit-oriented, advertising-financed moneymaking machine that turns users and their data into a commodity. And the result is large-scale surveillance and the immanent undermining of liberal democracy’s intrinsic privacy value. Liberal values thereby constitute their own limit and immanent critique. So on the level of the productive forces, Google and other web 2.0 platforms anticipate a commons-based public Internet from which all benefit, whereas the freedom (free service access) that it provides is now enabled by online surveillance and user commodification that threatens consumer privacy**. Google is a prototypical example for the antagonisms between networked productive forces and capitalist relations of production of the information economy (Fuchs 2008, 2011).
> (p. 47)

> “The conditions of bourgeois society are too narrow to comprise the wealth created by them“ (Marx and Engels 1848, 215). Google’s immanent potentials that can enhance human life are limited by Google’s class character – they cannot be realized within capitalism. The critical discussions that maintain that Google advances surveillance society, point towards Google’s immanent limit as capitalist company.
> (p. 47)

> It is a mistake to argue that Google should be dissolved or to say that alternatives to Google are needed or to say that its services are a danger to humanity. **Rather, Google would loose its antagonistic character if it were expropriated and transformed into a public, non-profit, non-commercial organization that serves the common good**. Google permanently expropriates and exploits Internet users by commodifying their content and user data. The best solution is the expropriation of the Google expropriator. Google stands at the same time for the universal and the particular interests on the Internet. It represents the idea of the advancement of an Internet that benefits humanity and the reality of the absolute exploitation of humanity for business purposes. **Google is the universal exploiter and has created technologies that can advance a universal humanity if humans in an act of universal appropriation act as universal subject and free themselves and these technologies from exploitative class relations**.
> (p. 47)

# Sobre as possibilidades de um serviço de buscas público em uma sociedade baseada em commons como realização de uma previsão nos Grundrisse

> Karl Marx stressed that the globalization of production and circulation **necessitates institutions that allow individuals to inform themselves on complex conditions. He said that “institutions emerge whereby each individual can acquire information about the activity of all others and attempt to adjust his own accordingly” and that these “interconnections” are enabled by “mails, telegraphs etc.”** (Marx 1857/58, 161). Is this passage not the perfect description of the concept of the search engine? **We can therefore say that Larry Page and Sergey Brin did not invent Google, but that rather the true inventor of the search engine and of Google was Karl Marx**. But if Marx’s thinking is crucial for the concept of the search engine, shouldn’t we then think about the concept of a public search engine?
> (p. 47)

> How could a public search engine look like? **Google services could be run by non-profit organizations, for example universities** (Maurer, Balke, Kappe, Kulathuramaiyer, Weber and Zaka 2007, 74), **and supported by public funding**. A service like **Google Books could then serve humanity by making the knowledge of all books freely available to all humans without drawing private profit from it**. A public search engine does not require advertising funding if it is a non-profit endeavour. Thereby the exploitation and surveillance of users could be avoided and the privacy violation issues that are at the heart of Google could be avoided. Establishing a public Google were the dissolution of the private business of Google. **This may only be possible by establishing a commons-based Internet in a commons-based society**. For doing so, first steps in the class struggle for a just humanity and a just Internet are needed. **These include for example the suggestion to require Google by law to make advertising an opt-in option and to surveil the surveillor by creating and supporting Google watchdog organizations that document the problems and antagonisms of Google**. Google’s 20% policy is on the one hand pure capitalist ideology that wants to advance profit maximization. On the other hand, **it makes sense that unions pressure Google to make these 20% of work time really autonomous from Google’s control**. If this could be established in a large company like Google, **then a general demand for a reduction of labour time without wage decreases were easier to attain**. Such a demand is a demand for the increase of the autonomy of labour from capital.
> (p. 48)
