---
title: "Da tecnopolítica às lutas cosmotécnicas"
tags:
    - Henrique Parra
    - antropoceno
    - virada cibernética
    - cosmotécnica
    - tecnologia
    - informação
    - tecnoceno
    - capitalismo
    - pandemia
    - fichamento

path: "/da-tecnopolitica-as-lutas-cosmotecnicas"
date: 2024-05-30
category: fichamento
featuredImage: "parra.jpg"
srcInfo: <a href="https://www.unifesp.br/prodmais/profile.php?lattesID=8314245614310718">Henrique Parra</a>
published: true
---

Fichamento do artigo "Da tecnopolítica às lutas cosmotécnicas" de Henrique Parra[^1].

[^1]: PARRA, Henrique Z.M. Da tecnopolítica às lutas cosmotécnicas: dissensos ontoepistêmicos face à hegemonia cibernética no Antropoceno. In. KLEBA,J. ; CRUZ,C.; ALVEAR, A. (org). **Engenharias e outras práticas técnicas engajadas : diálogos interdisciplinares e decoloniais**. Campina Grande: EDUEPB, 2022, p. 339-394. Disponivel em: <https://www.pimentalab.net/wp-content/uploads/2022/08/Tecnopoliticas_Lutas_Cosmotecnicas_Henrique_Parra_2022.pdf>

# Pandemia de covid-19 como acontecimento

> Se pensamos as tecnologias para além da materialização artefactual e dos procedimentos técnicos de delegação e mediação (LATOUR, 1994), considerando-as também como criadoras de ambientes dentro dos quais formas de vida são produzidas, **a pandemia de Covid-19 nos convoca a refletir sobre aspectos ontológicos e políticos que participam da criação tecnológica e dos mundos que elas engendram** (ESCOBAR, 2016). Assim como a bomba atômica ou a chegada do humano na Lua podem ser tomadas como marcadores históricos da formação de um diagrama tecnocientífico construído na confluência das dinâmicas entre ciência, tecnologia, militarização, capitalismo e geopolítica, **o acontecimento pandêmico pode ser interpretado como um ponto de chegada e de possível inflexão desse modelo.**
gg> (p. 341)

# Fracasso em desacelerar ante à pandemia

> Passados dois anos de sua eclosão, nosso maior assombro diante da crise pandêmica já não é a percepção dos agenciamentos que a produziram, mas sim **a dura constatação da nossa incapacidade coletiva de fazer parar a locomotiva do progresso**, como interrogava Bruno Latour acerca dessa possibilidade nas primeiras semanas da pandemia (2020). Ainda que aquele primeiro momento da pandemia, em diversos países, tenha sido marcado por uma desaceleração na atividade econômica e pela adoção de medidas de isolamento social, **o “novo normal” foi rapidamente se infiltrando nos discursos e práticas cotidianas, intensificando tendências que já estavam em curso**. A expansão dos arranjos cibermediados em diversos domínios da vida; a crescente financeirização e o extrativismo em suas diversas expressões; a adoção de medidas securitárias por governos, tudo para garantir que nosso modo de vida seguisse fluindo sob as novas condições intrapandêmicas. Nesse sentido, o Sars-Cov-2, em sua manifestação apocalíptica, é portador de revelações sobre o funcionamento das coisas.
> (p. 341-2)

# Objetivo do texto

> O texto a seguir deseja contribuir com ideias e perguntas que possam inspirar percursos para pesquisas situadas sobre uma formação específica, descrita na sessão seguinte: a confluência entre a dominância tecnopolítica, a hegemonia cibernética e o capitalismo financeirizado. Em seguida, apresentamos um conjunto heterogêneo de disputas tecnopolíticas, especialmente no campo das tecnologias digitais, procurando rastrear uma ontologia relacional própria à mediação tecno-digital. Ao final, lançamos a hipótese de um transbordamento das lutas tecnopolíticas em direção a conflitualidades cosmotécnicas, logo, cosmopolíticas.
> (p. 343)

# Alargamento da noção de cosmotécnica em Yuk Hui (fundamento ontológico das tecnologias)

> Pensamos a cosmotécnica não apenas como afirmação da tecnodiversidade (reconhecimento do caráter não abstrato-formal-universal de uma técnica), **mas como a reivindicação de um fundamento ontológico que articula as técnicas, os saberes e as formas de vida, de maneira que uma cosmotécnica específica é indissociável dos modos de ser-conhecer-agir que lhe dão existência**. A perspectiva cosmotécnica argumenta que toda tecnologia possui um fundamento ontológico. Nos termos de Arturo Escobar,
> 
> > Cada herramienta y tecnología es ontológica en el sentido de que, por muy humilde o insignificante que sea, inaugura una serie de rituales, formas de hacer y modos de ser. Las tecnologías son lo que Haraway llamó “actores materiales-semióticos” (1991) que contribuyen a dar forma a lo que es ser humano” (ESCOBAR, 2016, p.128).
> 
> (p. 344)

# Aproximação entre a perspectiva cosmotécnica e as abordagens decoloniais vinculadas a uma política ontológica

> Tal abordagem, aproxima a perspectiva cosmotécnica das abordagens decoloniais vinculadas a uma política ontológica (ESCOBAR, 2016), na qual as divisões e formas de classificação do pensamento moderno serão contrastadas com outras ontoepistemologias:
> 
> > Se quisermos ultrapassar a modernidade, não há uma forma de simplesmente reiniciá-la como se ela fosse um computador ou um _smartphone_. Em vez disso, precisamos escapar de seu eixo de tempo global, escapar de um (trans)humanismo que submete outros seres aos termos de nosso destino e propor uma nova agenda e uma nova imaginação tecnológica que possibilitem novas formas de vida social, política e estética e novas relações com não humanos, a Terra e o cosmos (HUI, 2020, p.95).
> 
> (p. 344-5)

> Ao acompanhar algumas lutas tecnopolíticas, podemos visualizar como a existência de outras formas de vida depende de uma pluralidade técnica, na qual a produção de conhecimentos e a criação tecnológica são compreendidas de forma situada e corporificada. **O local importa, o corpo importa, a posição do humano num cosmos importa, bem como os efeitos de sua ação**. Uma prática política ontológica não é só uma nova forma de descrever um velho problema; ela é sobretudo uma forma de redesenhar a conflitualidade a partir de um outro diagrama de forças, que ativa outros possíveis e futuros, outros imaginários tecnológicos:
> 
> > Como digo en broma, parafraseando, ‘denme una maloca y levantaré un mundo relacional’ (incluyendo las interrelaciones profundas entre los humanos y los no humanos); al contrario, denme una casa en los suburbios y levantaré un mundo de individuos descomunalizados, separados del mundo natural. Por eso el diseño genera, inevitablemente, las estructuras de posibilidad humanas (y de otros seres de la Tierra). El problema con el diseño moderno, sin embargo, es que ha estructurado la insostenibilidad como la forma dominante de ser (ESCOBAR, 2016, p.129).
> 
> (p. 345)

# Informatização como tópos tecnológico da contemporaneidade

> Abordamos as dinâmicas societais a partir da agência dos arranjos tecnociencientíficos nas economias capitalistas, **considerando que a informatização caracteriza o _tópos_ tecnológico de nossa época, participando de forma imanente das reconfigurações dos regimes de produção de conhecimento, cultura, economia e técnicas de exercício do poder.**
> (p. 345-6)

> A tecnociência e a informatização são portadoras de uma força de abstração-formalização, com pretensões de universalidade e objetividade há muito questionadas (MARTINS, 2006). Ainda assim, a crescente reticulação3 produzida pelas tecnologias de informação e comunicação digital fornece a infraestrutura simbólica e material para a expansão da fronteira do codificável e mensurável. Tal possibilidade instala novos conflitos em torno da partilha do sensível, modifica o meio associado, dispara novas individuações e disputas políticas (PARRA, 2009). Formações culturais, conceituais e jurídicas que organizaram por muito tempo a vida social são reorganizadas: relação tempo-espaço, público-privado, trabalho e não trabalho, vivo e não vivo etc. A mediação técnica altera a própria qualidade da relação e os entes que dela participam. Na dimensão econômica, ao mesmo tempo em que se ampliam as economias de cooperação e produção entre pares (BAUWENS, 2008; BENKLER, 2006), surgem novas possibilidades de cercamento e apropriação do Comum através de novas tecnologias jurídicas de expansão do regime proprietário (BOYLE, 2003) e de novas infraestruturas tecnológicas que promovem a hiperconcentração digital, alargando-se o campo da extração e da produção de valor.
> (p. 346)

# Reticulação em Simondon

> 3: As reticulações são, para Simondon, “as operações de transformação de uma realidade amorfa, pré‐individual, potencial, em realidades estruturadas, individualizadas, concretizadas, i.e., as operações transdutivas pelas quais um novo indivíduo e seu meio emergem (FERREIRA, 2017, p.119). Ou ainda, “a reticulação é um processo de propagação não linear de uma estruturação emergente criando níveis subsequentes de estruturação de uma realidade” (PARRA, 2019, p.115). 
> (p. 346)

# Informatização e financeirização da economia a partir de 1970s

> Essas tendências, a partir de meados dos anos 1970, **convergem e reforçam os processos de financeirização da economia, inaugurando novas potências de cálculo e tecnologias de extração e financeirização (GORZ, 2005; MATTELARD, 2005; MEZZADRA; NEILSON, 2017).**
> (p. 356-7)

# Eixos de análise da informatização

> Sinteticamente, investigamos esse processo em torno de três vetores analíticos:
> 
>  - a. **Tecnopolítica**: vivemos em ambientes extremamente organizados pelo funcionamento de grandes arranjos sociotécnicos, cujo modo de operação possui forte agência sobre nossos modos de vida. Basta pensar no desenho das cidades, em nossa dependência das infraestruturas de comunicação, nas redes de distribuição e fornecimento de bens e serviços básicos, nos modos de organização do trabalho e na crescente mediação de artefatos tecnológicos na vida cotidiana. **O poder se inscreve e se atualiza de forma imanente aos ambientes técnicos** (WINNER, 1986; FEENBERG, 2010; MARTINS, 2018).
>  - b. **Virada Cibernética**: a produção de conhecimento científico e desenvolvimento tecnológico, gestada e promovida na aliança entre grandes corporações privadas e os estados nacionais, dá forma ao bio-info-tecnocapitalismo financeirizado, amparada numa ontologia informacional (de caráter abstrato-formal-matemático) (SANTOS, 2003; 2011). **A crescente mediação das tecnologias de informação e comunicação digital fornece a infraestrutura para a confluência entre a produção de conhecimento simulacional (MARTINS, 2005) e novas tecnologias de exercício do poder amparadas na cibernética** (SILVEIRA, 2019; GALLOWAY, 2004; PARRA, 2009).
>  - c. **Financeirização e extrativismo**: finanças como tecnologia de extração de valor; a expansão da fronteira extrativista, tanto sobre territórios e bens naturais/materiais, como sobre a totalidade da vida cibermediada: cultura conhecimento, experiências, elementos intangíveis que, graças à codificação digital, tornam-se mensuráveis. **Há um novo avanço sobre o Comum pelas dinâmicas de extração transnacionais na escala planetária; energia, bens materiais, corpos e tempo vital** (GAGO; MEZZADRA, 2015; ROLNIK, 2019).
> 
> (p. 348)

# Informatização e pandemia de covid-19

> A maneira como a pandemia de Covid-19 evidencia a articulação entre esses três vetores acima pode ser exemplificada, respectivamente, nas seguintes dinâmicas: (a) a dificuldade para modificar o funcionamento das grandes cadeias globais de produção e circulação de mercadorias; (b) a centralidade política conferida, no enfrentamento da pandemia, às respostas biomoleculares-fármaco-corporativas-financeirizadas e às tecnologias digitais para o governo das populações e para a organização produtiva da vida; (c) a expansão econômica e a concentração de riquezas operadas por empresas de tecnologias de informação (infraestruturas de comunicação para o trabalho e educação, logística e intermediação de entregas etc.), e por grandes corporações do agronegócio.
> (p. 348)

# A pandemia como acontecimento cosmopolítico do Antropoceno

> Diante dessas dinâmicas, perguntamo-nos __como fazer da pandemia um acontecimento cosmopolítico do Antropoceno?__ Latour descreverá essa operação como a introdução de um vetor/ atrator transversal aos conflitos em jogo, reorganizando o campo científico e político a partir de uma outra geografia do pensamento, uma epistemopolítica “terrana”(2020), na qual os divisores que organizam o mundo moderno-ocidental são tensionados pela exigência de pensarmos a partir da não separação (naturezacultura; técnica-política; humano-não humano); um esforço de pensar e viver com outras formas de vida que coabitam a Terra. Em suas formas de ser-conhecer-agir, muitos povos ameríndios e outras populações não modernas-ocidentais vivem essa questão em termos da inseparabilidade e da coexistência entre o humano e o não humano (DANOWSKI; VIVEIROS de Castro, 2014).
> (p. 348-9)

# Modernização como processo de sincronização global em Hui

> Yuk Hui pensa a **modernização como um processo de sincronização global, no qual diferentes tempos históricos convergem para um mesmo eixo temporal. Nesse processo, alguns conhecimentos serão priorizados em função da sua capacidade de fortalecer as dinâmicas econômicas e políticas**. Na acepção de Hui, **essa seleção é realizada e materializada por meio da disseminação e adoção de tecnologias específicas, portadoras de formas de conhecer e racionalidades específicas** (HUI, 2020).
> (p. 349)

# Crítica à noção de tecnologia apropriada (TA)

> Na América Latina, já nos anos 60 do século XX, projetos de modernização tecnológica oriundos dos países do Norte global – presentes em modelos nacional-desenvolvimentistas – eram problematizados como portadores de dinâmicas que sustentavam a manutenção do desenvolvimento subalterno e dependente dos países do Sul global. **A noção de “Tecnologia Apropriada” surge como crítica sociotécnica, indicando a necessidade de criação de tecnologias adequadas ao contexto local, capazes de promover relações mais autônomas e soberanas, sem as quais as assimetrias geopolíticas não seriam superadas**. Ao longo dos anos 70 e 80, diversas iniciativas inspiradas pela abordagem das Tecnologias Apropriadas deram forma a um amplo movimento em toda a América Latina (SMITH et al., 2017). Seguindo uma trilha distinta, **Hui complementa essa reflexão a partir do argumento cosmotécnico, reverberando de certa maneira a crítica decolonial, que questiona o caráter pretensamente universal de certas tecnologias, indicando como _“seu funcionamento é assegurado e limitado por cosmologias particulares que vão além da mera funcionalidade e da utilidade”_ (2021, p. 25).**
> (p. 349-350)

# Tecnocracia, hegemonia e horizonte tecnológico em Feenberg

> A esse processo de colonização tecnológica, adicionamos uma problematização elaborada por Andrew Feenberg, relativa às tensões entre o desenvolvimento tecnológico e as dinâmicas sociopolíticas. O autor formula esse problema a partir do **conceito de _tecnocracia_, caracterizada como a expansão e uso da delegação técnica como forma de consolidar e legitimar um sistema de controle hierárquico, resultante da seleção sistemática e de longa duração de alternativas tecnológicas que favorecem determinadas relações de poder** (FEENBERG, 1994). **O desenho das máquinas, a maneira como o _código técnico_ é definido, reflete os fatores sociais operantes em uma racionalidade dominante, dando forma à hegemonia de um horizonte tecnológico específico**. Para Feenberg, **a hegemonia refere-se a uma _“forma de dominação tão profundamente arraigada na vida social que parece natural a esses a quem domina”_ (2010, p.79); o horizonte tecnológico, por sua vez, _“se refere a suposições genéricas e culturais que formam um pano de fundo inquestionado para qualquer aspecto da vida”_ (idem), informando os sentidos e critérios de eficiência e racionalidade técnica**. A tecnologia, uma vez introduzida, “oferece uma validação material do horizonte cultural para o qual foi pré-formada. […] A racionalidade funcional aparentemente neutra está alinhada em defesa de uma hegemonia” (2010, p. 82).
> (p. 350)

# Sobre a criação de mundo nas ações cotidianas

> Temos dificuldade em imaginar outros horizontes tecnológicos; desejamos e estamos habituados às soluções tecnológicas que se apresentam naturalmente como mais econômicas, práticas ou eficientes. Do sistema operacional que instalamos em nossos computadores pessoais aos alimentos que colocamos em nossos pratos, **realizamos ações que ativam longas cadeias sociotécnicas e sistemas econômicos. Mundos são produzidos, uma cosmotécnica específica é perpetuada.**
> (p. 351)

# Tecnoceno e antropoceno em Hermínio Martins

> Noutra escala do problema, **a noção de Tecnoceno, utilizada por Hermínio Martins, nos permite analisar os processos que tornam possível o Antropoceno sob uma perspectiva complementar, na qual os efeitos que produzem essa nova era geológica resultariam da agência tecnológica que ultrapassa os processos psico-físico-biológicos que constituem o _Homo sapiens sapiens_**. Argumenta-se que **a trajetória da nossa espécie tornou-se interdependente e mutuamente promotora da tecnicização e da mercantilização; a combinação do desenvolvimento tecnocientífico com as dinâmicas capitalistas de mercantilização promovem transformações radicais na própria caracterização do humano, do seu corpo e das instituições que regulam a vida social, dando forma ao Tecnoceno** (MARTINS, 2018).
> (p. 351)

> 4: Há, evidentemente, uma performatividade na construção dessa noção, cuja precisão conceitual é controversa. Antropoceno, Capitaloceno, Tecnoceno e Plantationceno formam uma paisagem conceitual proximal e ao mesmo tempo heterogênea, cujas diferenças e tensões não serão aqui discutidas. Situamo-nos nesse limiar conceitual para destacar o que nos parece central no atual contexto histórico: a agência da _monocultura-tecnocientífica-colonial-capitalista-antropocêntrica_, que hoje ameaça a vida no planeta.
> (p. 351)

# Definição de tópos tecnológico

> Reconhecendo a força adquirida pela agência dos artefatos tecnológicos em sociedades altamente tecnicizadas, pode-se avançar na caracterização das técnicas específicas que estão na base das transições sociotécnicas. **Em cada momento histórico, pode-se estabelecer relações de interdependência e codeterminação entre as dinâmicas socioeconômicas, as instituições e formas de ação política, as formas de produção-circulação-acesso ao conhecimento, as técnicas e artefatos de uma época, caracterizando um _tópos tecnológico_**. Pode-se pensar nas tecnologias marítimas, nas máquinas térmicas, nos engenhos mecânicos, na energia fóssil, eletricidade, na combinação e sobreposição entre elas e sua relação com os novos saberes e poderes que elas tornam possíveis.
> (p. 352)

# Informatização como resultado e meio de processos contemporâneos (virada cibernética)

> A partir da década de 1940, a informatização torna-se gradualmente a base técnica comum que marcará inúmeras transformações tecnológicas, sociais, culturais, econômicas e políticas nas décadas seguintes, especialmente a partir dos anos 70 (com a revolução microeletrônica) e, mais aceleradamente, nos anos 90 (com a disseminação das redes de informação e comunicação digital) (CASTELLS, 1996, 1997, 1998). **Um primeiro aspecto a ser destacado, e que talvez seja o mais sutil e ao mesmo tempo tão profundamente estruturante, é o fato de a informatização ser simultaneamente o resultado de tecnologias materiais (artefatos, infraestruturas, dispositivos) que realizam a digitalização, transmissão e processamento de informações, e o _medium_ de uma ontologia informacional que terá implicações nos processos socioculturais, econômicos e epistêmicos (MARTINS, 2005; SANTOS, 2003)**. Nos dizeres de Hermínio Martins (2005), **as tecnologias de informação causaram a maior tecnomorfia dentre todas as tecnologias anteriores, além de fornecerem metáforas e entendimentos que penetraram em todas as áreas do conhecimento**. Outra maneira de nomear essa transversalidade é a noção de “Virada Cibernética”, utilizada por Laymert Garcia dos Santos (2003).
> (p. 352-3)

# Informação em Simondon

> As tecnologias de informatização e a emergência da cibernética como um campo transdisciplinar partilham de uma noção de informação que irá oferecer um princípio unificador entre diversas áreas de conhecimento. Como aponta Gilbert Simondon com relação a esse primeiro momento da cibernética:
> 
> > Seria preciso definir uma noção que fosse válida para pensar a individuação na natureza física tanto quanto na natureza viva, e em seguida para definir a diferenciação interna do ser vivo que prolonga sua individuação, separando as funções vitais em fisiológicas e psíquicas. Ora, se retomamos o paradigma da tomada de forma tecnológica, encontramos uma noção que parece poder passar de uma ordem de realidade a outra, em razão de seu caráter puramente operatório, não vinculado a esta ou àquela matéria, e definindo-se unicamente em relação a um regime energético e estrutural: a noção de informação” (SIMONDON apud SANTOS, 2003, p. 13).
> 
> (p. 353)

# Ubiquidade da noção de informação no século XX

> Ao longo da segunda metade do século XX, essa concepção de informação torna-se presente em diferentes disciplinas e se hibridiza gradualmente com o desenvolvimento tecnocientífico. A convergência entre as nanotecnologias, biotecnologia, informática e ciências cognitivas (área conhecida como NBIC) compartilha inicialmente de uma mesma concepção de informação (MARTINS, 2005).
> (p. 354)

# Informação e mediação técnica

> A crescente mediação das tecnologias digitais cibernéticas em distintos domínios da vida materializa a reticulação informacional, expandindo a fronteira do codificável e mensurável. Tudo que for passível de ser traduzido em código digital, tudo que for algoritimizável, entrará em tensão com as forças de abstração informacional. Categorias modernas que haviam organizado a vida social por um longo período são subitamente tensionadas por uma nova ambiência criada pela mediação digital. Basta pensarmos na reconfiguração das fronteiras entre o público e o privado, o tempo de trabalho e não trabalho, as definições de presença e ausência etc.
> (p. 354)

> A produção de um mundo em que a informação adquire centralidade ontoepistemológica encontrará ressonância na crescente mediação das tecnologias digitais. Esse reforço sinérgico faz da informação digital uma espécie de medida geral. É o que Hermínio Martins denominará criticamente de _Metafísica da Informação_ (2005)5 .
> (p. 354)

# Science in vivo, in vitro e in silico em Hermínio Martins

> 5: No campo científico, essa formação dará maior força a processos de investigação amparados no conhecimento simulacional. Martins chega a caracterizar a emergência de uma espécie de 3º tipo de ciência empírica, fazendo da simulação uma tecnologia de epistemogêneses: [primeiro tipo] _Sciencing in vivo_: estuda seres naturais no “mundo selvagem”, com ou sem instrumentos especiais; [segundo tipo] _Sciencing in vitro_: estuda os seres naturais no laboratório (wet labs), para observação detalhada, experimentação física de forma controlada (Bacon e Humboldt combinam os dois primeiros tipos); **[terceiro tipo] _Sciencing in silico_: e-sciences, cybersciences: estuda os processos, mecanismos e dinâmicas de entidades naturais, culturais, através da simulação computacional (dry labs) (MARTINS, 2005, tradução nossa).**
> (p. 354-5)

# Intensificação no pós-guerra e regime internacional de propriedade intelectual

> É também no pós-guerra que se fortalece a arquitetura institucional da tecnociência e das políticas de inovação nutridas em novas interações entre a economia capitalista, produção científica, militarização e as disputas geopolíticas. A emergência da chamada Sociedade da Informação (e mais tarde Sociedade do Conhecimento) e **o fortalecimento de um novo regime de regulação internacional da propriedade intelectual são formações que caracterizam (a partir dos anos 70 e 80) essa profunda mutação societal (MATTELARD, 2005).**
> (p. 355)

# Intervenção e modelização de futuros

> Em síntese, o campo de intervenção amplia-se para a modelização de futuros; cenários que passam a determinar e legitimar a modulação das ações no presente (expectativas de mercado; projeções de rendimentos financeiros e valores monetários; cenários de crises e efeitos políticos; marketing eleitoral etc.):
> 
> > A ambição maior da nova economia é assenhorar-se da dimensão virtual da realidade, e não apenas da dimensão da realidade virtual, do ciberespaço, como tem sido observado. Se tivermos em mente que a dimensão virtual da realidade começa a ser mais importante em termos econômicos do que a sua dimensão atual, teremos uma ideia melhor do sentido da corrida tecnológica. Aliado à tecnociência, o capitalismo tem a ambição de apropriar-se do futuro (SANTOS, 2003, p. 18).
> 
> (p. 355-6)

# A hipótese cibernética em Tiqqun

> **Governar o futuro, modular os possíveis, gerir o presente como um campo de probabilidades calculáveis e administráveis é o projeto político que anima a “hipótese cibernética” , segundo o coletivo TIQQUN (2001)**. Nesse texto, o grupo analisa com precisão a forma como essa ontoepistemologia informacional articula-se ao desenvolvimento da razão neoliberal (modo de subjetivação e racionalidade socioeconômica), introduzindo uma nova ordem política como princípio organizador da vida social. **A hipótese cibernética concebe o mundo social como um campo de interações informacionais, no qual as entidades comunicantes (indivíduos, empresa, mercado, estado…) são reduzidas a construtos (in)formados, de tal maneira que suas ações sejam traduzíveis em um campo de probabilidades (redução do possível ao provável) passível de modulação, pilotagem, governo (_kubernetes_)**. Para isso, **é necessário que um força de abstração e codificação seja capaz de transformar/reduzir todas as coisas e entidades a elementos (in)formados, cuja agência seja passível de abstração e
> formalização**.
> (p. 356-7)

# Governamentalidade neoliberal e algorítmica

> **A cibernética, como princípio político, fornece uma lógica protocolar de organização da vida social, um sistema de autoridade distribuída, capaz de produzir ordem num campo de ações heterogêneas** (GALLOWAY, 2004). Para isso, indivíduo, empresa, mercado, estado, natureza, todos devem operar segundo um mesmo fundamento ontológico. Nessa nova ambiência tecnológica, a biopolítica imunitária e a governamentalidade neoliberal desdobram-se na governamentalidade algorítmica:
> 
> > A governamentalidade algorítmica apresenta uma forma de totalização, de encerramento do “real” estatístico sobre si mesmo, de redução da potência ao provável, de indistinção entre os planos de imanência (ou de consistência) e de organização (ou de transcendência), e constitui a representação digital do globo imunitário, de uma atualidade pura, expurgada, de modo preemptivo, de toda forma de potência de porvir, de toda dimensão “outra”, de toda virtualidade. Esse “impedimento da falha” da modelização digital dos possíveis – pela preempção dos possíveis ou pelo registro e inscrição automática de toda “irregularidade” nos processos de refinamento dos “modelos”, “padrões” ou perfis (no caso dos sistemas algorítmicos autodidatas) – retira do que poderia surgir do mundo em sua dissimetria relativa à realidade (aqui, o que lhe substitui é o corpo estatístico) sua potência de interrupção, de colocar em crise (ROUVROY; BERNS, 2015, p. 53).
> 
> (p. 357)

# Digitaliação, dataficação e dataísmo

> Na hegemonia cibernética, **a digitalização fornece as condições – o meio, o código e a infraestrutura material – para a _dataficação_ : a possibilidade de tradução de algo em dados mensuráveis e quantificáveis (SANTAELLA; KAUFMAN, 2021).** Sabemos, todavia, que não há dados “brutos”. Todo dado informacional depende de uma rede de relações conflituosa, capaz de definir o quê e de que forma algo será convertido em um índice mensurável. Ou seja, **a criação de um dado é sempre precedida por embates epistêmicos e políticos que definem as condições de existência dos dados**. Um importante desdobramento societal da crescente dataficação é a produção de um regime de verdade informacional, **o _dataismo_ (SANTAELLA; KAUFMAN, 2021), no qual os dados digitais adquirem legitimidade e poder explicativo, veículos do melhor acesso ao real e verdadeiro.**
> (p. 358)

> 7: Santaella e Kaufman utilizam o termo “datificação” para descrever esse fenômeno (SANTAELLA; KAUFMAN, 2021). Todavia, na literatura em português, o mais habitual é a adoção de “dataficação”. Optei por seguir essa segunda grafia.
> (p. 358)

# Comodificação informacional

> Considerando que a digitalização e a reticulação digitalcibernética estão predominantemente submetidas a processos de produção de valor que acontecem através de infraestruturas, algoritmos8 e plataformas digitais privadas-corporativas, a _comodificação informacional_ intensifica a ressonância interna entre o arranjo tecnológico, os modos de subjetivação e a racionalidade econômica neoliberal.
> (p. 358-9)

# Modelização comportamental em Bruno, Bentes e Faltay

> 8: Fernanda Bruno, Anna Bentes e Paulo Faltay têm investigado a maneira como a crescente dataficação e mediação algorítmica em aplicativos de saúde pessoal e plataformas de redes sociais criam dinâmicas de modelização comportamental, quando as métricas de autoavaliação e de ranqueamento social vão se constituindo como referências para a automodelização existencial. As autoras problematizam, inclusive, quais os paradigmas epistemológicos e os modelos de psicometria utilizados na construção dos softwares e algoritmos, indicando uma atualização da matriz comportamental behaviorista agora algoritmicamente mediada (BRUNO, BENTES; FALTAY, 2019).
> (p. 358-9)

# Financeirização, informatização e extrativismo

> Tal expansão [[ da financeirização ]] e os cálculos complexos que ela exige, sobretudo na circulação internacional das informações financeiras, depende de uma infraestrutura de comunicação informatizada. Internacionalmente, esse processo é acompanhado pela crescente _comodificação_ da economia, transformando cada vez mais diversos bens econômicos (de consumo, recursos primários, energia etc.) em _commodities_. A expansão, nas últimas décadas, da fronteira de exploração – territorial e imaterial – de entidades (naturais e culturais) convertidas em recursos/ commodities transacionadas internacionalmente é problematizada como uma atualização das dinâmicas coloniais extrativistas, fortalecidas na América Latina, por meio de pactos políticos nacional-desenvolvimentistas do chamado ciclo progressista, momento histórico-político também denominado de “_consenso das commodities_” (SVAMPA, 2015; 2019).
> (p. 359-60)

# Excedente informacional, mais-valia comportamental e produção de valor a partir de informação

> Para além dessas duas dimensões da financeirização, interessa-nos indicar a forma como **a crescente mediação digital-cibernética expande a fronteira do codificável, criando um novo campo de exploração e produção de valor**. **Toda e qualquer interação num meio digital-cibernético produz uma nova informação. Podemos chamar essa tecnicidade específica da comunicação digital-cibernética de “_excedente informacional_”**. Na medida em que toda informação digital é quantificável, **quando ela é traduzida e inserida nos circuitos de produção e extração de valor, tal fenômeno é caracterizado por Zuboff como mais-valia comportamental** (ZUBOFF, 2021). A linguagem, a cultura, o conhecimento, mas também a livre cooperação, os afetos, a subjetividade tecnomediada, tornam-se objeto das tecnologias de extração: ação de abstração e inserção num circuito de valorização de capital. Evidentemente, **isso depende da criação de novas tecnologias proprietárias, capazes de estabelecer regimes de escassez e rivalidade sobre bens cuja natureza não rival, imaterial e abundante seria, de outra maneira, antiproprietária; ou mais ainda, novas formas de extração sobre o terreno do Comum – aqui entendido em sua dimensão relacional e não substancial**. O extrativismo digital é especialmente capaz de atuar como força colonizadora na apropriação de bens não rivais, na expropriação de bens comuns.
> (p. 360)

# Duplo encontro da finenceirização com as TIs

> O ponto de encontro entre a financeirização da economia global e as tecnologias digitais manifesta-se sobre os corpos e territórios físicos de diferentes formas: como extração através de processos de financeirização (SASSEN, 2017; 2013); mas também na extração de recursos (água, minérios, energia) necessários ao funcionamento das tecnologias e redes globais de informação digital (CRAWFORD, 2021). No primeiro caso, basta pensarmos na combinação das políticas de especulação imobiliária, na monetização da vida de bairros e cidades em aluguéis, no capitalismo de plataforma como o Airbnb. Ou ainda, no capitalismo de vigilância tão bem descrito por Shoshana Zuboff ao investigar a produção, análise e extração da mencionada “mais-valia comportamental”, obtida graças à coleta e mineração de dados pessoais, dados residuais e relacionais em grandes escalas.
> (p. 361)

# Mineração (virtual e física)

> A imagem da “mineração” tem aqui um duplo sentido: ela é tanto um ato de prospecção do virtual, uma forma de atuação algorítmica nos mercados de futuros; como uma imagem diretamente relacionada à extração/obtenção de recursos naturais, como minerais preciosos ou energia. **Ambos os processos, na prática, estão conectados verticalmente e de forma multiescalar quando pensamos na conexão entre a extração de minérios, água e energia necessários para a produção e funcionamento das máquinas digitais que “materializam” a informatização que, por sua vez, realizará novos ciclos de extração digital sobre as relações cibermediadas.**
> (p. 361)

# Relação desses processos com o Brutalismo em Mbembe

> No Tecnoceno, a monocultura tecnocientífica, a hegemonia cibernética e o capitalismo financeirizado confluem sob uma cosmovisão orientada para a redução da vida a recursos exploráveis, atuando pela conversão do mundo às forças de tecnicização, abstração e extração. Mbembe, em seu livro mais recente, reflete sobre essa convergência: “a transformação da humanidade em matéria e energia é o projeto final do brutalismo” (MBEMBE, 2021, p. 19).
> (p. 361)

# Informação e ontologia relacional em Simondon

> No rastro de Gilbert Simondon, a noção de informação pode ser concebida de maneira relacional (contextual e situada), ao invés da informação como unidade-abstrata-formal. A informação é sempre uma diferença, um potencial, um devir que se efetua na relação (SIMONDON, 2015). Tal perspectiva instala dissensos sobre o saber-poder da governamentalidade algorítmica, imanente às sociedades de controle. Simondon convida-nos, assim, a outra metafísica da informação (VILALTA, 2021), na qual o sentido é uma propriedade emergente, e a relação (o entre) é dotada de consistência ontológica. Diversamente do fundamento político tecnoliberal que alimenta a Hipótese Cibernética descrita por Tiqqun (2001) – a imagem de uma sociedade organizada a partir do livre fluxo informacional entre entidades autônomas –, a concepção relacional de informação de Simondon dialoga com cosmovisões que partilham de uma ontologia relacional.
> (p. 362)

# Resistências pelo movimento negro como criação de novas cosmotécnicas

> Romper os imaginários tecnológicos hegemônicos, romper a _imaginação carcerária_ significa lutar por formas de vida que estejam inspiradas por outras cosmovisões: _“a era do Big Data, por exemplo, está entrelaçada com a fabricação da Big Deviance – a “explosão da política de crime punitivo” sem precedentes”_ (MURAKAWA apud BENJAMIN, 2020, p. 17). Ao invés de tecnologias que reforçam um modelo de sociedade construída sob o princípio hobbesiano do medo e da segurança, os coletivos e movimentos antiracistas reivindicam e praticam a criação permanente de tecnologias de pertencimento e de produção de confiança; tecnologias relacionais e de _aquilombamento_ (NASCIMENTO, 2006). **Quando a prática tecnológica transgride os limites das normatividades inscritas no código técnico (modelo de eficiência e controle, regime de verdade, hegemonia cultural, aceleração etc.), deparamo-nos com a possibilidade de abertura para outras cosmotécnicas.**
> (p. 365)

# Pós-automação em Smith e Fressoli

> Adrian Smith e Mariano Fressoli cunham o conceito de Post-Automação para abarcar uma diversidade de experiências tecnopolíticas que interrogam o “_futuro essencializado da automação_”. A noção de pós-automação propõe narrativas alternativas aos discursos do tecnosolucionismo aceleracionista de esquerda e aos discursos tecnoliberais de automação inexorável da 4a Revolução Industrial:
> 
> > Automation is propelled by relations imposed as universal and pursuant to abstract system efficiency, capital accumulation, managerial control, labour productivity, material abundance, and technology acceleration. These ends are the foundations for automation. Faced with sustainability challenges, automation redoubles its control strategies by extending them further and deeper into struggling ecological and social systems. **In post-automation, hierarchical control is reduced, and technology affordances are reconceived and reoriented instead towards more creative, collaborative and caring relationships** (SMITH; FRESSOLI, 2021, p.13).
> 
> (p. 366-7)

# Automação e raça

> Quando as lutas tecnopolíticas articulam-se a uma perspectiva cosmotécnica, podemos visualizar uma nova conflitualidade. O acúmulo dos aprendizados históricos aponta para a insuficiência das críticas tecnológicas que foram incapazes de transcender a cosmovisão das sociedades modernas ocidentais, seu horizonte cultural, valores e episteme. Como analisam Neda Atanasoski e Kolandi Vora em _Surrogate Humanity_ (2019), a própria ideia de substituição do trabalho humano (na robótica e na inteligência artificial) nas _“tarefas que consideramos enfadonhas, sujas, perigosas e repetitivas” é uma fantasia que manifesta uma “estrutura racializada, que delimita como o valor é atribuído ao uso de várias tecnologias”_ (2019).
> (p. 367)

# Desenvolvimento tecnológico e gênero

> A reflexão feminista sobre as tecnologias, desde os anos 1980, aponta para problemas análogos ao descrever como o imaginário tecnocientífico é atravessado por valores patriarcais e coloniais. A relação entre o desenvolvimento tecnocientífico, a corrida armamentista e a intensificação dos modelos agro-extrativistas é a vertente mais visível dessa articulação. Haraway, em seu _Manifesto Ciborgue_ (1985), analisa como os divisores que demarcam as fronteiras entre o humano e o animal, o humano e as máquinas, o mundo físico e o mundo não físico constituem um imaginário e relações de poder que se beneficiam das assimetrias construídas no interior dessas dicotomias, garantindo a perpetuação de formas de dominação inscritas no desenvolvimento tecnológico. **Sua reivindicação de uma ontologia ciborgue questiona os modelos ciberufanistas de fusão humano-máquina e aponta para uma reestruturação das relações entre natureza e cultura, na ciência e na política. O tensionamento das oposições modernas encontrará ressonância, nas décadas seguintes, em distintas abordagens teóricas que partilham de uma ontologia relacional que fundamenta as alianças multiespécies na produção da vida no planeta** (HARAWAY; 2021; HARAWAY et al., 2016; TSING, 2015).
> (p. 368)

# Crítica feminista à episteme tecnoliberal

> Além das críticas dirigidas à construção sexista das tecnologias e à maneira como sua aplicação no mundo reforça as desigualdades de gênero, reproduzindo o sistema de dominação patriarcal no mundo do trabalho, na vida pública e nos arranjos da domesticidade, tecnoativistas e investigadoras feministas das tecnologias avançam no enfrentamento dos fundamentos epistêmicos e políticos que organizam a criação tecnológica (OLIVEIRA; ARAUJO; KANASHIRO, 2020; NATANSOHN; REIS, 2020). Partindo de uma perspectiva relacional, algumas críticas feministas questionam as concepções liberais que informam o pensamento tecnológico. As ficções de sujeito autônomo, liberdade, racionalidade e privacidade, por exemplo, são constitutivas do imaginário tecnoliberal e participam da maneira como a relação e as fronteiras humano-máquina e humano-natureza são desenhadas tecnologicamente (HARAWAY, 1985; 1995).
> (p. 368)

# Surgimento de mulheres criptoativistas e a mudança de pautas da esfera individual para a coletiva

> Até muito recentemente, as comunidades ciberativistas da segurança da informação (criptoativistas) eram dominadas quase que exclusivamente por homens brancos. A segurança digital sempre fora concebida de uma perspectiva individual: como faço para proteger minhas informações, meu dispositivo, minha comunicação? As tecnoativistas feministas modificam radicalmente esse pensamento quando introduzem a relacionalidade como paradigma, transformando as questões sobre segurança e privacidade em problemas coletivos (HACHÉ; CRUELLS; BOSCH, 2021). É uma transformação epistêmica e política análoga à passagem de um paradigma individualista da saúde para as concepções de saúde coletiva ou social. Varon e Peña (2021) analisam de forma precisa, por exemplo, as tensões políticas e jurídicas sobre as concepções de privacidade e consentimento nos debates sobre a regulação da proteção de dados no mundo digital:
> 
> Para ela [ Julie Cohen], entender a privacidade simplesmente como um direito individual é um erro: “A capacidade de ter, manter e administrar a privacidade depende muito das características do ambiente social, material e informativo da pessoa” (2013). Dessa forma, a privacidade não é uma coisa ou um direito abstrato, mas uma condição ambiental que permite que sujeitos situados naveguem por matrizes culturais e sociais preexistentes. […] Assim, para Cohen, proteger a privacidade eficazmente requer a firme vontade de se afastar de forma mais decisiva de estruturas centradas no sujeito em favor de estruturas centradas em condições (COHEN, 2013, 2018 apud VARON; PENÃ, 2021, p. 230-231).
> 
> (p. 369-70)

> Muitas iniciativas feministas apontam para tecnopolíticas de produção do Comum, tecnologias de pertencimento, cuidados coletivos, economia de suporte, saúde coletiva, alianças multiespécies, um mundo feito de pessoas e entidades inacabadas, incompletas, vulneráveis, interdependentes e por isso vivas!
> 
> > La relacionalidad radical surge en este tipo de espacio político como el mejor antídoto a la metafísica de la separación y el aislamiento y a las ontologías de la antinegritud, la colonialidad, los órdenes sociales patriarcales y la devastación de la Tierra. La relacionalidad radical es una respuesta al imperativo de que «retejer comunidad a partir de los fragmentos existentes [es] entonces la consigna» (SEGATO, 2016, p. 27 apud ESCOBAR, 2020, p. 338).
> 
> (p. 370-1)

# Transformação da cidade pós informatização

> As lutas urbanas são outro campo relevante das disputas tecnopolíticas. Desde o final dos anos 1970, estudos na sociologia, geografia e urbanismo descrevem as metamorfoses no espaço urbano provocadas pela introdução da microeletrônica no mundo do trabalho industrial e pela informatização das atividades do comércio e serviços. As transformações na indústria, as novas possibilidades de organização das cadeias produtivas e de distribuição tornadas possíveis pelos sistemas informatizados de logística, os impactos das novas formas de organização da informação e do exercício do poder gerencial, as novas espacialidades provocadas pelas infraestruturas de comunicação (novas formas de transmissão de dados), todas terão profundos impactos no funcionamento e no desenho das cidades. São fenômenos diversos que foram caracterizados sob distintas nomenclaturas (sociedade pós-industrial; sociedade da informação; cidades informacionais; pós-fordismo).
> (p. 371)

> A partir de meados dos anos 90, as formas de especulação e financeirização das terras urbanas se intensifica, provocando também a aceleração de dinâmicas de expropriação e espoliação urbana para a criação de novos mercados imobiliários (ROLNIK, 2019). A convergência entre a crescente digitalização e a economia financeira no século XXI adensa e acelera as relações entre a produção física do espaço urbano e os fluxos globais de capital financeiro (SASSEN, 2017; 2012). Plataformização13, espoliação e rentismo, extrativismo neocolonial são alguns dos fenômenos emergentes investigados. Quando qualquer espaço físico (privado-doméstico ou público) é passível de ser convertido num ativo que pode ser explorado economicamente; quando os territórios urbanos e a própria experiência urbana podem ser codificados, mensurados e transformados em valor pelas corporações transnacionais de tecnologia da informação, amplia-se a fronteira da mercantilização e financeirização da vida urbana
> (p. 371-2)

# Smart cities

> **Os projetos de cidades inteligentes (_smart cities_), gestados há quase duas décadas, são outro exemplo desse processo de hibridização econômica tecnoliberal do espaço informacional com o espaço físico e a vida urbana**. A noção de cidades inteligentes refere-se genericamente à aplicação de tecnologias de informação e comunicação digital no próprio tecido infraestrutural das cidades e à crescente informatização e adoção de softwares voltados à gestão das cidades. **Parte-se do princípio de que a inserção de tecnologias digitais e sensores no espaço e equipamentos públicos amplia a produção e coleta de informações estratégicas que podem contribuir para uma melhor gestão das cidades.** Na prática, a expressão “cidades inteligentes” refere-se tanto à adoção de pacotes tecnológicos específicos para a produção, coleta e análise de dados sobre aspectos selecionados da vida urbana (como as iniciativas do Sistema Integrado de Comando e Controle implantados pelo governo brasileiro para as cidades que sediaram os eventos da Copa de 2014 e Olimpíadas de 2016 (cf. CARDOSO, 2018)); mas também para projetos urbanísticos de construção de novas infraestruturas urbanas infiltradas por dispositivos informacionais e até projetos de cidades inteiramente construídas sob essa utopia gerencial (BRANCO, 2019).
> (p. 372-3)

> Nessas iniciativas, a elaboração dos projetos e o protagonismo na implementação é claramente definido pelas grandes corporações de tecnologias da informação (inicialmente da área de hardware, como IBM e CISCO) que passam a oferecer produtos e serviços para a gestão pública em diferentes níveis governamentais (municipal, estadual e federal). Os discursos e imaginários elaborados em torno das cidades inteligentes fundamentam-se em modelos de eficiência gerencial, orientados por normatividades e racionalidades do mundo corporativo-privado (BRANCO, 2019). Além da colonização corporativa da gestão pública e da tomada geopolítica do espaço por grandes empresas transnacionais, erodindo os princípios públicos que fundam a urbanidade moderna, a colonialidade das cidades inteligentes também se manifesta na expansão securitária, vigilantista e de controle policial sobre a vida nas metrópoles, reforçando ainda mais as estruturas de racialização e criminalização, que agora podem ser exercidas preventivamente, graças à integração de grandes bancos de dados, à análise algorítmica, à rastreabilidade permanente e à perfilização dos cidadãos (FIRMINO, 2018; CARDOSO, 2018).
> (p. 373-4)

# Plataformização

> 13: A noção de plataformização aqui utilizada descreve um conjunto heterogêneo de fenômenos relacionados à hiperconcentração da cibermediação (trabalho, comércio, acesso à cultura e conhecimento, interação social e efetiva etc.) em infraestruturas tecnológicas digitais que se caracterizam pela capacidade de interoperabilidade entre diferentes aplicativos e dispositivos; possuindo elevado poder econômico para rapidamente produzir efeito de economia de rede; com modelo centralizado e fechado de controle sobre sua operação e funcionamento dos seus algoritmos, da gestão e análise dos dados coletados e produzidos; com presença ubíqua em seu nicho de atuação, tornando-se praticamente um ponto obrigatório de passagem para a realização da atividade/serviço mediado. Nesse sentido, quando nos referimos à plataformização, indicamos um amplo fenômeno sociotécnico de dominância de infraestruturas digitais corporativas, cujo modo econômico apoia-se na crescente digitalização, extração e produção de valor sobre as interações mediadas pela dataficação e algoritmos.
> (p. 372)

# Pertinência do termo cidade

> Devemos nos perguntar se a palavra “cidade”, em sua acepção moderna, ainda é apropriada para descrever o tipo de experiência social que está sendo produzida por essa tecnicização cibernética do urbano.
> (p. 374)

# Disputas na cidade, conflitos dividuais e ontologia relacional

> Assim, as lutas pelo direito à não identificação e à não rastreabilidade reclamam um direito comum (a experiência de não ser identificado permanentemente) que deve ser protegido e sustentado. Novamente, trata-se de um tensionamento sobre os limites das reivindicações centradas em direitos individuais, pois sua materialização tecnomediada dá existência a uma dimensão relacional que excede o paradigma liberal na definição do sujeito/ indivíduo de direitos. São as informações e saberes produzidos aquém do indivíduo (infra-individual) e além do indivíduo (supra-individual) que adquirem nova relevância, sem suprimir propriamente o indivíduo jurídico-moderno, mas criando novas tensões e um outro modo de subjetivação, analisado por Fernanda Bruno e Pablo Manolo Rodrigues em termos de uma dividuação (BRUNO; RODRIGUÉS, 2021). Transbordamos, enfim, para conflitos de ordem dividual, nos quais o “entre” surge como território relacional sob disputa (PARRA, 2018). Reconhecemos, nessas dissensões, exemplos da emergência política de uma _ontologia relacional._
> (p. 376)

# Tecnocidadania

> 16: Tecnocidadania foi o termo utilizado por Antonio Lafuente para descrever a condição de interdependência sociotécnica para a efetivação dos direitos de cidadania na vida contemporânea. Lutar por direitos exige cada vez mais o conhecimento sobre o funcionamento dos aparatos tecnológicos, a indissociabilidade entre as dimensões culturais, técnicas e ambientais na vida cotidiana (LAFUENTE, 2007).
> (p. 376)

# Resistências às cidades inteligentes

> A tecnoutopia liberal das _cidades inteligentes_, portanto, tem provocado reações contra os modelos de gestão corporativa e o controle cibernético da vida urbana. **Além das iniciativas de tecnocidadãos, que atuam na confluência da ciência cidadã, do tecnoativismo e da mobilização comunitária, há uma outra camada de lutas tecnopolíticas que abordam as condições de autogestão sobre as diversas camadas e processos tecnológicos envolvidos na mediação digital-cibernética.** A produção, coleta, análise e armazenamento de dados e informações digitais sobre os cidadãos e sobre a vida nas cidades é um recurso altamente estratégico para o mercado e para o estado. Na medida em que muitos governos contratam empresas privadas para a oferta de tecnologias digitais para a implementação de serviços ou para a gestão pública (sofwares, hardwares, infraestruturas, serviços de nuvem etc.), **abre-se um enorme campo de exploração econômica e política sobre as possíveis utilizações das informações produzidas e coletadas.**
> (p. 376-7)

> Rodrigo Firmino, Débora Pio e Gilberto Vieira nos apresentam experiências relevantes de lutas urbanas em comunidades vulnerabilizadas pelo estado, que atuam na confluência do ativismo de dados (_data justice_), da ciência cidadã e do urbanismo insurgente (2020). Iniciativas que, através de uma atuação implicada com os problemas locais, inventam formas de produção de dados e evidências contra-hegemônicas, promovendo ações coletivas e de incidência nas políticas públicas19. Como tornar visíveis problemas locais que muitas vezes são invisíveis ou invisibilizados pelo estado? Como questionar o regime de verdade dataficadocibernético mobilizado pelo estado para legitimar ações violentas nesses bairros20? Há uma luta epistêmica e política para produzir um conhecimento situado, corporificado e territorializado, tanto para confrontar a arbitrariedade do estado nesses territórios, como para dar visibilidade e sustentação a outras formas de vida na cidade21.
> (p. 377-8)

# Soberania digital ~ soberania alimentar

> Num sentido análogo às reivindicações de soberania alimentar elaboradas por movimentos camponeses de luta pelo direito à terra e à alimentação saudável, as lutas pela soberania digital indicam a importância de se ampliarem as condições de autogoverno sobre a produção, coleta e análise das informações digitais. O sentido da “soberania” proposto nessas iniciativas22 é distinto das conotações do “poder soberano” ou da “soberania nacional”, **referindo-se em especial à capacidade de autogoverno de um coletivo humano sobre os recursos necessários para a realização de uma determinada atividade (comunicação, alimentação etc.) sob condições e normatividades localmente determinadas.** Em alguns casos, isso pode significar a criação de estratégias de interdependência, alianças extralocais, internacionais, e entre humanos e não humanos. Portanto, uma perspectiva distinta da concepção jurídico-política moderna de soberania.
> (p. 378)

# Sobre a cidadania digital

> É nesse sentido que algumas cidades e países começam a implementar políticas de cidadania digital, objetivando ampliar o controle democrático sobre a crescente digitalização da vida nas cidades23. Como, para quais fins e sob que condições, informações sobre cidadãos podem ser produzidas, coletadas e analisadas é um problema central para o presente e o futuro das democracias. **Além da extração de valor, passível de ser obtida a partir dessas informações no mercado de dados (comercial ou político), é a própria história – passado, presente e futuro – da vida ciberermediada que está em jogo.**
> (p. 379)

# Materialidade dos processos digitais

> A despeito da aparente imaterialidade dos processos digitais, toda informação digital depende de uma longa cadeia sociotécnica que envolve muita matéria, energia, corpos e uso de territórios24. A extração de minérios para a produção de artefatos, a geração energética, transmissão, armazenamento e processamento de dados digitais produz, frequentemente, relações de exploração violentas sobre populações e territórios afetados por esses processos25. **A digitalização, nesse sentido, além de propagadora de modos de vida específicos, quando submetida à hegemonia cibernética, tem intensificado as dinâmicas extrativistas em dois sentidos: (a) a extração de informações a partir da produção, coleta, armezamento e análise de dados de populações e territórios, e sua posterior conversão em valor monetário e poder econômico e político; (b) a conversão da natureza em recurso através da extração de água, minerais e energia para a produção e funcionamento dos arranjos tecnológicos.**
> (p. 380)

# Geopolíticas coloniais e extrativistas envolvendo as tecnologias digitais

> Considerando a desigualdade econômica e a assimetria do poder tecnológico e computacional entre os estados nacionais, mas também entre as corporações privadas e os estados, pesquisadores apontam para um aprofundamento de assimetrias históricas. **Colonialismo de dados (COULDRY; MEJIAS, 2019) colonialidade da dataficação (RICAURTE, 2019), extrativismo informacional/digital/dados (CRAWFORD, 2021) são algumas das noções mobilizadas atualmente para descrever como as grandes corporações de tecnologia de informação atuam geopoliticamente: extração e transferência de informações sobre a totalidade da vida cibermedia; gestão e apropriação do conhecimento tecnocientífico necessário para a inovação tecnológica; extração do Comum (cooperação, inteligência, vida) de territórios específicos.**
> (p. 380-1)

# Cosmopolítica como conflitualidade ante à tripla crise: epistêmica, política e socioambiental

> Diante da tripla crise que enfrentamos – epistêmica (mutações no regime de verdade), política (erosão das instituições democráticas) e socioambiental (mudança climática) – acompanhamos a hipótese segundo a qual algumas lutas tecnopolíticas podem inaugurar/atualizar conflitualidades de ordem cosmopolítica. **Trata-se de uma investigação sobre a fabricação de mundos e suas tecnologias; lutas contra a hegemonia cibernética e seu regime de verdade algorítmico; a defesa e criação de coletividades políticas contra a conversão do vivo em recurso, propagada pelas formas de abstração digital e pela relação extrativista com a Terra e seus entes.**
> (p. 381)

# Tecnosolucionismo, tranhumanismo e a desfuturização

> Enquanto o primeiro [[ tecnosolucionismo ]] insiste na proposição de que a tecnociência sempre poderá oferecer soluções técnicas e eficientes para os problemas enfrentados no mundo, a outra vertente [[ ciberfuturismo, transhumanismo ]] aponta para uma necessária superação do corpo humano e da vida terrana. Nosso argumento, ao contrário, afirma que o domínio da tecnociência moderna e antropocêntrica e o consequente extermínio de outras formas de vida e epistemes são parte da equação que produz o problema. Como afirma Tony Fry:
> 
> > uno de los efectos más graves de la modernidad es lo que llama ‘desfuturización’, la destrucción sistemática de futuros posibles por la insostenibilidad estructurada y estructurante de la modernidad. La ‘futurización’, en cambio, pretende transmitir lo contrario: un futuro con futuros” (FRY apud ESCOBAR, 2016 p. 137).
> 
> (p. 383)

# Pela necessidade de uma virada ontoepistemopolítica

> Ao reconhecer a pandemia de Covid-19 como um acontecimento do Antropoceno, insistimos na urgência de uma virada ontoepistemopolítica capaz de cartografar novas conflitualidades e alternativas sociotécnicas. O transbordamento de lutas tecnopolíticas em direção à afirmação de uma diversidade cosmotécnica inspira a emergência de uma nova sensibilidade cosmopolítica, na qual a concretização de outros futuros tecnológicos é indissociável do reconhecimento e fundação de outras coletividades e instituições políticas; portanto, um movimento radical de invenção democrática.
> (p. 383)
