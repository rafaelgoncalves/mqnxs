---
title: Método em Garcia dos Santos (1981)
tags:
    - Laymert Garcia dos Santos
    - Gilbert Simondon
    - Karl Marx
    - ficção científica
    - teoria crítica
    - tecnologia
    - sociologia da tecnologia
    - método
    - fichamento

path: "/metodo-garcia-dos-santos"
date: 2024-04-27
category: fichamento
featuredImage: "desregulagens.png"
srcInfo: Capa de _Desregulagens_
published: true
---

Fichamento do capítulo "Questão de método" da tese _Desregulagens_[^1] de Laymert Garcia dos Santos.

[^1]: Santos, Laymert Garcia dos. 1981. _Desregularens: educação, planejamento e tecnologia como ferramenta social._ Campinas: Brasiliense, Fundação de Desenvolvimento da Unicamp.

# Método como racionalização a posteriori

> Um sociólogo francês {Nicolas Herpin, _Les Sociologues américains et le siècle_} já escreveu que em sua opinião o método não passa de uma codificação a posteriori da démarche da pesquisa. Eu diria que, em muitos trabalhos sociológicos, a questão do método freqúentemente assume o caráter de uma racionalização, no sentido freudiano do termo. Pois, quer a démarche seja empírica, parta do concreto ou abstrato ou, ao contrário, quer proceda da aplicação siste mática de um esquema de interpretação sobre um determinado objeto, freqúentemente considera-se a questão da metodologia como uma prova.
> (p. 10)

> Por isso, talvez, os capítulos metodológicos nunca tratam da trajetória de uma pes quisa, excluindo o que é realmente o motor do trabalho, isto é, a inquietação e a dificuldade que se apresentam ao sujeito de ter de pensar a complexidade dos fenômenos sociais. Talvez por isso também nos entregam um quebra-cabeça realizado, onde conceitos teóricos e fatos empíricos se encaixam maravilhosamente.
> (p. 10-1)

> Então, em vez de “esquecer” ou de postular que a perplexidade e as dificuldades teóricas e metodológicas já foram superadas, tentei fazer delas o eixo deste capítulo. E, realmente, foi a partir daí que começou (e não acabou) todo o trabalho de escritura deste livro.
> (p. 11)

# Escolha do objeto: crítica ideológica do projeto SACI

(p. 11)

# (1) Realidade problemática do objeto empírico em relação a imagem inicialmente construída

(p. 12)

# (2) Limites da crítica ideológica marxista

> Segundo ponto. A dúvida começava a se instalar quanto ao alcance de uma crítica ideológica do projeto. A leitura de inúmeros autores que perseguem em toda parte, salvo em suas próprias fileiras ou neles mesmos, as marcas da “ideologia dominante” mostrava-me cada vez mais o peso de uma impotência: a impossibilidade de fazer qualquer coisa que não fosse contrapor um esquema de explicação a outro, tão globalizante quanto o anterior. Para se dar conta do fenô meno, bastaria citar esses livros que se limitam a repetir, empobre- cendo-o o pensamento do Marx da Ideologia Alemã, do Lênin de O imperialismo, fase suprema do capitalismo, de Theodor Adorno ou de Louis Altnusser...
> (p. 12)

> Veio-me pois a impressão de que, no esquema de interpretação em que me situava, a dialética tornara-se um tique e que as contradi ções simplesmente haviam desaparecido! Além disso, certos instru mentos teóricos de que dispunha para conduzir meu trabalho come çavam estranhamente a se assemelhar aos de meus adversários. Jurgen Habermas3 mostrara-me que a teoria do capitalismo monopolista de Estado acaba encontrando-se com as teorias ocidentais da tecnocracia e da elite por duas razões: por um lado, ela supõe que o aparelho de Estado seria capaz de planejar ativamente, de elaborar uma estratégia económica central e de impô-la — o que desconhece os limites do planejamento administrativo, sua incapacidade para apagar as contra dições entre os interesses dos capitalistas individuais, entre os inte resses individuais e o interesse geral dos capitalistas, entre os interesses específicos do sistema e os interesses universalizáveis, sem falar da invasão do Estado por seus clientes; por outro lado, a teoria do capi talismo monopolista de Estado superestima a importância dos contatos pessoais e da uniformização da ação, sem se dar conta de que a análise do recrutamento, da composição e da interação entre as diferentes elites do poder não basta para explicar as relações entre sistema admi nistrativo e sistema económico. Uma ilustração do que acabo de dizer pode ser encontrada no livro de Ralph Miliband, O Estado na socie dade capitalista.
> (p. 13)

# Reducionismo da dimensão propriamente sociológica

> De tanto querer reduzir todas as instituições (Exército, Admi nistração, Igreja, Justiça, Parlamento) a um denominador comum — o caráter de classe dos indivíduos do topo, sua submissão aos interesses privados, sua ideologia comum, etc. — suprime-se tudo o que cons tituía a especificidade delas.
> (p. 13)

> Finalmente, desmascarar os desígnios reais da classe dominante num projeto de teleducação tão amplo exigia que o pesquisador iden tificasse sua estratégia. Ora, acontece que uma determinada concepção da noção de estratégia parecia ser partilhada tanto pelos teóricos “bur gueses” como por seus detratores “materialistas”. Como afirma Cor- nelius Castoriadis num ensaio brilhante, toda a literatura económica contemporânea apóia-se implicitamente numa psicologia do compor tamento individual expresso pelo famoso “princípio de minimax”, se gundo o qual o indivíduo só age sabendo perfeitamente o que quer e como obtê-lo, e só quer uma coisa, maximizar seu ganho com o mínimo de esforço. Para Castoriadis, isso “permite que a economia contempo rânea fale sem saber a prosa do hegelianismo mais ingenuamente absoluto ou mais absolutamente ingênuo pois tudo o que ela diz, por exemplo, sobre o papel e o comportamento do Estado, os conselhos que lhe dá, as regras de comportamento que lhe determina, postulam que o Estado é exdusivamente uma instância de racionahdade pura e a realidade da idéia moral. O que, aliás, não é um privilégio da economia acadêmica; também para o marxismo corrente, a integração entre a teoria do Estado como instância do poder de classe dominante e a ana- lise de seu papel económico nunca se realizou. O mesmo ocorre com a empresa e o sindicato. Em todos esses casos, ignora-se quase total mente tanto a função sociológica das instituições quanto sua consti tuição burocrática, além da irracionalidade que lhes é assim incorpo rada em sua própria construção”
> (p. 13-4)

> O "princípio de minimax", o “comportamento racional é, como se sabe, a noção central em que se fundamentam a teoria dos jogos, o homo strategicus e as extrapolações que ambos conheceram no campo das ciências sociais. **A estratégia, no caso, é a idéia do inventário exaustivo das possibilidades, graças à identificação do conjunto de futuros possíveis, é a expressão da vontade de controle do político e da história, é a base de todos os estudos prospectivos, de todas as simulações tão a gosto dos think tanks e dos estrategistas americanos que, como Hermann Kahn e os tecnocratas da Rand Corporation, querem “pensar o impensável”.** Analisando a teoria dos jogos, Michel Plon explica por que a noção de estratégia assim entendida **acarreta uma política imaginária, produz “uma dissolução da política como instância da luta de classes dentro da problemática de uma estratégia calculada”, por que, enfim, as relações políticas são reduzidas a relações interpessoais onde duas consciências simétricas se enfrentam.**
> (p. 14)

# Da impossibilidade da crítica através do uso das mesmas categorias do que se é criticado

> Se houvesse algo a explicar, esse algo seria: **por que os planeja dores procuram estratégias globalizantes a qualquer preço? No en tanto, os críticos de esquerda não se colocam tal pergunta e preferem considerar como uma realidade o que não passa de vontade de controle e de eficiência dos planejadores.** Obviamente, não se trata de negar, aqm, a importância social do planejamento implantado pelo Estado ou as incidências sócio-economicas e políticas da atividade dos grandes grupos monopolistas; simplesmente de saber que **não se pode pensar os problemas com as mesmas categorias do Estado e dos grandes grupos sem aderir à lógica que os anima.**
> (p. 14)

# Estratégia ideológica e política em Que fazer? (Lenin)

> Aliás, o problema é muito mais complexo do que se pensa habi tualmente. Bastaria lembrar a leitura finíssima de Pierre Ansart a res peito do Que fazer? de Lenin, onde o autor assinala a passagem da estratégia ideológica à estratégia política, passagem quase impercep- tível mas de graves consequências. **A transição se opera através de uma série de deslocamentos que acabarão favorecendo os meios em detri mento dos fins. Nesse momento, se antes o fundamento da promoção do líder era a correção de suas interpretações, agora o critério será a eficiência; se antes a verdade se encontrava na classe encarregada de realizar sua própria libertação, agora se encontrará no partido revo lucionário. A ideologia instrumentaliza-se e essa instrumentalização reativa a disciplina, a obediência, a autoridade, o controle, a devoção.**  “Chegamos, assim, a duas situações típico-ideais da relação entre a ideologia e o poder estabelecido” — escreve o sociólogo.7 — “Essas duas situações sócio-históricas impõem duas abordagens da ideologia e exigem a diferenciação de sua explicação e compreensão. No caso da estratégia ideológica determinamos no máximo a criatividade ideoló gica e o processo de consagração em que os fatos simbólicos intervêm para modificar e criar redes práticas de comunicação social; no caso da estratégia política temos de assinalar essencialmente a relação inversa, em que o poder estabelecido adapta a ideologia ao seu exercício, trans forma as significações em função da evolução de sua estratégia.” (...) “Assim, a obra de Lenin Que Fazer? apareceu-nos como um exemplo típico da estratégia ideológica, exemplo de inovação discursiva em rup- tura total com a ideologia oficial e o sistema de poder estabelecido. Se, ao contrário, o consideramos segundo a organização lógica de suas significações, encontramos um modelo estratégico que precisamente tende a legitimar uma organização excepcionalmente disciplinada onde as relações entre a nova instituição e a ideologia revestiriam um caráter de ortodoxia. Enquanto o modelo responde a todos os critérios de distanciamento e de ruptura que observamos para caracterizar a estra tégia ideológica, o modelo proposto anuncia uma organização de tipo centralista onde a ideologia tornar-se-ia um dos meios da ação política eficaz.”
> (p. 15)

> (7) Pierre Ansart, “Idéologie stratégique et stratégie politique”, Cahiers Interna tionaux de Sociologie, pp. 241 e 242.
> (p. 15)

# (3) Separação sujeito-objeto

> Terceiro ponto de perturbação. Começo a perceber a acuidade do problema da relação entre o sujeito e seu objeto de estudo, **a impossível existência de uma entidade inteiramente isolável do sujeito que quer conhecer;** em suma, começo a entrever essa trama obscura e enigmática onde **um certo sujeito constrói um certo objeto de análise a partir de um quadro teórico e de fatos observáveis, tenta situar os pontos cardeais, os núcleos, mas tenta também se situar, situar o movimento de seu pensamento.**
> (p. 15-6)

# Objeto redefinido: encruzilhada sociotécnica

> Uma encruzilhada onde imperava a procura do que chamam de racionalidade, onde celebrava-se o culto da produtividade, da eficiência, da ciência e dos dois outros deuses positivistas já inscritos na bandeira nacional: Ordem e Progresso.
> (p. 16)

> Impunha-se, com intensidade crescente, que esse era o fulcro do projeto, que ali estava o ponto a ser estudado. **Não como uma ideo logia, evidentemente “dominante”, que justificaria e tentaria legitimar o SACI mas como uma problemática que convocava recursos, saberes e poderes para capturar o real e forçá-lo a entrar nas malhas de aço de um modelo.** Era preciso pensar o projeto como **uma ambição de controle e de eliminação do desconhecido, graças ao emprego da tecnologia e da teoria de sistemas.** Era preciso questionar uma certa “lógica da escolha dos meios”, mostrar que essa racionalidade instrumental, isto é, a combinação ideal dos meios a serviço de fins exteriores ao sistema, **corporificava as finalidades que ele pretendia servir e que os fins estavam inscritos na própria materialidade, na natureza, na orga nização dos meios.** Em suma, fins e meios formavam um único tecido: **a teia da produção e reprodução do sistema social**. Era preciso refletir sobre tudo isso; com que palavras?
> (p. 16)

# Captar a especificidade do objeto técnico (SACI) em relação ao plano social

> Como se verá mais tarde, no projeto SACI a tecnologia tem valor de ortopedia. Portanto, **a primeira tarefa consistia em captar a espe cificidade do objeto técnico, seu estatuto, a maneira pela qual ele entra em relação com a evolução social.** O questionamento deveria se concen trar nas perguntas que eram simultaneamente o ponto de partida e o núcleo do seminário de Dominique Wolton sobre a informática: **como colocar o problema das relações entre mudança social e mudança téc nica? Haveria questões específicas criadas pela tecnologia? Haveria uma racionalidade do projeto técnico que fosse coextensiva ao sistema social? Como as ciências sociais tentam estabelecer uma coerência, um continuum, entre objeto técnico e sociedade?**
> (p. 16-7)

# Técnica e sociedade em Marx (O capital)

> Foi Marx, particularmente no capítulo XV do livro I d’O Capital, quem inaugurou o discurso sobre a relação entre técnica e sociedade, **fundando a teoria que traz ao conhecimento a evolução histórica das sociedades através de seu modo de produção, isto é, através das relações entre as forças produtivas e as relações sociais de produção.** Uma teoria onde, sob o comando do capital, a divisão do trabalho e a máquina encontram-se na base da Revolução Industrial, onde o desenvol vimento técnico acarreta a subversão da organização e das condições de trabalho, a transformação dos ofícios e da vida cotidiana, a espoliação do trabalhador, etc.
> (p. 17)

# Tecnologia como produto das relações sociais em Marx/Braverman

> Entretanto, como assinala Harry Braverman num livro sobre a degradação do trabalho no século XX {Trabalho e capital monopolista}, “Marx conferiu, de fato, **posição primacial aos _meios de produção_ na evolução social.** Mas isto jamais foi concebido como um determinismo simples e unilateral que causa um modo específico de produção, emergente automaticamente de uma tecnologia específica”. Dentro dos limites históricos e analíticos do capitalismo, de acordo com a análise de Marx, **a tecnologia em vez de simplesmente _produzir_ relações sociais é _produzida_ pelas relações sociais representadas pelo capital.** (...) Desta perspectiva, o primeiro volume de O Capital pode ser considerado um ensaio maciço sobre **como a mercadoria se constitui, em um apropriado quadro social e tecnológico, como amadurece na forma de capital e como a forma social de capital, levado a incessante acumulação como condição de sua própria existência, _transforma completamente a tecnologia._**
> (p. 17)

# Determinismo tecnológico x construção social da máquina em Marx

> Aí está, portanto, um problema grave, o da ambiguidade da análise marxista com relação à técnica. **Entre o social e o técnico, quem depende de quem? Não seriam eles indissoluvelmente ligados? Quando se aceita que no mundo moderno o desenvolvimento social depende do desenvolvimento técnico explode-se o paradoxo contido na concepção materialista da história**, escreve C. Castoriadis: “pois isso significaria que o desenvolvimento do mundo moderno depende do desenvolvimento de seu saber, e que portanto são as idéias que fazem a história progredir, com a única restrição de que essas idéias pertencem a uma categoria particular (idéias técnico-científicas)”
> (p. 18)

# Técnica destacada do social em análises teóricas marxistas

> Se capitalistas e tecnocratas reificam a tecnologia, se são fascinados pela técnica a ponto de colocá-la acima das clas ses sociais — nada mais compreensível, eles apenas exercem um direito adquirido, pois a história de seu triunfo e de sua legitimidade encontra-se inextricavelmente ligada ao desenvolvimento técnico e científico.  A questão se complica, provoca perplexidade, quando se constata que a crítica marxista também se instala alegremente nesse terreno.
> (p. 18)

> Mas, apesar de tudo, a t~ecnica conserva uma imagem positiva. Do ponto e vista teórico, o fato de os marxistas privilegiarem o estudo da produção de mais-valia absoluta, de limitarem a análise da mais-valia relativa às aplicações do taylorismo na indústria, ou ainda de só considerarem como produtivo o trabalho "material" (no sentido físico da palavra) enquanto o trabalho de um cientista ou engenheiro é tido por improdutivo — tudo isso parece ter contribuído para reforçar uma corrente que preservava a técnica e a desconectava de sua existência social.
> (p. 18-9)

> (15) Sobre o assunto, ver a observação de Charles Bettelheim em M. Janco e D. Furjot, Informatique et capitalisme, p. 70 e sua discussão em J. lon, A. Lefebvre, B. Miege,,^Peron, Capitalisme et industries culturelles, p. 181
> (p. 19)

# Uso não crítico da tecnologia na prática socialista

> Do ponto de vista político a questão era bem mais importante. Pois nin guém pode ignorar que os trabalhadores soviéticos carregam todos os estigmas das classes trabalhadoras ocidentais,16 que a organização do trabalho nos países socialistas retomou o essencial dos métodos capitalistas (chegando até a refiná-lo com a monstruosidade do stakhanovismo), que o taylorismo e a “gestão científica” foram promovidos desde o início. **Como poderia ser diferente, se a abordagem da tecno logia era exatamente a mesma que imperava nos países capitalistas?**  Não é impressionante que a vanguarda russa do período revolucionário se reconheça no futurismo italiano e que se possa ler no Manifesto de Dziga Vertov: “temporariamente, não queremos mais filmar o homem porque ele não sabe dirigir seus movimentos. Através da poesia da máquina, iremos do cidadão retardatário ao homem elétrico perfeito.  (...) O homem novo, liberto do embaraço e da falta de jeito, que terá os movimentos precisos e ligeiros da máquina será o tema nobre dos filmes”?
> (p. 19)

> Os sovietes mais a eletricidade... Os sovietes reduzidos mais e mais a uma estrutura formal de “controle operário”, enquanto a ele tricidade e a máquina desembestavam, a todo vapor, seguindo o ritmo regulado pelos planejadores socialistas. **A regra do silêncio torna-se então perfeitamente compreensível quando se trata de discutir uma tecnologia “completamente transformada” pelo capital: repensar o problema levaria inevitavelmente a repensar a organização do trabalho, reconhecer as cadências das linhas de montagem, o embrutecimento do trabalhador, etc., enfim, colocar em questão as relações entre forças produtivas e relações de produção nos países do Leste.**
> (p. 19)

# Análise infraestrutural da técnica em Jean-Pierre Vigier

> Após avaliar o papel da Revolução Científica e Técnica na va riação da taxa de lucro e suas consequências na lei de baixa tendencial, Jean-Pierre Vigier examina o impacto que ela exerce desde 1945 sobre as transformações do Estado, a extensão do mercado capitalista, a organização internacional do capital, a divisão internacional do tra balho e a nova dimensão do imperialismo; e o estudo termina com uma análise da crise atual encarada como a deflagração, pelos Estados Unidos, de uma 3a guerra mundial que se desenrola nos planos econó mico e político. Terminado o livro, **a conclusão a que se chega é a mesma que percorre a obra de Harry Braverman da primeira à última página, isto é: que a técnica é uma das molas mestras da dominação capitalista contemporânea.**
> (p. 20)

# Máquina, maquinaria e automatismo nos Grundrisse (Marx)

> "Integrado no processo de produção do capital, o meio de trabalho conhece entretanto diversas metamorfoses das quais a última é a de _máquina_, ou melhor, um **sistema automático de _maquinaria_** (sistema de maquinaria; o sistema automático é apenas sua forma mais acabada, mais adequada; e só ela transforma a maquinaria em sistema), movido por um autômato; **a força motriz que se move sozinha, autômato consiste num grande número de órgãos mecânicos  e intelectuais, tanto que os póprios operários sâo apenas suas articulações conscientes.** _Na maquina e mais ainda na maquinaria enquanto sistema automático, o meio de trabalho se transforma em função de seu valor de uso, isto é, como existência material, em uma existência adaptada ao capital fixo e ao capital em geral; e a forma sob a qual, enquanto meio de trabalho imediato, ele foi integrado no processo de produção do capital é absorvida por uma forma colocada pelo próprio capital e que lhe é correspondente._ A maquinaria não se apresenta de maneira alguma como meio de trabalho do trabalhador individual.  Sua _differentia specifica_ não consiste nunca, como é o caso dp meio de trabalho, em intervir entre a atividade do trabalhador e o objeto; ao contrário, essa atividade está de tal modo colocada que intervém ape nas no trabalho da máquina, na ação desta sobre a matéria-prima, vigiando e evitando que se escangalhe. (...) **Reduzida a uma mera abstração da atividade, a atividade do operário é determinada e regulada em todos os níveis pelo movimento da maquinaria, e não o contrário.** A ciência que obriga as articulações inanimadas da maquinaria construída para esta finalidade a agirem como um autómato não existe na consciência do operário; **através da máquina, ela (a ciência) age sobre ele como potência da própria máquina.** _A apropriação do tra balho vivo objetivado — do trabalho ou da atividade que realiza valor pelo valor existente em si —, que define a noção de capital, se coloca na produção baseada na maquinaria como característica do próprio processo de produção, mesmo no tocante a seus elementos materiais e a seus movimentos materiais."_
> (p. 20-1)

# Concepção neutra de técnica -> socialismo soviético

> **A concepção de uma técnica neutra, certamente positiva, subju gada por capitalistas exploradores, reduz todos os problemas das forças produtivas a uma questão de propriedade jurídica dos meios de produção. Consequentemente, basta que o aparelho produtivo mude de mãos, que seja nacionalizado, para que a produção capitalista não exista mais.** Claro que a questão da propriedade é capital; mas também é evidente que o controle pelo Estado não muda a relação entre o homem e a organização capitalista do trabalho. Na verdade, é por esta razão que a Oposição Operária entrou em conflito com Lênin e os bolcheviques entre 1918 e 1920. A Oposição Operário dizia: em condições pós-revolucionárias, o desenvolvimento da produção e um pr blema essencialmente social e político, cuja solução depende da inici.  tiva e da criatividade das massas trabalhadoras; portanto, é saber: Quem estabelece as normas? Quem controla e «aciona a obri gação de trabalhar? Sã<? as coletividades organizadas dos, trabalha- dores? Ou uma categoria social específica, cuja função é dirigir o trabalho dos outros? **Sabe-se que a resposta formulada pelos dirigentes do partido concebia a organização da produção como um problema administrativo e técnico, um problema de organização dos mesmos meios, desta vez postos a serviço de uma finalidade histórica diferente: a construção do socialismo. Sabe-se também que tipo de sociedade essa resposta gerou.**
> (p. 21-2)

> Por que conservar os mesmos meios? Há múltiplos fatores que concorreram para isso, mas um deles, observado por C. Castoriadis, pareceu-me particularmente importante: “se aos dirigentes bolchevi ques parece evidente que os únicos meios eficientes são os meios capi talistas, é porque estão convencidos de que o capitalismo é o único sistema de produção eficiente e racional. Fiéis nesse ponto a Marx, eles querem suprimir a propriedade privada, a anarquia do mercado — não a organização da produção realizada pelo capitalismo. Eles querem modificar a economia, não as relações de trabalho e o próprio trabalho.  **No fundo a filosofia deles é a filosofia do desenvolvimento das forças produtivas e aí, também, são os fiéis herdeiros de Marx — pelo menos de uma certa faceta de Marx, que é a faceta dominante nas obras da maturidade.** Se o desenvolvimento das forças produtivas não é a finalidade maxima, é, em todo caso, o meio absoluto, no sentido de que todo o resto deve resultar por acréscimo, e que tudo deve ser subordinado a esse desenvolvimento.
> (p. 22)

# Verdadeiro problema: a técnica como consequência da razão (e, portanto, positiva)

> A contradição que me envolve e que atinge o leitor é real... mas não tenho a estatura teórica necessária para enfrentá-la e tentar elucidá-la a partir dos textos de Marx. No entanto, ela designa o verda deiro problema: **o fascínio exercido pela tecnologia enquanto encarnação de racionalidade, de razão operante, em todo o mundo contem porâneo (seja os que defendem o capitalismo, o socialismo ou uma ter ceira via) e a impossibilidade de pensar o “fato técnico” senão como um fato positivo precisamente porque ele é portador de razão, aquele que a traz em si, mas também que a conduz e que a representa.**
> (p. 23)

# Elemento, indivíduo e conjunto técnicos em Simondon

> Estudando a evolução da realidade técnica, Gilbert Simondon concebe o objeto técnico em três níveis: o elemento, o indivíduo e o conjunto. **O elemento é esse objeto técnico infra-individual semelhante a um órgão de um ser biológico.** “No campo da vida, o órgão não é destacável da espécie; no campo da técnica, o elemento, precisamente porque é fabricado, é destacável do conjunto que o produziu.”25 **Ele é o primeiro termo de uma linhagem específica que por sua vez produzirá um novo órgão. Pois, “para que exista progresso técnico, é preciso que cada época possa legar à época seguinte o fruto de seu esforço técnico; não são os conjuntos técnicos, nem mesmo os indivíduos que podem passar de uma época para outra, mas sim os elementos que tais indivíduos, agrupados em conjunto, puderam produzir”.**
> (p. 23)

> **O indivíduo técnico é esse objeto técnico onde um determinado regime dos elementos naturais que o cercam está ligado a um determinado regime dos elementos que o constituem e que Simondon chama de "meio associado"** (exemplo: a turbina de Guimbal). "Diríamos que há indivíduo técnico quando o meio associado existe como condição _sine qua non_ de funcionamento, **enquanto há conjunto no caso contrário**."
> (p. 23-4)

> Com efeito, **o conjunto técnico é constituído por um certo número de indivíduos técnicos organizados entre si quanto a seu resultado de funcionamento e não se estorvando no condicionamento de seu funcio namento específico.** Assim, a corrediça de Stephenson e a caldeira tubular, elementos saídos do conjunto artesanal do século XV111, entram nos novos indivíduos do século XIX sob a forma, por exemplo, da locomotiva; a concentração de todos esses objetos técnicos que empregam energia térmica formará, por sua vez, os grandes conjuntos industriais do século passado. Do elemento termodinâmico passou-se para o indivíduo termodinâmico e dos indivíduos termodinâmicos para o conjunto termodinâmico.
> (p. 24)

# A máquina como novo indivíduo técnico (substituto do humano)

> A distinção em três níveis é importante porque permite com preender, na realidade técnica, **onde incide o progresso técnico (funda mentalmente a invenção) e onde incidem as determinações económicas.**  Enquanto a invenção está ligada ao elemento, a repercussão económica ocorre no indivíduo e no conjunto técnicos; quanto às incidências sociais, é principalmente ao nível do indivíduo técnico que se manifesta o problema da civilização industrial. Pois aqui, **ao contrário do que ocorria nas sociedades não industriais, o indivíduo técnico não é mais o homem e sim a máquina; a máquina substitui o homem funcional mente enquanto indivíduo, agora é ela quem conserva as ferramentas.**
> (p. 24)

# Humano como organizador do conjunto de indivíduos técnicos

> A questão é de grande importância e graves consequências, já que “mudou o próprio suporte da individualização técnica: o suporte era o indivíduo humano; agora é a máquina; as ferramentas são conservadas pela maquina, e poder-se-ia definir a máquina como aquilo que conserva suas ferramentas e as dirige. **O homem dirige ou regula a máquina que conserva as ferramentas: ele realiza agrupamentos de maquinas mas não conserva as ferramentas; a máquina exerce efetivamente o trabalho central, o trabalho do mestre ferreiro e não o do ajudante; o homem, desligado dessa função de indivíduo técnico que é, por essência, a função artesanal, pode tornar-se organizador do conjunto dos indivíduos técnicos, ou ajudante dos indivíduos técnicos.** (...) Nesse sentido, ele tem portanto um papel inferior ao da indivi dualidade técnica e um outro papel superior: servente e regulador, ele dá assistência à máquina, indivíduo técnico, ocupando-se da relação da máquina com os elementos e o conjunto; **ele é organizador das relações entre os níveis técnicos, em vez de ser ele próprio um dos níveis técnicos**, como o artesão”.
> (p. 24-5)

# Hierarquia não se justifica na realidade técnica

> A partir daí surgem os contornos de uma distinção hierárquica entre aquele que se ocupa dos elementos e aquele que trata dos con juntos. Mas é necessário grifar: tal distinção não tem nada a ver com a realidade técnica, que não é hierarquizável. Se há uma primazia dos conjuntos, é porque estes foram dotados com as prerrogativas das pessoas que assumem o papel de chefes; e se os elementos são desva lorizados, é porque outrora eram pouco elaborados ou executados por ajudantes.
> (p. 25)

# Progresso técnico em Simondon

elemento -> conjunto -> elemento (p. 25)

> **O nascimento dos indivíduos técnicos completos no século XIX introduz entretanto uma ruptura na noção de progresso, no momento em que o indivíduo humano toma-se servente da máquina ou respon sável pela organização dos conjuntos técnicos:** “A noção de progresso se desdobra, torna-se angustiante e agressiva, ambivalente; o progresso está distante do homem individual, pois para o homem não existem mais as condições para a percepção intuitiva do progresso. (...) **O progresso é então pensado de maneira cósmica, ao nível dos resultados do conjunto. Ê pensado abstratamente, intelectualmente, de modo doutrinal. Não são mais os artesãos, mas sim os matemáticos que pensam o progresso, concebido como uma posse da natureza pelo homem".**
> (p. 25-6)

# Alienação do trabalho (Marx) e alienação técnica (Simondon)

> E será o marxismo que, deburçando-se sobre o homem dos elementos, descobrirá a alienação. Todavia, para Simondon, a alienação que o marxismo capta como tendo sua origem na relação do trabalhador com os meios de produção, não procede somente uma relação de propriedade ou não propriedade entre o trabalhador e os instrumentos de trabalho. **Sob essa relação jurídica e econômica de propriedade, existe uma relação ainda mais profunda e essencial, "a da continuidade entre o indivíduo humano e o indivíduo técnico, ou da descontinuidade e e os dois seres. (...) A alienação surge no momento em que o trabalhador não é mais proprietário de seus meios de produção, mas ela não aparece apenas por causa da ruptura do vínculo de propriedade. Também surge fora de qualquer relação coletiva com os meios de produção, ao nível propriamente individual, fisiológico e psicológico. (...) A máquina não prolonga mais o esquema corporal, nem para os operários, nem para aqueles que possuem as máquinas. Os banqueiros cujo papel social é exaltado tanto pelos matemáticos como pelos saint-simonianos e Auguste Comte são tão alienados em relação à máquina quanto os membros do novo proletariado”.**
> (p. 26)

# Automatismo, causalidade e finalidade. Necessidade de uma cultura técnica para inteligência do indivíduo técnico.

> O resultado é que a reunião do capital e do trabalho já não será suficiente para reduzir a alienação: “O trabalho possui a inteligência dos elementos, o capital possui a inteligência dos conjuntos; mas não é reunindo a inteligência dos elementos e a inteligência dos conjuntos que se pode obter a inteligência do ser _intermediário e não misto_ que é o indivíduo técnico. (...) **A coletivização dos meios de produção não pode operar por si mesma uma redução da alienação; só pode operá-la se é condição prévia para a aquisição, pelo indivíduo humano, da inteligência do objeto técnico individuado”, o que supõe uma cultura técnica que autoriza outras atividades além daquelas do trabalho e da ação.** “Trabalho e ação têm em comum a predominância da finalidade sobre a causalidade; em ambos os casos, o esforço é orientado para um certo resultado que se quer obter: po esquema de ação conta menos que o resultado da ação. **No indivíduo técnico, ao contrário, esse desequilíbrio entre causalidade e finalidada desaparece; a máquina é exteriormente _feita para_ obter determinado resultado; mas quanto mais o objeto técnico se individualiza, mais esssa finalidade externa apaga-se em proveito coerência interna do funcionamento; o funcionamento é finalizado com com relação a si mesmo, antes de sê-lo com relação ao mundo exterior. Tal é o automatismo da máquina e tal é sua auto-regulação: ao nível das regulações, há funcionamento, e não só causalidade e finalidade; no funcionamento auto-regulado, toda causa lidade tem um sentido de finalidade, e toda finalidade um sentido de causalidade.”**
> (p. 26-7)

# Humano como subjugado ou dominador da técnica no marxismo (~progresso técnico)

> O estudo de Gilbert Simondon é fundamental porque permite que se comece a levantar a ambiguidade ancorada na noção de pro gresso técnico, sobretudo do lado dos marxistas. Pois se o marxismo esclareceu as relações entre a máquina e seu servente, o homem dos elementos, se conseguiu formular as consequências sociais e econó micas dessa submissão e propor diretivas políticas, no entanto perma neceu fiel à representação que faz da inteligência dos conjuntos o equivalente da racionalidade técnica e esgota o seu sentido. **O marxismo critica a alienação do homem dos elementos mas acredita que ela só pode ser superada fazendo dele o homem dos conjuntos, o senhor da natureza e da técnica, o organizador. Tal concepção da técnica é instrumentalista e evolui entre dois pólos, onde só se pode ser subjugado ou conquistador; para ela a máquina torna-se uma máquina de guerra,** o que aliás é muito bem expresso por Lucy Prenant, pensadora marxista dos anos trinta' que, depois de ressaltar que “o único fator permanente no progresso humano é a técnica”,35 explica o valor do conhecimento nos seguintes termos: “Por mais longe que avance o nosso conhecimento, para além da intuição sensível e ingénua, para além da reação primitiva, ele se origina na matéria, volta a ela como dominador, e assim se demonstra; por mais recuado no tempo e no espaço, por mais complicado que seja o objeto que estuda, sob este aspecto, nenhum limite seria capaz de detê-lo definitivamente”.
> (p. 27)

# Filosofia autocrática (tecnocratas e marxistas). Máquina como meio submisso.

> Mas também é preciso ver que, nesse ponto, os marxistas apro- ximam-se do pensamento tecnocrático pois, como este, são prisioneiros do que Simondon denomina uma filosofia autocrática da técnica, ‘‘aquela que considera 0 conjunto técnico como um lugar onde as máquinas são utilizadas para se obter potência. **A máquina é apenas um meio; o fim é a conquista da natureza, a domesticação das forças naturais através de uma sujeição primeira: a máquina é um escravo que serve para fazer outros escravos.** Tal inspiração dominadora e escra- vagista pode ir de encontro a uma exigência de liberdade para o homem. Mas é difícil libertar-se transferindo a escravidão para outros seres, homens, animais ou máquinas; reinar sobre um povo de máquinas subjugando o mundo inteiro ainda é reinar, e todo reino supõe a aceitação de esquemas de sujeição".
> (p. 27-8)

# Racionalidade instrumental (Marcuse) x realidade operacional

> Racionalidade instrumental. Essa é a questão, esse é o ponto em que Herbert Marcuse vai concentrar todos os seus esforços teóricos para demonstrar que as sociedades industrializadas são sociedades unidimensionais na medida em que reduzem o homem, a linguagem e o pensamento a uma dimensão operacional; em que suprimem a tensão que sustentava o pensamento crítico; em que criam um universo esterilizador ou neutralizador de qualquer tentativa consequente de subver são.
> (p. 28)

> Voltemos um instante a Marcuse e seu Homem unidimensional.  Para explicar como a racionalidade instrumental permeia todo o tecido social, o filósofo também postula uma identidade entre técnica e ope racionalidade. “A atitude ‘conforme’ diante da instrumentalidade é uma abordagem técnica, o logos ‘conforme’ é uma tecnologia, é ele quem simultaneamente projeta uma realidade tecnológica e responde a ela.”41 Ou ainda: “Não me ocupo aqui da relação histórica entre a racionalidade científica e a racionalidade social no início da era mo derna. Meu objetivo é demonstrar o caráter instrumentalista interno dessa racionalidade que faz com que ela seja a priori uma tecnologia; meu objetivo é demonstrar o a priori dè uma tecnologia específica — isto é, da tecnologia enquanto forma de controle e de dominação çopial”
> (p. 29-30)

> Se compreendermos tecnologia como inteligência dos conjuntos, então poderemos estabelecer uma homologia entre a racionalidade téc nica e a racionalidade social - homologia que e o propno fundamento da instrumentalidade e que permitirá a Marcuse duter. hoje a domi nação continua existindo (...) sobretudo enquanto tecnologia ’ Mas se uma abordagem realmente técnica supõe a inteligibilidade dos ele mentos, dos indivíduos e dos conjuntos, como quer Simondon, então a análise do Homem unidimensional já não se sustenta mais pois o fator instrumental não é específico de um nível essencial, o nível do indivíduo técnico. **Com efeito, em Marcuse tudo se passa como se o que é próprio do homem dos conjuntos — a previsão, o controle, a dominação pendesse e caísse do lado do “fato técnico”, transformando-o em racionalidade instrumental. O problema é que, nesse movimento, a própria reflexão de Marcuse também cai do lado do homem dos con juntos e de sua racionalidade específica. E acabamos desembocando em algo muito paradoxal: a luta contra a racionalidade instrumental passaria pela adesão a essa racionalidade!**
> (p. 30)

[Embora Marcuse leia e cite Simondon n'O homem unidimensional, sua filosofia continua a ser autocrática.]

> Responder a tais perguntas seria bastante difícil e, de qualquer maneira, escaparia ao âmbito deste trabalho. No entanto, pelo menos uma coisa parece certa: **Marcuse ergue-se contra a instrumentalização dos homens que constitui a regra do mundo contemporâneo; em seu lugar, quer instaurar o reino da Razão, uma Razão “que só pode preencher essa função se for uma racionalidade pós-tecnológica, na qual a própria técnica é um instrumento de pacificação, em suma, o organon da ‘arte de viver’”.** Mas o filósofo não percebe que ao conceder a supremacia ao Logos,52 faz do homem um instrumento da Razão; e que, talvez, seja precisamente a partir daí que o questiona- mento deva começar.
> (p. 31)

# Crítica de Habermas a Marcuse

> “Afinal, em muitos trechos do Homem unidimensional", escreve o teórico alemão, “não se trata mais de revolucionar a sociedade senão no sentido de uma mudança do quadro institucional que deixaria in- tactas as forças produtivas enquanto tais. A estrutura específica do progresso científico e técnico seria portanto mantida, só mudariam os valores diretores. Novos valores seriam traduzidos em termos de pro blemas suscetíveis de receberem uma solução técnica. O que seria novo, seria a _direção_ do progresso, mas a racionalidade como critério permaneceria intacta".
> (p. 31-2)

# Habermas sobre trabalho (racional, instrumental) x interação (comunicação). Capitalismo como expansão do sistema e da racionalidade instrumental para todo o tecido social.

(p. 32-4)

> A análise dessa dupla evolução tendencial — intervencionismo do Estado, cientifização da técnica — leva Habermas a rever os conceitos marxistas de luta de classes e ideologia. Por um lado, num capitalismo regulado pela intervenção estatal, os antagonismos de classç são conge lados e tornam-se latentes, enquanto o conflito desloca-se para os se- tores subpnvilegiados da vida social. Por outro, uma vez que o processo tecmco e cientifico é a força produtiva principal, ele aparece como a nova forma de legitimação e substitui a antiga ideologia burguesa.
> (p. 34)

> O fulcro da nova ideologia é a “consciência tecnocrática” que, extraindo da organização da vida coletiva as normas de ação, as reduz às funções de um sistema subordinado à racionalidade instrumental, despolitizando-as. Por conseguinte é através da consciência tecnocrática que se realiza a passagem entre sistemas técnicos e sociedade: "os modelos reificados das ciências passam para o mundo sócio-cultural vivida e adquirem um poder objetivo sobre a concepção que ele faz de si próprio. O núcleo ideológico da consciência em questão, é a *eliminação da diferença entre a prática e a técnica*”. Assim, a consciência tecnocrática oculta o interesse prático por trás de nosso inte resse em ampliar nosso poder de dispor das coisas tecnicamente.
> (p. 34-5)

[Crítica a partir de M. Chauí (p. 35)]

> Habermas, por sua vez, dilui as relações sociais numa latência, as substitui por relações entre os indivíduos e um _Welfare-State_ gerado pela expansão do capitalismo no período 1950-1973, e, no plano teórico, propõe a refundição das categorias ana líticas para se pensar a sociedade industrial. O problema, é que o teórico alemão deixa intacta a racionalidade instrumental que imputa à técnica e não acredita na possibilidade de concebê-la de outra maneira; **o problema, é que ele a aceita como tal mas pretende alojá-la em sub sistemas específicos ou, pelo menos, neutralizar suas incidências sociais através do diálogo democrático, da racionalidade comunicante.**
> (p. 36-7)

# Progresso como ideologia em Eliade

> Sabemos, porém, que tratamos com um dogma quando falamos do progresso. Numlivrinho maravilhoso, Mircea Eliade explica precisa mente por que ele é um dogma e quanto deve à ideologia dos alqui mistas: “Não se deveria acreditar que o triunfo da ciência experimental aniquilou os sonhos e a ideologia dos alquimistas. **Ao contrário, a ideologia da nova era, cristalizada em tomo do mito do progresso infinito, reconhecido pelas ciências experimentais e pela industrialização, essa ideologia que domina e inspira o século XIX inteiro apesar de sua secularização radical, retoma e assume o sonho milenar do alquimista. Ê no dogma específico do século XIX — que a verdadeira missão do homem é mudar e transformar a Natureza, que ele pode fazer melhor e mais depressa que a Natureza, que é chamado para tomar-se o senhor da Natureza —, é nesse dogma que precisamos buscar o autêntico prosseguimento do sonho dos alquimistas”.**
> (p. 40)

> Mircea Eliade diz que esse sonho é encontrado sempre que se exprime a fé nas possibilidades ilimitadas do _homo faber_, sempre que transparece a significação escatológica do trabalho, da técnica, da exploração científica da natureza, pois tudo isso se nutre da certeza de que dominando a Natureza pode-se rivalizar com ela, “*mas sem perder mais tempo*".
> (p. 40)

# Dessacralização do trabalho e da Natureza para que seja possível substituir a duração de tempo pela ciência e trabalho

> Assim, através da ciência e do trabalho o homem moderno substiui-se ao tempo, assume a função da duração temporal. Mas isso exige, em contrapartida, que o trabalho seja secularizado, dessacralizado, como a ciência moderna, que só pôde se constituir dessacralizando a Natureza. E o que é o trabalho secularizado, senão essa “chaga viva no corpo das sociedades modernas”.77 “A obra do tempo”, escreve ainda Eliade, “só podia ser substituída pelo trabalho intelectual e manual, sobretudo, e quanto! pelo trabalho manual. Sem dúvida, desde sempre o homem foi condenado a trabalhar. Mas há uma dife rença, e ela é fundamental: para fornecer a energia necessária aos sonhos e às ambições do século XIX, o trabalho teve de ser secula-, rizado. Pela primeira vez em sua história, o homem assumiu esse trabalho tão árduo ‘para fazer melhor e mais depressa que a Natureza’ não da dimensão litúrgica que em outras sociedades tornava o trabalho suportável. E é no trabalho definitivamente secularizado, no trabalho em estado puro, enumerado em horas e em unidades de energia gastas, é em tal trabalho que o homem suporta e sente mais implacavelmente a duração temporal, sua lentidão e seu peso.”
> (p. 40-1)

> Compreende-se agora por que é tão difícil propor uma mudança radical nas sociedades industriais. **Isso exigiria, nada menos! que uma outra relação com o Trabalho, outra concepção da Natureza, do Tempo e da Razão.** Questões que ficam necessariamente abertas, para as quais não há formulação científica, mas que estamos sempre ten ando conjurar através de um apelo cada vez mais urgente ao desenvolvimento da racionalidade da técnica, dos homens, das instituições, das sociedades, sem percebermos, como diriam G. Deleuze e F. Guattari, que **“não é o sono da razão quem gera os monstros, mas sim a racionalidade vigilante e insone”.**
> (p. 41)

# Sobre a impossibilidade de assentar a pesquisa sobre um modelo teórico acabado

> Talvez se pense que me distancio demais do meu assunto, que todas essas questões ultrapassam de muito o quadro de meu trabalho (e também de minha competência) e que o problema teórico deste estudo — pensar as relações entre a introdução de determinadas tecno logias e os efeitos sociais a ela imputados — perdeu-se no meio do caminho. Entretanto, se escolhi expor minha incursão nas obras teó ricas e os problemas que nelas encontrei, é porque pareceu-me não poder contornar essa etapa, indispensável para compreender o que me impedia de considerar sólido um terreno minado de interrogações fundamentais; em resumo, pareceu-me importante contar como fui levado a reconhecer a impossibilidade de referenciar minha analise a um modelo teórico indiscutível, e também de erguer toda a arquitetura deste trabalho sobre os pilares de uma teoria acabada, capaz de apazi guar todas as minhas dúvidas.
> (p. 41)

# A ficção como imaginação contra o pessimismo da racionalidade instrumental

> E foi curiosamente na ficção, ou mais precisamente no que denominam literatura de antecipação, que pude encontrar uma ponta de esperança capaz de atenuar um pouco o pessimismo ostentado pela reflexão teó rica sobre a racionalidade instrumental e capaz, também, de ensinar que a revolta é irredutível apesar de todas as servidões, de todas as algemas físicas, que a revolta está inscrita no gesto e na fala da vida cotidiana.
> (p. 42)

# Ficção de antecipação (Orwell, Huxley, Zamiatine) x literatura sobre o futuro

> Com efeito, desde a publicação do livro The reign of George VI, 1900-25, no século XVIII, a literatura sobre o futuro em língua inglesa, como mais tarde todas as legitimações promocionais das novas tecnologias, convida o público a participar de um ritual de controle onde o fascínio pela técnica mascara os fatores de política e de poder subja centes. Isso não ocorre, entretanto, quando se lê 1984, de George Orwell,84 Admirável mundo novo, de Aldous Huxley85 e Nous autres, de Eugène Zamiatine.86 Graças à ficção tais autores, **explorando os limites dos problemas que abordam, podem apreender o desfecho lógico de algumas das mais importantes linhas mestras que percorrem a sociedade industrial.**
> (p. 42-3)

# 1984 - Orwell

> O que caracteriza, porém, a sociedade de Oceânia não é tanto a maneira como ela trata seus dissidentes — a eliminação física através da “vaporização”. O essencial não está aí, mas na maneira como essa sociedade encara os signos, entendidos simultaneamente como sinto mas de não ortodoxia que se devem farejar em cada cidadão e como figuras que exprimem idéias e realidades complexas e movediças que se devem simplificar e fixar. **O essencial ocorre entre esses dois pólos: de um lado o Panopticon de Jeremy Benthan estendido ao tecido social inteiro (o olho de Big Brother que vigia o mínimo gesto ou fala); de outro, o empobrecimento sistemático da língua e a criação da Novi-língua, cujo objetivo é fornecer um modo de expressão para as idéias gerais e hábitos mentais dos devotos dos Angsoc, mas também de tomar impossível qualquer outro modo de pensar.**
> (p. 43)

> A maioria dos comentários do livro de Orwell que pude ler concentra sua atenção sobre o primeiro pólo e se pergunta se as novas tecnologias tecnologias tornam possível um sistema de controle social semelhante ou se, ao contrário, elas autorizam uma democratização mais ampla, visto que a multiplicação de sistemas computadores impediria a instauração da vigilância absoluta, por razões econômicas. Seguindo essa linha, esses comentários deixam na sombra uma questão muito importante; pois e evidente que os televídeos assumem papel capital porque Oceânia é uma sociedade de transição, porque ainda é necessário espionar constantemente ou, pelo menos, deixar parar sempre a possibilidade de que se está vendo e gravando a conduta dos cidadãos.  Também é evidente que a instituição mais promissora é o Ministério da Verdade, e não é por acaso que Winston Smith, personagem central do romance, encontra-se entre seus funcionários. **Lá são preparadas as edições cada vez mais coercitivas do dicionário da Novilíngua, lá se fabrica toda a produção cultural, lá o passado é reescrito regularmente segundo os imperativos imediatos, apagando-se todas as marcas, trabalhando-se cuidadosamente para aniquilar a memória e a história. O Ministério da Verdade é portanto o ministério da mentira sistemática e do que Bernard Cuau chama de rarefação da linguagem; sua lógica acabará tomando o controle supérfluo.** Oceânia caracteriza-se pela opressão precisamente porque é uma sociedade de transição: ainda é preciso impor a lei do partido, usar a força para obrigar os indivíduos a se curvarem diante das exigências do Estado, exercer uma pressão exterior ao sujeito enquanto não se tiverem ven cido suas resistências à manipulação.
> (p. 44-5)

> No entanto, importa ressaltar que a originalidade e o interesse do livro de Orwell não se encontram na luta desigual entre o Estado e os indivíduos nem mesmo no uso de televisões e gravadores para fins de controle social, mas na tentativa de redução da linguagem e do pensa mento a uma dimensão instrumental. Esse é o fenomeno realmente importante, é ele que sensibiliza Marcuse quando o Homem Unidi mensional faz a teoria da rarefação da linguagem e do pensamento, mostrando que sua existência e expansão não supõem necessariamente um regime totalitário. Não é o momento de prosseguir esta análise, que implicaria o exame das relações entre o impacto da linguagem funcio nal e a proliferação dos meios eletrónicos. Basta assinalar que, desig nando o que está em jogo com a instrumentalização da linguagem, Orwell traça um caminho fundamental para a compreensão de um aspecto aparentemente anódino — mas perigosíssimo — da racionali dade nas sociedades industriais.
> (p. 45)

# Admirável mundo novo - Aldous Huxley

> Todo mundo é feliz, reina a boa consciência. Como isso é possível? Simplesmente, a sociedade de Admiravel Mundo Novo descobriu que o segredo da felicidade e da virtude é amar o que se é obrigado a fazer. **Inspirado por Ford — o inventor da linha de montagem industrial —, auxiliado pelo progresso da ciência, o Estado Mundial construiu uma sociedade programada onde não há lugar para a arte, a literatura e a pesquisa científica não controlada, cujas verdades poderiam comprometer o conforto e a felicidade.** A organização social repousa sobre a produção e o consumo de massa e os indivíduos não têm nenhuma dificuldade de adaptação pois também são fabricados em série, em função dos imperativos da produção-con sumo. Com efeito, a harmonia é possível porque o Centro de Incubação e Condicionamento produz uma hierarquia sob medida para ocupar todas as posições sociais: há os Administradores Mundiais, escolhidos entre os Alfa Mais-Mais, e, abaixo, os Alfa, os Beta, os Gama, os Delta, os ípsilons Semi-Abortos. Intervenções durante a gestação artificial e o recurso sistemático do condicionamento do comporta mento durante a infância garantem o desenvolvimento das qualidades físicas e mentais exigidas pelo lugar reservado ao futuro adulto.
> (p. 45)

> Assim, como mercadorias, os indivíduos não têm interioridade e são intercambiáveis dentro de sua série de fabricação. Nesse sentido, o lema do Estado Mundial resume muito bem os traços específicos dessa sociedade: há comunidade de destino, pois todos trabalham exclusi vamente para o bem-estar de todos; há identidade absoluta, pois não há mais contradição entre os homens e sua função e, além disso, os seres de cada série são idênticos uns aos outros, comportando-se da mesma maneira, pensando do mesmo modo, drogando-se pelas mes mas razões, vestindo-se com as mesmas roupas e cores, etc...; **há, enfim, estabilidade social, pois a sociedade transformou-se numa gigantesca máquina homeostática.**
> (p. 45)

> Os homens dó Estado Mundial foram mais longe: ja não rivalizam com a Natureza controlam-na e substituem-se a ela realizando o sonho alquimista de criação do homúnculo, sonho perseguido pela ciencia que desde a segunda metade do século XIX desejava atingir a preparaç o sintética da vida”. Talvez seja por esta razão que a idéia de progresso não tem mais sentido nessa sociedade: **não há mais nada a ser conquistado, mais nada a ser subjugado... Poderíamos crer que é o advento da liberdade; no entanto, o que se vê, é a face horrenda da sujeição absoluta.**
> (p. 46)

# Nous autres - Eugène Zamiatine

> Descobre-se então um Estado que, depois de derrotar a fome, organizou calculou e fixou o amor, o sono, o trabalho físico; em suma, que transformou a totalidade da vida em funções harmoniosas e úteis segundo cntenos extremos da racionalidade.
> (p. 46)

> A aceitação desse limite, dessa coerção, é o preço que se tem de pagar para que a felicidade se tome organizável. Assim, o Estado Único construiu um muro duplo. Por um lado, 0 Muro Verde que cerca a cidade e a separa da desordem da Natureza;90 por outro, um muro interior que dentro de cada cidadão deve conter e recalcar tudo 0 que não é “lógico”, razoável. **Entre esses dois limites organiza-se 0 espaço da sociedade, onde “nada... é deixado ao acaso”: 0 Estado e seus súditos — ou melhor, seus números — constituem “um único orga nismo de milhões de células”, onde “todos” e “eu” formam um só “Nós”, onde as máquinas perfeitas assemelham-se aos homens e os homens perfeitos, às máquinas. As Tábuas das Horas, “coração e pulso do Estado Único”, ordenam 0 que se deve fazer a cada instante: dormir, comer, trabalhar, passear, trepar, etc. — pois cada ato foi racionalizado, taylorizado com a maior precisão, para que possa con tribuir para o funcionamento da máquina do Estado e da felicidade.**
> (p. 47)

> Todo mundo pode “ler” o pensamento dos outros antes mesmo que ele seja expresso: todos seguem a mesma lógica — apreender e transformar em equações —, todos formalizam seus enunciados em termos geométricos. Entre números, fala-se da beleza do quadrado (D-503 descreve-se como um quadrado), do cubo, da linha reta, a linha do Estado Único, porque é grande, precisa, sensata, a mais sen sata de todas”,92 ouve-se o choque metálico das idéias; sente-se tran quilo nas regiões azuis da abstração. **Mas, apesar da felicidade gelada e transparente no Estado Único ainda não se atingiu o ideal -- "quando nada poderá acontecer": ainda há acontecimentos imprevisíveis, incalculáveis, em suma, ainda existe o desconhecido.** Em seu diário D-S03 escreve- “É no ponto que se encontra o maior numero de incertezas.  basta que ele se mexa e se desloque e gera milhares de curvas centenas de corpos".’3 E será num ponto, no X das sobrancelhas de 1-330, que D-503 se perderá. Apesar da construção sólida de seu raciocínio, ape sar dos guardas, dos “anjos da guarda” que te seguem por toda parte e que te ajudam a respeitar a linha apesar da ameaça de morte na Campânula Pneumática, rompeu-se o muro interior: D-503 descobre o desejo.
> (p. 47-8)

> Vimos anteriormente que os teóricos críticos da sociedade indus trial aspiram a uma racionalidade cada vez maior para possibilitar o advento da liberação. Entretanto, através da ficção, Zamiatine des mente essa démarche e demonstra que na verdade a equação raciona lidade = revolta contra a sociedade só pode ser uma ficção. No universo da Razão acabada, realizada, em nome de que racionalidade reivindicar a revolta? Em Nous autres, a questão é outra: a rebelião vem de fora dos outros, dos que o sistema excluiu, e sobe também de dentro, de Nos, e se explode, se rompe os limites, não é tanto porque é razoável, mas necessária, porque faz parte da vida. O que, aliás, é dito admiravelmente num dialogo entre D-503 e I-330, no final do livro:
> "-- Meu caro, você é matemático, mais ainda, é um filósofo-matemático; pois bem, diga-me o _último_ algarismo.
> -- O quê? Não estou entendendo, que último algarimo?
> -- Pois o mais alto, o maior!
> -- Mas I, isso é absurdo. O número dos algarismos é infinito, não pode haver um último.
> -- Então porque você fala da revolução derradeira? Não há revolução derradeira, o número de revoluções é infinito. A derradeira é para as crianças: elas têm medo do infinito e precisam dormir em paz...”
> (p. 48-9)

> Não é de se espantar que dos três romances rapidamente mencio nados Nous autres seja o único que termina com uma abertura — a possibilidade de liberação e de revolta —, enquanto 1984 e Admirável mundo novo apresentam-se um universo fechado e sem saída. Também não surpreende que já em 1920 Zamiatine consiga apreender com tanta acuidade o problema espinhoso do “socialismo real” e indicar em que ponto ele prolonga 0 capitalismo industrial. Em 1920 um Vladimir Tátlin fazia das formas geométricas em movimento no interior de uma espiral sem fim um monumento em homenagem à III Internacional e ao triunfo definitivo da razão apresentada pela vitória do socialismo. **No mesmo momento, Zamiatine questionava os elementos básicos — a espiral, o cubo, o triângulo, o cilindro — de uma concepção que pretendia captar a forma pura de um “socialismo científico” mas que continuava prisioneira do racionalismo apesar da ruptura real que provocava no campo artístico.** Finalmente, é preciso observar que 0 escritor russo, em todas as passagens relativas à “grande operação” que suprime a imaginação, que trata cirurgicamente qualquer sinal de oposição à racionalidade do Estado Único, oferece-nos uma prefigu ração perturbadora das internações psiquiátricas e de sua lógica. Mas quando leio os enunciados hiper-racionais de D-503, quando 0 vejo pensar geometricamente, exaltar a sensatez da linha reta, imobilizar-se de medo diante do ponto — é impossível não pensar em Joana, a louca de que falava Maura Lopes Cançado96 que, em seu vestido xadrez, percorria maquinalmente o quadrado do pátio de um hospício carioca, apavorada de que uma curva, uma curva mínima como o arredonda- dado de uma gota d’água, a atirasse no vazio e a aniquilasse.
> (p. 49)
