---
title: "O divíduo: práticas digitais e biotecnologias"
tags:
    - Fernanda Bruno
    - Pablo Rodríguez
    - Gilles Deleuze
    - biotecnologias
    - digital
    - divíduo
    - algoritmo
    - sociedades de controle
    - perfil digital
    - perfilamento
    - redes sociais
    - governamentalidade algorítmica

date: 2023-05-15
category: fichamento
path: "/o-dividuo"
featuredImage: "cover.png"
srcInfo: <a href="https://journals.sagepub.com/toc/tcsa/39/3">Theory, Culture & Society</a>
published: true
---

Fichamento do artigo "O dividual: práticas digitais e biotecnologia"[^1] de Fernanda Bruno e Pablo Rodríguez, sobre a noção de divíduo principalmente em Deleuze e em relação às novas tecnologias.

[^1]: BRUNO, F.; RODRÍGUEZ, P. M. The Dividual: Digital Practices and Biotechnologies. **Theory, Culture & Society**, v. 39, n. 3, p. 27–50, 1 maio 2022. 

# Abstract

> This article revisits the concept of the dividual, taking as a starting point Deleuze’s diagnosis about the relevance that dividual practices have gained with the advent of biotechnology and digital culture. Although we agree with this diagnosis, we highlight the intersections between the dividual and the individual both in Modernity and in the present time. **The contemporary dividual is in tension with the modern individual, but not as a substitution, division or duplication of the individual. Rather, we state a complex dividual-individual composition, focusing on biotechnologies, digital culture and financial capitalism.** Following a Foucauldian approach, we understand this composition as a result of technologies of power and the result of certain modes of subjectivation. **In dialogue with Simondon’s theory of individuation, we suggest that the dividual takes part in a new mode of subjectivation which can be named a ‘dividuation’ in the context of technologically mediated experiences.**
> (p. 27)

# Dividual nas práticas digitais e nas biotecnologias

> Our behaviors and relations (with oneself, with others and with objects) are closer to what we shall call ‘the dividual’ than to the individual, especially in light of the experiences mediated by biotechnologies, applications, social networks and digital platforms. **The dividual emerges in such contexts by means of both data and partial and relational actions – clicks, likes, shares, posts in digital platforms. It emerges also by means of stem cells, genetic sequences and the bio-quantified self in the realm of biotechnologies – that do not fit the nuclear, unified and indivisible perspective of the individual.** A culture of dividuality is present in our technically mediated practices, and is ever more present in the way digital culture corporations, and occasionally the state, deal with our actions and behaviors.
> (p. 27-8)

# Co-presença indivíduo-divíduo

> In this article we want to consider the contemporary inscriptions of the dividual and of the individual, but not from a perspective of opposition or exclusion between them, but instead one of co-presence, which entails occasional tensions and ambiguities but also possible alliances and syntheses.
> (p. 28 )

# Divíduo como oposição ao indivíduo em _Post-scriptum sur les societés du contrôle_ (PSC) de Deleuze

> Deleuze identifies the primacy of the dividual in the new mode of operation of power that he calls control. Dividual is a frequent term in Deleuze’s work, but it is in the brief and much-quoted ‘Postscript on the Societies of Control’ that the philosophical aspects of the dividual were related to the Foucauldian perspective and to contemporary issues such as digital and biotechnological practices.
> (p. 28 )

> According to Deleuze, the dividual functions as a counterpoint to the individual in disciplinary society, as the sketch for a new type of social relationship that could be said to be already in existence in the early 1990s. On the one hand this starting point is crucial; on the other hand it is not enough to understand the current forms of technology-mediated experience and the complex relationships between the dividual and the individual in this context. In other words, **if Deleuze’s seminal text draws our attention to the relevance that dividual practices have gained with the advent of biotechnology and digital culture, it does not give us tools to address the processes of power and subjectivation that involve both the dividual and the individual. Unlike his own analyses of the dividual in other texts, ‘Postscript on the Societies of Control’ presumes a very sharp rupture between the individual form of disciplinary modernity and the dividual form of the emerging apparatuses of control.** This perspective is reproduced in many contemporary analyses of the dividual, especially in the field of digital culture studies, where it is usually conceived in opposition and/or in substitution of the individual.
> (p. 28 )

# Série de estudos sobre ritmo de Paul Klee: indivíduo como indivisível; divíduo como divisibilidade; síntese dividual-individual

> In Figure 1, the figure of the individual is linear and consists of a continuous line constituting a body (a dancing body, for it is from Paul Klee’s studies on rhythm). If we subtract a part, we destroy its totality; it becomes unrecognizable. ‘In that sense, the individual is first something indivisible: to divide it would be to mutilate it, to destroy its constitutive organic unity’ (Chamayou, 2014). 
> (p. 29 )

![Individual](./bruno-rodriguez2022-fig1.png)

> ‘The dividual, on the contrary, is defined by its divisibility’ (Chamayou, 2014). Figure 2, constituted by dividual lines, can be cut out and divided into parts, without its motif being dissolved. ‘It will remain despite its partition. This is, in brief, the difference between the pattern of a tapestry, whose rhythms are repetitive, and the drawing of the organic form of a body’ (Chamayou, 2014).
> (p. 29 )

![Dividual](./bruno-rodriguez2022-fig2.png)

> The figure we are trying to imagine and that arises in this strange present we inhabit, would be, perhaps, a composition of the previous two: a ‘dividual-individual synthesis’ (Klee, 1973: 63 in Chamayou, 2014). If this figure, on the one hand, challenges us to the issue of how much we recognize ourselves in it, on the other hand it encourages us to ask about the modes of subjectivation that can be produced from our dividual-individual practices.
> (p. 29 )

![Dividual-individual](./bruno-rodriguez2022-fig3.png)

![Grid dance](./bruno-rodriguez2022-fig4.png)

# Individualismo a partir do século XVI (perspectiva e retrato) e a distinção sociológica indivíduo-sociedade, privado-público

> **From the 16th century onwards, the notion of the individual became one of the key terms to define Modernity.** On the one hand, as argued by Le Breton (2013), on an aesthetic level the classic motif of the portrait and **the adoption of perspective as pictorial technique have heralded the emergence of modern individualism.** The technique of the portrait featured one of the characteristics of the modern individual: the reunion of the singular and the personal, heavily discussed in the earlier search for a principle of individuation in Scholastic philosophy (Raunig, 2016: 50–68). The portrait intended to summarize singularity and personality within individuality itself. On the other hand, on the political level, **the modern notion of the individual was defined in contrast with that of society in the classical works of contractualism.** **This issue was inherited by sociology and the human sciences as they emerged in the 19th century. The opposition between individual and society was translated into the distinction between the public, the private and the intimate realms of the individual (Sennett, 1977).**
> (p. 32 )

# Divíduo em Nietzsche

> In his archaeology of the dividual, Raunig points out that, concerning Modernity, **the strengthening of the individual in the two senses mentioned was challenged by the idea that the individual is already divided.** This can be found in Nietzsche’s _Human, All Too Human_ (2005), where **he sketches the image of moral as a self-division of the human being. Referring to a enamored woman who expects her lover’s infidelity in order to prove her own fidelity, to a soldier who gives his life for the Fatherland and to a mother who gives her son everything that she denies herself, Nietzsche states that ‘man is loving something of himself, a thought, a longing, an offspring, more than something else of himself; that he is thus dividing up his being and sacrificing one part for the other’ (Nietzsche, 2005: 42).** These are cases where the human being itself does not understand itself as an individual, but as _dividuum_.
> (p. 32)

# Divisão do indivíduo na psicanálise (Freud e Laing)

> **A second case of the individual ‘divided’ concerns the birth of psychoanalysis.** Freud argues that we should think of the subject from the point of view of its decentering, as ‘the ego is not the master in its own house’ (Freud, 1955 [1917]: 285). **As he displaced the being of the psychic from the self and from consciousness to the unconscious, Freud radically questioned the bases of the philosophy of the subject. This decentering will be later thought of as a division by Laing (2010), one of the fathers of anti-psychiatry.** Laing observes that schizoid individuals do not simply feature a divided self, but they experiment with various types of division, a product of ‘not having oneself’. In the same way, the other would not be, **for some schizophrenics, a unity, an individuality, but something that, for a brief moment, feels like a machine.**
> (p. 32-33 )

# Prática antropológica dividual a partir dos anos 50/70 (em oposição à distinção indivíduo/sociedade euroamericana) (Mauss, Fortes, Appadurai)

> While in these examples what is attacked is the alleged ‘non-divisibility’ of an individual that concentrates both personality and singularity, Raunig underlines that **the dividual’s anthropological approach, since the 1950s and more strongly in the 1970s, concerns the distinction between individual (intimacy and privacy) and society (publicity). These anthropological trends started to identify the dividual as a key concept to understand a kind of sociability distinct from the Euro-American notion founded on the opposition between individual and society.** According to Appadurai (2015a), by studying the social views of the Tallensi in reference to the classic definition of the person as a ‘microcosmos of social order’ (Marcel Mauss), Fortes (1973) identifies the fact that human beings become persons not in themselves, as individuals, but in the community. **As they die, they enter a lineage together with their ancestors and, in their turn, become the ancestor of others; that is, his or her individuality is not linked to corporeity, but instead to a social belonging extended in time, similar to a clan.** **Appadurai points out that Fortes was the first to use the term _dividual_ in the history of anthropology.** While Mauss’s contribution lies mainly in distinguishing the person from the self, **Fortes ‘identified the “dividual” as a more elementary and foundational element of agency (both human and animal) than the individual’ (Appadurai, 2015a: 103).**
> (p. 33 )

# Teoria antropológica do divíduo em Marriott (por Appadurai)

> Still, according to Appadurai, Marriott (1976) exposed the most radical formulation of a theory of the dividual in anthropology. **He argued that South Asian social systems were built on a wholly different architecture from those of the West. This foundation is the dividual, an agent of action and transaction that was continually transferred by contact with other dividuals to create new arrays of rank, purity, and potential liberation from the material world’ (Appadurai, 2015a: 103–4).**
> (p. 33 )

# Divíduo no "Gênero da dádiva" de Strathern

> Finally, in the seminal work _The Gender of The Gift_, Strathern describes the Melanesian person as dividual, in contrast to the Western individual: **‘Far from being regarded as unique entities, Melanesian persons are as dividually as they are individually conceived. They contain a generalized sociality within. Indeed, persons are frequently constructed as the plural and composite site of the relationships that produce them’ (Strathern, 1990: 13).**
> (p. 33 )

# Teoria política da individualidade em Foucault

> We cannot avoid the political theory of individuality developed by Foucault in this account. Despite the fact that he never mentioned the term dividual, he addressed both the division of the individual and its relationship with society on several levels, the same ones employed by Deleuze. **From Foucault’s perspective, to consider the individual equal to the person, as in contractualism, is to define the body as the material and political reality of the individual (Foucault, 1980).** The historical process that led to this is the expansion of the human sciences together with the disciplinary regime, in which the movement of individualization followed the opposite path than the one produced in early Modernity. **If in the age of the portraits ‘the higher you climb up the social ladder, the more you move away from anonymity towards individualization’ (Celis Bueno, 2020: 79), in the 19th century the power became anonymous and the bodies ‘on whom it is exercised tend to be more strongly individualized’ (Foucault, 1995: 193). Disciplinary societies are based on the individualization of the subject within the mass (Celis Bueno, 2020: 79).**
> (p. 34 )

# Individualização (individual) e objetivização (dividual)

> According to Foucault, the process of individualization is twofold: **while it is intimately related to the constitution of the subject, it is at the same time the grounding of the modern states in terms of the measurement of its strength.** On the level of disciplinary societies, we can find the issue of the individual’s division. **The ‘objectivizing of the subject’ is made possible by ‘dividing practices’: ‘The subject is either divided inside himself or divided from others […] the mad and the sane, the sick and the healthy, the criminals and the “good boys”’ (Foucault, in Dreyfus and Rabinow, 1982: 208).**
> (p. 34 )

# Anatomopolítica (indivíduo) e biopolítica (sociedade) como tecnologia política dos indivíduos

> On the level of biopolitics, Foucault addressed the other axis proposed here about the dividual, i.e. the link of the individual with the society, now in terms of the modern state and the notion of population. Two lines were drawn: **one that related the individual to the body as a way to regulate life by means of diverse apparatuses, and the other that leads to the notion of governmentality, where the individual was a part of a political calculus about degrees of freedom and security regarding its belonging to the state (Foucault, 2007a; Burchell et al., 1991).** This corresponds to a ‘political technology of individuals’ (Foucault, 1988a).
> (p. 34 )

# Individualização como subjetivação (indivíduo para si mesmo e para os outros ~ controle)

> Third, the process of individualization is understood by Foucault in terms of a process of subjectification.1 **The power ‘makes individuals subjects’, but now considering that there are at least ‘two meanings of the word subject: subject to someone else by control and dependence, and tied to his own identity by a conscience of self-knowledge’ (Foucault in Dreyfus and Rabinow, 1982: 212).**
> (p. 34 )

# Alinhamento com a teoria foucaultiana-deleuziana para pensar a cultura de dividualidade presente nas praticas tecnicamente mediadas da contemporaneidade

> We argue that the culture of dividuality present in our technically mediated practices can only be fully understood by placing its examination under the Foucauldian analytical frame of the individual. First, because Foucault refers to the ‘division of the self’ and the distinction of the subject regarding power as an intertwining on the level of political practices with something ‘decentering’ the individual during its constitution: disciplinary institutions (anatomo-politics), biological and medical interventions (bio-politics), and the construction of the self (subjectification). Second, because this is the basis for the Deleuzian analysis of the societies of control. Third, because, as we shall see, the Foucauldian approach sheds light on the growing literature about the dividual, following but also going beyond the Deleuzian approach, by distinguishing three levels of the individual-dividual composition: post-disciplinary, biopolitical and subjective.
> (p. 34-5 )

# Divíduo como divisão que muda qualitativamente o todo em Cinema 1

> Before the publication of _Postscript_, Deleuze and Guattari used the term dividual several times in line with the two approaches mentioned in the last section: **the division of the individual and its relationship with society.** In _Cinema 1: Movement-Image_, reflecting on Expressionist cinema, Deleuze writes that **the set ‘cannot divide into parts without qualitatively changing each time: it is neither divisible nor indivisible, but “dividual” [dividuel]’ (Deleuze, 1986: 14). The dividual is an ‘indivisible quality, which will only be divided by changing qualitatively’ (Deleuze, 1986: 98–9). This quality ‘exceeds the duality of the collective and the individual’ (Deleuze, 1986: 92).**
> (p. 35 )

# Divíduo como excesso da noção de indivíduo em direção ao coletivo em Mil platôs

> In _A Thousand Plateaus_ (1987), referring to the difference between voice and orchestration in Wagner, Verdi, Debussy and Mussorgsky, Deleuze and Guattari argue that **‘the people’ has nothing to do with ‘the persons within it’ and ‘must be individualized [through] the affects it experiences, simultaneously or successively’. That means that the people, far from being ‘a juxtaposition’ or representing ‘a power of the universal’, can be conceived in terms of ‘the Dividual’ and ‘the One-Crowd’ only when it exceeds the conception of the individuals and persons.** In other words, for Deleuze and Guattari the dividual is not the result of a self-division, and this allows them to think about the collective beyond the classical opposition between individual and society.
> (p. 35 )

# Divíduo como massas, amostras, dados, mercados ou bancos em oposição ao indivíduo em Postscriptum

Mas embora o ensaio vise enfatizar as distinções entre os "regimes de signo" disciplinar e de controle, nele também fica claro que a "semiótica" contemporânea é "mista", englobando aspectos disciplinares e de controle. A novidade é justamente o alargamento da sociedade disciplinar (que contém aspectos de soberania) para a sociedade de controle, que também conteria aspectos disciplinares e de soberania.

> Nevertheless, in the ‘Postscript’, while attempting to actualize Foucault’s classic analysis of the disciplinary societies, Deleuze did not refer explicitly to this reflection when developing a new understanding of the dividual, which is crucial for us:
> 
> > The disciplinary societies have two poles: the signature that designates the individual, and the number or administrative numeration that indicates his or her position within a mass […]. In the societies of control, on the other hand, what is important is no longer either a signature or a number, but a code: the code is a password, while on the other hand the disciplinary societies are regulated by watchwords (as much from the point of view of integration as from that of resistance) […]. We no longer find ourselves dealing with the mass/individual pair. Individuals become ‘dividuals’, and masses, samples, data, markets, or ‘banks’. (Deleuze, 1992: 5)
> 
> (p. 35-6)

# Divíduo como fração individual (em dado) na literatura posterior que parte de Deleuze

> It could be said that the dividual now replaces the individual just as data does the same with the masses. Nevertheless, according to the bibliography dedicated to the _Postscript_, there is not a doubling of the individual. **The dividual is understood within a multiplication, a fragmentation and explosion of data. When data goes to the individual, the process is ‘dividual’, a process of informational mediation, and when it goes to the mass, it becomes ‘banks’.** Lona Muir relates this distinction directly with the _dividuum_ when she says that Deleuze ‘speaks about the possibility of the breakdown of the individual by “dividing each within himself” in this new control paradigm’ (Muir, 2012: 267). In his turn, Williams defines the dividual as ‘a physically embodied human subject that is endlessly divisible and reducible to data representations via the modern technologies of control, like computer-based systems’ (Williams, 2005: 104). In this new situation individuals ‘lose their aura of distinctiveness because the selves are able to be classified (and thereby manipulated) by the very data which are supposed to serve individual needs’ (Williams, 2005: 108).
> (p. 36 )

# Sujeição social (individual) e servidão maquínica (dividual) em Lazzarato

> A second way to interpret the dividual in the societies of control is proposed by Lazzarato (2010), who states that **it is necessary to think the production of wealth in capitalism, as well as the subjective investment implied in it, according to two devices: social subjection and machinic enslavement.** Whereas social subjection equips us with an individual and individuated subjectivity, giving us an identity, a sex, a profession or a nationality, machinic enslavement would operate by desubjectivation, mobilizing semiotics that escape both representation and language, nevertheless being functional and operational. **‘Deleuze describes precisely the types of subjectivity over which this dual power apparatus exercises control. Subjection produces and subjects individuals, whereas enslavement would act on dividuals’ (Lazzarato, 2014: 26–7).** The dividual implies other compositions with machines. It neither makes use of them nor is opposed to them (as would be the case of the individual, respecting the division subject/object), but it is adjacent to machines.
> (p. 36 )

> In machinic enslavement, ‘human agents, like non-human agents, function as points of “connection, junction, and disjunction” of flows and as the networks making up the corporate, collective assemblage, the communications system, and so forth’ (Lazzarato, 2014: 27). **The dividual would not find its synthesis in a unified ‘I’, being just a broken piece of machinic agency. It would not be just a part of agency (as is the individual at the assembly line) but would instead find the components of its subjectivity (intelligence, affections, feelings, cognition, memory, physical strength) distributed into agencies that constitute huge social machines (enterprises, welfare state collective equipment, communication systems, etc.).**
> (p. 36-7 )

# Divíduo como propriedades estatísticas individuais agregadas como piscinas de risco no contexto de justiça biopolítica em O'Malley

> Finally, O’Malley (2010) interprets the problem of the dividual in light of ‘biopolitical justice’, that is, the administration of justice that is carried out through people’s codes and not through people themselves, which constitutes a kind of ‘simulated governance’.The codification mentioned by Deleuze is interpreted under the definition of risk.
> 
> > **For simulated governance, my primary importance is as part of a risky flow [...] It is risk that is governed ‘through’ the policing of my dividual, appropriately enough because as a property of distributions, risk does not apply to individuals so much as to dividuals in the form of those statistical properties of individuals that are aggregated to form risk pools. (O’Malley, 2010: 796)**
> 
> (p. 37 )

# A noção de divíduo biogenético (em Braidotti e Clough) a partir de Deleuze e Foucault

> Deleuze underlines the fact that the very notion of disease, key to biopolitics in Foucault and inspired in Canguilhem’s work, is partially detached from the explicit suffering present in the patient’s body. **What constitutes disease is risk, the very possibility of catching one, and this is the space in which a new kind of medicine is featured, where the diagnosis no longer has to do with what a body has but with what it could have grounded on its present determinations. This is what Braidotti and Clough have identified as a biogenetic dividual.** Quoting Massumi’s interpretation of Deleuze’s theory of affects, Clough affirms that **dividuals are ‘statistically configured in populations that surface as profiles of bodily capacities, indicating what a body can do now and in the future’. The dividual is defined by the ‘affective capacity of bodies, statistically simulated as risk factors’, that is, ‘apprehended as such without the subject, even without the individual subject’s body’ (Clough, 2008: 18).**
> (p. 37)

# Materialidade do risco em Rabinow e Rose a partir de Deleuze

> At the same time, according to Deleuze, **the risk is tied to a ‘dividual material’. This materiality of risks is explored by Rabinow and Rose (2006), who claim that today’s biopolitical approach should consider three dimensions: i) ‘one or more truth discourses about the “vital” character of living human beings’; ii) the ‘strategies for intervention upon collective existence in the name of life and health’; iii) the ‘modes of subjectification, through which individuals are brought to work on themselves, under certain forms of authority, in relation to truth discourses, by means of practices of the self, in the name of their own life or health, that of their family or some other collectivity’** (Rabinow and Rose, 2006: 197–8).
> (p. 37-8)

# Matéria dividual ~ vida molecularizada

> We propose that the Deleuzian ‘dividual matter’ corresponds to what Rose calls ‘molecularized life’ in the three levels mentioned. **It is a matter that forms part of the bodies but is not limited to them, as it is manifested in molecules, tissues, organs and all biological materiality dissociated from the bodies.** The clearest example is genetics, which has, in the last 50 years, focused on the theorization and on practices around DNA: a molecule that concentrates the key for genetic inheritance. **Indeed, there are numerous practices where one observes that ‘molecularization’ does not respond only to the fact that molecular biology has transformed what Western science understands as life, but also and above all to the description of a biomass ‘dissociated from the original bodies that once hosted them’** (Catts and Zurr, 2006: 1): genetic sequences, matter for assisted reproduction (sperm, eggs, embryos), stem cells kept in biobanks, tissues cultivated for medical or artistic needs, as in the case of bioart, etc.
> (p. 38 )

# Divíduo biológico como entendimento da individualidade corpórea disseminada em matéria biológica

> The biological dividual corresponds to the fact that **the body’s individuality, which is at the same time an indication of the constitution both of a person and of a subject, is found disseminated in a ‘biological matter’ related to, but not contained in, this individuality.** Another fundamental dimension of this biological matter is genomic data (McGonigle, 2019), which can be conceived in terms of the dividual, **not only because it belongs to that biological and non-individual matter, but also because it has a ‘shared nature […] with the wider genetic cohort, be they biological kin, tribe, ethnic group or nation’** (McGonigle, 2019: 1–2). **Hence genetic sequences, when considered as a part of a genome, are not only a sort of detachment of the biological individual (a body), but also a kind of collective good.** ‘Like the dividual self in an anthropological sense, genomic data gain their meaning and utility in reference to the wider genomic cohort’ (p. 2).
> (p. 38 )

# Divíduo biogenético ~ indivíduo separado da materialidade biológica. Captura da biomassa em um misto dividual-individual.

> Now we shall consider the modes of biological subjectivation proposed by Rabinow and Rose. We suggest that **this biogenetic dividual corresponds to an individual who, in order to become a subject, separates itself from his or her biological materiality, which can be managed according to diverse criteria. It is from such an exchange that a new self emerges, one particular ‘technology of the self’ (Foucault, 1988a), transformed into a ‘bio-technology of the self’.** In other words, it constitutes a new kind of the Foucauldian dividing practices. **The biomasses of the extended bodies are captured according to their potentialities and, as in the case of the profile that we will see in the next section, it is not about a substitution of a present body, but the constitution of a new reality with which it negotiates, a mix of individual and dividual.**
> (p. 38 )

# Perfis digitais como exposição parcial e relacional de si mesmo. [Perfil a priori].

> One of the aspects of digital profiles concerns the way each person manages his or her own public exposure in social media and digital platforms. This includes how much knowledge others will have about his or her privacy, intimacy and visibility. **In this sociotechnical management of one’s own exposure, each person deals with one’s intimacy not as an indivisible nucleus but as a dimension constituted by layers or portions that may be partially exposed and shared, according to modes and degrees differentiated from any others.** The profile does not have to be either reserved or extremely exposed regarding one’s intimacy or privacy; it can be both at the same time, possibly varying the degree of reserve or exposure, be it according to the moment or theme, or to those with whom interaction takes place. Besides, **a profile’s intimacy and privacy are not substantial but relational, resulting from a series of partial, non-coherent and non-unified connections. The trajectory of our profiles is modulated according to the interactions (partial connections) with other profiles and with the algorithms that manage the relations between them, and this will also interfere in the degree of visibility of such profiles for one’s connections, depending on the platform they exist in and on the ruling algorithm, widening or diminishing the potential margin to be affected (liked, shared, marked, indexed) by another. What the profile ‘is’, ‘can do’ or ‘does’ (its agency) is thus distributed to a diversity of heterogeneous actors – humans, non-humans, individual and collective.**
> (p. 39)

# Perfilamento como prática de antecipação potencial e performatividade a partir da captura de rastros digitais. [Perfil a posteriori].

> The many profiles and the many other actions that animate digital platforms, as well as the connections between them, leave traces about our lifestyles that feed huge and valuable data bases. **Such traces are the prime materials of the profile’s other face, a result of mining techniques and profiling, as the very term says. Such techniques involve the cross-refereeing and algorithmic analysis of such volume of data in order to build profiles that return to ourselves as personalized landscapes in very diverse realms. These profiles produce estimating patterns and anticipate potentialities**: consumption preferences, economic value, behavioral inclinations, professional capacities, virtual diseases, political preferences. **Such anticipated potentialities feature a high degree of performativity, acting directly on our actions and behaviors.** It is known that such algorithmic procedures operate as corporate decision mechanisms about the individuals’ potential qualities, with expressive effects over the opportunities that are offered to them, as well as its possible field of action in online and offline environments (cf. Bogard, 2006; Lyon, 2003; Noble, 2018). More recently, Shoshana Zuboff (2019) has expanded this debate into the broader field of future behavior markets prevailing in the surveillance capitalism operated by digital platforms like Google and Facebook.
> (p. 39-40 )

# Perfil estimado como conjunto de traços interpessoais

> It is worth noticing that **each profile is a set of traces that do not concern a specific individual but express correlations between countless traits of numerous individuals, being more interpersonal than intrapersonal.** Its main goal is not so much to know about an identifiable being but to use a set of personal data in order to act on similar ones (Bruno, 2013: 161). If we can think of individuality in this context, it is less about the past than about the immediate future, less of the order of interiority than one of exteriority, less regarding a singularity than the rules of similarity. **This algorithmic (in)dividuality has an all-relational topology, being an effect of complex correlations between massive volumes of dividualities. It works as an aggregate of infra and supraindividual features that project patterns applicable to individual behaviors, personalities and competences (see Rouvroy and Berns, 2013; Hildebrandt, 2008).**
> (p. 40 )

# Composição entre processos dividuais e individuais

> It is a composition, more than the substitution of one term for another, more than division or duplication of the individual. Of course, we still conceive ourselves and respond to others as individuals (institutionally, juridically, politically and even subjectively), but at the same time our technologically mediated practices increasingly involve dividual processes.
> (p. 40 )

# Mecanismos de conhecimento e controle algorítmico operam na composição

> Likewise, the very mechanisms of knowledge and algorithmic control operate with this composition, **capturing dividualities without, however, ceasing to appeal to the individual, be it as a target or as economically and juridically responsible for contracts, choices and actions.** The restrictions that certain social networks impose so that our profiles are faithful to our civilian identities, at the same time as they encourage the ceaseless production and sharing of any kind of data, trace and fragment, is a clear symptom of such composition.
> (p. 40 )

# Cidadania algorítmica em Cheney-Lippold

> In an attempt to understand identity mutations in the scope of digital culture, Cheney-Lippold (2017) proposes the notion of **‘algorithmic citizenship’, a new kind of citizenship, which he calls ‘_jus algoritmi_’, instituted by NSA’s massive digital surveillance.** Historically, the nation-state has demarcated the limit of citizenship according to blood kinship belonging (_jus sanguinis_) or territorial belonging (_jus soli_). Considering that on the internet it is not possible to identify people by means of blood kinship, of passports or birth certificates, how can one establish who is and who isn’t an American citizen? **According to the author, the NSA has a kind of algorithmic identification by implementing a ‘data-based assessment that assigned “foreignness” (and, in turn, “citizenship”) to targeted internet traffic, authorizing “legal” surveillance if a targeted data subject was deemed to be at least “51 percent foreigner”’ (Cheney-Lippold, 2017: 157). Such citizenship is not grounded on stable elements but instead on a constantly reevaluated interpretation of the user’s datafied behavior, whose ‘foreignness’ is calculated algorithmically from a set of variables, which are modulated by their online interactions (communication with an account/address/identifier supposedly associated to a foreign power or territory, metadata, etc.). ‘One day a user might be a citizen of the United States. Another day the user might be a foreigner’ (2017: 157).**
> (p. 40-1 )

# Coexistência dividual-individual nos aplicativos móveis, a noção de ser quantificado em Lindner

> Another dimension of the complex coexistence of the dividual-individual, both on the level of technologically mediated sociability and that of control mechanisms, can be found in digital mobile applications. Extensive literature on wearables and connected smartphone apps contains analyses on how self-tracking practices have modulated the relationship with oneself, with objects, and with the other in various domains of contemporary life (Lupton, 2016; Crawford et al., 2015; Gilmore, 2016; Neff and Nafus, 2016). From body care and health care to love and sexual encounters, from music to meditation, from photography to cognitive training and increased productivity, digital applications cover a huge range of social activities that are now shaped by a technically assisted management of oneself. **On the level of the ‘bio-technologies of the self’, this set of applications, wearables and sensor-software gadgets where individuals track and quantify their health, lifestyle, sports and wellness (the so-called quantified self) marks an ‘aretaic shift in biopolitical governance’ (Lindner, 2020). Aretaic refers not to ‘“life itself” but “life as it is lived”’, says Lindner in order to distinguish his position from that of Rose. This quantified self emerges from a ‘relationship between person and data’ that is similar to what we defined as the profile, now in biopolitical terms: on one setting we can find ‘the calculative space of software programming’, while in the other is ‘the realm of the practices of users who read these representations and modify their body behavior’ (Lindner, 2020: 80).**
> (p. 41 )

# Sujeito financial em Appadurai como composição dividual-individual

> Together with the biological dividual, the digital profiles and applications, **we can identify in what Appadurai calls a ‘financial subject’, regarding the subprime crisis of 2007–8, another emergence of the individual/dividual composition.** Appadurai (2015a, 2015b) revisits the issue of the dividual in the field of anthropology, as it has been shown before. However, he is less interested in a review of the category of person than in the exploration of the current forms of dividuation, especially in the field of contemporary finance and its processes of monetization.3 Appadurai’s perspective on the dividual is in dialogue with Deleuze’s work and with the context of the ‘new materialisms’ in science, media, and cultural studies. According to the author, **the ‘modern financial subject’ is characterized by a ‘infinite divisibility and re-combinability’ (2015a: 65). Scores, rankings, ratings, and profiles are the basis for a series of transactions, most of them invisible to the individual consumer. The primacy of the market’s dividuality in the financial market consists in the ‘slice and cut’ of such ‘profiles into numerous forms of speculative profit-making’ (2015a: 65).** The subprime system, mortgaging and derivative markets that were at the center of the 2007–8 crisis encouraged individuals to trust dividual transactions. According to Appadurai, a strange materiality was at play in this crisis, especially in the ‘housing mortgages’ system:
> 
> > The bizarre materiality of the mortgage-backed American house is that while its visible material form is relatively fixed, bounded, and indivisible, its financial form, the mortgage, has now been structured to be endlessly divisible, recombinable, salable, and leverageable for financial speculators, in a manner that is both mysterious and toxic. […] Mediated in the capitalist market, the house becomes the mortgage; further mediated, the mortgage becomes an asset, itself subject to trading as an uncertainly priced future commodity. Mediated yet again, this asset becomes part of an asset-backed security, a new derivative form, which can be further exchanged in its incarnation as a debt-obligation. (Appadurai, 2015b: 229–31)
> 
> (p. 41-2)

# Divíduo como miríade de relações. Divíduo e indivíduo como perspectivas sobre pessoas.

> According to what we have analyzed so far, it seems clear that **the dividual is not a new realm of subjectivity or personality, distinguishable from the individual, but a myriad of relationships: with profiles, with the ‘externalized’ biological materialities, with bio-quantified selves, with the financial derivatives or with the applications that feature an artificial intelligence complementing the human one.** If the contemporary dividual can be conceived in tension with the modern individual, it cannot be understood as a substitution, nor as division, not even as the individual’s duplication or partition. **As Raunig wrote about Strathern, ‘this is not a fixed distinction between “individuals” and “dividuals”, but rather between a dividual and an individual perspective on persons’ (Raunig, 2016: 165).**
> (p. 42 )

# Não coincidência entre pessoalidade e individualidade nos dados e biomassa

> Profiles are edited by individuals while at the same time they are redefined by the digital platforms. Biomasses such as a stem cell or a tissue are a part of an individual but treated beyond his or her actual body. Financial derivatives define a good, such as a house, that becomes a mortgage, then an asset, then an asset-backed security, then it is exchanged as a debt-obligation. **Digital applications are effective because they combine the data of millions of people in order to create an algorithmic singularization that is addressed to a ‘single’ person: _omnes et singulatim_, as in the famous Foucault lecture on governmentality (Foucault, 2000). It can be said that all such data and biomasses are part of the individuals without actually forming a part of each of them, because they describe and connect possible trajectories of multiple beings and generate an interaction between these trajectories and the actual indivisible being that continues to exist (the individual).** This is why the intimate domain of the individual and the body as the main possession of a political or juridical person are no longer useful to define the limits of the classic idea of subject. **Data and biomasses are the places where personality and individuality do not coincide.**
> (p. 42-3 )

# Governamentalidade algorítmica (Rouvroy e Berns) ~ Foucault, Simondon

> In the Foucauldian framework, the **algorithmic governmentality is ‘a certain type of (a)normative or (a)political rationality founded on the automated collection, aggregation and analysis of big data so as to model, anticipate and pre-emptively affect possible behaviours’ (Rouvroy and Berns, 2013: 10).** This process of three steps (dataveillance, datamining and anticipation of behaviors), while it refers to practices in social media, is **similar to the capture of the somatic potentialities in what we analyzed as the biogenetic dividual.**
> (p. 44)

> In the Simondonian framework, Rouvroy and Berns state that this governmentality evades the individual as its target. It aims at ‘merely infra-individual fragments’ and a supra-individual level where datamining correlates these ‘partial and impersonal reflections of daily existences’ (Rouvroy and Berns, 2013: 27). **The study of algorithmic governmentality ‘from a Simondonian perspective’ shows that ‘this mode of governance seems to rely on and target, no longer subjects, but relations as pre-existent to their terms’. The algorithmic governmentality is applied on individuation and not on individuals, producing ‘a rarefaction of subjectivation processes […] a difficulty to become subjects’ (Rouvroy and Berns, 2013: 16).**
> (p. 44)

# Devir dividual dos indivíduos em Stiegler

> This perspective is shared by Stiegler. For him **‘the dividual becoming of individuals’ consists in ‘subduing all the singularities into calculability’ (Stiegler, 2015: 111). The dividual must be understood as a process, the process of dividualization,** which means that the individuals are ‘disindividuated’, following another Simondonian term (Stiegler, 2015: 357), meaning a ‘disindividuation’ equal to the disintegration of the individuals (Stiegler, 2015: 286).
> (p. 44)

> We consider that these assertions can be nuanced. First, Stiegler seems to assume that the modern individual is something solid and robust that can be ‘disintegrated’ by digital practices, while the individual, as we saw, is already divided in himself and with the others. Second, we follow Yuk Hui’s remarks: ‘for Simondon, disindividuation is one of the phases of individuation, in which the preceding structure dissolves in favor of a new one (hence it is neutral and necessary)’, and not, as for Stiegler, ‘an inability to individuate both psychically and individually’ (Hui, 2015: 86).
> (p. 44 )

# Dividuação e governamentalidade algorítmica para as autoras

> For Rouvroy and Berns and for Stiegler, the dividual is a key concept to understand that in digital practices and biotechnologies there is another individuation process underway, different from the past, that produces an ‘interruption’ of the subjectivation process. **We hold instead that there is a _dividuation_, that is, a process of individuation working on the dividual (D’Amato, 2019), understood as the way individuals do undertake this process of subjectivation by means of digital and biogenetic practices. The _dividuation_ can be described as a kind of subjectivation not limited to the body and developing a sort of intimacy with technologies, and this is why the modern distinction between publicity, privacy and intimacy is today in question.** We share the critique of algorithmic governmentality as a technology of power trying to capture the _dividuation_ process for the capitalist process of accumulation, but not to the point of completely discrediting the subjectivation forces released by _dividuation_.
> (p. 44-5)

# Agência inerente ao perfilamento

> Besides that, this profusion of data feeds machinic processing increasingly more complex, involving dividual-individual relations and compositions between humans and non-humans. According to Raunig (2016), this machinic data can potentially be combined for the logistics of individual thing-movements and made accessible according to dividual logics. This underlines the importance of the machinic level, which constitutes part of the novelty regarding the history and the theory of the dividual and also the intersection between power and subjectivation. **If the dividual questions the equivalence between subjectivity, personality and individuality, the digital platforms not only multiply and redefine the person in terms of data but also include the objects as having an agency by means of their informational constitution.** Nevertheless, it is an agency driven to the imperative of profiling.
> (p. 45 )

# Personalização como generalização e definição dinâmica do eu em Lury e Day. Personalização como modo como dividuação interpela indivíduos a se tornarem pessoas.

> From the analysis of recommendation systems, Lury and Day (2019) argue that the **‘algorithmic personalization’ is a kind of individuation that involves not only the personal realm but is also generalization.** The recommendation systems, increasingly present in diverse types of applications, **classify people according to their belonging to categories that are constantly revised (different from the more stable classic statistical categorizations), at the same time as they address each one of us as ‘a you’. The individual is specified precisely and momentarily as ‘a you’, which the authors recognize as a dividual (Lury and Day, 2019: 15), yielding from a ‘recursive inclusion in types or classes’.** This way, ‘the you that is addressed is both specific and a you “that is like everyone else” (Chun, 2011), only more or less so’ (Lury and Day, 2019: 13). The authors conclude that ‘the familiar recognition that personalization seems to provide – knowing you better than you yourself do – should not be considered merely as a more precise form of individuation. To the contrary, personalization also constrains who and how we can be’ (Lury and Day, 2019: 3). In other words, **personalization is the way in which dividuation, as both individuation and subjectivation, ‘interpellates’ each individual to become a person.**
> (p. 45 )

# Divíduo como noção intrínseca à de indivíduo moderno

> Following a Foucauldian approach, we suggest that both the constitution and the crisis of the modern individual enable us to understand the current emergence of a culture of dividuality in terms that are historically more complex than the mere opposition between the fabrication of the disciplinary individual and the modulation of dividuals in control societies. The dividual already haunts the unity of the individual within Modernity itself. We also point out that the critique of Modernity has anticipated the limits of the notion of the individual, drawing attention to modes of sociability in which multiple levels of dividual exchange prevail over the Western opposition between the individual and society.
> (p. 45-6)

# Divíduo como campo de disputa pessoal e coletiva

> The theory of individuation as proposed by Simondon adds one more shift to the notion of dividual. In the initial sections of the article, we carried out a historical-anthropological shift, showing both the presence of the dividual beyond the societies of control and the persistence in these of the modern individual. In the last section, the notion of individuation allows us to explore the dividual not only as a product of power mechanisms in control societies and in algorithmic governance, but also as a dimension of processes of subjectivation involving the experience of being an individual, as well as human-machine compositions.5 A dimension that constitutes a fold with which one enters in relation, a matter that yields to an individuation and a subjectivation that is not necessarily reduced to that which is prescribed by the algorithmic governmentality program. **This perspective aims to open up future investigations into the field of studies around the dividual, in which it becomes a field for dispute, both personal and collective, and not an exclusive site for capture by power and capitalism.**
> (p. 46)
