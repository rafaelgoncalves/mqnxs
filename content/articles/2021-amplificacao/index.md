---
title: A amplificação nos processos de informação
category: fichamento
date: 2021-09-28
path: "/a-amplificacao-nos-processos-de-informacao"
tags: [Gilbert Simondon, transdução, modulação, fichamento, tecnologia, amplificação, informação]
featuredImage: "triodos.jpg"
srcInfo: <a href="https://commons.wikimedia.org/wiki/File:Triody_var.jpg">Retirado de wikimedia</a>
published: true

---

Fichamento da tradução do texto "A amplificação nos processos de informação"[^1] apresentado por Gilbert Simondon no Colloque de Royaumont de 1962 e publicado em 2010 na coletânea _Communication et information: cours et conférences_ (Chatou: Les Éditions de la Transparence).

# Informação como operação não como coisa

> _Ser ou não ser informação_ não depende somente de características internas de uma estrutura; a informação não é uma coisa, mas a operação de uma coisa ingressando num sistema, e nele produzindo uma transformação.
> (p. 283)

> A informação não pode ser definida fora desse ato de incidência transformadora e da operação de recepção. 
> (p. 283-284)

# O receptor (e não o emissor) define informação

> Não é o emissor que faz de uma estrutura uma informação, pois uma estrutura pode se comportar como informação com relação a um receptor dado, sem ter sido composta por um emissor individualizado e organizado; impulsões provenientes de fenômenos aleatórios podem disparar um dado receptor, da mesma forma como as provenientes de um emissor (cf. o sinal interferente, o ruído, a interferência e o QRM).
> (p. 284)

> A metaestabilidade do receptor é a condição de eficácia da informação incidente
> (p. 283)

![Estados metaestável, instável, estável](estabilidade.png)

1 = metaestável; 2 = instável; 3 = estável

# Modificação da realidade como função da informação

> Em contraposição, os sinais emitidos por um emissor resultam apenas na degradação de sua energia portadora, se eles não encontrarem um ou mais receptores nos quais eles desempenhem um papel eficaz, determinando as mudanças de estado que não poderiam ocorrer sem a incidência dos sinais:
> a realidade _local_, o receptor, é _modificado em seu devir_ pela realidade _incidente_, e é essa modificação da realidade local pela realidade incidente que é a função da informação
> (p. 284)

# Receptor como realidade indeterminada

 > É virtualmente _receptora_ toda realidade que não possui inteiramente nela mesma a determinação do curso de seu devir. 
 > (p. 284)

> Essa condição é realizada se o receptor não é completamente um sistema, isto é, se, de um lado, ele possui um nível de organização elevado (com isolamentos internos e uma distribuição não-aleatória de seus elementos, permitindo-lhe reter uma energia potencial capaz de operar transformações futuras), e se, de outro lado, a mudança de estado possível pelo jogo da energia potencial não depende de fatores internos, locais.
> (p. 284)

# O receptor é autônomo energeticamente, mas heterônomo em relação à causa

> O receptor é uma realidade autônoma, do ponto de vista energético, pois ele possui a energia potencial (energia de estado) capaz de garantir transformações, de as alimentar; mas o receptor não é efetivamente um receptor, se não for heterônomo do ponto de vista da causa que dispara as transformações – essa causa disparadora sendo um aporte de energia incidente, eventualmente tão fraco quanto se queira.
> (p. 284)

# Receptor como quase-sistema

> Por isso (autonomia energética e heteronomia da disparação das transformações por mudança de estado), um receptor é um _quase-sistema_, um sistema de entrada.
> (p. 284)

# Informação não é um acoplamento emissor-receptor

> Uma tal mudança de estrutura [no triodo, fruto da impulsão incidente] não implica troca reversível de energia entre os dois sistemas, mesmo fracamente acoplados: a incidência da informação não é um acoplamento.
> (p. 285)

# Metaestabilidade do receptor como condição de eficácia da informação (início no receptor de transformações entre estados metaestáveis)

> O receptor de informação é uma realidade que possui uma zona mista de interação entre as estruturas ou energias locais e os aportes de energia incidente; se essa zona mista de interação estiver em relação com a existência de estados metaestáveis, ela conferirá à informação incidente sua eficácia, isto é, a capacidade de iniciar, no receptor, transformações que não seriam produzidas nele espontaneamente apenas pelo jogo de fatores locais.
> (p. 285)

# Metaestabilidade promove distinção funcional entre entrada e saída como extremos de uma transformação não-reversível

> É a metaestabilidade do estado do receptor que mantém a distinção funcional entre entrada e saída; antes de serem organologicamente distintas, entrada e saída são os termos extremos de uma transformação não-reversível, na qual a entrada é a operação de informação por incidência em uma realidade em estado metaestável, e a saída é o efeito produzido no final da transformação da energia potencial desse estado metaestável, desencadeada pela incidência de informação.
> (p. 285)

# Amplificação transdutiva como modo mais elementar de operação de informação

Transdutiva ou transdutora? No original: _transdutif_

> O modo mais elementar da operação de informação que coloca em funcionamento a metaestabilidade do receptor é a amplificação transdutiva.
> (p. 285)

# Transdução (mudança) != transmissão (não muda o regime de equilíbrio)

Ex: germe de cristal em solução supersaturada, influxo nervoso, explosão

> É essa transferência gradual, alimentada energeticamente pela mudança de estado no próprio local onde se produz a transformação, que se pode nomear _transdução_. Esse processo, tomado aqui num caso elementar, é distinto da _transmissão_ de uma informação sob forma de propagação de uma perturbação mecânica ou eletromagnética; na transmissão, o campo atravessado não é receptor, ele não substitui energeticamente a incidência de informação e não muda o regime de equilíbrio.
> (p. 286)

# Transdução como reação em cadeia (aditiva ou multiplicativa)

> Os modelos tecnológicos que empregam um processo transdutivo supõem a possibilidade de uma reação em cadeia a qual se propaga gradualmente; segundo a estrutura inicial mais ou menos limitante, a propagação ganha um aspecto de transferência aditiva ou multiplicativa, segundo o modelo da linha, da superfície e do volume, sendo a entrada somente um ponto.
> (p. 287)

# Informação pode conter transmissão como expressão da mudança de estado do elemento anterior

> O fenômeno permanece informacional, mesmo se a ligação entre os elementos se faz por intermédio de uma transmissão, desde que essa transmissão seja apenas a expressão da _mudança de estado_ do elemento anterior.
> (p. 287)

> Assim, um fogo na floresta se propaga transdutivamente, mesmo se cada árvore queimada emita ao seu redor pinhas e faíscas que vão comunicar o fogo às árvores vizinhas: é preciso que a árvore em chamas tenha mudado de estado, tenha entrado em combustão com o ar ambiente, para emitir assim os elementos que transportam o fogo adiante; e é preciso que esses elementos transmitidos sejam recebidos pelas outras árvores em estado metaestável (secas, arejadas), para que a operação transdutiva continue; as transmissões intermediárias entre os elementos transdutivos, mudando de estado, autorizam, apenas, uma maior distância entre elementos; eles podem também, se acumularem energia, causar um atraso na propagação, conservando mesmo assim o esquema fundamental da amplificação transdutiva.
> (p. 287)

# Transdução nos processos psicossociais (psicológico/individual -> social/coletivo)

> Esse modelo se aplica aos processos psicossociais; em certo sentido, ele permite defini-los, pois os fenômenos psicossociais são psíquicos, por terem como entrada uma incidência de tipo psíquica, individual; mas eles são sociais, pois se propagam por amplificação transdutiva, o que os faz passar da dimensão individual de entrada à dimensão coletiva de saída.
> (p. 287)

# Fenômenos puramente psíquicos

> Com efeito, os fenômenos puramente psíquicos são aqueles que se produzem no indivíduo sem modificar seu estado de equilíbrio, sem desencadear nele uma transformação que se traduza por uma atitude percebida como nova e significativa para um outro indivíduo.
> (p. 287-8)

# Informação no contexto social

> É informação a incidência que, em um grupo dado, conduz a uma mudança de equilíbrio em um certo número de indivíduos, que, pelo próprio resultado de sua mudança, desencadeia uma mudança em outros indivíduos potencializados de maneira análoga. A condição primeira é a existência, em um grande número de indivíduos, de uma metaestabilidade inicial, predeterminando seletivamente a categoria de incidências que podem desempenhar um papel eficaz de desencadeamento. 
> (p. 288)

# Estados tensos como metaestáveis. Alienação como condição de fenômenos psicossociais (?)

> Os estados tensos – medo, inquietude, esperança de transformação – são, muito geralmente, o equivalente psíquico dos regimes físicos de metaestabilidade; o estado de alienação coletivamente experimentado é a condição de possibilidade dos fenômenos psicossociais, pois eles alimentam energeticamente a transdução amplificadora pela qual se opera [no caso físico] a passagem da incidência do germe cristalino microscópico à mudança de estado macrofísico do conjunto da solução supersaturada ou sobrefundida. 
> (p. 288)

# Limiar desencadeante (tudo ou nada) e período refratário (informação não é eficaz)

> Um caráter essencial da propagação transdutiva é a existência de um limiar de desencadeamento e de um caráter quântico de funcionamento, procedendo por tudo-ou-nada e implicando, após cada mudança de estado, um período refratário (ou tempo de recuperação [_recovery time_]), durante o qual nenhuma incidência de informação é eficaz.
> (p. 288)

# Constante de tempo nos fenômenos psicossociais

> Esse _recovery time_, bastante variável segundo os domínios, é a característica mais importante a se conhecer, para a previsão das mudanças nos processos psicossociais; ele permite definir uma verdadeira constante de tempo de cada efeito psicossocial, que governa e condiciona todos os processos mais complexos, implicando propagações transdutivas: autorregulação, adaptação, ajustamento de níveis, oscilações. O tempo de recuperação, para a fibra nervosa, é da ordem do milésimo de segundo; nos fenômenos psicossociais (atitudes étnicas, sentimentos nacionais), ele pode chegar a semanas ou mesmo anos.
> (p. 288)

# Fenômenos individual, psicossocial e social

> Entretanto, os fenômenos psicossociais são geralmente mais rápidos do que os fenômenos sociais, pois eles não exigem, para existir, uma modificação das condições de base (produção, desenvolvimento industrial, população, tipo de educação), mas apenas um acionamento de reservas energéticas já potencializadas, imediatamente disponíveis; no domínio psicossocial, “tudo é possível”, quando o estado das tensões permite uma propagação transdutiva; mas as tensões são seletivas; nem toda incidência é, para elas, uma informação eficaz, determinante; além disso, a incidência informacional deve se apresentar 
> no momento certo, pois a ativação de tensões não é indefinidamente disponível: a eficácia da informação requer condições de estrutura (de conteúdo) e condições de oportunidade da incidência. “Tudo é possível” no domínio psicossocial, mas não em qualquer lugar, em qualquer momento ou de qualquer forma. Além disso, nem tudo é psicossocial: a realidade psicossocial, precisamente por se alimentar de energias acumuladas, as utiliza e as esgota; ela é um intermediário necessário entre o individual e o social, mas esse processo de passagem, de mudança de nível, de passagem do microscópico ao macroscópico é, por sua própria natureza, momentâneo. Não há continuidade, se ele não se traduz por um efeito institucional no nível de estruturas sociais. 
> (p. 288-9)

# Fenômenos psicossociais são feitos por mudanças de atitude não de atitudes

> Os fenômenos psicossociais são essencialmente informacionais. 
> Eles não são feitos de atitudes, mas de mudanças de atitudes; uma atitude, como fenômeno psicossocial, é uma relação interindividual em transformação.
> (p. 289)

# Amplificação moduladora como domesticação da popagação transdutiva

> O esquema da amplificação moduladora é obtido domesticando-se a propagação transdutiva; isto é, dominando-a e a alimentado localmente, para fazê-la produzir e trabalhar em condições regulares.
> (p. 289)

# Modo de funcionamento da amplificação moduladora

```
                entrada
                    |
                ___ v ___
               |         |
alimentação -->| efetor  |---> saída (carga)
               |_________|
```

> Na transdução, o fenômeno de basculamento, de passagem da metaestabilidade à estabilidade, muda constantemente de suporte, ao avançar; na modulação, o local de passagem do estado energético metaestável ao estável é fixo: uma fonte de energia potencial (_power supply_) está aguardando e pode agir por meio de um efetor sobre uma carga; a energia não é mais contida em estado difuso na metaestabilidade do estado inicial do receptor, ela é contida por um dispositivo de alimentação que forma um quase-sistema com o efetor, agindo sobre a carga;
> enfim, _a entrada_ intervém como incidência no hiato desse quase-sistema (preferimos esse termo àquele de Bertalanffy: sistema aberto); entre _alimentação_ e _saída do efetor sobre a carga_, existe a _entrada_.
> (p. 289)

> A entrada age como um isolamento variável que se interpõe entre a fonte de energia e a carga; esse isolamento não acrescenta energia ao quase-sistema, tampouco a suprime; ele governa, controla o regime de mudança de estado de energia potencial, a cada instante, seja por escolha entre dois valores (tudo ou nada, regime pleno ou nulo, abertura ou fechamento do circuito reunindo a fonte de energia e a carga), seja por escolha entre uma infinidade de valores entre um máximo e um mínimo, entre a saturação e a interrupção.
> (p. 289)

# Modulação como governação do regime de transformação de energia potencial em trabalho

Na modulação, continua a irreversabilidade, mas não existe tempo refratário.

 > O modulador está constantemente em vias de decidir a partir da incidência de informação; essa incidência de informação governa, instante por instante, o regime de transformação de energia potencial em trabalho.
 > (p. 290)

# Modulador como equivalencia entre termos extremos num local privilegiado

> O modulador é um amplificador sem iteração ou processo de multiplicação, pois ele coloca em jogo uma relação entre termos extremos de uma série energética incidente e de uma série local, ao realizar, num espaço privilegiado, uma equivalência entre esses termos extremos
> (p. 290)

 > o modulador é um dispositivo que permite a irreversibilidade, não graças à passagem da metaestabilidade à estabilidade do receptor, mas porque uma diferenciação funcional e organológica permite ao termo extremo de uma série incidente equivaler, num pequeno espaço privilegiado, ao termo inicial de uma série local, eventualmente muito mais potente que a série incidente.
 > (p. 291)

# Equivalência no espaço privilegiado entre um termo final fraco e um inical potente

 > A criação dessa equivalência no espaço privilegiado de interferência funcional resulta da colocação, em presença, de um termo final fraco e de um termo inicial potente; a equivalência é possível, pois o termo inicial da série local está ainda muito próximo do seu ponto de origem; pôde, até então, fornecer muito pouco trabalho, adquiriu muito pouca energia cinética: a queda de potencial da qual se beneficiaram os elétrons saídos do cátodo é muito fraca no nível da grade de comando; quanto mais fraca for essa transformação, da energia potencial local em energia cinética e em energia atual (juntas/somadas [ajout]) no espaço privilegiado, mais é elevada a eficácia da incidência; é por isso que se emprega, nas válvulas de cátodo quente, uma polarização negativa da grade.
 > (p. 291)

# Amplificação moduladora como regeneração

> Pode-se dizer que o receptor é mais jovem em seu devir do que a informação incidente; a informação é o 
 > último termo de uma série; a modulação amplificadora em um relé é também uma troca entre ciclos de existência; a realidade _incidente_ é mais velha do que a realidade _local_. Nesse sentido, pelo aporte de energia potencial, o relé realiza uma regeneração. A amplificação moduladora é um recomeço, a passagem a uma nova etapa.
 > (p. 291-2)

# Nem toda modulação é uma amplificação

> Pode-se realizar montagens moduladoras não-amplificadoras, quando se trata somente de controlar uma certa espécie de energia por meio de uma outra, que age como portadora de informação; assim, a modulação anódica de um emissor de ondas hertzianas não é amplificadora; aqui, a energia fornecida pela corrente de baixa frequência se acrescenta parcialmente àquela fornecida pela fonte de alimentação anódica do nível modulado pelo ânodo; os moduladores de díodo não são amplificadores; não se pode, ademais, propriamente falando, considerá-los como relés (ver Stevens e Tucker, _Modulators and Frequency Changers_). Seria mesmo possível modular uma energia, fazendo variar a adaptação de impedância da carga, o que equivaleria a modular por controle do rendimento; esses sistemas não são amplificadores, pois eles não utilizam o termo extremo final da série de transformações incidentes para controlar, no seu nascimento, o termo extremo inicial de uma nova série local. 
> (p. 292)

# Amplificação moduladora como estabilização

> O esquema de amplificação moduladora pode ser transposto para o domínio biológico e também para o domínio social; ele permite dar conta dos funcionamentos nos quais um processo é estabilizado, apesar de variações aleatórias da carga, por uma informação que exprima o nível do efeito sobre a carga e controle o regime do modulador em função desse nível.
> (p. 292)

# A autorregulação como modulação

> A autorregulação não passa, aliás, de um caso particular do funcionamento da amplificação moduladora.
> (p. 292-3)

> Esse caso é interessante, pois produz efeitos de estabilização na transformação [devenir], apesar das flutuações do nível de carga; esses efeitos de homeostase também são encontrados nos organismos, sejam eles indivíduos vivos ou os corpos sociais.
> (p. 293)

> Aqui ainda intervém, em cada caso, uma constante de tempo característica do domínio; a autorregulação só pode ocorrer, se o retorno de informação da saída para a entrada é curto com relação à duração média de uma perturbação aleatória da carga que se deve corrigir; quanto maior for a defasagem entre a perturbação e o retorno da informação, mais será necessário reduzir o coeficiente de amplificação do modulador, para evitar a entrada em um regime de instabilidade.
> (p. 293)

# Propriedade fundamental da amplificação moduladora é a redução de regime

> Mas, importa notar que a propriedade fundamental da amplificação moduladora não reside na autorregulação possível: ela reside no fato de que a regulação, autônoma ou heterônoma, _só se pode efetuar por uma redução do regime_ que torna a informação eficaz.
> Essa redução do regime é realizada, nas montagens técnicas, pela polarização do modulador que, na ausência de qualquer sinal, reduz seu regime a uma fração de seu pleno regime de saturação.
> (p. 293)

# Modulação autoritária e transdução emancipatória (no domínio social)

> Um modo de amplificação existe elementarmente na estrutura de conflito: quando diversos germes de estruturação são lançados juntos num meio metaestável, cada um deles começa a induzir uma mudança que vai até os limites do campo disponível e que pode mesmo reconverter em benefício de sua própria estrutura, incorporar ao campo de sua própria transdução, o terreno que já foi estruturado por outros germes; nesse caso, todo o aporte constituído pelos diferentes domínios diversamente estruturados se torna um campo de expansão estrutural para o germe portador a estrutura que esgota mais completamente as possibilidades de mudança de estado da realidade metaestável; estruturações prévias fracas (que não esgotam as possibilidades energéticas) reservam um aporte a estruturações fortes. Particularmente, estruturas sociais antigas de modulação, mantidas por uma estrutura autoritária que emprega apenas uma parte da energia disponível e obtém um fraco rendimento, oferecem um terreno de expansão a uma estruturação transdutiva intensa, que empregue mais profundamente as reservas de energia, alcançando um resultado mais estável. O jogo, onde cada jogador porta a contribuição de sua aposta pessoal, é um modelo simples de amplificação transdutiva: a aposta está em estado
> metaestável, durante a partida; a guerra é de mesma espécie, particularmente quando ela se torna um vasto conflito: o vencedor encontra um vasto campo de expansão a estruturar, se as metaestabilidades que nele são contidas não forem refratárias aos germes estruturais que ele porta; no jogo, a aposta, sendo em dinheiro, é sempre homogênea às possibilidades estruturais do vencedor; no conflito, o vencedor nem sempre pode estruturar transdutivamente o domínio que ele conquistou; nesse caso, ele se contenta em modulá-lo autoritariamente, impondo-lhe as estruturas que ele já utilizou em seu direito e em suas instituições; mas ele chega assim a uma inibição, a um fraco rendimento, e as energias não empregadas constituem uma metaestabilidade residual que permite a constituição de movimentos psicossociais que interrompam a modulação assimiladora e autoritária. O acesso à independência dos povos colonizados apresenta essa passagem da modulação à transdução.
> (p. 292-293)

# Normas como polarização

> A unidade do grupo social repousa sobre a homogeneidade de 
> normas de ação; ora, as normas não são, na grande generalidade dos casos, sinais ou conteúdos que desencadeiam por si próprios uma ação determinada, em um momento determinado, como um programa; antes, as normas são uma escala de valores a qual constitui a polarização prévia de cada membro do grupo, tornando-o capaz de apreciar uma informação determinada, um esquema de conduta, como uma grandeza positiva ou negativa com relação a essa polarização inicial.
> (p. 293-4)

# Moral e religião

> Nesse sentido, uma moral se distingue de uma religião, por esta comportar um código, um conteúdo de sinais de ação, um programa de ação ritualizado, ao passo que uma moral é uma polarização sem programa, fornecendo uma escala de valores em cada circunstância, mas sem sequência programada, sem ritualização. Por sua própria natureza, sendo um programa, a escala de valores religiosa é rígida, fixa, enquanto aquela das morais é variável, podendo se modificar em função do regime médio dos acontecimentos.
> (p. 294)

# Modulação como controle do atual em função de seu passado

> Toda polarização, como todo sinal de informação recebido por um amplificador-modulador, resulta de um acontecimento ou de uma série de acontecimentos passados, parcialmente arcaicos no momento em que o modulador opera.
> (p. 294)

> Essa estrutura é um controle do regime atual da ação da energia potencial sobre uma carga em função de seu passado; e esse controle é tornado possível por uma diminuição da ação da energia potencial, por uma divisão da potência utilizável, graças à polarização que age como inibidor prévio; a incidência de um elemento do passado (informação) na ação presente do quase-sistema necessita, para ser eficaz, de uma diminuição da utilização da energia e do nível de atividade total.
> (p. 294)

 > O ponto ótimo de estabilidade homeostática é distante do máximo das possibilidades de funcionamento do modulador e do pleno emprego do rendimento da energia potencial da fonte, devido às condições de polarização.
 > (p. 294)

 > No domínio social, religiões e morais operam um regime permanente de limitação da atividade.
 > (p. 294)

# A amplificação organizadora como modulação da transdução

> Um regime comum de transdução e de modulação pode se estabelecer, quando as decisões sucessivas da transdução, em lugar de somente se desencadearem umas às outras, numa perpétua instantaneidade, se ordenam em série por meio de uma autorregulação: o recrutamento é, assim, orientado para um fim, em lugar de ser indefinido; cada decisão sucessiva leva em conta o efeito de decisões precedentes: o domínio da transdução intervém como carga sobre a qual se opera um trabalho na sequência de decisões sucessivas: há como
> que uma previsão do trabalho a ser realizado desde o início da transdução. A série transdutiva de decisões é modulada por uma informação tomada sobre o conjunto do campo, que intervém, assim, como uma totalidade no percurso que o recobre e o transforma.
> (p. 294-5)

# Exemplo: visão binocular

> A tensão de incompatibilidade entre as duas imagens retinianas se torna seriação, organização compatibilizadora e totalizante, princípio dimensional de ordem mais elevada. Uma tal operação é uma resolução de problema. Aqui, a informação não é uma estrutura dada, mas um desenquadramento de estruturas, entre estruturas vizinhas, uma quase-identidade que exclui, no entanto, a superposição e a identificação.
> (p. 295)

# Informação como exigência na organização

 > A informação intervém como exigência, problema colocado, sistema de compatibilidade a ser inventado por passagem a uma axiomática dimensional mais elevada; é essa exigência que, aqui, controla; ela não é uma estrutura que se faz transmitir por uma energia nova, como no modulador polarizado.
 > (p. 295)

# Transdução na organização

> O processo transdutivo tem como efeito essencial permitir a mudança de ordem de grandeza de uma modificação, por passagem do elementar ao coletivo (o que chamamos de fenômeno psicossocial de amplificação por recrutamento, por meio da propagação gradual dos basculamentos em um meio metaestável); ele cria a homogeneidade final de todo o campo percorrido e realiza a ontogênese de uma macroestrutura, a partir de uma microestrutura-germe e de uma energia de estado macrofísico.
> (p. 295)

# Modulação na organização

> A modulação se exerce segundo a via complementar e inversa: estruturas
> macrofísicas carregadas por uma energia fraca governam o devir de uma população perpetuamente nova de elementos microfísicos determinados, muito próximos da origem de seu percurso livre, como a população de elétrons provenientes do cátodo quente da válvula a vácuo e submetidos, ao mesmo tempo, à ação do campo anódico (que continuará a se exercer de modo cada vez mais eficaz, ao longo do deslocamento) e do campo proveniente da grade, consequência derradeira de eventos chegando ao termo de sua realização
> (p. 295-6)

# O modelo do triodo é o análogo funcional da sociedade

> O controle da realidade nascente pela realidade antiga define o modo social do processo informacional: o modelo triódico [triodique] é o análogo funcional de uma estrutura social.
> (p. 296)

# Possibilidade de organização = compatibilidade entre transdução e modulação (síntese real)

> Ora, há possibilidade de organização, quando há compatibilidade entre esses dois processos, segundo um modo de _síntese real_.
> (p. 296)

> A possibilidade aparece, quando o controle é exercido, não por uma sóestrutura, mas por um grupo; em outras palavras, por um nó de tensão entre duas ou mais estruturas.
> (p. 296)

# Organização como ação que possui significação no todo (implica memória e previsão)

> A tomada de forma é realmente ininterrupta, feita de etapas encadeadas, mas essas etapas são simultaneamente significativas, do ponto de vista da organização; elas respondem a um problema modulador único, inteiramente dado, e, enquanto etapas de uma ação organizada, elas podem ser submetidas a uma transdução reversível; é na totalidade de seu encadeamento que uma ação possui sua significação, segundo uma forma de complexidade que implica memória e previsão.
> (p. 296)

# Organização vital em Lamarck

> Deve-se destacar que existem condições comuns à consciência e à vida: o vivo está em relação com seu meio e, segundo a hipótese de Lamarck, uma invenção evolutiva se faz por incorporação ao organismo, sob formas e funções novas, de sequências de eventos que estavam antigamente sob a dependência do meio; o problema vital é resolvido por desenvolvimento do organismo, que absorve e estabiliza operações anteriormente exteriores e aleatórias; aqui o problema colocado não é apenas feito de termos interiores ao ser vivo, mas de termos interiores e exteriores em relação complexa de correlação e de incompatibilidade parcial.
> (p. 296)

# Organização na consciência

> Da mesma forma, a consciência não é feita apenas de conteúdos que a reenviam a ela mesma; ela consiste na correlação entre termos autógenos e alógenos; é esta a relação fundamentalmente problemática. A resolução do problema não é um simples arranjo de termos autógenos, mas um crescimento do campo da consciência, por meio da nova organização descoberta, tornando compatíveis, em um sistema dimensional mais vasto, os elementos antigos de interioridade e de exterioridade. A organização não é uma obra autárquica, nem para o organismo, nem para a consciência.
> (p. 296)

# Integração provável entre vida e consciência

> Uma analogia funcional tão profunda deve conduzir a não separar vida e consciência como duas ordens estrangeiras [_étrangers_] uma à outra. É provável
> que, em certo nível de integração, problemas vitais só possam ser resolvidos por modalidades conscientes; nesse sentido, a consciência seria uma função vital, porque ela seria fonte de amplificação organizadora; em sentido inverso, nada me permite dizer que aspectos elementares da vida não sejam dados de consciência.
> (p. 296-7)

# Alargamento das noções de social e psicossocial para além de humanos

> Mas uma tal hipótese, que levaria a considerar a organização como a forma mais complexa de amplificação, incorporando ao mesmo tempo um processo transdutivo e um processo modulador, e os correlacionando funcionalmente, levaria também à consideração da resolução de problemas vitais e psíquicos (portanto, à evolução e à invenção) como supondo, a título de base, a possibilidade anterior de aspectos sociais e psicossociais. Essa afirmação pode parecer paradoxal, mas ela não o é realmente, se se realiza uma extensão da noção de relações sociais: moléculas, células elementares, podem estar, umas com relação às outras, numa relação de tipo social, implicando controle, modulação, redução da atividade; o fenômeno psicossocial é, então, somente a propagação transdutiva de uma perturbação que, tendo realizado uma mudança de equilíbrio no nível elementar, ressoa no nível coletivo por recrutamento de todos os elementos
> (p. 297)

> Talvez as grandes etapas filogenéticas tiveram por condição fenômenos sociais e psicossociais; assim, a noção de sociedade, inicialmente definida para o homem, se estende às espécies animais; mas ela pode ser mais completamente generalizada, englobar relações entre vegetais e mesmo entre moléculas; da mesma forma, a relação de tipo psicossocial, definida inicialmente nas relações interindividuais humanas, é suscetível de receber uma generalização. 
> (p. 297)

# Organização como síntese de fenômenos social e psicossocial

> Então, poderíamos considerar que existe uma relação dialética entre a relação psicossocial, a relação social pura, e a relação de organização, aparecendo como síntese das duas primeiras: transdução, modulação e organização seriam os três níveis do processo informacional de amplificação por recrutamento positivo, por limitação e por descoberta de um sistema de compatibilidade.
> (p. 297)

# Evolução dialética onde a escassez informa um modulador social (?)

> Comparando-se um grupo social a um modulador, pode-se explicar parcialmente, por motivações sociais, as escolhas sucessivas das direções de busca para as invenções organizadoras: em cada época, e em cada civilização, é o tipo mais significativo de escassez sentida pelo modulador social que motiva a busca desinteressada. A Antiguidade greco-latina das civilizações ocidentais manifesta essencialmente uma escassez de informações incidentes; ela elaborou filosofias do saber, da sabedoria, representações do mundo, metafísicas. Ao contrário, após a Renascença, o surgimento das ciências exatas satisfaz a falta de saber; mas, entre a informação e a ação eficaz sobre a carga, que é o mundo a transformar, existe um vasto hiato: a escassez é aquela de uma ação eficaz, tão segura e ordenada quanto o saber científico; assim, nasce a busca por uma metodologia técnica e humana de ação eficaz; o objeto mesmo é analisado como o resultado de uma ação construtiva, procedendo por etapas transdutivas ordenadas e operando uma transferência de força, de causalidade, de apoio ou de evidência; ele é uma longa cadeia, resultado de uma operação e possível instrumento futuro de operação. A dedução é um
> modo controlado de transdução sobre símbolos, enquanto os métodos analógicos, paradigmáticos da Antiguidade, são de tipo modulador. Enfim, a partir do século XIX, o desenvolvimento das técnicas de realização pôs fim à escassez metodológica de realização operatória: é então a energia potencial que falta para esses vastos conjuntos possuidores de saber e método operatório; a busca do power supply nas dimensões das sociedades do século XIX conduziu o pensamento filosófico à descoberta das realidades motoras: motivações humanas do socialismo, fontes naturais dos fisiocratas, exploração da natureza pelos homens em sociedade do marxismo. A filosofia do século XIX é, sob suas diferentes formas, uma filosofia da energia potencial; ela é a última etapa de uma evolução dialética. Com a noção de organização, a nossa [filosofia] começa um ciclo novo, onde se manifesta, como na Antiguidade, uma busca primordial de saber, de informação que se possa inserir na entrada do modulador social ou psicossocial (coletivo).
> (p. 297-8)

# Relação dialética como progresso transdução-modulação-organização

>Se os três modos possíveis de amplificação são característicos de três tipos de relações em relação dialética, podemos perguntar, de início, se a relação dialética não é sempre o progresso, em três etapas, de um processo de amplificação inicialmente transdutivo, depois modulador e, enfim, organizador, devendo-se essa sucessão à mudança do domínio que realiza o exercício de um determinado tipo de amplificação.
> (p. 298)

# Transdução (+) e modulação (-)

> Com efeito, a amplificação transdutiva é essencialmente positiva; ela não supõe nem isolamento, nem limite; ela é o modelo da operação positiva, que se alimenta dela mesma e se propaga pelo resultado instantâneo de seu próprio exercício: ela se afirma, porque causa perpetuamente sua própria capacidade de avançar; ela é autoposição, e não é autolimitada. Ao contrário, a modulação supõe isolamento entre seus órgãos fundamentais: fonte de energia, entrada e saída do modulador; ela não é uma propagação autocondicionada, mas uma operação localizada [à poste fixe], que só é possível por uma inibição da atividade espontânea possível: ela se inscreve negativamente na atualização de uma energia potencial, e diminui o rendimento da transformação possível.
> (p. 298)

# Modulação como submissão do novo ao passado (condicionamento)

> Em lugar de ser, como a transdução, movimento rumo à zona rica em energia potencial, a partir do domínio já estruturado, a modulação realiza uma submissão de uma série nascente do devir ao ato final de uma série anterior; ela determina o novo segundo a estrutura do antigo (ação de relé), o condiciona.
> (p. 298)

# Modulação como dominação

> Enquanto a transdução é orientada para o futuro, a modulação é uma vitória do antigo sobre o novo, uma reciclagem da estrutura antiga. A modulação é o ato crítico, redutor, no sentido próprio do termo; ela é o modelo do controle, da autoridade, no domínio social, assim como da operação destinada a evitar a variação fortuita, a detectar o erro.
> (p. 298)

# A organização corresponde a estabilidade do presente

> A organização corresponde à estabilidade do presente completo, dilatado em momento, etapa que condensa e mantém uma certa dimensão do passado e uma certa duração do futuro:
> ela corresponde a um adensador de tempo, e de tempo presente, enquanto a transdução é um impulso instantâneo rumo ao futuro, e a modulação uma iteração fixa do passado sob forma de conservação. A ordem dessa sucessão é necessária, pois a modulação só pode ocorrer num domínio onde já existam estruturas; a transdução é precisamente capaz de criar estruturas a partir de um meio homogêneo metaestável.
> (p. 298-9)

# Invenção organizadora como síntese com a fecundidade dos processos associativos e a capacidade formalizadora da modulação

> Enfim, se a característica própria do psiquismo é a capacidade de organização, os atos de organização devem ser precedidos por operações transdutivas e moduladoras que os preparam dialeticamente. Atividades instintivas em cadeia e processos de tipo associativo são essencialmente transdutivos. Ao contrário, as atividades de abstração e de generalização, controladas pelo raciocínio formal, aplicando esquemas adquiridos a conteúdos novos, são moduladoras. Enfim, os atos de invenção organizadora têm, ao mesmo tempo, a fecundidade dos processos associativos e a capacidade formalizadora da modulação.
> (p. 299)

[^1]: SIMONDON, Gilbert, A amplificação nos processos de informação, _Trans/Form/Ação_, v. 43, n. 1, p. 283–300, 2020. Disponível em: <https://revistas.marilia.unesp.br/index.php/transformacao/article/view/8165>.
