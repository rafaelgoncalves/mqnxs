---
title: Pensar e pesquisar algoritmos
tags:
    - Rob Kitchin
    - algoritmo
    - metodologia
    - fichamento

path: "/pensar-e-pesquisar-algoritmos"
date: 2024-03-14
category: fichamento
featuredImage: "rob-kitchin.jpg"
srcInfo: <a href="https://www.maynoothuniversity.ie/faculty-social-sciences/our-people/rob-kitchin">Rob Kitchin</a>
published: true
---

Fichamento do texto "Pensando criticamente sobre e pesquisando algoritmos"[^1] de Rob Kitchin. O artigo tem muitas referências para outros artigos que tratam de agência algoritmica e podem ser interessantes para uma pesquisa.

[^1]: KITCHIN, R. Thinking Critically About and Researching Algorithms. **SSRN Electronic Journal**, 2014.

# Abstract 

> More and more aspects of our everyday lives are being mediated, augmented, produced and regulated by software-enabled technologies. Software is fundamentally composed of algorithms: sets of deﬁned steps structured to process instructions/data to produce an output. This paper synthesises and extends emerging critical thinking about algorithms and considers how best to research them in practice. Four main arguments are developed. First, there is a pressing need to focus critical and empirical attention on algorithms and the work that they do given their increasing importance in shaping social and economic life. Second, algorithms can be conceived in a number of ways – technically, computationally, mathematically, politically, culturally, economically, contextually, materially, philosophically, ethically – but are best understood as being contingent, ontogenetic and performative in nature, and embedded in wider socio-technical assemblages. Third, there are three main challenges that hinder research about algorithms (gaining access to their formulation; they are heterogeneous and embedded in wider systems; their work unfolds contextually and contingently), which require practical and epistemological attention. Fourth, the constitution and work of algorithms can be empirically studied in a number of ways, each of which has strengths and weaknesses that need to be systematically evaluated. Six methodological approaches designed to produce insights into the nature and work of algorithms are critically appraised. It is contended that these methods are best used in combination in order to help overcome epistemological and practical challenges.

# Sobre o autor

> Rob Kitchin is a professor and ERC Advanced Investigator at the National University of Ireland Maynooth. He is currently a principal investigator on the Programmable City project, the Digital Repository of Ireland, the All-Island Research Observatory and the Dublin Dashboard. [email: rob.kitchin@nuim.ie]

# Ubiquidade dos algoritmos

> The era of ubiquitous computing and big data is now ﬁrmly established, with more and more aspects of our everyday lives – play, consumption, work, travel, communication, domestic tasks, security, etc. – being mediated, augmented, produced and regulated by digital devices and networked systems powered by software (Greenﬁeld, 2006; Kitchin & Dodge, 2011; Manovich, 2013; Steiner, 2012).
> (p. 14)

# Software como máquina algoritmica

> Software is fundamentally composed of algorithms – sets of deﬁned steps structured to process instructions/data to produce an output – with all digital technologies thus constituting ‘algorithm machines’ (Gillespie, 2014a).
> (p. 14)

# Governança algoritmica como automação da disciplina e controle de sociedades e aumento de eficiência na acumulação de capital

> Such conclusions have led a number of commentators to argue that we are now entering an era of widespread algorithmic governance, wherein algorithms will play an everincreasing role in the exercise of power, a means through which to automate the disciplining and controlling of societies and to increase the efﬁciency of capital accumulation.
> (p. 15)

# O que falta é entender como algoritmos exercem poder sobre nós (funcionamento). Problema da caixa-preta.

> However, Diakopoulos (2013, p. 2, original emphasis) warns that: ‘What we generally lack as a public is _clarity about how algorithms exercise their power over us_.’ Such clarity is absent because although algorithms are imbued with the power to act upon data and make consequential decisions (such as to issue ﬁnes or block travel or approve a loan) they are largely black boxed and beyond query or question. What is at stake then with the rise of ‘algorithm machines’ is new forms of algorithmic power that are reshaping how social and economic systems work.
> (p. 15-6)

# Aumento do número de estudos sobre algoritmos

> In response, over the past decade or so, a growing number of scholars have started to focus critical attention on software code and algorithms, drawing on and contributing to science and technology studies, new media studies and software studies, in order to unpack the nature of algorithms and their power and work. Their analyses typically take one of three forms: a detailed case study of a single algorithm, or class of algorithms, to examine the nature of algorithms more generally (e.g., Bucher, 2012; Geiger, 2014; Mackenzie, 2007; Montfort et al., 2012); a detailed examination of the use of algorithms in one domain, such as journalism (Anderson, 2011), security (Amoore, 2006, 2009) or ﬁnance (Pasquale, 2014, 2015); or a more general, critical account of algorithms, their nature and how they perform work (e.g., Cox, 2013; Gillespie, 2014a, 2014b; Seaver, 2013).
> (p. 16)

# Objetivos do artigo: sintetizar, criticar e extender esses estudos

> This paper synthesises, critiques and extends these studies. Divided into two main sections – thinking critically about and researching algorithms – the paper makes four key arguments. First, as already noted, there is a pressing need to focus critical and empirical attention on algorithms and the work that they do in the world. Second, it is most productive to conceive of algorithms as being contingent, ontogenetic, performative in nature and embedded in wider socio-technical assemblages. Third, there are three main challenges that hinder research about algorithms (gaining access to their formulation; they are heterogeneous and embedded in wider systems; their work unfolds contextually and contingently), which require practical and epistemological attention. Fourth, the constitution and work of algorithms can be empirically studied in a number of ways, each of which has strengths and weaknesses that need to be systematically evaluated. With respect to the latter, the paper provides a critical appraisal of six methodological approaches that might proﬁtably be used to produce insights into the nature and work of algorithms.
> (p. 16)

# Origens do termo algoritmo (matemática e programação)

> Miyazaki (2012) traces the term ‘algorithm’ to twelfth-century Spain when the scripts of the Arabian mathematician Muḥ ammad ibn Mūsā al-Khwārizmī were translated into Latin. These scripts describe methods of addition, subtraction, multiplication and division using numbers. Thereafter, ‘algorism’ meant ‘the speciﬁc step-by-step method of performing written elementary arithmetic’ (Miyazaki, 2012, p. 2) and ‘came to describe any method of systematic or automatic calculation’ (Steiner, 2012, p. 55). By the mid-twentieth century and the development of scientiﬁc computation and early high level programming languages, such as Algol 58 and its derivatives (short for ALGOrithmic Language), an algorithm was understood to be a set of deﬁned steps that if followed in the correct order will computationally process input (instructions and/or data) to produce a desired outcome (Miyazaki, 2012).
> (p. 16)

# Algoritmo como Lógica+Controle

> From a computational and programming perspective an ‘Algorithm = Logic + Control’; where the logic is the problem domain-speciﬁc component and speciﬁes the abstract formulation and expression of a solution (what is to be done) and the control component is the problem-solving strategy and the instructions for processing the logic under different scenarios (how it should be done) (Kowalski, 1979). The efﬁciency of an algorithm can be enhanced by either reﬁning the logic component or by improving the control over its use, including altering data structures (input) to improve efﬁciency (Kowalski, 1979). As reasoned logic, the formulation of an algorithm is, in theory at least, independent of programming languages and the machines that execute them; ‘it has an autonomous existence independent of “implementation details”’ (Goffey, 2008, p. 15).
> (p. 16-7)

# Programação como 2 traduções sucessivas: problema -> pseudocódigo -> código específico

> Coding thus consists of two key translation challenges centred on producing algorithms. The ﬁrst is translating a task or problem into a structured formula with an appropriate rule set (pseudo-code). The second is translating this pseudo-code into source code that when compiled will perform the task or solve the problem. Both translations can be challenging, requiring the precise deﬁnition of what a task/problem is (logic), then breaking that down into a precise set of instructions, factoring in any contingencies such as how the algorithm should perform under different conditions (control). The consequences of mistranslating the problem and/or solution are erroneous outcomes and random uncertainties (Drucker, 2013).
> (p. 17)

# Essas traduções são apresentadas como "puramente técnicas", "objetivas" e "puro raciocínio" por cientistas e empresas

> The processes of translation are often portrayed as technical, benign and commonsensical. This is how algorithms are mostly presented by computer scientists and technology companies: that they are ‘purely formal beings of reason’ (Goffey, 2008, p. 16). Thus, as Seaver (2013) notes, in computer science texts the focus is centred on how to design an algorithm, determine its efﬁciency and prove its optimality from a purely technical perspective. If there is discussion of the work algorithms do in real-world contexts this concentrates on how algorithms function in practice to perform a speciﬁc task. In other words, algorithms are understood ‘to be strictly rational concerns, marrying the certainties of mathematics with the objectivity of technology’ (Seaver, 2013, p. 2). ‘Other knowledge about algorithms – such as their applications, effects, and circulation – is strictly out of frame’ (Seaver, 2013, pp. 1–2). As are the complex set of decision-making processes and practices, and the wider assemblage of systems of thought, ﬁnance, politics, legal codes and regulations, materialities and infrastructures, institutions, inter-personal relations, which shape their production (Kitchin, 2014).
> (p. 17)

# Aspecto frágil, construído, político, heterogêneo e em rede dos algoritmos

> In other words, creating an algorithm unfolds in context through processes such as trial and error, play, collaboration, discussion and negotiation. They are ontogenetic in nature (always in a state of becoming), teased into being: edited, revised, deleted and restarted, shared with others, passing through multiple iterations stretched out over time and space (Kitchin & Dodge, 2011). As a result, they are always somewhat uncertain, provisional and messy fragile accomplishments (Gillespie, 2014a; Neyland, 2015). And such practices are complemented by many others, such as researching the concept, selecting and cleaning data, tuning parameters, selling the idea and product, building coding teams, raising ﬁnance and so on. These practices are framed by systems of thought and forms of knowledge, modes of political economy, organisational and institutional cultures and politics, governmentalities and legalities, subjectivities and communities. As Seaver (2013, p. 10) notes, ‘algorithmic systems are not standalone little boxes, but massive, networked ones with hundreds of hands reaching into them, tweaking and tuning, swapping out parts and experimenting with new arrangements.’
> (p. 18)

# Algoritmo como parte de um agenciamento sociotécnico

> Algorithms cannot be divorced from the conditions under which they are developed and deployed (Geiger, 2014). What this means is that algorithms need to be understood as relational, contingent, contextual in nature, framed within the wider context of their socio-technical assemblage. From this perspective, ‘algorithm’ is one element in a broader apparatus which means it can never be understood as a technical, objective, impartial form of knowledge or mode of operation.
> (p. 18)

# Algoritmos como políticos e performativos (agem)

> Beyond thinking critically about the nature of algorithms, there is also a need to consider their work, effects and power. Just as algorithms are not neutral, impartial expressions of knowledge, their work is not impassive and apolitical. Algorithms search, collate, sort, categorise, group, match, analyse, proﬁle, model, simulate, visualise and regulate people, processes and places. They shape how we understand the world and they do work in and make the world through their execution as software, with profound consequences (Kitchin & Dodge, 2011). In this sense, they are profoundly performative as they cause things to happen (Mackenzie & Vurdubakis, 2011). And while the creators of these algorithms might argue that they ‘replace, displace, or reduce the role of biased or self-serving intermediaries’ and remove subjectivity from decision-making, computation often deepens and accelerates processes of sorting, classifying and differentially treating, and reifying traditional pathologies, rather than reforming them (Pasquale, 2014, p. 5).
> (p. 18-9)

# Autoridade algoritmica, governança algoritmica e gestão automatizada (criação de regras algoritmicas/gerativas por onde o poder capitalista trabalha). Poder através dos algoritmos.

> Algorithms thus claim and express algorithmic authority (Shirky, 2009) or algorithmic governance (Beer, 2009; Musiani, 2013), often through what Dodge and Kitchin (2007) term ‘automated management’ (decision-making processes that are automated, automatic and autonomous; outside of human oversight). The consequence for Lash (2007) is that society now has a new rule set to live by to complement constitutive and regulative rules: algorithmic, generative rules. He explains that such rules are embedded within computation, an expression of ‘power through the algorithm’; they are ‘virtuals that generate a whole variety of actuals. They are compressed and hidden and we do not encounter them in the way that we encounter constitutive and regulative rules. … They are … pathways through which capitalist power works’ (Lash, 2007, p. 71).
> (p. 19)

# Existe uma via de mão dupla de interações, o usuário também interfere na agência algoritmica

> Moreover, once computation is made public it undergoes a process of domestication, with users embedding the technology in their lives in all kinds of alternative ways and using it for different means, or resisting, subverting and reworking the algorithms’ intent (consider the ways in which users try to game Google’s PageRank algorithm). In this sense, algorithms are not just what programmers create, or the effects they create based on certain input, they are also what users make of them on a daily basis (Gillespie, 2014a).
> (p. 19)

# Problema 1: Acesso (caixa-preta)

> Many of the most important algorithms that people encounter on a regular basis and which (re)shape how they perform tasks or the services they receive are created in environments that are not open to scrutiny and their source code is hidden inside impenetrable executable ﬁles. Coding often happens in private settings, such as within companies or state agencies, and it can be difﬁcult to negotiate access to coding teams to observe them work, interview programmers or analyse the source code they produce. This is unsurprising since it is often a company’s algorithms that provide it with a competitive advantage and they are reluctant to expose their intellectual property even with non-disclosure agreements in place. They also want to limit the ability of users to game the algorithm to unfairly gain a competitive edge. Access is a little easier in the case of open-source programming teams and open-source programmes through repositories such as Github, but while they provide access to much code, this is limited in scope and does not include key proprietary algorithms that might be of more interest with respect to holding forms of algorithmic governance to account.
> (p. 20)

# Problema 2: heterogêneo e embarcado (sistemas algoritmicos)

> Heterogeneous and embedded If access is gained, algorithms, as Seaver (2013) notes, are rarely straightforward to deconstruct. Within code algorithms are usually woven together with hundreds of other algorithms to create algorithmic systems. It is the workings of these algorithmic systems that we are mostly interested in, not speciﬁc algorithms, many of which are quite benign and procedural. Algorithmic systems are most often ‘works of collective authorship, made, maintained, and revised by many people with different goals at different times’ (Seaver, 2013, p. 10). They can consist of original formulations mashed together with those sourced from code libraries, including stock algorithms that are re-used in multiple instances. Moreover, they are embedded within complex socio-technical assemblages made up of a heterogeneous set of relations including potentially thousands of individuals, data sets, objects, apparatus, elements, protocols, standards, laws, etc. that frame their development. Their construction, therefore, is often quite messy, full of ‘ﬂux, revisability, and negotiation’ (p. 10), making unpacking the logic and rationality behind their formulation difﬁcult in practice. Indeed, it is unlikely that any one programmer has a complete understanding of a system, especially large, complex ones that are built by many teams of programmers, some of whom may be distributed all over the planet or may have only had sight of smaller outsourced segments. Getting access to a credit rating agency’s algorithmic system then might give an insight into its formula for assessing and sorting individuals, its underlying logics and principles, and how it was created and works in practice, but will not necessarily provide full transparency as to its full reasoning, workings or the choices made in its construction (Bucher, 2012; Chun, 2011).
> (p. 20-1)

# Problema 3: ontogenetico, performativo e contingente (agenciamentos dinamicos, sempre evoluindo e mudando). Aspecto reativo e modulatório dos algoritmos.

> As well as being heterogeneous and embedded, algorithms are rarely ﬁxed in form and their work in practice unfolds in multifarious ways. As such, algorithms need to be recognised as being ontogenetic, performative and contingent: that is, they are never ﬁxed in nature, but are emergent and constantly unfolding. In cases where an algorithm is static, for example, in ﬁrmware that is not patched, its work unfolds contextually, reactive to input, interaction and situation. In other cases, algorithms and their instantiation in code are often being reﬁned, reworked, extended and patched, iterating through various versions (Miyazaki, 2012). Companies such as Google and Facebook might be live running dozens of different versions of an algorithm to assess their relative merits, with no guarantee that the version a user interacts with at one moment in time is the same as ﬁve seconds later. In some cases, the code has been programmed to evolve, re-writing its algorithms as it observes, experiments and learns independently of its creators (Steiner, 2012).
> (p. 21)

> Similarly, many algorithms are designed to be reactive and mutable to inputs. As Bucher (2012) notes, Facebook’s EdgeRank algorithm (that determines what posts and in what order are fed into each users’ timeline) does not act from above in a static, ﬁxed manner, but rather works in concert with the each individual user, ordering posts dependent on how one interacts with ‘friends.’ Its parameters then are contextually weighted and ﬂuid. In other cases, randomness might be built into an algorithm’s design meaning its outcomes can never be perfectly predicted. What this means is that the outcomes for users inputting the same data might vary for contextual reasons (e.g., Mahnke and Uprichard (2014) examined Google’s autocomplete search algorithm by typing in the same terms from two locations and comparing the results, ﬁnding differences in the suggestions the algorithm gave), and the same algorithms might be being used in quite varied and mutable ways (e.g., for work or for play). Examining one version of an algorithm will then provide a snapshot reading that fails to acknowledge or account for the mutable and often multiple natures of algorithms and their work (Bucher, 2012). Algorithms then are often ‘out of control’ in the sense that their outcomes are some-
> (p. 21)

# Uso simultâneo de mais de uma abordagem metodológica

> Each approach has its strengths and drawbacks and their use is not mutually exclusive. Indeed, I would argue that there would be much to be gained by using two or more of the approaches in combination to compensate for the drawbacks of employing them in isolation. Nor are they the only possible approaches, with ethnomethodologies, surveys and historical analysis using archives and oral histories offering other possible avenues of analysis and insight.
> (p. 22)

# Abordagem metodológica (AM) 1:  Examinar o pseudocódigo / código fonte

## AM1.1: Ler o código fonte e documentação a fim de entender o funcionamento

> The ﬁrst is to carefully deconstruct the pseudo-code and/or source code, teasing apart the rule set to determine how the algorithm works to translate input to produce an outcome (Krysa & Sedek, 2008). In practice this means carefully sifting through documentation, code and programmer comments, tracing out how the algorithm works to process data and calculate outcomes, and decoding the translation process undertaken to construct the algorithm.
> (p. 22)

## AM1.2: Traçar uma genealogia do algoritmo, checando mudanças que foram feitas (git diff)

> The second is to map out a genealogy of how an algorithm mutates and evolves over time as it is tweaked and rewritten across different versions of code. For example, one might deconstruct how an algorithm is re-scripted in multiple instantiations of a programme within a code library such as github. Such a genealogy would reveal how thinking with respect to a problem is reﬁned and transformed with respect to how the algorithm/code performs ‘in the wild’ and in relation to new technologies, situations and contexts (such as new platforms or regulations being introduced).
> (p. 22)

## AM1.3: Fazer uma análise comparativa das contingências do algoritmo para uma linguagem/hardware específicos

> The third is to examine how the same task is translated into various software languages and how it runs across different platforms. This is an approach used by Montfort et al. (2012) in their exploration of the ‘10 PRINT’ algorithm, where they scripted code to perform the same task in multiple languages and ran it on different hardware, and also tweaked the parameters, to observe the speciﬁc contingencies and affordances this introduced.
> (p. 22)

# Problemas com essas abordagens: é trabalhoso/difícil, exige um saber específico, descontextualiza o algoritmo da rede sociotécnica a qual ele participa

> While these methods do offer the promise of providing valuable insights into the ways in which algorithms are built, how power is vested in them through their various parameters and rules, and how they process data in abstract and material terms to complete a task, there are three signiﬁcant issues with their deployment. First, as noted by Chandra (2013), deconstructing and tracing how an algorithm is constructed in code and mutates over time is not straightforward. Code often takes the form of a ‘Big Ball of Mud’: ‘[a] haphazardly structured, sprawling, sloppy, duct-tape and bailing wire, spaghetti code jungle’ (Foote & Yoder, 1997; cited in Chandra, 2013, p. 126). Even those that have produced it can ﬁnd it very difﬁcult to unpack its algorithms and routines; those unfamiliar with its development can often ﬁnd that the ball of mud remains just that. Second, it requires that the researcher is both an expert in the domain to which the algorithm refers and possesses sufﬁcient skill and knowledge as a programmer that they can make sense of a ‘Big Ball of Mud’; a pairing that few social scientists and humanities scholars possess. Third, these approaches largely decontextualise the algorithm from its wider socio-technical assemblage and its use.
> (p. 22-3)

# AM2: Produzir código reflexivamente

> A related approach is to conduct auto-ethnographies of translating tasks into pseudo-code and the practices of producing algorithms in code. Here, rather than studying an algorithm created by others, a researcher reﬂects on and critically interrogates their own experiences of translating and formulating an algorithm. This would include an analysis of not only the practices of exploring and translating a task, originating and developing ideas, writing and revising code, but also how these practices are situated within and shaped by wider socio-technical factors such as regulatory and legal frameworks, form of knowledge, institutional arrangements, ﬁnancial terms and conditions, and anticipated users and market. Ziewitz (2011) employed this kind of approach to reﬂect on producing a random routing algorithm for directing a walking path through a city, reﬂecting on the ontological uncertainty in the task itself (that there is often an ontological gerrymandering effect at work as the task itself is re-thought and re-deﬁned while the process of producing an algorithm is undertaken), and the messy, contingent process of creating the rule set and parameters in practice and how these also kept shifting through deferred accountability. Similarly, Ullman (1997) uses such an approach to consider the practices of developing software and how this changed over her career.
> (p. 23)

# Problemas: auto-etnografia envolve muita subjetividade, exclui atos inconscientes da análise, não é possível fazer isso com algoritmos ampla e concretamente usados em redes sociotécnicas

> While this approach will provide useful insights into how algorithms are created, it also has a couple of limitations. The ﬁrst is the inherent subjectivities involved in doing an auto-ethnography and the difﬁculties of detaching oneself and gaining critical distance to be able to give clear insight into what is unfolding. Moreover, there is the possibility that in seeking to be reﬂexive what would usually take place is inﬂected in unknown ways. Further, it excludes any non-representational, unconscious acts from analysis. Second, one generally wants to study algorithms and code that have real concrete effects on peoples’ everyday lives, such as those used in algorithmic governance. One way to try and achieve this is to contribute to open-source projects where the code is incorporated into products that others use, or to seek access to a commercial project as a programmer (on an overt, approved basis with non-disclosure agreements in place). The beneﬁt here is that the method can be complemented with the sixth approach set out below, examining and reﬂecting on the relationship between the production of an algorithm and any associated ambitions and expectations vis-à-vis how it actually does work in the world.
> (p. 23)

# AM3: Engenharia reversa

> Reverse engineering In cases where the code remains black boxed, a researcher interested in the algorithm at the heart of its workings is left with the option of trying to reverse engineer the compiled software. Diakopoulos (2013, p. 13) explains that ‘[r]everse engineering is the process of articulating the speciﬁcations of a system through a rigorous examination drawing on domain knowledge, observation, and deduction to unearth a model of how that system works.’ While software producers might desire their products to remain opaque, each programme inherently has two openings that enable lines of enquiry: input and output. By examining what data are fed into an algorithm and what output is produced it is possible to start to reverse engineer how the recipe of the algorithm is composed (how it weights and preferences some criteria) and what it does.
> (p. 23-4)

Exemplos:

> The main way this is attempted is by using carefully selected dummy data and seeing what is outputted under different scenarios. For example, researchers might search Google using the same terms on multiple computers in multiple jurisdictions to get a sense of how its PageRank algorithm is constructed and works in practice (Mahnke & Uprichard, 2014), or they might experiment with posting and interacting with posts on Facebook to try and determine how its EdgeRank algorithm positions and prioritises posts in user time lines (Bucher, 2012), or they might use proxy servers and feed dummy user proﬁles into e-commerce systems to see how prices might vary across users and locales (Wall Street Journal, detailed in Diakopoulos, 2013). One can also get a sense of an algorithm by ‘looking closely at how information must be oriented to face them, how it is made algorithm-ready’; how the input data are delineated in terms of what input variables are sought and structured, and the associated meta-data (Gillespie, 2014a). Another possibility is to follow debates on online forums by users about how they perceive an algorithm works or has changed, or interview marketers, media strategists, and public relations ﬁrms that seek to game an algorithm to optimise an outcome for a client (Bucher, 2012).
> (p. 24)

# Problemas: Essa abordagem é limitada e só da uma noção de como o algoritmo funciona

> While reverse engineering can give some indication of the factors and conditions embedded into an algorithm, they generally cannot do so with any speciﬁcity (Seaver, 2013). As such, they usually only provide fuzzy glimpses of how an algorithm works in practice but not its actual constitution (Diakopoulos, 2013). One solution to try and enhance clarity has been to employ bots, which posing as users, can more systematically engage with a system, running dummy data and interactions. However, as Seaver (2013) notes, many proprietary systems are aware that many people are seeking to determine and game their algorithm, and thus seek to identify and block bot users.
> (p. 24)

# AM4: Entrevistar designers ou conduzir etnografia do grupo de programadores

> In the ﬁrst case, respondents are questioned as to how they framed objectives, created pseudo-code and translated this into code, and quizzed about design decisions and choices with respect to languages and technologies, practices, inﬂuences, constraints, debates within a team or with clients, institutional politics and major changes in direction over time (Diakopoulos, 2013; MacKenzie, 2014; Mager, 2012). In the second case, a researcher seeks to spend time within a coding team, either observing the work of the coders, discussing it with them, and attending associated events such as team meetings, or working in situ as part of the team, taking an active role in producing code. An example of the former is Rosenberg’s (2007) study of one company’s attempt to produce a new product conducted over a three-year period in which he was given full access to the company, including observing and talking to coders, and having access to team chat rooms and phone conferences. An example of the latter is Takhteyev’s (2012) study of an open-source coding project in Rio de Janeiro where he actively worked on developing the code, as well as taking part in the social life of the team. In both cases, Rosenberg and Takhteyev generate much insight into the contingent, relational and contextual way in which algorithms and software are produced, though in neither case are the speciﬁcities of algorithms and their work unpacked and detailed.
> (p. 24-5)

# AM5: Entender a rede sociotécnica dos algoritmos

> As already noted, algorithms are not formulated or do not work in isolation, but form part of a technological stack that includes infrastructure/hardware, code platforms, data and interfaces, and are framed and conditions by forms of knowledge, legalities, governmentalities, institutions, marketplaces, ﬁnance and so on. A wider understanding of algorithms then requires their full socio-technical assemblage to be examined, including an analysis of the reasons for subjecting the system to the logic of computation in the ﬁrst place. Examining algorithms without considering their wider assemblage is, as Geiger (2014) argues, like considering a law without reference to the debate for its introduction, legal institutions, infrastructures such as courts, implementers such as the police, and the operating and business practices of the legal profession. It also risks fetishising the algorithm and code at the expense of the rest of the assemblage (Chun, 2011).
> (p. 25)

> Interviews and ethnographies of coding projects, and the wider institutional apparatus surrounding them (e.g., management and institutional collaboration), start to produce such knowledge, but they need to supplemented with other approaches, such as a discursive analysis of company documents, promotional/industry material, procurement tenders and legal and standards frameworks; attending trade fairs and other inter-company interactions; examining the practices, structures and behaviour of institutions; and documenting the biographies of key actors and the histories of projects (Montfort et al., 2012; Napoli, 2013). Such a discursive analysis will also help to reveal how algorithms are imagined and narrated, illuminate the discourses surrounding and promoting them, and how they are understood by those that create and promote them. Gaining access to such a wider range of elements, and being able to gather data and interlink them to be able to unpack a socio-technical assemblage, is no easy task but it is manageable as a large case study, especially if undertaken by a research team rather than a single individual.
> (p. 25)

# AM6: Examinar como o algoritmo funciona no mundo

> Examining how algorithms do work in the world Given that algorithms do active work in the world it is important not only to focus on the construction of algorithms, and their production within a wider assemblage, but also to examine how they are deployed within different domains to perform a multitude of tasks. This cannot be simply denoted from an examination of the algorithm/code alone for two reasons. First, what an algorithm is designed to do in theory and what it actually does in practice do not always correspond due to a lack of reﬁnement, miscodings, errors and bugs. Second, algorithms perform in context – in collaboration with data, technologies, people, etc. under varying conditions – and therefore their effects unfold in contingent and relational ways, producing localised and situated outcomes. When users employ an algorithm, say for play or work, they are not simply playing or working in conjunction with the algorithm, rather they are ‘learning, internalizing, and becoming intimate with’ it (Galloway, 2006, p. 90); how they behave is subtly reshaped through the engagement, but at the same time what the algorithm does is conditional on the input it receives from the user. We can therefore only know how algorithms make a different to everyday life by observing their work in the world under different conditions.
> (p. 25-6)

> One way to undertake such research is to conduct ethnographies of how people engage with and are conditioned by algorithmic systems and how such systems reshape how organisations conduct their endeavours and are structured (e.g., Lenglet, 2011). It would also explore the ways in which people resist, subvert and transgress against the work of algorithms, and re-purpose and re-deploy them for purposes they were not originally intended. For example, examining the ways in which various mobile and web applications were re-purposed in the aftermath of the Haiti earthquake to coordinate disaster response, remap the nation and provide donations (Al-Akkad et al., 2013). Such research requires detailed observation and interviews focused on the use of particular systems and technologies by different populations and within different scenarios, and how individuals interfaced with the algorithm through software, including their assessments as to their intentions, sense of what is occurring and associated consequences, tactics of engagement, feelings, concerns and so on. In cases where an algorithm is black boxed, such research is also likely to shed some light on the constitution of the algorithm itself.
> (p. 26)


