---
title: A recente ressurreição da economia solidária no Brasil
date: 2021-10-27
path: "/ressureicao-economia-solidaria-brasil"
category: fichamento
tags: [Paul Singer, economia solidária, fichamento, Brasil]
featuredImage: "paul-singer.jpeg"
srcInfo: <a href="https://commons.wikimedia.org/wiki/File:PAUL_Singer.JPG">Retirado de wikimedia. Foto por Antonio Cruz/ ABR</a>
published: true
---

Fichamento de texto[^1] sobre economia solidária no Brasil escrito pelo economista brasileiro Paul Singer.

# Gênese da economia solidária na europa industrial do início do século XIX

> A economia solidária foi inventada por operários, nos primórdios do capitalismo industrial, como resposta à pobreza e ao desemprego resultantes da difusão «desregulamentada» das máquinas-ferramenta e do motor a vapor, no início do século XIX.
<!-- > (p. 1) -->

# Gênese nas cooperativas e no movimento operário (ideologia? socialista)

> As cooperativas eram tentativas por parte de trabalhadores de recuperar trabalho e autonomia econômica, aproveitando as novas forças produtivas. Sua estruturação obedecia aos valores básicos do movimento operário de igualdade e democracia, sintetizados na ideologia do socialismo.
<!-- > (p. 1) -->

# Identidade entre trabalho e posse na empresa solidária

> A empresa solidária nega a separação entre trabalho e posse dos meios de produção, que é reconhecidamente a base do capitalismo. A empresa capitalista pertence aos investidores, aos que forneceram o dinheiro para adquirir os meios de produção e é por isso que sua única finalidade é dar lucro a eles, o maior lucro possível em relação ao capital investido. O poder de mando, na empresa capitalista, está concentrado totalmente (ao menos em termos ideais) nas mãos dos capitalistas ou dos gerentes por eles contratados.
<!-- > (p. 1) -->

# Propriedade da empresa, capital e lucro divididos entre os trabalhadores

> O capital da empresa solidária é possuído pelos que nela trabalham e apenas por eles. Trabalho e capital estão fundidos porque todos os que trabalham são proprietários da empresa e não há proprietários que não trabalhem na empresa. E a propriedade da empresa é dividida por igual entre todos os trabalhadores, para que todos tenham o mesmo poder de decisão sobre ela. Empresas solidárias são, em geral, administradas por sócios eleitos para a função e que se pautam pelas diretrizes aprovadas em assembléias gerais ou, quando a empresa é grande demais, em conselhos de delegados eleitos por todos os trabalhadores.
<!-- > (p. 1) -->

# Ênfase no trabalho (e não na posse) e na quantidade e qualidade do trabalho (não no lucro)

> A empresa solidária é basicamente de trabalhadores, que apenas secundariamente são seus proprietários. Por isso, sua finalidade básica não é maximizar lucro mas a quantidade e a qualidade do trabalho. Na realidade, na empresa solidária não há lucro porque nenhuma parte de sua receita é distribuída em proporção às cotas de capital.
<!-- > (p. 2) -->

# Sobra x lucro

> O excedente anual - chamado «sobras» nas cooperativas - tem a sua destinação decidida pelos trabalhadores. Uma parte, em geral, destina-se ao reinvestimento e pode ser colocada num fundo «indivisível», que não pertence aos sócios individualmente mas apenas ao coletivo deles. Outra parte, também reinvestida, pode acrescer o valor das cotas dos sócios, que têm o direito de sacá-las quando se retiram da empresa. O restante das sobras é em geral destinado a um fundo de educação, a outros fundos «sociais» (de cultura, de saúde, etc.) e eventualmente à repartição entre os sócios, por critérios aprovados por eles. Portanto, o capital da empresa solidária não é remunerado, sob qualquer pretexto, e por isso não há «lucro» pois este é tanto jurídica como economicamente o rendimento proporcionado pelo investimento de capital.
<!-- > (p. 2) -->

# As cooperativas de produção são a modalidade básica da economia solidária

> A cooperativa de produção é a modalidade básica da economia solidária e as relações sociais de produção que a definem são as delineadas acima.
<!-- > (p. 2) -->

# Cooperativa de comercialização

> Outra é a cooperativa de comercialização, composta por produtores autônomos, individuais ou familiares (camponeses, taxistas, profissionais liberais, artesãos, etc.) que fazem suas compras em comum e, quando cabe, também suas vendas. Sendo a produção individual, o ganho também é e as sobras das operações comerciais são em geral distribuídas entre os cooperadores em proporção ao montante comprado e vendido por cada um através da cooperativa.
<!-- > (p. 2) -->

# Cooperativa de consumo

> Outra modalidade de empresa solidária é a cooperativa de consumo, que é possuída pelos que consomem seus produtos ou serviços. A finalidade dela é proporcionar a máxima satisfação ao menor custo aos cooperadores. Mas, para ser empresa solidária, não pode haver separação entre trabalho e capital. Muitas cooperativas de consumo empregam trabalho assalariado, o que enseja lutas de classe em seu interior. Por isso não fazem parte da economia solidária. Só pertencem a ela as cooperativas de consumo que tornam seus trabalhadores membros plenos. Alguns a denominam por isso de cooperativas mistas.
<!-- > (p. 3) -->


#Cooperativas de crédito

> O mesmo se aplica às cooperativas de crédito. Estas são empresas de intermediação financeira possuídas pelos depositantes. Para que sejam solidárias, é preciso que os trabalhadores que as operam profissionalmente sejam sócios delas. As cooperativas de crédito comunitárias, formadas por moradores da mesma cidade ou membros do mesmo sindicato, etc. aplicam os depósitos em empréstimos pessoais aos cooperadores. Isso se chama crédito rotativo e resgata gente pobre das garras da agiotagem, já que os bancos comerciais estão quase sempre fechados para ela.
<!-- > (p. 3) -->


# Empresas solidárias tendem a se federar

> As empresas solidárias tendem a se federar, formando associações locais, regionais, nacionais e internacionais. O que impulsiona esta tendência é o mesmo conjunto de fatores que produz a centralização dos capitais em grandes empresas multinacionais e conglomerados: os ganhos de escala que permitem reduzir custos; a necessidade de juntar recursos para desenvolver nova tecnologia e difundir a melhor tecnologia, além de outros empreendimentos de alto custo e alto risco.
<!-- > (p. 3) -->


# Existem cooperativas não baseadas na autogestão (não solidárias)

> O abandono da autogestão nas empresas criadas por cooperativas de consumo foi posteriormente imitado pelas de comercialização. Ela representou na prática uma ruptura jamais admitida com os seus princípios. O que não impediu que o movimento cooperativista, representado em plano mundial pela ACI (Aliança Cooperativa Internacional), continuasse sustentando os princípios de Rochdale, que definem a cooperativa como democrática e igualitária. Assim, em tese, as cooperativas continuam sendo autogestionárias, mas na prática muitas assalariam os que a operam.
<!-- > (p. 4) -->

# Oscilação nas cooperativas entre momentos solidários e capitalistas

> Muitas cooperativas provavelmente passaram por períodos em que eram empresas solidárias e outros em que se assemelhavam mais a empresas capitalistas. Estas oscilações se devem à inserção econômica e social de cada cooperativa - muitas surgem a partir de lutas operárias ou camponesas - e ao «espírito da época», que impregna os cooperadores ora de valores solidários e democráticos, ora de individualismo e culto à competição.
<!-- > (p. 4) -->

# Economia solidária se compões de empresas baseadas na autogestão ('princícpios do cooperativismo')

> A economia solidária se compõe das empresas que efetivamente praticam os princípios do cooperativismo, ou seja, a autogestão. Ela faz parte portanto da economia cooperativa ou social, sem no entanto se confundir com as cooperativas que empregam assalariados.
<!-- > (p. 4) -->

# Economia solidária como modo de produção alternativo funcionando no capitalismo (modo de produção hegemônico)

> A economia solidária constitui um modo de produção que, ao lado de diversos outros modos de produção - o capitalismo, a pequena produção de mercadorias, a produção estatal de bens e serviços, a produção privada sem fins de lucro -, compõe a formação social capitalista, que é capitalista porque o capitalismo não só é o maior dos modos de produção mas molda a superestrutura legal e institucional de acordo com os seus valores e interesses.
<!-- > (p. 5) -->

# A economia só se torna uma alternativa real ao capitalismo com a adesão da maioria da sociedade

> A economia solidária cresce em função das crises sociais que a competição cega dos capitais privados ocasiona periodicamente em cada país. Mas ela só se viabiliza e se torna uma alternativa real ao capitalismo quando a maioria da sociedade, que não é proprietária de capital, se conscientiza de que é de seu interesse organizar a produção de um modo em que os meios de produção sejam de todos os que os utilizam para gerar o produto social.
<!-- > (p. 5) -->

# A economia solidária no Brasil

> A economia solidária surge no Brasil, nesta etapa histórica, provavelmente como resposta à grande crise de 1981/83, quando muitas indústrias, inclusive de grande porte, pedem concordata e entram em processo falimentar. É desta época a formação das cooperativas que assumem a indústria Wallig de fogões, em Porto Alegre, a Cooperminas, que explora uma mina de carvão falida em Crisciuma (Santa Catarina) e as cooperativas que operam as fábricas (em Recife e em S.José dos Campos) da antiga Tecelagem Parahyba de cobertores. Todas elas continuam em operação até hoje.
<!-- > (p. 5) -->

> A equipe que melhor desenvolve esta tecnologia tem sua origem na antiga Secretaria de Formação do Sindicato dos Químicos de São Paulo, onde tinha por missão agir dentro das empresas «conscientizando os trabalhadores, avaliando a sociedade em seu conjunto e os políticos, a partir do que representavam do ponto de vista dos interesses da classe dominante nacional e internacional» (Anteag, 2000: 15). Em 1991, muda a diretoria do Sindicato dos Químicos e a Secretaria de Formação é fechada.
<!-- > (p. 6) -->

# O caso da Makerly

> Os trabalhadores encamparam a idéia do sindicato e se propuseram a adquirir o maquinário dos donos da Makerly por 600000 dólares. Para conseguir o crédito correspondente do Banespa (Banco do Estado de São Paulo, banco oficial do Estado de São Paulo, hoje vendido ao Santander) foi necessária intensa luta política, que culminou com a ocupação da sede do Banespa em Franca. Após 91 dias de pressão e negociações, assinou-se um acordo pelo qual, como garantia do empréstimo, 49% das ações da empresa ficaram com o banco. Por esse acordo, a Makerly teve de continuar sendo uma sociedade anônima e não uma cooperativa. Controlada pelos trabalhadores, a empresa funcionou nos anos seguintes com êxito, até que em Março de 1995 o governo federal interveio no Banespa e suspendeu a linha de crédito à Makerly, o que impôs o encerramento de suas atividades.
<!-- > (p. 7) -->

> A experiência da Makerly foi a base que permitiu desenvolver uma metodologia de transferência de empresas capitalistas a seus empregados. «Gente de todo o país, sindicalistas, políticos, trabalhadores, imprensa, todos iam até Franca para conhecer a experiência que eles denominaram ‘fábrica de trabalhador’» (Anteag, 2000: 56). Outras empresas, em geral grandes e antigas, entraram em crise e acabaram se tornando autogestionárias: Cobertores Parahyba, Facit, Hidro-Phoenix, etc. Em 1994, foi realizado em São Paulo o 1º Encontro dos Trabalhadores em Empresas de Autogestão, em que participaram representantes de seis empresas. Neste encontro decidiu-se criar a Anteag (Associação Nacional dos Trabalhadores em Empresas de Autogestão e Participação Acionária).
<!-- > (p. 7) -->


# Etapas para apropriação de empresas em falência

> A primeira é ganhar a anuência dos próprios trabalhadores, que precisam se propor a trocar seus créditos trabalhistas por cotas de capital da «sua» nova empresa, o que só acontece se eles acreditarem de que são capazes de assumir coletivamente a gestão da empresa em crise e reabilitá-la.
<!-- > (p. 8) -->

> Em geral, nos casos em que a refundação da empresa nas mãos dos trabalhadores dá certo, o operariado se divide entre uma maioria que se engaja na proposta e uma minoria que se recusa. O fato da massa falida ser mantida em funcionamento pela nova firma preserva o seu valor, o que é vantagem de todos os credores, inclusive dos trabalhadores que não querem integrá-la, pois estes também acabam recebendo uma fração maior dos seus créditos trabalhistas.
<!-- > (p. 9) -->

> A segunda etapa é conseguir que o patrimônio da firma passe para os trabalhadores associados, o que muitas vezes requer um crédito, cuja garantia é o próprio patrimônio transacionado. Em geral, crédito volumoso de prazo longo só pode ser obtido em bancos oficiais, o que depende de uma decisão política de sua direção. «Arrancar» tal decisão exige em geral forte mobilização e intensa pressão sobre ela, que no caso da Makerly (como vimos) tomou a forma de ocupação da sede do banco.
<!-- > (p. 9) -->

> A terceira etapa consiste na viabilização da nova empresa mediante a recuperação da clientela, dos fornecedores e dos créditos da antiga empresa. Os primeiros tempos são muito duros pois os trabalhadores têm de acumular capital de giro, o que significa que durante certo período eles não vão ter a retirada «cheia» (nível almejado de ganho mensal, em geral igual ao que tinham quando empregados) mas muito menos. É o chamado «período heróico», que pode durar meses, em que os trabalhadores às vezes não conseguem sequer um rendimento de subsistência.
<!-- > (p. 9) -->

# Sucesso da transformação de firmas falidas em empresas solidárias

> Por surpreendente que seja, a grande maioria das tentativas de transformar firmas meio ou inteiramente falidas em empresas solidárias tem tido sucesso. Ele se explica em primeiro lugar pelos sacrifícios feitos pelos cooperadores, que se dispõem a trabalhar durante meses por ganhos mínimos, algumas vezes apenas em troca de cestas básicas (conjunto padronizado de alimentos que devem suprir as necessidades essenciais duma família por determinado período). Mas também pela enorme dedicação e amor ao trabalho não mais alienado, do que resultam aumentos inesperados de produtividade e grande redução de perdas e desperdícios. E finalmente pelo aprendizado por parte dos novos administradores das técnicas e manhas da gestão de comprar e vender, de receber e dar crédito, de inovar produtos e processos e de tecer relações solidárias com outras autogestões.
<!-- > (p. 10) -->

# O surgimento da Anteag e os sindicatos

> Se, por um lado, as relações de solidariedade entre trabalhadores, o apoio de alguns sindicatos às suas iniciativas eram fundamentais, por outro não eram suficientes. Havia necessidade de articular pessoas e instituições, democratizar informações, criar um espaço para o debate e produção de alternativas. Enfim, havia a necessidade de uma entidade que assumisse esses papéis. Era o começo da Anteag (Nakano, 2000: 68).
<!-- > (p. 8) -->

> A Anteag se origina do movimento sindical e acabou se transformando numa organização de apoio, independente do sindicalismo mas permanentemente envolvida em parcerias com sindicatos empenhados na conversão de empresas capitalistas em solidárias.
<!-- > (p. 11) -->

# O 'novo sindicalismo' e o sindicato dos metalurgicos do ABC

> Estes sindicatos foram o dos Metalúrgicos do ABC e dos Químicos do ABC. O dos Metalúrgicos é o mais poderoso, pois representa os trabalhadores da indústria automobilística, que até recentemente concentrava a maioria das montadoras em São Bernardo do Campo, com numerosas fábricas de componentes localizadas nos municípios vizinhos. Foi o Sindicato de Metalúrgicos que organizou em 1978, em pleno regime militar, uma greve com ocupação da fábrica que surpreendentemente acabou não sendo reprimida, o que foi o sinal de que a partir de então o direito de greve voltava a ter vigência no Brasil. O que desencadeou enorme vaga de greves que atingiu o país inteiro e deu fama nacional ao chamado «novo sindicalismo», do qual Lula se tornou a figura emblemática.
<!-- > (p. 11) -->

> É no contexto de «um sindicalismo propositivo, que formula propostas de intervenção nas políticas públicas, nas políticas industriais e setoriais e nas mudanças conduzidas nas fábricas» (Oda, 2000: 94) que dá para entender que o Sindicato dos Metalúrgicos também tenha se engajado, relativamente cedo, no movimento da economia solidária. Já em seu 2º Congresso, em 1996, o sindicato resolveu discutir com os trabalhadores a formação de cooperativas, autogestão, etc. como meios de garantir a manutenção de postos de trabalho. O Congresso resolveu que poderiam ser sócios do sindicato todos os trabalhadores da categoria (inclusive cooperadores) e não apenas assalariados formais, como era a regra praticamente geral no Brasil até então.
<!-- > (p. 12) -->

# O surgimento da Unisol

> Uma peça-chave no projeto do Sindicato dos Metalúrgicos do ABC, em relação às cooperativas, tendo como referência as experiências internacionais, é a constituição de uma associação a União e Solidariedade das Cooperativas do Estado de São Paulo (Unisol Cooperativas). Esta entidade, que foi politicamente lançada durante o encerramento do 3º Congresso dos Metalúrgicos do ABC, tem fortes vínculos com o movimento sindical, com partidos políticos e com outras entidades da sociedade civil (Oda, 2000: 102). A Unisol surge em 1999, como possível rival da Anteag: propõe-se os mesmos objetivos e inevitavelmente acabará desenvolvendo atividades semelhantes. Formalmente, constitui também uma associação de cooperativas, que embora por enquanto se localizam todas no ABC paulista, pretende abranger entidades de todo o Estado de S. Paulo. A Unisol conta com uma Incubadora de Cooperativas Populares, suportada pela Prefeitura de Santo André e ligada à Fundação Santo André (instituição municipal de ensino superior). As cooperativas criadas e amparadas pela Incubadora possivelmente se integrarão à Unisol, que tenderá a se expandir, impulsionada pelos mesmos fatores que explicam o grande crescimento da Anteag: o desemprego em massa, a intensificação da concorrência que leva empresas antigas e de envergadura à crise e eventualmente à falência; o êxito das cooperativas de produção que sucederam a empresas que fecharam possivelmente reforça a confiança dos trabalhadores de que em suas mãos elas têm grandes chances de ressuscitar.
<!-- > (p. 14) -->

# O caso da Conforja

> As cooperativas de serviço não vingaram porque não houve quem convencesse os empregados dos setores técnico e administrativo a dar o salto mortal e se tornarem patrões de si mesmos. Os principais líderes que haviam integrado a Assecon já estavam nas cooperativas de produção. Além disso, tecnocratas e burocratas são mais apegados a hierarquia, já que ocupam nela posições privilegiadas. É entendível que em sua maioria tivessem optado por enfrentar as agruras dum mercado de trabalho com excesso de oferta em vez de se igualar aos trabalhadores manuais num arranjo autogestionário que ainda não havia comprovado sua «eficiência».
<!-- > (p. 19) -->

> Apesar das óbvias dificuldades que algumas das cooperativas enfrentam, os sócios têm atingido o seu objetivo imediato, qual seja preservar seus postos de trabalho e alcançar remunerações compatíveis com o trabalho que realizam. A informação disponível a este respeito refere-se à Cooperlaf, onde a retirada média em Julho de 2000 era de R$ 1.094,86, que se compara favoravelmente com o salário médio do metalúrgico do ABC que era então de R$ 1.051,63. (Oda, 2001: 111) Considerando-se a situação desesperadora da Conforja, que levou afinal a sua falência, é provável que o desempenho econômico das cooperativas deva ser considerado uma melhora, que com o passar do tempo deverá se acentuar.
<!-- > (p. 22) -->

> De uma forma geral, a prática da autogestão tornou-se habitual nas quatro cooperativas. Em cada uma delas, além do Conselho Administrativo estatutário, há um coordenador geral que de fato exerce a chefia. São pessoas que já exerciam posição de liderança na Conforja, sendo todos ex-chefes ou engenheiros e ex-integrantes da Assecon. Cabe-lhes articular os processos produtivos e supervisionar os demais sócios, estando entre suas funções «indicação para aumento de retiradas/enquadramentos de funções/cargos, definição e cumprimento de sobrejornadas quando necessárias, definição de prioridades da produção para atendimento de clientes/interesses financeiros da cooperativa, entre outros» (Oda, 2001: 89). Embora os coordenadores disponham de grande poder, suas decisões em geral são submetidas às assembléias gerais, que costumam ser numerosas. Entre Março de 1998 e Fevereiro de 2000, as 4 cooperativas realizaram nada menos de 120 assembléias, com um comparecimento médio que varia entre 70,4% na Coopertratt e 77,8% na Coopercon. É interessante notar que as duas cooperativas com pior desempenho (Cooperfor e Coopercon) são as que registram maiores índices de comparecimento, o que parece indicar que a participação dos sócios é mais intensa exatamente porque os problemas a serem enfrentados são mais graves. As assembléias dão ampla oportunidade aos sócios de inserir itens na agenda e de se manifestar.
<!-- > (p. 22) -->

> O caso da Conforja é muito revelador das potencialidades que a transformação de empresas capitalistas em crise em cooperativas de produção encerra. Uma grande parte das hesitações e resistências dos trabalhadores a se lançar em tal aventura se deve ao seu ineditismo. Com o tempo e a experiência acumulada em cooperativas e entidades de apoio, como a Anteag e a Unisol, é de se esperar que a percepção das vantagens para os trabalhadores de passar da condição de asalariados subalternos a sócios com plenos direitos de participação nas decisões se generalize. É curioso que outros atores, como os detentores do capital ou de direitos sobre a massa falida, sejam mais rápidos em perceber as vantagens para eles em transferir aos ex-empregados os direitos e responsabilidades sobre o capital físico, pelo simples fato de que só sua utilização contínua garante sua manutenção material e portanto a preservação de seu valor.
<!-- > (p. 23) -->

# MST

> A agricultura de subsistência praticada em pequenas propriedades familiares não consegue melhorar o padrão de vida dos camponeses e alguns são obrigados a entregar a terra. A partir de 1986, começa a discussão de como organizar os assentados, com o I Encontro Nacional de Assentados, em que estiveram representados 76 assentamentos de 11 estados. Apesar da resistência inicial ao cooperativismo «pelas experiências negativas do modelo tradicional do cooperativismo, caracterizado como grandes empresas agroindustriais que desenvolveram uma política de exploração econômica dos agricultores» (CONCRAB, 1999: 6), a discussão evoluiu a favor do cooperativismo, em termos que hoje diríamos serem os da economia solidária.
<!-- > (p. 24) -->

> Em 1989, o MST passa a tentar organizar a produção nos assentamentos através de Laboratórios Organizacionais, metodologia desenvolvida por Clodomir de Morais a partir da experiência das Ligas Camponesas e que visa a formação de cooperativas de produção autogestionárias. Criam-se ainda em 1989 as primeiras CPAs (Cooperativas de Produção Agropecuária) no Rio Grande do Sul: a COOPANOR e a COOPTIL. Nesta fase, a motivação para organizar a cooperação passa a ser econômica (acumular capital) e política (liberar quadros e procurar sustentar o MST) (CONCRAB, 1998: 31).
<!-- > (p. 25) -->

> Os documentos do próprio movimento registram que houve uma ruptura em 1989: Pela primeira vez formulam-se linhas políticas para a organização dos assentados e para a organização da produção. [...] Surge o desafio de fazer uma produção que envolvesse a subsistência e o mercado. O problema da produção passava a ser tão importante como ocupar. [...] Percebeu-se que os pequenos coletivos e as grandes associações não conseguiam fazer avançar a produção, ora porque eram muito pequenas, ora por não se guiarem por critérios econômicos (CONCRAB, 1998: 29).
<!-- > (p. 25) -->

> A política do MST em relação aos seus assentamentos se consolida em 1991/2 com a criação do Sistema Cooperativista dos Assentados, formado em cada assentamento por Cooperativas Agro Pecuárias, Cooperativas de Comercialização Regionais, Grupos Coletivos e Associações; em nível estadual, estabeleceram-se Cooperativas Centrais de Reforma Agrária e em nível nacional criou-se a CONCRAB (Confederação das Cooperativas de Reforma Agrária do Brasil Ltda.), em 15 de Maio de 1992, em Curitiba.
<!-- > (p. 25) -->

> As CPAs unificam os lotes de terra dos membros e trabalham o conjunto deles de acordo com um plano de produção. Sendo esta coletiva, a repartição do produto em natura e em dinheiro tinha de se orientar pela contribuição de cada um, avaliada de alguma maneira. Passou-se assim dum modelo bastante individualista, em que o pequeno agricultor tem toda a autonomia e se expõe a todos os riscos, para um modelo totalmente coletivista, em que cada cooperador participa num trabalho socializado, de acordo com uma divisão de trabalho previamente planejada.
<!-- > (p. 25) -->

> Nas CPAs, criaram-se creches, refeitórios coletivos, possibilitando a participação das mulheres na produção. [...] A integração com grandes agroindústrias, opção e condição de algumas cooperativas, possibilitou o acesso ao capital e ao conhecimento e qualificação da mão de obra dos agricultores. A capitalização das CPAs leva os assentados das CPAs estarem em média com renda maior que os individuais e a capitalização é em média 10% superior. O padrão de vida é superior na maioria dos lugares ao de muitas famílias que vivem empregadas na cidade, considerando a produção e renda monetária. É em média também superior aos posseiros, meeiros e até grande parcela de pequenos agricultores que ainda resistem no campo (CONCRAB, 1999: 24-25).
<!-- > (p. 26) -->

> O fracasso das CPAs foi causado possivelmente porque a maioria dos assentados prefere a pequena produção de mercadorias, mesmo que ela resulte em menor padrão de vida e maior risco, dada a grande oscilação dos preços dos produtos agrícolas. Nas cooperativas que se formam a partir da crise de empresas industriais, os associados sempre trabalharam coletivamente sob o comando do capital, o que os deixou conscientes de que dividir o empreendimento em pequenas oficinas individuais seria pouco factível e uma receita segura de fracasso econômico. Além disso, não têm dificuldade de avaliar a contribuição de cada um ao produto e portanto de definir regras de repartição entre eles do rendimento obtido. A situação dos trabalhadores rurais é, neste sentido, completamente diferente. O trabalho agrícola é feito geralmente em pequenas unidades, mesmo quando se realiza sobre terra alheia, arrendada, cedida ou ocupada. São poucas, no agro brasileiro, as empresas agrícolas integradas análogas às fábricas. Além disso, cada assentado é dono dum lote individual que recebe do Incra. Portanto, a alternativa de trabalhar autonomamente é factível, além de ser o modelo «natural» no campo brasileiro. Não deve surpreender portanto que a primeira tentativa de implantar a economia solidária mediante a reforma agrária tenha fracassado em parte.
<!-- > (p. 27) -->

> Não obstante, o MST continua empenhado em desenvolver uma agricultura moderna nos assentamentos que conquistou e sabe que esta meta exige um grau avançado de cooperação entre os agricultores. Reconhecendo que a CPA não é desejada pela maioria dos assentados, o movimento fez um recuo tático: em vez de priorizar a CPA unicamente, ele passou a desenvolver outras formas de cooperação, como as cooperativas de comercialização, que preservam a individualidade do camponês, mas permitem organizar compras e vendas em comum, com palpáveis vantagens para todos em termos de preços. Além disso, estas cooperativas (apelidadas de CPSs: Cooperativas de Prestação de Serviços) ajudam a mecanizar a agricultura mediante a compra em comum de equipamentos caros como tratores, colheitadeiras e permitem o desenvolvimento de agro-indústrias
<!-- > (p. 27) -->

> Para o MST o que importa é que todos os assentados participem de uma experiência de cooperação, rompendo assim com o isolamento. Pois a cooperação tem como objetivo principal o desenvolvimento da produção. Ela visa contribuir com o avanço da organização da produção em vista da melhoria da qualidade de vida das famílias assentadas. Uns podem apenas trocar dias de serviço. Outros podem comercializar em conjunto. Outros podem ter uma associação de máquinas. Outros podem ter alguma linha de produção em comum. Outros podem estar em grupos coletivos. Outros podem estar ligados a uma cooperativa. Outros estão em uma cooperativa totalmente coletiva (CONCRAB, 1998: 50).
<!-- > (p. 28) -->

> O contínuo de solidariedade, construído desta forma, é um modelo que o movimento popular poderá desenvolver nas cidades. Cada modalidade de cooperação combina em graus diferentes autonomia individual com trabalho coletivo e depende tanto da vontade dos membros como das características da produção. Na agricultura mesmo há ramos de produção como o cultivo de morangos ou a criação de pequenos animais em que o trabalho individual ou familiar tende a ser mais eficaz que o trabalho coletivo em grande escala, que por sua vez é provavelmente superior nas plantações de cereais altamente mecanizadas e quimificadas.
<!-- > (p. 28) -->

> De uma forma geral, os depoimentos colhidos pelas duas pesquisas junto a assentados que permaneceram na cooperativa e junto aos que a deixaram deixam entrever três motivações principais para o abandono: 1. O magro retorno econômico a um esforço de investimento relativamente grande. Muitos depoimentos falam da frustração com o ganho insuficiente mas reconhecem o imediatismo dos que desistem, pois é preciso mais tempo para colher os frutos. 2. O descontentamento com a falta de incentivos aos que trabalham mais e produzem melhor. As retiradas são calculadas pelo número de horas trabalhadas, sem distinguir diferenças de esforço nem de resultado. Isso leva alguns a «amolecer o corpo» primeiro e depois optar pelo lote individual, onde esforço e produtividade não são compartilhados e nem os ganhos uniformizados pela média. 3. Insatisfação com o papel do trabalho familiar no coletivo: só adultos são sócios e ganham de modo que as famílias com filhos pequenos têm de sustentá-los à sua própria custa; além disso, o pai perde o seu poder de «chefe» da família e esta perde o poder de decidir sobre sua produção e seu trabalho.
<!-- > (p. 33) -->

> A pesquisadora apresenta como conclusão sua que fazendo algumas comparações externas, como por exemplo, entre favelados urbanos e esses assentados, podemos afirmar que sua condição de sobrevivência é superior as dos primeiros, pois se alimentam bem, têm casa, boa saúde, trabalho. Não possuem um padrão de vida ideal, mas estão muito distantes da miserabilidade das favelas (Sizanoski, 1998: 55). Maria Antônia de Souza apresenta avaliação idêntica: «Com relação ao funcionamento da cooperativa e aos avanços obtidos no assentamento, é notória a melhoria na qualidade de vida das famílias, da construção das casas, da infra-estrutura e dos setores de produção» (Souza, 1999: 145).
<!-- > (p. 34) -->

> A Copavi (Cooperativa de Produção Agropecuária Vitória) foi fundada por 25 famílias, que haviam sido arrendatárias. Por ocasião da pesquisa de Maria Antônia de Souza, havia 19 famílias, «sendo que 4 em processo de experiência - constitui uma espécie de estágio no coletivo, onde a família insere-se num setor de produção e na organização do assentamento como um todo, passando a conhecer as normas da cooperativa» (Souza, 1999: 148). Esta é uma prática nos assentamentos do MST: «Os lugares deixados pelas famílias desistentes são ocupados por outras famílias, que se interessam pelo coletivo e que passam por um ano de experiência no assentamento» (Souza, 1999: 146).
<!-- > (p. 35) -->

> O propósito dos cooperadores era tanto econômico quanto político. O regimento interno Copavi determina como seus objetivos ser uma cooperativa de produção, comercialização e industrialização em vistas de organizar o trabalho dos seus sócios, liberar mão de obra para contribuir ao MST e SCA, ser uma organização social de reivindicação e de luta em favor da reforma agrária e de interesse de seu quadro social, dar exemplo através dos resultados econômicos e social de que a reforma agrária dá certo; especialização da mão de obra, garantir a participação nas decisões, execução, controle e divisão das sobras através da gestão democrática (Souza, 1999: 149).
<!-- > (p. 35) -->

> A deficiência técnica, que parece ter sido geral nas CPAs, tornou-se um impedimento para a industrialização dos produtos agropecuários. É provável que a qualificação profissional da nova geração permita superar este problema. Mas, por enquanto, ressurge o «imediatismo», a impaciência com o retorno dos investimentos, que neste depoimento adquire conotação política: o ganho modesto pode dar a impressão que o experimento fracassou, o que seria grave para uma cooperativa que colocou entre seus objetivos «dar exemplo através dos resultados econômicos e social de que a reforma agrária dá certo».
<!-- > (p. 36) -->

> Uma maneira encontrada pelo MST para facilitar a integração das famílias no coletivo é sua organização em «núcleos de família», formados por vizinhos. A agrovila é formada por fileiras de casas, cada uma dando origem a um núcleo. Este desenvolve leituras, discute os problemas vivenciados, a prestação de contas, o planejamento das atividades. «Outro espaço para discussão é o restaurante coletivo.
<!-- > (p. 36) -->

> Durante o horário do café da manhã fazem se consultas às pessoas, encaminhamentos e informes». Estas modalidades de integração suprem as deficiências das assembléias, feitas mensalmente, pois, conforme afirma o presidente da cooperativa, «a participação das pessoas não é o que deveria ser, elas têm dificuldades para discutir, opinar» (Souza, 1999: 149).
<!-- > (p. 36) -->

> Esta parece ser uma dificuldade em muitos empreendimentos solidários, não só em cooperativas rurais do MST. Camponeses e operários são pessoas humildes, que se intimidam diante dum auditório maior e por isso raramente ou nunca falam em assembléias. Mas tentam manifestar seus pontos de vista através de companheiros mais desinibidos, com os quais confabulam em grupos menores. Daí a importância dos núcleos de família e da confabulação informal durante o café da manhã, no refeitório.
<!-- > (p. 37) -->

> Os três estudos de caso evidenciam as dificuldades de implantar formas avançadas de cooperação, nos assentamentos orientados pelo MST, tanto por questões culturais - a preferência da maioria dos assentados pela agricultura familiar, em moldes tradicionais - como por questões econômicas. As famílias se deixaram convencer da superioridade das cooperativas de produção pela sua maior facilidade em adquirir equipamentos e máquinas portadoras de tecnologia avançada. Esperavam que o «sacrifício» de seus recursos do Procera, cedidos ao fundo comum, resultasse em receitas abundantes, o que em nenhum dos dois casos - Cooproserpe e Copavi - aconteceu, ao menos nos primeiros anos de vida das cooperativas. A frustração destas expectativas talvez tenha sido o mais importante dos motivos para a desistência das famílias que optaram pela produção individual e comercialização associada.
<!-- > (p. 37) -->

> O I Censo da Reforma Agrária - 1997 mostra o predomínio nos assentamentos da produção individual: 93,96% contra apenas 1,21% de produção coletiva e 4,82% de forma mista (Souza, 1999: 150). Os estudos de caso dão uma idéia da dinâmica que levou a esta situação. O mesmo censo dá outra informação relevante: a origem social dos assentados. 66,13% eram agricultores ou camponeses, 5,67% trabalhadores rurais, «sendo o restante distribuído entre outras atividades rurais, boia-fria, motorista, mecânico, pedreiro e carpinteiro» (Souza, 1999: 152-153).
<!-- > (p. 37) -->

> Talvez as seguintes considerações da pesquisadora sirvam de conclusão: No interior do MST, a proposta de tais coletivos surge tendo como objetivo central a mudança da sociedade e do sistema capitalista. [...] No entanto, as formas idealizadas de coletivos (totalmente coletivos) não estão sendo reproduzidas nos assentamentos, enquanto que as associações de produção e de comercialização estão se proliferando. O interessante é questionar o porquê desta ocorrência e qual a influência do processo de socialização política vivenciada no momento do acampamento, pela maioria dos assentados. De um lado, conforme depoimentos dos assentados, estes sentem maior liberdade nos seus lotes individuais, embora saibam que, para sobreviver, no lote, é necessário estar agrupado. Por outro lado, destacam-se os fatores sociais e culturais, como influenciadores desta resistência ao coletivo. Por exemplo, enquanto no coletivo todos trabalham ‘igualmente’, sendo organizados em setores e coordenações de grupos, nos lotes individuais, quem orienta o trabalho e as ordens geralmente é o marido ou um filho mais velho ou ainda a esposa, em alguns casos. No coletivo, a divisão do trabalho e a repartição das sobras é semelhante ao que ocorre numa empresa e as normas de funcionamento são aplicadas de acordo com o previsto no regimento interno. Ou seja, os coletivos exigem uma ruptura sócio-cultural de um paradigma anterior de trabalho e de família. O ‘novo’ é tido como algo muito diferente do vivido anteriormente, principalmente para os ex-pequenos agricultores. Geralmente é bastante aceito entre os jovens, cujo interesse é trabalhar com maquinários e industrialização de produtos. [ênfase minha] (Souza, 1999: 163-164).
<!-- > (p. 38) -->

# Cáritas

> A Cáritas Brasileira é uma instituição da Igreja Católica, sendo parte da rede de Cáritas Internacional. Ela tem por fim dar sustentação à ação social da Igreja e está orgânicamente ligada à CNBB [Conferência Nacional dos Bispos do Brasil]. Tem um secretariado nacional em Brasília, que coordena uma rede de Cáritas Diocesanas e Regionais. A Cáritas desenvolve suas atividades com fundos gerados no Brasil pela Campanha de Solidariedade, que é permanente, e com fundos doados pelas Cáritas e outras instituições confessionais do 1º Mundo voltadas para a cooperação internacional.
<!-- > (p. 39) -->

> Há que se distinguir nas ações da Cáritas três grandes ênfases [...]: a ênfase assistencial, a promocional e a da solidariedade libertadora. A ênfase assistencial data de 1956 [...] a Cáritas encarregou-se de articular as obras sociais de inspiração católica para promover a distribuição dos donativos e alimentos, especialmente o leite em pó americano. [...] A ênfase promocional tem início em 1966. As inquietações advindas das contradições do programa de distribuição de alimentos no contexto do regime militar instaurado resultam em processo de mudança [...] O lema ‘ensinar a pescar’ contrapunha-se ao ‘dar o peixe’, próprio da fase anterior. Experiências diversificadas de ações comunitárias do tipo das comunidades eclesiais de base, das associações de desenvolvimento comunitário, do cooperativismo, dão sustentação prática à reflexão sobre o desenvolvimento. [...] A ênfase à solidariedade libertadora, atual fase da Cáritas, privilegia um enfoque, um ponto de partida sobre o qual atua seja nas situações de emergência, seja no apoio às iniciativas comunitárias ou associativistas, seja no apoio às mobilizações populares [...] A premência para implementar ações através de projetos que respondessem às reais necessidades da comunidade levou à opção pela linha de apoio aos Projetos Alternativos Comunitários (PACs) como expressão de compromisso social com o povo e como uma demonstração visível de que os trabalhadores organizados e apoiados têm uma saída para suas condições de miséria (Bertucci, 1996: 60-62).
<!-- > (p. 39) -->

> Este relato sintetiza a imensa evolução da Igreja Católica duma ação meramente assistencial a uma postura de crítica ao capitalismo, com a proposição de que a solidariedade liberta. Ela implica numa tese ousada: a de que os trabalhadores, desde que se organizem e granjeiem apoio, podem por si só superar a miséria. Uma das implicações desta tese é que este apoio não tem de ser do Estado, pressuposto geral de todas as correntes de esquerda até então. A Cáritas passou a apoiar milhares de Projetos Alternativos Comunitários (PACs) por todo Brasil, desde 1984, contando com a ajuda da Cáritas Suiça, Miserior, Cebemo, Entraide e Fraternité e Cáritas Alemã.
<!-- > (p. 40) -->

> A estratégia de vida dos milhões de excluídos passou a ser considerada como ‘alternativa de sobrevivência’. Alternativos foram também os novos movimentos sociais que emergiram como forças sociais capazes de se confrontar com o autoritarismo presente. [...] Alternativos foram ainda os novos partidos políticos oriundos dos movimentos sociais. Como foram também alternativas as Comunidades Eclesiais de Base [...]. Aos excluídos cabia a busca de soluções para seus problemas de forma alternativa àqueles tradicionais tentadas até então.
<!-- > (p. 40) -->

> O sentido revolucionário emprestado à palavra «alternativo» testemunha a notável guinada da Igreja provocada pela opção preferencial pelos pobres, isto é, pelos não possuidores de meios de produção. A nova postura de início não tinha um programa claro de como os trabalhadores podem sair da miséria pelas suas próprias forças. Por isso ela convoca as próprias comunidades a encontrar as saídas, pela aplicação do antigo mas ainda hoje indispensável método de ensaio e erro, através duma vasta multiplicação de diferentes «experiências».
<!-- > (p. 40) -->

> Os projetos alternativos, em sua maioria, são recentes na história da Cáritas - e do Estado, vale acrescentar - e denotam uma aproximação maior entre as pastorais sociais, organizações não governamentais e movimentos populares, fato que se deu no curso da última década e que foi penetrando lentamente nos diversos ambientes da Igreja Católica.
<!-- > (p. 42) -->

# A Ação pela Cidadania Contra a Miséria e pela Vida  

> O desenvolvimento de experiências de economia solidária sofreu forte aceleração em 1994, quando a Ação da Cidadania contra a Miséria e pela Vida [ACCMV] resolveu mudar sua tática e, em vez de apenas distribuir alimentos, passou também a fomentar a geração de trabalho e renda. Ela completou em dois anos a mesma evolução que a Cáritas havia feito em quinze, ao passar duma ação assistencial à «solidariedade libertadora».
<!-- > (p. 42) -->

> atividade da Cáritas, apesar de sua amplitude territorial, era desconhecida do grande público, ficando de certo modo restrita à Igreja e às comunidades mobilizadas por ela. A ACCMV era um amplo movimento de massas, o maior do Brasil desde a luta pelas eleições diretas, em 1985, no ocaso da ditadura militar. É curioso notar que de sua Secretaria Executiva Nacional tomou parte a Cáritas (representando a CNBB), ao lado da OAB, da CUT, do INESC, COFECON e da ANDIFES, o que leva a crer que a atividade da Cáritas no campo da economia solidária tenha influído na guinada da Ação a favor dela.
<!-- > (p. 42) -->

> Iniciado com 200 integrantes, a Cootram fechou o ano de 1999 com cerca de 1.200 trabalhadores cooperativados, atuando em áreas de trabalho de baixa densidade tecnológica (...) e, de forma independente das relações com a Fiocruz, a produção de material de construção e as oficinas de corte e costura. [...] O material de construção produzido tem qualidade equivalente e preço significativamente mais baixo, sendo utilizado para a melhoria das habitações populares e da infra-estrutura urbana. Os recursos financeiros nascem e circulam na própria comunidade. Trata-se, segundo muitos analistas, de uma das mais exitosas experiências de cooperativas populares de trabalho do país (Buss, 2000: 120-128).
<!-- > (p. 45) -->

> O caso da Cooperativa de Trabalho de Manguinhos é emblemático sob vários aspectos. Em primeiro lugar, porque nasce duma iniciativa da Fiocruz enquanto integrante da Campanha contra a fome, exatamente quando esta prioriza a opção pela economia solidária como meio de combate à miséria. Em seus desdobramentos, a COEP continuará participando ativamente, como será visto a seguir. Em segundo lugar, o grande êxito da Cootram se deveu à abertura do mercado de serviços da Fiocruz. Tudo leva a crer que estes mesmos serviços já eram feitos pelos moradores das favelas, na condição de assalariados das empresas privadas prestadores dos mesmos. Mas, ao substituir a empresa capitalista pela solidária, a Fiocruz pôde fazer uma bela economia, enquanto os cooperadores tiveram o seu ganho dobrado. Não resta dúvida que na prestação de serviços de baixa densidade tecnológica, a cooperativa de trabalho, ao menos em Manguinhos, é mais competitiva do que qualquer empresa capitalista análoga.
<!-- > (p. 45) -->

# Incubadoras de Cooperativas (e extensão universitária)

> Em terceiro lugar, o processo de formação da Cootram envolveu pela primeira vez universidades, no caso a ENSP e a UFSM. Uma parte da elite científica e educacional do Brasil resolveu engajar-se para ajudar a construir a economia solidária. O passo seguinte foi padronizar esta ajuda na forma das Incubadoras Tecnológicas de Cooperativas Populares. A primeira ICTP foi criada, em 1995, na COPPE/UFRJ, o centro de pós-graduação de engenharia da Universidade Federal do Rio de Janeiro, mediante convênio da COPPE com a FINEP e a Fundação Banco do Brasil, sendo as duas últimas financiadoras da nova entidade. No segundo semestre de 1995, a Incubadora apoiou a implantação da Cootram, juntamente com a Fio Cruz e a Universidade Federal de Santa Maria. A partir de 1996, ela iniciou a formação de cooperativas na Baixada Fluminense e nas favelas cariocas.
<!-- > (p. 45) -->

> A Incubadora de Cooperativas Populares veio preencher uma lacuna vital no processo de formação de cooperativas e grupos de produção associada, iniciada pela Cáritas e expandida pela ACCMV: a de prestar assessoria contínua aos empreendimentos solidários, divulgando os princípios do cooperativismo entre grupos interessados, ajudando-os a organizar atividades produtivas ou a prestação de serviços, a apurar as técnicas empregadas, a legalizar as cooperativas, a buscar mercados e financiamento, etc.
<!-- > (p. 46) -->

> As Incubadoras Universitárias decidem integrar uma rede para a troca de experiências e a ajuda mútua tendo em vista estabelecer em cada universidade não só um centro de extensão (em que se enquadra a incubação) mas também de ensino e pesquisa. O ensino é necessário para formar quadros para as próprias cooperativas e para entidades de apoio à economia solidária que continuam se multiplicando, como veremos adiante. A pesquisa é indispensável para se conhecer a realidade da economia solidária no Brasil e também no exterior, de modo a sistematizar a análise e avaliação das experiências para gerar proposições teóricas que sirvam para tornar a economia solidária mais autêntica e mais efetiva.
<!-- > (p. 47) -->

> Mas, o apoio a cooperativas autogestionárias formadas por ex-assalariados sofreu resistência de sindicalistas, que identificavam o processo com a terciarização da mão-de-obra, que se realizava cada vez mais mediante a formação de pseudo cooperativas, com a única finalidade de roubar dos trabalhadores os seus direitos trabalhistas. Como esta identificação (de cooperativas autênticas com as falsas) é absurda, a questão pôde ser esclarecida através do melhor conhecimento da natureza das cooperativas autênticas. Surgiu, no entanto, uma outra oposição à economia solidária, de natureza ideológica, que apontava a necessidade de reforçar o trabalho assalariado por ser a base social dos sindicatos e porque só a classe operária assalariada teria por missão histórica derrubar o capitalismo e instaurar o socialismo. As cooperativas eliminariam o caráter de classe dos trabalhadores, tornando-os patrões e operários ao mesmo tempo.
<!-- > (p. 48) -->

> Também esta argumentação se baseia na ignorância do que é a economia solidária. As cooperativas de produção e de trabalho são chamadas de «operária» - «worker cooperatives» - por causa de sua ligação orgânica ao movimento operário. A ANTEAG, a UNISOL e o MST não são menos operários e socialistas do que os sindicatos mais militantes. Além disso, os membros do sindicato que formam cooperativas operárias devem continuar a pertencer ao sindicato, que deveria abrir suas portas a todos os trabalhadores que não exploram trabalho alheio e queiram se filiar. O fato de no Brasil a lei definir o sindicato como representante de trabalhadores assalariados não deveria ser impedimento para que sindicatos ampliem sua abrangência, passando a representar o conjunto dos que dependem de seu próprio trabalho para subsistir.
<!-- > (p. 48) -->

## A Cresol

> Vale a pena registrar o surgimento duma rede de cooperativas de crédito no sul, o sistema Cresol, que veio a atender a necessidade de fontes próprias de financiamento dos agricultores familiares do Paraná, Santa Catarina e Rio Grande do Sul. As cooperativas de crédito do sistema tradicional são todas ligadas a cooperativas agrícolas dominadas em geral pelo capital. As Cresol são independentes, autogestionárias.
<!-- > (p. 50) -->

> Os princípios e objetivos orientadores das Cresol são: interação solidária, democratização e ampliação do acesso ao crédito e aos serviços bancários pelos agricultores familiares, descentralização e horizontalização, profissionalização do crédito, transparência e contribuição para o desenvolvimento sustentável (social, econômico e ambiental). [...] Em Dezembro de 1999, ao completar quatro anos de funcionamento, o Sistema Cresol era formado por 28 cooperativas [...] além de mais duas da Cresol esperando liberação pelo Banco Central. Está presente diretamente em mais de cem municípios [...]. O número de associados chega a 10.500 famílias de agricultores. [...] As cooperativas são criadas e compostas por agricultores familiares, sendo fortalecida por sindicatos, associações e outras formas de organização dos agricultores familiares da região onde atuam (Bittencourt, 2000: 197-199).
<!-- > (p. 50) -->

# A economia solidária no Brasil

> O que impulsiona este desenvolvimento não é mais apenas o agravamento do desemprego em massa e da exclusão social. Este foi muito provavelmente o principal fator nos anos 80 e início dos 90, quando a Cáritas e alguns sindicatos começaram a apoiar sistematicamente os esforços de trabalhadores e famílias marginalizadas de se libertar da pobreza através da solidariedade. Depois surgiram a ANTEAG, a Campanha contra a Fome, as Incubadoras de Cooperativas Populares, a Agência de Desenvolvimento Solidário. Faltaria referir ainda a formação dos Fóruns Estaduais de Cooperativas, no Rio de Janeiro, Rio Grande do Sul e São Paulo, o crescente desenvolvimento de cursos de diferentes formatos de economia solidária e assim por diante. O que impele a economia solidária a se difundir com força cada vez maior já não é mais a demanda das vítimas da crise mas a expansão do conhecimento do que é e a tecnologia social, econômica e jurídica de implementação da economia solidária. Centenas de iniciativas, que tendiam antes a ficar isoladas e por isso debilitadas, a partir dos últimos anos passam a receber a atenção e o apoio de instituições especializadas como a ANTEAG, MST, Incubadoras, Unisol, ADS e Cáritas, entre outras. O que este breve relato deixou claro é que a economia solidária já firmou sua identidade e por causa disso está em condições de se estruturar, em nível local, regional e nacional.
<!-- > (p. 50) -->

[^1]: SINGER, P. A recente ressurreição da economia solidária no Brasil. In: Boaventura de Sousa Santos (org.) _Produzir para viver: os caminhos da produção não capitalista_. Civilização Brasileira: Rio de Janeiro, 2002. Disponível em: <http://www.ceeja.ufscar.br/a-recente-ressurreicao-singer>.
