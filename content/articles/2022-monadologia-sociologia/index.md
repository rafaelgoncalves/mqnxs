---
title: Monadologia e sociologia
category: fichamento
date: 2024-05-19
path: "/monadologia-e-sociologia"
tags: 
 - Gabriel Tarde
 - Gottfried Leibniz
 - monadologia
 - sociologia
 - sociedade
 - ciência
 - química
 - biologia
 - evolucionismo
 - fichamento

featuredImage: "tarde.jpg"
srcInfo: <a href="https://commons.wikimedia.org/wiki/File:M._Tarde,_membre_de_l%27Institut,_professeur_au_Coll%C3%A8ge_de_France_CIPB0463.jpg">Eugène Pirou</a>, Licence Ouverte, via Wikimedia Commons

published: true
---

Fichamento de "Monadologia e sociologia" de Gabriel Tarde[^1].

[^1]: TARDE, Gabriel. Monadologia e sociologia. In: TARDE, Gabriel. **Monadologia e sociologia: e outros ensaios.** tradução: Paulo Neves. São Paulo, SP: Cosac-Naify, 2007. p. 51–131.

# Monadas (Leibniz) estão no coração da ciência contemporânea (séc XIX)

> As monadas, filhas de Leibniz, seguiram caminho desde seu pai. Por diversas vias independentes, elas se insinuam no coração da ciência contemporanea, sem o conhecimento dos próprios cientistas.
> (p. 53)

# Redução de matéria/espírito ao espírito na ciência contemporânea e multiplicação de agentes integralmente espirituais. Descontinuidade (de elementos) e homogeneidade (de ser).

> Com efeito, ela implica, em primeiro lugar, **a redução a uma só destas duas entidades, a matéria e a espírito, confundidas na segunda, e ao mesma tempo a multiplicação prodigiosa dos agentes integralmente espirituais do mundo**. Em outras palavras, **ela supõe a descontinuidade dos elementos e a homogeneidade de seu ser**. Aliás, e somente sob essa dupla condição que **o universo é translúcido até seu fundamento ao olhar da inteligencia**.
> (p. 53)

# Química e a descontinuidade do mundo (QUÍMICA)

> Por outro lado, os progressos da química nos conduzem à afirmação do átomo, à negação da continuidade material que o carater contínuo das manifestações físicas e vivas da materia, a extensão, o movimento, o crescimento, parecia superficialmente revelar.
> (p. 53)

> Aqui [[ nas reações químicas ]], nenhuma evolução, nenhuma transição, tudo e nítido, brusco, bem marcado; e, no entanto, tudo o que há de ondulante, de harmaniosamente graduado nos fenômenos vem daí, mais ou menos como a continuidade das matizes seria impassível sem a descontinuidade das cores.
> (p. 54)

# A noção de gravitação como soma da gravitação das massas que compõem um corpo em Newton (FÍSICA)

> Por essa ideia, bem mais original do que pode nos parecer, Newton rompia, pulverizava a individualidade do corpo celeste, considerado ate entao como uma unidade superior cujas relações internas em nada se assemelhavam a suas relações com os corpos estrangeiros. Era preciso um grande vigor de espírito para resolver essa unidade aparente em uma multiplicidade de elementos distintos, ligados entre si do mesmo modo que com os elementos de outros agregados. Os progressos da física e da astronomia datam do momento em que essa maneira de ver substituiu o preconceito contrario.
> (p. 54)

# A teoria celular e a decomposição da unidade do corpo vivo (BIOLOGIA)

> Nisto, os fundadores da teoria celular se mostraram as continuadores de Newton. Eles romperam do mesmo modo a unidade do corpo vivo, decompuseram-no em um numero prodigioso de organismos elementares, isoladamente egoístas e ávidos de se desenvolver às expensas do exterior, entendendo por exterior tanto as células vizinhas e fraternas quanto as partículas inorgânicas do ar, da água ou de qualquer outra substância. Não menos fecunda que a visão de Newton fOi a de Schwann sobre esse ponto. Sabemos, graças à sua teoria celular, que
>
> > [...] uma força vital, enquanto princípio distinto da matéria, não existe nem no conjunto do organismo, nem em cada célula. _Todos os fenômenos da vida vegetal au animal devem ser explicadas pelas propriedades dos átomos_ (entenda-se dos elementos últimos de que se compõem as átomos), quer sejam forças conhecidas da natureza inerte _ou forças desconhecidas até este momento_.
>
> (p. 54-5)

# Negação do princípio vital como positivismo. Espiritualismo leibniziano como consequência. Dispersão ao infinitesimal.

> **Seguramente, nada mais positivista, nada mais conforme à ciência sadia e séria do que essa negação radical do princípio vital**, negação contra a qual o espiritualismo vulgar costuma protestar. Percebe-se, porém, aonde nos conduz essa tendência levada ao extremo: às mônadas que realizam a promessa mais ousada do espiritualismo Leibniziano. **Assim como o princípio vital, também a doença, outra entidade, tratada como uma pessoa pelos antigos médicos, pulveriza-se em desordens infinitesimais de elementos histológicos**; além disso, graças, sobretudo, às descobertas de Pasteur, a teoria parasitária das doenças, que explica essas desordens por confiitos internos de organismos minúsculos, generaliza-se cada vez mais, e mesmo com um excesso que deve provocar uma reação. Mas os parasitas têm também seus parasitas. **E assim por diante. Ainda o infinitesimal!**
> (p. 55)

# Potência individual inventiva na revolução política ou social (que seria mais do que consequência de um intelectual, etc., emergiria das entranhas do povo) (HISTÓRIA)

> Assim como os astros, os indivíduos vivos, as doenças e os radicais químicos, também as nações não são mais do que entidades há muito vistas como seres verdadeiros nas teorias ambiciosas e estéreis dos historiadores ditos filósofos. **Já não se repetiu bastante, por exemplo, que é uma mesquinharia buscar a causa de uma revolução política ou social na influência marcada de escritores, homens de Estado, inventores de todo tipo, e que ela brota espontaneamente do gênio da raça, das entranhas do povo, ator anônimo e sobre-humano?** Mas esse ponto de vista cômodo, que consiste em ver falsamente a criação de um ser novo no fenômeno, este sim realmente novo e imprevisto, que o encontro dos verdadeiros seres suscitou, só é bom a título provisório. Uma vez esgotado, e rapidamente, pelos abusos literários cometidos, ele conduz a um retorno sério à explicações mais claras e mais positivas, **que dêem conta de um acontecimento histórico qualquer por ações individuais apenas, e especialmente por ações de homens inventivos, que serviram de modelo aos outros e se reproduziram em milhares de exemplares, espécies de células-mãe do corpo social**.
> (p. 56)

> Mas esse ponto de vista comodo, que consiste em ver falsamente a criação de um ser novo no fenomeno, este sim realmente novo e imprevisto, que o encontro dos verdadeiros seres sucitou, so é bom a titulo provisorio. **Uma vez esgotado, e rapidamente, pelos abusos literários cometidos, ele conduz a um retorno sério a explicações mais claras e mais positivas, que deem conta de um acontecimento histórico qualquer por ações individuais apenas, e especialmente por ações de homens inventivos, que serviram de modelo aos outros e se reproduziram em milhares de exemplares, especies de celulas-mae do corpo social.**
> (p. 56)

# Elementaridade do átomo, da célula e o indivíduo como limite do ponto de vista de disciplinas particulares

> Isso nao é tudo: esses elementos últimos aos quais chega toda ciencia, o indivíduo social, a celula viva, o alomo químico, só sao ultimos da perspectiva de sua ciencia particular.
> (p. 57)

# Tendência pelo infinitesimal inclusive no evolucionismo

> Não há meio algum de se deter nessa inclinação para o infinitesimal, que se torna, de modo sem dúvida muito inesperado, a chave do universo inteiro. Daí talvez a importância crescente do cálculo infinitesimal; daí também, pela mesma razão, o espantoso sucesso momentâneo da doutrina da evolução. Nessa teoria, um tipo específico, diria um geômetra, é a integral de inumeráveis diferenciais chamadas variações individuais, devidas elas próprias a variações celulares, no fundo das quais aparecem miríades de mudanças elementares. A fonte, a razão de ser, a razão do finito, do definido, está no infinitamente pequeno, no imperceptível: tal é a convicção profunda que inspirou Leibniz e também nossos transformistas.
> (p. 58)

# Dois exemplos da explicação infinitesimal

> Mas por que uma transformação, incompreensível quando apresentada como uma soma de diferenças nítidas, definidas, é facilmente compreendida se a consideramos como uma soma de diferenças infinitamente pequenas? Mostremos em primeiro lugar que esse contraste é bastante real. Suponho que, por milagre, um corpo desapareça, se aniquile no lugar A onde estava, para depois reaparecer, _voltar a ser_ no lugar Z, a um metro de distância do primeiro, _sem ter atravessado as posições intermediárias_: esse tipo de _deslocamento_ não encontra abrigo em nosso espírito, ao passo que não nos surpreendemos de ver esse corpo passar de A a Z seguindo uma linha de posições justapostas. Assinalemos também que nosso primeiro espanto _em nada_ teria diminuído se tivéssemos visto o desaparecimento e o reaparecimento bruscos efetuarem-se a uma distância de meio metro, de trinta, de vinte, de dez, de dois centímetros ou qualquer fração perceptível de milímetro. Nossa razão, ou mesmo nossa imaginação, ficaria tão chocada com o último caso quanto com o primeiro. Do mesmo modo, se nos apresentarem duas espécies vivas distintas, muito distantes ou muito próximas, não importa, digamos um fungo e uma labiada, ou duas labiadas de um mesmo gênero, nunca chegaremos a compreender, não mais aqui do que lá, que uma tenha podido subitamente e sem transição transformar-se na outra. **Mas se nos disserem que, em virtude de um cruzamento, o óvulo fecundado de uma sofreu um desvio, extremamente pequeno de início, mas depois gradativamente maior, de seu itinerário habitual, não teremos nenhuma dificuldade de admitir isso. Dirão que o inconcebível da primeira hipótese deve-se a um preconceito formado em nós por associação de idéias. O que é muito verdadeiro e prova, justamente, que a realidade, fonte da experiência na qual nasceu esse preconceito, está em conformidade com a explicação do finito pelo infinitesimal**.
> (p. 58-9)

# Difusão de luz, células e ideias inventivas (vibração, geração e imitação)

> Para falar apenas dos mais vulgares, verifica-se que uma imensa esfera de luz expandida no espaço deve-se a vibração unica, multiplicada e contagiosa, de um atomo central de eter - que toda a população de uma especie deve-se a multiplicação prodigiosa de uma primeira e única celula ovular, especie de irradiação geradora -, que a presença cia verdadeira teoria astronômica em milhões de cerebros humanos deve-se a repetição multiplicada de uma ideia surgida, certa dia, em uma celula do cerebro de Newton. 
> (p. 59-60)

# Diferença qualitativa entre o infinitesimal e o finito (monismo infinitesimal)

> Se _o infinitesimal diferisse do finito apenas por grau_, se tanto no fundo das coisas como em sua superfície apreensível houvesse apenas posições, distâncias, deslocamentos, por que um deslocamento, inconcebível como finito, mudaria de natureza ao tornar-se infinitesimal? Logo, **o infinitesimal difere qualitativamente do finito; o movimento tem uma causa diferente dele mesmo; o fenômeno não é todo o ser. Tudo parte do infinitesimal e tudo a ele retorna; nada, coisa surpreendente que não surpreende ninguém, nada aparece subitamente na esfera do finito, do complexo, nem nela se extingue**. Que concluir daí, senão que **o infinitamente pequeno, ou seja, o elemento, é a fonte e a meta, a substância e a razão de tudo**? ~ Enquanto o progresso da física leva os físicos a _quantificar_ a natureza para compreendê-la, **é significativo que o progresso da matemática conduza os matemáticos, para compreender a quantidade, a decompô-la em elementos que não têm absolutamente nada de quantitativo**.
> (p. 60)

# A importância dada ao infinitesimal (afastada da hipótese das mônadas) levaria a um simple amontoado de contradições

> Essa importancia crescente atribuída pelo aumento dos conhecimentos ao infinitesimal e tanto mais estranha quanto, em sua forma ordinária (afastada a hipótese das monadas), ele e um simples amontoado de contradições. 
> (p. 60)

# Mônadas como agentes, variações infinitesimais como ações

> Seja como for, seriam então os verdadeiros _agentes_ esses pequenos seres que dizemos serem infinitesimais, seriam as verdadeiras _ações_ essas pequenas variações que dizemos serem infinitesimais.
> (p. 61)

# Caráter contraditório das interações infinitesimais geraria transformações graduais (aparentemente contínuas). Matéria ativa agindo infiniterimalmente.

> Do que precede, parece mesmo resultar que esses agentes são autônomos, que essas variações se chocam e se obstruem tanto quanto cooperam. **Se tudo parte do infinitesimal é porque um elemento, um elemento único, tem a iniciativa de uma mudança qualquer, movimento, evolução vital, transformação mental ou social. Se todas essas mudanças são graduais, e aparentemente contínuas, isso mostra que a iniciativa do elemento empreendedor, embora secundada, encontrou resistências**. Suponhamos que todos os cidadãos de um Estado, sem exceção, adiram plenamente a um programa de reorganização política nascido no cérebro de um deles e, mais especialmente, em um ponto desse cérebro. A reforma inteira do Estado segundo esse plano, em vez de ser sucessiva e fragmentária, será brusca e total, seja qual for o radicalismo do projeto. **É somente a contrariedade de outros planos de reforma ou de outros tipos de Estado ideal que cada membro de uma nação possui conscientemente ou não, que explica a lentidão das modificações sociais. Do mesmo modo, se a matéria fosse tão passiva, tão inerte quanto se supõe, não vejo por que o movimento, isto é, o deslocamento gradual, existiria; não vejo por que a formação de um organismo seria obrigada à travessia de suas fases embrionárias, obstáculo oposto à realização imediata de seu estado adulto não obstante visado desde o início pelo impulso do germe**.
> (p. 61)

# Noção de linha reta (retilinearidade, distância mínima) na biologia e na lógica

> À idéia de linha reta, cabe observar, não é exclusivamente própria à geometria. Há uma retilinearidade biológica, há também uma retilinearidade lógica. **Com efeito, assim como, para passar de um ponto a outro, a abreviação, a diminuição do número de pontos interpostos não poderia ser indefinida e detém-se no limite chamado linha reta, assim também, na passagem de uma forma específica a uma outra forma específica, de um estado individual a um outro estado individual, há uma interposição _mínima_, irredutível, de formas e de estados à percorrer, a única talvez a explicar a repetição abreviada, pelo embrião, de uma parte dos tipos sucessivos dos quais procede**. Do mesmo modo, **na exposição de um corpo de ciências, não há uma maneira de ir _diretamente_ de uma tese a outra tese, de um teorema a outro teorema, e não consiste ela em ligá-los por uma cadeia de _posições lógicas_ necessariamente intermediárias**? Necessidade realmente surpreendente. **Esta ordem racional e retilínea de exposições, à qual nos apegamos e na qual nos detemos nos livros elementares que resumem em poucas páginas o labor de alguns séculos, coincide com frequência, mas não sempre, em muitos pontos, mas não em todos, com a ordem histórica de aparecimento das descobertas sucessivas das quais toda ciência é a síntese**. É talvez o que ocorre com a famosa recapitulação da _filogênese pela ontogênese_,* que seria a retificação e não apenas a aceleração prodigiosa do caminho mais ou menos tortuoso segundo a qual as formas de antepassados, as _invenções biologicas_ acumuladas e legadas em massa ao óvula, sucederam-se nas idades anteriores.
> (p. 62-3)

> *: Em _Monadologie et sociologie_ (1895: 329) está grafado _phytogenêse_ (fitogênese ou gênese dos Vegetais) e _autogenêse_ (autogênese ou geração espontânea); já em _Les Monades et la science sociale_ (1893: 164) está grafado _phylogenêse_ (filogênese ou gênese das espécies) e _ontogenêse_ (ontogênese ou gênese dos indivíduos). Optamos pela tradução dos termos utilizados no ensaio de 1893. [EVV]
> (p. 62)

# Evolução por associação e evolução por saltos como possíveis substitutos da evolução por seleção natural

> Mas, entre os produtos variados da fermentação inédita suscitada por Darwin, há apenas dois que acrescentam ou substituem à idéia própria do mestre uma novidade verdadeira e realmente fecunda. Refiro-me, em primeiro lugar, à _evolução por associação_ de organismos elementares em organismos mais complexos formulada pelo Sr. Edmond Perrier, e, em segundo lugar, à _evolução por saltos_, por crises, que, indicada e predita há muitos anos nos escritos clarividentes de Cournot, voltou a germinar espontaneamente aqui e acolá no espírito de muitos cientistas contemporâneos. De acordo com um deles, a transformação específica de um tipo preexistente em vista de uma adaptação nova deve ter-se operado _em um momento dado e de uma forma de certo modo imediata_ (isto é, penso eu, muito curta relativamente à prodigiosa duração das espécies uma vez formadas, mas talvez muito longa em comparação com a brevidade de nossa vida) e, ele acrescenta, por um _processo regular_ e não por tateio. Do mesmo modo, para um outro transformista, a espécie, de sua formação relativamente rápida à sua decomposição também relativamente rápida, permanece realmente fixa dentro de certos limites, por estar essencialmente em estado de equilíbrio orgânico estável. Gravemente perturbado em sua constítuíção por uma mudança excessiva de seu meio (ou por alguma revolução interna em virtude da rebelião contagiosa de algum elemento), o organismo só sai de sua espécie para rolar, de certo modo, na encosta de uma outra espécie, também em equilíbrio estável, e então ali permanece por um tempo, que para nós seria uma eternidade.
> (p. 63-4)

> Não me cabe, evidentemente, discutir essas conjeturas. **Apenas observo que elas estão crescendo, ou melhor, esgueirando-se sorrateiramente, ainda humildes, porém invasoras, ao mesmo tempo que a seleção natural perde terreno a cada dia, mostrando-se mais apta a depurar os tipos do que a aperfeiçoá-los, e a aperfeiçoá-los do que a transformá-los profundamente por si mesma**. Acrescento que, seja por um ou por outro dos dois caminhos indicados, somos necessariamente conduzidos a povoar, **a encher os corpos vivos de átomos espirituais ou quase espirituais**. Com efeito, o que é a _necessidade de sociedade_ atribuída como alma, pelo Sr. Perrier, ao mundo orgânico, senão um feito de pequenas _pessoas_? E o que pode ser a transformação _direta, regular_, rápida, imaginada por outros, senão a obra de operários ocultos que colaboram para a realização de algum _plano de reorganização específico_ concebido e desejado primeiramente por um deles?
> (p. 64)

# Necessidade de pensar as associações (as sociedades) na ciência -> psicomorfismo inevitável (x antropomorfismo)

> Eis o suficiente para provar, creio eu, que a ciência tende a pulverizar o universo, a multiplicar indefinidamente os seres. Mas, como eu dizia mais acima, ela não tende menos claramente a unificar a dualidade cartesiana da matéria e do espírito. Com isso ela se lança, não digo em um antropomorfismo, mas em um _psicomorfismo inevitável_. 
> (p. 65)

# Três maneiras de conceber o monismo: Jano bifronte; elemento original comum; identificação matéria-espírito

> Não se pode efetivamente conceber o _monismo_ (isso foi dito muitas vezes, eu sei) senão de três maneiras: **ou considerando o movimento e a consciência**, a vibração de uma célula cerebral, por exemplo, e o estado de espírito correspondente, **como duas faces de um mesmo fato**, e se auto-engana por esta reminiscência do Jano Antigo; **ou fazendo decorrer a matéria e o espírito, cuja natureza heterogênea não é negada, de uma fonte comum**, de um espírito oculto e incognoscível, e ganha-se assim apenas uma trindade em vez de uma dualidade; **ou, enfim, abrmando decididamente que a matéria é espírito**, nada mais. **Essa última tese é a única que se compreende e que oferece realmente a redução exigida.** Mas há duas maneiras de entendê-la.
> (p. 65)

# Duas maneiras de entender a redução matéria-espírito: solipsismo e monadologia

> **Com os idealistas, pode-se dizer que o universo material, inclusive os outros eus, é _meu_, exclusivamente meu; ele se compõe de meus estados de espírito ou de sua possibilidade na medida em que esta é afirmada por mim, ou seja, na medida em que ela própria é um de meus estados de espírito**. Se essa interpretação for rejeitada, não resta senão admitir, **com os monadologistas, que todo o universo exterior é composto de almas outras que a minha, mas no fundo semelhantes à minha**. Aceitando-se este último ponto de vista, retira-se do precedente seus melhores fundamentos. Reconhecer que se ignora o que é o _ser em si_ de uma pedra, de um vegetal, e ao mesmo tempo obstinar-se em dizer que ele _é_, é logicamente insustentável; a idéia que se tem dele, é fácil mostrá-lo, tem como conteúdo total nossos estados de espírito, e, como nada resta se forem abstraídos nossos estados de espírito, ou bem se afirma apenas a existência deles ao afirmar esse X substancial e incognoscível, ou se é forçado a admitir que, ao afirmar outra coisa, nada se afirma. Mas se o ser em si é, no fundo, semelhante ao nosso ser, não sendo mais incognoscível, ele se torna afirmável.
> (p. 66-7)

> Por consequência, o monismo nos encaminha ao **psicomorfismo universal**.
> (p. 67)

# Crença e desejo (x movimento e sensação) ~ expaço e tempo, afirmação e vontade

> É que, de fato, um desses termos [[ _movimento_ e _sensação_ ]] pelo menos está mal escolhido. Entre as variações puramente quantitativas do movimento, cujos desvios são eles próprios mensuráveis, e as variações puramente qualitativas da sensação, quer se trate de cores, de odores, de sabores ou de sons, o contraste é demasiado chocante para nosso espírito. **No entanto, se entre nossos estados internos, por hipótese diferentes da sensação, houvesse alguns quantitativamente variáveis, como procurei mostrar alhures\* esse caráter singular permitiria talvez tentar por eles a _espiritualização do universo_**. A meu ver, os dois estados da alma, ou melhor, as duas forças da alma chamadas crença e desejo, de onde derivam a _afirmação_ e a _vontade_, apresentam esse caráter eminente e distintivo. Pela universalidade de sua presença em todo fenômeno psicológico do homem ou do animal, pela homogeneidade de sua natureza de um extremo a outro de sua imensa escala, desde a menor inclinação a crer e a desejar até a certeza e a paixão, enfim, **por sua mútua penetração e por outros traços de similitude não menos impressionantes, à crença e o desejo desempenham no eu, em relação às sensações, precisamente o papel exterior do espaço e do tempo em relação aos elementos materiais**.
> (p. 66-7)

> *: Tarde provavelmente se refere aos ensaios “Les Póssibles" (infra, p. 191), e “La Croyance et le désir” (1880), bem como ao primeiro item do quinto capítulo de _Les Lois de Imitation_ (1890). [EVV]
> (p. 66)

# Hipótese de espaço e tempo como noções primitivas tanto à matéria quanto à sensação (crença e desejo). Limites da explicação mecânica.

> Caberia examinar se essa analogia não encobriria uma identidade, se, em vez de serem simplesmente formas de nossa sensibilidade, como afirmou seu mais profundo analista, **o espaço e o tempo não seriam eventualmente noções primitivas ou quase-sensações contínuas e originais pelas quais se traduziriam a nós, graças às nossas duas faculdades de crer e de desejar, fonte comum de todo julgamento e, portanto, de toda noção, os graus e os modos de crença, os graus e os modos de desejo dos agentes psíquicos diferentes de nós**. Nessa hipótese, os movimentos dos corpos seriam apenas espécies de julgamentos ou desígnios formados pelas mônadas.
> (p. 67)

> Percebe-se que, se Fosse assim, a transparência do universo seria perfeita, e o connflito manifesto de duas correntes opostas da ciencia contemporanea estaria resolvido. Pois se esta, par um lado, nos leva apsicologia vegetal, a "psicologia celular" em breve a psicologia atomica, em suma, a uma interprtação totalmente espiritual do mundo mecanico e material, por outro lado sua tendencia a explicar tudo mecanicamente, mesmo o pensamento, não e menos eviciente. Na "psicologia celular" de Haeckel, e curiosa ver alternarem-se de uma linha a outra essas duas maneiras de ver contraditorias. Mas a contradição é suprimida pela hipótese precedente, e só assim pode sê-lo.
> (p. 67-8)

# Não-antropomorfismo. Possibilidade de estados inconscientes na crença e no desejo (x sensação).

> Aliás, **essa hipótese nada tem de antropomórfica**. A crença e o desejo possuem o privilégio único de comportar estados inconscientes. Há com certeza desejos, julgamentos inconscientes. **Tais são os desejos implicados em nossos prazeres e em nossas aflições, os julgamentos de localização e outros incorporados a nossas sensações. Ao contrário, sensações inconscientes, não sentidas, são manifestamente impossíveis**; e, se são concebidas por alguns espíritos, é porque, sem o saberem, eles as confundem com sensações não afirmadas e não discernidas, ou então porque, compreendendo a necessidade muito real de admitirem estados inconscientes da alma, consideraram erradamente as sensações como suscetíveis de serem tais estados.
> (p. 68)

# Sobre a possibilidade de crença e desejo (objetiváveis) sobre fonômenos desconhecidos ou incogniscíveis

> Ora, por esse caráter marcante, as duas forças internas que nomeei se apresentam a nós como objetiváveis no mais alto grau. **Uma vez que se aplicam a quaisquer sensações, por mais radicalmente diferentes que estas possam ser, tanto ao vermelho quanto ao dó ou ao ré, tanto ao perfume da rosa quanto ao frio ou ao calor, por que não se aplicariam igualmente a fenômenos _desconhecidos_ e, admito, incognoscíveis, por hipótese outros que as sensações, mas nem mais nem menos distintos das sensações do que elas o são umas das outras**? Por que a sensação não seria considerada como uma simples espécie do gênero _qualidade_, e não se admitiria que **existem fora de nós _marcas qualificativas de modo nenhum sensacionais_ e capazes de servir, como nossas sensações, de ponto de aplicação às forças psíquicas por excelência, a força estática chamada crença e a força dinâmica chamada desejo**? É talvez por um sentimento instintivo e confuso dessa verdade que se forjou, sobre o tipo do desejo, a idéia de força, na qual se busca a chave do enigma universal.
> (p. 69-70)

# Objetivação ~ crença - Hegel ~ substancialismo (x dinamismo ~ desejo (?))

> O que é surpreendente é que, no meio de tantas conjeturas filosóficas, ninguém ainda tenha pensado, explicitamente ao menos, em **buscar na objetivação da _crença_, e não do desejo, a solução dos problemas da física e da vida**. Digo explicitamente, pois, sem que o saibamos, concebemos a matéria, a substância coerente e sólida, satisfeita e repousada, não apenas com o auxílio, mas também à imagem e à semelhança de nossas _convicções_, assim como a força à imagem de nossos esforços. **Hegel foi O único a entrever isso, a julgar por sua pretensão de compor o mundo com séries de afirmações e de negações**. Daí talvez, apesar das aberrações e das sutilezas estranhas, esse aspecto arquitetônico e de magistral grandeza de sua obra em ruína, e que caracteriza, em geral, a superioridade própria aos sistemas substancialistas de todos os tempos, de Demócrito a Descartes, sobre as doutrinas dinamistas as mais empolgantes.
> (p. 70)

# Força -> substância (Espinosa)

> Não se viu em nosso brilhante evolucionismo atual, que leva ao extremo a idéia leibniziana de força, o monismo buscando rejuvenescer a substância de Espinosa? Pois, assim como a vontade dirige-se à certeza, e o movimento dos astros e dos átomos à sua aglomeração dehinitiva, também **a idéia de força leva naturalmente à idéia de substância**, na qual, cansado das agitações de um fenomenismo ilusório, percebendo hnalmente realidades que se dizem imutáveis, refugia-se um pensamento ora idealista, ora materialista. **Mas, dessas duas atribuições feitas aos misteriosos númenos exteriores de nossas duas quantidades interiores, qual é legítima? Por que não arriscar que ambas o são?**.
> (p. 70-1)

# Partindo do mecanicismo se chega a ideia de que os elementos que constituem um corpo devem conter elementos psíquicos ocultos (psicomorfismo)

> Com efeito, **se o ser organizado é apenas uma máquina admirável, ele deve sê-lo tanto dessa máquina como de todas as outras, nas quais não apenas nenhuma força nova, mas também nenhum produto radicalmente novo poderia ser criado pela virtude dos mais maravilhosos agenciamentos de engrenagens**. Uma máquina é somente uma distribuição e uma direção especial de forças preexistentes que a atravessam sem se alterar essencialmente. Não é senão uma mudança de forma dada a materiais brutos que ela recebe de fora e cuja essência não se altera. Portanto, **se os corpos vivos, uma vez mais, são máquinas, a natureza essencial dos únicos produtos e das únicas forças resultantes de seu funcionamento que nos são conhecidos até o fundo (sensações, pensamentos, volições), nos mostra que seus alimentos (carbono, azoto, oxigênio, hidrogênio etc.) contêm elementos psíquicos ocultos**.
> (p. 71-2)

> De maneira especial, entre esses resultados superiores das funções vitais há dois que são forças e que, brotando do cérebro, nao puderam ser criados pelo jogo mecanico de vibrações celulares. **Pode-se negar que o desejo e a crença sejam forças? Acaso não se percebe que, com suas combinações recíprocas, as paixoes e os desígnios, eles sao os ventos perptuos das tempestades da historia, as quedas d'água que fazem girar os moinhos da politica? O que é que conduz e impele o mundo, senao as crenças religiosas ou outras, as ambicções e a cupidez?** Esses supostos produtos sao de tal modo forças que, por si sós, eles produzem as sociedades, vistas ainda por tatos filósofos atuais como verdadeiros organismos.
> (p. 72)

> Conseqüentemente, se o desejo e a crença são forças, é provável que em sua _saída_ do corpo, nas nossas manifestações mentais, não sejam notavelmente diferentes de como eram em sua _entrada_, sob forma de coesões ou de afinidades moleculares. O último fundamento da substância material nos seria assim entreaberto; e vale a pena examinar se, seguindo as conseqüências desse ponto de vista, permanecemos de acordo com os fatos obtidos pela ciência[[ exemplos do protoplasma, do esporo ]].
> (p. 72)

~inteligências não-humanas (fungos, plantas, etc.)

> Para citar apenas urn exemplo, vejamos uma peguena massa de protoplasma, onde nenhum indfcio de organizac;ao pode ser descoberto, "geleia lfmpida como a clara de avo", diz a Sr. Perrier. No entanto, ele acrescenta, essa geleia excuta movimentos, _captura animais, as digere_ etc. **Tem apetite, eevidente, e, portanto, uma percep~ao mais ou menos clara do que the apetece.**
> (p. 73)

# Sobre a possibilidade de uma intencionalidade distribuída (x centralizada)

> Na verdade, é lícito perguntarmo-nos, comparando às invenções celulares, às indústrias celulares, às artes celulares, tais como um dia de primavera as _expõe_, nossas artes, nossas indústrias, nossas pequenas descobertas humanas mostradas em nossas exposições periódicas, **se é realmente certo que nossa inteligência e nossa vontade próprias, grandes _eus_ dispondo de vastos recursos de um gigantesco estado cerebral, levam a melhor sobre as dos pequenos eus confinados na minúscula cidade de uma célula animal ou mesmo vegetal**. Com certeza, se o preconceito de nos acreditarmos sempre superiores a tudo não nos cegasse, a comparação não penderia a nosso favor. É esse preconceito, no fundo, que nos impede de crer nas mônadas.
> (p. 74)

> Certamente o Sr. Espinas tern razao ao dizer que basta _pouca inteligencia_ para explicar os trabalhos sociais das abelhas e das formigas.  Mas, se concordamos com esse _pouco_ e se o julgamos ncessaria para expliear esses produtos, de resto muito simples como as de nossas industrias, **ha de se convir que para prduzir a organização mesma desses insetos, tao infinitamente superior em complexidade, em riqueza, em flexibilidade de adaptação a todas as suas obras, foi preciso muita inteligencia e _muitas inteligencias_**. 

> Consintamos em fazer esta reflexão muito natural: posto que a realização da mais simples ftunção social, da mais banal e uniforme desde muitos séculos, posto, por exemplo, que o movimento conjunto um pouco regular de uma procissão ou de um regimento exige, como sabemos, tantas lições prévias, tantas palavras, tantos esforços, tantas forças mentais despendidas quase em pura perda — **o que não é preciso então de energia mental ou quase mental, distribuída em jorros, para produzir as manobras complicadas de funções vitais simultaneamente efetuadas, não por milhares, mas por bilhões de atores diversos, todos, temos razões de pensar assim, essencialmente egoístas, todos tão diferentes entre si quanto os cidadãos de um vasto império!**
> (p. 74-5)

# Sobre o bom senso e o antropocentrismo

> Mas, dirão, se não atingimos os limites do psiquismo, o bom senso nos afirma que, em média, os seres muito menores que nós são muito menos inteligentes; e seguindo essa progressão teremos certeza de chegar, no caminho da pequenez crescente, à ininteligência absoluta. — **O bom senso! Deixemolo de lado**. O bom senso diz também que a inteligência é incompatível com um tamanho desmedido, e nisto, cumpre reconhecer, a experiência lhe dá razão. Mas juntemos essas duas ahirmações do bom senso, e fica claro que ambas emanam, uma gratuita, a outra verossímil, **do preconceito antropocêntrico**. Na realidade, **julgamos os seres tanto menos inteligentes quanto menos os conhecemos, e o erro de acreditar o desconhecido ininteligente pode ir de par com o erro, de que falaremos adiante, de acreditar o desconhecido indistinto, indiferenciado, homogêneo**.
> (p. 76)

# Sobre a não-finalidade da natureza como melhor premissa metodológica do que a suposição de uma finalidade única

> Seria preciso evitar ver no que precede uma defesa disfarçada em favor do princípio de finalidade, tão justamente desacreditado em nossos dias sob a forma ordinária. **Com efeito, do ponto de vista do método, talvez seja ainda mais preferível recusar à natureza toda finalidade, toda idéia, do que pretender ligar todas as suas finalidades e todas as suas idéias, como se faz, a um pensamento, a uma vontade única**. Singular explicação dada a um mundo em que todos os seres se entredevoram, no qual, em cada ser, o acordo das funções é apenas, quando existe, uma transação de interesses e de pretensões contrárias, no qual, no estado normal, no indivíduo melhor equilibrado, se observam funções e órgãos inúteis, como no Estado mais bem governado ocorrem sempre, aqui e ali, dissidências de seitas, particularidades provinciais religiosamente perpetuadas pelos cidadãos e necessariamente respeitadas pelos governantes, ainda que elas quebrem a unidade sonhada! Por mais infinito que se suponha o pensamento, a vontade divina, se quiserem que ele seja _uno_, logo ele se torna insufrciente como explicação das realidades.
> (p. 76-7)

# O que da no mesmo? Sobre as mônadas como lembrança do caráter puramente simbólico de verdades macro (determinadas por elementos micro)

> Nenhuma inteligência na matéria ou uma matéria modelada pela inteligência: aqui não há intermediário algum. E, a bem dizer, cientificamente, dá no mesmo. Pois suponhamos, por um instante, que um de nossos Estados humanos, composto não de alguns milhares, mas de alguns _quatrilhões_ ou _quintilhões_ de homens hermeticamente fechados e inacessíveis individualmente (espécie de China infinitamente mais populosa ainda e mais fechada), nos seja simplesmente conhecido pelos dados de seus estatísticos, cujos números relativos a enormes massas se reproduziriam com uma extrema regularidade. Quando uma revolução política ou social, que nos seria revelada por um aumento ou uma diminuíção bruscos de alguns desses números, se produzisse nesse Estado, **por mais que tivéssemos certeza de que se trata de um fato causado por idéias e paixões individuais, evitaríamos nos perder em conjeturas supérfluas sobre a natureza dessas únicas causas verdadeiras, apesar de impenetráveis, e o mais sensato nos pareceria explicar, bem ou mal, os números anormais por comparações engenhosas com os números normais habilmente manejados. Atingiríamos assim, pelo menos, resultados claros e verdades simbólicas**. Contudo, **seria importante nos lembrarmos, de tempos em tempos, do caráter puramente simbólico dessas verdades**; e é precisamente o serviço que poderia prestar às ciências a afirmação das mônadas.
> (p. 77-8)

# Diante da monadologia, porque a liberdade não se impõe à ordem? Miriateísmo (~politeísmo x monoteísmo).

> **Acabamos de ver que a ciência, após ter pulverizado o universo, acaba necessariamente por espiritualizar sua poeira**. Chegamos, porém, a uma objeção capital. Em um sistema monadológico ou atomístico qualquer, todo fenômeno não é senão uma nebulosa decomponível em ações emanadas de uma infinidade de agentes que são outros tantos pequenos deuses invisíveis e inumeráveis. Esse politeísmo, eu ia dizer esse _miriateísmo_, deixa por explicar o acordo universal dos fenômenos, por mais imperfeito que ele seja. Se os elementos do mundo nasceram separados, independentes e autônomos, não se compreende por que um grande número deles e um grande número de seus agrupamentos (por exemplo, todos os átomos de oxigênio ou de hidrogênio) se assemelham, se não perfeitamente — como se supõe sem razão suficiente —, pelo menos dentro de limites mais ou menos fixos; não se compreende por que um grande número deles, se não todos, parece ser cativo, subjugado, e ter renunciado àquela liberdade absoluta que implica sua eternidade: **não se compreende, enfim, por que a ordem, e não a desordem, e sobretudo a condição primeira da ordem, a concentração crescente, e não a dispersão crescente, resultam de seu relacionamento**.
> (p. 78-9)

# A ideia de mônada aberta interpenetrante (amparo na física)

> Pode-se esperar resolvê-los [[ problemas inerentes à ideia de mônada fechada em Leibniz ]] concebendo **mônadas abertas que se interpenetrariam reciprocamente** em vez de serem exteriores umas às outras? Creio que sim, e observo que, ainda por esse lado, os progressos da ciência, não digo contemporânea apenas, mas moderna, favorecem a eclosão de uma monadologia renovada. A descoberta newtoniana da atração, da ação à distância e a qualquer distância dos elementos materiais uns sobre os outros, mostra que se deve reconsiderar sua impenetrabilidade. Cada um deles, outrora visto como um ponto, torna-se uma esfera de ação indefinidamente ampliada (pois a analogia leva a crer que a gravidade, assim como todas as outras forças físicas, propaga-se sucessivamente); e todas essas esferas que se interpenetram são igualmente domínios próprios a cada elemento, talvez igualmente espaços distintos, embora misturados, que tomamos falsamente como um espaço único. O centro de cada uma dessas esferas é um ponto singularizado por suas propriedades, mas, ainda assim, um ponto como outro qualquer; aliás, sendo a atividade a essência mesma de todo elemento, cada um deles está inteiramente lá onde age.
> (p. 79-80)

# O átomo como meio universal

> O átomo, em conseqiiência do desenvolvimento desse ponto de vista, naturalmente sugerido pela lei de Newton (e que em vão se tenta de vez em quando explicar por impulsões de éter), cessa, a bem dizer, de ser um átomo; **ele é um _meio universal_ ou que aspira a sêlo, um universo _para si_, não apenas um _microcosmo_, como queria Leibniz, mas o cosmo inteiro conquistado e absorvido por um único ser**.
> (p. 80)

# Sobre o contínuo como descontínuo infinitesimal

> Desse modo estaria afastada, da forma mais simples, a objeção fundamental que se pode fazer a toda tentativa, atomística ou monadológica, de decompor o contínuo fenomênico em descontinuidade elementar. _Com efeito, o que colocaremos no descontínuo último senão o contínuo_? Colocamos, como novamente será explicado adiante, a totalidade dos outros seres. **No fundo de cada coisa, há toda coisa real ou possível**.
> (p. 81)

# No fundo de cada coisa há toda coisa real ou possível

>  Desse modo estaria afastada, da forma mais simples, a objeção fundamental que se pode fazer a toda tentativa, atomística ou monadológica, de decompor o continuo fenomenico em descontinuidade elementar. _Com efeito, o que colocaremos no deseontfnuo ultimo senão a continuo?_ Colocamos, como nova mente sera explieado adiante, a totalidade dos outros seres. **No fundo de cada coisa, ha toda coisa real ou posslvel.**
> (p. 81)

# Toda coisa é uma sociedade, todo fenômeno é um fato social

> Mas isso supõe, em primeiro lugar, _que toda coisa é uma sociedade, que todo fenômeno é um fato social_. Ora, é significativo que a ciência tenda, aliás por uma continuidade lógica de suas tendências precedentes, a generalizar estranhamente a noção de sociedade. Ela nos fala de sociedades animais (ver o excelente livro do Sr. Espinas a esse respeito), de sociedades celulares, e por que não de sociedades atômicas? la-me esquecendo das sociedades de astros, os sistemas solares e estelares. **Todas as ciências parecem destinadas a tormaremse ramos da sociologia**.
> (p. 81)

# Sobre o papel da associação no desenvolvimento dos organismos

> Mas é realmente notável que um cientista, um naturalista dos mais circunspectos como o Sr. Edmond Perrier, **tenha podido ver na assimilação dos organismos às sociedades a chave dos mistérios da vida e a derradeira fórmula da evolução**. Depois de dizer “que se pode comparar um animal ou um vegetal a uma cidade populosa, onde florescem numerosas corporações, e que os glóbulos sangitíneos são verdadeiros comerciantes que arrastam consigo no líquido onde flutuam a bagagem complicada do que negociam”, ele acrescenta:
>
> > Assim como foram empregadas todas as comparações que os graus de parentesco podem fornecer para exprimir as relações que os animais apresentam entre si, antes de supor que fossem unidos por um parentesco real, que fossem efetivamente consangúíneos, assim também não se deixou de comparar, até o presente, os organismos a sociedades ou as sociedades a organismos, sem ver nessas comparações algo mais que simples imagens do espírito. Ao contrário, _chegamos (...) à conclusão de que a associação desempenhou um papel considerável, quando não exclusivo, no desenvolvimento gradual dos organismos; etc_.
>
> (p. 82)

# Rebaixamento operado pela ciência entre as barreiras entre o vivo e o inorgânico. Possibilidade de uma sociedade molecular. Fase orgânica -> fase mecânica.

> Mas observemos agora que **a ciência assimila também, e cada vez mais, os organismos aos mecanismos, rebaixando entre o mundo vivo e o mundo inorgânico as barreiras de outrora**. Por que então a molécula, por exemplo, não seria uma sociedade do mesmo modo que a planta ou o animal? A regularidade e a permanência relativas pelas quais os fenômenos de ordem molecular parecem se opor aos fenômenos de ordem celular ou vital nada possuem que nos deva fazer rejeitar tal conjetura, se, com Cournot, considerarmos também que as sociedades humanas passam, ao se civilizarem, de uma fase bárbara e de certo modo _orgânica_ a uma fase _física_ e _mecânica_.
> (p. 83)
 
# Passagem do modo orgânico à fase mecânica das sociedades (Cf. Durkheim sobre solidariedade mecânica e orgânica)

> Durante a primeira, com efeito, todos os fatos gerais de seu engenhoso e instintivo desenvolvimento em poesia, artes, línguas, costumes e leis lembram estranhamente os caracteres e os procedimentos vitais; daí elas passam gradativamente a uma fase administrativa, industrial, erudita, racional, mecânica em suma, que, pelos grandes números disponíveis que o estatístico transforma em montes iguais, dá ensejo ao aparecimento das leis ou das pseudoleis econômicas, semelhantes sob tantos aspectos às leis da física e em particular da estática.
> (p. 83)

# Sobre a extensibilidade da noção de sociedade à entes mecânicos. A sociedade molecular como infinitamente mais numerosa e mais avançada que um organismo ou um Estado.

> Dessa assimilação, que se apóia em uma massa de fatos, e para a qual remeto ao _Traité de l'enchainement des idées fondamentales_, resulta primeiramente que **o abismo não é mais intransponível** (contrariamente a um erro do próprio Cournot sobre esse ponto) **entre a natureza dos seres inorgânicos e a natureza dos seres vivos, já que vemos uma idêntica evolução, a de nossas sociedades, modificar sucessivamente os traços dos segundos e os traços dos primeiros**. Resulta, em segundo lugar, que, **se um ser vivo é uma sociedade, com mais forte razão um ser puramente mecânico deve sê-lo também, já que o progresso de nossas sociedades consiste em mecanizar-se**. Portanto, uma molécula, comparada a um organismo e a um Estado, não seria senão uma espécie de nação **infinitamente mais numerosa e mais avançada**, que teria chegado àquele período estacionário que Stuart Mill almeja para todos nós.
> (p. 83-4)

# Resposta à crítica de Spencer e Espinas sobre a diferença entre o vivo (fronteiras simétricas e definidas) e o social (irregularidades ou ausência de fronteiras)

> Isso posto [[ a especificidade dos agregados sociais serem mais planos haja vista a inexistência de constantes ameaças aéreas ]], já não observamos que, quanto mais um agregado social cresce em altura, às expensas de suas duas outras dimensões, diminuindo assim a distância sempre considerável de sua forma própria às outras formas orgânicas, mais ele se aproxima também destas pela regularidade, pela simetria crescente de sua conformação exterior e de sua estrutura interna? Um grande estabelecimento público, uma escola do Governo, uma caserna, um mosteiro são como pequenos Estados muito centralizados, muito disciplinados, que confirmam essa maneira de ver. Ao contrário, quando um ser organizado como o líquen se apresenta excepcionalmente sob a forma de uma fina camada de células largamente espalhadas, é de notar que seus contornos são mal definidos e assimétricos.
> (p. 86)

> Quanto à significação dessa simetria que as formas vivas geralmente apresentam, ela pode nos ser fornecida por um outro tipo de considerações tomadas também de nossas sociedades. Seria inútil tentar explicá-la por simples motivos de utilidade funcional. Pode-se provar quanto se quiser, com o Sr. Spencer, que à locomoção exigiu a passagem da simetria radial à simetria bilateral, menor porém mais perfeita, e que lá onde a manutenção da simetria era incompatível com a saúde do indivíduo ou a duração da espécie (por exemplo, nos pleuronectídeos) a simetria foi excepcionalmente perturbada. Mas não se deve esquecer que tudo o que pôde ser mantido da simetria primitiva, provavelmente esférica, isto é, plena e vaga, de onde surgiu a vida, e tudo o que pôde ser obtido da simetria precisa e realmente bela que a vida adquire ao elevar-se, foi salvaguardado ou realizado. De um extremo a outro da vegetação e da animalidade, das diatomáceas às orquídeas, do coral ao homem, a tendência à simetria é evidente. De onde vem essa tendência? Observemos que, em nosso mundo social, tudo o que é o resultado, não de um concurso de desígnios misturados que se bloqueiam, mas de um plano pessoal executado sem restrição, é simétrico e regular. (...) Todo déspota ama a simetria; escritor, são-lhe necessárias as antíteses perpétuas; filósofo, as divisões dicotômicas ou tricotômicas repetidas; rei, o cerimonial, a etiqueta, as revistas militares. Sendo assim, e se, como será mostrado adiante, a possibilidade de fazer executar integralmente, em grande escala, um plano pessoal, é um sinal de progresso social, a conseqúiência forçosa será que o caráter simétrico e regular das obras vivas atesta o alto grau de perfeição atingido pelas sociedades celulares e pelo despotismo esclarecido ao qual estão submetidas. Não devemos perder de vista que, sendo as sociedades celulares mil vezes mais antigas que as sociedades humanas, a inferioridade destas nada teria de muito surpreendente. Além disso, estas são limitadas em seu progresso pelo pequeno número de homens que o planeta pode comportar. O mais vasto império do mundo, a China, tem apenas 300 ou 400 milhões de súditos. Um organismo que contivesse apenas um número igual de elementos anatômicos _últimos_ seria necessariamente colocado nos degraus mais baixos da vegetação ou da animalidade.
> (p. 86-8)

# Resposta à crítica de que sociedades humanas são variáveis, enquanto que as espécies orgânicas são relativamente estáticas

> Estando agora afastada a objeção extraída das formas orgânicas contra a assimilação dos organismos aos grupos sociais, convém dizer uma palavra sobre uma outra objeção que não é sem importância. Opõe-se à variabilidade das sociedades humanas, mesmo das que variam mais lentamente, a fixidez relativa das espécies orgânicas. Mas se, como poderia ser demonstrado, a causa quase exclusiva da diferenciação interna de um tipo social deve ser buscada nas relações extra-sociais de seus membros, isto é, em suas relações ou com a fauna, a flora, o solo, a atmosfera do país, ou com os membros de sociedades estrangeiras, constituídas de outro modo, a diferença assinalada não pode surpreender. **Pela natureza mesma de sua disposição inteiramente _superficial_, de modo nenhum _volumosa_, quase sem espessura, e pela dispersão extrema de seus elementos, pela multiplicidade enfim das trocas intelectuais e industriais de povo a povo, o agregado social dos homens comporta uma proporção singularmente pequena de relações _intra-sociais_, essencialmente conservadoras, entre seus membros, e os impede de manter entre si as relações de sociedade _omni-laterais_, que a forma globulosa de uma célula ou de um organismo supõe**.
> (p. 88)

> **Em apoio à idéia precedente, cabe assinalar que as células exteriores, cutâneas, as que têm o monopólio das principais relações extra-sociais, são sempre as mais facilmente modificáveis.** Nada mais _variável_ do que a pele e seus apêndices; nas plantas, a epiderme pode ser tanto lisa quanto peluda, espinhosa etc. Isso não pode ser explicado simplesmente pela heterogeneidade do meio externo, suposta maior que a do meio interno. Este último ponto está bem estabelecido. **Além disso, e consequentemente, são sempre as células externas que dão impulso às variações do resto do organismo. Prova disso é que os órgãos internos das novas espécies, embora também modificados relativamente à espécie de origem, o são sempre menos do que os órgãos periféricos, e parecem deixar-se arrastar como retardatários na via do progresso orgânico.**
> (p. 88-9)

> Será necessário indicar que, do mesmo modo, **a maior parte das revoluções de um Estado se deve à fermentação interior produzida pela introdução de idéias novas que as populações limítrofes, os marinheiros, os guerreiros vindos de expedições distantes como as Cruzadas, importam diariamente do estrangeiro**? Seria pouco equivocado considerar um organismo como uma cidade ciumenta e fechada conforme o sonho dos antigos.
> (p. 89)

# Hypothesis fingo ~ imaginação sociológica. O ponto de vista sociológico universal.

> Deixo de lado muitas outras objeções secundánrias com as quais a aplicação do ponto de vista sociológico se defronta em seu caminho. **Já que, afinal, o fundo das coisas nos é, a rigor, inacessível, e já que a necessidade de fazer hipóteses para penetrá-lo impõe-se a nós, adotemos claramente esta e a levemos até o fim**. _Hypotheses fingo_ [imagino hipóteses]\*, eu diria ingenuamente. **O que há de perigoso nas ciências não são as conjeturas bem construídas, logicamente seguidas até as últimas profundezas ou aos últimos precipícios; são os fantasmas de idéias em estado flutuante no espírito. O ponto de vista sociológico universal parece-me ser um desses espectros que atormentam o cérebro de nossos contemporâneos especulativos**. Vejamos desde o início onde ele deve nos levar. Sejamos exagerados com o risco de passar por extravagantes. Nessa matéria em particular, o temor do ridículo seria o mais antifilosófico dos sentimentos. **Assim, todas as explanações a seguir terão por objeto mostrar a profunda renovação que a interpretação sociológica de todas as coisas deverá ou deveria impor a todos os domínios do conhecimento**.
> (p. 89-90)

> \*: Réplica a uma célebre expressão dos _Principia Mathematica_ de Newton: “_Hypotheses non fingo_”. [N.E.]
> (p. 89)

# A sociedade como cérebro de mônadas cooperando entre si

> Como preâmbulo, tomemos um exemplo ao acaso. Do nosso ponto de vista, o que significa esta grande verdade de que toda atividade psíquica está ligada ao funcionamento de um aparelho corporal? Ela significa o seguinte: **em uma sociedade, nenhum indivíduo pode agir socialmente, nem se revelar de uma maneira qualquer, sem a colaboração de um grande número de outros indivíduos, na maioria das vezes ignorados pelo primeiro. Os trabalhadores obscuros que, pela acumulação de pequenos fatos, preparam o aparecimento de uma grande teoria científica formulada por um Newton, um Cuvier, um Darwin, compõem como que o organismo do qual esse gênio é a alma; e seus trabalhos são as vibrações cerebrais das quais essa teoria é a consciência.** Consciencia quer dizer gloria cerebral, de certo modo, do
elemento mais influente e mais poderoso do cerebro.
> (p. 90)

> Portanto, **entregue a si mesma uma mônada nada pode**. Eis aí o fato capital, e ele serve imediatamente para explicar um outro: _a tendência das mônadas a se reunirem_. Essa tendência exprime, a meu ver, a necessidade de um máximo de crença despendida. Quando esse máximo for atingido pela coesão universal, o desejo consumido se aniquilará, o tempo acabará. Observemos, aliás, que os trabalhadores obscuros aos quais me referi podem ter o mesmo tanto ou mais de mérito, de erudição, de força intelectual, que o glorioso beneficiário de seus labores. Isso seja dito de passagem a propósito do preconceito que nos leva a julgar inferiores a nós todas as mônadas exteriores. Se o eu é somente uma mônada dirigente entre miríades de mônadas comensais do mesmo crânio, que razão teremos, no fundo, de acreditar na inferioridade destas? Acaso um monarca é necessariamente mais inteligente que seus ministros ou súditos?
> (p. 90-1)

# A maneira científico-filosófica correntemente aceita (das condições ao resultado) nega a criação (por negar a agência das mônadas)

> Tudo isso pode parecer muito estranho, mas no fundo o é bem menos do que uma maneira de ver correntemente aceita até agora pelos cientistas e os filósofos, e da qual o ponto de vista sociológico universal deve ter por efeito lógico nos livrar. É de fato muito surpreendente ver os homens de ciência, tão dispostos a repetir a todo instante que _nada se cria_, admitir implicitamente como algo evidente que _as simples relações de diversos seres podem se tomar elas próprias novos seres acrescentados numericamente aos primeiros_. **No entanto é o que se admite, sem talvez suspeitar, quando se descarta a hipótese das mônadas e se busca por meio de alguma outra, especialmente pelo jogo dos átomos, explicar estas duas aparições capitais: um novo indivíduo vivo, um novo eu**.
> (p. 91)

# Antropocentrismo na explicação corrente

> Embora mascarado sob a noção ordinária da relação das condições ao resultado, noção que as ciências naturais e sociais utilizam de forma abusiva, o absurdo por assim dizer mitológico que indico continua, no fundo, a transparecer. Uma vez nesse caminho, não há razão para se deter; **toda _relação_ harmoniosa, profunda e íntima entre elementos naturais torna-se _criadora_ de um elemento novo e superior, que por sua vez colabora com a criação de um outro e mais elevado elemento**; a cada grau da escala das complicações fenomênicas do átomo ao eu, passando pela molécula cada vez mais complexa, pela célula ou plastídio de Haeckel, pelo órgão e enfim pelo organismo, contam-se tantos novos seres criados quanto novas unidades surgidas, e, até chegar ao eu, **segue-se sem obstáculo invencível na rota desse erro, graças à impossibilidade em que estamos de conhecer intimamente a verdadeira natureza das relações elementares que se produzem em sistemas de elementos exteriores dos quais não fazemos parte**.
> (p. 92)

# Falha na explicação corrente ao se passar para além do humano

> Mas uma grave dificuldade se apresenta quando se chega às sociedades humanas; aqui estamos em casa, nós é que somos os verdadeiros elementos desses sistemas coerentes de pessoas chamados cidades ou Estados, regimentos ou congregações. Sabemos tudo o que se passa aqui. **Ora, por mais íntimo, profundo e harmonioso que seja um grupo social qualquer, nunca vemos brotar _ex-abrupto_, entre os associados surpresos, um _eu coletivo_, real e não simplesmente metafórico, resultado maravilhoso do qual eles seriam as condições**. Certamente há sempre um associado que representa e personifica o grupo inteiro, ou então é um pequeno número de associados (os ministros em um Estado), cada qual sob um aspecto particular, que o individualiza não menos inteiramente. Mas esse chefe ou esses chefes continuam sendo membros do grupo, nascidos de pai e mãe e não de seus súditos ou de seus administrados coletivamente. Por que então o acordo de células nervosas inconscientes teria o dom de evocar diariamente do nada uma consciência em um cérebro de embrião, enquanto o acordo de consciências humanas jamais teria tido essa virtude em sociedade alguma?
> (p. 92-3)

# O ponto de vista sociológico universal implica questionar que o resultado seria mais complexo do que as condições e uma crítica da ideia de progresso (do menos ao mais diferenciado)

> Retomando então a objeção assinalada [[ de que a explicação monodológica é falha pois não explica a complexidade inerente às mônadas ]], ataca-la-ei em sua origem mesma, **no preconceito tão difundido segundo o qual o resultado é sempre mais complexo que suas condições, a ação mais diferenciada que os agentes, donde se segue que a evolução universal é necessariamente uma marcha do homogêneo ao heterogêneo, uma diferenciação progressiva e constante**.
> (p. 93)

# Caráter necessário e absoluto da diferença e da mudança (não diminuem ou aumentam)

> A verdade é que a diferença vai diferindo, que a mudança vai mudando, e que, ao darem-se assim como metas a si mesmas, a mudança e a diferença atestam seu caráter necessário e absoluto; mas não está nem poderia ser provado que a diferença e a mudança aumentam no mundo ou diminuem.
> (p. 94)

# Mundo social: complexo e diferenciado -> simples e estável (Cf. Durkheim sobre sol. mecânica e orgânica)

> **Se olharmos o mundo social, o único que nos é conhecido de dentro, vemos os agentes, os homens, muito mais diferenciados, mais caracterizados individualmente, mais ricos em variações contínuas, do que o mecanismo governamental, os sistemas de leis ou de crenças, os próprios dicionários e as gramáticas, mantidos por eles**. Um fato histórico é mais simples e mais claro que qualquer estado de espírito de um de seus atores. Mais ainda, à medida que a população dos grupos sociais aumenta e os cérebros dos societários se enriquecem de idéias e sentimentos novos, o funcionamento de suas administrações, de seus códigos, de seus catecismos, da estrutura mesma de suas línguas regulariza-se e simplifica-se, mais ou menos como as teorias científicas à medida que se preenchem de fatos mais numerosos e diversos.
> (p. 94)

> Vemos, ao mesmo tempo, que, se a marcha da civilização diversifica sob certos aspectos os indivíduos humanos, é somente com a condição de gradualmente os nivelar sob outras relações, pela uniformidade crescente de suas leis, de seus hábitos, de seus costumes, de suas linguagens.
> (p. 94)

> Em geral, a similitude desses traços coletivos favorece a dessemelhança intelectual e moral dos indivíduos cuja esfera de ação ela estende; aliás, se em consequência do movimento civilizador as instituições, os costumes, o vestuário, os produtos industriais etc. diferem muito menos _de um ponto a outro_ em um território dado, eles diferem muito mais _de um momento a outro_ em um tempo dado.
> (p. 94-5)

# Heterogeneidade -> homogeneidade relativa

> Se for negado o caráter real do espaço, a argumentação não se aplica, mas a pretensa lei e contrariada por mil hares de exemplos que nos mastram a homageneidade relativa nascenda da heterogeneidade, sendo os sinais impressionantes fornecidos pela observação de sociedades, sejam humanas ou animais. **A agregaçãoo dos pólipos, animais em geral rnnito complicados, forma urn polipeiro, especie de vegetal aquatico dos mais rudimentares.  A agregação dos homens em tribos ou em nações da origem a uma lingua, especie de planta inferior da qual os filólogos\* estudam a _vegetação_, o _crescimento_, a _floração_ históricos, para empregar suas próprias expressoes.**
> (p. 95)

> \*: Em Monadologie et sociologie (1895: 353) consta philosophes (filósofos); em Les Monades et la science sociale (1893: 234) Consta philologues (filólogos) opção adotada nesta tradução por ser mais adeuqada ao argumento em questão. [EVV]
> (p. 95)

# Juízo de simplicidade/homogeneidade como ignorância

> Mas quando, sob nossos olhos, a diversidade provincial dos usos, dos costumes, das ideias, dos sotaques, dos tipos fsicos, e substituída pelo nivelamento mesmo, pela unidade de peso e medida, de linguagem, de sotaque, de conversação inclusive, condição necessaria do relacionamento isto e da ação de todos os espíritos para o seu desenvolvimento mais livre e mais caracterizado, as lágrimas dos poetas e dos artistas nos mostram o valor do pitoresco social sacrificado a essas vantagens. Por serem mais vantajosas, pois respondem a uma maior soma de desejos, as diferenças recentemente aparecdas sao mais consideraveis que as antigas? Nao. **Infelizmente, temos uma tendencia inexplicavel a imaginar homogeneo tudo o que ignoramos.** Sendo os antigos estados geologicos do planeta menos conhecidos que o estado atual, considermos como certo que eles eram menos diferenciados, precoceito contra o qual Lyell protesta com frequencia.
> (p. 96)

# Progressão do complexo para o complexo

> Sendo os sóldos e os líquidos mais acessíveis a nossos sentidos do que os gases, e estes mais do que a natureza eterea, consideramos as sólidos e as líquidos como mais diferentes entre si do que as gases, e dizemos em física o eter e nao os eteres (embora Laplace empregue esse plural) como diríamos o gas e nao os gases, se estes nos fossem conhecidos apenas por seus efetos fisicos, notavelmente analogos, sem levar em conta suas propriedades quimicas. Quando o vapor d'agua cristaliza-se em uma infinidade de agulhas variadas ou simplesmente se liquefaz em agua corrente, seria essa condensação realmente, como somos levados a pensar, um aumento das diferenças inerentes as moleculas da agua? Nao, **nao esqueçamos a liberdade que estas, no estado de dispersao gasosa, gozavam antes, seus movimentos em todas as direções, seus choques, suas distancias infinitamente variadas. Isso significa que houve diminuição de diferenças? Tambem nao, mas simplesmente substituição de diferenças de certo genero, interiores, por dferenças de outro genero, exteriores umas as outras.**
> (p. 97)

# Existir é diferir, a diferença como substância

> **Existir é diferir; na verdade, a diferença é, em certo sentido, o lado substancial das coisas, o que elas tem ao mesmo tempo de mais próprio e mais comum.**
> (p. 97-8)

# A identidade como caso particular da diferença

> É preciso partir daí e evitar explicar esse fato, ao qual tudo retorna, inclusive a identidade, da qual falsamente se parte. **Pois a identidade é apenas um _mínimo_ e, portanto, apenas uma espécie, e uma espécie infinitamente rara, de diferença, assim como o repouso é apenas um caso do movimento, e ocírculo, uma variedade singular da elipse.** Partir da identidade primordial é supor na origem uma singularidade prodigiosamente improvável, uma coincidência impossível de seres múltiplos, ao mesmo tempo distintos e semelhantes, ou então o inexplicável mistério de um único ser simples posteriormente dividido não se sabe por quê. Em certo sentido, é imitar os antigos astrônomos que, em suas explicações quiméricas do sistema solar, partiam do círculo, e não da elipse, sob pretexto de que aprimeira figura era mais perfeita.
> (p. 98)

# Similitude/repetição como intermediários da diversidade/movimento

> Todas as similitudes, todas as repetições fenomênicas não me parecem ser senão intermediários inevitáveis entre as diversidades elementares mais ou menos apagadas e as diversidades transcendentes obtidas por essa parcial imolação.
> (p. 98)

# Exemplo da ciência

> Se Fosse passivel reunir em urn meSilla local todos os pequisadores que eIaboram juntos uma mesma cii.~ncia em vias de forma~ao (a quimica organica, por exemplo, a meteorolgia, a lingiifstica), nenhum pandemonio seria comparavel em extravagancia a esse cadinho cientffico. Ora, af se forma urn monumento impessoal, glacial e cinzento, no qual parecera apagar-se 0 vestigia meSilla dos estados psicol6gicos multicloridos que 0 edificaram. Mas esperem urn pOlleD. A ciencia DaD poderia ser a ultima palavra do progresso. Suponhamo-Ia acahada, completa, reunida em urn catecismo definitivo que se alojaria facilmente em urn canto de todas as mem6rias: restaria no cerebro humano imensamente rna is energia diponfvel para outros empregos do que podemos imaginar atuamente. Entao ficaria claro que a sistematiza<;ao consumada e a propaga<;ao universal da ortodoxia cientffica bveram por ultima e suprema razao de ser 0 desenvolvimento extraordnario de hip6teses, de heresias filos6ficas, de sistemas pessoais e indefinidamente mulbplieados, de fantasias lfricas e drambeas extraordinarias, nos quais se satisfaria plena mente em cada espfrito, gra<;as ao saber impessoal, a necessidade prfunda de universalizar sua nuan<;a especial, de imprimir no mundo sua marca. **A inteligência levada ao extremo acabaria por ser apenas um manual pratieo de imaginação.**
> (p. 100)

# Composto -> simples -> composto

> **Vê-se então por esses exemplos que a ordem e a simplicidade, estranhamente, mostram-se no composto, embora estrangeiras a seus elementos, para depois desaparecerem nvamente nos compostos superiores, e assim por diante.** Mas aqui, nas evoluções sociais e nas agregações sociais, das quais fazemos parte e com a vantagem de perceber ao mesmo tempo as duas extremidades da cadeia, a mais baixa e a mais alta pedra do edifício, vemos manifestamente que a ordem e a simplicidade são simples termos médios, alambiques onde se sublima de algum modo a diversidade elementar poderosmente transfigurada. **O poeta, o filósofo essencialmente e, scundariamente, o inventor, o artista, o especulador, o político, o estrategista: eis, em suma, as flores terminais de uma árvore nacional qualquer; para fazê-las desabrochar, trabalharam todos os germes abortados de aptidões extra-sociais ou antsociais que cada pequeno cidadão trouxe consigo ao vir ao mundo e que a foice niveladora, indispensável, da educação fez perecer para a maioria desde o berço.**
> (p. 101-2)

# Vivo -> social

> **Essas aptidões caracteristicas, ao mesmo tempo que são o primeiro termo da série social, sao o último da série vital.** Ao tentar remontar esta, atravessariamos primeiro o tipo especifico, harmoniosamente constituido e regularmente repetido ha séculos, dos quais aquelas aptidées sao as variações; depois o período crítico durante o qual esse tipo foi formado por uma coincidéncia de causas mtltiplas e bizarramente justapostas; depois os tipos anteriores dos quais deriva e suas formações análogas; depois a célula, e finalmente o protoplasma informe ou proteiforme aos caprichos súbitos e que nenhuma formula é capaz de apreender. ~ Aqui também a diversidade pitoresca é o alfa e o ômega.
> (p. 102)

# Físico/químico (átomo) -> vivo

> Mas o protoplasma, primeiro termo da série vital, não é o último termo da série química? Esta, remontada por sua vez, nos mostra os tipos moleculares cada vez menos coplexos da química orgânica, e os tipos moleculares, também cada vez menos complexos, da química inorgânica, todos regularmente edificados e consistindo provavelmente em ciclos harmoniosos de movimentos periódicos e ritmados, mas todos separados uns dos outros pelas crises tumultuosas e desordenadas de suas combinações; e chegamos assim, por conjetura, ao átomo ou aos átomos mais simples dos quais os outros seriam formados. Mas será esse o elemento inicial?  Não. **Pois o átomo mais simples é um tipo material, um tubilhão, dizem-nos, um ritmo vibratório de certo gênero, algo de infinitamente complicado segundo todas as aparências.**
> (p. 103)

# Diversidade como coração das coisas

> **A diversidade, e não a unidade, está no coração das cosas: essa conclusão deduz-se para nós, de resto, de uma observação geral que um simples olhar lançado ao mundo e às ciências nos permite fazer.** Em toda parte uma exuberante rqueza de variações e de modulações inusitadas emana destes temas permanentes que chamamos espécies vivas, sistemas estelares, equilíbrios de todo tipo, e acaba por destruí-los e renová-los inteiramente; no entanto, em nenhuma parte as forças ou as leis que estamos habituados a chamar princípios das coisas parecem propor-se a variedade como termo ou como meta.
> (p. 104)

> Mas como é que tudo isso pôde ou poderá perecer? Como, se no universo há apenas leis reputadas imutáveis e onipotentes, visando a equilíbrios estáveis, e uma substância reputada homogênea sobre a qual se exercem essas leis, como a ação dessas leis sobre essa substância pode produzir a magnífica floração de variedades que rejuvenescem a toda hora o universo e a série de revoluções inesperadas que o tranfiguram? Como pode mesmo o menor floreio introduzir-se através desses ritmos austeros e enfeitar, por pouco que seja, a eterna salmodia do mundo? Do casamento do monótono e do homogêneo, que pode nascer senão o tédio? Se tudo vem da identidade e se tudo visa e dirige-se a ela, qual a origem desse rio de variedade que nos deslumbra? **Estejamos certos, o fundo das coisas não é tão pobre, tão opaco, tão descolorido quanto se supõe. Os tipos são apenas freios, as leis são apenas diques opostos em vão ao transbordamento de diferenças rvolucionárias, intestinas, nas quais se elaboram em segredo as leis e os tipos de amanhã, e que, apesar da superposição de seus jugos múltiplos, apesar da disciplina química e vital, apesar da razão, apesar da mecânica celeste, acabam um dia, como os homens de uma nação, por derrubar todas as barreras e por fazer de seus próprios destroços um instrumento de diversidade superior.**
> (p. 104)

# O social como parcela dos seres (que escapam por propriedades extrassociais)

> Insistamos nessa verdade capital: **chega-se a ela ao observar que, em cada um desses grandes mecanismos regulres o mecanismo social, o mecanismo vital, o mecanismo estelar, o mecanismo molecular, todas as revoltas internas que acabam por rompê-los são provocadas por uma condição análoga: seus elementos componentes, soldados desses divesos regimentos, encarnação temporária de suas leis, nunca pertencem ao mundo que constituem senão por um lado de seu ser, escapando por outros lados.** Esse mundo não exitiria sem eles; mas, sem aquele, estes ainda seriam alguma coisa. Os atributos que cada elemento deve à sua incorpração no regimento não formam sua natureza completa; ele tem outras inclinações, outros instintos que procedem de aregimentações diferentes; outros, enfim e por conseqüência (veremos a necessidade dessa conseqüência), que lhe vêm de seu âmago, de si mesmo, da substância própria e fundametal na qual pode se apoiar para lutar contra a potência coltiva, mais vasta, porém menos profunda, da qual faz parte, e que não é senão um ser artificial, composto de lados e fchadas de seres.
> (p. 106)

> No entanto, apesar da extensão de nossa dívida para com o meio social e nacional, é certo que não lhe devemos tudo. **Ao mesmo tempo que franceses ou ingleses, somos mamíferos, e desse modo correm em nosso sangue não apenas germes de institos sociais que nos predispõem a imitar nossos semelhantes, a acreditar no que acreditam, a querer o que eles querem, mas também fermentos de instintos não sociais, entre os quais alguns anti-sociais.** Certamente, se a sociedade nos ti vesse feito inteiramente, ela nos teria feito apenas sociáveis.  E, portanto, das profundezas da vida orgânica (e mesmo de mais longe, acreditamos) que irrompem em nossas cidades as lavas de discórdia, de ódio e de inveja que às vezes as sumergem. Basta contar todos os Estados que o amor sexual derrubou, todos os cultos que ele abalou ou deturpou, todas as línguas que corrompeu, e também todas as colônias que fundou, todas as religiões que suavizou ou melhorou, todos os idiomas bárbaros que civilizou, todas as artes de que foi a seiva! A fonte das rebeliões, com efeito, é ao mesmo tempo a dos rejuvenescimentos. **A bem dizer, de propriamente social há somente a _imitação_ dos compatriotas e dos antepassados: no sentido mais amplo da palavra.**
> (p. 107)

# Independência das ordens química e orgânica na molécula

> Se o elemento dc uma sociedade tem uma natureza vital, o elemento orgânico de um corpo vivo tem uma natureza química.
> (p. 107)

> Mas penso que é preciso ir mais longe e rconhecer que somente essa independência {da natureza química em relação à natureza orgânica, na molécula, por exemplo} torna inteligível a resistência de certas porções dos órgãos à aceitação do tipo vivo hereditário, e a necessidade que às vezes tem a vida, isto é, o conjunto de moléculas que permaneceram dóceis, de transigir enfim, mediante a adoção de um tipo novo, com as moléculas rebeldes. Com efeito, parece haver aí de prpriamente vital somente a geração (da qual a nutrição ou a regeneração celular é apenas um caso), em conformidade ao tipo hereditário.
> (p. 108)

> Isso é tudo? Possivelmente não; **a analogia nos convida a crer que as próprias leis químicas e astronômicas não se apóiam no vazio, que elas se exercem sobre pequenos seres já caracterizados interiormente e dotados de diversidades inatas, de modo nenhum acomodadas às particularidades das máquinas celestes ou químicas.** E verdade que não percebemos nos corpos químicos nenhum traço de doenças ou de devios acidentais que pudessem ser comparados às desordens orgânicas ou às revoluções sociais. **Contudo, já que existem atualmente heterogeneidades químicas, sem dúvida nenhuma houve, em uma época muito remota, formações químicas.**
> (p. 108)

> Já que, pelo que mostra esse instrumento, tdos os corpos chamados simples, ou muitos deles, entram na composição dos planetas e das estrelas mais distantes, cujas evoluções foram independentes umas das outras, o bom senso diz que os corpos simples foram formados antes dos astros, como os tecidos antes das roupas. Conseqüentemente, o desmembramento sucessivo da substância primitiva admite apenas uma explicação: **é que suas partículas eram dessemlhantes e suas cisões foram causadas por essa dessemelhança essencial. Portanto, há motivo para pensar que o hidrogênio, por exemplo, tal como existe hoje após tantas eliminações ou emigrações sucessivas, é notavelmente diferente do hidrgênio antigo, confuso amontoado de átomos discordantes. A mesma observação aplica-se a cada um dos corpos simples sucessivamente engendrados. Ao consumirem-se e ao reduzirem-se, cada um deles adquiriu equilíbrio, fortalecido por suas próprias perdas.**
> (p. 109-10)

# Vantagens negativas que o ponto de vista sociológio universal provê à ciência

> Nas duas divisões que precedem, mostramos que **o ponto de vista sociológico universal prestaria à ciência dois grandes serviços, livrando-a, em primeiro lugar, das entidades ocas sugeridas pela relação mal compreendida das condições ao resultado, e que falsamente substituem os agentes reais; em segundo lugar, do preconceito de crer na similitude perfeita desses agentes elementares.**
> (p. 112)

# Sociedade como sistema de posse recíproca. Posessão unilateral (~escravidão) e recíproca.

> **O que é a sociedade? Poderíamos defini-la de nosso ponto de vista: a possessão recíproca, sob formas extremamente variadas, de todos por cada um.** A possessão unilateral do escravo pelo mestre, do filho pelo pai ou da mulher pelo marido no velho direito não é senão um primeiro passo em direção ao liame social, **graças à civilização crescente, o possuído tornse cada vez mais possuidor, e o possuidor possuído, até que, pela igualdade dos direitos, pela soberania popular, pela troca eqüitativa dos serviços, a escravidão antiga, mutualizada, unversalizada, faça de cada cidadão ao mesmo tempo o mestre e o servidor de todos os outros mesmo tempo, as maneiras de possuir os concidadãos e de ser possuído por eles são a cada dia mais numerosas.** Toda função nova, toda indústria nova que se cria, faz com que os funcionários ou os indutriais novos trabalhem em proveito de seus administrados ou de seus consumidores novos, que neste sentido adquirem um verdadeiro direito sobre eles, um direito que não tinham ates, enquanto eles próprios se tornam inversamente, por essa nova relação de dupla face, a coisa desses industriais ou desses funcionários. **Direi o mesmo de toda inovação. Quando uma ferrovia recém-inaugurada permite a uma pequena cidade do planalto central abastecer-se de peixe fresco pela primeira vez, o domínio dos habitantes é acrescido dos pescadores do mar que dele agora fazem parte, e aumenta do mesmo modo a clientela destes últimos, assinante de um jornal, possuo meus jornalistas, que possuem seus assinantes. Possuo meu governo, minha religião, minha força pública, assim como meu tipo específico humano, meu temperamento, minha saúde; mas sei também que os ministros do meu país, os sacerdotes do meu culto ou os policiais do meu cantão arrolam-me no rbanho do qual têm a guarda, assim como o tipo humano, se fosse personificado em alguma parte, veria em mim apenas uma de suas variações articulares.**
> (p. 112-3)

# Metafísica do Ser e do Haver (~propriedade)

> **Toda a filosofia fundou-se até agora no verbo _Ser_ [Être] cuja definição parecia a pedra filosofal a descobrir. Pode-se afirmar que, se tivesse sido fundada no verbo _Haver_ [Avoir], muitos debates estéreis, muitos passos do espírito no mesmo lugar triam sido evitados.** Deste princípio, _eu sou_ [je suis], é imposível deduzir, mesmo com toda a sutileza do mundo, qualquer outra existência além da minha; daí a negação da realidade exterior. **Mas coloque-se em primeiro lugar este postulado _"Eu hei"_ [J'ai] como fato fundamental; o _havido_ [eu] e o _havendo_ [ayant] são dados ao mesmo tem o como inseparáveis.**
> (p. 113)

> Se o haver parece implicar o ser, **o ser seguramente implica o haver.** Esta abstração vazia, **o ser, jamais é concebida senão como a _propriedade_ de alguma coisa, de um outro ser, ele próprio composto de _propriedades_, e assim por diante idefinidamente.** No fundo, todo o conteúdo da noção de ser é a noção de haver, Mas a recíproca não é verdadeira: o ser não é todo o conteúdo da idéia de propriedade.
> (p. 113)

# Desejo, creio, logo hei (ser redutível ao haver)

> A noção concreta, substancial, que descobrimos em nós é, portanto, esta. **Em vez do famoso _cogito ergo sum_, eu diria de bom grado: _"Desejo, creio, logo hei" [Je désire, je crois, donc j'ai]_.** O verbo _Ser_ às vezes significa _haver_, às vezes igualar. "Meu braço é/está [_est_] quente": o calor de meu braço é a propriedade de meu braço. Aqui _é [est]_ quer dizer _há [a]_ "Um francês é [_est_] um europeu, o metro é [_est_] uma medida de comprimento." Aqui _é [est]_ quer dizer _iguala_. **Mas essa igualdade mesma não é senão a relação do continente ao conteúdo, do gênero à espécie, ou vice-versa, isto é, um tipo de relação de possessão. Por seus dois sentidos, portanto, o _ser_ é redutível ao haver.**
> (p. 114)

# Contradição e devir (estanque) x haver (ganho e perda, intermediários). O meu como oposto do eu.

> Se, custe o que custar, quiserem estender da noção de _Ser_ desdobramentos que sua esterilidade essencial não comporta, **acaba-se por opor-lhe o não-ser e por fazer com que este termo (no qual se objetiva simplesmente e em vão nossa faculdade de negar, assim como se objetiva no Ser nossa faculdade de afirmar) desempenhe um papel importante e insensato. ~ Sob esse aspecto, o sistema hegeliano pode ser considerado a última palavra da filosofia do Ser.** No mesmo caminho, **acabam se forjando também as noções impenetráveis, e no fundo contraditórias, de _devir_ e de _desvanecimento_, antiga e estéril pastagem dos ideólogos do Além-Reno.** Ao contrário, **nada mais claro do que as idéias de _ganho_ e de _perda_, de aquisição e de espoliação, que correspondem ao que chamarei a filosofia do Haver, para dar um nome ao que não existe ainda. Entre ser ou não ser, não há meio-termo, ao passo que se pode haver mais ou menos.**
> (p. 114)

> O ser e o não-ser, o eu e o não-eu: oposições infecundas que fazem esquecer os correlatos verdadeiros. **O oposto verdadeiro do _eu_ não é o não-eu, é o _meu_; o oposto verdadeiro do ser, isto é, do _havendo [ayant]_, não é o não-ser, é o _havido [eu]_.**
> (p. 114)

# Redução na ciência da noção de propriedade à categorias abstratas (possível) x propriedades reais (relações intra e extra sociais)

> A divergência profunda, que se acentua a cada dia, entre a corrente da ciência propriamente dita e a da filosofia provém de que a primeira, afortunadamente para ela, tomou po guia o verbo Haver. **Tudo se explica, a seu ver, por propriedades, não por entidades. Ela desdenhou a relação enganador da substância ao fenômeno, dois termos vazios nos quais Ser se desdobrou; ela fez um uso moderado da relação d causa a efeito, em que a possessão se apresenta apenas sob uma de suas duas formas, e a menos importante, a possessã pelo desejo.** Mas usou amplamente, e infelizmente abusou da relação de _proprietário_ à _propriedade_. **O abuso consistiu sobretudo, em ter compreendido mal essa relação, não vendo que a verdadeira propriedade de um proprietário qualquer é um conjunto de outros proprietários; que cada massa, cada molécula do sistema solar, por exemplo, tem como propriedade física e mecânica não palavras tais como a extensão, motilidade etc., mas todas as outras massas, todas as outras moléculas; que cada átomo de uma molécula tem como propriedade química, não atomicidades ou afinidades, mas todos os outros átomos da mesma molécula; que cada célula de um organismo tem como propriedade biológica, não a irritabilidade, a contratilidade, a inervação etc., mas todas as outras células do mesmo organismo e especialmente do mesmo órgão.** **Aqui a possessão é recíproca como em toda relação _intra-social_; mas ela pode ser unilateral, como nas relações _extra-sociais_ do mestre e do escravo, do fazendeiro e seu gado. Por exemplo, a retina tem como propriedade, não a visão, mas os átomos etéreos vibrando luminosamente, que não a possuem; e o espírito possui mentalmente todos os ojetos de seu pensamento, aos quais não pertence de maneira alguma.** Isso quer dizer que esses termos abstratos, motildade, densidade, peso, afinidade etc., não exprimem nada, não correspondem a nada? **Eles significam, penso eu, que para além do domínio real de todo elemento há seu domínio condicionalmente necessário, isto é, certo, embora não real, e que essa antiga distinção do real e do possível em um novo sentido não é quimérica.**
> (p. 115-6)

# Agente -> proprietário

> Certamente, **os elementos são agentes tanto quanto os proprietários; mas eles podem ser proprietários sem serem agentes, e não podem ser agentes sem serem proprietários.**  Além disso, **sua ação revela-se a nós apenas como uma mdança produzida na natureza de sua possessão.**
> (p. 116)

# Possessão e aquisição (x correspondência e adaptação)

> Há milhares de anos vêm sendo catalogadas as diversas maneiras de ser, os diversos graus do ser, mas nunca se teve a idéia de classificar as diversas espécies, os diversos graus da possessão. No entanto, **a possessão é o fato universal, e não há termo melhor que o de aquisição para exprimir a formação e o crescimento de um ser qualquer. Os termos _correspondência_ e _adaptação_, postos em moda por Darwin e Spencer, são mais vagos, mais equívocos, e só compreendem o fato universal por fora.** Será verdade que a asa do pássaro se adapta ao ar, a nadadeira dos peixes à água, o olho à luz? Não, como tampouco a locomotiva adapta-se ao carvão ou a máquina de costura ao fio da costureira. Diremos também que os nervos vasomotores, engenhosos mecanismos pelo qual se mantém o equilíbrio interno da temperatura do corpo apesar das variações da temperatura externa, são adaptados a essas variações? **_Adaptar-se a_, singular maneira de _lutar contra_! A locomotiva está adaptada, se quiserem, à locomoção terrestre, e a asa à locomoção aérea, e isso equivale a dizer que a asa utiliza o ar para movese, assim como a locomotiva utiliza o carvão e a nadadeira a água. Não é esse emprego uma tomada de possessão? Todo ser quer, não se apropriar aos seres exteriores, mas apropriá-los a si.** Aderência atômica ou molecular no mundo físico, nutrição no mundo vivo, percepção no mundo intelectual, direito no mundo social, a possessão não cessa de estender-se, sob inúmeras formas, de um ser aos outros seres, por um entrecruzmento de domínios variados cada vez mais sutis.
> (p. 116-7)

# Possessão recíproca e unilateral, entre elementos individuais ou entre elemento e grupo

> Seja qual for a forma da possessão, física, química, vital, mental, social (sem falar das subdivisões que podem ser feitas em cada forma), **precisamos distinguir, primeiro, se ela é unlateral ou recíproca e, em segundo lugar, se ela se estabelece entre um elemento e um ou vários outros elementos indivdualmente considerados, ou entre um elemento e um grupo indistinto de outros elementos.** Comecemos por dizer uma palavra desta segunda distinção. **Quando entro em comuncação verbal com um ou vários de meus semelhantes, nossas mônadas respectivas, segundo meu ponto de vista, apreedem-se reciprocamente; ao menos é certo que essa relação é a relação de um elemento social com elementos sociais tmados como distintos. Ao contrário, quando olho, quando ecuto, quando estudo a natureza ambiente, as pedras, as águas, as próprias plantas, cada um dos objetos de meu pensamento é um mundo hermeticamente fechado de elementos que sem dúvida se conhecem ou se apreendem entre si intimamente, da mesma forma que os membros de um grupo social, mas que não se deixam abraçar por mim a não ser em bloco e de fora.**
> (p. 117-8)

# Possessão social como relação por excelência das mônadas

> Mas é preciso chegar ao mundo social para ver as môndas apreenderem-se de maneira nua e sensível pela intensdade de seus caracteres transitórios plenamente desdobrados um diante do outro, um no outro, um pelo outro. **Essa é a relação por excelência, a possessão típica da qual o restante não é senão um esboço ou um reflexo.** Pela persuasão, pelo amor e pelo ódio, pelo prestígio pessoal, pela comunhão das crenças e das vontades, pela cadeia mútua do contrato, espécie de rede cerrada que não cessa de estender-se, os elemetos sociais se ligam e se esticam de mil maneiras, e de sua cooperação nascem as maravilhas da civilização.
> (p. 118)

# Relação criadora e destruidora

> Se fosse assim, a ação possessiva de mônada a mônada, de elemento a elemento, seria a única relação realmente fcunda. Quanto à ação de uma mônada ou de um elemento pelo menos sobre um grupo confuso de mônadas ou de elmentos indiscernidos, ou vice-versa, ela não seria senão uma perturbação acidental das belas obras efetuadas pelo duelo ou pela união dos elementos. **Enquanto esta última relação é criadora a outra é destruidora. Mas ambas são necessárias.**
> (p. 119)

# Possessão unilateral e recíproca

> A possessão unilateral e a possessão recíproca não são mnos necessariamente unidas, mas a segunda é superior à prmeira.
> (p. 119)

# Transformação explicado pela tendência conflitante das mônadas a se apropriarem reciprocamente

> Ao contrário {da explicação da transformação a partir de uma substância única criadora}, **na hipótese das mônadas tudo flui naturamente. Cada uma delas absorve o mundo a si, o que é apreendese melhor a si mesma. Elas bem fazem parte umas das outras, mas podem pertencer-se mais ou menos, e cada uma aspira ao mais alto grau de possessão; daí sua concentração gradual; além disso, elas podem pertencer-se de mil maneiras diferentes, e cada uma aspira a conhecer novas maneiras de apropriar-se de suas semelhantes.** Daí suas transformações. **E para conquitar que elas se transformam; mas, como jamais se submetem a uma dentre elasa não ser por interesse, o sonho ambicioso de nenhuma delas se realiza inteiramente, e as mônadas vassalas em re am a mônada suserana en anto esta as utiliza.**
> (p. 120)

> Sua multiplicidade atesta sua diversidade, única capaz de dar-lhes uma razão de ser. **Nascidos diversos, eles tendem a se diversificar, é sua natureza que o exige; por outro lado, sua diversidade deve-se a eles serem, não unidades, mas totalidades especiais.**
> (p. 120-1)

# Mônadas ~ virtualidades

> Penso também que muitos enigmas indecifráveis seriam explicados imaginando-se que **a especialidade de cada um dos elementos, verdadeiro ambiente universal, é ser não apenas uma totalidade, mas uma virtualidade de certo tipo, e encarnar dentro dele uma idéia cósmica sempre chamada, mas raramente destinada, a realizar-se efetivamente.** Seria, de certo modo, alojar as idéias de Platão nos átomos de Epicuro ou, melhor, de Empédocles, uma vez que, segundo Zeller, este último filósofo ao que parece professava, como Leibniz, a diversidade elementar. **É bom às vezes poder abrigar-se por trás de algum antepassado grego.**
> (p. 121)

# Tendência à universalização. Formas de repetição universal: ondulação, geração e imitação.

> **Já que o ser é o haver, segue-se que toda coisa deve ser ávida.** Ora, se há um fato que deveria impressionar todos os olhos, é realmente a avidez, a ambição imensa que, de uma ponta a outra do mundo, do átomo vibrante ou do animálculo prolífico ao rei conquistador, preenche e move todos os seres.  **Toda possibilidade tende a realizar-se, toda realidade tende a universalizar-se. Toda possibilidade tende a realizar-se, a caracterizar-se nitidamente: daí a irrupção de variações por cima e através de todos os temas vivos, físicos e sociais. Toda realidade, todo caráter uma vez formado tende a universalzar-se.** Eis por que a luz e o calor se irradiam e a eletricidade se propaga com a velocidade que sabemos, e a menor vibrção atômica aspira a preencher sozinha o éter infinito, alvo também cobiçado por todas as outras. Eis por que toda espcie, toda raça viva recém-formada, multiplicando-se em uma progressão geométrica, não tardaria a cobrir o globo inteiro se não deparasse com a fecundidade das concorrentes, não apenas as espécies e as raças, mas também suas menores paticularidades discerníveis, as doenças mesmas de cada uma delas, o que exclui a explicação teleológica da fecundidade falsamente considerada como meio destinado à conservação dos tipos. Eis por que, enfim, uma obra social qualquer, com um caráter próprio mais ou menos marcado, um produto idustrial, um verso, uma fórmula, uma idéia política ou outra surgida um dia em urn ponto de um cérebro, sonha como Alexandre a conquista do mundo, busca projetar-se por mlhares e milhões de exemplares por toda parte onde haja hmens, e só se detém nesse caminho barrada pelo choque com sua rival não menos ambiciosa. **As três principais formas da repetição universal, a ondulação, a geração, a imitação, como eu disse alhures,* são espécies de procedimentos de governo e instrumentos de conquista que dão ensejo a três tipos de invasão física, vital, social: a irradiação vibratória, a expansão geradora, o contágio do exemplo.**
> (p. 123-4)

# Realidade material como resistência (à avidez inerente às coisas)

> **A criança nasce déspota: outrem para ela, assim como para os reis negros, existe apenas para servi-la. São necessários anos de castigos e compressão escolar para curá-la desse erro.  Pode-se dizer que todas as leis e todas as regras, a disciplina química, a disciplina vital, a disciplina social, são outros tantos freios acrescidos e destinados a conter esse apetite onívoro de todo ser.** Em geral, nós, homens civilizados, tiranizados desde nossas fraldas, temos pouca consciência disso. Esmagada no ovo, nossa ambição aborta, mas quão profunda ela deve ser para que, à menor fissura de nossos diques habituais, e apesar de tantos séculos de compressão hereditária, ela ainda irrompa aqui e ali na história em ímpetos como os de César ou Napoleão 1!
> (p. 124)

> Deparar com seu limite, com sua impotência consttada: que choque terrível para todo homem e, antes de tudo, que surpresa! **Nessa pretensão universal do infinitamente pequeno ao infinitamente grande, e no choque universal e eterno resultante, há certamente com que justificar o pessmismo.** Para um desenvolvimento único, bilhões de abortos!  **Nossa noção da matéria traduz bem esse caráter essenciamente contrariante do mundo que nos cerca. Os psicólogos disseram a verdade, mais verdadeira do que supunham: a realidade exterior não é para nós senão a propriedade que ela tem de nos _resistir_, resistência aliás não apenas tátil, por sua solidez, mas também visual por sua opacidade, voluntria por sua indocilidade a nossos desejos, intelectual por sua impenetrabilidade a nosso pensamento.** Quando se diz que a matéria é sólida, é como se dissesse que ela é indócil; **é uma relação _dela a nós_ e não _dela a ela_, apesar da ilusão contrária, que especificamos desse modo, tanto pelo primeiro atributo quanto pelo segundo.**
> (p. 124-5)

# Progresso ~ desigualdade [~ guerra de mundos]

> Pode-se esperar do futuro um remédio para esse estado de cosas? Não, se acreditarmos nas induções que o exemplo de nossas sociedades nos sugere; **a desigualdade aumentará cada vez mais entre os vencedores e os vencidos do mundo. A vitória de uns e a derrota de outros se tornarão a cada dia mais copletas. Com efeito, uma das marcas mais certas do progresso da civilização em um povo é que as grandes reputações, os grandes empreendimentos militares ou industriais, as grandes reformas e as reorganizações radicais tornam-se possíveis. Dito de outro modo, o progresso da civilização, pela supressão dos dialetos e a difusão de uma única língua, pelo apagamento dos costumes distintos e o estabelecimento de um mesmo código, pela alimentação uniforme dos espíritos por meio de jornais mals procurados que os livros, e por uma infinidade de outros traços, consiste em facilitar a realização cada vez mais integral, cada vez menos mutilada, de um plano individual único pela massa inteira da nação.** De modo que **os milhares de planos diferentes que, em uma fase menos avançada, triam recebido, concorrentemente com o eleito, um começo de execução, são votados assim a uma extinção fatal.**
> (p. 125)

# Ideia e desígnico como metáforas (para a força-crença e força-desejo)

> Mas, quando falo de conquista e e ambição a proposito das sociedades celulares, é antes de propaganda e de dedicação que deveria falar. Certamente, isso tudo é metafórico, mas ainda assim é preciso escolher bem os termos das comprações; e o leitor também não deve esquecer que, se a crença e o desejo, no sentido puro e abstrato em que entendo essas duas grandes forças, essas duas únicas quantidades da alma, têm a universalidade que lhes atribuo, **então faço apenas uma metáfora ao chamar _idéia_ a aplicação da _força-crença_ a marcas qualitativas internas, sem nenhuma relação, porém, com nossas sensações e nossas imagens; ao chamar _desígnio_ a aplicação da _força-desejo_ a uma dessas quase-idéias; ao chamar propaganda a comunicação de elemento a elemento, não verbal, por certo, mas especificamente desconhecida, do _quase-desígnio_ formado por um elemento iniciador; ao chamar coversão a transformação interna de um elemento no qual entra, no lugar de seu quase-desígnio próprio, aquele de outrem etc.**  Sob o benefício dessa observação, prossigamos.
> (p. 127)

# Aproximação da biologia (vida) à religião (x militarismo)

> E, se essa similitude for aproximada de muitas outras, se for observado que **cada espécie viva, assim como cada igreja ou comunidade religiosa,** é um mundo fechado aos grupos rivais, e no entanto hospitaleiro, ávido de novos recrutas; um mundo enigmático e indecifrável visto de fora, no qual se passam palavras de ordem misteriosas, conhecidas apenas dos fiéis; um mundo conservador onde as pessoas se conformam escrupulosa e indefinidamente, com uma admirável abnegação, aos ritos tradicionais; um mundo muito hierarquizado onde no entanto a desigualdade não parece suscitar revoltas; um mundo ao mesmo tempo muito ativo e muito regrado, muito tenaz e muito flexível, hábil em adaptar-se às circunstâncias novas e perseverante em suas seculares imagens; **ver-se-á que não abuso das liberdades da analogia ao assimilar os fenômenos biológicos antes às manfestações religiosas de nossas sociedades do que a seu aspecto guerreiro, industrial, científico ou artístico.**
> (p. 128)

> Sob certos aspectos, um exército parece assemelhar-se, tão exatamente quanto um convento, a um organismo. Mesma disciplina, mesma subordinação rigorosa, mesmo poder do espírito de corpo, em um organismo e em um regimento.  O modo de nutrição (isto é, de recrutamento) é também o mesmo, por intussuscepção, por incorporação periódica de rI crutas, por preenchimento de quadros até um certo limite não ultrapassável. Mas, sob outros aspectos não menos importantes, a diferença é notável: **a arregimentação transforma e regenera menos o conscrito do que a assimilação vital a célula alimetar, ou a conversão religiosa o neófito. A educação militar não penetra de modo algum até o fundo da alma.**
> (p. 128-9)

> Por esses caracteres diferenciais, **a vida apresenta-se-nos portanto, como uma coisa respeitável e sagrada, como um grande e generoso empreendimento de salvação, de redenção dos elementos encadeados nos laços estreitos da química;** é seguramente desconhecer sua natureza considerar sua evolução, com Darwin, como uma série de operações militares em que a destruição é sempre a companheira e a condição da vitória.
> (p. 129)

> **Quando um ser vivo destrói um outro para comê-lo, os elementos que compõem o ser destruidor propõem-se talvez prestar aos elementos do ser destruído o mesmo tipo de serviço que os fiéis de uma religião crêem prestar aos adeptos de um outro culto ao destruir seus templos, suas instituições clericais, seus liames religiosos, e ao buscar convertê-los à "verdadeira fé".** O que se destrói aqui é o exterior dos seres, dos elementos dotados de fé e de amor, mas estes não são de modo algum imolados. Cumpre reconhecer, em geral, que é a vida superior que absorve e assimila a vida inferior, assim como são as grandes e elevadas religiões — cristianismo, islamismo, budismo — que convertem os fetichistas, e não o contrário.
> (p. 129-30)

# Consciência e morte

> Assim concebida a vida, tenho necessidade de dizer de que maneira se pode conceber a consciência e a morte?  **Chamo consciência, alma, espírito, o triunfo passageiro de um elemento eterno, que sai, por um favor excepcional, do infinitesimal obscuro para dominar um povo de irmãos tranformados em seus súditos, submetendo-os por algum tempo à sua lei transmitida pelos predecessores e ligeiramente modifcada por ele, ou marcada com seu selo real; e chamo morte o destronamento gradual ou súbito, a abdicação voluntária ou forçada desse conquistador espiritual que, despojado de todos os seus Estados,** como Dario depois de Arbela e Napoleão depois de Waterloo ou como Carlos V em Saint-Just e Dicleciano em Tessalônica, porém ainda mais completamente nu, **retorna ao infinitesimal de onde partiu, ao infinitesimal natal, saudoso talvez, com certeza não invariável e, quem sabe? , não inconsciente.**
> (p. 130)

# Vida x não-vida

> Portanto, **não digamos nem a _outra vida_ nem o _nada_; digamos a _não-vida_, sem nada prejulgar. A não-vida, como o não-eu, não é necessariamente o não-ser; e os argumentos de alguns filósofos contra a possibilidade da existência depois da morte não pesam mais que os dos céticos idealistas contra a realidade do mundo exterior.** Que a vida seja preferível à não-vida, não há nada, também, de menos demonstrado.  **Talvez a vida seja apenas um tempo de provas, de exercícios escolares e penosos impostos às mônadas que, ao saírem dessa dura e mística escola, vêem-se purgadas de sua necessidade anterior de dominação universal.** Estou convencido de que poucas delas, tendo caído do trono cerebral, aspirem a retomá-lo. **Devolvidas à sua originalidade própria, à sua indpendência absoluta, elas renunciam sem dificuldade e sem retorno ao poder corporal, e, durante a eternidade, saboreiam o estado divino no qual o último instante da vida as megulhou, a isenção de todos os males e de todos os desejos, não digo de todos os amores, e a certeza de possuir um bem oculto, eternamente durável.**
> (p. 130-1)

> Mas já são muitas hipóteses.
> (p. 131)

