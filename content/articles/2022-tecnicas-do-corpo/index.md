---
title: "As técnicas do corpo"
date: 2022-05-09
path: "/tecnicas-do-corpo"
category: fichamento
author: Rafael Gonçalves
featuredImage: "mauss.jpg"
srcInfo: <a href="https://pt.wikipedia.org/wiki/Ficheiro:M.Mauss_1872-1950.jpg">Marcell Mauss</a>
tags: 
 - técnica
 - corpo
 - antropologia
 - Marcell Mauss
published: true
---

Fichamento do texto _As técnicas do corpo_ de Marcell Mauss[^1].

[^1]: MAUSS, Marcel. As técnicas do corpo. (Trad.: Paulo Neves) In: _Sociologia e Antropologia_. São Paulo: Cosac & Naify, pp.399-422. 2003 [1934].

# Técnicas do corpo como maneiras de servir-se do corpo. Método: concreto -> abstrato.

> Eu digo as técnicas do corpo, porque se pode fazer a teoria _da_ técnica do corpo a partir de um estudo, de uma exposição, de uma descrição pura e simples _das_ técnicas do corpo. Entendo por essa expressão as maneiras pelas quais os homens, de sociedade a sociedade, de uma forma tradicional, sabem servir-se de seu corpo. Em todo caso, convém proceder do concreto ao abstrato, não inversamente.
> (p. 401)

# O nado como exemplo de ténica específica

> Um exemplo nos fará compreender isso imediatamente, a nós, psicólogos, biólogos, sociólogos. Outrora nos ensinavam a mergulhar depois de ter aprendido a nadar. E, quando nos ensinavam a mergulhar, nos diziam para fechar os olhos e depois abri-los dentro d'água. Hoje a técnica é inversa. Começa-se toda aprendizagem habituando a criança a ficar dentro d'água de olhos abertos. Assim, antes mesmo que nadem, as crianças são treinadas sobretudo a controlar reflexos perigosos mas instintivos dos olhos, são antes de tudo familiarizadas com a água, para inibir seus medos, criar uma certa segurança, selecionar paradas e movimentos. Há portanto uma técnica do mergulho e uma técnica da educação do mergulho que foram descobertas em meu tempo. E vejam que se trata claramente de um ensino técnico, e que há, como para toda técnica, uma aprendizagem do nado. Por outro lado, nossa geração, aqui, assistiu a uma mudança completa de técnica: vimos o nado a braçadas e com a cabeça fora d'água ser substituído pelas diferentes espécies de crawl. Além disso, perdeu-se o costume de engolir água e de cuspi-la. Pois os nadadores se consideravam, em meu tempo, como espécies de barcos a vapor. Era estúpido, mas, enfim, ainda faço esse gesto: não consigo desembaraçar-me de minha técnica. Eis aí, portanto, uma técnica corporal específica, uma arte gímnica aperfeiçoada em nosso tempo.
> (p. 402)

# Outros exemplos: o andar, as posições das mãos, a corrida

> Era uma idéia que eu podia generalizar. A posição dos braços e das mãos enquanto se anda é uma idiossincrasia social, e não simplesmente um produto de não sei que arranjos e mecanismos puramente individuais, quase inteiramente psíquicos. Por exemplo: creio poder reconhecer assim uma jovem que foi educada no convento. Ela anda, geralmente, com as mãos fechadas. E lembro-me ainda de meu professor do ginásio interpelandome: "Seu animal! Andas o tempo todo com as manoplas abertas!". Portanto, existe igualmente uma educação do andar.
> (p. 404)

> Outro exemplo: há posições da mão, em repouso, convenientes ou inconvenientes. Assim, podeis adivinhar com certeza, se uma criança conserva à mesa os cotovelos junto ao corpo e, quando não come, as mãos sobre os joelhos, que ela é inglesa. Uma criança francesa não se comporta mais assim: abre os cotovelos em leque e os apoia sobre a mesa, e assim por diante.
> (p. 404)

> Sobre a corrida, enfim, também presenciei, como vós todos, a mudança de técnica. Imaginem que meu professor de ginástica, um dos melhores formados em Joinville, por volta de 1860, ensinou-me a correr com os punhos colados ao corpo: movimento completamente contraditório a todos os movimentos da corrida; foi preciso que eu visse os corredores profissionais de 1890 para compreender que devia correr de outro modo.
> (p. 404)

# Natureza social do "habitus" (Aristóteles)

> Assim, durante muitos anos tive a noção da natureza social do "fiabitus". Observem que digo em bom latim, compreendido na França, "habitus"'. A palavra exprime, infinitamente melhor que "hábito", a "exis" [hexis], o "adquirido" e a "faculdade" de Aristóteles (que era um psicólogo). Ela não designa os hábitos metafísicos, a "memória" misteriosa, tema de volumosas ou curtas e famosas teses. Esses "hábitos" variam não simplesmente com os indivíduos e suas imitações, variam sobretudo com as sociedades, as educações, as conveniências e as modas, os prestígios. É preciso ver técnicas e a obra da razão prática coletiva e individual, lá onde geralmente se vê apenas a alma e suas faculdades de repetição.
> (p. 404)

# Abordagem tríplice do "homem total" para entender esses fenômenos

fisico, psicológico e sociológico?

>  E concluí que não se podia ter uma visão clara de todos esses fatos, da corrida, do nado etc., senão fazendo intervir uma tríplice consideração em vez de uma única, fosse ela mecânica e física, como uma teoria anatômica e fisiológica da marcha, ou, ao contrário, psicológica ou sociológica. É o tríplice ponto de vista, o do "homem total", que é necessário.
>  (p. 405)

# Educação (imitação prestigiosa?) x imitação

> Enfim, uma outra série de fatos se impunha. Em todos esses elementos da arte de utilizar o corpo humano os fatos _de educação_ predominavam. A noção de educação podia sobrepor-se à de imitação. Pois há crianças, em particular, que têm faculdades de imitação muito grandes, outras muito pequenas, mas todas se submetem à mesma educação, de modo que podemos compreender a seqüência dos encadeamentos. O que se passa é uma imitação prestigiosa. A criança, como o adulto, imita atos bem-sucedidos que ela viu ser efetuados por pessoas nas quais confia e que têm autoridade sobre ela. O ato se impõe de fora, do alto, mesmo um ato exclusivamente biológico, relativo ao corpo. O indivíduo assimila a série dos movimentos de que é composto o ato executado diante dele ou com ele pelos outros.
> (p. 405)

> É precisamente nessa noção de prestígio da pessoa que faz o ato ordenado, autorizado, provado, em relação ao indivíduo imitador, que se verifica todo o elemento social. No ato imitador que se segue, verificam-se o elemento psicológico e o elemento biológico.
> (p. 405)

# Sobre o natural e o artificial (exemplo na maneira de andar)

> Era uma maneira adquirida, e não uma maneira natural de andar. **Em suma, talvez não exista "maneira natural" no adulto**. E com mais razão ainda quando outros fatos técnicos intervém: no que se refere a nós, o fato de andarmos calçados transforma a posição de nossos pés; sentimos isso bem ao andarmos descalços.
> (p. 405-6)

# Sobre a magia e sua relação com a técnica e o físico

> Essa mesma questão fundamental colocava-se a mim, por um outro aspecto, a propósito de todas as noções relativas à força mágica, à crença na eficácia não apenas física, mas oral, ritual, de certos atos. Aqui me situo mais em meu terreno do que no terreno perigoso da psicofisiologia dos modos de andar, no qual me arrisco diante de vós.
> (p. 406)

> O fenômeno psicológico que constatamos nesse momento é, do ponto de vista habitual do sociólogo, muito fácil de perceber e de compreender. Mas o que queremos destacar agora é a confiança, o _momentum_ psicológico capaz de associar-se a um ato que é antes de tudo uma proeza de resistência biológica, obtida graças a palavras e a um objeto mágico.
> (p. 406)

>Ato técnico, ato físico, ato mágico-religioso confundem-se para o agente. Eis aí os elementos de que eu dispunha.
> (p. 406-7)

# Noção ampliada de técnica (em Platão) x ferramenta

> Todos cometemos, e cometi durante muitos anos, o erro fundamental de só considerar que há técnica quando há instrumento. Era preciso voltar a noções antigas, aos dados platônicos sobre a técnica, quando Platão falava de uma técnica da música e em particular da dança, e ampliar essa noção.
> (p. 407)

# Definição: técnica como ato tradicional eficaz sentido pelo autor como mecânico, físico ou fisico-químico e efetuado com esse objetivo

> Chamo técnica um ato _tradicional eficaz_ (e vejam que nisso não difere do ato mágico, religioso, simbólico). Ele precisa ser _tradicional e eficaz_. Não há técnica e não há transmissão se não houver tradição. Eis em quê o homem se distingue antes de tudo dos animais: pela transmissão de suas técnicas e muito provavelmente por sua transmissão oral.
> (p. 407)

> Peco-vos então a permissão de considerar que adotais minhas definições. Mas qual é a diferença entre o ato tradicional eficaz da religião, o ato tradicional, eficaz, simbólico, jurídico, os atos da vida em comum, os atos morais, de um lado, e o ato tradicional das técnicas, de outro? É que este último é sentido pelo autor _como um ato de ordem mecânica, física ou físico-química_, e é efetuado com esse objetivo.
> (p. 407)

# Técnicas do corpo (primeiro e mais natural objeto técnico)

> Nessas condições, cabe dizer simplesmente: estamos lidando com _técnicas do corpo_. O corpo é o primeiro e o mais natural instrumento do homem. Ou, mais exatamente, sem falar de instrumento: o primeiro e o mais natural objeto técnico, e ao mesmo tempo meio técnico, do homem, é seu corpo. Imediatamente, toda a imensa categoria daquilo que, em sociologia descritiva, eu classificava como "diversos" desaparece dessa rubrica e ganha forma e corpo: sabemos onde colocá-la.
> (p. 407)

# Relação corpo-moral

> Eu não acabaria nunca se quisesse vos mostrar todos os fatos que poderíamos enumerar para demonstrar esse concurso do corpo e dos símbolos morais ou intelectuais. Olhemos para nós mesmos, neste momento. Tudo em nós todos é imposto. Estou a conferenciar convosco; vedes isso em minha postura sentada e em minha voz, e me escutais sentados e em silêncio. Temos um conjunto de atitudes permitidas ou não, naturais ou não. Assim, atribuiremos valores diferentes ao fato de olhar fixamente: símbolo de cortesia no exército, de descortesia na vida corrente.
> (p. 408)

# Divisão questionável de técnicas do corpo entre sexos

> 1) _Divisão das técnicas do corpo entre os sexos_ (e não simplesmente divisão do trabalho entre os sexos). — O assunto é bastante considerável. As observações de Yerkes e de Kõhler sobre a posição dos objetos em relação ao corpo e especialmente ao regaço, no macaco, podem inspirar comentários gerais sobre a diferença de atitudes dos corpos em movimento em relação a objetos em movimento nos dois sexos. Sobre esse ponto, aliás, há observações clássicas feitas sobre o homem. Seria preciso completá-las. Permito-me indicar a meus amigos psicólogos esta série de pesquisas. Tenho pouca competência e, de resto, não teria tempo. Tomemos a maneira de fechar o punho. O homem fecha normalmente o punho com o polegar para fora, a mulher com o polegar para dentro; talvez porque ela não foi educada para isso, mas estou certo de que, se a educassem, ela teria dificuldades. O soco, o arremesso do golpe, na mulher, são frouxos. E todos sabem que, ao lançar uma pedra, o arremesso da mulher é não apenas frouxo, mas sempre diferente do do homem: plano vertical em vez de horizontal
> (p. 409)

# Divisão de técnicas do corpo entre idades

> 2) _Variação das técnicas do corpo com as idades_. — A criança se agacha normalmente. Nós não sabemos mais nos agachar. Considero isso um absurdo e uma inferioridade de nossas raças, civilizações, sociedades. Um exemplo. Estive no _front_ com australianos (brancos). Eles tinham uma superioridade considerável sobre mim. Quando interrompíamos a marcha sobre a lama ou poças d'água, eles podiam sentar-se sobre os calcanhares, repousar, e a "inundação", como dizíamos, ficava abaixo de seus calcanhares. Eu era obrigado a ficar em pé com minhas botas, todo ereto dentro d'água. A posição agachada é, em minha opinião, uma posição interessante, que pode ser conservada numa criança. É um gravíssimo erro proibir-lhe. Toda a humanidade, exceto nossas sociedades, a conservou.
> (p. 409-10)

# Classificação das técnicas do corpo em relação ao rendimento (~adestramento humano)

> 3) _Classificação das técnicas do corpo em relação ao rendimento_. — As técnicas do corpo podem se classificar em função de seu rendimento, dos resultados de um adestramento. O adestramento, como a montagem de uma máquina, é a busca, a aquisição de um rendimento. Aqui, é um rendimento humano. Essas técnicas são portanto as normas humanas do adestramento humano. Assim como fazemos com os animais, os homens as aplicaram voluntariamente a si mesmos e a seus filhos. As crianças foram provavelmente as primeiras criaturas assim adestradas, antes dos animais, que precisaram primeiro ser domesticados. Numa certa medida, portanto, eu poderia comparar essas técnicas, elas mesmas e sua transmissão, a adestramentos, classificando-as por ordem de eficácia.
> (p. 410)

# Destreza

> Aqui intervém a noção, muito importante em psicologia e em sociologia, de destreza. Mas, em francês, temos apenas um termo ruim, "kaèile", que traduz mal a palavra latina "habilis", bem melhor para designar as pessoas que têm o senso da adaptação de seus movimentos bem coordenados a objetivos, que têm hábitos, que "sabem como fazer". É a noção inglesa de "craft", de "dever" (destreza, presença de espírito e hábito), é a habilidade em alguma coisa. Mais uma vez, estamos claramente no domínio técnico.
> (p. 410-1)

# Classificação da transmissão da forma das técnicas (natureza da educação e adestramento)

> 4) _Transmissão da forma das técnicas_. — Ultimo ponto de vista: o ensino das técnicas sendo essencial, podemos classificá-las em relação à natureza dessa educação e desse adestramento. E eis aqui um novo campo de estudos: incontáveis detalhes inobservados, e cuja observação deve ser feita, compõem a educação física de todas as idades e dos dois sexos. A educação da criança é repleta daquilo que chamam detalhes, mas que são essenciais. Veja-se o problema da ambidestria, por exemplo: observamos mal os movimentos da mão direita e os da mão esquerda, e sabemos mal como são ensinados. Reconhecemos à primeira vista um religioso muçulmano: mesmo quando tem um garfo e uma faca (o que é raro), ele fará o impossível para servir-se apenas de sua mão direita. Ele jamais deve tocar o alimento com a esquerda e certas partes do corpo com a direita. Para saber por que ele não faz determinado gesto e faz outro, não bastam nem fisiologia nem psicologia da dissimetria motora no homem, é preciso conhecer as tradições que impõem isso. Robert Hertz colocou bem esse problema.1 Mas reflexões desse gênero e outras podem aplicar-se a tudo que é escolha social dos princípios dos movimentos.
> (p. 411)

> Há razão de estudar todos os modos de adestramento, de imitação e, particularmente, essas formas fundamentais que podemos chamar o modo de vida, o _modus_, o _tonus_, a "matéria", as "maneiras", a "feição".
> (p. 411)

# Enumeração biográfica das técnicas do corpo

1. Técnicas do nascimento e da obstetrícia
1. Técnicas da infância -- criação e alimentação da criança
1. Técnicas da adolescência
1. Técnicas da idade adulta
    1. Técnicas do sono
    1. Técnicas de repouso (vigília)
    1. Técnicas da atividade, do movimento
    1. Técnicas dos cuidados do corpo
    1. Técnicas do consumo
    1. Técnicas da reprodução
    1. Técnicas da medicaçao, do anormal

> Questões gerais talvez vos interessem mais do que essas longas enumerações de técnicas que apresentei. O que sobressai nitidamente delas é que em toda parte nos encontramos diante de montagens fisio-psicosociológicas de séries de atos. Esses atos são mais ou menos habituais e mais ou menos antigos na vida do indivíduo e na história da sociedade.
> (p. 420)

> Em toda sociedade, todos sabem e devem saber e aprender o que devem fazer em todas as condições. Naturalmente, a vida social não é isenta de estupidez e de anormalidades. O erro pode ser um princípio. Só recentemente a marinha francesa passou a ensinar seus marujos a nadar. Mas o princípio é este: exemplo e ordem. Há portanto uma forte causa sociológica em todos esses fatos. Espero que concordeis comigo
> (p. 420)

# Psicológico como engrenagens

> Por outro lado, já que se trata de movimentos do corpo, tudo supõe um enorme aparelho biológico, fisiológico. Qual a espessura da roda de engrenagem psicológica? Digo propositalmente roda de engrenagem. Um seguidor de Comte diria que não há intervalo entre o social e o biológico. O que posso vos dizer é que vejo aqui os fatos psicológicos como engrenagens e que não os vejo como causas, exceto nos momentos de criação ou de reforma. Os casos de invenção, de posição de princípios, são raros. Os casos de adaptação são de natureza psicológica individual. Mas geralmente são comandados pela educação, e no mínimo pelas circunstâncias da vida em comum, do convívio.
> (p. 420-1)

# Sobre a educação do sangue-frio (?)

> Creio que essa noção de educação das raças que se selecionam em vista de um rendimento determinado é um dos momentos fundamentais da própria história: educação da visão, educação da marcha - subir, descer, correr. É, em particular, na educação do sangue-frio que ela consiste. E este é, antes de tudo, um mecanismo de retardamento, de inibição de movimentos desordenados; esse retardamento permite, a seguir, uma resposta coordenada de movimentos coordenados, que partem então na direção do alvo escolhido. Essa resistência à perturbação invasora é fundamental na vida social e mental. Ela separa entre si, ela classifica mesmo as sociedades ditas primitivas: conforme as reações são mais ou menos brutais, irrefletidas, inconscientes, ou, ao contrário, isoladas, precisas, comandadas por uma consciência clara.
> (p. 421)

# Sobre a possibilidade de técnicas do corpo subjacentes a toda prática religiosa e o protagonismo dos orientais

> Não sei se prestastes atenção ao que nosso amigo Granet já indicou a partir de suas pesquisas sobre as técnicas do taoísmo, técnicas do corpo, da respiração, em particular. Fiz suficientes estudos nos textos sânscritos do loga para saber que os mesmos fatos se verificam na índia. No meu entender, no fundo de todos os nossos estados místicos há técnicas do corpo que não foram estudadas, e que foram perfeitamente estudadas pela China e pela índia desde épocas muito remotas. Esse estudo sócio-psico-biológico da mística deve ser feito. Penso que há necessariamente meios biológicos de entrar em "comunicação com o Deus". E, embora a técnica da respiração etc., seja o ponto de vista fundamental apenas na índia e na China, creio, enfim, que ela é bem mais difundida de um modo geral. Em todo caso, temos sobre esse ponto meios de compreender um grande número de fatos até aqui não compreendidos. Penso também que todas as descobertas recentes em reflexoterapia merecem nossa atenção, a atenção dos sociólogos, depois da dos biólogos e psicólogos... bem mais competentes que nós.
> (p. 422)
