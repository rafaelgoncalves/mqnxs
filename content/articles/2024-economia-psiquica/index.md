---
title: Economia psíquica dos algoritmos
tags:
    - Fernanda Bruno
    - Shoshana Zuboff
    - Matteo Pasquinelli
    - Nick Seaver
    - capitalismo
    - saúde mental
    - modulação
    - captura
    - google
    - facebook
    - economia da atenção
    - algoritmos
    - fichamento

path: "/economia-psiquica-dos-algoritmos"
date: 2024-04-23
category: fichamento
featuredImage: "img.jpg"
srcInfo: <a href="https://medialabufrj.net/projetos/economia-psiquica-dos-algoritmos-racionalidade-subjetividade-conduta-em-plataformas-digitais/">Economia psíquica dos algoritmos</a>
published: true
---

Fichamento do artigo "Economia psíquica dos algoritmos"[^1] de Fernanda Bruno et al.

[^1]: Bruno, Fernanda Glória, Anna Carolina Franco Bentes, e Paulo Faltay. 2019. “Economia psíquica dos algoritmos e laboratório de plataforma: mercado, ciência e modulação do comportamento”. **Revista FAMECOS** 26(3):e33095–e33095. doi: 10.15448/1980-3729.2019.3.33095.

# Resumo

> O extensivo e ininterrupto monitoramento de nossas ações online integram as engrenagens de um investimento econômico que direciona imensos volumes de dados para aplicação de estratégias de modificação do comportamento humano. Nesse contexto,  observamos  um  aumento  do  interesse  tecnocientífico,  econômico  e  social  em processos algorítmicos de extração e utilização de dados psíquicos e emocionais. **Este artigo tem como objetivo apresentar reflexões sobre essa Economia Psíquica dos Algoritmos, enfatizando sua inquietante dimensão laboratorial.** Analisando casos recentes e aspectos da arquitetura das plataformas digitais, mostraremos como o uso desses dados articula **três camadas: a econômica; a epistemológica; e a de gestão e controle comportamental.** Notaremos que o cruzamento dessas camadas tornam as fronteiras entre o laboratório e a vida social extremamente tênues, o que nos convoca a recolocar o problema das relações entre ciência, tecnologia e sociedade nesse novo cenário.

# Facebook, fenômeno de contágio e Cambridge Analytica

> Sem o conhecimento ou autorização dos envolvidos, o experimento [[ _Evidência experimental de contágio emocional em escala massiva através de redes sociais_ (KRAMER; GuIllORy; HANCOCK, 2014 ]] tinha como propósito saber se o humor ou estado emocional desses grupos seria ‘contaminado’ pelo conteúdo visualizado no _feed._ Para tanto, as atualizações de status desses mesmos usuários foram monitoradas. Segundo os autores do artigo, a hipótese de contágio emocional teria sido confirmada pelo experimento. Ou seja, os usuários reproduziram, em suas atualizações de status, o estado emocional preponderante em seus _feed._
> (p. 4)

> Sseguindo a mesma trilha dos rastros psicoafetivos e sociais, a via de acesso às informações de perfis do _Facebook_ pela _Cambridge Analytica_ foi, não por acaso, um teste de personalidade, chamado _thisisyourdigitallife_ e ofertado como um aplicativo da rede social. No que consiste o tal teste de personalidade, que foi a isca mordida por 270 mil pessoas, dando acesso posteriormente, sem que elas soubessem, aos dados  de  sua  rede  de  amigos  na  plataforma,  alcançando  informações  de  aproxi-madamente 87 milhões de perfis? Simplificadamente, o teste utilizado é baseado no modelo Big Five, que em psicometria consiste numa estrutura de cinco grandes fatores (extroversão, neuroticismo, socialização, realização e abertura à experiên-cia) que remetem a dimensões de personalidade. Tais dimensões não representam um sistema teórico específico, e foram elaboradas a partir da análise dos termos que as pessoas usam, em linguagem natural, para definir a si mesmas e aos outros.
> (p. 4)

# Economia psíquica dos algoritmos

> Por economia psíquica dos algoritmos (BRUNO, 2018) designamos **o investimento contemporâneo – tecnocientífico, econômico e social – em processos algorítmicos de captura, análise e utilização de informações psíquicas e emocionais extraídas de nossos dados e ações em plataformas digitais (redes sociais, aplicativos, serviços de streaming, plataformas de compartilhamento e/ou consumo de conteúdo audiovisual etc.).** As informações que interessam ao veloz capitalismo de dados não são mais apenas os rastros de nossas ações e interações (cliques, curtidas, compartilhamen-tos, visualizações, postagens), mas também **sua “tonalidade” psíquica e emocional.** É esta economia psíquica e afetiva que alimenta as atuais **estratégias de previsão e indução de comportamentos nas plataformas digitais (e eventualmente fora delas).**
> (p. 5)

# Dados como: "moeda", conhecimento e poder de controle

> No seio desta lógica, os dados pessoais digitais e suas informações psíquicas e emocionais são simultaneamente: a principal “moeda” do modelo de negócios que prevalece nas plataformas digitais; a fonte privilegiada de conhecimento de uma nova ciência de dados; um meio de controle do comportamento, orientado para diferentes fins, do consumo ao voto.
> (p. 5)

> Essa tríplice característica dos dados psíquicos e emocionais constituem as três camadas da economia psíquica dos algoritmos que serão exploradas neste artigo: **a camada propriamente econômica ou mercadológica; a camada epistemológica, voltada para a produção de conhecimento sobre indivíduos e populações; e a camada de gestão e controle comportamental.** Notaremos que elas se entrecruzam no cotidiano das plataformas e aplicativos digitais e assumem uma dimensão laboratorial extremamente inquietante.
> (p. 5-6)

# Laboratório-mundo, ciência de plataforma, laboratório de plataforma

> As fronteiras entre o laboratório e a vida social, política e subjetiva tornam-se extremamente  tênues.  **Estamos  diante  de  um  laboratório-mundo  ou  de  uma  ciência de plataforma,** intimamente conectados às engrenagens do mercado de  dados  pessoais,  em  que  uma  complexa  e  crescente  economia  psíquica  e  emocional nutre algoritmos que pretendem nos conhecer melhor do que nós mesmos, além de fazer previsões e intervenções sobre nossas emoções e condu-tas (BRUNO, 2018).
> (p. 6)

# Capitalismo de dados (West): baseado na extração e mercantilização de dados digitais

> 4: Conforme veremos no próximo tópico deste artigo, **o capitalismo de dados é definido como um sistema baseado na extração de valor e na mercantilização de dados digitais, perpassando as dimensões sociais, políticas e econômicas das redes sociotécnicas.** Segundo West (2017), **é um sistema no qual a comodificação de nossos dados engendra uma redistribuição assimétrica de poder, de modo a consolidar e fortalecer os atores que têm o acesso e a capacidade de dar sentido a tais informações.** Tal noção também dialoga, em alguma medida, com as noções de capitalismo de vigilância, proposta por shoshana Zuboff (2018) e de capitalismo de plataforma, formulada por Nick Srnicek (2017).
> (p. 5)

> Conforme apontamos, a atual dinâmica do capitalismo de dados, centrado no modelo de negócios das plataformas e aplicativos digitais, tem como um de seus  pilares  a  extração  de  valor  de  dados  provenientes  de  **mecanismos  automatizados de coleta e análise de nossas ações e comportamentos online.** Sob a ordem de grandeza do big data e a velocidade da gestão algorítmica, os difusos processos de monitoramento digital estão cada vez mais atrelados a estratégias econômicas que visam prever e modificar o comportamento humano.
> (p. 6)

# Venda de dados e de acesso ao fluxo informacional

> No entanto, a dinâmica em jogo nesta economia não se resume à venda de dados para publicidade, mas **inclui também a venda do acesso em tempo real ao fluxo de ações online de indivíduos e populações que, através de ferramentas de análise algorítmicas, procuram influenciar e modificar os comportamentos a fim de gerar lucro** (ZUBOFF, 2016).
> (p. 7)

# Google e sociedade do metadado em Pasquinelli

> 7: Matteo Pasquinelli (2015) aponta a criação do **primeiro centro de processamento de dados do Google, conhecido como Google Cage, como o marco inicial do que ele conceitua como sociedade dos metadados.** Segundo o autor, **foi o primeiro banco de dados a operar de acordo com um mapeamento em escala global da topologia da internet e de suas tendências, de modo que “nos últimos anos, a sociedade em rede radicalizou uma mudança topológica: sob a superfície da rede, datacenters gigantescos foram transformados em monopólios de dados coletivos.** Se as redes eram sobre fluxos abertos de informação (como Manuel Castells costumava defender), os datacenters são sobre o acúmulo de **informações sobre informações, isto é, metadados.** Além disso, sobre o algoritmo do Google, o PageRank, Pasquinelli (2010) afirma: “PageRank descreve especificamente o valor de atenção de qualquer objeto, a tal ponto que se tornou a principal e mais importante fonte de visibilidade e autoridade, mesmo fora da esfera digital”.
> (p. 6)

# Captura ininterrupta de small data

> No cotidiano de plataformas digitais online, são coletados inúmeros tipos de informações de diferentes fontes. Nesta colheita, qualquer tipo de informação é relevante: **desde “curtidas” do Facebook, passando pelas buscas no Google, e-mails, textos, fotos, músicas e vídeos, localizações, padrões de interações, redes, compras, movimentos, todos os cliques, até palavras com erros ortográficos, mensagens escritas e apagadas, velocidade de digitação, visualizações de páginas, e muito mais.** Ainda que a escala do “big data” seja constituída por uma captura constante e inin-terrupta de todo tipo de “small data” (ZuBOFF, 2018), desejamos ressaltar neste artigo o crescimento expressivo da relevância dos dados psíquicos e emocionais.
> (p. 7)

# Aprendizado de máquina

> 8: Nos referimos aqui às ferramentas de _machine learning_, que conferem aos códigos de um conjunto de algoritmos a capacidade de autoajuste e autocorreção, de modo a se adaptarem e revisarem seus resultados a partir de ações anteriores. O conjunto de algoritmos do feed do Facebook e do Page Rank do serviço de buscas do Google são exemplos: a cada interação na plataforma ou no buscador, seus algoritmos «aprendem» quais conteúdos seriam de maior relevância ou interesse dos usuários.
> (p. 7)

# Captura de dados psíquicos e emocionais pela Facebook

> Além  dos  casos  já  citados,  este  interesse  é  visível,  por  exemplo,  na  popularização  de  ferramentas  voltadas  para  a  expressão  e  **captura  de  emoções  e  estados psíquicos dos usuários em plataformas e aplicativos: emoticons, emojis, GIFs animados, stickers etc.** Numa plataforma como o Facebook, podemos ver claramente  a  ampliação  desse  investimento:  o  primeiro  passo  explícito  nessa  direção se dá em 2009, com o lançamento do botão de “Curtir” (Like); em 2013, o espectro de expressão de emoções e estados psíquicos amplia-se enormemente com a opção de “Atualização de status” (_Status Update fields_), permitindo que o usuário utilize uma grande diversidade de ícones gráficos para indicar a tonalidade emocional e psíquica de sua postagem. Na categoria “sentimento/Atividade” o usuário pode escolher o sentimento que lhe é mais pertinente entre um leque de mais de 200 “carinhas” que correspondem a confiante, inspirado, esperanço-so, frustrado, exausto, nostálgico, sexy etc. Em 2016, uma nova funcionalidade emocional passa a acompanhar o já banal botão “Like”: os “ícones de Reação” (Reaction Icons) permitem que qualifiquemos as postagens dos outros segundo um espectro de seis emoções básicas (Curtir, Amei, Haha, uau, Triste e Grr).
> (p. 7-8)

# Aspecto intencional de criação de uma infraestrutura de captura de dados relacionais

> Nota-se o quanto **há todo um design e uma arquitetura voltados para alimentar algoritmos de plataformas e aplicativos com dados psíquicos e emocionais, de modo a torná-los disponíveis para o cálculo computacional.** Além disso, como já apontamos, não são apenas os dados de cada um que importam aqui, mas, sobretudo, o seu valor relacional (GERLITZ; HELMOND, 2013). Assim, a relação entre as pessoas (LURY; DAY, 2019), bem como a interação entre seus processos emocionais e psíquicos se tornam disponíveis para o cálculo computacional, alimentando não apenas modelos de negócios das plataformas digitais, como também os modelos de conhecimento e gestão dos comportamentos.
> (p. 8)

# Dividualidades ~ correlações entre indivíduos (padrões supraindividuais ou interindividuais)

> Mais  precisamente,  trata-se  de um saber que se exerce privilegiadamente sobre dividualidades9 e não tanto sobre indivíduos (DELEUZE, 1992; LAZZARATO, 2014; ROUVROY; BERNS, 2015). Por  isso,  no  caso  do  _Cambridge  Analytica_,  **os  dados  que  mais  interessam  são  aqueles que derivam das correlações entre os padrões de atividade dos usuários do Facebook e os perfis psicológicos** – e não tanto os perfis psicológicos em si.
> (p. 8)

> 9: Referimo-nos ao conceito empregado por Deleuze (1992) para caracterizar **os processos de subjetivação característicos das sociedades de controle,** que deixam de operar predominantemente com a norma disciplinar e a produção de individualidade(s) para privilegiar a modulação de diferenças e a condução das condutas através do reconhecimento de padrões de componentes parciais da subjetividade.
> (p. 8)

> Com base no modelo dos pesquisadores da _Cambridge University_, o teste de personalidade utilizado pela _Cambridge Analytica_ visa menos um conhecimento individualizado, unificado e aprofundado da personalidade de indivíduos específicos do que **um conhecimento sobre as correlações entre os diferentes traços de personalidade e de atividade de inúmeros perfis.** Assim, o conhecimento dessas correlações  pretende  revelar  padrões  supraindividuais  ou  interindividuais  que  permitam fazer predições em larga escala.
> (p. 9)

> Essa similaridade, vale dizer, concerne a “parcelas” de seu perfil de atividade e de personalidade – como, por exemplo, a correlação entre seu grau de extroversão (indicado no teste de personalidade) e o seu número de amigos e a frequência com que atualiza seu status e interage em grupos no Facebook(indicados  no  seu  padrão  de  atividade).10  Ou  entre  o  seu  nível  de  socialização  indicado no teste de personalidade e a frequência com que aparece em fotos com outros usuários. **Um conhecimento sobre a correlação entre traços parciais (dividuais, portanto), e não tanto sobre indivíduos considerados em sua unidade.**
> (p. 9)

# Captura (dividual) e modulação (individual)

> Ao mesmo tempo, como vimos, esse saber extraído das correlações entre dados parciais de nossos perfis e condutas visa orientar conteúdos cada vez mais específicos e pertinentes a indivíduos particulares. Ou seja, **um saber extraído de dados parciais e relacionais geraria uma “inteligência” que se pretende preditiva sobre alvos individuais cada vez mais precisos** (em especial, como invocado na fala de Alexander Nix citada na introdução, com uma ênfase maior na segmentação psicométrica do que na demográfica). Vemos, portanto, que **se trata de um conhecimento que opera sobre uma dupla escala – dividual e individual – visando inferir algoritmicamente a personalidade de pessoas que se tornarão objetos futuros de diferentes tipos de ações** (_microtargeting_, sistemas de recomendação, direcionamento de conteúdos etc.). **Neste sentido, esta condução algorítmica de condutas pode não levar em conta sujeitos específicos, mas não deixa de mirar alvos** (ROUVROY; BERNS, 2015).
> (p. 9)

# Economia comportamental (~sujeitos previsivelmente irracionais)

> um exemplo disso, segundo Nadler e McGuigan (2017), consiste nas estra-tégias de marketing digital e de design de plataforma que têm se apropriado da linguagem e das técnicas da _Economia Comportamental_, reunindo um conjunto de teorias do campo psicológico – tais como **o behaviorismo, a psicologia cognitiva, psicologia evolutiva e a neuropsicologia** – para **desenvolver modelos que buscam identificar e prever padrões da forma como as pessoas tomam decisões econômicas, de modo a intervir sobre essas escolhas.** Nas aplicações computacionais que têm essas teorias como modelo epistemológico, os usuários não são concebidos como consumidores racionais e perfeitamente informados, mas sim como impulsivos e “previsivelmente irracionais”.12
> (p. 10)

# Arquitetura de decisões

> Assim,  fatores  contextuais  e  tendências  cognitivas  são  explorados  para  construir o que os economistas comportamentais chamam de “arquitetura de decisões”, isto é, **uma organização específica do contexto no qual as decisões são tomadas a fim de influenciar a ação das pessoas em certa direção.** Tais técnicas podem envolver desde **a elaboração da interface, o design de softwares, os recursos técnicos das próprias plataformas, até os sistemas de recomendação.**
> (p. 10)

# econômico, epistemológico, político -> captura, imaginação, modulação

> Fica mais claro, a essa altura, de que modo se entrecruzam as três camadas que retroalimenta a economia psíquica dos algoritmos. **Processos automatizados de captura, análise e utilização de dados psíquicos e emocionais estão na base de um modelo de negócios que é inseparável de modelos específicos de conhecimento sobre a cognição e o comportamento humanos que, por sua vez, estão atrelados a estratégias de gestão de condutas, como veremos no próximo tópico.**
> (p. 10)

# O modelo preditivo: previsão e intervenção automática sobre comportamentos futuros

> A breve história do monitoramento de rastros digitais na internet sempre esteve relacionada a mecanismos de intervenção sobre as condutas online (BRUNO, 2012 e 2013). Apesar da diversidade de dispositivos e aplicações dos sistemas automatizados de monitoramento e análise dos rastros digitais, podemos afirmar que **o modelo preditivo prevaleceu na última década** (BRUNO, 2013; ZUBOFF, 2018). Conforme já apontamos, **tal modelo consiste em mecanismos automatizados de captura, processamento e análise do maior volume e diversidade possível de dados, buscando extrair padrões que orientam previsões e consequentemente intervenções sobre comportamentos futuros.** Esse paradigma é patente na conclusão do artigo sobre a pesquisa realizada com base no teste myPersonality, mencionada no tópico anterior:
> 
> > [...] o estudo mostra que, combinando várias características, pode-mos fazer previsões relativamente precisas em relação à personali-dade de um indivíduo, sendo a Extroversão a mais fácil de prever e a socialização sendo a mais elusiva. uma aplicação potencial para o nosso trabalho é a publicidade online e os sistemas de recomendação. Analisando as informações das redes sociais, seria possível “perfilar” os indivíduos, dividindo automaticamente os usuários em diferentes segmentos e adaptando os anúncios a cada segmento com base na personalidade. Da mesma forma, pode-se imaginar a construção de sistemas  de  recomendação  baseados  em  perfis  de  personalidade  (BACHRACH e outros, 2012, p. 31, tradução nossa).
> 
> (p. 11)

# Modelo de captura/engajamento: intervenção em tempo real no fluxo comportamental

> Entretanto, tal centralidade começa a ser perturbada ou disputada por outro modelo: o da captura ou do engajamento.
> (p. 11)

> Por que gastar tempo e inteligência computacional prevendo comportamentos se as plataformas e aplicativos permitem intervenções em tempo real sobre a conduta dos usuários? De uma certa perspectiva, **podemos ver o modelo da captura/engajamento como uma espécie de aceleração do modelo preditivo: o aumento da capacidade e velocidade de monitoramento e processamento em tempo real das ações dos usuários online torna dispensável a previsão, permitindo que os algoritmos atuem de modo ainda mais performativo do que no modelo preditivo, intervindo no próprio fluxo das condutas enquanto elas acontecem.**
> (p. 11-2)

> Tais mudanças podem ser observadas na trajetória dos sistemas de recomendação algorítmicos, ferramenta que vem se espraiando e adquirindo protagonismo na mediação da oferta de conteúdo cultural, comercial e político por diversas plataformas. Nos últimos anos, **desenvolvedores digitais têm se voltado cada vez mais para a arquitetura e o design desses sistemas, visando não apenas prever preferências, interesses e comportamentos futuros, mas sobretudo capturar, enganchar e engajar a atenção de usuários.**
> (p. 12)

# Previsão (inferência) -> captura (incidência)

> Nessa  mudança  de  um  paradigma  preditivo  para  um  paradigma  de  captura,  **tais ferramentas algorítmicas funcionam cada vez mais com o objetivo de manter os usuários o máximo de tempo conectados às plataformas digitais.** Enquanto no primeiro “um sistema de recomendação prevê como os usuários avaliarão os itens e é julgado pela precisão de suas previsões” (SEAVER, 2018, p.10, tradução nossa), tendo como contraprova de acerto e de satisfação a avaliação explícita dos usuários (likes, número de estrelas ou notas de avaliação, por exemplo), o segundo tem como premissa que “ser preciso não é suficiente” (MCNEE; RIEDL; KONSTAN, 2006) e que **a eficiência de um sistema de recomendação é medida pela capacidade em capturar a atenção e produzir o engajamento dos usuários** (BENTES, 2019).
> (p. 12)

# Giro captológico (Seaver)

> Este movimento que **Seaver denomina de virada captológica (_captological turn_) tem como principal referência o trabalho desenvolvido por B.J Fogg, fundador do Persuasive Technology lab e criador do campo de pesquisa que ele designou por “captology”, termo derivado da sigla em inglês de computers as persuasive technologies.** Ligado à universidade de stanford, Fogg é um dos precursores no desenvolvimento de modelos e métodos no campo do **“behavioral design”, que combina teorias da psicologia behaviorista com a psicologia cognitivo-comportamental, a economia comportamental e as neurociências em aplicações para a economia digital e a indústria computacional.**
> (p. 12-3)

> De  acordo  com  a  apresentação  no  site  do  laboratório,  o  objetivo  dessas  técnicas e saberes é **“criar respostas sobre como produtos de computação – de sites a softwares de smartphones – podem ser projetados para alterar crenças e comportamentos”** (STANFORD PERSUASIVE TECH LAB, 2019, tradução nossa).
> (p. 13)

> Vemos, portanto, que essa virada não implica apenas uma mudança no funcionamento desses sistemas, mas também um deslocamento de como são percebidos, conhecidos e operados a satisfação e o desejo das pessoas, bem como suas crenças e comportamentos. As plataformas de recomendação e de sociabilização deixam, assim, de privilegiar formas explícitas de avaliação feitas pelos próprios usuários. **Em vez disso, passam a valorizar tipos de métricas implícitas e a sua tonalidade psíquica e emocional como evidências dos juízos e preferências das pessoas.**
> (p. 13)

> Por exemplo, ao invés de priorizar notas de classificações, comentários, compartilhamentos, **tornam-se cada vez mais relevantes para o funcionamento desses sistemas  certas  informações  como  o  tempo  médio  gasto  em  tipos  diferentes  de postagens, a pausa em um vídeo, o padrão de navegação, pular uma música recomendada ou um determinado conteúdo.**
> (p. 13)

# Captura ~ engajamento

> Essas  mudanças  nas  estratégias  de  gestão  algorítmica  da  conduta  estão  diretamente ligadas ao modelo de negócios das plataformas digitais, como mencionado anteriormente. Para a expansão deste mercado de dados, uma condição é fundamental: **que os usuários gastem o maior tempo possível em plataformas ou dispositivos, para, assim, seus dados serem extraídos e seus comportamentos se tornarem reconhecíveis e suscetíveis a intervenções.**
> (p. 13)

# Economia da atenção

> Nesse  sentido,  a  atual  dinâmica  do  capitalismo  de  dados  está  intimamente  ligada aos já conhecidos mecanismos de uma economia da atenção (GOLDHABER, 1997; DAVENPORT; BECK, 2001). Sua premissa fundamental é a de que, **em meio a um espaço-tempo cada vez mais saturado de estímulos visuais e informacionais (CRARY, 2014), a atenção se torna um recurso escasso e, por sua vez, extremamente valioso e imensamente disputado** (CAlIMAN, 2012; CITTON, 2016; WU, 2016).
> (p. 13)

# Lógica circular da economia da atenção

> Não por acaso, como vimos, a disputa econômica pela atenção vem popularizando um tipo de abordagem na indústria tecnológica que aposta no **desenvolvimento de uma arquitetura de decisões nas plataformas direcionada para capturar e cativar a atenção dos usuários, explorando tendências e vulnerabilidades cognitivas que mantenham os usuários enganchados e engajados nesses serviços.** Engendrando, assim, uma lógica circular entre a experimentação da dimensão laboratorial e os efeitos sociais, políticos e subjetivos do capitalismo de dados.
> (p. 13-4)

# Criação de hábitos

> Por  isso,  **um  dos  principais  objetivos  das  empresas  de  tecnologia  no  paradigma da captura é fazer com que o uso de seus serviços não seja apenas um comportamento pontual, mas se torne um hábito.** Entendido, nesse contexto tecnobehaviorista do design e da arquitetura das plataformas, como **“comportamentos automáticos desencadeados por pistas situacionais: coisas que fazemos com pouco ou nenhum pensamento consciente”** (EYAl, 2014, p.8), o hábito é construído por pequenos estímulos e recompensas. Tais comportamentos são assim incentivados a se tornar uma prática rotineira, de modo que usuários retornem constantemente a esses serviços e preferencialmente nunca saiam deles.
> (p. 14)

# Arendt sobre o behaviourismo

> > O problema das modernas teorias do behaviorismo não é que estejam erradas, mas sim que possam vir a tornarem-se verdadeiras, que realmente constituam as melhores conceituações possíveis de certas tendências óbvias da sociedade moderna (ARENDT, 1999, p. 336).
>
> (p. 14)

# Experiência humana como matéria-prima e automatização do comportamento em Zuboff 

> Como chama atenção Zuboff (2019), **no capitalismo de vigilância, a experiência humana é tomada como matéria-prima disponível e acessível gratuitamente a um tipo de poder que não apenas automatiza o fluxo de informações sobre nós, mas visa automatizar nosso próprio comportamento.** Não à toa, o denunciante Chris Wylie, em sua declaração ao The Guardian (CADWALLADR, 2018), referiu-se diversas vezes ao caso da _Cambridge Analytica_ e Facebook como um “experimento” em que se tratava de elaborar “armas psicológicas” para uma “guerra cultural cujo campo de batalha seria a internet (as mídias sociais) e o alvo, cada um de nós”.
> (p. 15)

> A leitura de um trecho das conclusões de uma das pesquisas tomadas como referência para empresas como a Cambridge Analytica e similares é suficiente para interrogar o quanto suas conclusões talvez sejam superestimadas:
> 
> > Mostramos  que  registros  digitais  facilmente  acessíveis  de  comportamento, Facebook Likes, podem ser usados para prever automaticamente e com precisão uma variedade de atributos pessoais altamente sensíveis, **incluindo orientação sexual, etnia, visões religiosas e políticas, traços de personalidade, inteligência, felicidade e uso de substâncias aditivas,  separação  parental,  idade  e  sexo**  (KOSINSKI,  STILLWELL,  GRAEPEL, 2013, p.5082, tradução nossa).
>
> (p. 15)

# O uso do erro no AdM

> Os erros não significam, neste contexto, ausência de efeitos. A descoberta de padrões por indução estatística, base da análise algorítmica, não exclui de seu aprendizado os desvios e anomalias (PAsquINEllI, 2015).
> (p. 16)

# Taxa pequena de influência no experimento de contágio da Facebook ainda assim é significativa

> Retomando o caso do experimento de contágio emocional, citado anterior-mente, vale enfatizar que, apesar de afirmarem que a hipótese foi comprova-da,  os  resultados  foram  estatisticamente  baixos,  cientificamente  irrelevantes,  poderíamos dizer. Segundo o artigo (KRAMER, GUILLORY E HANCOCK, 2014), quando postagens positivas foram reduzidas no News Feed, a porcentagem de palavras positivas nas postagens das pessoas diminuiu 0,1%, enquanto o percen-tual de palavras negativas aumentou 0,04%. Já quando os posts negativos foram reduzidos, as palavras negativas postadas por usuários diminuíram em 0,07%, e o número de palavras positivas aumentou 0,06%.
> (p. 16)

> Entretanto, um aspecto citado no artigo de modo secundário, sem maiores detalhes, como uma espécie de “efeito colateral” do experimento nos dá importantes pistas sobre a mudança do interesse das empresas por dados psíquicos e emocionais e sobre o deslocamento do paradigma preditivo para o da captura/engajamento: **notou-se que a exposição dos usuários a conteúdos emocionais, tanto positivos quanto negativos, os tornou mais ativos e engajados na plataforma.** Este resultado nos aponta que os processos de gestão algorítmica de dados psíquicos e emocionais podem produzir efeitos e consequências na modulação e controle do comportamento das pessoas mesmo sem inferir necessariamente atributos como “personalidade” ou “identidade”.
> (p. 16)

> Nesse caso, vemos bem como **a modulação e o controle do comportamento da economia psíquica dos algoritmos são exercidos privilegiadamente através de alterações no CONTEXTO e no AMBIENTE da oferta de conteúdo, propiciados pela arquitetura e o design das plataformas.** Embora alimentada por dados psíquicos e emocionais, o valor dessa economia psíquica dos algoritmos não deriva propriamente da acuidade de previsões de personalidade, mas sim da **capacidade de intervir em tempo real nas ações e emoções dos usuários.**
> (p. 16)

> O laboratório aqui em questão é, sobretudo, performativo. Numa base gigantesca de conteúdos direcionados para centenas de milhões de “alvos”, **mesmo uma margem relativamente baixa de acertos já é bastante alta se comparada aos  métodos  tradicionais  de  propaganda  direcionada.**  Como  reconhecem  os  autores  do  experimento  de  contágio:  “dada  a  escala  maciça  de  redes  sociais como o Facebook, mesmo pequenos efeitos podem ter grandes consequências” (KRAMER; GUILLORY; HANCOCK, 2014, tradução nossa).
> (p. 16-7)

# A predição acaba sendo uma consequência da normalização (ou seja, da modulação da atividades convergentes com as predições).

> O  desenvolvimento  desses  programas [[ tais quais o Big Five ]]  **requer  uma  escrita  ou  linguagem  que seja, ao mesmo tempo, legível para os padrões e lógicas da máquina, como também operável para a tradução de outputs interpretáveis por humanos.** Um diagnóstico  similar  é  proposto  pelo  artista  e  pesquisador  Ruben  van  de  Ven  (2017), em seu estudo de sistemas automatizados de análise de emoções a partir os rostos humanos: **a utilização e legitimação dessas tecnologias seria menos capaz de aferir os estados emocionais das pessoas do que de normalizar o que se entende por emoções como “raiva”, “tristeza” e “desprezo”. Ao invés de “dar novos insights sobre como os seres humanos interagem, esses sistemas reforçam  uma  pré-concepção  existente  do  que  são  as  emoções.  Por  essa  razão,  a  tecnologia fornece uma diretriz para os humanos se expressarem”.**
> (p. 17)

# AdM como fabricação de mundos

> As inquietações que daí derivam não são apenas sobre ciência mal aplicada, ou sobre negócios e propagandas, nem só sobre vigilância e privacidade. **É sobre fabricação de mundos. O que está em jogo é uma economia psíquica dos algoritmos que, com suas estratégias próprias, extrai valor e capitaliza nossa atenção, nossos estados psíquicos e afetivos a fim de produzir efeitos reais nas paisagens de dados e informações por onde trafegamos, em nossa percepção e em nossas condutas.**
> (p. 17-8)
