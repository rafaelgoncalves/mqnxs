---
title: "Mitigando vieses no aprendizado de máquina: uma análise sociotécnica"
tags:
    - Lívia Ruback
    - Denise Carvalho
    - Sandra Avila
    - algoritmos
    - aprendizado de máquina
    - viés
    - gênero
    - interseccionalidade
    - sexismo algorítmico
    - fichamento

path: "/mitigando-vieses"
date: 2024-11-12
category: fichamento
featuredImage: "./fig3.png"
srcInfo: "Fig. 3: Vieses inseridos no aprendizado de máquina."
published: true
---

Fichamento do artigo "Mitigando vieses no aprendizado de máquina"[^1].

[^1]: Ruback, Lívia, Denise Carvalho, e Sandra Avila. 2022. “Mitigando Vieses no Aprendizado de Máquina: Uma Análise Sociotécnica”. iSys - Brazilian Journal of Information Systems 15 (1): 23:1-23:31. <https://doi.org/10.5753/isys.2022.2396>.

# Abstract

> Este artigo apresenta uma análise sociotécnica sobre os vieses inseridos durante o aprendizado de máquina. **Descrevemos aqui quatro tipos de vieses: vieses históricos, vieses nos dados, vieses no modelo e vieses de interpretação humana;** apontamos como eles podem ser inseridos nos modelos durante o processo de aprendizado e suas implicações sociais e culturais.  **Apontamos também as direções para mitigar estes vieses, que incluem soluções computacionais, como o balanceamento das bases de dados utilizadas para o treinamento dos modelos e das métricas alternativas para avaliar estes modelos, até soluções não computacionais, regulação do uso dos modelos e polı́ticas para promover a diversidade na tecnologia e na academia.**
> (p. 23:1)

# Aprendizado de máquina como subcampo da IA

> O aprendizado de máquina **surgiu como um subcampo da Inteligência Artificial que projeta algoritmos que aprendem a partir de grandes quantidades de exemplos — ou dados — relacionados a um determinado fenômeno** [Mitchell 1997]. Hoje, o aprendizado de máquina é utilizado em uma infinidade de sistemas, desde sistemas de recomendação, sistemas de recrutamento, sistemas de tradução automática, de liberação de créditos, sistemas prisionais e em serviços de vigilância pública por reconhecimento facial.  Nesta seção, apresentamos alguns conceitos importantes relacionados ao aprendizado de máquina e detalhamos as suas etapas.
> (p. 23:3)

# AdM supervisionado, não supervisionado e por reforço

> O aprendizado de máquina, ou aprendizagem de máquina, é classificado em tipos, de acordo com os objetivos dos algoritmos. **O mais frequentemente usado na prática é o aprendizado _supervisionado_, realizado a partir de um conjunto de dados** (dataset, em Inglês) contendo exemplos _rotulados_. O principal objetivo do aprendizado supervisionado é aprender a partir de dados treinados e rotulados (ou seja, respostas corretas) de forma a ser capaz de gerar um _modelo_ que faça novas predições, para dados não rotulados (não treinados) [Raschka 2015]. **Os demais aprendizados — _não supervisionado_ e _por reforço_ — não utilizam dados rotulados e não serão abordados neste trabalho.**
> (p. 23:3)

# AdM supervisionado: regressão e classificação

> O aprendizado de máquina supervisionado — baseado em rótulos de exemplos aprendidos e capaz de prever rótulos para exemplos futuros — é subdividido em duas categorias: _classificação_ e _regressão_. Ambas as tarefas aprendem a partir de dados rotulados, porém, **para tarefas de classificação, como tarefas de classificação de e-mails como spam, o rótulo predito é um valor categórico, como sim ou não. Já para as tarefas de regressão, o valor predito é um valor contı́nuo, como tarefas de previsão do tempo ou de previsão de custos** [Raschka 2015].
> (p. 23:4)

# Pipeline do AdM supervisionado

![Fig. 1: Etapas do aprendizado de máquina supervisionado.](fig1.png)

# Coleta de dados

> O processo de aprendizado de máquina começa tipicamente com a coleta de dados. **Tais dados são compostos por duas partes: entrada e saı́da. A entrada pode conter quaisquer caracterı́sticas dos exemplos a serem treinados e a saı́da é o rótulo a ser predito.** Por exem- plo, para um sistema que irá detectar automaticamente e-mails que são spam, a entrada é um conjunto de mensagens de e-mail, já rotulado como “spam” ou “não spam”, e a saı́da é o mesmo rótulo, predito para mensagens de e-mail ainda não conhecidas [Burkov 2019].  Já para um sistema que aprende a detectar faces em imagens, as entradas são as imagens já rotuladas e a saı́da é o rótulo “sim” (quando o modelo detecta a face) ou o rótulo “não” (caso contrário). Em alguns sistemas que detectam faces, a saı́da pode ser, também, as coordenadas do retângulo (bounding box, do Inglês) contornando o rosto detectado.
> (p. 23:4)o

# Pré-processamento

> Os dados brutos passam então pela etapa de pré-processamento (Figura 1), **que tem como objetivo transformar os dados brutos no conjunto de dados utilizado para o aprendizado. Esta tarefa geralmente é chamada de engenharia de caracterı́sticas** (_feature engineering_, em Inglês) e tem como objetivo selecionar — ou criar — as caracterı́sticas (ou atributos) mais _informativas_, com alto poder preditivo, que permitem construir um modelo que dê boas predições (com uma taxa maior de acertos). Algumas vezes, porém, principalmente quando o conjunto de dados é produzido manualmente, estas caracterı́sticas podem estar ausentes e então é necessária a remoção ou substituição de dados incompletos ou inválidos.
> (p. 23:5)

# Treinamento e avaliação

> **Os dados pré-processados são embaralhados e particionados em dois conjuntos: o conjunto de treinamento e o conjunto de teste.** O conjunto de treinamento geralmente é maior e é utilizado para construir (ou treinar) o modelo; já o conjunto de teste é utilizado para avaliar o modelo antes de liberá-lo para ser integrado nos sistemas. Tipicamente, o conjunto de treinamento representa 70% dos dados e o de teste representa 30% dos dados [Burkov 2019]. Por exemplo, se um sistema que usa dados de treinamento para detecção de faces coleta 1000 imagens aleatórias de rostos, seguindo tal proporção, 700 delas são utilizadas para o treinamento e 300 para teste.
> (p. 23:5)

> É importante reforçar que **o aprendizado só funciona porque o modelo _aprende_ com um conjunto de dados (o de treinamento) e é _avaliado_ com um outro conjunto de dados (o de teste).** A avaliação, neste contexto, é um processo incremental nas duas direções: o modelo é testado e otimizado até que atinja um desempenho considerado bom o suficiente. Dessa forma, caso o modelo gerado tenha um bom desempenho na sua avaliação, ele será bom em predizer novos exemplos que o algoritmo de aprendizagem ainda não conheceu, ao invés simplesmente “memorizar” os exemplos de treinamento.
> (p. 23:5-6)

> Na prática, há ainda um terceiro conjunto: o conjunto de validação. Este conjunto de dados é geralmente utilizado para escolher o algoritmo para o aprendizado e encontrar os melhores valores das variáveis que definem o modelo matemático aprendido pelo algo- ritmo (chamados de hiperparâmetros do algoritmo). Neste trabalho, porém, nos referimos a ambos os conjuntos (validação e teste) somente como conjunto de teste, para simplificar o entendimento.
> (p. 23:6)

# Criação e avaliação do modelo

Exemplo de uma matriz de confusão para classificação

![Fig. 2: Exemplo hipotético de matriz de confusão para reconhecimento facial.](fig2.png)

> A matriz de confusão apresentada na Figura 2 é o principal mecanismo para se avaliar se o modelo teve um bom desempenho, pois a partir dos valores que ela apresenta, são calculadas diferentes métricas. A Tabela 1 apresenta as principais métricas utilizadas para avaliar o desempenho de classificadores. Apresentamos, juntamente com a fórmula utilizada para o cálculo, a descrição de cada métrica, exemplos de casos e a pontuação de cada métrica para o nosso exemplo.
> (p. 23:7)

![Tab. 1](tab1.png)

## Precisão

> Muitas vezes, porém, a acurácia sozinha para avaliar um modelo não é sufici- ente. Em sistemas que usam reconhecimento facial para a identificação de criminosos, por exemplo, os falsos positivos representam casos em que o modelo reconhece incorre- tamente uma face, o que pode levar a prisões de pessoas acusadas injustamente. Um outro exemplo de falsos positivos que pode trazer muitos prejuı́zos são os modelos de detecção de spam (um falso positivo em tais modelos representa um e-mail que foi classificado como spam, mas não era spam). Para estes casos, a métrica de precisão (_precision_, do Inglês) é mais indicada. A precisão é a proporção entre os verdadeiros positivos e o total de positivos. Quanto mais falsos positivos, menor será a precisão (fórmula da Tabela 1), ou seja, um modelo com poucos falsos positivos tem uma boa precisão. No exemplo da Figura 2, a precisão seria de 70 (verdadeiros positivos) / 100 (total de positivos), indi- cando que o modelo teve uma precisão de 70%, ou seja, a probabilidade de um indivı́duo reconhecido pelo modelo de reconhecimento facial ser culpado é de 70%.
> (p. 23:7-8)

## Revocação

> Em outras vezes, queremos evitar ao máximo os falsos negativos, como por exem- plo, em modelos de previsão de diagnósticos de doenças (um diagnóstico negativo de uma doença existente diminui as suas chances de tratamento). Para estes casos, utilizamos a métrica _revocação_ (_recall_, do Inglês). A revocação é a proporção entre os verdadeiros positivos e o total de exemplos que são de fato verdadeiros. Ou seja, quanto mais falsos negativos, menor será a revocação. No exemplo da Figura 2, a revocação seria de 70 (verdadeiros positivos) / 80 (total de exemplos que são de fato positivos), indicando que o modelo teve uma revocação de 87%, ou seja, a probabilidade de um culpado ser cor- retamente reconhecido pelo modelo de reconhecimento facial é de 87, 5%. A revocação também é conhecida como Taxa de Verdadeiros Positivos (TVP).
> (p. 23:8-9)

## Taxa de falsos positivos

> Em outras vezes, queremos analisar a taxa de falsos positivos, ou seja, a proporção de falsos positivos em relação a todos os casos que eram de fato negativos. Esta métrica é conhecida como Taxa de Falsos Positivos (TFP). Nos modelos de reconhecimento facial, por exemplo, esta métrica avalia a probabilidade de um culpado não ser reconhecido pelo modelo. Quanto mais verdadeiros negativos, menor será a Taxa de Falsos Positivos (TFP) (fórmula da Tabela 1). A pontuação da métrica para o nosso exemplo seria 30 (falsos positivos) / 80 (total de exemplos que são de fato negativos), ou seja, o modelo teve uma Taxa de Falsos Positivos (TFP) de 37%. Dessa forma, a probabilidade de um culpado não ser reconhecido pelo modelo é de 37%.
> (p. 23:9)

# Pós-processamento

> Por exemplo, se o algoritmo escolhido para o modelo de reconhecimento facial gera como saı́da uma porcentagem que representa a probabilidade da face detectada na foto ser de determinada pessoa, e o sistema espera uma saı́da binária (sim ou não) é pre- ciso escolher um limiar para converter tal probabilidade em uma classificação binária. Um limiar escolhido de 90%, por exemplo, indica que a correspondência da face de entrada com uma outra face seria de 90%. Nestes casos, para as correspondências a partir de 0, 9, o rosto procurado é identificado. Já para as correspondências cuja saı́da do modelo é um número menor do que 0, 9, o rosto buscado não corresponde à imagem analisada.
> (p. 23:9)

# Feedback e melhoria contínua e incremental

> O processo de desenvolvimento de um modelo de aprendizado de máquina geralmente é incremental, de forma que eles são retroalimentados com _feedbacks_. Os erros de predição do modelos servem como aprendizado para correção das falhas. Dessa forma, os modelos aprendem com os próprios erros, o que permite a melhora contı́nua do seu desempenho.
> (p. 23:9)

# Vieses e sua relação com a pipeline proposta

> Neste trabalho, consideramos quatro deles: (1) viés histórico, (2) viés nos da- dos, (3) viés no modelo e (4) viés de interpretação humana. **Consideramos dois destes vieses como _vieses computacionais_, gerados diretamente por escolhas na construção {10} e preparação dos dados e dos modelos (viés nos dados e viés no modelo) e os outros dois como vieses _não computacionais_, vieses relacionados a fatores sociais, culturais e éticos que vão além do uso dos dados, algoritmos e métricas (viés histórico e viés de interpretação humana).** Na prática, porém, não consideramos que tais vieses — computacionais e não computacionais — estão dissociados, pelo contrário, não consideramos ser possı́vel, na prática, mensurar qual deles tem maior importância ou peso para o problema como um todo. A seguir detalhamos cada um deles.
> (23:9-10)

# Viés histórico (assimetrias históricas)

> Os vieses históricos acontecem na etapa anterior à coleta de dados e podem se propagar por todo o _pipeline_ do aprendizado de máquina. Quando vieses sociais e culturais inerentes nos dados que refletem resultados passados são discriminatórios, os modelos acabam perpetuando vieses de forma não intencional [Tecs USP 2018]. Quando o mundo como é — ou foi — leva a um modelo com vieses que reforça julgamentos e preconceitos dos indivı́duos e instituições, como o racismo e os preconceitos de gênero, tais modelos reforçam estereótipos, que se refletem em casos de discriminação algorı́tmica.
> (p. 23:10)

> **Podemos compreender o viés histórico na prática quando nos deparamos com casos de racismo algorı́tmico.** Muitos são os casos reportados de discriminação algorı́tmica. A Linha do Tempo do Racismo Algorı́tmico3 , desenvolvida por Tarcı́zio Silva, apre- senta casos, reportagens e reações ao racismo algorı́tmico. 
> (p. 23:11)

> - __Exemplo 1.__ Sistemas de contratação que utilizam modelos de aprendizado de máquina para **selecionar candidatos para uma indústria predominantemente masculina e branca tendem a priorizar candidatos com estas caracterı́sticas, sem considerar outros aspectos como diversidade e inclusão.** Tais sistemas frequentemente recomendam desproporcionalmente a contratação de mais homens brancos para tais cargos, uma vez que se enquadram mais com a cultura já existente nestas empresas [D’Ignazio e Klein 2020]. Sem dados diversos para treinar estes modelos, tais ferramentas de contratação carregam os mesmos preconceitos que existiam na contratação de profissionais de tecnologia desde os anos 80.
> - __Exemplo 2.__ Em um outro exemplo, no contexto de visão computacional, que ficou conhecido em 2016, um jovem mostrou a diferença ao fazer buscas no Google Imagens por “three black teenagers” (três adolescentes negros) e por “three white teenagers” (três adolescentes brancos). **Para a busca por adolescentes negros, a plataforma exibia majoritariamente imagens usadas nas fichas policiais de jovens afro-americanos, enquanto para a busca por adolescentes brancos, as imagens predominantes exibiam jovens sorridentes, aproveitando seu tempo livre.** Reconhecer o viés histórico requer uma compreensão retrospectiva de como a opressão se manifestou em um determinado contexto ao longo do tempo [Suresh e Guttag 2019], como o racismo. Silvio de Almeida [Almeida 2019] define racismo como “uma forma sistemática de discriminação que tem a raça como fundamento, e que se manifesta por meio de práticas conscientes ou inconscientes que culminam em desvantagens ou privilégios para os indivı́duos, a depender do grupo racial ao qual pertençam”. Os vieses históricos são, portanto, assim como o racismo estrutural, sistêmicos por natureza.
>
> (p. 23:11-12)

# Viés nos dados (vieses de representação, amostra ou seleção)

## Quando os dados coletados não são representativos da população a ser modelada.

> __Exemplo.__ Joy Buolamwini, pesquisadora do MIT, realizou uma das primeiras pesquisas que tratam de vieses em sistemas de reconhecimento facial. Boulamwini e Gebru [Buolamwini e Gebru 2018] **analisaram o desempenho de modelos de classificação de gênero (feminino / masculino) por sistemas reconhecimento facial de alguns sistemas comerciais** (Microsoft, da IBM e do Face++). **A pesquisa concluiu que, no geral, homens e pessoas brancas foram melhor classificados pelos modelos do que os outros grupos. Uma visão interseccional da pesquisa revelou que todos os classificadores avaliados tiveram um pior desempenho ao classificar especificamente mulheres negras, explicitando a algoritmização de desigualdades socialmente estruturadas de raça e de gênero** [Carrera 2020].  Análises interseccionais nos permitem compreender melhor as desigualdades, visto que as formas de preconceito — como o racismo e o sexismo — muitas vezes se sobrepõem, de forma que um mesmo indivı́duo pode ser discriminado de várias formas [Collins e Bilge 2021; Akotirene 2018; Gonzalez 2018; Noble 2022; Carneiro 2003]. Pioneira na definição do conceito de interseccionalidade, Crenshaw [Crenshaw 2002] demarca este fenômeno como a captura das consequências estruturais resultantes do atravessamento e/ou das dinâmicas de interação entre dois ou mais eixos de opressão. A ótica interseccional permite uma compreensão aprofundada com relação ao modo como desigualdades como o patriarcalismo, o racismo e a opressão de classe estruturam as representações e posições sociais de mulheres, classes, raças e etnias, por exemplo [Crenshaw 2002]. **Tais resultados são um exemplo de viés nos dados potencialmente presentes em modelos de reconhecimento facial, que se baseiam em dados de treinamento desbalanceados, treinados majoritariamente em rostos de homens e pessoas brancas.**
> (p. 23:12)

## Quando os dados coletados não refletem a população alvo do sistema

> - __Exemplo 1.__ Quando a amostra coletada de um modelo contém, majoritariamente, dados representativos da cidade do Rio de Janeiro, e o modelo treinado será utilizado para a população da cidade de São Paulo, ele também irá apresentar um viés nos dados. De forma semelhante, dados coletados há 30 anos — independente da região — provavelmente não irão refletir a população que será alvo do modelo atualmente.
> - __Exemplo 2.__ No diagnóstico de diabetes, por exemplo, a dosagem de hemoglobina A1C é um dos instrumentos mais importantes para controlar a doença. Porém, estudos mostram que os nı́veis de A1C variam de formas complexas entre diferentes etnicidades e gênero [Herman e Cohen 2012]. Portanto, a coleta dos dados usados para o treinamento do modelo que não considera as particularidades de cada uma das subpopulações analisadas, irá gerar um viés nos dados que pode prejudicar qualquer um dos diferentes grupos populacionais.
>
> (p. 23:13)

# Viés no modelo (viés algorítmico, de aprendizado ou de avaliação)

> É importante destacar que, neste trabalho, nós abordamos os vieses presentes no processo de aprendizado de máquina em geral. O modelo, em si, é gerado durante o processo e pode ser utilizado em outros sistemas. Portanto, o viés no modelo é um dos vieses que abordamos aqui. Os vieses no modelo podem se manifestar de diferentes maneiras. 
> (p. 23:13)

## Vieses relacionados ao funcionamento interno do algoritmo

> __Exemplo__ Alguns sistemas da área financeira utilizam modelos para automatizar a decisão de concessão de crédito a clientes. Tais sistemas se baseiam em pontuações de crédito — processo de atribuição de pontos às variáveis de decisão de crédito de um indivı́duo ao longo da sua vida como consumidor. Vilarino e Vicente [Vilarino e Vicente 2020] **detalharam um modelo experimental de pontuação de crédito desenvolvido com dados reais no Brasil e demonstraram como o uso de informações de localização introduz preconceito racial.** Os pesquisadores fizeram algumas simulações e observaram que, ao “mover” potenciais consumidores de regiões com uma maior população branca, como o estado de São Paulo, para regiões com uma maior população não branca, como o estado da Bahia, em 99,8% dos casos, as suas pontuações de crédito reduziram, embora todos os outros atributos, como idade e histórico financeiro não tivessem sido alterados. **O modelo, dessa forma, assumiu que a correlação (localização geográfica e probabilidade de inadimplência) implica causalidade (a localização geográfica teve um papel deter- minante no risco de inadimplência).**]
> (p. 23:14)

## Vieses relacionados à avaliação do modelo criado

> Por exemplo, um modelo de reconhecimento facial pode ter uma precisão geral de 80%, mas se formos considerar a precisão dentro do grupo que inclui mulheres ne- gras, a precisão cai para 60%, enquanto que a precisão dentro do grupo que corresponde a homens brancos, a precisão sobe para 90%. Na Seção 4, descrevemos algumas das métricas alternativas para avaliar os modelos de forma a mitigar os vieses inseridos du- rante esta etapa.
> (p. 23:14)

# Viés de interpretação humana

> **Os vieses de interpretação humana podem ser inseridos na etapa de pós-processamento, durante a integração dos sistemas (Figura 3). A saı́da do modelo, por exemplo, a identificação de um suspeito, nos sistemas de reconhecimento facial, deve ser sempre interpretada por seres humanos, de forma a evitar consequências injustas.** Estes vieses ocorrem quando há uma incompatibilidade entre o problema que o modelo se propôs a resolver e a forma em que ele é usado na prática [Suresh e Guttag 2019]. Nos sistemas de reconhecimento facial, o uso indiscriminado de modelos que já tiveram a sua eficácia questionada em determinados grupos, como de pessoas negras, sem oportunidade de uma avaliação humana para validar e/ou questionar os dados pode perpetuar problemas e desigualdades sociais e culturais. Muitas vezes, quando acontece o reconhecimento facial de um suspeito por um sistema, as autoridades que buscam a punição criminal de alguém já consideram a saı́da do modelo como prova da prática do crime, sem realizar uma análise mais aprofundada ou dar continuidade às investigações.
> (p. 23:14-5)

> A opacidade destes modelos, ou seja, a dificuldade — ou impossibilidade — de se compreender claramente como estes modelos geraram as previsões, juntamente com a crença em sua objetividade, tornam o processo de interpretação humana ainda mais passı́vel de ser enviesado. Neste contexto, há uma área de pesquisa dentro da computação, conhecida como _interpretabilidade_, que vem crescendo bastante nos últimos, e buscando estratégias para que seja possı́vel “abrir” as caixas opacas (modelos) e assim interpretá-los [Arrieta et al. 2020].
> (p. 23:15)
 
# "Mitigar" vieses

> Os vieses inseridos no processo de aprendizado de máquina, apresentados na Seção 3, representam um problema complexo e que exige esforços multidisciplinares. Tais vieses são incorporados no processo de aprendizado de forma consciente ou inconsciente, portanto, a sua eliminação completa se torna uma tarefa muito difı́cil. **Utilizamos, portanto, neste trabalho o termo “mitigar”, entendendo que para este problema não existe uma solução única, que funciona sempre e para todos os casos.**
> (p. 23:16)

> Para os _vieses computacionais_ apresentados na Seção 3 (vieses nos dados e nos modelos), apresentamos _soluções computacionais_, que envolvem ajustes nos dados (Seção 4.1) e nos modelos (Seção 4.2). Para os _vieses não computacionais_ (vieses históricos e de interpretação humana), descrevemos algumas _soluções não computacionais_ (Seção 4.3), que vão além de soluções envolvendo o uso de dados, novos algoritmos e métricas. Vale relembrar neste ponto que, na prática, não consideramos que os vieses computacionais e não computacionais estão dissociados, pelo contrário, consideramos ser impossı́vel, na prática, mensurar qual deles tem maior importância ou peso para o problema. Os esforços para mitigar os vieses devem cobrir ambas as esferas, que se complementam.
> (p. 23:16)

# Fairness

> Neste ponto, surgem muitos questionamentos a respeito dos critérios para avaliar o quanto um sistema pode ser “justo”. **O termo _fairness_, em Inglês, traduzido muitas vezes como “equidade” (ou “justiça”), tem ganhado espaço na comunidade acadêmica e no mundo corporativo.** Porém, trabalhar com a ideia de equidade não é simples, pois a própria ideia de equidade (ou de justiça) não é restrita a área de aprendizado de máquina, sendo parte também de múltiplas e diversas reflexões e teorias das ciências humanas [Cortiz 2020].
> (p. 23:16)

# Mitigando vieses nos dados

## Desbalanceamento na base de dados

> Os vieses nos dados podem ser gerados por várias razões. **Em uma grande parte dos casos, a razão é o próprio desbalanceamento nos dados — quando a amostra coletada não é representativa da população, de forma balanceada.** Para lidar com este problema, algumas medidas podem ser tomadas. **Uma dessas medidas é a construção de dados de treinamento representativos.**
> (p. 23:17)

## Uso de benchmarks e datasets representativos

> Para modelos utilizados em sistemas de reconhecimento facial, alguns conjuntos de dados inclusivos vêm sendo construı́dos para gerar modelos mais justos. **Estes dados podem ser utilizados tanto como dados de treinamento para modelos aprenderem quanto como dados de referência (_benchmarks_).** Os benchmarks são conjuntos de dados utilizados como referência para comparar o desempenho de modelos com a mesma finalidade que vêm sendo propostos.
> (p. 23:17)

> Um exemplo de conjunto de dados balanceado, que pode ser usado para treinar modelos de visão computacional, como os utilizados no reconhecimento facial, é o _FairFace_ [Karkkainen e Joo 2021], contendo mais de cem mil imagens curadas para mitigar vieses raciais, coletadas do Flicker e englobando 7 grupos raciais: Brancos, Negros, Indi- anos, Asiáticos do leste, Asiáticos do sudeste, Oriente Médio e Latinos.
> (p. 23:17)

> Um outro conjunto de dados de referência representativo é o PPB (Pilot Parliaments Benchmark) (Figura 5), proposto por Joy Buolamwini e Timnit Gebru, no projeto _Gender Shades_, **criado para prover uma melhor representação interseccional de gênero e raça em sistemas de visão computacional.** Os dados contém 1270 imagens de rostos de pessoas, incluindo três paı́ses africanos e três paı́ses europeus, que pode ser usado tanto como dados de treinamento quanto como dados de teste e de referência para modelos que implementam reconhecimento facial. As pesquisadoras, sozinhas, conseguiram gerar dados mais precisos do que os oferecidos por algumas gigantes de tecnologia. **O impacto das pesquisas iniciadas pelas pesquisadoras foi tão grande que, em meados de 2020, a IBM encerrou as suas pesquisas em reconhecimento facial, e se posiciona, hoje, contra o uso da tecnologia para monitoramento em massa e vigilância.**
> (p. 23:17)

## Descontinuação de bases de dados desbalanceadas

> **Além de se construir conjuntos de dados balanceados, estamos vendo algumas iniciativas para descontinuar o uso de alguns não balanceados.** Um das maiores bibliotecas de aprendizado de máquina em Python, a scikit-learn, descontinuou, por questões éticas, um conjunto de dados descrevendo moradias em Boston. **Este conjunto de dados vem sendo bastante utilizado para demonstrações práticas em estudos e cursos de aprendizado de máquina e é usado como demonstração para treinar um modelo que prevê preços de imóveis em Boston, baseado em algumas caracterı́sticas nas regiões. Um dos atributos utilizados para treinar o modelo representa, porém, a proporção de negros em cada uma das regiões e o modelo assume que a segregação racial tem um impacto positivo no valor dos imóveis, perpetuando o racismo sistêmico neste contexto.**
> (p. 23:18-9)

# Mitigando vieses no modelo

## Correlação-causalidade

> **Os vieses relacionados ao funcionamento interno do algoritmo podem ser gerados, por exemplo, quando alguns modelos assumem que correlação implica causalidade e utilizam caracterı́sticas de forma indevida nos algoritmos para treinar o modelo.**  Muitas abordagens para mitigar este tipo de viés consideram que alguns dos atributos, chamados de **atributos protegidos — ou atributos sensı́veis, não deveriam ser utilizados no treinamento para o aprendizado,** pois definem, muitas vezes, aspectos dos dados que são potencialmente perigosos para as previsões, sob o ponto de vista sociocultural [Caton e Haas 2020].
> (p. 23:18)

## Atributos protegidos e proxies (correlacionados)

> Como exemplos de atributos protegidos, podemos citar: raça, gênero, etnia/nacionalidade, religião, idade, deficiência, entre outros [Bellamy et al. 2018]. Porém, a noção de atributo protegido vai além dessas dimensões, podendo abranger qualquer outra caracterı́stica que envolva indivı́duos. Neste ponto, surge uma dúvida muito comum na produção de modelos de aprendizado de máquina justos: **Quais atributos devem ser protegidos? Alguns atributos são compreendidos universalmente como protegidos (ou sensı́veis), como raça, gênero e etnia. Outros são explicitamente definidos em regulamentações especı́ficas que dispõem sobre o tratamento de dados pessoais, com o objetivo de proteger os direitos de privacidade dos indivı́duos. Ainda há casos de atributos que não são rigorosamente protegidos, mas que tem uma relação com atributos protegidos** [Vilarino e Vicente 2020]. A Tabela 2 mostra alguns exemplos de atributos protegidos e equivalentes que são considerados correlacionados.
> (p. 23:19)

![Tab. 2](tab2.png)

## Novas métricas

> Além de proteger alguns atributos do “aprendizado”, como descrito até aqui, os vieses no modelo podem ser mitigados também através da utilização de novas métricas de avaliação que levem em consideração não apenas requisitos técnicos, mas também aspec- tos sociais [Cortiz 2020], ao invés de simplesmente utilizar as métricas tradicionais para avaliar os modelos, como a acurácia, precisão e a revocação, apresentadas na Seção 2.3.
> (p. 23:20)

> Estas métricas alternativas utilizam outros conceitos e definições, que apresentaremos a seguir. **Quando a predição favorece um determinado indivı́duo/grupo, representando uma vantagem em relação aos demais, podemos dizer que este indivı́duo/grupo recebeu um _rótulo favorável_** [Bellamy et al. 2018], como por exemplo, ter um crédito aprovado, ser contratado para um emprego e não ser preso injustamente (para os casos de falha nos sistemas de reconhecimento facial). **Quando estes vieses indesejados fornecem uma vantagem sistemática para um grupo, este grupo é chamado de _grupo privilegiado_, e quando fornecem uma desvantagem sistemática, o grupo é chamado de _grupo não privilegiado_.**
> (p. 23:20)

> Nesta seção, descrevemos quatro métricas de justiça definidas por Bellamy et al. e utilizadas pela ferramenta _AI Fairness Toolkit_, desenvolvida pela IBM, para investigar e mitigar discriminação e vieses inseridos por modelos de aprendizado de máquina [Bellamy et al. 2018]. A ferramenta, de código aberto, oferece casos de demonstração utilizando alguns dados e vários algoritmos para mitigação dos vieses.
> (p. 23:20)

> Para descrever estas métricas, retomamos o cenário do reconhecimento facial, que utiliza datasets com imagens de rostos para o treinamento. Consideramos também um recorte interseccional raça-gênero em 4 grupos: (1) mulheres brancas, (2) mulheres não brancas, (3) homens brancos e (4) homens não brancos. Consideramos aqui somente os atributos raça e gênero, por simplificação, mas é importante lembrar que os vieses podem prejudicar indivı́duos pertencentes a outros grupos não contemplados aqui. **Neste exem- plo, consideramos o grupo com imagens compostas por homens brancos como grupo privilegiado e o grupo composto por imagens de mulheres negras como grupo não privilegiado.** A Tabela 3 apresenta algumas das métricas que podem mitigar os vieses descritos neste trabalho, listadas a seguir.
> (p. 23:20)

![Tab. 3](tab3.png)

### Diferença de paridade estatística (_statistical parity difference_)

> **A diferença de paridade estatı́stica considera a probabilidade de rótulos favoráveis entre grupos não privilegiados e privilegiados.** Uma pontuação de 0 indica que não há diferença, já um valor menor do 0 que o indica que o grupo privilegiado tem uma maior vantagem e um valor maior do que 0 indica que o grupo não privilegiado tem uma vanta- gem.
> (p. 23:21)

> No reconhecimento facial, por exemplo, considerando um modelo com uma pro- babilidade de 85% de um homem branco ser corretamente reconhecido (e, portanto, não correr o risco de ser preso injustamente) e de 68% no grupo de mulheres negras, o impacto desproporcional considerando estes dois grupos seria de 0, 68 − 0, 85, portanto, −0, 17.  Neste caso, de acordo com esta métrica, tal modelo não seria justo e estaria fornecendo uma vantagem para o grupo privilegiado.
> (p. 23:21)

### Impacto desproporcional (_disparate impact_)

> **O impacto desproporcional considera a razão entre a probabilidade de rótulos fa- voráveis entre grupos privilegiados e não privilegiados.** Uma pontuação de 1 indica que ambos os grupos têm a mesma probabilidade de rótulos favoráveis, isto é, os grupos têm a mesma vantagem. Uma pontuação menor do que 1 indica uma maior vantagem para o grupo privilegiado e um valor maior do que 1 indica uma maior vantagem para o grupo não privilegiado.
> (p. 23:22)

> No reconhecimento facial, por exemplo, considerando um outro modelo, com uma probabilidade de 91% de um homem branco ser corretamente reconhecido (e portanto, não correr o risco de ser preso injustamente) e de 52% no grupo de mulheres negras, o impacto desproporcional considerando estes dois grupos seria de 0, 91/0, 52, portanto, 57% (ou 0, 57). Neste caso, tal modelo não seria justo, de acordo com a métrica impacto desproporcional.
> (p. 23:22)

### Diferença de probabilidade média (_average odds difference_)

> **A diferença de probabilidade média considera a diferença entre as taxas de falsos positivos (TFP) (Tabela 1) entre grupos não privilegiados e privilegiados.** Uma pontuação de 0 indica que não há diferença, já um valor menor do 0 que o indica que o grupo privilegiado tem uma maior vantagem e um valor maior do que 0 indica que o grupo não privilegiado tem uma vantagem.
> (p. 23:22)

> No reconhecimento facial, por exemplo, considerando um modelo com uma taxa de falsos positivos (TFP) de 60% para o grupo composto por homens brancos e uma taxa de falsos positivos (TFP) de 90% para o grupo composto por mulheres negras, a diferença de probabilidade média considerando estes dois grupos seria de 0, 6−0, 9, portanto, −0,3. Neste caso, tal modelo não seria justo e estaria fornecendo uma vantagem para o grupo privilegiado.
> (p. 23:22-3)

### Diferença de igualdade de oportunidade (_equal opportunity difference_)

> **A diferença de igualdade de oportunidade considera a diferença entre as taxas de verdadeiros positivos (TVP) entre grupos não privilegiados e privilegiados.** Uma pontuação de 0 indica que não há diferença, já um valor menor do 0 que o indica que o grupo privilegiado tem uma maior vantagem e um valor maior do que 0 indica que o grupo não privilegiado tem uma vantagem.
> (p. 23:23)

> No reconhecimento facial, por exemplo, considerando um modelo com uma taxa de verdadeiros positivos (TVP) de 80% para o grupo composto por homens brancos e uma taxa de verdadeiros positivos (TVP) de 70% para o grupo composto por mulheres negras, a diferença de igualdade de oportunidade considerando estes dois grupos seria de 0, 7 − 0, 8, portanto, −0, 1. Neste caso, tal modelo não seria justo e estaria fornecendo uma vantagem para o grupo privilegiado.
> (p. 23:23)

# Mitigando vieses não computacionais (histórico, interpretação)

> Os vieses históricos e de interpretação humana (Seções 3.1 e 3.4) não são vieses inseridos diretamente por um escolha na construção dos dados de treinamento ou do algoritmo utilizado para treinar o modelo, ou seja, não demandam soluções computacionais. Esses vieses são um reflexo das nossas estruturas sociais e culturais enviesadas, e demandam, antes de tudo, o reconhecimento e conscientização do problema por parte das pessoas analistas e programadoras envolvidas no processo de criação de tais modelos.
> (p. 23:23)

## Stuart Hall sobre estereotipagem

> As possibilidades de mitigação destes vieses também precisam envolver um entendimento acerca de algumas dinâmicas socioculturais e que precisam ser evitadas o quanto possı́vel, como a estereotipagem. **Hall [Hall 2016] atesta que o processo de produção de significados, especificamente com relação à representação da diferença racial, por vezes é perpassado por práticas representacionais chamadas de estereotipagem. _Apropriando-se_ de poucas caracterı́sticas de uma pessoa, o estereótipo reduz esta pessoa a estes traços que, posteriormente, são _exagerados_ e _simplificados_** [Hall 2016, grifos do autor]. Assim, na visão do autor, **a estereotipagem atua no processo de redução, essencialização, naturalização e fixação da diferença.** Além disso, a prática da estereotipagem também implementa uma estratégia de cisão, que tipifica e separa as diferenças em duas categorias: o normal e aceitável e o anormal e inaceitável. Como consequência, Hall [Hall 2016] afirma que **a estereotipagem, exclui tudo o que é diferente, tudo o que não está delimitado dentro dos limites de normalidade estabelecidos.**
> (p. 23:24)

## Fanon sobre construtivismo informado pelo racismo e colonialismo

> **Fanon [Fanon 2008] ainda ressalta que as interações sociais que circundam o mundo são perpassadas por modos de criação e de vivência socialmente construı́dos por percepções advindas do racismo e do colonialismo.** É sob estas duas bases que os negros são _construı́dos como negros_ na sociedade [Fanon 2008]. Estes mecanismos sociais de racialização das pessoas negras muitas vezes suscitam comportamentos humanos discriminatórios, com base em reproduções estereotipadas, simplificando matematicamente os indivı́duos em _scores_, reproduzindo assimetrias, perpetuando invisibilidades e amplificando as exclusões.
> (p. 23:24)

## Ações afirmativas

> Neste contexto, é importante destacar o impacto da adoção de polı́ticas de inclusão e/ou cotas no paı́s no combate à desigualdade e ao racismo estrutural. **Ao implementar tais estratégias, as bases produzidas nestes novos contextos naturalmente já incluem alguns dos grupos historicamente marginalizados, o que pode reduzir significativamente os vieses históricos nestas bases.** Replicar estratégias como estas é essencial para a adoção de tecnologias digitais dissociadas do contexto de relações de poder presentes no cenário global com base em fatores raciais, por exemplo, como afirma Noble [Noble 2022]. Mas, vale ressaltar que esta estratégia também precisa ser atravessada pela observação de marcos que envolvam também a dimensão interseccional para reduzir desigualdades de classe, raça, gênero e das demais formas de opressão [Collins e Bilge 2021].
> (p. 23:24)

## Políticas de letramento

> **Por fim, os vieses históricos e de interpretação humana também precisam ser mitigados por meio da implementação de polı́ticas de letramento com relação a questões étnico-raciais direcionadas à sociedade brasileira em geral, para reduzir o distanciamento entre a população brasileira e as heranças culturais construı́das pelos povos originários do Brasil e pela população afro-diaspórica.** O alcance da história e das culturas afro-brasileira e indı́gena nos currı́culos pedagógicos regulamentados pelas Leis no. 10.639/200316 {estabelece as diretrizes e bases da educação nacional, para incluir no currı́culo oficial da Rede de Ensino a obrigatoriedade da temática ”História e Cultura Afro-Brasileira”, e dá outras providências.} e 11.645/200817 { estabelece as diretrizes e bases da educação nacional, para incluir no currı́culo oficial da rede de ensino a obrigatoriedade da temática “História e Cultura Afro-Brasileira e Indı́gena”.}, por exemplo, possuem um papel essencial no reconhecimento do valor cultural e da ressignificação dos saberes dos povos originários do Brasil e dos povos afro-diaspóricos como fontes de conhecimento legı́timas e fundamentais para a formação da identidade cultural brasileira.
> (p. 23:24-5)

## Legislação (GDPR e LGPD)

### Dados

> O Regulamento Geral de Proteção de Dados da União Europeia19 (General Data Protection Regulation, em Inglês) foi aprovado em meados de 2016 e entrou em vigor em meados de 2018 em toda a União Europeia. A lei representou um marco regulatório em relação à privacidade e proteção de dados pessoais e sensı́veis de cidadãos europeus.  Inspirada na lei europeia, a brasileira Lei Geral de Proteção de Dados Pessoais (LGPD)20 foi aprovada em meados de 2018 e sancionada em outubro de 2020. O artigo 5 da LGPD considera três grandes grupos de dados:
> 
> - __I - dado pessoal:__ informação relacionada a pessoa natural identificada ou identificável;
> - __II - dado pessoal sensı́vel:__ dado pessoal sobre origem racial ou étnica, convicção religiosa, opinião polı́tica, filiação a sindicato ou a organização de caráter religioso, filosófico ou polı́tico, dado referente à saúde ou à vida sexual, dado genético ou biométrico, quando vinculado a uma pessoa natural;
> - __III - dado anonimizado:__ dado relativo a titular que não possa ser identificado, considerando a utilização de meios técnicos razoáveis e disponı́veis na ocasião de seu tratamento.
> 
> (p. 23:25)

### Reconhecimento facial

> A LGPD estabelece usos especı́ficos para cada um destes grupos de dados, sendo estes mais restritivos para os dados sensı́veis (Artigo 11) do que para os dados pessoais (Artigo 7). Os dados biométricos processados por sistemas de reconhecimento facial podem ser reconhecidos, pela LGPD, como dados pessoais, já que eles nos permitem extrair informações que podem identificar uma pessoa. Mas também podem ser considerados dados sensı́veis, pela sua própria definição, que consta na lei [Carvalho et al. 2021].
> (p. 23:25-6)

> Na prática, porém, os sistemas de reconhecimento facial são muitas vezes utilizados sem restrições, gerando muitas das consequências já apresentadas neste artigo. **San Francisco, em 2019, foi a primeira cidade dos Estados Unidos a banir o uso de reconhecimento facial na segurança pública pelo governo, o que inspirou outras cidades pelo mundo.** No Rio de Janeiro, por exemplo, está em tramitação na Câmara Municipal do Rio de Janeiro o Projeto de Lei no. 824/202122 , que tem como objetivo proibir o uso de tecnologias de reconhecimento facial pelo poder executivo municipal.
> (p. 23:26)

## Diversidade e inclusão (na área de IA)

> **Em se tratando da crise de diversidade de gênero na esfera acadêmica, o relatório _Gender Diversity in AI Research_, produzido pela fundação de inovação britânica Nesta, aponta que as mulheres seguem sub-representadas em todas as subáreas dentro da Ciência da Computação.** O relatório analisou publicações em Inteligência Artificial no arXiv, repositório amplamente utilizado pela comunidade acadêmica. A pesquisa aponta que as mulheres representam 13,8% dos autores de artigos na área. Em se tratando de artigos de autoria única, representam somente 6,72% dos artigos.
> (p. 23:27)

> No mundo corporativo, o cenário é também desafiador. **Segundo o _Relatório Global de Equidade de Gênero_, do Fórum Econômico Mundial de 2021, as mulheres repre- sentam, globalmente, 32% da força de tarefa nas áreas de Inteligência Artificial e Ciência de Dados.**
> (p. 23:27)

> **Já quando se trata de diversidade racial na tecnologia, o cenário é tão invisibilizado que faltam inclusive dados precisos para mensurar o problema.** Segundo Silvana Bahia, diretora de projetos do Olabi, organização social que trabalha para democratizar a produção de tecnologia, a ausência de mulheres negras e indı́genas na tecnologia está ligada diretamente a dois fatores: acesso e falta de referências. Segundo a diretora, **os custos relacionados à formação na área são muito caros, muitas vezes inacessı́veis, em Inglês, e são raras as polı́ticas (públicas ou privadas) destinadas ao ingresso e permanência de mulheres negras nestes espaços.** Além disso, **a ausência de referências positivas sobre mulheres negras na área é uma questão social que vai além do mundo da tecnologia e atinge os mais variados campos profissionais e de poder.**
> (p. 23:27)
