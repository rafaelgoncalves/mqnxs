---
title: Para distinguir amigos e inimigos no Antropoceno
tags: [Bruno Latour, Antropoceno, negacionismo, Gaia]
date: 2021-06-20
category: fichamento
path: "/para-distinguir-amigos-Latour"
featuredImage: "../../images/gravity.jpg"
srcInfo: Cena do filme Gravidade de Alfonso Cuarón
published: true
---

Fichamento do texto _Para distinguir amigos e inimigos no tempo do Antropoceno_[^1] de Bruno Latour.

> Quando Ryan, a única sobrevivente
> da aventura no espaço, chega nadando à margem do lago no qual sua
> nave finalmente aterrissou, e agarra com força um punhado de terra, ela
> sofre literalmente uma metamorfose. De humana ela se torna terrana
> [Earthbound ], enquanto o herói americano antiquado – seu colega de
> equipe, interpretado de maneira cômica por George Clooney – desa-
> parece para sempre no espaço sideral, juntando-se aos destroços das
> estações espaciais da Europa e da China.
> (p. 12)

[^1]: LATOUR, B. Para distinguir amigos e inimigos no tempo do Antropoceno. **Revista de Antropologia**, v. 57, n. 1, p. 11-31, 2014. Disponível em: https://www.revistas.usp.br/ra/article/view/87702. Acesso em: 20 jun. 2021.

## O antropoceno como conceito passível de enfrentar os mitos modernos

> Apesar de suas armadilhas, o conceito de Antropoceno oferece um
> modo poderoso, se usado de maneira sensata, de evitar o perigo da
> naturalização à medida que permite reconfigurar o antigo domínio do
> social – ou “humano” – em domínio dos Terráqueos ou dos Terranos.
> (p. 12)

> Mas esse conceito pode também chamar a nossa atenção
> para o fim do que Alfred N. Whitehead (1920) chamou de “bifurcação
> da natureza”, ou seja, a recusa decisiva da separação entre Natureza e
> Humanidade, que tem paralisado a ciência e a política desde a aurora
> do modernismo.
> (p. 13)

> Esta Constituição [moderna] está completamente
> mal-adaptada para lidar com os conflitos que temos para nos orientar. E ela está tão mal-adaptada que mesmo a noção de conflito, ou
> ainda, para dar nome aos bois, o estado de guerra – traço definidor do
> Antropoceno – é constantemente minimizado ou tratado de maneira
> eufemística. Em tal época, tanto a política como a ciência assumem
> uma configuração totalmente diferente.
> (p. 14)

## Antropoceno como conceito instável

> O júri ainda não se decidiu sobre a persistência do conceito de
> Antropoceno (sua meia-vida pode ser muito mais curta do que imagino). Neste exato momento, entretanto, é a melhor alternativa que
> temos para sair da noção de modernização.
> (p. 13)

## Ciência e política no Antropoceno

> E este é um dos problemas que paralisam a política no Antropoceno. Não se trata de um debate racional. Trata-se, isso sim, de um de-
> bate para o qual os climatólogos do ipcc, que teriam sido considerados
> racionais em outro clima, estão sendo destituídos de poder. Eles são
> retratados como irracionais por aqueles que usam o poder da razão e
> apelam para a liberdade de investigação científica para poluir não apenas a atmosfera, mas também a esfera pública (para usar a expressão
> de James Hoggan). E isso, por quê? Porque ambos os lados – e eis o
> que produz a ideia de que há dois lados – usam o mesmo repertório
> ciência versus política.
> (p. 15)

> Parte um: a ciência trata de fatos incontroversos e incontestáveis. Parte dois: a ciência fornece subsídios para
> as políticas. Uma vez que é nisso que todos os políticos – e todos os
> espectadores – acreditam, e uma vez que é essa também a maneira pela
> qual os programas de tv organizam os debates como se fossem juízes
> em uma sala de tribunal, torna-se incrivelmente fácil fazer emergir dois
> lados mesmo quando existe apenas um.
> (p. 15-16)

> Historiadores da ecologia estão certos em dizer que não há provavelmente nada de completamente novo no conceito de Antropoceno, já que
> os conflitos por territórios e seus recursos são tão antigos quanto a raça
> humana e que os alertas quanto às consequências dessas “apropriações de
> terras” sobre o ambiente são tão antigos quanto a Revolução Industrial.
> O que me parece realmente novo nesse rótulo, o Antropoceno (afora a
> colaboração incomum entre geologia, história – ou, ainda, geo-história –, política e filosofia), é que ele modifica simultaneamente os quadros
> espaciais e temporais nos quais a ação está sendo situada; é que, além
> disso, este quadro modificou os dois principais pilares sobre os quais a
> metafísica da Ciência foi estabelecida desde a “Bifurcação da Natureza”,
> para usar a famosa descrição de Whitehead.
> (p. 27)

## Negacionismo como prática imobilizatória

> Mas não há
> a menor chance de chegarmos a uma conclusão final, uma vez que o
> sucesso dos negacionistas não reside em vencer algum conflito, mas simplesmente em assegurar que o resto do público esteja convencido de que
> há um conflito. Como poderiam os pobres e desamparados climatólogos
> levar a melhor em um pseudo-tribunal, cujo objetivo não é chegar a um
> veredito (uma vez que o veredito já tinha sido dado pelo relatório do
> ipcc)? A nova disciplina a que chamamos “agnotologia”, para usar uma
> expressão de James Proctor, é a produção deliberada de ignorância que
> funcionou maravilhosamente no caso do cigarro e do amianto e, com
> mais resistência, dos campos de concentração durante a Segunda Guerra.
> Ela vai funcionar muito melhor, e por muito mais tempo, no caso da
> ciência do clima, e isso por uma outra razão: diz respeito ao cotidiano
> de bilhões de pessoas. A chance de encerrar o debate é zero. Ademais,
> esperar por uma conclusão antes de traçar uma política tampouco se
> torna uma opção.
> (p. 16-17)

## Os dois lados: ciência versus política e ciência com política

> Há dois
> lados, é verdade, mas não entre climatólogos e negacionistas climáticos.
> Há dois lados: aqueles que atualizam uma versão tradicional da ciência
> versus política e aqueles que compreenderam que essa antiga _epistemologia política_ (para chamá-la pelo seu verdadeiro nome) é o que enfraquece tanto a ciência como a política no momento em que as questões
> em jogo tornam-se amplas demais para um número grande demais de
> pessoas envolvidas e diretamente impactadas pelas decisões de ambas.
> É aí que devemos realmente distinguir um acordo do Holoceno e um
> acordo do Antropoceno. O que pode ter sido bom para os Humanos
> (e duvido que isto já teria sido o caso) perdeu todo o sentido para os
> Terranos [Earthbound ].
> (p. 17)

> A única coisa que elas _não podem se
> permitir é atuarem separadamente_ : sem os instrumentos da ciência, o corpo político jamais saberá quantas entidades desconhecidas é preciso levar
> em consideração. E sem a política, o mesmo corpo político jamais saberá
> ordenar, selecionar e ranquear aquele número desconcertante de agências
> com as quais ele tem de compor progressivamente um mundo comum –
> que é a definição que propus para a política com ciência.
> (p. 18)

> Não há conflito entre ciência
> e política, mas há conflito entre duas epistemologias políticas radicalmente
> opostas, cada uma com sua própria definição do que vêm a ser ciência
> e política, e de como ambas poderiam colaborar uma com a outra.
> (p. 19)

## Sobre um essencialismo ou um positivismo "estratégicos"

> Existem, é claro, várias razões para imitar o que as feministas chamam
> de “essencialismo estratégico” e para empregar, quando necessário, uma
> forma de “positivismo estratégico”, como se pudéssemos confiar a uma
> ciência do clima estabelecida a missão de servir como premissa incontestável para políticas. Mas mesmo se essa estratégia fosse bem-sucedida
> (e a reposta ao último relatório do ipcc indica que ela falhou do mesmo
> modo que todas as tentativas prévias de “convencer” o público), ela não
> resolveria a questão, pois permaneceria como um ganho pedagógico,
> e não político. Decerto, mais gente saberia, o que é sempre bom, mas isso
> não levaria as pessoas muito além do fato de apenas saber. Não estamos
> lidando aqui com matters of fact inquestionáveis, mas com matters of
> concern postos em disputa.
> (p. 19-20)

## Senso comum como ato de urgência?

Em situações em que a não-ação não é uma opção, ou em situações em geral (considerando-se que em pouquíssimos casos existem consensos absolutos), o melhor que podemos fazer é nos guiarmos pelo "senso comum"? Senso comum como tradição e cultura? Não seria mais coerente que ao invés disso (buscar um comum, uma média das opiniões), se escolhesse de fato um conjunto de valores (políticos, se quiser, uma ponderação específica e consciente) como mediadores de nossas tomadas de decisão?

Senso comum não seria o nome dado aos valores reproduzidos pelas parcelas detentoras de poder na sociedade sempre, ou quase sempre, visando a reprodução do atual estado de coisas? Penso aqui em ideologia (no sentido de Marx).

> Tomar decisões diante de evidências contraditórias sobre questões urgentes é uma atitude comum a cientistas, políticos e membros comuns do
> público. Tal atitude baseada no senso comum ganha plena força quando
> o território dessas pessoas se encontra ameaçado. O sentimento a que
> poderíamos chamar de _mobilização_ é perigoso, incômodo e intranquilo, uma fonte de consequências mal definidas; mas uma coisa é certa:
> em caso de guerra, a atitude não é de complacência, apaziguamento e
> delegação aos _experts_ .
> (p. 22)

## Guerra entre a ciência engajada "do Antropoceno" e a ciência moderna "do Holoceno"

> Para eles [, adversários dos militantes e dos "cientistas engajados"], trata-se de uma violenta _apropriação de terras_ : a terra é
> deles e eles tomam conta dela rapidamente.
> (p. 22)

> Embora possa ser perigoso falar em guerra – quando há um estado de paz – é ainda mais perigoso
> negar que há uma guerra quando se está sob ataque. Os apaziguadores
> acabariam se tornando negacionistas – desta vez não por negarem a
> ciência do clima –, mas por negarem que há uma guerra pela definição
> e controle do mundo que habitamos coletivamente.
> (p. 22-23)

> Há decerto uma guerra pela definição e controle da Terra: uma
> guerra que coloca uns contra os outros – para ser um pouco dramático –, Humanos que vivem no Holoceno e os Terranos que vivem no
> Antropoceno.
> (p. 23)

> O que é um território senão isso sem o qual não poderíamos viver?
> Listemos todos esses seres, essas agências sem as quais nada seria possível.
> Identificaremos então os territórios que estão sob ataque, aqueles que
> vale a pena defender e aqueles que podem ser abandonados. Feito isso,
> poderemos comparar nossas chances de perder ou de ganhar.
> (p. 24-25)

## Multiplicidade de agências possíveis que dependem da epistemologia subjacente

> Em ambos os relatos, ["carbono inocente" e "democracia do carbono",] o carbono não exerce o mesmo papel,
> não recebe as mesmas qualificações, não tem as mesmas propriedades.
> Tudo bem. Isso não prova qualquer distorção dos fatos científicos.
> Isso significa que há várias maneiras pelas quais o carbono pode entrar
> na composição de um mundo comum. Se os mesmos átomos podem
> gerar materiais tão diferentes quanto o grafite e o diamante, deveríamos nos surpreender com o fato de que o mesmo carbono nas mãos
> de um negacionista do clima tenha diferentes arranjos e virtudes, isto
> é, diferentes agências se comparado com os átomos de carbono de um
> historiador do Oriente Médio, como Mitchell? “Inocência” e “culpa”
> são propriedades de átomos que – de maneira bastante precisa e literal
> – dependem de sua composição.
> (p. 24)

## A paz política como desfecho

> Não uma paz pedagógica obtida por meio
> do repertório ciência-versus-política: como se pudéssemos começar a
> discutir políticas agora que aprendemos todos com as ciências naturais,
> agora que passamos a concordar necessariamente uns com os outros
> acerca do que é feito o mundo. Em vez disso, uma paz política, aquela
> negociada pelas facções bélicas que, tendo exaurido todas as outras
> opções e sabendo que nem o “Deus” nem a “Natureza” bordados em
> seus estandartes estão realmente por detrás deles, aventuram-se em um
> acordo como se não houvesse arbítrio algum acima deles.
> (p. 25)

> A principal dife-
> rença entre as duas formas de paz é que a pedagógica vem antes de toda
> guerra – nesse sentido, a guerra é simplesmente o equívoco irracional
> daqueles que não entenderam as leis da natureza ou da economia: a paz
> será trazida de volta já que todos terão descoberto a verdade a respeito
> do que as coisas são e sempre foram. A paz pedagógica é semelhante à
> intervenção policial ou ao que hoje é chamado de “governança”. Por
> contraste, a paz política vem depois que a guerra exauriu as partes que
> guerreiam e acaba por compor o que é exatamente nomeado, um modus vivendi, isto é, um conjunto emaranhado de arranjos improvisados
> visando a sobrevivência.
> (p. 25)

> Isso ocorre porque a paz política não é ditada pelo que já está lá,
> mas pela consciência progressiva de que não há mais como postergá-la.
> Essa postergação é parte do sonho modernista, e também sua definição de futuro, um futuro que não é senão um voo que vem do passado
> e um “vendar de olhos” para o que está por vir.
> (p. 26)

## Teologia do Antropoceno

> O que eles [visões teológicas de Jean-Pierre Dupuy e Michael Northcott] têm em comum é que do mesmo modo que propõem
> uma base espacial diferente para cada facção bélica, oferecem um outro
> ritmo temporal para a ação. A ação não pode ser postergada porque
> o tempo não flui do presente para o futuro – como se tivéssemos de
> escolher entre dois cenários e esperar que tudo corra bem – mas como
> se o tempo fluísse daquilo que está vindo (“l’avenir”, como se diz em
> francês para diferenciar de “le futur”) para o presente, o que é uma
> outra maneira de considerar “apocalípticos” os tempos nos quais deveríamos viver.
> (p. 26)

> Não há mais fronteira modernizadora.
> Em vez disso, há tantas novas linhas de conflitos que uma Gaia-política totalmente diferente passa agora a redesenhar todos os mapas. Ao
> recombinar todos os ingredientes do que costumava a pertencer aos
> diferentes domínios da subjetividade e da objetividade, a própria noção
> de Antropoceno torna-se realmente uma imensa fonte de confusão –
> porém, uma fonte bem-vinda.
> (p. 28)
