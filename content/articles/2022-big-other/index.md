---
title: Big Other
subtitle: "capitalismo de vigilância e perspectivas para uma civilização informacional"
category: fichamento
date: 2022-06-16
path: "/big-other"
featuredImage: "./zuboff.jpg"
srcInfo: <a href="https://en.wikipedia.org/wiki/Shoshana_Zuboff#/media/File:Shoshana_Zuboff_at_Alexander_von_Humboldt_Institut.jpg">Retirado de wikimedia</a>
published: true
tags:
 - fichamento
 - Shoshana Zuboff
 - capitalismo de vigilância
 - Google
 - informação
 - inteligência artificial

---

Fichamento do artigo _Big Other: capitalismo de vigilância e perspectivas para uma civilização informacional_[^1] escrito originalmente em 2015 pela pensadora Shoshana Zuboff.

[^1]: ZUBOFF, S. Big other: capitalismo de vigilância e perspectivas para uma civilização de informação. In: BRUNO, F. et al. (Eds.). **Tecnopolíticas de Vigilância**: perspectivas da margem. 1. ed. São Paulo: Boitempo, 2018. p. 17–68.

# Aparente agência algoritmica nos big data (tida como autônoma, destacada do social)

> O que essas duas afirmações [relatório da casa branca sobre proliferação e controle dos dados, e afirmação do presidente da Google sobre reter dados do Google Search] compartilham é a atribuição de agência à “tecnologia”. O _big data_ é projetado como a consequência inevitável de um rolo compressor tecnológico que possui uma vida própria totalmente exterior ao social. Nós somos apenas espectadores.
> (p. 17-8)

# Origem social dos big data

> A maioria dos artigos sobre _big data_ começa por uma tentativa de definir a própria expressão, o que indica que ainda não chegamos a uma definição razoável para ela. Defendo que isso ocorre porque continuamos a ver o _big data_ como um objeto, um efeito ou uma capacidade tecnológica. A inadequação dessa percepção nos força a retornar sempre ao mesmo ponto. Neste artigo, adoto uma abordagem diferente, na qual o _big data_ não é uma tecnologia ou um efeito tecnológico inevitável. Tampouco é um processo autônomo, como Eric Schmidt e outros querem que acreditemos. O _big data_ tem origem no social, e é ali que devemos encontrá-lo e estudá-lo.
> (p. 18)

# Big data como componente fundamental do capitalismo de vigilância - nova lógica de acumulação capitalista

> Explorarei então a proposta de que o _big data_ é, acima de tudo, o componente fundamental de uma nova lógica de acumulação, profundamente intencional e com importantes consequências, que chamo de _capitalismo de vigilância_.
> (p. 18)

# Objetivo do capitlismo de vigilância (nova forma de capitalismo de informação) é modular comportamentos, produzir lucro e controlar o mercado.

> Essa nova forma de capitalismo de informação procura prever e modificar o comportamento humano como meio de produzir receitas e controle de mercado.
> (p. 18)

# Origem do capitalismo de vigilância (e do papel dos big data para extração com indiferença formal às populações fonte/alvo) na última década

> O capitalismo de vigilância se formou gradualmente durante a última década, incorporando novas políticas e relações sociais que ainda não haviam sido bem delineadas ou teorizadas. Mesmo que o _big data_ possa ser configurado para outros usos, estes não apagam suas origens em um projeto de extração fundado na indiferença formal em relação às populações que conformam tanto sua fonte de dados quanto seus alvos finais.
> (p. 18)

# Artigo New games, new rules' como referência importante para "abrir a caixa-preta" dos big data

> Algumas pistas importantes para desvendarmos essa nova direção são dadas por Constantiou e Kallinikos no artigo “New games, new rules: big data and the changing context of strategy”3 , no qual desvelam a caixa-preta do _big data_ para revelar os conteúdos epistêmicos e suas problemáticas inerentes. “New games” é uma contribuição potente e necessária para este território intelectual ainda opaco.
> (p. 18)

> O artigo toma por base avisos anteriores para delinear de forma penetrante as características epistêmicas do _big data_ – heterogêneo, não estruturado, transemiótico, descontextualizado, agnóstico – e para iluminar as descontinuidades epistemológicas que essas informações implicam para os métodos e mentalidades das convenções formais, dedutivas, introspectivas e positivistas das estratégias corporativas.
> (p. 18-9)

> 3: I. D. Constantiou e J. Kallinikos, “New games, new rules: big data and the changing context of strategy”, _Journal of Information Technology_, v. 30, n. 1, mar. 2015, p. 44- -57; disponível em: <https://link.springer.com/article/10.1057/jit.2014.17>, acesso em 16 jul. 2018

# Objetivo do artigo: contribuir no entendimento dos big data explorando 1) a transformação da cotidianidade em estratégia de comercialização 2) o ofuscamento das divisões 3) a natureza da empresa e sua relação com as populações

> Minha intenção neste artigo é contribuir com uma nova discussão sobre esses territórios ainda não teorizados, nos quais as efêmeras misturas do _big data_ de Constantiou e Kalliniki estão incorporadas: a transformação da cotidianidade em estratégia de comercialização; o ofuscamento das divisões; a natureza da empresa e sua relação com as populações. Começo com uma breve revisão de alguns conceitos fundamentais como base para os argumentos que vou apresentar. Passarei posteriormente a um exame detalhado de dois artigos do economista-chefe da Google, Hal Varian, que divulgam a lógica e as implicações do capitalismo de vigilância, bem como o papel fundamental do _big data_ nesse novo regime.
> (p. 19)

# Automação x mediação (por computador). Produção de informação pelas TIs. Diferença entre tecnologias inteligentes (smart) ou simples (dumb).

> Em 1981, desenvolvi a noção de “mediação por computador” em um artigo intitulado “Psychological and organizational implications of computer-mediated work”5 . Tanto nesse artigo quanto em escritos subsequentes, distingui o trabalho “mediado pelo computador” da mecanização e automação do trabalho típicas das gerações anteriores, projetadas para simplificar ou até mesmo substituir o trabalho humano6 . Observei que a tecnologia de informação é caracterizada por uma dualidade fundamental que ainda não havia sido completamente apreciada. Ela podia ser aplicada para automatizar operações de acordo com uma lógica que pouco diferia daquela presente em séculos anteriores: substituir o corpo humano por máquinas que possibilitassem maior controle e continuidade. Porém, na tecnologia de informação, a automação gera simultaneamente informação que proporciona um nível mais profundo de transparência a atividades que pareciam parcial ou totalmente opacas. A automação não somente impõe informação (sob a forma de instruções programadas) mas também produz informação. A ação de uma máquina é totalmente investida em seu objeto, mas a tecnologia de informação reflete tanto em suas atividades quanto no sistema de atividades ao qual está relacionada. Isso produz ação ligada a uma voz reflexiva, pois a mediação pelo computador representa simbolicamente eventos, objetos e processos, que se tornam visíveis, passíveis de serem conhecidos e compartilhados de uma nova maneira. Para simplificar, essa distinção marca a diferença entre “inteligente” e “simples”*.
> (p. 19-20)

> * A autora usa as palavras “smart” e “dumb”, que no campo das ciências tecnológicas são utilizadas para diferenciar os dispositivos com mais recursos (smartphones) daqueles mais simples (dumbphones, telefones que só fazem chamadas). (N. T.)
> (p. 20)

# Capacidade de 'informatização' das TIs. Texto eletrônico no trabalho mediado por computador. "Divisão do aprendizado" na empresa.

>  Para descrever essa capacidade peculiar, criei o termo “_informate_”**. Apenas a tecnologia de informação possui a capacidade de automatizar e de _informatizar_. Como uma consequência do processo de _informatização_, o trabalho mediado pelo computador amplia a codificação organizacional, resultando em uma abrangente textualização do ambiente de trabalho – o que chamei de “texto eletrônico”. Esse texto, por sua vez, criou novas oportunidades de aprendizado e, portanto, novas disputas sobre quem aprenderia, como e o quê. A partir do momento em que uma empresa está imbuída da mediação por computador, essa nova “divisão de aprendizado” se torna mais relevante do que a divisão tradicional do trabalho.
>  (p. 20-1)

[O que é o texto eletrônico? Registros organizacionais? Dados em geral? Programas? Outra coisa?]

> O texto, mesmo nos anos 1980, estágios iniciais desses desenvolvimentos, era um tanto heterogêneo e refletia os fluxos de produção e os processos administrativos, assim como as interfaces de clientes, mas também revelava comportamentos humanos: chamadas telefônicas, digitações, intervalos do trabalho e outros sinais de continuidades atencionais, ações, localizações, conversações, redes, compromissos específicos com pessoas e equipamentos etc.
> (p. 21)

> ** O termo “informate”, em inglês, é um neologismo que reúne as palavras “informar” e “automatizar”. Optamos por traduzi-lo como “informatizar” para manter o sentido mais próximo do proposto pela autora, ainda que não comporte a nuance presente no termo em inglês. (N. T.)
> (p. 20)

# Sobreposição entre trabalho e aprendizado na contemporaneidade. Práticas institucionalizadas e informais

> O aprendizado em tempo real, baseado em informação e mediado pelo computador, tornou-se tão endógeno para as atividades cotidianas dos negócios que os dois domínios já se confundem, sendo aquilo que a maioria de nós faz quando trabalha. Esses novos fatos estão institucionalizados em milhares, se não milhões, de novos tipos de ações dentro das organizações. Algumas dessas ações são mais formais: metodologias de aperfeiçoamento contínuo, integração empresarial, monitoramento de empregados, sistemas de tecnologia da informação e comunicação que proporcionam a coordenação global de operações dispersas de manufatura, atividades profissionais, formação de equipes de trabalho, informações sobre clientes, cadeias de fornecedores, projetos inter-empresas, forças de trabalho móveis e temporárias e abordagens de _marketing_ para diferentes configurações de consumidores. Outras são menos formais: o fluxo incessante de mensagens eletrônicas, buscas _online_, atividades no _smartphone_, aplicativos, textos, videoconferências, interações em redes sociais etc.
> (p. 21-2)

# Divisão do aprendizado não possui forma pura. Como essas questões se relacionam? Retomar (??)

> A divisão do aprendizado não possui uma forma pura, contudo. Após vinte anos de pesquisa de campo, encontrei uma mesma lição com centenas de variações. A divisão do aprendizado, assim como a divisão do trabalho, é sempre conformada por disputas sobre as seguintes questões: Quem participa, e como? Quem decide quem participa? O que acontece quando a autoridade falha? Na esfera do mercado, o texto eletrônico e o que se pode aprender a partir dele nunca foram nem podem ser “coisas em si”. Eles estão sempre já constituídos pelas respostas a essas questões. Em outras palavras, eles já estão incorporados no social, e suas possibilidades estão circunscritas pela autoridade e pelo poder.
> (p. 22)

# Lógica da acumulação pressuposta no texto eletrônico. Desta forma, existe uma lógica invisível pressuposta nas tecnologias (?) e que é tomado como dado em qualquer modelo de negócio.

> O ponto-chave aqui é que o texto eletrônico, quando estamos tratando da esfera do mercado, já se encontra organizado pela lógica de acumulação na qual está incorporado, bem como pelos conflitos inerentes a essa lógica. A lógica de acumulação organiza a percepção e molda a expressão das capacidades tecnológicas em sua origem, sendo aquilo que já é tomado como dado em qualquer modelo de negócio. Suas suposições são amplamente tácitas e seu poder de moldar o campo das possibilidades é, então, amplamente invisível. Ela define objetivos, sucessos, fracassos e problemas, além de determinar o que é mensurado e o que é ignorado, o modo como recursos e pessoas são alocados e organizados, quem – e em quais funções – é valorizado, quais atividades são realizadas e com que propósitos. A lógica de acumulação produz suas próprias relações sociais e com elas suas concepções e seus usos de autoridade e poder.
> (p. 22)

# Computação ubíqua como truismo. Mundo traduzido em uma nova dimensão simbólica: dados. Texto eletrônico universal em escala e escopo (?).

> De que forma esses blocos conceituais podem nos ajudar a tirar algum sentido do big data? Alguns pontos são óbvios: 3 bilhões dos 7 bilhões de pessoas do mundo têm uma ampla gama de atividades diárias mediadas, muito além das fronteiras tradicionais do local de trabalho. Para elas, o antigo sonho da computação ubíqua12 é um truísmo que mal se nota. Como resultado da penetrante mediação por computador, quase todos os aspectos do mundo são traduzidos em uma nova dimensão simbólica à medida que eventos, objetos, processos e pessoas se tornam visíveis, cognoscíveis e compartilháveis de uma nova maneira. O mundo renasce como dados e o texto eletrônico é universal em escala e escopo13.
> (p. 23-4)

> 13: Em 1986, calculamos a existência de 2,5 exabytes de informação comprimida, dos quais somente 1% se encontrava digitalizado; M. Hilbert, “Technological information inequality as an incessantly moving target: the redistribution of information and communication capacities between 1986 and 2010”, Journal of the American Society for Information Science and Technology, v. 65, n. 4, 2013, p. 821-35. No ano 2000, somente 25% da informação armazenada em todo o mundo era digital; V. Mayer-Schönberger; K. Cukier, Big data: a revolution that will transform how we live, work, and think (Boston, Houghton Mifflin Harcourt K., 2013), p. 9. Já em 2007, calculamos 300 exabytes comprimidos de forma ideal com uma taxa de 94% de digitalização; M. Hilbert, “Technological information inequality as an incessantly moving target”, cit. A digitalização e a dataficação (o programa que permite a computadores e algoritmos processar e analisar dados brutos), junto com o desenvolvimento de tecnologias de armazenamento mais baratas, produziram 1.200 exabytes de dados armazenados no mundo com uma taxa de 98% de conteúdo digital; V. Mayer-Schönberger; K. Cukier, Big data, cit., p. 9.
> (p. 24)

# Civilização da informação: aspecto global dessa lógica de acumulação baseada em dados.

> Há não muito tempo, ainda parecia razoável concentrar nossas preocupações nos desafios de um local de trabalho informacional ou de uma sociedade da informação. Agora, as questões persistentes de autoridade e poder devem ser direcionadas ao quadro mais amplo possível, mais bem definido como civilização ou, especificamente, civilização da informação. Quem aprende com os fluxos de dados globais, como e o quê? Quem decide? O que acontece quando a autoridade falha? Qual lógica de acumulação moldará as respostas a essas perguntas? Reconhecer sua escala civilizacional confere força e urgência a essas novas questões. Suas respostas moldarão o caráter da civilização da informação ao longo deste século, assim como a lógica do capitalismo industrial e seus sucessores moldaram o caráter da civilização industrial nos últimos dois séculos.
> (p. 24)

# Objetivo do artigo: explicitar uma lógica emergente de acumulação hegemônica. A Google como foco (pioneira em _big data_ e no capitalismo de vigilância).

> Minha ambição neste artigo é dar início à tarefa de iluminar uma lógica emergente de acumulação hegemônica nos espaços interconectados atuais. O foco dessa exploração é o Google, o serviço de busca mais popular do mundo. **A Google* é considerada por muitos como a pioneira do _big data_14 e com a força desses feitos também foi pioneira na lógica de acumulação mais ampla que denomino de capitalismo de vigilância, da qual o _big data_ é tanto uma condição quanto uma expressão.**
> (p. 24-5)

> * Ao longo do artigo, o serviço de buscas que funciona a partir de um site será tratado no masculino (“o Google”), enquanto a empresa que criou e gere esse serviço de buscas e muitos outros dos mais importantes negócios da internet será referida no gênero feminino (“a Google”). (N. T.)
> (p. 24)

# Capitalismo de vigilância como lógica emergente e modelo-padrão de startups online.

> Essa lógica emergente não apenas é compartilhada pelo Facebook e outras grandes empresas _online_ mas parece ter se tornado o modelo-padrão para a maior parte das _startups_ _online_ e aplicativos.
> (p. 25)

# Foco de análise: documentos de Hal Varian, economista da Google

> A discussão apresentada neste artigo é orientada por dois documentos extraordinários escritos por Hal Varian, economista-chefe da Google16. Suas observações e afirmações nos oferecem um ponto de partida para a compreensão da lógica sistêmica da acumulação na qual o _big data_ está incorporado. Ressalto que, mesmo que Varian não seja um executivo de linha da Google, seus artigos nos convidam a uma inspeção bem próxima das práticas da empresa, que são exemplares dessa nova lógica de acumulação. Varian ilumina seus argumentos com exemplos retirados da Google nesses dois escritos, muitas vezes utilizando a primeira pessoa no plural, como nas seguintes passagens: “A Google tem obtido muito sucesso a partir de nossos experimentos, a ponto de os disponibilizarmos para nossos anunciantes e editores em dois programas”, [[...]]
> (p. 25)

> Parece justo supor, portanto, que as perspectivas de Varian refletem a substância das práticas de negócios da Google e, até certo ponto, a visão de mundo que está subjacente a essas práticas.
> (p. 25)

> 16: H. R. Varian, “Computer mediated transactions”, _American Economic Review_, v. 100, n. 2, 2010, p. 1-10; “Beyond big data”, _Business Economics_, v. 49, n. 1, 2014, p. 27-31.
> (p. 25)

# Tema dos documentos de Varian: transações mediadas por computador

> Nos dois artigos que examino aqui, o principal assunto tratado por Varian é a universalidade das “transações econômicas mediadas por computador”. Ele escreve: “O computador cria um registro da transação [...]. Eu argumento que essas transações mediadas por computador permitiram melhorias significativas na forma como as transações são realizadas e continuarão a impactar a economia no futuro que prevemos”18.
> (p. 25-6)

# Diferenças dessa nova ordem cognoscível [pra quem?] em relação ao ideal de mercado neoliberal em Hayek

> A _informatização_ da economia, como ele observa, é constituída por um registro persistente e contínuo dos detalhes de cada transação. Nessa visão, a mediação por computador torna a economia transparente e cognoscível de novas maneiras. Isso se configura como um contraste marcante com o clássico ideal neoliberal do “mercado” como intrinsecamente inefável e incognoscível. Na concepção de Hayek, o mercado seria como uma “ordem ampliada” incompreensível à qual os meros indivíduos devem subjugar suas vontades19. Foi precisamente a impossibilidade de conhecimento do universo das transações de mercado que ancorou as reivindicações de Hayek quanto à necessidade de liberdade radical da intervenção ou de regulação por parte do Estado.
> (p. 26)

# Novos 'usos' a partir de transações mediadas por computador: 1) transação e análise de dados 2) novas formas contratuais devido a um melhor monitoramento 3) personalização e customização 4) experimentos contínuos

> Diante dos novos fatos a respeito de um mercado cognoscível, Varian afirma quatro novos “usos” que se seguem a transações mediadas por computador: “extração e análise de dados”, “novas formas contratuais devido a um melhor monitoramento”, “personalização e customização” e “experimentos contínuos”20. Cada um deles possibilita _insights_ sobre uma lógica emergente de acumulação, a divisão de aprendizagem que ela forma e o caráter da civilização da informação para a qual ela conduz.
> (p. 26)

> 20 H. R. Varian, “Beyond big data”, cit.
> (p. 26)

# Origem dos dados: transações econômicas, sistemas institucionais e outros fluxos mediados por computador (sensores, computação ubíqua)

> Os dados derivados de transações econômicas mediadas por computadores constituem uma dimensão significativa do _big data_. Existem, entretanto, outras fontes, incluindo fluxos que surgem de uma variedade de sistemas institucionais e transinstitucionais mediados por computador. Podemos incluir junto a estes uma segunda fonte de **fluxos mediados por computador, que deverá crescer exponencialmente: dados de bilhões de sensores incorporados em uma ampla gama de objetos, corpos e lugares**.
> (p. 27)

# Internet of Everything

> Um relatório bastante citado da Cisco prevê um novo valor agregado de US$14,4 trilhões à “internet de todas as coisas”22.
> (p. 27)

> 22 Cisco, _Embracing the internet of everything to capture your share of $14.4 trillion_ (Cisco Systems, 2013); disponível em: <http://www.cisco.com/web/about/ac79/docs/innov/ IoE_Economy.pdf>, acesso em 9 jun. 2014; _The internet of everything: global private sector economic analysis_ (Cisco Systems, Inc., 2013); disponível em: <https://www. cisco.com/c/dam/en_us/about/business-insights/docs/ioe-vas-public-sector-top-10insights.pdf>, acesso em 31 out. 2018.
> (p. 27)

# Google e a criação de uma "nova infraestrutura inteligente para corpos e objetos"

> Os novos investimentos da Google em **_machine learning_*, _drones_, dispositivos vestíveis, carros automatizados, nanopartículas que patrulham o corpo procurando por sinais de doenças e dispositivos inteligentes para o monitoramento do lar** são componentes essenciais dessa cada vez maior **rede de sensores inteligentes e dispositivos conectados à internet** destinados a formar uma nova infraestrutura inteligente para corpos e objetos23.
> (p. 27)

> 23 T. Bradshaw, “Google bets on ‘internet of things’ with $3.2bn Nest deal”, Financial Times, 13 jan. 2014; disponível em: <https://www.ft.com/content/90b8714a-7c9911e3-b514-00144feabdc0>, acesso em 22 nov. 2014; “Google buys UK artificial intelligence start-up”, Financial Times, 27 jan. 2014; disponível em: <https://www. ft.com/content/f92123b2-8702-11e3-aa31-00144feab7de>, acesso em 22 nov. 2014; S. Kovach, “Google’s plan to take over the world”, Business Insider, 18 maio 2013; disponível em: <http://www.businessinsider.com/googles-plan-to-take-overthe-world-2013-5>, acesso em 22 nov. 2014; BBC News, “Wearables tracked with Raspberry Pi”, BBC News, 1o ago. 2014; disponível em: <http://www.bbc.com/news/ technology-28602997>, acesso em 22 nov. 2014; T. Brewster, “Traffic lights, fridges and how they’ve all got it in for us”, The Register, 23 jun. 2014; disponível em: http:// www.theregister.co.uk/2014/06/23/hold_interthreat/>, acesso em 22 nov. 2014; E. Dwoskin, “What secrets your phone is sharing about you”, Wall Street Journal, 14 jan. 2014; disponível em: <http://online.wsj.com/articles/SB10001424052702303 453004579290632128929194>, acesso em 16 jul. 2018; “The new GE: Google, everywhere”, The Economist, 16 jan. 2014; disponível em: <https://www.economist. com/business/2014/01/16/the-new-ge-google-everywhere>, acesso em 16 jul. 2018; E. Fink, “This drone can steal what’s on your phone”, CNNMoney, 20 mar. 2014; disponível em: <https://money.cnn.com/2014/03/20/technology/security/dronephone/index.html>, acesso em 22 nov. 2014; H. Kelly, “Smartphones are fading. Wearables are next”, CNNMoney, 19 mar. 2014; disponível em: <https://money. cnn.com/2014/03/19/technology/mobile/wearable-devices/index.html>, acesso em 22 nov. 2014; P. Lin, “What if your autonomous car keeps routing you past Krispy Kreme?”, The Atlantic, 22 jan. 2014; disponível em: <https://www.theatlantic.com/ technology/archive/2014/01/what-if-your-autonomous-car-keeps-routing-you-pastkrispy-kreme/283221/>, acesso em 22 nov. 2014; B.-A. Parnell, “Is Google building SKYNET? Ad kingpin buys AI firm DeepMind”, The Register, 27 jan. 2014; disponível em: <http://www.theregister.co.uk/2014/01/27/google_deep_mind_buy/>, acesso em 16 nov. 2018; R. Winkler; D. Wakabayashi, “Google to buy nest labs for $3.2 billion – update”, EuroInvestor, 14 jan. 2014; disponível em: <http:// www.euroinvestor.com/news/2014/01/14/google-to-buy-nest-labs-for-32-billionupdate/12658007>, acesso em 22 nov. 2014.
> (p. 27-8)

# Outra fonte de dados: fluxos de bases de dados governamentais e corporativos

> Uma terceira fonte de dados flui de bancos de dados governamentais e corporativos, incluindo aqueles associados aos bancos, à intermediação de pagamentos eletrônicos, às agências de avaliação de crédito, às companhias aéreas, aos registros censitários e fiscais, às operações de planos de saúde, aos cartões de crédito, aos seguros, às empresas farmacêuticas e de comunicações, e outros mais.
> (p. 27-8)

# O papel dos _data brokers_ na disponibilização desses dados

> Muitos desses dados, juntamente com os fluxos das transações comerciais, são adquiridos, agregados, analisados, acondicionados e por fim vendidos por _data brokers_ que operam (pelo menos nos Estados Unidos) de forma sigilosa, ao largo dos estatutos de proteção do consumidor e sem seu consentimento e conhecimento, ignorando seus direitos à privacidade e aos devidos procedimentos legais24.
> (p. 28)

> 24 U.S. Committee on Commerce, Science, and Transportation. A review of the data broker industry: collection, use and sale of consumer data for marketing purposes (Washington, D.C., Office of Oversight and Investigations, 2013); disponível em: <http://www.commerce.senate.gov/public/?a=Files.Serve&File_id=0d2b3642-62214888-a631-08f2f255b577>, acesso em 16 jul. 2018.
> (p. 28)

# Câmeras de vigilância como fluxo de dados: Google na linha de frente com o Street View e Google Earth

> Uma quarta fonte de _big data_, que fala sobre seu caráter heterogêneo e transemiótico, flui de câmeras de vigilância públicas e privadas, incluindo qualquer coisa desde _smartphones_ até satélites, do Google Street View ao Google Earth. A Google tem estado na linha de frente desse domínio contencioso de dados.
> (p. 28)

# Sobre a prática ilegal de extração de dados pelo Google Street View

> Por exemplo, o Street View foi lançado em 2007 e logo encontrou resistência em todo o mundo. Autoridades alemãs descobriram que, entre outros problemas, os carros do Street View estavam equipados com escâneres ativados para extrair dados de redes sem fio privadas25. Em um processo movido por 39 procuradores do Estado norte-americanos contra a Google, sumariado pelo Electronic Privacy Information Center (Epic) [Centro de Informações de Privacidade Eletrônica], foi concluído que “a empresa participou na coleta não autorizada de dados de redes sem fio, incluindo dados de redes Wi-Fi privadas de usuários de internet residencial”. O relatório do Epic resume uma versão redigida de um relatório da FCC* que revela que a “Google interceptou intencionalmente dados com fins comerciais e que muitos engenheiros e supervisores da empresa revisaram o código-fonte e os documentos associados ao projeto”26. De acordo com a reportagem do _The New York Times_ sobre o processo, o qual resultou em um acordo de US$7 milhões, “a empresa de buscas pela primeira vez em sua história foi obrigada a policiar de forma agressiva seus empregados em assuntos de privacidade [...]”27. O Street View sofreu restrições em muitos países e continua a enfrentar processos litigiosos em torno do que os reclamantes caracterizam como táticas “secretas”, “ilícitas” e “ilegais” de coleta de dados nos Estados Unidos, na Europa e em outras regiões28.
> (p. 29)

> 25 K. J. O’Brien; C. C. Miller, “Germany’s complicated relationship with Google Street View”, Bits Blog, 23 abr. 2013; disponível em: <http://bits.blogs.nytimes. com/2013/04/23/germanys-complicated-relationship-with-google-street-view/>, acesso em 21 nov. 2014.
> (p. 29)

> 26 Epic, Investigations of Google Street View (Washington, D.C., Electronic Privacy Information Center, 2014); disponível em: <https://epic.org/privacy/streetview/>, acesso em 21 nov. 2014.
> (p. 29)

> 27 D. Streitfeld, “Google concedes that drive-by prying violated privacy”, The New York Times, 12 mar. 2013; disponível em: <https://www.nytimes.com/2013/03/13/ technology/google-pays-fine-over-street-view-privacy-breach.html>, acesso em 16 jul. 2018.
> (p. 29)

> 28 Office of the Privacy Commission of Canada, Google contravened Canadian privacy law, investigation finds (Quebec, Office of the Privacy Commissioner of Canada, 2010); disponível em: <https://www.priv.gc.ca/en/opc-news/news-and-announce ments/2010/nr-c_101019/>, acesso em 21 nov. 2014; K. J. O’Brien, “European regulators may reopen Google Street View inquiries”, The New York Times, 2 maio 2012; disponível em: <http://www.nytimes.com/2012/05/03/technology/europeanregulators-to-reopen-google-street-view-inquiries.html>, acesso em 16 jul. 2018; A. Jammet, “The evolution of EU law on the protection of personal data”, Center for European Law and Legal Studies, v. 3, n. 6, 2014, p. 1-18.
> (p. 29)

# "Imperialismo de infraestrutura da Google": método de incursões em territórios privados não protegidos até que se encontre resistências

> Com o Street View, a Google desenvolveu um método declarativo que foi utilizado em outros empreendimentos relativos a dados. O _modus operandi_ consiste em fazer incursões em territórios privados não protegidos até que alguma resistência seja encontrada. Como um observador dos direitos do consumidor resumiu para o _The New York Times_, “a Google coloca a inovação à frente de tudo e resiste a pedir permissão”29. A empresa não pergunta se pode fotografar casas para seus bancos de dados, ela simplesmente pega o que quer. A Google, então, esgota seus adversários no tribunal ou eventualmente concorda em pagar multas que representam um investimento negligenciável para um retorno significativo30. Siva Vaidhyanathan chamou esse processo de “imperialismo de infraestrutura”31. O Epic mantém um registro online abrangente das centenas de processos de países, estados, grupos e indivíduos abertos contra a Google, havendo ainda muitos outros casos que nunca se tornaram públicos32.
> (p. 30)

> 29 D. Streitfeld, “Google concedes that drive-by prying violated privacy”, cit. Veja também M. Burdon; A. McKillop, The Google Street View Wi-Fi scandal and its repercussions for privacy regulation, Research Paper n. 14-07, University of Queensland, TC Beime School of Law, 2013; disponível em: <https://papers.ssrn.com/sol3/papers. cfm?abstract_id=2471316>, acesso em 16 jul. 2018.
> (p. 30)

> 30 A decisão da Corte Europeia de 2014 em favor do “direito de ser esquecido” representou a primeira vez que a Google foi forçada a modificar substancialmente suas práticas em função de demandas regulatórias – o primeiro capítulo do que seguramente é uma história em desenvolvimento.
> (p. 30)

> 31 S. Vaidhyanathan, The googilization of everything (Berkeley, University of California Press, 2011).
> (p. 30)

> 32 Epic, Google glass and privacy (Washington, D.C., Electronic Privacy Information Center, 2014); disponível em: <https://epic.org/privacy/google/glass/>, acesso em 15 nov. 2014; Investigations of Google Street View, cit.
> (p. 30)

# Aspecto subjetivo da produção desses dados? A internet como ponto de não retorno.

> Esses fluxos de dados produzidos institucionalmente representam o lado da “oferta” da interface mediada por computador. Apenas com esses dados é possível construir perfis individuais detalhados. Mas a universalidade da mediação por computador se deu mediante um complexo processo de causalidade que inclui também atividades subjetivas – que constituem a sua “demanda”. As necessidades individuais impulsionaram as curvas de penetração acelerada da internet. Uma pesquisa da BBC realizada em 2010 descobriu que, menos de duas décadas após o navegador Mosaic ter sido liberado ao público, permitindo um fácil acesso à _world wide web_ (www), 79% das pessoas em 26 países consideraram que o acesso à internet era um direito humano fundamental33.
> (p. 30-1)

# A internet como espaço livre e de 'formas não mercantis de produção social'

Parece uma visão um pouco ultrapassada de um campo que há muito parece majoritariamente capturado

> Fora dos hierárquicos espaços de trabalho que operam pela lógica do mercado, o acesso, a indexação e as buscas na internet significaram a possibilidade de liberdade dos indivíduos de procurar os recursos de que necessitavam para uma vida mais eficaz, sem os impedimentos impostos pelo monitoramento, pela métrica, pela insegurança, por requisitos de função e pelo sigilo imposto pela empresa e sua lógica de acumulação. As necessidades individuais de autoexpressão, voz, influência, informação, aprendizagem, empoderamento e conexão reuniram em poucos anos uma ampla gama de novas capacidades: pesquisas do Google, música do iPod, páginas do Facebook, vídeos do YouTube, blogs, redes, comunidades de amigos, estranhos e colegas, todos ultrapassando as antigas fronteiras institucionais e geográficas em uma espécie de exultação de caça, coleta e compartilhamento de informações para todos os propósitos, ou mesmo para nenhum. Era meu, e eu poderia fazer o que quisesse com isso34! Essas subjetividades de autodeterminação encontraram expressão em uma nova esfera individual em rede caracterizada pelo que Benkler35 resumiu adequadamente como formas não mercantis de “produção social”.
> (p. 31)

# Atividades não mercantis (cotidianidade, _small data_, _data exaust_) como fonte de dados

> Essas atividades não mercantis são uma quinta fonte principal de big data e a origem do que Constantiou e Kallinikos36 chamam de “cotidianidade”. O _big data_ é constituído pela captura de _small data_, das ações e discursos, mediados por computador, de indivíduos no desenrolar da vida prática. Nada é trivial ou efêmero em excesso para essa colheita: as “curtidas” do Facebook, as buscas no Google, _e-mails_, textos, fotos, músicas e vídeos, localizações, padrões de comunicação, redes, compras, movimentos, todos os cliques, palavras com erros ortográficos, visualizações de páginas e muito mais. Esses dados são adquiridos, tornados abstratos, agregados, analisados, embalados, vendidos, analisados mais e mais e vendidos novamente. Esses fluxos de dados foram rotulados pelos tecnólogos de “_data exhaust_”*. Presumidamente, uma vez que os dados são redefinidos como resíduos, a contestação de sua extração e eventual monetização é menos provável.
> (p. 31-2)

# A Google como principal detentora de tais dados. O modelo de propagandas.

> A Google tornou-se a maior e mais bem-sucedida empresa de _big data_ por ter o site mais visitado e, portanto, possuir a maior quantidade de _data exhaust_. Como muitas outras empresas digitais, a Google correu para atender às ondas de demanda reprimida que inundaram a esfera individual em rede nos primeiros anos da world wide web. Era um exemplo claro de empoderamento individual nas demandas de uma vida mais eficaz. Mas, à medida que as pressões para o lucro avançavam, os líderes da Google se preocupavam com os efeitos que o modelo de serviços pagos poderia ter no crescimento do número de usuários. Eles então optaram por um modelo de propaganda. **A nova abordagem dependia da aquisição de dados de usuários como matéria-prima para análise e produção de algoritmos que poderiam vender e segmentar a publicidade por meio de um modelo de leilão exclusivo, com precisão e sucesso cada vez maiores**. À medida que as receitas da Google cresciam rapidamente, aumentava a motivação para uma coleta de dados cada vez mais abrangente37. **A nova ciência de análise de _big data_ explodiu, impulsionada em grande parte pelo sucesso retumbante da Google**.
> (p. 32)

> Em um artigo na revista _Wired_ em 2009 sobre “Googlenomics”, Varian comentou: “Por que a Google disponibiliza produtos de graça...? Qualquer coisa que aumente o uso da internet, em última instância, enriquece a Google [...]”. O artigo continua: “[...] mais olhos fixados na _web_ levam inexoravelmente a mais vendas de anúncios para a Google [...]. E, como a previsão e a análise são tão cruciais para o Google AdWords, qualquer _bit_ de dados, mesmo que aparentemente trivial, tem valor potencial”39. O tema é reiterado no artigo “_Big data_”, de Mayer-Schönberger e Cukier: “Muitas empresas projetam seus sistemas para que possam colher _data exhaust_ [...]. A Google é a líder incontestável [...] **todas as ações que um usuário executa são consideradas sinais a serem analisados e que servirão de _feedback_ para o sistema**”40. Isso ajuda a explicar por que a Google superou todos os concorrentes pelo privilégio de fornecer Wi-Fi gratuito para os 3 bilhões de clientes anuais da Starbucks41. Mais usuários produzem mais _data exhausts_, que, por sua vez, melhoram o valor preditivo das análises e resultam em leilões mais lucrativos.
> (p. 33)

# A Google é 'formalmente indiferente' aos dados, o que importa é quantidade, não qualidade

Captura da informação em dado.

> O que importa é a quantidade, e não a qualidade. Outra maneira de dizer isso é que a Google é “formalmente indiferente” ao que os usuários dizem ou fazem, **contanto que o digam e o façam de forma que o Google possa capturar e converter em dados**.
> (p. 33)

# Fontes de dados:

1. Fluxos institucionais e transinstitucionais mediados por computador (dos quais as transações econômicas são parte significativa)
1. Fluxos de sensores incorporados em objetos, corpos e lugares (IoT, IoE, etc.)
1. Fluxos de bases de dados governamentais e corporativas (incluindo bancos, serviços públicos, telecomunicações, etc.)
1. Fluxos de câmeras de vigilância públicas e privadas (desde _smartphones_ até satélites)
1. Fluxos residuais produzidos acidentalmente por usuários individuais (curtidas, visualizações, textos, erros ortográficos, etc.)

# Proeminência da indiferença formal nessa emergente lógica de acumulação.

> Essa “indiferença formal” é uma característica proeminente, talvez decisiva, da emergente lógica de acumulação em exame aqui. O segundo termo na frase de Varian, “extração”, também ilumina as relações sociais implicadas pela indiferença formal.
> (p. 33)

# Extração como ato unidirecional (captura)

> Em primeiro lugar, e de forma mais óbvia, a extração é um processo unidirecional, e não um relacionamento. A extração tem por conotação “tomar algo” em vez de “entregar” ou de uma reciprocidade de “dar e receber”. Os processos extrativos que tornam o _big data_ possível normalmente ocorrem na ausência de diálogo ou de consentimento, apesar de indicarem tanto fatos quanto subjetividades de vidas individuais.
> (p. 33-4)

# Status de subjetividade como algo valioso, não realização individual

> Essas subjetividades percorrem caminhos ocultos para agregação e descontextualização, apesar de serem produzidas como íntimas e imediatas, ligadas a projetos e contextos individuais42. Na verdade, é o _status_ de tais dados como sinais de subjetividades que os tornam mais valiosos para os anunciantes. Para a Google e outros agregadores de _big data_, no entanto, os dados são apenas _bits_. As subjetividades são convertidas em objetos que reorientam o subjetivo para a mercantilização. Os sentidos individuais dados pelos usuários não interessam ao Google ou às outras empresas nessa cadeia.
> (p. 34)

# População como fonte de estração e alvo das ações que os dados produzem

> Dessa forma, os métodos de produção de _big data_ a partir de _small data_ e as formas pelas quais o _big data_ adquire valor refletem a indiferença formal que caracteriza o relacionamento da empresa com suas populações de “usuários”. As populações são as fontes das quais a extração de dados procede e os alvos finais das ações que esses dados produzem
> (p. 34)

# Importância de Snowden na divulgação das práticas da Google

> Suas práticas parecem destinadas a ser indetectáveis ou, pelo menos, obscuras, e, se não fosse a denúncia de Edward Snowden sobre a National Security Agency (NSA) [Agência de Segurança Nacional], aspectos importantes de suas operações, especialmente quando se sobrepõem aos interesses de segurança do Estado, ainda estariam ocultos.
> (p. 34)

# Práticas ilegais de invasão de privacidade e extração de daodos pela Google

> A maior parte do que se sabia sobre as práticas da Google surgiu a partir dos
> conflitos que essas práticas produziram43. Por exemplo, a Google enfrentou
> oposição legal e protesto social em relação a reclamações contra
>
> 1. a prática de varredura de e-mails, incluindo os de usuários que não são do Gmail e os de estudantes que usam seus aplicativos educacionais44,
> 2. a captura de
> comunicações de voz45,
> 3. ignorar as configurações de privacidade46,
> 4. práticas unilaterais de agrupamento de dados em seus serviços online47,
> 5. extensa retenção de dados de pesquisa48,
> 6. rastreamento dos dados de localização dos smartphones49 e
> 7. suas tecnologias portáteis e capacidades de reconhecimento facial50.
>
> (p. 34-5)


> 43 J. Angwin, Dragnet nation: a quest for privacy, security, and freedom in a world of relentless surveillance (Nova York, Times, 2014).
> (p. 34)

> 44 B. Herold, “Google under fire for data-mining student email messages – education week”, Education Week, 26 mar. 2014; disponível em:<https://www.edweek.org/ew/ articles/2014/03/13/26google.h33.html>, acesso em 17 jul. 2018; Q. Plummer, “Google email tip-off draws privacy concerns”, Tech Times, 5 ago. 2014; disponível em: <http://www.techtimes.com/articles/12194/ 20140805/google-email-tip-offdraws-privacy-concerns.htm>, acesso em 21 nov. 2014.
> (p. 34-5)

> 45 J. Menn; D. Schåfer; T. Bradshaw, “Google set for probes on data harvesting”, Financial Times, 17 maio 2010; disponível em: <https://www.ft.com/content/254ff5b6-61e211df-998c-00144feab49a>, acesso em 21 nov. 2014.
> (p. 35)

> 46 J. Angwin, “Google faces new privacy probes”, The Wall Street Journal, 16 mar. 2012; disponível em: <https://www.wsj.com/articles/SB1000142405270230469280457 7283821586827892>, acesso em 21 nov. 2014; J. Owen, “Google in court again over ‘right to be above British law’ on alleged secret monitoring”, The Independent, 8 dez. 2014; disponível em: <https://www.independent.co.uk/news/uk/crime/googlechallenges-high-court-decision-on-alleged-secret-monitoring-9911411.html>, acesso em 17 jul. 2018.
> (p. 35)

> 47 CNIL, Google privacy policy: WP29 proposes a compliance package (Paris, Commission Nationale de L’informatique et des Libertés, 2014); disponível em: <http://www. cnil.fr/english/news-and-events/news/article/google-privacy-policy-wp29-proposesa-compliance-package/>, acesso em 21 nov. 2014. J. Doyle, “Google facing legal action in EVERY EU country over ‘data goldmine’ collected about users”, Daily Mail Online, 2 abr. 2013; disponível em: <http://www.dailymail.co.uk/sciencetech/ article-2302870/Google-facing-legal-action-EVERY-EU-country-data-goldminecollected-users.html>, acesso em 21 nov. 2014.
> (p. 35)

> 48 N. Anderson, “Why Google keeps your data forever, tracks you with ads”, ArsTechnica, 8 mar. 2010; disponível em: <https://arstechnica.com/tech-policy/2010/03/ google-keeps-your-data-to-learn-from-good-guys-fight-off-bad-guys/>, acesso em 21 nov. 2014; K. J. O’Brien; T. Crampton, “E.U. probes Google over data retention policy”, The New York Times. 26 maio 2007; disponível em: <http://www.nytimes. com/2007/05/26/business/26google.html>, acesso em 17 jul 2018.
> (p. 35)

> 49 J. Mick, “Aclu fights for answers on police phone location data tracking”, Daily Tech, 4 ago. 2011; disponível em: <https://web.archive.org/web/20110807005631/http:// www.dailytech.com:80/ACLU+Fights+for+Answers+on+Police+Phone+Location+Da ta+Tracking/article22352.htm>, acesso em 31 out. 2018; D. Snelling, “Google Maps is tracking you! How your smartphone knows your every move”, Express, 18 ago. 2014; disponível em: <https://www.express.co.uk/life-style/science-technology/500811/ Google-Maps-is-tracking-your-every-move>, acesso em 21 nov. 2014.
> (p. 35)

> 50 Epic, Google glass and privacy, cit.
> (p. 35)

# Oposição nos EUA e Europa (e na América Latina e no Brasil?)

> Esses contestáveis movimentos de coleta de dados enfrentam oposição substancial na União Europeia (UE), bem como nos Estados Unidos51.
> (p. 35-6)

# Reciprocidade entre empresa e população (funcionários e consumidores) no sec XX

> A “extração” resume a ausência de reciprocidades estruturais entre a empresa e suas populações. Esse fato sozinho separa a Google, bem como outros que participam de sua lógica de acumulação, da narrativa histórica das democracias de mercado ocidentais. Por exemplo, a empresa do século XX, canonizada por estudiosos como Berle & Means52 e Chandler Jr.53, teve sua origem e foi sustentada por profundas interdependências com suas populações. [[ ... ]] Essa forma de mercado valorizou intrinsecamente suas populações de indivíduos recém-modernizados como fonte de funcionários e clientes; ela dependia de suas populações de maneiras que levaram, ao longo do tempo, a reciprocidades institucionalizadas.
> (p. 37)

# Google e big data não depende de uma população (senão como recursos a serem extraídos)

> A Google e o projeto de _big data_ representam uma ruptura com esse passado. Suas populações não são mais necessárias como fonte de clientes ou funcionários. Os anunciantes são seus clientes, junto com outros intermediários que compram suas análises de dados. A Google empregava apenas cerca de 48 mil trabalhadores quando da publicação deste artigo e é conhecida por ter milhares de candidatos para cada abertura de vaga, contrastando com a General Motors, que, no auge de seu poder, em 1953, foi a maior empregadora privada do mundo. A Google, assim sendo, tem pouco interesse em seus usuários enquanto funcionários. Esse padrão vale para as empresas de alta tecnologia em hiperescala que alcançam o crescimento, principalmente, ao ampliar a automação. Por exemplo, as três maiores empresas do Vale do Silício em 2014 tiveram receita de US$247 bilhões, com apenas 137 mil funcionários e uma capitalização de mercado combinada de US$1,09 trilhão. Em contraste, mesmo em 1990, as três principais montadoras de Detroit produziram receitas de US$250 bilhões com 1,2 milhão de funcionários e uma capitalização de mercado combinada de US$36 bilhões58
> (p. 37-8)

> Pelo contrário, apesar do seu papel de “principal utilitário para a _world wide web_60 e seus investimentos substanciais em tecnologias com consequências sociais explosivas, como inteligência artificial, robótica, reconhecimento facial, tecnologias vestíveis, nanotecnologia, dispositivos inteligentes e drones, a Google não esteve sujeita a nenhuma supervisão pública significativa61. Em uma carta aberta à Europa, o presidente da Google, Eric Schmidt, expressou sua frustração com a perspectiva de supervisão pública, caracterizando-a como uma “regulamentação pesada”, que criaria “sérios perigos econômicos” para a Europa62.
> (p. 38-9)

# Hiperescala e exploração de recursos com pouco custo

> A indiferença formal da Google e a distância funcional das populações são ainda mais institucionalizadas nas necessidades de “análise” enfatizadas por Varian. A Google é a pioneira da hiperescala e, à semelhança de outras empresas desse tipo – Facebook, Twitter, Alibaba e uma crescente lista de firmas cujo negócio é o grande volume de informações, como as de telecomunicações e as de pagamentos globais –, seus centros de dados requerem milhões de “servidores virtuais” que aumentam exponencialmente a capacidade de computação sem exigir expansão significativa do espaço físico, do resfriamento dos equipamentos ou da demanda de energia elétrica63. As empresas de hiperescala exploram o custo marginal da economia digital para rapidamente alcançar uma grande escala a custos quase nulos64.
> (p. 39-40)

# Ativos de vigilância fruto de operações automatizadas ubíquas. Capital de vigilância e capitalismo de vigilância.

> Esse exame da combinação feita por Varian entre dados, extração e análise sugere algumas características-chave da nova lógica de acumulação associada ao _big data_, encabeçada pela Google. Em primeiro lugar, as receitas dependem de ativos de dados apropriados por meio de ubíquas operações automatizadas. Essas operações constituem uma nova classe de ativos: os _ativos de vigilância_. Os críticos do capitalismo de vigilância podem caracterizar tais ativos como “bens roubados” ou “contrabando” na medida em que foram tomados, em vez de fornecidos, e não produzem, como argumentarei a seguir, as devidas reciprocidades. A apreciada cultura da produção social na esfera individual em rede apoia-se nas próprias ferramentas que são agora os principais veículos para a apropriação baseada em vigilância das _data exhausts_ mais lucrativas. Esses ativos de vigilância atraem investimentos significativos que podem ser chamados de _capital de vigilância_. A Google, até agora, triunfou no mundo em rede através da construção pioneira dessa nova forma de mercado, que é uma variante radicalmente descolada e extravagante do capitalismo de informação, que identifico como _capitalismo de vigilância_. Rapidamente se tornou o modelo-padrão de negócios na maioria das empresas e _startups_, em que as rotineiras estimativas de valor dependem de “olhos”, mais do que de receita, para prever a remuneração dos ativos de vigilância.
> (p. 40-41)

# Maior observabilidade (vigilância) sobre transações mediadas por computador: novos modelos de negócio (ex: controle sobre o usuário de um produto)

> De acordo com Varian: “Como as transações são agora mediadas pelo computador, podemos observar comportamentos que anteriormente não eram observáveis e redigir contratos sobre esses comportamentos. Isso permite transações que simplesmente não eram viáveis antes [...]. As transações mediadas pelo computador permitiram novos modelos de negócio [...]”65. Ele oferece alguns exemplos: se alguém parar de pagar as parcelas mensais do carro, o credor pode “instruir o sistema de monitoramento veicular a não permitir que o veículo seja iniciado e sinalizar o local onde ele pode ser retirado”. As companhias de seguros, ele sugere, podem contar com sistemas de monitoramento similares para verificar se os clientes estão dirigindo com segurança e, assim, determinar se devem ou não manter o seguro dele ou lhe pagar o prêmio da apólice. Ele também sugere que se podem contratar agentes locais remotos para executar tarefas e usar dados de seus _smartphones_ – geolocalização, marcação de horário, fotos – para “provar” que eles realmente realizaram suas atividades conforme previsto no contrato.
> (p. 41)

# Utopia contratual em Williamson: eliminar a incerteza e permitir a expressão de uma "racionalidade ilimitada"

> Varian parece apontar para o que Oliver Williamson chama de “condição de utopia contratual”66. Na economia de transação de William, os contratos existem para mitigar a inevitabilidade da incerteza. Eles operam para economizar em “racionalidade limitada” e salvaguardar contra o “oportunismo” – ambas condições intratáveis dos contratos no mundo real da atividade humana. Ele observa que a certeza exige “racionalidade ilimitada” derivada de “competência cognitiva irrestrita”, que, por sua vez, deriva de adaptações “completamente descritas” a eventos contingentes “publicamente observáveis”. Williamson observa que essas condições são inerentes a “um mundo de planejamento”, e não ao “mundo da governança”, no qual, “outras coisas sendo iguais [...], as relações que se caracterizem pela confiança pessoal sobreviverão a um maior estresse e mostrarão maior adaptabilidade”67.
> (p. 41-2)

# Contratos mediados por computador em Varian como "processos de máquinas" (descolado do social). Dimensão espiritual do poder -> dimensão material do poder (disciplina e controle).

> A visão de Varian dos usos de transações mediadas por computador retira a incerteza do contrato, assim como a necessidade e a própria possibilidade de se desenvolver a confiança. Outra maneira de dizer isso é que os contratos são descolados do social e repensados como processos de máquinas.
> (p. 42)

> No sistema econômico de Varian, a autoridade é suplantada pela técnica, o que eu chamo de **“dimensão material do poder”, em que sistemas impessoais de disciplina e controle produzem certo conhecimento do comportamento humano independentemente do consentimento**.
> (p. 42)

# Exposição sobre contratualismo, liberdade e coerção (Locke, Durkheim, Weber, Arendt)

(p. 42-3)

# Big Other como regime institucional distribuido de registro, modificação e mercantilização da experiência quotidiana visando o lucro

>  O revigoramento humano, a partir das falhas e triunfos das afirmações da previsibilidade e do exercício da vontade em face da incerteza natural, dá lugar ao vazio da submissão perpétua. Em vez de permitir novas formas contratuais, esses arranjos descrevem o surgimento de uma nova arquitetura universal que existe em algum lugar entre a natureza e Deus, batizada por mim de _Big Other_. Essa nova arquitetura configura-se como **um ubíquo regime institucional em rede que registra, modifica e mercantiliza a experiência cotidiana, desde o uso de um eletrodoméstico até seus próprios corpos, da comunicação ao pensamento, tudo com vista a estabelecer novos caminhos para a monetização e o lucro**.
> (p. 43-4)

# Big Other como regime do planejamento (preempção?) que se contraporia a um regme da governança e dos contratos ("democracia de mercado")

> O _Big Other_ é o poder soberano de um futuro próximo que aniquila a liberdade alcançada pelo Estado de direito. É um novo regime de fatos independentes e independentemente controlados que suplanta a necessidade de contratos, de governança e o dinamismo de uma democracia de mercado.
> (p. 44)

# Big Other ~ ordem ampliada (Hayek)

> Os processos institucionais que constituem a arquitetura do Big Other podem ser imaginados como a instanciação material da “ordem ampliada” de Hayek, que ganha vida na transparência didática da mediação por computador.
> (p. 44)

# Poder distribuído e onipresente no Big Other (em oposição ao panóptico)

~ sociedades de controle (Deleuze). Neste, não há a visão fatalista sobre a inescapabilidade ante ao controle. Toda forma de captura cria novas condições de fuga.

> Esses processos reconfiguram a estrutura de poder, conformidade e resistência herdada da sociedade de massa e simbolizada durante mais de meio século pelo _Big Brother_. **O poder não pode mais ser resumido por esse símbolo totalitário de comando e controle centralizado**. Mesmo o panóptico do projeto de Bentham, que usei como metáfora central em meu trabalho anterior72, é prosaico em comparação com essa nova arquitetura. O panóptico era um projeto físico que privilegiava um único ponto de observação. A conformidade antecipada que ele induzia exigia a produção de comportamentos específicos em quem estivesse dentro do panóptico, mas esse comportamento poderia ser deixado de lado uma vez que a pessoa abandonasse esse lugar físico. Na década de 1980, o panóptico constituiu-se como uma metáfora adequada para os espaços hierárquicos do local de trabalho. **Em um mundo organizado segundo os pressupostos de Varian, os _habitat_ dentro e fora do corpo humano estão saturados de dados e produzem oportunidades radicalmente distribuídas para observação, interpretação, comunicação, influência, predição e, em última instância, modificação da totalidade da ação**. Ao contrário do poder centralizado da sociedade de massa, não existe escapatória em relação ao _Big Other_. Não há lugar para estar onde o Outro também não está.
> (p. 44)

# Automatização da conformidade antecipatória (?). Experiência do real baseada em estímulo-resposta. Desaparecimento da conformidade (invisibilidade da normatividade algoritmica).

~Governamentalidade algoritmica

> Nesse mundo do qual não existe fuga, os efeitos arrepiantes da conformidade antecipatória73 cedem à medida que a agência mental e o autodomínio da antecipação são gradualmente submersos em um novo tipo de automatização. A conformidade antecipatória assume um ponto de origem na consciência a partir do qual é feita a escolha de se conformar, com o objetivo de evitar sanções e de camuflagem social. Isso também implica uma diferença, ou pelo menos a possibilidade de uma diferença, entre o comportamento que se deveria ter performado e o comportamento que se escolhe performar como uma solução instrumental contra o poder invasivo. **No mundo do _Big Other_, sem rotas de fuga, a agência implicada no trabalho de antecipação é gradualmente mergulhada em um novo tipo de automaticidade – uma experiência real baseada puramente em estímulo-resposta**. A conformidade não é mais um ato típico, como no século XX, de submissão em relação à massa ou ao grupo, não é mais a perda de si próprio para o coletivo produzida pelo medo ou pela compulsão, nem é mais o desejo psicológico de aceitação e pertencimento. **A conformidade agora desaparece na ordem mecânica de coisas e de corpos, não como ação, mas como resultado, não como causa, mas como efeito. Cada um de nós pode seguir um caminho distinto, mas esse caminho já é moldado pelos interesses financeiros e/ou ideológicos, que imbuem o Big Other e invadem todos os aspectos da “vida privada” de cada um**. A falsa consciência já não é produzida pelos fatos escondidos da classe e sua relação com a produção, mas pelos fatos ocultos da modificação do comportamento mercantilizada. **Se o poder já foi uma vez identificado com a propriedade dos meios de produção, agora ele é identificado com a propriedade dos meios de modificação comportamental [[ mereceria uma análise mais extensa na medida que meios de modificação de comportamento são possíveis segundo estruturas materiais que podem ser vistas como produto de meios de produção ]]**.
> (p. 45)

> 73: Ver minha discussão sobre conformidade antecipatória em S. Zuboff, In the age of the smart machine, cit., p. 346-56. Para um debate mais atualizado sobre o tema, ver pesquisa recente sobre comportamento e buscas na internet feita por A. Marthews e C. Tucker, Government surveillance and internet search behavior (Cambridge, Digital Fourth, 2014); disponível em: <http://www.ssrn.com/abstract=2412564>, acesso em: 18 jul. 2018.
> (p. 45-6)

# Aproximações entre psicologia comportamental (vórtice de estímulos) e neoliberalismo (ordem ampliada). Varian adiciona a noção de previsibilidade, aumentando o reducioismo do humano ao estatuto de "mero animal".

> Na verdade, há pouca diferença entre a inefável “ordem ampliada” do ideal neoliberal e o “vórtice de estímulos” responsável por toda a ação na visão dos teóricos clássicos da psicologia comportamental. Nas duas visões de mundo, a autonomia humana é irrelevante e a experiência vivida da autodeterminação psicológica é uma ilusão cruel. Varian acrescenta uma nova dimensão a ambos os ideais hegemônicos fazendo com que agora essa “visão de Deus” possa ser totalmente explicada, especificada e conhecida, eliminando toda a incerteza. O resultado é que as pessoas são reduzidas a uma mera condição animal, inclinadas a servir às novas leis do capital impostas a todos os comportamentos por meio da alimentação implacável de registros ubíquos em tempo real, baseados em fatos de todas as coisas e criaturas.
> (p. 45-6)

# Hanna Arendt sobre o comportamentalismo como realidade produzida

> Hannah Arendt tratou esses temas décadas atrás com uma clareza notável enquanto lamentava a transferência da nossa concepção de “pensamento” a algo que seria realizado por um “cérebro” e, portanto, possível de ser transferido para “instrumentos eletrônicos”:
>
> > A última etapa da sociedade do trabalho, a sociedade dos empregados, exige dos seus membros um completo funcionamento automático, como se a vida individual tivesse sido realmente mergulhada no ciclo vital da espécie e a única decisão ativa ainda necessária do indivíduo fosse largar tudo, por assim dizer, abandonar sua individualidade, a dor individualmente sentida e o problema de viver, e concordar com um comportamento funcional atordoado e “tranquilo”. **O problema com as teorias modernas do behaviorismo não é que elas estejam erradas, mas que elas possam se tornar verdadeiras, que elas sejam a melhor conceitualização possível de certas tendências óbvias na sociedade moderna**. É bem concebível que a era moderna – que começou com um surto promissor e sem precedentes de atividade humana – possa acabar na mais letal e estéril passividade que a história já conheceu.74
>
> (p. 46)

# Privacidade como liberdade (individual) de escolha sobre publicidade ou sigilo

> O sigilo é um efeito da privacidade, que é sua causa. Exercitar o direito à privacidade produz escolha, e uma pessoa escolhe manter algo sigiloso ou compartilhá-lo. Os direitos de privacidade conferem, assim, direitos de decisão; a privacidade permite uma decisão sobre onde se quer estar no espectro entre sigilo e transparência em cada situação.
> (p. 47)

# Redistribuição dos direitos de privacidade no capitalismo de vigilância (empresas possuem plenos direitos em oposição aos usuários)

> O trabalho da vigilância, ao que parece, não é corroer os direitos de privacidade, mas sim redistribuí-los. Em vez de um grande número de pessoas possuindo alguns direitos de privacidade, esses direitos foram concentrados no interior do regime de vigilância. Os capitalistas de vigilância possuem amplos direitos de privacidade e, portanto, muitas oportunidades para segredos. Estes são cada vez mais utilizados para privar as populações de escolha no que diz respeito a que partes de sua vida desejam manter em sigilo. Essa concentração de direitos é efetivada de duas maneiras. No caso da Google, do Facebook e de outros exemplos de capitalistas de vigilância, muitos dos seus direitos parecem vir do ato de tomar os direitos de outros sem consentimento, em conformidade com o modelo do Street View. Os capitalistas de vigilância exploraram de forma hábil um lapso na evolução social, uma vez que o rápido desenvolvimento de suas habilidades de vigiar para o lucro em muito suplantou a compreensão pública e o eventual desenvolvimento de leis e regulamentações legais. Como resultado, os direitos à privacidade, uma vez acumulados e afirmados, podem então ser invocados como legitimação para manter a obscuridade das operações de vigilância78.
> (p. 47-8)

# Acumulação de direitos no capitalismo de vigilância. Hiperescala como fator antidemocrático ("autoridade absolutista pré-moderna").

> Esses argumentos sugerem que a lógica da acumulação que sustenta o capitalismo de vigilância não é totalmente capturada pelo campo institucional convencional da empresa privada. Acumulam-se não apenas capital e ativos de vigilância mas também direitos. Isso ocorre mediante um agenciamento único de processos de negócios, que opera fora dos auspícios de mecanismos democráticos legítimos ou das tradicionais pressões do mercado, de reciprocidade e escolha do consumidor. Essa acumulação é obtida por meio de uma declaração unilateral que se parece mais com as relações sociais de uma autoridade absolutista pré-moderna. No contexto dessa nova forma de mercado que eu chamo de capitalismo de vigilância, a hiperescala se torna uma ameaça profundamente antidemocrática.
> (p. 49)

# Big Other como _coup de gens_: redistribuição autoritária e unilateral de direitos

> O capitalismo de vigilância, portanto, se qualifica como uma nova lógica de acumulação, com uma nova política e relações sociais que substituem os contratos, o Estado de direito e a confiança social pela soberania do _Big Other_. Ele impõe um regime de conformidade baseado em recompensas e punições e administrado privadamente, sustentado por uma redistribuição unilateral de direitos. O _Big Other_ existe na ausência de uma autoridade legítima e é em grande parte livre de detecção ou de sanções. Neste sentido, o Big Other pode ser descrito como um golpe automatizado de cima: não um _coup d’État_, mas sim um _coup des gens_*.
> (p. 49)

# Responder o que você deseja saber antes de perguntar (modulação): Google Now (assistente de voz para android)

> Varian sustenta que, “hoje em dia, as pessoas esperam resultados de pesquisa e anúncios personalizados”. Ele diz que a Google quer fazer ainda mais. Em vez de você precisar fazer perguntas, a Google deve “saber o que você deseja e lhe dizer antes que a pergunta seja feita”. Essa visão, afirma, “já foi realizada pelo Google Now”.
> (p. 49)

# Assimetrias entre o que capitalistas de vigilância sabem e o que o usuário sabe

> Por exemplo, a Google sabe muito mais sobre sua população de usuários do que estes sabem sobre si mesmos. De fato, não há meios pelos quais as populações possam atravessar essa divisão, dados os obstáculos materiais, intelectuais e proprietários necessários para a análise de dados e a ausência de feedback loops. Outra assimetria assenta no fato de que o usuário típico tem pouco ou nenhum conhecimento sobre as operações comerciais da Google, sobre a ampla gama de dados pessoais com que contribui para os servidores da Google ou sobre a retenção desses dados ou, ainda, como eles são instrumentalizados e monetizados. Já é bem sabido que os usuários têm poucas opções significativas para a autogestão de privacidade82. O capitalismo de vigilância prospera na ignorância do público.
> (p. 50)

# Assimetria de poder assentada na dependência do usuário. Modulações algorítmicas como pecado ("fruto proibido")

> Essas assimetrias no conhecimento são sustentadas por assimetrias de poder. **O _Big Other_ é institucionalizado nas funções automáticas indetectáveis de uma infraestrutura global que é considerada pela maioria das pessoas como essencial para a participação social básica**. As ferramentas oferecidas pela Google e outras empresas capitalistas de vigilância respondem às necessidades dos indivíduos sitiados da segunda modernidade – e, assim como o fruto proibido, uma vez que são experimentadas, torna-se impossível viver sem elas. **Quando o Facebook ficou fora do ar em cidades dos Estados Unidos durante algumas horas no verão de 2014, muitos estadunidenses chamaram seus serviços de emergência locais no 911.
> (p. 50-1)

> 83: “911 calls about Facebook outage angers L. A. County sheriff’s officials”, Los Angeles Times, 1o ago. 2014; disponível em: <http://www.latimes.com/local/lanow/la-meln-911-calls-about-facebook-outage-angers-la-sheriffs-officials-20140801-htmlstory. html>, acesso em 18 jul. 2018.
> (p. 51)

# Ferramentas da Google como iscas, como pacto com o diabo (Fausto) (mas um pacto ilegítimo)

> As ferramentas da Google não são objeto de valor de troca. Elas não estabelecem reciprocidades construtivas entre produtores e consumidores. Em vez disso, são as “iscas” que atraem os usuários para as operações extrativistas e transformam a vida comum na renovação diária de um pacto faustiano do século XXI. **Essa dependência social está no cerne do projeto de vigilância. Necessidades fortemente sentidas como essenciais para uma vida mais eficaz se opõem à inclinação para resistir ao projeto de vigilância**. Esse conflito produz uma espécie de entorpecimento psíquico que habitua as pessoas à realidade de serem rastreadas, analisadas, mineradas e modificadas – ou as predispõe a racionalizar a situação com cinismo resignado.
> (p. 51)

> Historicamente, essa poderosa característica evolutiva da demanda levou à expansão da produção e dos empregos, a salários mais altos e a bens de menor custo. Essas reciprocidades não estão mais no horizonte de Varian. Em vez disso, ele considera tal mecanismo de crescimento da demanda a força inevitável que empurrará as pessoas comuns para o pacto faustiano do Google Now de “necessidades” em troca de ativos de vigilância. 
> (p. 52)

# Possibilidades de resistência: legislação nos EUA e Europa, atitudes individuais conscientes (o papel de Assange e Snowden)

> Um crescente conjunto de evidências sugere que pessoas em muitos países podem vir a resistir ao coup des gens, já que a confiança nos capitalistas de vigilância se esvazia na medida em que eclodem novos fatos que indicam o impiedoso panorama da sociedade futura descrito por Varian. Essas questões são agora objeto de debate político sério na Alemanha e na União Europeia, onde propostas para “desmembrar” a Google já estão sendo discutidas91. Uma pesquisa recente do Financial Times indica que tanto os europeus quanto os estadunidenses estão alterando substancialmente seu comportamento online à medida que buscam mais privacidade92. Um grupo de pesquisadores por trás de um grande estudo do comportamento online entre jovens concluiu que a “falta de conhecimento” – e não uma “atitude espontânea em relação à privacidade”, como alegaram os líderes das empresas de tecnologia – é uma razão importante pela qual um grande número de jovens “se envolve com o mundo digital de maneira aparentemente despreocupada”93. Novos estudos jurídicos revelam danos ao consumidor provocados pela sua perda de privacidade associada à Google e ao capitalismo de vigilância94. O fundador do WikiLeaks, Julian Assange, publicou um relato sobre a liderança, a política e as ambições globais da Google95. O último relatório do PEW Research Center sobre as percepções públicas da privacidade na era pós-Snowden indica que 91% dos adultos dos Estados Unidos concordam ou concordam fortemente que os consumidores perderam controle sobre seus dados pessoais, enquanto apenas 55% concordam ou concordam fortemente estar dispostos a “compartilhar algumas informações sobre si com as empresas no intuito de usar gratuitamente os serviços online”96.
> (p. 53-4)

# Experimentos de massa como comeplementariedade necessária às correlações de _big data_ para que se entenda a causalidade

> Como a análise de _big data_ produz apenas padrões correlacionais, Varian anuncia a necessidade de experimentos contínuos que possam trazer à tona questões de causalidade. Tais experiências são fáceis de fazer na _web_, “atribuindo grupos de tratamento e de controle com base no tráfego, cookies, nomes de usuários, áreas geográficas, e assim por diante”97. A Google tem tido tanto sucesso na experimentação que compartilhou suas técnicas com anunciantes e produtores de conteúdo. O Facebook também fez incursões nessa área, conduzindo experimentos de modificação no comportamento dos usuários com a finalidade de monetizar seu conhecimento, sua capacidade preditiva e seu controle. Sempre que foram reveladas, no entanto, essas experiências acenderam um intenso debate público98. 
> (p. 54)

# Ativos de vigilância simultaneamente de comportamentos virtuais e reais (online e offline?)

> O entusiasmo de Varian pela experimentação toca em um assunto maior, no entanto. As oportunidades de negócios associadas aos novos fluxos de dados implicam um deslocamento da análise _a posteriori_ a que Constantiou e Kallinikos99 se referem, **em direção à observação, à comunicação, à análise, à previsão e à modificação em tempo real do comportamento atual e futuro**. **Isso implica outra mudança na fonte dos ativos de vigilância, do comportamento virtual para o comportamento real, enquanto as oportunidades de monetização são reorientadas para combinar o comportamento virtual com o real**. **Essa é uma nova fronteira de negócios composta do conhecimento sobre o comportamento em tempo real, que cria oportunidades para intervir nesse comportamento e modificá-lo objetivando o lucro**. 
> (p. 55)

# Google e NSA como principais agentes de mineração de dados, padrões de análise e análise preditiva. Negócio da realidade.

Negócio da realidade como transações envolvendo a modulação de ações concretas? Em que sentido isso se aproxima/distancia da noção de colonização do virtual (em oposição ao atual) em Laymert?

> As duas entidades na vanguarda dessa nova onda de “mineração de realidade”, “padrões de análise de vida” e “análise preditiva” são o Google e a NSA. Como diz o relatório da Casa Branca, “existe um potencial crescente para a análise de _big data_ ter um efeito imediato no ambiente em torno de uma pessoa ou nas decisões feitas sobre sua vida”101. Isso é o que chamo de _negócio da realidade_ e reflete uma evolução na fronteira da ciência de dados, indo da mineração de dados para a mineração da realidade, na qual, de acordo com Sandy Pentland, do MIT (Massachusetts Institute of Technology [Instituto de Tecnologia de Massachusetts]), sensores, telefones celulares e outros dispositivos de captura de dados fornecem os “olhos e ouvidos” de um “organismo vivo global” a partir de um “ponto de vista de Deus”102.
> (p. 55)
>
# Expansão do texto eletrônico para a "ordem ampliada" global

> Essa é mais uma representação da “ordem ampliada”, totalmente explicada pela mediação por computador. O texto eletrônico do local de trabalho informatizado se transformou em um organismo vivo global – um “ponto de vista de Deus” que é interoperacional, transformador de comportamento, criador de mercados e tem direitos de propriedade.
> (p. 55-6)

# Ficções em Karl Polanyi (vida->trabalho, natureza->terra, troca->dinheiro)

Proximidade com os aparelhos de captura descritos por D&G em Mil Platôs: território->terra (renda de terra), atividade->trabalho/sobretrabalho (lucro), troca->dinheiro (imposto)

> O historiador Karl Polanyi observou, há quase setenta anos, que as economias de mercado dos séculos XIX e XX dependiam de três invenções mentais surpreendentes que ele chamava de “ficções”. A primeira era que a vida humana pode ser subordinada à dinâmica do mercado e renascer como “trabalho”. Em segundo lugar, que a natureza, subordinada também à ordem de mercado, renasce como “terra”. Em terceiro lugar, a troca que renasce como “dinheiro”. A própria possibilidade do capitalismo industrial dependia da criação dessas três “mercadorias fictícias” críticas. Vida, natureza e troca foram transformadas em coisas, para que pudessem ser lucrativamente compradas e vendidas. “[A] ficção da mercadoria”, ele escreveu, “menosprezou o fato de que deixar o destino do solo e das pessoas por conta do mercado seria o mesmo que aniquilá-los.”
> (p. 56)

# Quarta mercadoria fictícia no capitalismo de vigilância: realidade->comportamento

Pensando com D&G podemos propor um quarto aparelho de captura de Estado: informação->dados (modulação do comportamento)

> Com a nova lógica de acumulação do capitalismo de vigilância, uma quarta mercadoria fictícia emerge como característica dominante da dinâmica do mercado no século XXI. A própria realidade está passando pelo mesmo tipo de metamorfose fictícia por que passaram as pessoas, a natureza e a troca. **A “realidade” é agora subjugada à mercantilização e à monetização e renasce como “comportamento”**. Os dados sobre os comportamentos dos corpos, das mentes e das coisas ocupam importante lugar em uma dinâmica compilação universal em tempo real de objetos inteligentes no interior de um domínio global infinito de coisas conectadas. **Esse novo fenômeno cria a possibilidade de modificar os comportamentos das pessoas e das coisas tendo por objetivo o lucro e o controle. Na lógica do capitalismo de vigilância, não há indivíduos, apenas o organismo mundial e todos os elementos mais ínfimos em seu interior.** 
> (p. 56)

# Sobre a existência de funcionalidades inerentes a uma tecnologia; sua submissão às lógicas institucionais; o hacker como liberador desse entrave institucional às funcionalidades inerentes a uma tecnologia

> As tecnologias são constituídas por funcionalidades específicas, mas o desenvolvimento e a expressão dessas funcionalidades são moldados pelas lógicas institucionais nas quais as tecnologias são projetadas, implementadas e usadas. Essa é, afinal, a origem do _hacker_. O hackeamento pretende liberar funcionalidades das lógicas institucionais em que estão congeladas e redistribuí-las em configurações alternativas para novos fins. Na esfera do mercado, essas lógicas circunscritas são lógicas de acumulação.
> (p. 56-7)

# Objetivo: identificar e identificar a atual lógica de acumulação baseado em agenciamentos em hiperescala de dados objetivos e subjetivos visando conhecer, controlar e modificar comportamentos

> Com essa visão em mente, meu objetivo foi começar a identificar e teorizar **a lógica de acumulação atualmente institucionalizada que produz agenciamentos em hiperescala de dados objetivos e subjetivos sobre indivíduos e seus habitat no intuito de conhecer, controlar e modificar comportamentos para produzir novas variedades de mercantilização, monetização e controle**.
> (p. 57)

# Google como lider de práticas que passam da análise do comportamento "virtual" passado para a modificação do comportamento "real" futuro. Comportamentalismo através de publicidades.

~psicologia comportamental

onde exatamente entra o papel central das publicidades?

> O desenvolvimento da internet e de métodos para acessar a world wide web disseminaram a mediação por computador, antes restrita a locais de trabalho delimitados e ações especializadas, para a ubiquidade global tanto na interface institucional quanto nas esferas íntimas da experiência cotidiana. As empresas de alta tecnologia, **lideradas pela Google**, perceberam novas oportunidades de lucro nesses fatos. **A Google compreendeu que capturar cada vez mais desses dados, armazená-los e analisá-los lhe daria o poder de afetar substancialmente o valor da publicidade**. As capacidades da Google nessa arena, tendo desenvolvido e atraído lucros históricos, levaram à **produção de práticas sucessivamente ambiciosas que expandiram a lente dos dados do comportamento virtual passado para o comportamento real futuro. Novas oportunidades de monetização estão assim associadas a uma nova arquitetura global de captura e análise de dados que produz recompensas e punições destinadas a modificar e transformar em mercadoria o comportamento visando à obtenção de lucro**.
> (p. 57)

# Análise do porque da permanencia dessas práticas assimétricas institucionalizadas: invisibilidade e dependência -> inevitabilidade

> Esses novos fatos institucionais se mantiveram por diversos motivos: foram construídos muito rapidamente e projetados para serem indetectáveis; fora de um domínio estreito de especialistas, poucas pessoas entenderam seu significado; **assimetrias estruturais de conhecimento e direitos tornaram impossível que as pessoas tomassem conhecimento dessas práticas**; as principais empresas de tecnologia foram respeitadas e tratadas como emissários do futuro; nada na experiência passada havia preparado as pessoas para essas novas práticas, havendo, portanto, escassez de barreiras para que se protegessem; **os indivíduos rapidamente passaram a depender das novas ferramentas de informação e comunicação como recursos necessários na luta cada vez mais estressante, competitiva e estratificada para uma vida mais eficaz; as novas ferramentas, redes, aplicativos, plataformas e mídias tornaram-se requisitos para a participação social**. E, finalmente, o rápido acúmulo de fatos institucionalizados – _data brokers_, análise de dados, mineração de dados, especializações profissionais, fluxos de caixa inimagináveis, poderosos efeitos de rede, colaboração estatal, recursos materiais de hiperescala e concentrações sem precedentes de poder de informação – produziu uma **sensação esmagadora de inevitabilidade**.
> (p. 57-8)

# Capitalismo de vigilância como nova lógica de acumulação (extração e controle) institucionalizada baseada no texto eletrônico generalizado (na forma de um organismo global inteligente, o _Big Other_)

> Esses desenvolvimentos tornaram-se a base para uma nova lógica de acumulação totalmente institucionalizada que chamo de capitalismo de vigilância. Nesse novo regime, a arquitetura global da mediação por computador transforma o texto eletrônico, anteriormente delimitado dentro das organizações, em um organismo global inteligente que chamo de _Big Other_. Novas possibilidades de subjugação são produzidas à medida que essa lógica institucional inovadora prospera em mecanismos inesperados e ilegíveis de extração e controle que exilam as pessoas de seus próprios comportamentos.
> (p. 58)

# Divisão de aprendizagem como questão civilizacional

> Sob essas condições, a divisão de aprendizagem e as disputas ao redor dela passam a ter âmbito civilizacional. Para a pergunta “quem participa?” a resposta é “aqueles com os recursos materiais, de conhecimento e financeiros para acessar o _Big Other_”.
> (p. 58)

# Os mercados de controle comportamental

> Para a pergunta “quem decide?” a resposta é que o acesso ao _Big Other_ é decidido por novos mercados na mercantilização do comportamento: os mercados de controle comportamental. Estes são compostos daqueles que vendem oportunidades de influenciar comportamentos para obter lucro e daqueles que compram tais oportunidades. 
> (p. 58)

# Exemplos do funcionamento dessa nova lógica. Aproximações com o exemplo do cartão dividual em Guattari citado por Deleuze e com a noção de governamentalidade algorítmica

> Assim, a Google, por exemplo, pode vender acesso a uma companhia de seguros e essa empresa compra o direito de intervir, mediante um circuito de informações, em seu carro ou em sua cozinha para aumentar suas receitas ou reduzir seus custos. Pode desligar o seu carro porque você está dirigindo muito rápido. Pode bloquear o seu refrigerador quando aumentar seu risco de desenvolver uma doença cardíaca ou diabetes tomando muito sorvete. Você poderá, então, enfrentar a perspectiva de prêmios mais altos ou da perda de cobertura do seguro.
> (p. 58)

# Substituição do Estado e dos contratos (baseado na confiança) por uma lógica da preempção (baseada no comportamentalismo: recompensas, punições, estímulos e respostas)

> A renderização da civilização da informação pela Google substitui o Estado de direito e a necessidade da confiança social como base para as comunidades humanas por um novo “mundo da vida” de recompensas e punições, estímulos e respostas. 
> (p. 59)

# Distanciamento do social, capitalismo de vigilância como autoritarismo

> Nesse novo modelo, as populações são alvo de extração de dados. Esse descolamento radical do social é outro aspecto do caráter antidemocrático do capitalismo de vigilância. Sob o capitalismo de vigilância, a democracia não funciona mais como um meio para a prosperidade; na verdade, ela ameaça as receitas de vigilância
> (p. 59)

# Coparticipação do público e privado no capitalismo de vigilância

> Desde Edward Snowden, aprendemos sobre a confusão de fronteiras do público e do privado em atividades de vigilância, incluindo colaborações e interdependências construtivas entre as autoridades de segurança do Estado e empresas de alta tecnologia
> (p. 59)
