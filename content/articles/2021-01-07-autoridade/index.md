---
path: "/autoridade"
date: 2021-01-17
title: Autoridade, liberdade e infância
author: Rafael Gonçalves
category: ensaio
featuredImage: "../../images/Banksy_hula2400.jpg"
srcInfo: Grafite por <a href="https://www.banksy.co.uk">Banksy</a>
published: true
tags: ["anarquismo", "educação", "infância", "Emma Goldman", "Elisée Reclus", "ensaio"]
---

> Tenhamos a firme resolução de fazer deles homens livres - nós, que ainda não temos da liberdade senão uma vaga esperança. - Elisée Reclus

Em qualquer sociedade há uma preocupação especial em relação às crianças, pois são estas que em breve tomarão nosso lugar na constituição daquela, sendo elas as principais responsáveis pelo rumo que o infindável processo de sociogênese tomará daqui em diante.

Assim, é natural que - como o Estado capitalista que, através da cultura, do sistema educacional, da grande mídia, etc. reproduz seus ideais visando se conservar - mães, pais e educadoraes ensinem as crianças conforme suas visões de mundo. A criança, esvaziada de subjetividade própria, vira suporte dos ideais, sejam eles conservadores ou revolucionários, de seus pais, de suas mães e demais criadoraes e educadoraes.

> Todas as instituições hoje, a família, o Estado, os códigos morais veem em uma personalidade forte, bonita e intransigente um inimigo mortal; portanto, dedicam-se a limitar as emoções humanas e a originalidade do pensamento em uma camisa de força desde a primeira infância; o a moldar todo ser humano de acordo com um padrão; não como uma individualidade completa, mas como um escravo paciente, um autômato profissional, um cidadão pagador de impostos ou um moralista honesto. [^1]

Mas o que quer da criança, o sujeito interessado numa sociedade livre? Não seria justamente o contrário disso: uma subjetividade autônoma? A criança deveria ser vista não como ser conservador ou revolucionário em potencial, mas sim como sujeito provido de pensamentos, experiências e emoções aqui e agora.

O anarquismo têm importância singular nesse debate, pois pensar uma sociedade livre de hierarquias é também questionar a posição, sempre inferior, em que se colocam as crianças. Dizer que o anarquismo se opõe às hierarquias ou ao autoritarismo, não é dizer que não se reconhece que pessoas diferentes possuem competências diferentes dado suas vivências singulares; mas sim, justamente ao pensar que cada indivíduo é singular e pode ser considerado autoridade em determinado assunto, rejeitar uma posição privilegiada *de um determinado grupo sobre outro, de algumas experiências sobre outras*. Negar o autoritarismo ao reconhecer diferentes autoridades, essa é a luta constante dos anarquistas, visando uma sociedade livre (e com pleno reconhecimento da diversidade do ser).

Desta forma, a criança não deve ser menor, estar abaixo, ou até mesmo ser vista como um estágio meramente anterior ao que ela vai se transformar. A criança tem suas potencialidades dado sua posição única no processo de subjetivação. E justamente por estar na posição em que está, uma de suas maiores potencialidades é a capacidade de ver o novo onde adultos aprenderam a enxergar resignadamente a norma, a capacidade de ver espaço para mudança, onde ninguém enxerga senão a mesmice do dia-a-dia. E não é de se esperar outra coisa de alguém menos invenenada de valores já cristalizados, pois "[a]s mudanças são a essência da vida, variações e a contínua inovação"[^2].

Assim, por mais que pais, mães e educadoraes tentem limitar a criança, existe uma parte dela que resiste e, não raro, a criança expressará sua insurgente e necessária vontade de liberdade e de mudança, contrariando as posições que lhe foram impostas:


> Mas a impressionante mente infantil percebe cedo que as vidas de seus pais estão em contradição com as ideias que representam; como o bom cristão que reza fervorozamente no domingo, mas continua quebrando os mandamentos do Senhor durante o resto da semanal, o pai radical denuncia a deus, o sacerdócio, a Igreja, o governo, a autoridade doméstica, mas continua agindo conforme a condição que abomina. [^3]

Pois "[a] criança alimentada com ideias unilaterais, prontas e fixas, logo se cansa de repetir as crenças de seus pais, e se lança em busca de novas sensações, por mais insignificantes e superficial que sejam, a mente humana não pode suportar a mesmice e a monotonia"[^4]. Encaremos isso como uma feliz evidência de que, não importa o quanto tentemos sufocar um indivíduo, sobretudo uma criança, sua liberdade sempre prevalecerá. Filhos conservadores de pais radicais "são a maior prova de que um espírito independente sempre resistirá a qualquer força externa e alheia exercida sobre os corações e mentes humanas."[^5]

Desta forma - de certa maneira aliviadaos de saber que não existe força grande o suficiente para esmagar a subjetividade e o desejo de liberdade da criança -, a única saída possível é abraçar a autonomia do outro e "insistir no livre crescimento e desenvolvimento das forças e tendências inatas da criança"[^6]. Sabendo que uma sociedade livre só é possível na medida que é constituída por uma totalidade de sujeitos livres, sejam elaes adultaos ou crianças.


[^1]: GOLDMAN, E. A criança e seus inimigos. **Educação**. Biblioteca Terra Livre. 2019, p. 83.

[^2]: Ibid., p. 85.

[^3]: Ibid., p. 89-90.

[^4]: Ibid., p. 90.

[^5]: Ibid., p. 91.

[^6]: Ibid., p. 91.
