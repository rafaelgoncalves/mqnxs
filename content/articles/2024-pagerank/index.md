---
title: O algoritmo PageRank da Google
tags:
    - Matteo Pasquinelli
    - pagerank
    - google
    - capitalismo cognitivo
    - algoritmo
    - intelecto geral
    - capitalismo
    - fichamento

date: 2024-06-30
category: fichamento
path: "/pagerank"
featuredImage: "pasquinelli_upscayl.png"
srcInfo: <a href="https://www.unive.it/data/people/27968562/curriculum">Matteo Pasquinelli</a>
published: true
---

Fichamento do texto "O algoritmo PageRank da Google: um diagrama do capitalismo cognitivo e a renda do intelecto comum"[^1] de Matteo Pasquinelli.

[^1]: PASQUINELLI, Matteo. Google’s PageRank Algorithm: A diagram of Cognitive Capitalism and the Rentier of the Common Intellect. **Deep Search**, , 2009. Disponível em: https://www.academia.edu/1992027/Googles_PageRank_Algorithm_A_diagram_of_Cognitive_Capitalism_and_the_Rentier_of_the_Common_Intellect. Acesso em: 28 fev. 2022. 

# Abstract

> **The origin of Google’s power and monopoly is to be traced to the invisible algorithm PageRank.** The _diagram_ of this technology is proposed here as the most fitting description of the _value machine_ at the core of what is diversely called knowledge economy, attention economy or cognitive capitalism. **This essay stresses the need of a political economy of the PageRank algorithm rather than expanding the dominant critique of Google’s monopoly based on the Panopticon model and similar ‘Big Brother’ issues (dataveillance, privacy, political censorship).** First and foremost **Google’s power is understood from the perspective of _value production_ (in different forms: attention value, cognitive value, network value, etc.): the biopolitical consequences of its data monopoly come logically later.**
> 
> This essay advances **three main arguments in relation to the ‘Google economy’ by focusing respectively: value _production_, value _accumulation_ and value _re-appropriation_.** First, **Google’s Page Rank is introduced as the best implementation of the _diagram_ of cognitive capitalism. This cognitive and economic diagram actually reverses the Panopticon diagram of Foucauldian lineage: it is not simply an apparatus of surveillance or control, but a machine to capture living time and living labour and to transform the _common intellect_ into _network value_.** Dataveillance is then made possible only thanks to a monopoly of data that are previously accumulated through the PageRank algorithm. Second, **this model of _cognitive hegemony_ needs a new theory of _cognitive rent_ to be understood, as it is based on the exploitation of a new mediascape for the collective intelligence that is only apparently free and open.** Google is defined as a _parasite_ of the digital datascape as, on one hand, it provides benevolent free services but, on the other hand, it accumulates value through a pervasive platform of web advertisement (Adsense and Adwords). More importantly, Google establishes its own proprietary hierarchy of value for each node of the internet and becomes then the first systematic _global rentier_ of the _common intellect_. Third, **a political response can be conceptualised and organised only by reversing the chain of value production (blatantly: ‘Reclaiming your page rank’) instead of indulging in a nominal resistance to the ‘digital Panopticon’.**

# Inversão do panóptico: Google como aparato de produção de valor a partir de baixo

> Besides true concerns, there is an abuse of a Foucauldian paradigm that highlights only one side of the problem, as Google’s power is not given as a metaphysical being but it is originated from its technological platform and business model. As Paolo Virno puts it, to really understand biopolitics we should begin from the potentiality of our living bodies and from labour power itself: biopolitical structures come later as an apparatus of capture of this potentiality.4 **The metaphor of the Panopticon must be reversed: Google is not simply an apparatus of dataveillance from above but an apparatus of value production from below. Specifically, Google produces and accumulates value through the PageRank algorithm and by rendering the collective knowledge into a proprietary scale of values — this is the core question.** The political economy of Google starts from the political economy of PageRank.
> (p. 4)

# PageRank

> The first description of Google’s PageRank was presented by Sergey Brin and Lawrence Page in their 1998 paper “The Anatomy of a Large-Scale Hypertextual Web Search Engine”.5 The PageRank algorithm introduced a revolutionary break in the Information Retrieval technologies and in the search engine panorama of the late 90s: **for the first time the apparently flat data ocean of the internet was shaped by Google in dynamic hierarchies according to the visibility and importance of each website. The ranking of a web page is quite intuitive to understand: this value is determined by the number and quality of incoming links. Particularly, a link coming from a node with a high rank has more value than a link coming from a node with a low rank.**
> (p. 4)

> While in the late ‘90s search engines like Yahoo were still hand-indexing the web and organising it according to the tree structure typical of encyclopaedic knowledge, Google provided a formula to trace a semantic value across a dynamic and chaotic hypertext. PageRank started to describe webpages according to their popularity and the search engine returned a hierarchy of results according to their rank. Apart from Yahoo’s trees and Google’s rankings, there are other techniques of Information Retrieval and new ones will be developed in the future.
> (p. 4)

[Penso aqui especificamente nos modelos de linguagem como o chatGPT]

![Métrica PageRank para os nós de uma rede simples, expressos em percentagens. (O Google usa uma escala logarítmica). O nó C tem um valor de PageRank mais elevado do que o nó E, apesar de existirem poucas ligações para C, a ligação para C vem de um nó importante e, portanto, tem um valor elevado. Se um utilizador começar num nó aleatório com uma probabilidade de 85% de escolher uma ligação aleatória a partir do nó que está a visitar no momento, e uma probabilidade de 15% de saltar para um nó escolhido aleatoriamente de toda a rede, esse utilizador vai chegar ao nó E 8,1% das vezes. (A probabilidade de 15% de saltar para um nó arbitrário corresponde a um fator de amortecimento de 85%). Sem amortecimento, qualquer utilizador acabariam nos nós A, B, ou C, e todos os outros teriam o valor zero para PageRank. Através da utilização do fator de amortecimento, o nó A está ligado a todos os nós da rede, mesmo que não tenha ligações para outros nós. Fonte: Wikipedia (PageRank).](pagerank-ex.png)

# PageRank como diagrama do capitalismo cognitivo a partir do hipertexto

> The PageRank diagram depicted above has no resemblance with the centralised structure of the Panopticon described by Foucault in Discipline and Punishment.8 **The liquid and hypertextual nature of the web (and more generally of the noosphere)** demands a further illustration. **A diagram of cognitive capitalism can be intuitively traced if — within the structure of a hypertext — each symmetrical link is replaced by an asymmetrical vector of energy, data, attention or value.** What PageRank unveils and measures is precisely this asymmetrical constitution of any hypertext and network.
> (p. 5)

# Inspiração no sistema de citações acadêmico

> The source of inspiration for PageRank was the academic citation system. The ‘value’ of an academic publication is notoriously calculated in a very mathematical way according to the number of citations that an article receives from other articles. Consequently, the general rank of an academic journal is the sum of all the incoming citations received by its articles.
> (p. 5)

# Condensação de atenção e desejo como valor de objetos cognitivos

> This bookish genealogy of PageRank should not be underestimated. **A similar way to describe value can be applied to any cognitive object and it is native also to the ‘society of the spectacle’ and its wild economy of brands.** In a spectacular regime **the value of a commodity is produced mainly by a condensation of attention and collective desire driven by mass media and advertisement.** From academic publications to commercial brands and the internet ranking itself equivalent processes of condensation of value can be assumed. **As the digital colonisation gave an online presence to any offline entity, this matrix of social and value relations migrated online and it become digitally traceable and measurable by search engines. PageRank specifically describes the attention value of any object to a such extent that it has become the most important source of visibility and authority even outside the digital sphere.** Eventually PageRank gives a formula of value accumulation that is hegemonic and compatible across different media domains: an effective diagram to describe the attention economy and the cognitive economy in general.
> (p. 5-6)

# Economia da atenção e PageRank

> **The notion of _attention economy_ is useful to describe how (part of) the value of a commodity is produced today via a media-driven accumulation of social desire and intelligence.** Regarding the constitution of this value, other schools of thought may refer to _cultural capital_ (Pierre Bourdieu), _collective symbolic capital_ (David Harvey) or _general intellect_ (especially in the tradition of post-Operaismo, with a more cognitive spin). Before the internet this process was described as a generic collective drive – after the internet, the structure of the network relations around a given object can be easily traced and measured. **PageRank is the first mathematical formula to calculate the attention value of each node in a complex network and the general attention capital of the whole network.** What is the nature of the value that is measured by PageRank? **More interestingly each link and vector of attention is not simply an instinctive gesture but a concretion of intelligence and often a conscious act. If it is fashionable to describe the network society as a conurbation of _desiring flows_, however those flows are dense of knowledge and belong also to the activity of a common intelligence.**
> (p. 6)

> In the introducing quote of this article, Nicholas Carr described very well how Google’s Page Rank works, how it feeds on our collective intelligence and how value is produced and accumulated starting from this common intellect.  PageRank establishes so its own attention economy, but a great part of this attention capital is more precisely built on intellect capital, as each link represents a concretion of intelligence. In this sense **Google is a parasitic apparatus of capture of the value produced by the common intelligence.**
> (p. 6)

> > At the heart of [Google] is the PageRank algorithm that Brin and Page wrote while they were graduate students at Stanford in the 1990. They saw that every time a person with a Web site links to another site, he is expressing a judgment. He is declaring that he considers the other site important. They further realized that while every link on the Web contains a little bit of human intelligence, all the links combined contain a great deal of intelligence – far more, in fact, that any individual mind could possibly posses. Google’s search engine mines that intelligence, link by link, and uses it to determine the importance of all the pages on the Web. The greater the number of link that lead to a site, the greater its value. As John Markoff puts it, Google’s software “systematically exploits human knowledge and decisions about what is significant”. Every time we write a link, or even click on one, we are feeding our intelligence into Google’s system. We are making the machine a little smarter – and Brin, Page, and all of Google’s shareholders a little richer.
>
> — Nicholas Carr, The Big Switch (2008)1
> (p. 3)

# Valor de rede (produzido pelo PageRank) e a Google como potência monopolista na economia de propagandas

> Can network theory exist without a notion of _network value_ — a notion of value specific to the network ecosystem and economy? Sounding the cognitive density of the internet, **PageRank unveils precisely a mechanism that is responsible for setting a rank value for each node of the web. This rank value set by Google is unofficially recognized as the currency of the global attention economy and crucially influences the online visibility of individuals and companies and subsequently their prestige and business.** This attention value is then transformed in monetary value in different ways. **If the PageRank algorithm occupies the inner core of Google’s hegemonic matrix, its revenues are coming from the advertisement platform Adwords that exploits this dominant position** (99% of revenues are derived from advertisement according to 2008 Annual Report). **The PageRank algorithm plus gigantic data centres (running 24-hour and constantly indexing the web) provide a monopolistic position for Google advertisement channels.**
> (p. 7)

# Google Adsense, Adword e Pagerank

> The way through which Google generates value deserves a more attentive analysis, as contrary to traditional mass media Google does not produce any content by itself. Specifically, Google captures millions of websites and users through its advertisements syndication program Adsense. **Google’s Adsense provides a light infrastructure for advertising that infiltrates each interstice of the web as a subtle and mono-dimensional parasite, extracting profit without producing any content. Money enters the cycle in Adwords and are then distributed through Adsense to single bloggers or web companies. Within the economy of the internet, both the traffic of a website and the redistribution of value is today extensively governed by PageRank.** PageRank is at the core of the attention economy of the internet as well as at the core of a general economy of prestige that affect many other domains controlled directly or indirectly by Google (take for instance academia and Google Scholar, music industries and Youtube, etc.  — many, for instance, are the cases of a symbiosis between the internet and the show business).
> (p. 7)

# Valor de rede como camada paralela ao valor de uso e de troca. Mais valor maquínico, de código e de fluxo em D&G.

> What PageRank identifies and measure is _network value_ in a very numeric form. If a commodity is described traditionally by _use-value_ and _exchange-value_, **_network-value_ is a further layer attached to the previous ones to describe its ‘social’ relations.** This term is ambiguous for many as it might simply point to a ‘value of networks’ (like in Benkler’s much-celebrated ‘wealth of networks’). To be more precise, a new notion of _network surplus-value_ should be advanced and articulated here. Indeed, **PageRank produces what Deleuze and Guattari described as a _machinic surplus-value_ referring to the surplus-value accumulated through the cybernetic domain, that is the transformation of a _surplus-value of code_ in a _surplusvalue of flux_.** Through PageRank, Google has not simply conquered a dominant position in the storage of web indexes, but also the monopoly of the production of this _network value_.
> (p. 8)

# Caráter assimétrico, ternário e vortical das relações no PageRank e no capitalismo cognitivo

> The diagram of PageRank underlines an important aspect about the relation between two nodes of a network. This relation is never purely symmetrical yet asymmetrical: **each link features indeed a one-way direction like an arrow, each link represents an exchange of desire, attention and knowledge that is never symmetrical.** This relation is never binary and equal, **but actually _ternary_, as there is always a third node influencing it and then an accumulation of value absorbed to another direction.** A network is never flat and horizontal. **The digital _ontology_ is always influenced by external values and material networks, by the analogue world of labour and life (that is the influence of the bio-political and bio-economic fields).** A network is never symmetrical and homogenous, **it is a topological surface rippled in molecular vortices.** Between the _vertical_ hierarchies of traditional knowledge and the so-much celebrated _horizontal_ networks of today’s knowledge production, this _vortical_ dimension shows how the two axes are always connected and how dynamic hierarchies keep on following us also onto the digital realm. **Google’s PageRank installed itself precisely on this movement that shapes the collective sphere of knowledge and the internet in molecular vortices of value.**
> (p. 8)

# Acumulação de valor de rede na fábrica imaterial da Google

> The previous paragraphs have tried to show how value is collectively produced within the digital networks and then captured by the _immaterial factory_ of Google.  Once the production of cognitive value has been introduced, it is important to clarify stages and modes of its accumulation. Google’s case study helps to illuminate the more general question about how cognitive capitalism extracts surplus-value and “makes money”. To understand today’s knowledge economy and the cultural industries, it is important eventually to distinguish different business models and possibly to visualise a machinic assemblage of different regimes of accumulation and not simply one typology.
> (p. 9)

# Duas perspectivas sobre a economia do conhecimento

> In a basic overview knowledge economy is currently described according to two dominant paradigms: on one side, **exploitation of intellectual property** and, on the other, **exploitation of cultural capital.** The definition of Creative Industries, for instance, stresses the “exploitation of intellectual property”, whereas the muchcelebrated ‘creative economy’ of Richard Florida is actually based on the exploitation of the general human capital of a given city. Similarly, Italian post-Operaismo has underlined the productive nature of the _general intellect_ of yesterday’s industrial workers and today’s metropolitan multitudes. In this reading, **the collective production of knowledge is constantly parasited by the corporations of cognitive capitalism, as once factories were extracting surplus-value from workers’ living labour.** On the opposite, approaches like Benkler’s notion of “social production” or Lessig’s “free culture” **celebrate a network-based production with no acknowledgment of the dimension of surplus-labour and surplus-value**. All these schools of thought should be confronted by the same question: **how is surplus-value extracted and accumulated within knowledge economy?**
> (p. 9)

# Renda cognitiva

> Monopolies of intellectual property are enough clear today. Music corporations are fighting precisely to defend this regime from the assault of digital networks. **This regime is described by many as a parasitic rent extracted on intellectual property or _cognitive rent_, as media corporation simply exploit the copyright of artworks that have virtually no costs of reproduction in the current technological regime.** Google itself functions on the basis of a strong monopoly, but it has no intellectual property to defend (aside from the PageRank patent!). So which sort of cognitive rent is embodied by Google? After reversing the Panopticon model, it is necessary also to reverse the common interpretations regarding network economy and the production of network value.
> (p. 10)

> In classical economic theory, rent is distinguished from profit. **Rent is the _parasitic_ income an owner can earn just by possessing an asset and it is traditionally associated with land property. Profit, on the other hand, is meant to be _productive_ and it is associated with the power of capital to generate and extract surplus-value (from commodities and the workforce).** Yet Vercellone criticises the idea of a “good productive capitalism” by highlighting the _becoming rent of profit_ as the characteristic trait of current knowledge and financial economy. Vercellone, accordingly, provides a slogan for cognitive capitalism: “rent is the new profit”.  Accordingly, **Google can be described as a global rentier that is exploiting the new lands of the internet with no need for strict enclosures and no need to produce content too.** In this picture, **Google appears as pure rent on the meta dimension of information that is accumulated through the digital networks. Google does not possess the information of the internet but the fastest diagram to access and measure the collective intelligence that has produced it.**
> (p. 10)

> The rent form is a more suitable model to describe the exploitation of the common intellect and the _common itself_ (if profit and wage are more related to an individual dimension and rent to a more collective and social dimension of production). Such a new theory of rent is useful to escape the impasse of the so-called new media criticism that is still incapable to identify the axes of production and exploitation along the digital domain. A taxonomy of the new forms of rent and new business models is necessary and urgent. **For Negri and Vercellone themselves, for example, the central axis of contemporary valorisation is the “expropriation of the common through the rent”.** According to them (as well as many others), this explains the ongoing pressure for a stronger intellectual property regime: **copyright is one of the strategic evolutions of rent to expropriate the cultural commons and reintroduce artificial scarcity.** Speculation then is directed toward intellectual property, forcing artificial costs on cognitive goods that can paradoxically be reproduced or copied virtually for free. However, the composite case of intellectual property must be further illuminated, **as rent may not necessarily arise simply from knowledge enclosures, but also from the exploitation of cognitive spaces that are completely new and virgin, as Google shows in relation to the internet.** The PageRank diagram seems to suggest a sort of differential rent along dynamic spaces that would deserve a further investigation.
> (p. 10-1)

# Resposta política à dominação da Google deve se basear na criação de sistemas alternativos de ordenamento

> **A consistent political response to Google’s neo-dominion should be based on an alternative ranking system able to undermine the monopoly of attention economy and also the accumulation of value controlled by Google.** Can such a monopolistic production of network value be reversed in some way? A first option would be to imagine a collective voluntary hand-made indexing of the web based on an open protocol (a sort of Wikipedia of network relations described under the FOAF ontology). However, Google cannot be challenged on the scale of its computing power: such a competition would be quite silly and primitive. On the other hand, **the idea of an _open source page rank_ algorithm would not address the issue of value accumulation and monopoly. By the way, the idea of an OpenRank algorithm has been rapidly abandoned. The fatal attraction of the masses for Google seems to rely more on its mystical power to set a _spectacular value_ for anything and anybody than on the precision of its results.** Rumours say that PageRank will be replaces soon by TrustRank, another algorithm developed by Stanford University and Yahoo researchers to separate useful webpages from spam and establish a sort of community trust or a new _cybernetic social pact_ across the internet. In such a scenario the everyday life and production of social networks will be integrated in an even deeper way.
> (p. 11-2)

# Acumulação primitiva de conhecimento e acumulação de mais valor de rede

> The battle against the accumulation of data operated by PageRank reminds the social struggles against the traditional forms of monopoly and accumulation of capitals. **PageRank is to the internet, as primitive accumulation and rent are to early capitalism.** If we refer to Marx’s _general intellect_, **we should imagine also an _original accumulation of knowledge_ at the source of the digital economy.** Anyhow, a critique of the present mode of networking cannot be established simply on the predictable narrative of the good networks against the evil monopolies. **A political response can be imagined only if the nature of the molecular _dispositif_ that produces the network value is understood.** PageRank and Google cannot be easily made more democratic. On the other side, interestingly, also **the new fashionable schools of peer-to-peer cooperation and internet-based “social production” will fail to represent a decent political proposal until they address the issue of production and accumulation of _network surplus-value_.
> (p. 12)
