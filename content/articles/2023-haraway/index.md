---
title: Donna Haraway
tags:
    - Donna Haraway
    - antropoceno
    - tecnociência
    - ciborgue
    - materialismo semiótico
    - mundificação
    - testemunho modesto
    - cama de gato
    - multiespécie
    - fichamento
    - entrevista

date: 2023-04-21
category: fichamento
path: "/entrevistas-donna-haraway"
featuredImage: "haraway.jpg"
srcInfo: <a href="https://commons.wikimedia.org/wiki/File:Donna_Haraway_and_Cayenne.jpg">Donna Haraway e Cayenne (2006)</a>
published: true
---

Fichamento das entrevistas _Quanto como uma folha_ (1997)[^1] e _Uma enorme e pretensiosa ninhada_ (2019)[^2] da bióloga, filósofa e feminista Donna Haraway.

[^1]: HARAWAY, D. J.; GOODEVE, T. N. Fragmentos: quanto como uma folha. entrevista com Donna Haraway. **Mediações - Revista de Ciências Sociais**, Londrina, v. 20, n. 1, p. 48–68, 2015. DOI: 10.5433/2176-6665.2015v20n1p48. Disponível em: <https://ojs.uel.br/revistas/uel/index.php/mediacoes/article/view/23252>.

[^2]: HARAWAY, D. Uma enorme e pretensiosa ninhada: Donna Haraway sobre verdade, tecnologia e resistência à extinção. **GARRAFA**. Vol. v. 20, n. 57 (2022): Janeiro - Junho. p. 20 – 40. Disponível em: <https://revistas.ufrj.br/index.php/garrafa/article/view/55723/pdf>

# Sobre a entrevista

> 1:  Fragmentos  publicados  originalmente  em _How like a leaf_:  an  interview  with  Thyrza  Nichols Goodeve / Donna J. Haraway. New York and London: Routeldge, 1999. A Comissão Editorial e as organizadoras  do  dossiê  agradecem  às  autoras,  Donna  Haraway  e  Thyrza  Goodeve,  e  à  editora Baldini  &  Castoldi  pela  autorização  para  traduzir  para  o  português  os  trechos  ora  publicados. Tradução de Pedro Peixoto Ferreira e André Favilla, revisão técnica de Daniela Tonelli Manica e Martha Ramírez-Gálvez.
> (2015, p. 48)

> Apresentamos  aqui  a  tradução  de  alguns  trechos  de  uma  entrevista conduzida  por  Thyrza  Nichols  Goodeve,  e  divulgada  inicialmente  no  catálogo _Fleshfactor do  festival  Ars  Electronica_  de  1997.  Embora  a  edição  inglesa  dessa entrevista, publicada no livro _How Like a Leaf_, já tenha completado quinze anos, a conversa condensa informações preciosas sobre o percurso pessoal-intelectual de Haraway, bem como alguns de seus principais pressupostos e o estilo peculiar com o qual contribui para uma apreensão feminista da tecnociência.
>
> O texto que se segue é uma compilação de partes do segundo, quarto e quinto capítulos dessa entrevista, intituladas respectivamente: Organicismo como teoria  crítica;  Mais  do  que  metáfora;  Prática  mundana;  Ele  não  nasceu  num jardim,  mas  certamente  nasceu  numa  história;  Quanto  como  uma  folha;  e Testemunha Modesta
>
> (2015, p. 48-9)

# Contribuições recentes sobre animais não-humanos e espécies companheiras (2003, 2008)

> 2: No período que se sucedeu à publicação desta entrevista, Donna Haraway publicou mais dois livros dedicados à discussão sobre (outros) animais e espécies companheiras: _The Companion Species Manifesto: Dogs, People, and Significant Otherness_,  Chicago:  Prickly  Paradigm  Press, 2003; e _When Species Meet_, Minnesota: University of Minnesota Press, 2008. Um dos capítulos deste último foi traduzido e publicado em: HARAWAY, Donna. A partilha do sofrimento: relações instrumentais entre animais de laboratório e sua gente. _Horizontes antropológicos_, Porto Alegre, v. 17, n. 35, p. 27-64, Junho, 2011.
> (2015, p. 48)

# Quatro livros publicados como história sobre as relações Natureza-Sociedade

> Donna J. Haraway: Um dos modos como vejo estes quatro livros [[ _Crystals, Fabrics, and Fields_ (1976),  _Primate  Visions_ (1989), _Simians, Cyborgs, and Women_ (1991) e _Modest Witness_ (1997) ]], quando eles estão alinhados em sequência é que **contam uma narrativa histórica. Desde o início  e  até  o  presente,  meu  interesse  tem  se  voltado  para  aquilo  que  acaba contando  como  natureza  e  para  quem  acaba  habitando as  categorias  naturais.** Mais do que isso, interesso-me por aquilo que está em jogo no julgamento sobre a natureza e por aquilo que está em jogo na manutenção das fronteiras entre o que é chamado de natureza e o que é chamado de cultura na nossa sociedade. E como os valores se invertem? Como funciona esse dualismo, entre natureza e sociedade ou  natureza  e  cultura,  tão  importante  em  nossa  história  cultural  e  em  nossa política?
> (2015, p. 49)

> [T]odos os quatro livros são versões desse problema e todos eles o  abordam  por  meio  da  biologia. [[...]] Mas a questão principal é a manutenção dessa junção muito potente entre fato e ficção, entre o literal e o figurativo ou tropológico, entre o científico e o expressivo.
> (2015, p. 49)

# Cristais, tecidos e campos

> [O] primeiro livro, _Crystals, Fabrics, and Fields: Metaphors of Organicism  in  Twentieth-Century  Developmental  Biology_,  discutiu  três estruturas  metafóricas  que  foram  usadas  para  interpretar  a  forma  biológica  no século  XX,  a  concepção  para  a  formação  e  para  o  controle  da  forma  biológica. **“Cristais”, “tecidos” e “campos” são todas metáforas não-reducionistas, querendo dizer,  não-atomísticas,  não-particularistas.  São  metáforas  que  lidam  com totalidades complexas e com processos complexos. Em outras palavras, você não pode entender adequadamente a forma quebrando-a em suas menores partes e depois devolvendo-lhes suas relações.**
> (2015, p. 50)

# O que conta como natureza na trilogia (Primate Visions, Symians... e Modest_Witness)

> Quando as pessoas perdem as relações, o todo, e focam apenas nos  pedaços  separados,  surgem  todos  os  tipos  de  leituras  distorcidas  de  meu trabalho. **Todas as minhas metáforas implicam em algum tipo de ação sinergética em um nível de complexidade que não é acessível por meio das suas menores partes.  Portanto,  elas  são  todas  metáforas  em  torno  da  complexidade.  Meu trabalho tem sempre sido sobre o que conta como natureza.** De certa forma, sinto que  escrevi  sobre  uma  gama  de  tipos  de  naturezas.  Escrevi  sobre  naturezas artefatuais nos diversos tipos de trabalhos ciborguianos que escrevi. **Uma maneira de encarar esses livros é que _Primate Visions_, _Simians, Cyborgs, and Women_ e _Modest_Witness_ tratam  de  três  tipos  de  entidades  –  cada  um  deles  investiga diferentemente um conjunto de historicidades, de binarismos, de interfaces, de práticas de conhecimento.** Embora ecoando um no outro, eles não são a mesma coisa. _Modest_Witness_ é, de certo modo, o terceiro livro de uma trilogia. Cada um dos três volumes é composto por seus próprios ensaios e cada um destes tem sua própria história de publicação. Em todos os livros há muita coisa nova que nunca havia sido publicada antes, mas todos eles contêm ensaios que foram escritos e publicados em outras ocasiões. Cada um dos três livros faz algum trabalho dos outros também. Por exemplo, em _Simians, Cyborgs, and Women_, [os capítulos] “A Cyborg Manifesto” e “The Biopolitics of Postmodern Bodies” são dois ensaios-chave,  mas  há  também  os  ensaios  sobre  primatas  e  aqueles  sobre  gênero.  Da mesma  forma,  em _Primate Visions_,  há  capítulos  que  enfatizam  as  qualidades ciborguianas da pesquisa sobre primatas. E em _Modest_Witness_ há capítulos que enfatizam certos temas ciborguianos mas não o material sobre primatas. Contudo, muitas questões de saberes situados reemergem em _Modest_Witness_.
> (2015, p. 50-1)

# Sobre a "metodologia" de Haraway e sua ligação com a biologia

> Palavras como “metodologia” são muito assustadoras, você sabe! **Em vez de “metodologia”, preferiria dizer que tenho modos definidos de trabalhar que se  tornaram  mais  conscientes  com  o  passar  dos  anos.**  E  é  certo  que  o  meu treinamento  em  biologia  –molecular,  celular  e  do  desenvolvimento  –  importa muito para mim. Em particular, **na maneira como me permite estar alerta aos seres  biológicos  e  às  redes  biológicas  de  relação, tirando  um  tremendo  prazer deles. Sou fascinada pela arquitetura interna das células e dos cromossomos.** E não há dúvida de que eu frequentemente penso por metáforas biológicas.
> (2015, p. 51)

# Metáfora biológica, escala e mais-que-metáfora

> Sou  fascinada  por  mudanças  de  escala.  Penso  que  **os  mundos biológicos convidam a pensar _em_, e _sobre_, diferentes tipos de escala.** Ao mesmo tempo, mundos biológicos são plenos de imaginações e de seres desenvolvidos de mecanismos  e  de  arquiteturas  biológicas  bastante  extraordinárias.  **A  biologia  é uma fonte inesgotável para a tropologia. É certamente plena de metáfora, mas é mais do que metáfora.**
> (2015, p. 51)

> **Quero dizer [[ com o termo "mais que metáfora"]] não somente as metáforas discursivas e fisiológicas que podem  ser  encontradas  na  biologia,  mas  as  estórias.**  Por  exemplo,  todas  as diferentes  incongruências  irônicas,  quase  engraçadas.  A  pura  astúcia  e complexidade disso tudo. **De modo que a biologia não seja apenas uma metáfora que  ilumina  alguma  outra  coisa,  mas  uma  fonte  inesgotável  de  acesso  à  não-literalidade do mundo.** E também, **quero chamar a atenção para a simultaneidade do fato e da ficção, da materialidade e da semioticidade, do objeto e do tropo.**
> (2015, p. 51-2)

# Tensionamento da distinção indivíduo-coletivo a partir do _Mixotricha paradoxa_

> Utilizo a _Mixotricha paradoxa_ como uma entidade que interroga a individualidade e a coletividade ao mesmo tempo. Trata-se de um organismo unicelular  microscópico  que  vive  no  intestino  posterior  do  cupim  da  Austrália setentrional. Aquilo que conta como “ele” é complicado, pois ele vive em simbiose obrigatória  com  outros  cinco  tipos  de  entidades.  Cada  uma  tem  um  nome taxonômico  e  cada  uma  se  relaciona  estreitamente  com  bactérias,  pois  não possuem um núcleo celular. Elas possuem ácido nucléico, possuem DNA, mas este  não  é  organizado  em  um  núcleo.  Cada  um  destes cinco  tipos  de  coisas diferentes vive em ou sobre uma região diferente da célula. Por exemplo, um vive nas interdigitações da superfície exterior da membrana celular. De modo que você tem estas pequenas coisas que vivem nestas dobras da membrana celular e outras que vivem dentro da célula. **Mas elas não são, no sentido pleno, parte da célula. Por  outro  lado,  elas  vivem  em  simbiose  obrigatória.  Ninguém  pode  viver independentemente aqui. Isso é co-dependência pra valer! E, então, a questão é – ela  é  uma  entidade  ou  seis?  Mas  seis  tampouco  está correto,  pois  há aproximadamente  um  milhão  das  cinco  entidades  anucleadas  para  cada  célula mononuclear.  Há  múltiplas  cópias.  Então,  quando  é  que  um  decide  se  tornar dois? Quando é que este conjunto completo se divide de modo que agora você tem dois conjuntos? E o que conta como _Mixotricha_? É somente a célula nucleada ou é conjunto todo? Obviamente, esta é uma fabulosa metáfora que é uma coisa real para interrogar nossas noções de um e de muitos.**
> (2015, p. 52)

# Biologia x psicanálise

> A biologia é um recurso infindável. **Esta é a razão pela qual sempre preferi biologia [[ ~corpo ]] à psicanálise [[ ~linguagem ]], pois ela coloca para fora muito mais possibilidades para estórias que parecem alcançar algo de nossa existência política, psicológica, histórica.** A psicanálise define tudo muito cedo – ela pode ser parte da verdade, mas não é a parte mais interessante. Eu também simplesmente amo o nome _Mixotricha [[ fios misturados ]] paradoxa_!
> (2015, p. 53)

# Figuras como complexificação da literalidade

> É  um  tipo  de  mentalidade  literal [[ a que lê seu trabalho como tecnofílico ou tecnofóbico, etc. ]].  E  é  por  isso  que  figuras  são  tão importantes  para  mim,  porque  **figuras  são  imediatamente  complexas  e  não-literais,  para  não  falar  instâncias  de  real  prazer na  linguagem.**  Nota-se  um estranho literalismo quando críticos criam posições que na verdade não existem – como lendas urbanas recicladas de pessoas dizendo, “Você acredita em DNA!?!” Mas que falta de sofisticação! Isto é triste, revoltante, e retira todo o prazer em linguagem _e_ corpos que anima tanto do trabalho sério em estudos culturais da ciência.
> (2015, p. 54)

# Materialidade da linguagem, semioticidade da matéria

> A primeira coisa que eu diria é que palavras são intensamente físicas para mim. Acho palavras e linguagem mais próximas à carne do que às ideias.
> (2015, p. 54)

> **TNG:** _Roland Barthes tem esta ótima frase, “A linguagem é uma pele: esfrego minha linguagem contra a outra. É como se eu tivesse palavras em vez de dedos, ou dedos nas pontas de minhas palavras” (BARTHES, 1978, p. 73). De modo bem semelhante você conta com a suculência metafórica carnal da linguagem._
>
> **DH:**  Dado  que  eu  experiencio  a  linguagem  como  um  processo intensamente físico, não posso _não_ pensar por meio de metáfora. Não é como se fizesse  uma  escolha  de  trabalhar  com  e  por  meio  de metáfora,  **é  que  me experiencio  dentro  destes  processos  de  semiose  intensamente  físicos, constantemente desviantes. Bioquímica e linguagem simplesmente não parecem tão  diferentes  para  mim.**  Há  também  uma  dimensão  católica  em  tudo  isto.  A minha  profunda  formação  em  simbolismo  e  sacramentalismo  católico  – doutrinas de encarnação e transubstanciação – foram todas intensamente físicas. **A  inexorável  simbolização  da  vida  católica  não  é  somente  anexada  ao  mundo físico, ela é o mundo físico.** Olhe a arte religiosa do sudoeste dos Estados Unidos, a arte mexicana, latina e chicana, e você tem um exemplo intenso disto. Contraste esta arte com a arte protestante mais abstêmia e então imagine o interior de uma igreja  na  Cidade  do  México.  Cresci  em  meio  ao  mundo  da  arte  da  Cidade  do México, por assim dizer, embora tenha crescido em Denver, Colorado. Era uma cena católica irlandesa, em nada tão rica quanto a tradição cultural latina, mas **cresci  em  grande  parte  dentro  de  um  mundo  narrativo  figurado  simbólico elaborado  no  qual  as  noções  de  signo  e  de  carne  estavam  profundamente amarradas uma à outra.** Eu entendi o mundo deste modo quando tinha quatro anos.
>
> (2015, p. 54-5)

> Meus instintos são sempre fazer a mesma coisa. É **insistir na junção entre materialidade e semiose. Carne não é mais uma coisa do que um gene é. Mas a semiose materializada da carne sempre inclui os tons da intimidade, do corpo, do sangramento, do sofrimento, da suculência.** Carne é sempre de algum modo úmida. Está claro que não se pode utilizar a palavra carne sem entender vulnerabilidade e dor.
> (2015, p. 55)

> **TNG:** Há esta citação que guardei de seu “A Manifesto for Cyborgs” de 1985 na qual você diz: “Por que nossos corpos deveriam terminar na pele ou incluir, na melhor das hipóteses, outros seres encapsulados pela pele”?
>
> **DH:**  E  outros  organismos  assim  como  objetos  construídos.  **Há  todos  os tipos de não-humanos com os quais nós estamos entrelaçados.**
>
> **TNG:** _E  também  os  modos  como  a  nossa  carne  é  feita  de  carne artefatual. Estou pensando na forma como você emprega sinais sintáticos – “@”,  “©”,  “TM”  –  em Modest_Witness@Second_Millennium.FemaleMan© _Meets_OncoMouseTM para nos localizar. É um exemplo de como o seu título cria  com  sucesso  um  novo  tipo  de  sintaxe  e  de  figuração. O  título “Modest_Witness@Second_Millennium.FemaleMan©_Meets_OncoMouseTM”  é seu  próprio  poema  tecnocultural.  Você  visualiza  e  teoriza  por  meio  das palavras e sinais sintáticos do título, situando-nos na história do final do século  XX.  Isto  é  maravilhoso  porque  estes  sinais  são  as  novas  marcas [brands]._
>
> **DH:** Especialmente com o duplo sentido de marca como tipo e de marca de propriedade, queimada na carne.
>
> (2015, p. 55)

# Humor no título de _Modest_Witness_, modernidade e pós-modernidade

> **TNG:** _E em vez de usar a palavra pós-modernismo, ou qualquer outro tipo  de  categoria  da  modernidade  para  marcar  a  diferença  constitucional entre o final do século XX e os momentos iniciais da modernidade, você diz, “Eu dou ao leitor um endereço de e-mail, se não uma senha, para situar as coisas na rede”. (HARAWAY, 1997, p. 43). E-mail é familiar para quase todo mundo hoje. É uma localização crucial para nós na vida cotidiana e significa um modo de comunicação particular à tecnocultura do final do século XX. “@” instancia todas as complexas redes de relação (econômicas, ontológicas, sociais, históricas, tecnológicas) que são cruciais para o pós-modernismo sem nos  forçar  mais  uma  vez  ao  engajamento  em  todos  os ásperos  debates acadêmicos em torno do termo._
>
> **DH:** E é uma piada também.
>
> **TNG:** _Sim.  Humor,  tanto  quanto  ironia,  é  decisivo  para  seu  estilo teórico. Como podemos não rir da descrição que você oferece da combinação transgênica  anticongelante  tomate-peixe  desenvolvida  em  Oakland, Califórnia,  em  1991.7  Dado  que  eu  mencionei  o  pós-modernismo,  estou interessada na sua definição de modernidade._
>
> **DH:** Minha definição de **modernidade é que ela é o período do transporte intensificado  de  sementes  e  de  genes.**  Por  exemplo, olhe  para  a  invenção  do primeiro grande sistema industrial – a agricultura de **plantation** (que não é uma ideia  minha,  mas  que  tomei  de  outros)  –  e  siga  todo  o  realocamento  de populações, de plantas, de açúcar, de mandioca, para alimentar as populações das quais a força de trabalho masculina foi removida para fins de agricultura colonial. **Você pode fazer a história da modernidade como a história do transporte de genes também. De fato, você pode tomar cada uma das células-tronco tecnocientíficas que menciono em _Modest_Witness_ – o cérebro, o chip, o gene, o feto, a bomba, a raça, o banco de dados e o ecossistema, e fazer a história da modernidade.**
>
> (2015, p. 56)

> 7: “Safras resistentes a herbicidas são provavelmente a maior área de engenharia genética ativa de plantas. Eu me encontro particularmente atraída por estes simpáticos novos seres como o tomate com um gene de um linguado de águas geladas profundas, que codifica para uma proteína que retarda  o  congelamento,  e  a  batata  com  um  gene  do  bicho-da-seda  gigante,  que  aumenta  a resistência  à  doença.  A _DNA  Plant  Technology_  (Oakland,  Califórnia)  começou  a  testar  a combinação anticongelante tomate-peixe em 1991”. (HARAWAY, 1997, p. 88).
> (2015, p. 56)

# Prática mundana x (realismo-relativismo)

> Me refiro [[ como "prática mundana" ]] àquele conjunto implodido de coisas no qual a fisiologia do corpo, o fluxo de sangue e hormônios e as operações químicas – a carnalidade do organismo – se misturam com a vida total do organismo. De forma que, num certo sentido, você pode começar a falar sobre qualquer dimensão daquilo que significa ser mundano – o comercial, o fisiológico, o genético, o político.
> (2015, p. 57)

> "[M]undano” é uma grande palavra para mim. **Mas todos esses adjetivos tratam de maneiras de começar a conversar, a trabalhar.** Eles são modos de começar a puxar os fios pegajosos nos quais o técnico, o comercial, o mítico, o político, o orgânico estão implodidos.
> (2015, p. 57)

> Eu  **escolhi  essa  palavra  como  uma  maneira  de  contornar  o  debate entre realismo e relativismo.** Eu poderia ter dito que “realidade” é a separação entre naturezas e culturas e que eu estou trabalhando na direção de um tipo de realismo melhor, mas isso me apoia em uma série de argumentos equivocados. E eu  sou  acusada  de  ser  uma  relativista  por  aqueles  que  deliberadamente  me interpretam mal, o que me deixa muito brava, pois eu me desdobrei para dizer que essa dicotomia  particular  é  parte  do  problema.  **Também,  dado  que  meu comprometimento  é  com  coisas  como  mortalidade  e  finitude  e  carnalidade  e historicidade e contingência, pareceu-me que “mundano” era uma boa escolha. Mundano também implica em prestar atenção a coisas como poder e dinheiro.**
> (2015, p. 57)

> **TNG:** _Certo – e novamente, o motivo pelo qual eu perguntei é porque isso me lembra de Heidegger e de como ele era tão comprometido com o uso de uma linguagem que era do mundo, que era ordinária ou comum. Mundano é uma  palavra  terrena,  uma  palavra  aterrada.  Ela  é  francamente despretensiosa._
>
> **DH:** Correto. De certa forma, “situado” foi um esforço parecido de pegar uma palavra comum e tentar fazê-la fazer um certo número de coisas.
>
> (2015, p. 57)

# Criticalidade como noção positiva em Haraway

> **TNG:** _Uma das coisas mais importantes que aprendi com você é uma **noção de criticalidade [criticality] que vai além do mero “criticismo” – além da criticalidade  didática,  diagnóstica.**  Isso  me  interessa  especialmente,  pois ultimamente tenho percebido como aquilo que conta **como teoria crítica é mais submissa à história do que eu jamais havia pensado.** Isso provavelmente tem a ver com a minha posição no mundo da arte, onde a arte crítica assumiu todo tipo de diferentes dimensões de geração a geração. Mas recentemente eu me tornei cada vez menos certa sobre o que as pessoas querem dizer por “crítico”.  **Sua  noção  de  criticalidade  é  notadamente  diferente  da  noção tradicional de crítico, significando desconstruir argumentos e enxergar onde se situa o poder. Significaria “crítico” apenas a posse de um argumento? Estou pensando na arte que, ao produzir novos significados, oferece um avanço crítico – abrindo e produzindo. O trabalho crítico pode ser uma atividade produtiva, e não apenas negativa.** Li recentemente uma maravilhosa distinção que dizia que a teoria deveria fundar a mudança, e não encontrá-la. Tive esse problema na pós-graduação. Eu sempre lia por aquilo que um texto me dava e não  por  aquilo  que  ele  não  me  dava,  por  isso  eu  constantemente  me surpreendia quando “ler” significava que todos deveriam atacar um autor gritando sobre tudo o que ele ou ela deixou de fora. Procurar apenas as falhas ou  as  ausências  parece  uma  maneira  muito  estranha  de  aprender.  Na verdade, parece o oposto de aprendizado._
>
> **DH:** Odeio este modelo.
>
> **TNG:** _E por que as pessoas pensam que essa é a única maneira de ser crítico?_
>
> **DH:** Parte disso é a competição e o medo de parecer tolo se você não fizer a crítica primeiro. De fato, penso que parte das péssimas políticas raciais funciona a partir do mesmo princípio, segundo o qual as pessoas se apressam em acusar os outros de racistas para não serem julgadas elas mesmas. É como se pensassem que  o  racismo  é  algo  que  você  pode  expelir  facilmente  por  meio  de  algumas poucas  afirmações.  Você  não  pode  eliminar  racismos por  meio  de  mantras  ou apontando como um determinado artigo não lidou com a raça dessa ou daquela maneira, e então relaxar e pensar que está livre por ter notado isso. Em outras palavras, o fato de ter visto te retira de lá. Não são apenas pessoas brancas que têm essa relação com o racismo. **E penso que algo desse estilo de crítica negativa na pós-graduação, em relação não apenas ao racismo mas a muitos outros tipos de coisas, está enraizado num medo de aceitar algo com toda a sua bagunça e sujeira e imperfeição.**
>
> (2015, p. 58)

[Ainda sobre isso, em _The Haraway Reader_ (2015, p. 326-7): "(...) **I use critique, i.e., in the sense that things might be otherwise.It is a sense of critique that is not negative, necessarily, except in the particular way that the Frankfurt School understood negativity-a way which I think is really worth remembering and holding on to. It is critique in the deep sense that things might be otherwise.** There is much of the Frankfurt School that I have never embraced, but that sense of critique as a freedom project is important. There was a certain amount of work, and **there even still is a certain amount of work in that freedom project that oppositional, or critical cyborgs can do, but I agree that it is much less true now than it was in 1983. Precisely because of the kind of tightening of the Internet around us all; precisely because we are now in the matrix in such a relentlessly literal way that there is some really new tropic work that has to be done in this figure.**"]

# Ciborgue como figura aberta e dinâmica, pós 2a grande guerra (~militarização, industrialização)

> **TNG:** _Vamos  seguir  para  o  ciborgue,  para  como  você  escolheu desenvolver um sistema crítico por meio da produção de novas formulações e relacionamentos  a  partir  dos  problemas  e  contradições  –  a  “bagunça  e sujeira” – da vida como a habitamos diariamente. Obviamente, o mito do ciborgue é seu exemplo principal. Uma grande má-interpretação do ciborgue ocorre quando as pessoas não veem sua qualidade generativa, que não é somente uma negação das velhas estruturas de poder (militarismo, Grande Ciência,  patriarcalismo,  etc.)  mas,  uma  tentativa  de  ver  as  coisas diferentemente. Como em sua discussão do gene, o ciborgue não é uma coisa ou um tópico acabado mas, por definição, constantemente transformando e sendo  repensado.  Ou,  como  você  uma  vez  colocou,  “Ciborgues  não  ficam parados”.8_
>
> **DH:** Isto está certo, ele **é um tópico aberto e o ciborgue está neste conjunto curioso de relacionamentos familiares com espécies-irmãs de vários tipos.** É uma figuração  que  requer  que  se  pense  nos  aspectos  dos sistemas  de  comunicação feitos  pelo  homem,  a  mistura  do  orgânico  e  do  técnico  que  é  inescapável  nas práticas ciborguianas.
>
> **TNG:** _Há uma tendência para o ciborgue ser deshistoricizado hoje em dia.  No  entanto,  é  crucial  compreender  que  o  próprio  ciborgue  tem  uma história, é um filho de um certo momento da história, e portanto ganhará diferentes sentidos e características em relação aos processos históricos._
>
> **DH:**  Definitivamente  –  ele  tem  camadas  de  histórias.  **Como  emprego  o termo,  sou  inflexível  que  o  ciborgue  não  diga  respeito  a _todos_  os  tipos  de relacionamentos maquínicos, artefatuais, com seres humanos. Tanto o humano quanto o artefatual possuem histórias específicas.** Por um lado, o ciborgue não é a mesma coisa que o androide. O androide tem de fato uma história muito mais longa. O androide surge dos brinquedos mecânicos do século XVIII e do esforço para  construir  modelos  maquínicos,  especificamente modelos  miméticos  de movimento  humano.  **Embora  haja  um  certo  tipo  de  câmara  de  eco  entre  o androide  e  o  ciborgue,  certos  tipos  de  continuidades  e  descontinuidades,  me empenho  muito  para  que  o  termo  “ciborgue”  seja  utilizado  para  designar especificamente  aqueles  tipos  de  entidades  que  se  tornaram  historicamente possíveis  por  volta  da  Segunda  Guerra  Mundial  e  logo  após.  O  ciborgue  está intimamente envolvido em histórias específicas de militarização, de projetos de pesquisa específicos com ligações com a psiquiatria e a teoria da comunicação, a pesquisa  comportamental  e a  psicofarmacológica,  as teorias  da  informação  e o processamento de informação. É essencial que o ciborgue seja visto emergindo de tal matriz específica.** Em outras palavras, o ciborgue não é “nascido” mas ele tem de fato uma matriz (_rindo_)! Ou melhor, ele não tem uma mãe, mas ele tem de fato  uma  matriz!  Ele  não  nasceu  num  jardim,  mas  certamente  nasceu  numa história. E esta história não tem sido suave e tem aproximadamente meio século agora.
>
> (2015, p. 59-60)

> 8: “Ciborgues não ficam parados. Já nas poucas décadas que eles têm existido, eles, em fato e ficção, alteraram-se  em  entidades  de  segunda  ordem  como  bancos  de  dados  genômico  e  eletrônico  e outros habitantes da zona chamada ciberespaço”. (HARAWAY, 1995a, p. xix)
> (2015, p. 59)

# Modernismo x pós-modernismo como escolha narrativa, ciborgue como figura que promove a emergência do inesperado

> **TNG:** _Você poderia fazer uma distinção modernista, pós-modernista?_
>
> **DH:** Poderia, mas, novamente, estas são todas escolhas narrativas. **Não é que a própria história determine estas narrativas, mas que as narrativas moldam a história.**
>
> **TNG:** _Bem colocado._
>
> **DH:** Está relacionado com o que falamos antes quando as pessoas engatam somente em um aspecto. Por exemplo, aqueles que relegam o ciborgue a um tipo estranho, atenuado, de euforia tecnofílica ou de amor cintilante a todas as coisas ciber,  o  que  está  completamente  errado.  Ou  eles  pensam  que  o  ciborgue  seja meramente uma figura condenatória, encravado como ele está no militarismo. **O que  me  interessa  mais  sobre  o  ciborgue  é  que  ele  faz  coisas  inesperadas  e responde por histórias contraditórias, permitindo ao mesmo tempo algum tipo de trabalho _no_ e _do_ mundo.**
>
> (2015, p. 60)

# "Quanto como uma folha eu sou"

> Bem,  um [[ momento que me lembro da "ciborgologia" ou "ciborguidade" se cristalizando para mim ]]  é  certamente  **meu  sentido  de  complexidade,  interesse  e prazer – assim como a intensidade – de como imaginei quanto como uma folha eu sou.** Por exemplo, eu sou fascinada com a arquitetura molecular que plantas e animais   compartilham,   e   também   pelos   tipos   de   instrumentação, interdisciplinaridade e práticas de conhecimento que entraram nas **possibilidades históricas de compreender o quanto eu sou como uma folha.**
> (2015, p. 61)

# Contingencia histórica como alternativa ao construtivismo radical e ao determinismo, ênfase na especificidade histórica da tecnociência

> Parte  do  desconforto [[ em relação ao seu modo de encarar a biologia e a ciência ]] vem  do  fato  de  que  **se  você  fala  sobre  a implacável _contingência  histórica_ de  experienciar  a  si  próprio,  ou  sobre  a manufatura  do  conhecimento  científico, as  pessoas  ouvem  relativismo  ou  puro construtivismo social, o que não é de modo algum o que estou dizendo.** Mas este é o tipo de redução que continua sendo feita. E há então as pessoas que se sentem ameaçadas porque leem tais análises _como_ determinismo biológico! Um tipo de naturalismo que elas não querem, pois são construtivistas sociais e não querem dar muito peso ao biológico ou ao natural. **Estou tentando dizer _ambos_, _e_, _nem_, _tampouco_,  e  então  um  monte  de  confusão  aparece,  e  não  um  tipo  muito produtivo de confusão. Estou falando sobre um modo de interação com o mundo que é implacavelmente específico historicamente. A tecnociência é uma semiose materializada.**  É  como  nós  nos  engajamos  com  e  no  mundo.  **O  que  não  é  a mesma coisa que dizer que o conhecimento é opcional. É dizer que há nele uma especificidade que você _não pode_ esquecer.**
> (2015, p. 61-2)

# Responsabilidade (ética) do humano, atividade dos atores não-humanos (?)

> **TNG:** _Responsabilidade  é  uma  das  forças  –  e  substâncias  –  mais potentes em seu trabalho. De muitas maneiras ela está no centro – se o seu trabalho tem um centro. É o principal ponto de sustentação de suas análises. Você  nos  ensina  a  sermos responsivas  a  todas  as  complexidades  na tecnocultura do final do século XX, e então você anexa a esta responsividade os requerimentos da responsabilidade._
>
> **DH:** Bem, são as pessoas que são éticas, não estas entidades não-humanas.
>
> **TNG:** Você quer dizer, romantizar o não-humano?
>
> **DH:** Certo, este é um tipo de antropomorfização dos atores não-humanos com a qual precisamos ter cautela. Nossa relacionalidade não é do mesmo tipo de ser. São as pessoas que têm responsabilidade emocional, ética, política e cognitiva dentro destes mundos. Mas não-humanos são ativos, não passivos, recursos ou produtos.
>
>(2015, p. 62)

# Cama de gato como jogo, como uma metodologia

> **TNG:** _É nesta compreensão multidimensional que aparece a sua noção de  uma  cama  de  gato,  isto  é,  um  estudo  da  tecnociência  anti-racista, feminista, multicultural?_
>
> **DH:** Esta é uma daquelas formulações impossíveis!
>
> **TNG:** _O que estou perguntando é se a cama de gato é uma outra figura para você, ou é uma metodologia?_
>
> **DH:**  Bem,  dado  que  **cama  de  gato  é  um  jogo,  suspeito  que  seja  uma metodologia  com  “m”  minúsculo.  É  um  modo  de  trabalhar  e  de  pensar  sobre trabalho, de forma que neste caso ele seja endereçado às pessoas dos estudos da ciência para se valerem mais densamente dos estudos feministas e dos estudos culturais e vice-versa.** Cama de gato pode ser jogada em suas próprias mãos, mas é  mais  interessante  jogar  com  outra  pessoa.  **É  uma  figura  para  construir relacionalidade que não seja agonística.**
>
> **TNG:** _É  similar  àquilo  que  você  defende  em  termos  do  discurso  do sistema imunológico em “The Biopolitics of Postmodern Bodies” usando a série Xenogenesis de Octavia Butler?10_ 
>
> **DH:** Sim. **Mas é importante que a cama de gato não se torne o modelo singular.** Existem algumas práticas tecnocientíficas contra as quais gostaríamos de adotar  uma  postura  oposicionista  e  antagônica.  **As  metáforas  de  harmonia  e coletividade também não são a estória completa dado que, às vezes, competição e luta e mesmo metáforas militares podem ser o que precisamos.** É apenas que a agonística  tem  sido  tão  excessivamente  enfatizada  dentro  de  muito  da tecnociência.  **Eu  estava  escrevendo  especificamente contra  aspectos  do  livro  de Bruno  Latour, _Ciência em Ação_,  que  é  tão  esmagadoramente  dependente  de metáforas de agonística e combate. A figura da cama de gato é uma resposta direta àquilo.** É portanto uma metáfora contextual.
>
> (2015, p. 62-3)

# Sobre os estudos feministas da tecnociência

> Se  a  tecnociência,  em  nosso  momento  na  história,  é inequivocadamente “natureza” para nós – e não somente natureza mas natureza-cultura – então **compreender a tecnociência é um modo de compreender como naturezas e culturas se tornaram uma palavra só.** Então, a análise da tecnociência, a compreensão de em que tipo de mundo estamos vivendo, é o que chamamos de estudos de tecnociência. **Estudos feministas da tecnociência levam a sério aquela lista  de  coisas  que  você  acabou  de  ler [[ “tecnociência  com  democracia”,  “objetividade  forte,  uma  que  seja comprometida com projetos de igualdade humana”, é “modesta, universal, abundante”, e “constituída de projetos de conhecimento autocríticos” ]].**  Então,  isto  envolve  a  liberdade tecnocientífica, a democracia tecnocientífica, a compreensão de que democracia diz respeito ao empoderamento de pessoas que estão envolvidas na montagem e desmontagem  de  mundos,  que  processos  tecnocientíficos  estão  lidando  com alguns mundos em vez de outros, que a democracia exige que as pessoas estejam substantivamente envolvidas e se saibam envolvidas, e tenham poder para serem cobradas e coletivamente responsáveis umas pelas outras. E estudos feministas de tecnociência passam repetidamente pelas contradições permanentes e dolorosas de gênero. 
> (2015, p. 63-4)

> Tecnociência  feminista  realmente  significa  ir  além  dos  tipos  de instituições que temos agora. Ela está cheia de diferentes tipos de processos de trabalho  e  de  práticas-conhecimento,  incluindo  a  remodelagem  do  tempo  e  do espaço. Por exemplo, **interagir efetivamente no trabalho, trabalhar com pessoas, realmente envolve repensar o tempo e as carreiras e a velocidade da pesquisa.**
> (2015, p. 64)

# Sobre o testemunho modesto

> Processos tecnocientíficos no momento contam com vastas disparidades de riqueza, poder, agência, soberania, oportunidades de vida e morte. Os projetos iluministas para igualdade têm um tipo de saliência modificada dentro da tecnociência hoje. Sou uma filha do Iluminismo; é disso que trata, em grande parte, _Modest_Witness_. Não estou repudiando a herança de democracia e liberdade  e  todas  aquelas  heranças  iluministas  poluídas.  Eu  as  vejo  como  que deturpadas. Estou tentando retrabalhá-las.  
> (2015, p. 64)

> 12: Em _Modest_Witness_, **a testemunha modesta representa a estória dos estudos da ciência assim como da ficção científica.** FemaleMan© é a figura mais importante do feminismo. OncoMouseTM é a figura da biotecnologia e da engenharia genética, uma sinédoque para a tecnociência.
> (2015, p. 64)

> “Testemunha modesta”, junto com OncoMouseTM e FemaleMan©, são figuras  que  uso  no  livro  para  representar  novos  modos de  imaginar  e  de  fazer tecnociência.12 Em referência a _Modest_Witness@Second_Millennium_, o leitor vê  imediatamente  que  ela/e  é  o  emissor  e  o  receptor  de  mensagens  em  meu endereço  de  e-mail.  Mas  **conto  também  com  a  complexa  história  do “testemunhar” e de ser uma “testemunha” dentro das estórias dos estudos da ciência em relação ao desenvolvimento do método experimental por Robert Boyle no século XVII e às controvérsias subsequentes sobre como fatos são estabelecidos crivelmente.** Por exemplo, Thomas Hobbes repudiou o modo de vida experimental precisamente  porque  o  conhecimento  associado  a  este  modo  de  vida  era dependente de uma prática de _testemunho_ por uma comunidade especial, como aquela  dos  clérigos  e  dos  advogados.  **Estou  interessada  neste  tipo  preciso  de testemunho,  pois  é  sobre  ver;  atestar;  ser  publicamente  responsável  por,  e fisicamente  vulnerável  a,  suas  visões  e  representações.  Testemunhar  é  uma prática  coletiva,  limitada,  que  depende  da  credibilidade  construída  e  nunca finalizada daqueles que o fazem, todos os quais são mortais, falíveis e repletos das consequências  de  desejos  e  temores  inconscientes  e rejeitados.**  Como  cria  da Sociedade Real da Restauração Inglesa de Robert Boyle e do modo experimental de vida, eu permaneço ligada à figura da testemunha modesta. **Minha testemunha modesta é sobre dizer a verdade – dar testemunho confiável – ao mesmo tempo evitando  o  aditivo  narcótico  das  fundações  transcendentais.  Ela _refigura_ os sujeitos, os objetos e o comércio comunicativo da tecnociência em diferentes tipos de nós.**
> (2015, p. 64-5)

> Modesta”,  como  “testemunha”,  tem  uma  história  profunda  e complexa  nos  estudos  da  ciência  em  relação  a  gênero  e  aos  experimentos  de Robert  Boyle  com  a  bomba  de  ar  e  o  desenvolvimento do  modo  de  vida experimental. Retomo a análise de Elizabeth Potter da maneira como o gênero estava em jogo no modo de vida experimental do período dentro do contexto dos debates sobre a proliferação de gêneros na prática do _cross-dressing_. **Mantenho a  figuração  de  “modéstia”  porque  o  que  contará  como  modéstia agora  é precisamente  o  que  está  em  questão.**  Há  o  tipo  de  modéstia  que  faz  você desaparecer e há o tipo que ressalta sua credibilidade. Modéstia feminina tem sido sobre  estar  fora  do  caminho  enquanto  modéstia  masculina  tem  sido  sobre  ser uma testemunha crível. **E então há o tipo de modéstia _feminista_ (e não feminina) que  estou  defendendo  aqui,  que  é  sobre  um  tipo  de  imersão  no  mundo  da tecnociência  na  qual  você  faz  uma  dura  intersecção de  perguntas  sobre  raça, classe, gênero, sexo com o objetivo de fazer uma diferença no mundo “material-semiótico” real.**
> (2015, p. 65-6)

> Eu  sei  o  que  você  quer  dizer [[ sobre confiar em pessoas modestas ]].  E  as  pessoas  também  tomam erroneamente modéstia por vitimização por causa do duplo sentido de modéstia – a modéstia que é sobre desaparecimento, ou acobertamento, que é mal entendida como incompetência. **Modéstia verdadeira é sobre ser capaz de dizer que você tem mesmo  certas  habilidades.  Em  outras  palavras,  ser  capaz  de  fazer  afirmações fortes de conhecimento. Não se render ao relativismo estúpido, mas testemunhar, atestar.  O  tipo  de  testemunha  modesta  que  demando  é  um  que  insiste  na situacionalidade, onde a localização é ela própria uma construção complexa assim como  uma  herança.**  É  uma  figura  que  faz  suas  apostas  com  projetos  e necessidades daqueles que não habitariam ou não poderiam habitar as posições subjetivas  dos  “laboratórios”,  do  homem  civil  e  crível  de  ciência.  O  ponto  é, **_Testemunha_Modesta_no_Segundo_Milênio_ precisa de um novo modo de vida experimental para satisfazer a esperança milenar de que a vida sobreviverá neste planeta.  Uma  testemunha  não  é  um  observador  desinteressado,  não  é  um marciano.  Penso  sobre  testemunhar  como  algo  implicado  na  prática  mundana que  discutimos  anteriormente  porque  uma  testemunha também  não  é  um cérebro-num-vaso.  Uma  testemunha  está  sempre  em  risco  de  atestar  alguma verdade  em  vez  de  outras.  Você  testemunha.  Pessoas que  vão  à  Guatemala, Chiapas,  Nicarágua  ou  El  Salvador  para  testemunhar estão  fazendo  algo  que  é absolutamente  sobre  estar  engajado.**  Elas  estão  também  envolvidas  no requerimento de dizer a verdade, fazendo-se responsáveis por testemunhar e dizer a  verdade.  **Testemunhar,  neste  caso,  é  anti-ideológico  no  sentido  de  resistir  à “estória oficial”. Verdade aqui não se escreve com um “V” maiúsculo; i.e., verdade que é transcendente ou fora da história. Ela é resolutamente histórica; atestando as condições de vida e morte.**
> (2015, p. 66-7)

> Definitivamente [[ há na noção de testemunho um senso próprio de ética ]].  **E  conhecimento  científico é  sobre  testemunhar.  O método experimental é sobre isto, sobre o fato de estar lá. E o fato de saber certas coisas  por  estar  lá  modifica  o  senso  de  responsabilidade.**  Assim,  longe  de  ser indiferente  à  verdade,  o  enfoque  que  tento  defender  é  rigorosamente comprometido com testar e atestar. **Com se engajar e compreender que este é um empreendimento  sempre  interpretativo,  interessado, contingente,  falível.  Não  é nunca um relato desinteressado.**
> (2015, p. 67)

# Localidade da objetividade e situacionalidade do saber

> Certo [[ tem a ver com uma noção comum de objetividade científica ]],  mas  objetividade  é  sempre  uma  conquista  local.  **Trata-se sempre de manter as coisas juntas bem o bastante para que as pessoas possam participar poderosamente daquele relato. “Local” não significa pequeno ou inapto a viajar.**
> (2015, p. 67)

> A  testemunha  modesta  é  aquela  que  pode  estar  engajada  em conhecimentos situados.
> (2015, p. 67) 

**<center>___</center>**


# Sobre sua perspectiva ser anti-perspectivista (não-relativista)

> Nossa visão _nunca_ foi de que a verdade é apenas uma questão de perspectiva. "A verdade é uma perspectiva" nunca foi nossa posição. Nós íamos contra ela. **A teoria feminista do ponto de vista [_standpoint theory_] sempre foi _anti_ -perspectivista. Assim como era o Manifesto Ciborgue, os conhecimentos situados, as noções vindas da teoria ator-rede [do filósofo] Bruno Latour, e assim por diante.**
> (2022, p. 8)

# Pós-verdade x materialismo semiótico. Corporidade do discurso.

> **A "pós-verdade" desiste do materialismo. Ela desiste do que eu venho chamando de materialismo semiótico: a ideia de que o materialismo é sempre uma produção situada de significados e nunca simplesmente uma representaçãoa**. Essas não são questões de perspectiva. São questões de mundificação em toda sua espessura. **O discurso não é apenas ideias e linguagem. O discurso é corporal. Ele não é incorporado como algo que se prende em um corpo. Ele é corporal e é a corporificação, a mundificação. Isso é o oposto da pós-verdade.** Trata-se de entender como reivindicações fortes sobre o conhecimento não são apenas possíveis, mas _necessárias_ ― pelas quais vale a pena viver e morrer.
> (2022, p. 8)

# Episódio no Brasil sobre se Haraway e Latour "acreditam na realidade"

> Estávamos nesta conferência no Brasil. Era um bando de primatólogos mais eu e Bruno. E o Stephen Glickman, um biólogo muito legal, um homem que nós dois amamos, que lecionou na Universidade de Berkeley por anos e estudava hienas, nos chamou para conversar em particular. Ele disse: “Então, eu não quero causar nenhum constrangimento. Mas vocês acreditam na realidade?”
> 
> Nós dois ficamos meio chocados com a pergunta. Primeiro, **ficamos chocados por essa ser uma questão de _crença_, que é um problema protestante. Um problema confessional. A ideia de que a realidade é uma questão de crença é um legado de certa forma secularizado das guerras religiosas.** Na verdade, **a realidade é uma questão de mundificação e habitação dos mundos. É uma questão de testar a sustentação [_holding-ness_] das coisas.** As coisas se _mantêm_ [_hold_] ou não?
> 
> Veja a evolução. A noção de que você poderia ou não “acreditar” na evolução já acaba com o jogo. **Se você diz: “É claro que acredito na evolução”, você perde, porque com isso você entra na semiótica do representacionalismo ― e, consequentemente, na pós-verdade. Você entra em uma arena onde tudo isso é apenas questão de convicção interna e nada tem a ver com o mundo. Você deixou o domínio da mundificação.**
> 
> Os Guerreiros da Ciência que nos atacaram durante as Guerras da Ciência estavam determinados a nos pintar como construcionistas sociais ― aqueles que acreditam que toda verdade é puramente construída socialmente. Eu acho que entramos nisso. Nós demos espaço para essas leituras equivocadas a nosso respeito de várias maneiras. Poderíamos ter sido mais cuidadosos, ter ouvido e nos envolvido mais devagar. Era muito fácil nos ler da maneira que os Guerreiros da Ciência faziam. **Então, a direita se apropriou das Guerras da Ciência, o que acabou ajudando a alimentar todo o discurso das fake-news.**
> 
> (2022, p. 9)

# Sobre mundo como relacionalidade como mundificação

> Ambos os termos [[ objetividade e relativismo ]] habitam o mesmo quadro ontológico e epistemológico - um quadro que meus colegas e eu temos tentado tornar inabitável. Sandra Harding falava de "objetividade forte" e meu idioma era "saberes localizados". **Nós tentamos desautorizar o individualismo possessivo que vê o mundo como unidades + relações. Pegamos as unidades, misturamos essas unidades com relações, e chegamos aos resultados. Unidades + relações = mundo.**
> (2022, p. 10)

> Não temos unidades + relações. **Temos apenas relações. É mundificação. Toda a história é sobre _gerúndios_ ― mundificando, corporificando, tudo-ndo [_worlding, bodying, everything-ing_].** As camadas são herdadas de outras camadas, temporalidades, escalas de tempo e espaço, que não se encaixam perfeitamente mas que têm geometrias estranhamente modeladas. Nada começa do zero. Mas o jogo ― e acredito que o conceito de jogo é incrivelmente importante nisso tudo ― propõe algo novo, quer seja o jogo entre cachorros ou entre cientistas em seu campo de ação.
> (2022, p. 10)

> Não se trata da oposição entre objetividade e relativismo. **E sim da espessura da mundificação. Trata-se, também, de pertencer a certos mundos e não outros, de estar disponível para alguns mundos e não para outros; trata-se de um comprometimento materialista em muitos sentidos.**
> (2022, p. 10)

# Sobre o essencialismo estratégico

> Até hoje conheço apenas um ou dois cientistas que gostam de falar assim. E há boas razões pelas quais cientistas permanecem muito cautelosos com esse tipo de linguagem. Eu faço parte do movimento Defend Science e na maioria dos eventos públicos que participo, falo com cuidado sobre meus próprios compromissos ontológicos e epistemológicos. Eu uso uma linguagem representacional. E defendo uma objetividade menos forte, porque acho que temos que fazê-lo, situadamente.
> 
> Isso é má-fé? Não exatamente. **Isso está relacionado com [o que a teórica pós-colonial Gayatri Chakravorty Spivak chamou de] "essencialismo estratégico". Há uma estratégia em falar o mesmo idioma das pessoas que estão no mesmo espaço que a gente. Nós criamos um idioma bom o suficiente para que possamos trabalhar em algo conjuntamente. Interessa aquilo que é possível fazermos juntos nesse espaço. E amanhã nós vamos mais longe.**
> 
> Nas lutas em torno das mudanças climáticas, por exemplo, você tem que se juntar a seus aliados para bloquear a máquina cínica, bem-financiada e exterminacionista que está desenfreada na Terra. Acredito que meus colegas e eu estamos fazendo isso. **Não nos calamos, nem desistimos do aparato que desenvolvemos. Mas é possível colocar em primeiro ou em segundo plano o que é mais proeminente, dependendo da conjuntura histórica.**
> (2022, p. 10-11)

# Sobre o contexto de escrita do manifesto ciborgue

> Isso aconteceu logo após a eleição de Ronald Reagan. O futuro que conseguíamos prever era uma guinada intensa à direita. Era o fim definitivo dos anos 1960. Na mesma época, Jeff [[ Escoffier, editor da _Socialist Review_ ]] perguntou se eu poderia representar a Socialist Review em uma conferência sobre Novas e Velhas Esquerdas em Cavtav na Iugoslávia [hoje Croácia]. Eu disse que sim e escrevi um pequeno artigo sobre biotecnologias reprodutivas. Muitas pessoas desembarcaram em Cavtav e havia relativamente poucas mulheres. Então, nós rapidamente nos identificamos umas às outras e formamos alianças com as mulheres da equipe do evento, mulheres que estavam fazendo todo o trabalho reprodutivo, cuidando de nós. Nós acabamos deixando de lado nossos artigos e palestrando sobre diversos tópicos feministas. Foi bastante divertido e bem empolgante.
> 
> Depois dessa experiência, eu voltei à Santa Cruz e escrevi o Manifesto Ciborgue. Ele acabou tendo mais de cinco páginas e sendo uma reconciliação com tudo que aconteceu comigo entre 1980 e o ano que ele foi lançado, 1985.
>
> [[ A proximidade com o vale do silício ]] É parte do ar que você respira aqui. Mas as verdadeiras alianças tecnológicas da minha vida vêm de Rusten, meu parceiro, e seus amigos e colegas de trabalho, porque ele trabalha como desenvolvedor de software freelance. Ele fez trabalhos para a Hewlett Packard por anos. Ele tinha um vasto histórico nesse mundo: quando tinha apenas catorze anos, conseguiu um trabalho programando cartões perfurados para empresas em Seattle.
>
> O Manifesto Ciborgue foi o primeiro artigo que escrevi na tela de um computador. Nós tínhamos um HP-86 velho. E eu o imprimi em uma daquelas impressoras de impacto. Uma impressora da qual eu nunca conseguia me livrar, ninguém queria ela. Ela acabou indo parar em alguma lixeira. Deus nos ajude.
>
> (2022, p. 12)

# Sobre o uso da categoria trabalho no movimento _Wages for Housework_

> O _Wages for Housework_ foi muito importante. E sempre acho que devemos trabalhar com adição e não subtração. Sempre a favor de expandir a ninhada. Assistamos as uniões e as desuniões, as composições e decomposições, à medida que a ninhada se prolifera. **Trabalho é uma categoria importante com uma história forte, e o _Wages for Houseworki_ promoveu uma expansão dela.**
> 
> Mas os pensadores com raízes marxistas também tem uma tendência de fazer a categoria **trabalho** trabalhar demais. **Grande parte das coisas que acontecem precisam ser espessamente descritas com _outras_ categorias ― ou ao menos descritas em emaranhados interessantes que envolvem o conceito de trabalho.**
> 
> (2022, p. 13)

# Trabalho x Jogo

> Jogo é uma [[ outra categoria interessante de se acrescentar à de trabalho ]]. **Trabalho está muito ligado à funcionalidade, enquanto que o jogo é uma categoria da não-funcionalidade.**
> 
> A ideia de jogo diz respeito a muito do que acontece no mundo. **Existe uma espécie de oportunismo barato na biologia e na química, onde as coisas trabalham estocásticamente para formar sistematicidades emergentes. Não é uma questão de funcionalidades objetivas. Precisamos desenvolver maneiras de pensar esses tipos de atividades que não são capturadas pela funcionalidade, que propõem possíveis-ainda-impossíveis, ou coisas que são impossíveis mas que ainda estão em aberto.**
> 
> (2022, p. 13-4)

# Sobre a necessidade de noções positivas (criatividade?) para além de negativas (crítica)

> Parece, acredito eu, que nossa política atualmente exige que demos uns aos outros a força para fazer exatamente isso. Para **descobrir como, juntos uns dos outros, conseguiremos abrir as possibilidades daquilo que ainda pode vir a ser. E não é possível fazer isso num estado mental negativo. Não é possível fazer isso se tudo o que fazemos é criticar. Nós precisamos da crítica, é evidente. Mas não é ela que vai abrir nossas percepções para aquilo que pode vir a ser. Não é ela que vai abrir nossas percepções para aquilo que ainda não é possível, mas é profundamente necessário.**
> (2022, p. 14)

# Jogo e imaginação política em ocupações políticas

> O tanto que as pessoas nessas ocupaçõesi [[ _Graham Common_ ou Dakota Access Pipeline ]]  investem em **jogar é parte crucial de como elas geram uma nova imaginação política**a, o que, por sua vez, aponta para o tipo de trabalho que precisa ser feito. Elas possibilitam a imaginação de algo que não é o que [a etnógrafa] Deborah Bird Rose chamou de “morte dupla” ― exterminação, extração, genocídio.
> [[...]]
> Essa não é uma questão humanista. É uma questão multi-gênero e multi-espécie.
> (2022, p. 14-5)

# Sobre o surgimento de novas questões: a imigação, o exterminismo e o extrativismo

> Sim e não [[ em relação à pergunta sobre se as catástrofes climáticas e ecológicas mudam o que Haraway pensa no Manifesto ciborgue ]]. As teorias que desenvolvi naquele período emergiram de uma conjuntura histórica específica. Se eu estivesse mapeando o circuito integrado hoje, esse mapeamento teria parâmetros diferentes daqueles usados no mapa que fiz no começo dos anos 1980. **E certamente as questões da imigração, do exterminismo e do extrativismo teriam que ser profundamente levadas em conta. A reconstrução da vida em sua ligação com o território em que se vive, teria que receber mais atenção.**
> (2022, p. 15)

# Guinada à direita em 1980 x guinada fascista em 2010

> O Manifesto Ciborgue foi escrito no contexto da acentuada guinada à direita dos anos 1980. **Mas essa guinada foi uma coisa; a acentuada guinada facista do final dos anos 2010 é outra coisa. Não é o mesmo que Reagan. Os presidentes da Colômbia, da Hungria, do Brasil, do Egito, da Índia, dos Estados Unidos ― estamos vendo um novo capitalismo facista, o que coloca a necessidade de reelaborar as ideias do início da década de 1980 para que elas façam sentido.**
> (2022, p. 15)

# Sobre a otimização tecnológica, a vigilância e o cercamento do comum

> Se a dicotomia público-privado era antiquada em 1980, em 2019 nem sei como chamá-la. **Temos que tentar reconstruir algum senso de público. Mas como é possível reconstruir o que é público diante de uma vigilância quase total? E essa vigilância sequer tem um centro único. Não existe um olho que tudo vê.**
> 
> E ainda temos o cercamento contínuo dos comuns. **O capitalismo produz novas formas de valor e em seguida cerca essas formas de valor — o digital é um exemplo especialmente bom.**  Essa dinâmica envolve a monetização de praticamente tudo o que fazemos. E não é como se ignorássemos essa dinâmica. Nós sabemos o que está acontecendo. Só não temos a menor ideia de como lidar com isso.
> 
> (2022, p. 15-16)

# Sobre o xenofeminismo e o foco sobre questões climáticas e multiespécie (e não tecnológicas)

> __Uma tentativa de atualizar as ideias do Manifesto Ciborgue veio das “xenofeministas” do coletivo internacional _Laboria Cuboniks_. Acredito que algumas delas se descreveram como suas “filhas desobedientes”.__
> 
> Exagerar nas coisas, esse não é o meu feminismo.
> 
> __Por que não?__
> 
> Sinceramente, não estou muito interessada nessas discussões. Não é o que estou fazendo.  Não é o que me dá vigor agora. **Em um momento de urgência ecológica, estou mais engajada em problemas de justiça ambiental e reprodutiva multiespécies. Essas questões certamente envolvem problemas de cultura digital, robótica e maquínica, mas elas não são meu foco agora.  O que mais chama minha atenção agora são as questões de soberania no acesso à terra e à água, como as que envolvem o oleoduto _Dakota Access_, ou a mineração de carvão no planalto _Black Mesa_, ou o extrativismo que se dá em todos os lugares.** Tenho concentrado minha atenção nas crises de extermínio e extinção acontecendo a nível global, com o desalojamento e a falta de moradia para humanos e não-humanos. É aí que estão minhas forças. Meu feminismo está nesses outros lugares e corredores.
> 
> (2022, p. 16)

# Sobre a pertinência da figura do ciborgue

> O ciborgue continua sendo uma figura trapaceira e astuta. E, assim, eles também são meio antiquados. Eles dificilmente são atualizados. Eles são bastante desajeitados, um pouco como o R2-D2 ou um marca-passo. **Talvez a nossa digitalidade corporificada não seja capturada especialmente bem pelo ciborgue. Então, não tenho certeza. Mas, sim, acho que os ciborgues ainda estão na ninhada.** Eu acho que precisamos de uma enorme e pretensiosa ninhada, com um monte de pessoas fodas ― algumas até são homens!
> (2022, p. 16)

# Dúvidas quanto a possibilidade de resolução de problemas climáticos por meio da tecnologia

> Então, entro em conflito comigo mesma quando vejo enormes campos solares e parques eólicos, porque, por um lado, eles podem ser melhores do que a hidrofraturação [fracking] no condado de Monterey ― mas só talvez. Também porque sei de onde vêm os minerais raros necessários para o funcionamento das tecnologias de energia renovável e em que condições eles são minerados. Ainda não estamos fazendo a análise de toda cadeia de suprimentos de nossas tecnologias. Sendo assim, acho que temos um longo caminho a percorrer na compreensão socialista dessas questões.
> (2022, p. 17)

# Sobre o Antropoceno

> Eu acho que o Antropoceno enquanto quadro de análise tem sido um termo guarda-chuva fértil para bastante coisa, na verdade. O Antropoceno se revelou um território bastante amplo para a incorporação de pessoas em luta. Há muitas colaborações interessantes acontecendo entre artistas, cientistas e ativistas.
> 
> O principal ponto negativo sobre o termo é que ele perpetua o mal-entendido de que o que aconteceu é um traço humano enquanto espécie, como se os seres humanos como espécie necessariamente exterminassem todos os planetas em que ousamos viver. Como se não pudéssemos parar nossos excessos produtivos e reprodutivos.
> 
> **O extrativismo e o exterminacionismo não são traços da espécie humana. Eles vêm de uma conjuntura situada historicamente há cerca de quinhentos anos, inaugurada com a invenção da _plantation_ e depois com a modelagem do capitalismo industrial. Uma conjuntura situada historicamente que teve efeitos devastadores mesmo quando criou uma riqueza surpreendente.**
> 
> (2022, p. 18)

# Sobre a necessidade de se criar pontos de contato

> Existem diversas outras formas de pensar. A mudança climática, por exemplo. **Atualmente, "mudança climática" é uma categoria necessária e essencial. Mas se você vai para o Círculo Polar Ártico, sendo um cientista do sul que quer colaborar com povos indígenas em questões de mudanças climáticas ― como as mudanças na porção congelada do mar (banquisa), por exemplo, ou as mudanças nos modos de caça e subsistência ―, as limitações dessa categoria serão profundas.** Porque ela não interage com as categorias indígenas que são de fato acionadas e incidem sobre aquele espaço.
> 
> Em inuctitute existe a palavra "sila". Num léxico anglófono, "sila" pode ser traduzida como "clima". Mas, na realidade, ela é muito mais complicada do que isso. No Círculo Polar Ártico, mudança climática é um conceito que reúne um grande número de coisas que cientistas do sul não entenderão. Por isso, para o cientista do sul que deseja colaborar com a questão das mudanças climáticas é quase impossível construir uma zona de contato.
> 
> De toda forma, há diversas outras maneiras de pensar os problemas contemporâneos compartilhados. Mas **elas exigem que consigamos construir zonas de contato entre aparatos cognitivos, zonas das quais nenhuma das partes sairá da mesma forma que entrou. São esses tipos de encontro que precisam acontecer mais.**
> 
> (2022, p. 20)
