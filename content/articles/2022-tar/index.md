---
path: "/teoria-ator-rede"
date: 2022-03-28
title: "Teoria ator-rede"
author: Rafael Gonçalves
featuredImage: image.png
srcInfo: <a href="https://commons.wikimedia.org/wiki/File:SocialNetworkAnalysis.png">Grafo por Martin Grandjean</a>
published: true
category: fichamento
tags:
 - Fabian Muniesa
 - Bruno Latour
 - sociologia
 - teoria ator-rede
---

Fichamento do artigo _Actor-network theory_[^1] que faz um panorama histórico e conceitual da teoria ator-rede.

[^1]: MUNIESA, Fabian. Actor-network theory. _In_: **International Encyclopedia of the Social & Behavioral Sciences**. Vol. 1. Elsevier, pp. 80-4. 2015.

# Abstract

> This article summarizes some important aspects of actor–network theory. It provides relevant elements of the historical origins of this line of inquiry, examines some of its most distinctive concepts, and signals its development in science and technology studies and in some other areas of the social sciences. The article emphasizes the position of actor–network theory within the poststructuralist landscape, discusses its particular treatment of constructivism, and examines in particular the philosophical implications of the notion of translation, which plays a crucial role in the articulation of this approach.

# Surgimento da teoria ator-rede (TAR) em Paris entre 1950 e 80 no _Centre de Sociologie de l'Innovacion_ (CSI) da _École des Mines_

> The expression ‘actor–network theory’ was developed in the late 1980s in order to refer to an emergent school of thought that started to form in Paris, France, a decade earlier. An early mention to an ‘actor–network approach’ is found in the introduction to an edited volume on the sociology of technology (Bijker et al., 1987). The editors used that expression as a tag to identify and differentiate the approach defended by Michel Callon, who contributed to the volume with a seminal ANT case study on the attempted development of an electric vehicle in France (Callon, 1987). Callon had already proposed a different expression, ‘sociology of translation’ (sociologie de la traduction in French), in order to specify the singularity of this contribution, most notably in another seminal ANT case study on scallop farming along the coast of Brittany (Callon, 1986; see also Callon, 1980). But, in a somewhat path-dependent way, it was the notion of ‘actor–network theory’ that prevailed in Anglophone academic circles. The intellectual foundations and historical origins of ANT are located in the concrete institutional site of its inception, in the emergence of French poststructuralism and in debates in the sociology of science.
> (p. 80)

> ANT’s origins are to be found in a very concrete location. The approach was crafted at the Centre de Sociologie de l’Innovation (CSI), a small research center at the École des Mines de Paris, an engineering school that is both an elite higher education institution (a so-called grande école) and a research institution distinctively oriented toward industrial innovation. The CSI, created in 1967 with the purpose of studying the social determinants of innovation processes, had progressively shifted to a more catholic interest on the interrelations between science and society. Michel Callon, an engineer and sociologist by training and École des Mines alumnus whose earlier research focused on the interfaces between science and industry, was appointed director of the CSI in 1982. The same year, the CSI recruited Bruno Latour, a philosopher by training who was already known for his pioneering ethnography of a neuroendocrinology laboratory (Latour and Woolgar, 1979), a study that prefigured the most salient tenets of ANT’s blend of semiotics.
> (p. 80)

# Construtivismo da TAR ~ engenharia

> This localization of the founding of ANTis not accidental. In a sense, the special circumstances that were prompted at the CSI gave ANT some notable characteristics. First, the CSI’s intellectual familiarity and daily contact with engineering (in particular in relation to electric systems and extractive industries) translated into a distinctively industrial and technological understanding of constructivism. For ANT, reality is constructed, but it is constructed in the engineer’s sense (solid reality as the outcome of an organized, fragile, and laborious process of material articulation) rather than in the sense usually put forward in standard social sciences (social construction considered in terms of social conventions, belief systems, mental states, or collective representations).
> (p. 80)

# Adisciplinariedade da TAR (heterogeneidade)

> Second, the CSI’s marginal position in the field of instituted social sciences in France and its relative lack of disciplinary control translated into an interdisciplinary (or rather adisciplinary) attitude. Consequently, ANT questions the boundaries between disciplines and, more profoundly, between the social and the natural sciences, favoring approaches that are able to cope with heterogeneous realities
> (p. 80-1)

# Posiciona a/o pesquisador/a no emaranhado da prática

Mas quanto isso poderia ser benéfico a partir de um contrato com uma entidade privada?

> Third, the tradition of pursuing contract research and developing applied expertise at the CSI, and the combination of highly theoretical developments with extremely practical research reports, translated into a novel preference for hybrid styles. ANT indeed portrays the researcher in general, and the social scientist in particular, as an entangled participant rather than as an external observer viewing reality from above.
> (p. 81)

# Pós-estruturalismo francês como contexto da TAR. Tradução (Michel Serres). Entendimento materialista da significação (D&G). Semiótica - actantes (Greimas).

> The formation of ANT is, in part, an episode in the history of French poststructuralism and a consequence of the intellectual atmosphere prompted in Parisian academic circles by authors such as Michel Serres (see Dosse, 1999; Serres and Latour, 1995). The constructivist approach to science defended by Serres in the 1970s and his distinctive use of the notion of translation played a pivotal role in the intellectual education of early contributors to ANT.
> (p. 81)

> The philosophical take on a materialist understanding of signification defended by Gilles Deleuze and the decisive impact that his collaboration with Félix Guattari had on a number of French intellectuals in the 1970s and 1980s was also at work in the emergent repertoire of ANT, especially with their notions of rhizome and collective assemblage of enunciation.
> (p. 81)

> The approach to semiotics constructed by Algirdas Julien Greimas (his actantial model in particular) was crucial in the development of ANT’s interpretation of the operations of signs and texts.
> (p. 81)

> Other notable influences on the formation of ANT can be located in the works of authors such as Michel Foucault, Gilbert Simondon, François Dagognet, André LeroiGourhan, and Michel de Certeau, who were widely read at the CSI in the 1980s.
> (p. 81)

# Estudos sociais da ciência e tecnologia como contexto da TAR. Abordagem materialista para agência e entendimento construtivista da verdade. Realismo construtivista da TAR.

> The formation of ANT is, also in part, an episode in the history of science and technology studies, one mainly characterized by a materialist approach to agency and a constructivist understanding of truth (see Pickering, 1992, 1995).
> (p. 81)

> But what is ruled out from this perspective [[ construção social da verdade na epistemologia francesa ]], ANT would claim, is the relevance of any empirical account of the concrete operations (experimenting, measuring, calculating, writing, communicating) that are needed in order to obtain scientific facts – precisely the type of inquiry envisaged by ANT.
> (p. 81)

> Accordingly, their response [[ Barry Barnes e David Bloor, em resposta ao empiricismo inglês ]] emphasized the role of social interests and collective representations in the establishment of scientific truth and falsehood. Seen from this angle, ANT’s emphasis on the study of scientific controversies (situations in which scientific truth and falsehood are still in the making) was appreciated, but its focus on the empirical operations of science and on the material conditions of scientific work appeared as a fatal step backward toward scientific realism and positivism. As exemplified in numerous debates (for exemplary studies, position statements and debates, see Knorr-Cetina and Mulkay, 1983; Bijker et al., 1987; Law, 1986; Pickering, 1992), ANT was trying to be constructivist and realist at the same time, by accepting that scientific truth and scientific reality do exist, but not without a painstakingly material process of instauration (Latour, 1987, 1999a).
> (p. 81)

# Não uma insistência na agência não-humana, mas uma consideração de todas as formas de agência

> The point is twofold. First, the events that need to be accounted for are the conjunction of all kinds of agencies, the variety of which cannot be subsumed under a simple human/nonhuman or intentional/unintentional divide. For example, in the microbiology laboratory, action is prompted by personal purposes, unconscious motives, physical bodies, social institutions, corporate actors, living organisms, technological devices, gravitational forces, atmospheric conditions, budgetary constraints, collective ideologies, measurement instruments, legal codes, etc. In short, as soon as something happens, there is action to be accounted for, and a good ANT account does not single out any particular form of action, be it social or otherwise. Second, the fact that accounts are usually biased in favor of one form or other of ‘purified’ agency, is due, ANT claims, to the legacy of modern reason, that is, to a style of thought that is most distinctively characterized by the instauration of a neat divide between nature and culture, or between the physical condition of the world and its political constitution. This is both a bias that needs to be overcome in order to account for the proliferation of hybrid realities that modernity itself prompted (hence the need to develop an appropriate vocabulary) and an object of anthropological inquiry as such, as posited in Bruno Latour’s ‘anthropology of modern reason’ (Latour, 1993, 2013).
> (p. 82)

# Foco em processos coletivos ao invés de agência individual

> On the question of the nature of human agency alone, ANT is known for contributing to a double refutation: that of the modern, liberal ideal of the sovereign, emancipated individual and that of the equally modern, sociological ideal of the dominated, conditioned social agent. The idea of using actors’ interests as ultimate explanatory factors was replaced, in early contributions, by the idea of studying processes of ‘interessement’ and elaborating how they come about (Callon and Law, 1982; Callon, 1986). The pivotal notion of ‘attachment’ and the notable case studies on drug addiction and music lovers that accompanied its development (Gomart and Hennion, 1999; Latour, 1999b; Hennion, 2005, 2007) allowed ANT to shift attention from the ‘doing’ to the ‘making do,’ and from the ‘moving’ to the ‘being moved,’ as a constitutive feature of humanity.
> (p. 82)

# Uso de uma semiótica actancial (Greimas) e materialista nos estudos de tecnologia. Referencia/significação como operações materialmente traçaveis de deslocamento, trasnporte e circulação. Circulação como tradução. Metafísica das operações de tradução. Ação-rede como forma de não distinguir agentes e ações (rede que age, ator que circula).

> The analysis of how a program of action or a script of conduct is inscribed or not into a material artifact, and of how it interacts with other programs or other scripts, stood as one key ingredient of the ANT recipe (Akrich, 1992; Latour, 1992). The semiotic, actantial analysis of scientific publications was accompanied by quite novel developments, at the time, that traced materially the networks formed by texts, for example, through coword analysis and bibliometric maps (Callon et al., 1986). Inventing new social-scientific literary styles altogether was also part of ANT’s original endeavor (Latour, 1996).
> (p. 82)

> ANT’s particular blend of material semiotics is intrinsically pragmatist. Instead of considering reference or signification from the viewpoint of a binary partition between signs and things (or between mind and matter), ANT considers reference as a materially traceable operation of displacement, transportation, and circulation. As demonstrated by Latour in his minute examination of the scientific power of Louis Pasteur, the ability to refer to the anthrax bacillus within the laboratory setting; the ensuing ability to refer, in entirely new terms, to a disease affecting cattle; and the ability to transform an entire country altogether (its cattle, its farmers, its inhabitants, its bacteria, and its economy) can be traced back to the material organization of referential operations through which all these realities circulate (Latour, 1983; see also Latour, 1988).
> (p. 82)

> A translation, in ANT’s sense, is an operation that transforms one particular problematic statement into the language of another particular statement: tackling problematic statement A in its own terms means in fact shifting terms and therefore tackling problematic statement B. This operation of translation transforms the issues at stake and the configuration of relations that link statements to one another. It also modifies acting capacities (since these statements are claims for conduct, and their strength is synonymous with power to act), and it engenders agency (since an operation of translation is both an empirical act and the condition of the configuration of a capable actor).
> (p. 83)

> But it also contains the theoretical potential of a nondualistic form of metaphysics: all that there is a network of ongoing operations of translation that define the agency of problematic statements. There is no need to revert to a distinction between an inside and an outside, or between actors and actions. The very notion of an ‘actor–network’ (a notion that spread through what was first labeled ‘sociology of translation’ but then became ‘actor– network theory’) conveys this intuition quite well: instead of considering a distinction between actors (that act) and networks (which actors form among themselves, in order, for example, to act collectively), the ‘actor–network’ is both the ‘network-that-acts’ and the ‘actor-that-springs-from-a-network,’ precisely what is constituted through processes of translation.
> (p. 82)

# TAR como sociologia (generalização de uma visão política para entender a constituição de mundos)

 > It does so in a quite particular sense: as a science of the formation and deformation of collective worlds, worlds that are constituted precisely through the material semiotic processes that ANT sets out to describe. This is ‘sociological’ not in the sense that it emphasizes ‘social factors,’ but rather in the sense that it generalizes a political understanding of the constitution of these worlds. ANT indeed is recognizable through the use of words imported from political science (e.g., ‘spokesperson,’ ‘parliament,’ ‘association,’ ‘representative’) in order to analyze, say, a laboratory and all its constituents. ANT can hence be seen as the generalization of a political frame to the understanding of all types of assemblages, not only human assemblies.
 > (p. 83)

# Impactos da TAR nas políticas tecnológicas através de termos como parlamento das coisas (inovação responsável)

> Notions developed in ANT, such as ‘hybrid forum’ or ‘parliament of things,’ take into account both the democratic appraisal of issues of a technical nature – issues that provoke the emergence of new entities and new collectives – and the consideration of the constitution of nature as a political endeavor. Aspects of these ideas have had some impact in science and technology policy literature, especially through the topic of responsible innovation.
> (p. 83)

# Relações da TAR com pragmatismo (Peirce, Whitehead, Dewey, ...) e com Gabriel Tarde

> In the 1990s and 2000s, and in parallel to progress in a wide variety of empirical fields, proponents and commentators of ANT have engaged in a number of philosophical debates. In particular, ANT has reclaimed affiliation with the pragmatist tradition in philosophy, most notably with authors such as William James, John Dewey, Charles S. Peirce, and Alfred North Whitehead. The breadth of a nondualist approach to knowledge and of a materialist, semiotic approach to reality in pragmatism marks ANT’s entanglement with this tradition. Rapprochement with the philosophical viewpoint of Gabriel Tarde obeys a similar logic, with an emphasis on contesting the dominant, modern understanding of society. Latour’s philosophical elaborations also have played a pivotal role in the development of new approaches to metaphysics that focus attention on the reality and effectuation of objects (Harman, 2009; Latour et al., 2011).
> (p. 83)

# Vertente da TAR engajada em questões filosóficas e políticas da contemporaneidade

> Two versions of ANT, it can be said, are in circulation in the early first quarter of the current century. One, certainly more attached to the vocabulary developed in the 1980s and to the topics of inquiry that have most distinctively characterized the label ‘ANT’ through the years, works today as a widely used and acknowledged perspective in the social sciences, particularly appropriate for the study of complex and controversial situations. Another one, rather experimental and philosophical in nature, shifts in a sense from exploitation to exploration, perhaps at the expenses of the neatness of the label ‘ANT,’ and engages with a number of serious issues of political philosophy. Perhaps the most crucial issue, which corresponds indeed to recent undertakings of Latour (2013), can be formulated as follows: how can a perspective that emerged as a frontal critique of the intellectual categories of modernity, including the political ones, consider ideals of political constitution (or political insurrection) that are nonetheless suitable for the maintenance and preservation, if not reparation, of a world largely inherited from modernity itself?
> (p. 83-4)
