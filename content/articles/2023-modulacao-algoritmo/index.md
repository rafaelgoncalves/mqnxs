---
title: A noção de modulação e os sistemas algorítmicos
tags:
    - Sérgio Amadeu da Silveira
    - sistemas algorítmicos
    - modulação
    - inteligência artificial
    - neoliberalismo
    - patentes
    - fichamento

date: 2023-06-05
category: fichamento
path: "/modulacao-sistemas-algoritmicos"
featuredImage: "sa.jpg"
srcInfo: <a href="https://commons.wikimedia.org/wiki/File:S%C3%A9rgio_Amadeu_(3972863002).jpg">Sérgio Amadeu (2009)</a>
published: true
---

Fichamento do texto "A noção de modulação e os sistemas algorítmicos"[^1] do sociólogo Sérgio Amadeu da Silveira.

[^1]: SILVEIRA, S. A. DA. A noção de modulação e os sistemas algorítmicos. **PAULUS: Revista de Comunicação da FAPCOM**, v. 3, n. 6, 12 nov. 2019. 

# Abstract

> O texto apresenta a noção de modulação. Busca demonstrar que **conceitos gerados para a compreensão dos fenômenos da comunicação de massas são inadequados para tratar dos processos comunicacionais nas redes e plataformas digitais**. Também expõe a **análise de patentes como uma possibilidade de esclarecer a gestão algorítmica das plataformas, a dinâmica e as finalidades dos processos de modulação nas redes de relacionamento on-line e na utilização das tecnologias de informação e comunicação.**

# Modulação (Deleuze, Simondon) como operação fundamental da comunicação no capitalismo neoliberal

> Este texto contém uma reflexão sociológica sobre alguns processos existentes nas pla-taformas de comunicação e relacionamento on-line que podem ser descritos pela noção de modulação e nela enquadrados. O termo “modulação”, também utilizado no texto de Gil-les Deleuze sobre as sociedades de controle, foi recuperado dos escritos sobre tecnologia de Gilbert Simondon. É importante ressaltar que a reflexão aqui apresentada não seguirá na direção do debate ontológico, metafísico ou filosófico. **A pretensão, aqui, é levantar alguns pontos para a análise da modulação como expediente fundamental da comunicação no ca-pitalismo, em sua fase neoliberal.**
> (p. 17-8)

# Relevânicia das redes sociais no contexto de emergência da Internet

> As plataformas foram adquirindo relevância conforme a internet se popularizava, principalmente, a partir dos anos 2000, com o sucesso dos sites que permitiam relacionamentos entre pares, com a explosão do compartilhamento nas chamadas redes P2P (peer-to-peer). O sucesso da cultura do compartilhamento foi reconhecido pelo mercado, que buscou operar a capitalização desse modelo. **A audiência dos sites produtores de conteúdos foi superada pelas plataformas de interação em que os usuários produziam as matérias e os objetos.** O surgimento e o espraiamento dos blogues já haviam demonstrado que a colaboração e a produção distribuída de conteúdos eram práticas envolventes e atraentes de milhares de pessoas. Em 2003, foi lançado o LinkedIn. Em janei-ro de 2004, o Orkut foi inaugurado, e em fevereiro, o Facebook. O YouTube foi criado em 2005 e o Twitter nasceu em 2006. **O êxito dessas plataformas incentivou a proliferação de modelos de negócios baseados na intermediação entre ofertantes e demandantes de serviços e mercadorias.** O Airbnb surgiu em 2008 e o Uber no ano seguinte.
> (p. 18)

# Mudanças P2P -> Streaming em 2010s

> Em 2009, as redes P2P representavam mais de 50% do tráfego da internet. Todavia, a indústria do copyright trocou sua estratégia de criminalização do compartilhamento de arquivos digitais pela apropriação privada do trabalho colaborativo, pelo barateamento dos serviços e produtos digitais e pela negociação das informações dos seus usuários nos mercados de dados pessoais. **Hoje, os serviços pagos em plataformas de streaming audiovisuais representam mais de 60% do fluxo do tráfego nas redes digitais. Em outubro de 2008, o Spotify iniciou sua operação.** Em 2011, o Netflix já contava com 23 milhões de assinantes apenas nos Estados Unidos. O Instagram começou a operar em 2010 e foi adquirido pelo Facebook em 2012. O Waze foi lançado em 2008 e adquirido pelo Google em 2013.
> (p. 18)

# Ampliação do acesso à internet com a emergência do celular

> A popularização e a queda dos preços dos smartphones, bem como a expansão das re-des wi-fi ampliaram a conectividade e o tempo médio de utilização da internet. **Esse fato aumentou o poder de intermediação das operadoras de telecomunicação, que havia sido minimizado com a expansão dos serviços de voz e imagem sobre IP da internet, mas não reduziu a força das plataformas.**
> (p. 18-9)

# A coleta de dados para o marketing, o microtargeting, os filtros-bolha

> **As plataformas ganharam ainda mais poder quanto mais armazenavam dados dos seus clientes para, com isso, construírem amostras que permitiam às empresas de _marketing_ atingirem com precisão o público que buscavam influenciar.** **O _microtargeting_ é muito mais eficiente do que as técnicas massivas de propaganda.** O mundo industrial forjou tecnologias que não eram as mais propícias para a coleta de dados, mas **as tecnologias da informação permitiam realizar as transações e, simultaneamente, gerar dados sobre como elas ocorre-ram e quem as realizou** (SRNICEK, 2017). **O Big Data, o _machine learning_ e os sistemas de algoritmos preditivos aprimoraram a capacidade de tratar e analisar as informações obtidas nas plataformas.** Eli Pariser (2011) alertou-nos que essas plataformas filtram nossa comunicação, analisam nossos comportamentos e nos inserem em bolhas de pessoas semelhantes.
> (p. 19)

# O mercado de dados, os problemas legais e o consentimento do usuário

> Um grande mercado de dados e uma microeconomia da interceptação de informações pessoais se fortaleceram a partir do final da primeira década do século XXI (SILVEIRA, 2017). **A limitação desse mercado só pode ser dada pelas legislações de proteção de dados pessoais e pelo direito à privacidade.** Todavia, Shoshana Zuboff (2015) mostrou-nos que as corporações que operam essa economia atuam nos vazios legais e nas fragilidades das leis e dos seus órgãos de execução. Diante do avanço das gigantescas plataformas norte-america-nas e chinesas, diversas empresas dos demais mercados passaram a temer pelos seus negó-cios e se somaram aos esforços dos ativistas pelos direitos digitais em busca da aprovação de legislações de proteção de dados. Mesmo assim, **tais peças legais, em geral, não podem impedir ou simplesmente bloquear as plataformas que se alimentam de dados pessoais, uma vez que seu tamanho e a popularização de seu modelo de gratuidade não têm como ser repentinamente revertidos.** Em geral, essas leis baseiam-se no consentimento inequívoco e consciente dos usuários de que seus dados serão coletados e compartilhados. **Obviamente, o efeito desse consentimento é pequeno, pois as pessoas, na maioria das vezes, não têm opção de negar a entrega de determinados dados diante da necessidade de uso do serviço.**
> (p. 19)

# Modulação de percepções e comportamentos a partir de datos produzidos por usuários no capitalismo de vigilância

> As plataformas se alimentam de **dados pessoais que são tratados e vendidos em amostras com a finalidade de interferir, organizar o consumo e as práticas dos seus clientes.** Em geral, os conteúdos desses espaços virtuais **são produzidos ou desenvolvidos pelos próprios usuários que, ao mesmo tempo, entregam seus dados pessoais e seus metadados de nave-gação para os donos desses serviços.** Desse modo, não há nenhum exagero em nomear o capitalismo informacional como capitalismo de vigilância (ZUBOFF, 2015). Aqui, podemos realçar que **a grande concentração das atenções e do dinheiro dos demais segmentos da economia nas plataformas se dá porque elas conseguem modular as percepções e os comportamentos em escala inimaginável até a sua existência.**
> (p. 19)

# Papel de organização de conteúdos produzidos por usuários

> Aqui pretendo mostrar que a noção de modulação é mais adequada para tratar dos processos de forma-ção de opinião nas plataformas de relacionamento on-line, especialmente nas chamadas redes sociais. No mundo pré-internet, o discurso das mídias era o que exercia maior impacto. A escassez induzida de canais de comunicação propiciava a concentração das atenções em um conjunto de produtores e distribuidores de narrativas. **No mundo da internet, na fase do predomínio das plataformas, os conteúdos são produzidos de modo distribuído, mesmo que assimétrico, e são por elas organizados.**
> (p. 20)

# Caráter centralizado das plataformas no que diz respeito ao controle dos fluxos informacionais (~Castells)

> A organização do que é postado e disposto nos circuitos fechados das plataformas não é realizada livremente por seus criadores. **As plataformas têm sua própria arquitetura de informação, que é centralizada – completamente diferente da topologia distribuída da internet. O fluxo de acesso aos conteúdos também é definido pelos gestores das plataformas.** A descrição do sociólogo Manuel Castells parece descortinar o processo de controle existente nessas redes fechadas:
> 
> > Em um mundo de redes, a capacidade para exercer controle sobre os outros depende de dois mecanismos básicos: 1) a capacidade de constituir redes e de programar/reprogra-mar as redes segundo os objetivos que lhes atribuam; e 2) a capacidade para conectar diferentes redes e assegurar sua cooperação compartilhando objetivos e combinando re- cursos, enquanto se evita a competência de outras redes estabelecendo uma cooperação estratégicas. (CASTELLS, 2009, p. 76).
> 
> (p. 20-1)

# Controle por meio de modulação de opções e de caminhos de interação e de acesso

Poder que age diretamente sobre o meio (UI) para agir indiretamente sobre a usuária.

> As plataformas reúnem pessoas que querem ou necessitam se agrupar ou pertencer às redes de amizade, negócios, afetos, entretenimento. Como integrantes, essas pessoas têm o poder de entrar ou abandonar a plataforma, muito diferente do poder que os gestores ou que os donos dessas redes privadas detêm. **Um dos principais modos de controle que os gestores das plataformas exercem sobre seus usuários é a modulação das opções e dos cami-nhos de interação e de acesso aos conteúdos publicados.**
> (p. 21)

# Modulação como operação organizadora de conteúdos para usuárias

> A modulação é um processo de controle da visualização de conteúdos, sejam discursos, imagens ou sons. As plataformas não criam discursos, mas contam com sistemas de algorit-mos que distribuem os discursos criados pelos seus usuários, sejam corporações ou pessoas. Assim, os discursos são controlados e vistos, principalmente, por e para quem obedece aos critérios constituintes das políticas de interação desses espaços virtuais. Para engendrar o processo de modulação não é preciso criar um discurso, uma imagem ou uma fala, apenas é necessário encontrá-los e destiná-los a segmentos da rede ou a grupos específicos, conforme critérios de impacto e objetivos previamente definidos.
> (p. 21)

# Modulação ~ marketing ~ mercado de anúncios. Amostras ~ bolhas.

> Para modular é necessário reduzir o campo de visão dos indivíduos ou segmentos que serão modulados. **É preciso oferecer algumas alternativas para se ver.** **A modulação encurta a realidade e a multiplicidade de discursos e serve assim ao _marketing_.** Os sistemas algorítmicos filtram e classificam as palavras-chave das mensagens, detectam sentimentos, buscam afetar decisivamente os perfis e, por isso, **organizam a visualização nos seus espaços para que seus usuários se sintam bem, confortáveis e acessíveis aos anúncios que buscarão estimulá-los a adquirir um produto ou um serviço.** A modulação opera pelo encurtamento do mundo e pela oferta, em geral, de mais de um caminho, exceto se ela serve aos interesses de uma agência de publicidade, instituição ou uma corporação compradora. Assim, ficamos quase sempre em bolhas – **que prefiro chamar de amostras** –, filtradas e organizadas conforme os compradores, ou melhor, anunciantes.
> (p. 21)

# Modulação ~ mercado de dados pessoais. Captura, perfilamento -> modulação.

> Para que o processo de modulação seja eficiente e eficaz, as plataformas precisam conhecer bem cada um que interage em seus espaços ou dispositivos. Por isso, **a modulação é um recurso-procedimento do mercado de dados pessoais e um estágio na cadeia da microe-conomia da interceptação de dados pessoais.** **A captura ou a colheita de dados é o primeiro passo. O armazenamento e a classificação desses dados devem ser seguidos pela análise e formação de perfis.** Diversos bancos de dados podem ser agregados a um perfil pelas possibilidades trazidas pelo Big Data. Os sistemas algorítmicos modelados como aprendizado de máquina devem acompanhar os clientes das plataformas em cada passo, reunindo informações precisas sobre os cliques dados, os links acessados, o tempo gasto em cada página aberta, os comentários apagados, entre outros.
> (p. 21)

[[ Acho arriscado considerar que esses passos são, na prática, separados. O modelo de aprendizado de máquina perfila quando captura e age modulando quase que instantaneamente enquanto acompanha o cotidiano. Treinamento e inferência se misturam em um algorítmo funcionando _online_, isto é, que treina enquanto infere. ]]

> O processo de modulação começa por identificar e conhecer precisamente o agente modu-lável. O segundo passo é a formação do seu perfil e o terceiro é a construção de dispositivos e processos de acompanhamento cotidiano constantes e, se possível, pervasivos. O quarto passo é atuar sobre o agente para conduzir seu comportamento ou opinião. 
> (p. 21-2)

# Sobre as patentes como fonte de descrição tecnológica

> A patente é um instituto importante do capitalismo. **Corporações registram patentes para aplicá-las  e  também  para  impedir  que  os  concorrentes utilizem  seus  modelos,  inventos  e  procedimentos sem a devida remuneração do titular,** isto é, daquele que as registrou, passando a ser seu dono. Todavia, **nem todas as patentes são utilizadas, muitas servem para bloquear uma tecnologia ainda em pesquisa ou simplesmente como munição em uma guerra contra outras empresas.** Como os procedimentos e os sistemas algorítmicos das plataformas são obscuros, **a análise do texto das patentes, mesmo genérico, pode nos ajudar a compreender a dinâmica ofuscada e invisível aos usuários.**
> (p. 22)

# Exemplo em patente

> Para ilustrar esse processo, vamos observar a patente da Samsung denominada “Apparatus and method for determining user’s mental state”, em português, “Aparelho e método para determinar o estado mental do usuário”. A solicitação de patente, registrada no escritório coreano em 9 de novembro de 2012 e no escritório norte-americano, em 8 de novembro de 2013, com o número US9928462-B2, permite obter informações fundamentais para o processo de modulação, seja na formação do perfil, seja no acompanhamento cotidiano do agente. Seu resumo é esclarecedor:
> 
> > Um aparelho para determinar o estado mental de um usuário em um terminal é fornecido. O aparelho inclui um coletor de dados configurado para coletar dados do sensor; um processador de dados configurado para ex-trair dados de recursos do sensor; e um determinador de estado mental configurado para fornecer os dados do recurso a um modelo de inferência para determinar o estado mental do usuário . (LEE, 2013).
> 
> (p. 22)

> A descrição da patente em questão nos permite compreender o potencial de modulação dos dispositivos mediadores de nossa comunicação:
> 
> > [...] o estado mental pode incluir uma ou mais de uma emoção, um sentimento e um estresse, cada um dos quais pode ser classificado em vários níveis inferiores. Por exemplo, emoção pode ser classificada em felicidade, prazer, tristeza, medo etc. sentimento pode ser classificado em bom, normal, deprimente etc.; e o estresse pode ser classificado em alto, médio e baixo.
> 
> Mas como é possível identificar tais sensações e sentimentos? A patente nos dá uma indicação:
> 
> > [...] quando a velocidade de digitação usando um teclado é de 23 caracteres por minu-to, a frequência de uso da tecla de retrocesso é três vezes ao escrever uma mensagem, a frequência de uso de um sinal especial é cinco vezes, o número de tremores de um dispositivo é 10, uma iluminância média é de 150 Lux, e um valor numérico de uma localização específica (por exemplo, estrada) é 3, um estado de emoção classificado apli-cando os dados do recurso ao modelo de inferência é “susto”, com um nível de confiança de 74%. (LEE, 2013).6
> 
> O conhecimento do estado emocional dos agentes é um dos elementos importantes para que o processo de modulação seja bem-sucedido. Existem 5.162 patentes consideradas si-milares à patente da Samsung, aqui descrita, registradas ou aguardando o registro final nos principais escritórios de patentes.7 Destas, 7,4% são da Samsung Electronics Coreana; 4,5% são da Samsung Electronics dos EUA; 3,9% são da Microsoft Technology Licensing, Llc; 3,3% do Google Inc.; 3,2% da Microsoft Corporation; 3,1% da Apple Inc.; 2,7% do Face-book, Inc.; 2,5% da IBM; 1,1 % da Amazon Technologies, Inc.; 1% do Linkedin Corpora-tion; 1% do Ebay Inc.; 1% do Yahoo! Inc.
> (p. 22-3)

# Outros exemplos de patente sobre modulação

> As plataformas on-line possuem outras patentes esclarecedoras e que confirmam a de-finição do processo de modulação aqui descrito. São milhares delas, das quais apresento cinco, cuja denominação é suficiente para dar uma noção de sua finalidade:
> 
>   - US2010088607-A1 – Sistema e método para manter o usuário sensível ao contexto (Yahoo);
>   - US2012272338-A1 – Gerenciamento unificado de dados de rastreamento (Apple);
>   - US2012226748-A1 – Identifique especialistas e influenciadores em uma rede social (Facebook);
>   - US2018019226-3A1 – Prever o estado futuro de um usuário de dispositivo móvel (Fa-cebook);
>   -  US20080033826-A1 – Fornecimento de anúncios baseados no humor e na personali-dade (Pudding Ltd);
> 
> (p. 23)

# Centralidade de aprendizado de máquina para estruturação de processos de modulação

> Com a utilização de algoritmos, principalmente de _machine learning_, as plataformas conseguem estruturar processos de modulação, desenvolvidos para delimitar, influenciar e reconfigurar o comportamento dos interagentes na direção que os mantenha disponíveis e ativos na plataforma ou que os faça clicar e adquirir os serviços, produtos e ideias nego-ciados pelos donos do empreendimento. A modulação depende dos sistemas de algoritmos e da estrutura de dados ampla, vasta e variada dos viventes, dentro e fora das plataformas e redes digitais. **Como nos alertou Deleuze (1992), a modulação passa a ser contínua e o marketing se tornou a principal forma de controle social.**
> (p. 23-4)

# Neoliberalismo nas redes digitais

> O neoliberalismo é a atual doutrina do capital. **Pode ser visto como a nova racionalidade do capitalismo**  (DARDOT;  LAVAL,  2017).  A  doutrina neoliberal interfere e tem implicações no desenvol-vimento da internet e de suas invenções. **Além disso, o pensamento neoliberal opera nas redes digitais e plataformas com a finalidade de anular e dissipar todas as ações coletivas que criem outras lógicas que não sejam voltadas à concorrência e à reprodução do capital. [[ nos termos de Dardot e Laval, tomam a empresa como norma institucional-subjetiva ]]** Os processos de espetacularização que já existiam no mundo industrial se intensificaram no cenário informacional e foram reforçados nas redes sociais embaladas pelo contexto neoliberal.
> (p. 24)

# Automatização pelas tecnologias naturalizada ~servidão maquínica em Guattari

> A modulação nas plataformas digitais tem servido, principalmente, à expansão do neoliberalismo. O marketing utiliza as corporações para moldar nossas subjetividades e formatar nossos afetos. Robôs têm lido nossos e-mails mais íntimos e apresentado respostas possíveis ao nosso remetente. **Isso passa despercebido para grande parte das pessoas e tem sido compreendido como “algo natural da tecnologia”.** O poder de tratamento das informações é legitimado por um entorpecimento subjetivo diante das vantagens oferecidas pelas tecnologias apresentadas pelas corporações. **São tecnologias que reforçam o que Guattari chamou de “servidão maquínica”.** Ao organizar nossas práticas cotidianas em torno dessas corporações, passamos de utilizadores a dependentes de suas tecnologias.
> (p. 24)

# Modulação ~ aceleração da concorrência

> A lógica da concorrência foi apontada por Foucault (2008) como a lógica estruturante do pensamento neoliberal. **As pessoas permanentemente conectadas têm seus dados sucessivamente coletados por sistemas algorítmicos, que municiarão processos de modulação extremamente úteis à aceleração da concorrência.** Quem não conhecer profundamente seus possíveis consumidores será derrotado ou engolido no cenário neoliberal, por isso a crescente aposta nessa microeconomia da intrusão e da intercep-tação de dados pessoais.
> (p. 24)

> As técnicas de modulação são imprescindíveis para que as corporações pratiquem o marketing certeiro, específico e personalizado. **Quanto mais dependente dos dispositivos tecnológicos que coletam dados, mais as pessoas terão seus perfis comportamentais e opinativos organizados e analisados como parte de um processo que culminará no encurtamento do mundo, da condução da visão e na entrega de opções delimitadas.** Os sistemas algorítmicos preditivos das plataformas querem conhecer cada vez mais as pessoas para melhor atendê-las e “fidelizá-las”. **A munição dessa guerra concorrencial são os dados obtidos de cada pessoa para nutrir o processo de modulação, sem o qual não será possível manter-se, nem vencer os concorrentes.**
> (p. 24-5)

# Possibilidades de resistência

> O neoliberalismo se fortalece na modulação, mas também gera resistências. Suas tentativas de redução da vida à concorrência de mercado e ao enaltecimento da empresa como unidade principal e basilar da sociedade são denunciadas na esfera pública automatizada (PASQUELE, 2017). Nesse sentido, **nas redes digitais e nas plataformas as modulações do capital e de suas forças político-culturais convivem com tecnopolíticas antissistêmicas e com articulações pós-capitalistas que ainda não conseguiram superar o axioma do capital, mas resistem à sua supremacia.**
> (p. 25)
