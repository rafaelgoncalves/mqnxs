---
title: Sujeição e servidão no capitalismo contemporâneo
tags:
    - Maurizio Lazzarato
    - Karl Marx
    - Gilles Deleuze
    - Félix Guattari
    - sujeição social
    - servidão maquínica
    - capitalismo cognitivo
    - trabalho
    - intelecto geral
    - capitalismo
    - fichamento

date: 2024-07-04
category: fichamento
path: "/sujeicao-servidao"
featuredImage: "lazzarato.jpg"
srcInfo: Por Centre for Digital Cultures - <a href="//commons.wikimedia.org/wiki/Category:Vimeo" title="Category:Vimeo">Vimeo</a> <a rel="nofollow" class="external text" href="https://vimeo.com/93379160">93379160</a> (<a rel="nofollow" class="external text" href="//web.archive.org/web/*/https://vimeo.com/93379160"><span style="color:#9602fc">view archived source</span></a>), <a href="https://creativecommons.org/licenses/by/3.0/us/deed.en" title="Creative Commons Attribution 3.0 us">CC BY 3.0 us</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=58445436">Hiperligação</a>
published: true
---

Fichamento do texto "Sujeição e servidão no capitalismo contemporâneo"[^1] de Maurizio Lazzarato.

[^1]: LAZZARATO, Maurizio. Sujeição e servidão no capitalismo contemporâneo. **Cadernos de Subjetividade**, n. 12, p. 168–179, 2010.

> 1: Este artigo constitui o primeiro capítulo de um livro inédito de Maurizio Lazzarato, provisoriamente intitulado _O existencial e o discursivo_, a ser publicado em breve na França.

[Talvez se trate do livro _Signos, máquinas, subjetividades_ publicado em 2014 no Brasil (n-1/sesc) cujo primeiro capítulo trata das duas categorias aqui apresentadas]

# Sujeição social ~ subjetivação neoliberal

> O capitalismo conhece duas modalidades de produção, de tratamento e de exploração da subjetividade: a sujeição social e a servidão maquínica. **A sujeição social, ao nos prover de uma subjetividade, ao nos assinalar uma identidade, um sexo, uma profissão, uma nacionalidade, etc., produz e distribui papéis e lugares.**  Ele constitui uma armadilha significante e representativa à qual ninguém escapa.  **A sujeição social produz um “sujeito individuado” cuja forma paradigmática, no capitalismo neoliberal, é a do “empresário de si”.** Todas as funções, todos os lugares que a sujeição distribui, devem ser assumidos como funções e lugares que nós escolhemos e nos quais nos realizaremos ao investir, como todo bom empresário, a integralidade de nossa vida. Em todos os domínios, quer se trate de produção, de formação, de consumo ou de comunicação, somos convidados a nos comportar como “empresários” que assumem todos os riscos e custos de sua atividade.  A micro–empresa, o auto–empresário, o capital humano celebram o casamento entre o individualismo econômico e o político.
> (p. 168)

# Servidão maquínica como dessubjetivação (objetificação?)

> Na servidão maquínica, o individuo não é mais instituído como sujeito (capital humano ou empresário de si). Ao contrário, **ele é considerado como uma peça, como uma engrenagem, como um componente do agenciamento “empresa”, do agenciamento “sistema financeiro”, do agenciamento mídia, do agenciamento “Estado Providência” e seus “equipamentos coletivos de subjetivação” (escola, hospital, museu, teatro, televisão, internet, etc.)**. O indivíduo “funciona” e é submetido ao agenciamento do mesmo modo que as peças de máquinas técnicas, que os procedimentos organizacionais, que os sistemas de signos, etc.
> (p. 168)

> 2: A servidão é um conceito utilizado na cibernética (palavra grega que significa “pilotagem” ou “governo”) e na automação.
> (p. 168)

# Mediação na servidão maquínica

> A sujeição fabrica um sujeito em relação com um objeto exterior (máquina, dispositivo de comunicação, moeda, serviço público, etc.) que ele _usa_ e com o qual ele age. **Na sujeição o sujeito individuado trabalha ou se comunica com outro sujeito individuado através de uma máquina – objeto que funciona como um “meio” ou uma mediação de sua ação ou de seu uso.** A lógica “sujeito–objeto”, que constitui o modo de funcionamento da sujeito social, é uma lógica “humana, demasiado humana”.
> (p. 168)

# Servidão maquínica não diferencia humano e não-humano (híbridos)

> **A servidão maquínica não se embaraça com as distinções entre os objetos e os sujeitos, entre as palavras e as coisas, entre a “natureza” e a “cultura”. Na servidão maquínica, o sujeito individuado não se opõe às máquinas, é adjacente a elas.**  Juntos constituem um dispositivo “homens–máquinas” onde os homens e as máquinas são tão–somente peças recorrentes e reversíveis do processo de produção, de comunicação, de consumo, etc. **Na servidão maquínica, o abismo ontológico entre o humano e o não humano, entre os sujeitos e os objetos, entre as palavras e as coisas é constantemente transposto por técnicas, procedimentos, protocolos que mobilizam semióticas assignificantes (diagramas, planos, equações, gráficos, esquemas, etc.).** Para a ergonomia, nos sistemas homens–máquinas, onde “interagem vários elementos humanos e não humanos (...) os componentes de todo trabalho podem ser expressos em termos de informação”. **Mas aqui a “noção de informação perde seu aspecto antropocêntrico”**. Em ergonomia não se fala mais em “sinal–organismo–resposta”, não se utiliza mais o modelo das teorias da comunicação onde a troca se realiza entre sujeitos–individuados distintos, que permite “analogias cômodas ainda que limitadas: emissor–receptor”. **Fala–se e utiliza–se os conceitos de “entradas” e de “saídas” que já não têm nada de antropomórfico.  Os “sistemas homens–máquinas” (no plural) não podem ser considerados como um simples acúmulo de postos de trabalho “homem–máquina” (no singular), pois diferem em natureza da “díade” sujeito (homem)/objeto (máquina).**
> (p. 169)

> Para dizê–lo não mais nos termos da ergonomia, mas com os conceitos filosóficos de Félix Guattari, **a servidão não comporta, propriamente falando, nem sujeitos e nem objetos, mas entidades “ontologicamente ambíguas”, híbridos, “objetidades/ subjetidades”, ou seja, entidades “bi–faces objeto–sujeito”.** Os “objetos”, as máquinas, os protocolos, os diagramas, os gráficos, os esquemas podem constituir vetores de “proto–subjetivação” ou focos de “proto–enunciação”. O “para si” e o “para outrem”, normalmente considerados atributos exclusivos da subjetividade humana, aderem também às “coisas”. **Os “sujeitos”, em contrapartida, cristalizam hábitos, rotinas físicas e intelectuais que podem possuir a consistência dos “objetos”.**
> (p. 169)

> Na servidão, o indivíduo não somente “faz peça com o agenciamento”, como é também _feito em pedaços_ por ele: os componentes de sua subjetividade (inteligência, afetos, sensações, cognição, memória, força física) não são mais unificados no “eu”, não têm mais o sujeito individuado como referente. **Inteligência, afetos, sensações, cognição, memória, força física constituem doravante componentes que não encontram mais sua síntese na pessoa, mas no agenciamento ou no processo (empresa, mídia, serviço público, escola, etc.).**
> (p. 169)

# Servidão ~ modelização e modulação

> A servidão não age nem por repressão, nem por ideologia. **Ela procede por técnicas de modelização e modulação, que se conectam às “energias mesmas da vida e da atividade humana”.** Ela se apodera dos seres humanos “por dentro” e “por fora” ao equipá–los com certos modos de percepção e de sensibilidade, bem como de representações inconscientes. A formatação exercida pela servidão maquínica intervém no “funcionamento de base dos comportamentos perceptivos, sensitivos, afetivos, cognitivos, lingüísticos” 
> (p. 170)o

# Dominação de multidões de quanta de poder e desejo para além da exploração econômica

> “Por meio de um enriquecimento contínuo de seus componentes semióticos, o capital toma o controle, para além do trabalho assalariado e de seus bens monetarizados, de multidões de quanta de poder e de desejo que outrora permaneciam enquistadas na economia local, doméstica, libidinal” {Guattari}.
> (p. 170)

# Duplo cinismo capitalista ("humanização e desumanização")

> O capitalismo exerce um duplo cinismo: **cinismo “humanista” de nos atribuir uma individualidade e papéis pré–estabelecidos (trabalhador, consumidor, desempregado, homem/mulher, artista, etc.)** nos quais os indivíduos devem se alienar; **e cinismo “desumanizante” de nos incluir em um agenciamento que não distingue mais humano e não humano, sujeito e objeto, as palavras e as coisas.**  Na servidão não agimos mais, nem mesmo _fazemos uso_ de qualquer coisa, se por ação e uso entendemos funções do sujeito. Antes constituímos simples entradas e saídas, _inputs_ ou _outputs_ do funcionamento de processos econômicos, sociais, comunicacionais, governados ou pilotados pela servidão.
> (p. 170)

# Funcionameno econômico do capitalismo

> Na retomada e na renovação do conceito marxista de “produção” por Deleuze e Guattari, sujeição e servidão determinam, em conjunto e pela sua diferença, o funcionamento “econômico” do capitalismo. **O capital, ao comprar a força de trabalho, remunera uma sujeição: um tempo de presença em um emprego, em uma função, um estar à disposição (como o caso do desempregado, ou do “tempo de cérebro disponível” do telespectador). Mas, na realidade, o que o capital compra não é somente o tempo de presença da força de trabalho em uma empresa, em uma instituição, em uma função social, ou ainda o tempo disponível do desempregado ou do telespectador. O que ele compra em primeiro lugar é o direito de poder explorar um agenciamento “complexo”, que põe “em jogo os meios de transporte, os modelos urbanos, as mídias, as indústrias de lazer, as maneiras de perceber e de sentir, todas as semióticas”**. A servidão libera assim potências de produção incomensuráveis com as do emprego e do trabalho humano.
> (p. 171)

# Antropomorfismo e antropocentrismo em Marx. Tempo e mais-valor maquínico em D&G.

> Na lei de valor de _O Capital_, **Marx tem ainda uma visão “antropomórfica” e “antropocêntrica” da produção, pois a mais–valia é humana, assim como o tempo de trabalho.** Somente o trabalho do operário é produtor de mais–valia, enquanto as máquinas não fazem nada além de transmitir seu valor que, por sua vez, resulta do tempo de trabalho humano necessário à sua fabricação. **Deleuze e Guattari, sensíveis ao crescimento extraordinário do capital constante (as maquinarias), introduzem o conceito de mais–valia maquínica e de tempo maquínico. Esses tempos são os tempos de servidão onde não distinguimos mais sujeito e objeto, humano e não humano, natural e artificial. Essas temporalidades maquínicas constituem os fatores essenciais da produção capitalista.** À diferença do tempo e da mais–valia humanos, os tempos e a mais–valia maquínicos têm a propriedade de não serem nem quantificáveis, nem determináveis.
> (p. 171)

> 9: “O controle real dos tempos maquínicos, da servidão dos órgãos humanos aos agenciamentos produtivos, não pode ser validamente medido a partir de um equivalente geral. Pode–se medir um tempo de presença, um tempo de alienação, uma duração de encarceramento dentro de uma usina ou de uma prisão: não se pode medir suas consequências sobre um indivíduo. Pode–se quantificar o trabalho visível de um físico em um laboratório, não o valor produtivo das fórmulas que ele elabora”.  Félix Guattari, _La Révolution Moléculaire_, Ed. Recherche, coll. Encres, Paris, 1977, p. 74
> (p. 171)

> Dentro da empresa, o assalariado deve agir e se reconhecer como um produtor sujeito às máquinas que lhe são exteriores e das quais se utiliza. Mas nunca é o assalariado (subjetividade individuada), nem uma simples cooperação de assalariados (inter–subjetividade) que produz. A produtividade do capital depende, de um lado, do agenciamento de órgãos (cérebro, mãos, músculos, etc.) e de faculdades humanas (memória, percepção, cognição, etc.); e, de outro, das performances “intelectuais” e físicas das máquinas, de protocolos de organização, de softwares, ou ainda de sistemas de signos, da potência da ciência, etc.
> (p. 172)

# O capital explora a diferença entre sujeição e servidão

> O capital não extorque, portanto, um simples acréscimo de tempo de trabalho (diferença entre o tempo humano remunerado e o tempo humano transcorrido em um local de trabalho), mas **instala um processo que explora a diferença entre sujeição e servidão: pois se a sujeição subjetiva, a alienação social inerente a um local de trabalho ou a não importa qual função social (trabalhador, desempregado, professor, mãe, etc.), é sempre assinalável e calculável (salário que corresponde a um emprego, rendimento que corresponde a uma função social), a parte de servidão maquínica que entra na atividade humana não é jamais assinalável, nem quantificável enquanto tal**.
> (p. 172)

# A produção como composição da sujeição social e da servidão maquínica

> Na servidão maquínica, não há nenhuma proporcionalidade entre o trabalho individual e a produção. **A produção não é a soma dos tempos de emprego individual, nem mesmo a soma de tempos de trabalho individuais,** se considerarmos que se deve distinguir o trabalho do emprego, já que o primeiro transborda o segundo. **A produção e a produtividade só dependem parcialmente do emprego e mesmo do trabalho; elas provêm, em primeiro lugar, do agenciamento, isto é, da mobilização das potências do mecanismo, da comunicação, da ciência, do social, como Marx já havia antecipado nos Grundrisse_.** Assim, da mesma maneira que nos foi preciso distinguir o emprego do trabalho, é preciso agora separar o trabalho humano e o emprego da produção.
> (p. 172)

# Antecipação do mais-valor maquínico nos Grundrisse e no capítulo inédito de Marx

> 10: Enquanto que a teoria do valor do primeiro livro de _O Capital_ de Marx é ainda uma teoria aditiva (soma aritmética do trabalho) do valor, e que a mais–valia é ainda concebida como uma “mais–valia humana”, **nos _Grundrisse_ e no _Capítulo inédito do Capital_, Marx descreve a servidão maquínica sem elaborar, contudo, uma teoria do valor “maquínico” que correspondesse a ela.** Guattari faz notar que a concepção marxista da mais–valia humana corresponde à prática contábil do capitalismo, mas não certamente ao seu funcionamento real. Esta contabilidade é hoje em dia retomada para legitimar a contra–reforma de aposentadorias, pois se calcula seu financiamento a partir do emprego individual e de seu salário. Só a sujeição é levada em conta, ao passo que a servidão não conta para nada. “Trapaça cósmica”, dizia Gilles Deleuze.
> (p. 172)

# Sobre o Estado-providência, seguro desemprego não considerarem a servidão maquínica

> O “equipamento” social do Estado–providência que gerencia o seguro–desemprego obriga o desempregado a agir e se reconhecer como “usuário” do seguro, isto é, como capital humano responsável por sua empregabilidade. Mas, ao mesmo tempo, lhe é imposto funcionar como uma simples variável de ajustamento do mercado de trabalho, como uma peça flexível e adaptável aos “automatismos” da demanda e da oferta de emprego. De um lado, os dispositivos “pastorais” de controle e de incitação, ocupando–se em detalhe da formação, dos projetos, das competências e dos comportamentos do desempregado, forçam–no a se instituir como sujeito, ao passo que, de outro lado, o mercado o considera como uma peça desindividualizada que participa do automatismo de sua auto–regulação. Ora, se o seguro–desemprego é a medida do que custa colocar o desempregado à disposição (a medida da sujeição), o que o desempregado produz com sua mobilidade e sua flexibilidade no mercado de emprego, o que produz enquanto consumidor ou enquanto contribui para fazer girar a máquina da relação de serviço do seguro– desemprego (as informações que fornece, apesar de si mesmo, o índice subjetivo e objetivo que representa, apesar de si mesmo, fazem do desempregado um dos feed–backs dessa “máquina social”), não é determinável e nem calculável.
> (p. 173)

# Sistema financeiro

> No sistema financeiro, o indivíduo é sujeito (capital humano) de outra maneira ainda. Enquanto “investidor/devedor”, pode ser considerado como o modelo mesmo da subjetivação: a promessa de saldar a dívida que contraiu implica a fabricação de uma memória e os afetos (tais como a culpabilidade, a responsabilidade, a lealdade, a confiança, etc.) necessários para garantir a realização de sua promessa.  **Mas quando o crédito entra na máquina finança, ele se torna completamente outra coisa, um simples _input_ do agenciamento financeiro. O crédito/dívida incorporado no agenciamento perde, com efeito, toda referência ao sujeito que contratou a dívida.** O crédito/dívida é literalmente feito em pedaços (da mesma maneira que o agenciamento faz o sujeito em pedaços) pela máquina financeira, como a crise dos _subprimes_ mostrou. Não se trata mais deste ou daquele investimento, desta ou daquela dívida: **o agenciamento financeiro a transformou em moeda que age como “capital”, em dinheiro que gera dinheiro, que não se limita a comandar o trabalho, mas comanda antes de tudo um agenciamento, um processo social complexo.**
> (p. 173-4)

# Servidão maquínica e trabalho

> Na servidão, em compensação, o trabalho parece explodir em duas direções: **aquela de um sobre–trabalho “intensivo” que nem mesmo passa mais pelo trabalho, mas por “uma servidão maquínica” generalizada, “a ponto de fornecer uma mais–valia independentemente de qualquer trabalho (a criança, o aposentado, o desempregado, o telespectador, etc.), e aquela de um trabalho extensivo que se tornou precário e flutuante”**. Nessa situação, os usuários (do seguro–desemprego, da televisão, dos serviços públicos e privados, etc.), como todo consumidor, tendem a se tornar os “empregados”. No “trabalho do consumidor” temos a exemplificação de uma produtividade que não passa mais pela “definição físico–social do trabalho”.
> (p. 174)

> 14: “A participação dos consumidores na produção é extremamente heterogênea...Pudemos mostrar que cada uma dessas atividades pode ser qualificada de trabalho no sentido econômico, sociológico e ergonômico do termo. Elas produzem o valor para a empresa... A exemplo de um assalariado, a atividade do consumidor é fortemente prescrita e enquadrada. Ela é freqüentemente realizada sob constrangimento de tempo, de produtividade, de resultado, por meio dos instrumentos específicos”.  Marie–Anne Dujarier, _Le travail du consommateur_, Editions la Découverte, Paris, 2008, p. 230–231.
> (p. 174-5)

# O capital como operador semiótico, as semióticas (significante da sujeição e a-significante da servidão) como condição da produção

> O capital é um “operador semiótico”. O que significa essa afirmação de Guattari? Numa primeira aproximação, equivale a dizer que as semióticas são condições da produção
> (p. 175)

> Do ponto de vista semiótico, a servidão maquínica e a sujeição social implicam regimes de signos distintos. **A sujeição social mobiliza semióticas significantes, em especial a linguagem, que se dirige à consciência e às representações com vistas a constituir um sujeito individuado** (o “capital humano”), enquanto **a servidão maquínica funciona a partir de semióticas assignificantes (os índices da bolsa, a moeda, as equações, os diagramas, a linguagem de computador, etc.)** que não passam pela consciência e pelas representações e não têm como referente o sujeito.
> (p. 175)

> 15: “Nossa oposição entre as semiologias significantes despóticas e as semiologias assignificantes continua muito esquemática. **Na realidade, há somente semióticas mistas que participam de ambas em graus diversos. Uma semiologia significante é sempre freqüentada por uma máquina de signos e, inversamente, uma máquina de signos está sempre em vias de ser recuperada por uma semiologia significante.** Mas certamente é útil discernir as relações de polaridade definidas por esses componentes”. (Félix Guattari, _La révolution moléculaire_, Paris, Ed. Recherches, 1977, p. 346
> (p. 175)

> Os signos e as semióticas podem funcionar segundo essas duas lógicas, ao mesmo tempo heterogêneas e complementares: **produzir operações, iniciar ações, funcionar, constituir os componentes de inputs e de outputs de uma máquina social ou tecnológica, como no caso da servidão maquínica; produzir sentido, significações, interpretações, discursos, representações, através da linguagem, como no caso da sujeição social.** As semióticas assignificantes **agem junto às coisas.** Colocam um órgão, um sistema de percepção, uma atividade, etc., diretamente em conexão com uma máquina, com processos, com signos, sem passar pela representação de um sujeito. Elas desempenham um papel inteiramente específico no capitalismo, uma vez que, “no essencial, o capitalismo se apóia em máquinas assignificantes”.
> (p. 175)

# Crítica ao privilégio da linguagem nas ideias de capitalismo cognitivo e cultural

> 16: As teorias que fazem do “primado da linguagem” a chave do funcionamento semiótico de nossa sociedade, correm o risco de passar ao lado do funcionamento real do capitalismo. **O capital opera a partir de uma multiplicidade de semióticas e não somente das semióticas significantes e lingüísticas como pensam as teorias do capitalismo “cognitivo” ou cultural.**
> (p. 175)

# Semiótica a-significante

> Os índices da bolsa, as estatísticas de seguro–desemprego, as funções e diagramas da ciência, as linguagens da informática não constroem discursos e nem fazem narrativas (os discursos e as narrativas são produzidas ao lado); funcionam executando um programa e aumentando a potência do agenciamento “produtivo”. **As semióticas assignificantes permanecem mais ou menos tributárias das semiologias significantes, mas, ao nível do seu funcionamento intrínseco, elas escapam à linguagem e às significações sociais dominantes.** Se o banco central europeu aumentar sua taxa de desconto em 1%, dezenas de milhares de “projetos” desaparecerão como fumaça por falta de crédito. Se os preços do imobiliário desmoronarem, como no caso das subprimes nos Estados Unidos, milhares de famílias não poderão mais pagar sua dívida. Se as contas da seguridade social apresentarem um déficit, serão determinadas medidas de redução das “despesas sociais”.
> (p. 175)

> Os fluxos de signos assignificantes agem diretamente sobre os fluxos materiais, para além da separação entre produção e representação própria das semióticas significantes, e funcionam independentemente de significarem alguma coisa para quem quer que seja.
> (p. 175)

> Os signos, ao invés de reenviar a outros signos, atuam diretamente sobre o real, no sentido de que os signos de uma linguagem informatizada põem em funcionamento uma máquina técnica como o computador, os signos monetários ativam a máquina econômica, e os signos de uma equação matemática entram na construção de uma ponte ou de um imóvel.
> (p. 176)

# Automatismo sobre o qual o capitalismo se sustenta visa despolitizar as relações de poder

> **O capitalismo é um sistema que procura sempre se apoiar em sistemas automáticos de avaliação, de medida e de regulação. Por essa razão, é importante para ele controlar os dispositivos semióticos assignificantes (a escrita econômica, contábil, bolsista, etc.) com os quais visa despolitizar as relações de poder.** A potência das semióticas assignificantes reside, de um lado, no fato de serem **modalidades de avaliação e de medida “automáticas”**, e, de outro, de **colocarem em comunicação e em equivalência “formal” domínios heterogêneos de potência e de poder assimétricos**, organizando sua integração e sua recentragem sobre a acumulação econômica.
> (p. 176)

# Potência na dessubjetivação

> A dessubjetivação operada pela servidão maquínica poderia ser aproveitada para escaparmos às sujeições coletivas assassinas da modernidade capitalista e das sujeições individuais que nos encerram sempre de novo no eu, na pessoa, na família, etc. Aproveitar a dessubjetivação para produzir outra coisa que não um individualismo paranoico, e sair da falsa alternativa de ser condenado a funcionar como uma peça entre outras de uma maquinaria social ou de se tornar um sujeito individual, um capital humano.
> (p. 176-7)

> **É contra essa possibilidade que a sujeição trabalha, assegurando a reterritorialização e a recomposição dos componentes subjetivos “liberados” pela servidão maquínica sobre o “sujeito” individuado, carregando–o assim de culpabilidade, de medo e de responsabilidade.** A sujeição social e suas semióticas significantes devem produzir os valores sociais, as significações, as identificações individuais e coletivas necessárias aos papéis e às funções da produção social.
> (p. 177)

# Crítica a abordagens que só levam em conta a sujeição (Rancière e Badiou)

> As teorias que se limitam a levar em conta a “sujeição social” e negligenciam completamente a servidão maquínica (Jacques Rancière e Alain Badiou, por exemplo), submetem o capitalismo a tais mutilações que é duvidoso que possam dar conta dos processos de subjetivação que nele operam. **Se elas permitem apreender as divisões entre aqueles que monopolizam o poder e o saber e aqueles que os sofrem ao nível macro–político, se permitem cruzar essas divisões com aquelas das raças, dos sexos, das faixas etárias, etc., elas desconhecem a natureza e as funções das servidões maquínicas.** Ora, se considerarmos o capitalismo unicamente do ponto de vista da sujeição, perdemos a especificidade das modalidades de servidão maquínica. **Se não tivermos em conta as servidões corremos o risco de confundir, à maneira de Rancière e de Badiou, a democracia grega com o capitalismo, o trabalho dos artesãos e dos escravos com o dos “operários”, e Marx com Platão.**
> (p. 177)

# Relação indireta (assalariamento) da sujeição social com a produção real

> Do ponto de vista econômico, a sujeição distribui salários e rendimentos que só têm uma relação indireta com a “produção real”. A sujeição divide a população entre aqueles que possuem um emprego e aqueles que não possuem, entre aqueles que possuem direitos sociais e aqueles que não possuem, entre “ativos” e “inativos”, e isto sobre uma base que não responde a nenhuma necessidade econômica, uma vez que a contribuição de cada um à “produção” não é assinalável e nem mensurável.
> (p. 177)

# Segmentariedade dura e binária da sujeição x multipla e flexível na servidão

> A sujeição funciona a partir de segmentariedades binárias (emprego/desemprego, produtor/consumidor, homens/mulheres, artista/não–artista, produtivo/ não–produtivo, etc.), enquanto a servidão, passando através das sujeições e seus dualismos, funciona a partir de uma segmentaridade flexível. **Na servidão maquínica, as divisões emprego/desemprego, seguro/assistência, produtivo/não–produtivo não ocorrem mais, pois do ponto de vista da “produção real”, do ponto de vista do agenciamento ou do processo, todo mundo “trabalha”, todo mundo é “produtivo” segundo modalidades diversas.**
> (p. 177)

> 19: “De uma certa maneira, a dona–de–casa ocupa um local de trabalho em seu domicílio, a criança na escola, o consumidor no supermercado, o espectador diante de sua tela”. Do ponto de vista da servidão maquínica, as crianças “trabalham diante da televisão; trabalham na creche com brinquedos concebidos para melhorar suas performances produtivas. **Em certo sentido, pode–se comparar esse trabalho àquele dos aprendizes na escola profissionalizante**”. (Félix Guattari, _La Révolution moléculaire_, Paris, Ed. Recherches, 1980, p. 80).
> (p. 177-8)

# Expansão do local de trabalho no capitalismo contemporâneo

> No capitalismo contemporâneo, o assalariado não recobre senão parcialmente a multiplicidade de “trabalhos” efetuados, e ainda mais parcialmente a multiplicidade dos agenciamentos “produtivos”. **A noção de local de trabalho deveria ser estendida à maior parte das atividades não assalariadas, e a de empresa aos equipamentos coletivos do Estado providência, às mídias, etc.** “Seria totalmente arbitrário considerar hoje em dia o assalariado de empresa independentemente dos múltiplos sistemas de salários diferenciados, de assistência e de custos sociais que afetam de perto ou de longe a reprodução da força de trabalho, que passam ao largo do circuito monetário da empresa e são controlados pelas múltiplas instituições e equipamentos de poder".
> (p. 178)

# Nos sindicatos, movimento do trabalho ao emprego como entrega aos patrões da noção de sociedade

> A partir do pós–guerra, os sindicatos e a esquerda operaram um deslizamento do “trabalho ao emprego”, fazendo passar às mãos dos patrões e do Estado a questão política fundamental de integração do “social” (Foucault), do “socius” (Deleuze–Guattari) ou da “sociedade” (Operaísmo italiano) à valorização capitalista.
> (p. 178)

> **A “produção”, contrariamente ao que pensam Rancière e Badiou, não depende da economia.** Ela tampouco se limita a explorar o conhecimento, o saber, a cultura, como afirmam as teorias do capitalismo cognitivo, **mas captura e explora algo mais profundo e transversal à sociedade em seu conjunto.** A servidão maquínica e a sujeição social **põem o desejo para trabalhar, se por desejo entendemos não uma simples pulsão, uma mera energia libidinal, apenas um “conatus”, mas a potência de agir dentro de um agenciamento, de um conjunto, de um coletivo.**
> (p. 178)

# Desejo como construtivismo (agenciar componentes humanos e não-humanos)

> **A definição abstrata do desejo é construtivismo (Deleuze). Desejar significa sempre agenciar e construir um conjunto; desejar significa sempre agenciar e construir uma multiplicidade ao mesmo tempo atual e virtual, um coletivo de elementos humanos e não–humanos.** O desejo flui num agenciamento do qual ele é o produto e, ao mesmo tempo, o produtor. **O agenciamento em Deleuze e Guattari é constituído por quatro componentes, pelo menos: estados de coisas, enunciações, movimentos de territorialização, de construção de territórios e movimentos de desterritorialização, de saída do território.** A modelização esquizoanalítica de Guattari prevê assim **quatro componentes: fluxos materiais e semióticos, territórios existenciais, Universos incorporais e _phylums_ maquínicos.** Nos dois casos o desejo passa por componentes do agenciamento que o constituem e que ele ajuda a constituir.
> (p. 178)

# Armadilha idealista: separar o sujeito desejante do objeto desejado

> **A armadilha idealista consiste em separar o sujeito desejante e o objeto desejado, em extrair do agenciamento o desejo, como desejo do sujeito, e o objeto, como objeto do desejo.** Na realidade, sujeito, objeto e desejo são tomados num maquinismo que os produz, eles não preexistem ao agenciamento. O desejo não é uma força biológica, um impulso, uma energia primária, uma paixão.
> (p. 178-9)

> 22: Reencontramos todos esses limites no livro de Frédéric Lordon, _Capitalisme, désir et servitude_: o desejo como energia e a separação entre um “estruturalismo das relações” e uma “antropologia das paixões”. **É precisamente tal divisão do desejo e da estrutura que o conceito de “máquina” de O Anti–Édipo atacava. “...a produção social e as relações de produção são uma instituição do desejo, e pela qual os afetos ou as pulsões fazem parte da própria infraestrutura. Pois eles fazem parte dela, estão presentes nela de todas as maneiras, criando nas formas econômicas tanto a sua própria repressão quanto os meios de romper essa repressão”.** (G. Deleuze e F. Guattari, _L´Anti–Oedipe_, p. 75 [O Anti–Édipo, trad. Luiz B. L. Orlandi. São Paulo, Ed. 34, 2010, p. 90] ). **Em Lordon, o “paralelismo estéril” não é entre Marx e Freud, mas entre Marx e Espinoza.**
> (p. 179)

# Produção e desejo

> A problemática da produção é inseparável da problemática do desejo (Guattari). **O desejo é sempre o modo de produção de alguma coisa, seu modo de construção. O desejo é construído e, ao mesmo tempo, construtor.** Não se deseja uma pessoa ou uma coisa, mas os mundos e os possíveis que eles envolvem. **Desejar significa construir um agenciamento que desdobre os possíveis e os mundos que uma coisa ou uma pessoa envolvem.**
> (p. 179)

> Ao articularem o social ao desejo, Deleuze e Guattari estão em condições de definir a natureza do capitalismo contemporâneo que parece ter realizado por si mesmo esta afirmação de _O Anti–Édipo_: **“Toda produção é ao mesmo tempo desejante e social”.**
> (p. 179)


# Nova política > dessubjetivação e nova subjetivação > reconfiguração do agenciamento maquínico, de um mundo e seus possíveis

> A ação política se coloca, então, de maneira nova, pois deve partir do modo em que as servidões e os assujeitamentos trabalham o desejo, isto é, implicam a subjetividade. **A ação política deve operar uma dessubjetivação e produzir, ao mesmo tempo, uma nova subjetivação, recusar a injunção de ocupar os lugares e os papéis dentro da divisão social do trabalho, e construir, problematizar e reconfigurar o agenciamento maquínico, isto é, um mundo e seus possíveis.**
> (p. 179)
