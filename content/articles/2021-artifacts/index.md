---
title: "Artefatos têm política?"
date: 2021-08-14
path: "/artefatos-tem-politica"
category: fichamento
tags: [Langdon Winner, neutralidade, fichamento, tecnologia, determinismo tecnológico]
featuredImage: "power_plant.jpg"
srcInfo: Foto de <a href="https://www.flickr.com/photos/158088088@N03/50147200831/">Olivier Cremers</a>
published: true

---

Fichamento do texto _Do artifacts have politics_[^1] do cientista político estadunidense Langdon Winner. O artigo explora duas formas de não-neutralidade tecnológica: a construção, design e uso de tecnologias levando em conta posições políticas; e tecnologias inerentemente políticas, ou seja, tecnologias cuja utilização exige (ou favorece) um arranjo específico de poder.

> It is no surprise to learn that technical systems of various kinds are deeply
> interwoven in the conditions of modern politics. The physical arrangements of
> industrial production, warfare, communications, and the like have fundamen
> tally changed the exercise of power and the experience of citizenship. But to go
> beyond this obvious fact and to argue that certain technologies _in themselves_ have
> political properties seems, at first glance, completely mistaken. We all know
> that people have politics, not things. To discover either virtues or evils in aggre
> gates of steel, plastic, transistors, integrated circuits, and chemicals seems
> just plain wrong, a way of mystifying human artifice and of avoiding the true
> sources, the human sources of freedom and oppression, justice and injustice.
> Blaming the hardware appears even more foolish than blaming the victims when it comes to judging conditions of public life.
> (p. 122)

# Determinação social da tecnologia e determinismo tecnológico

> Hence, the stern advice commonly given those who flirt with the notion that
> technical artifacts have political qualities: What matters is not technology itself,
> but the social or economic system in which it is embedded. This maxim, which
> in a number of variations is the central premise of a theory that can be called
> the social determination of technology, has an obvious wisdom. It serves as a
> needed corrective to those who focus uncritically on such things as "the comput
> er and its social impacts" but who fail to look behind technical things to notice
> the social circumstances of their development, deployment, and use. This view
> provides an antidote to naive technological determinism -- the idea that 
> technology develops as the sole result of an internal dynamic, and then, unmediated
> by any other influence, molds society to fit its patterns. Those who have not
> recognized the ways in which technologies are shaped by social and economic
> forces have not gotten very far.
> (p. 122)

# Teoria da política tecnológica (_technological politics_)

> The theory of technological
> politics draws attention to the momentum of large-scale sociotechnical systems,
> to the response of modern societies to certain technological imperatives, and to
> the all too common signs of the adaptation of human ends to technical means. In
> so doing it offers a novel framework of interpretation and explanation for some
> of the more puzzling patterns that have taken shape in and around the growth of
> modern material culture. One strength of this point of view is that it takes
> technical artifacts seriously. Rather than insist that we immediately reduce
> everything to the interplay of social forces, it suggests that we pay attention to
> the characteristics of technical objects and the meaning of those characteristics.
> A necessary complement to, rather than a replacement for, theories of the social
> determination of technology, this perspective identifies certain technologies as
> political phenomena in their own right. It points us back, to borrow Edmund
> Husserl's philosophical injunction, _to the things themselves_.
> (p. 123)

# Política como arranjo de poder em associações humanas

> By "politics," I mean arrangements of power and authority in human associations as well as the activities that
> take place within those arrangements. 
> (p. 123)

# Tecnologia como todo artefato prático moderno

> For my purposes, "technology" here is
> understood to mean all of modern practical artifice,7 but to avoid confusion I
> prefer to speak of technology, smaller or larger pieces or systems of hardware
> of a specific kind.
> (p. 123)

# As pontes de Moses em Long Island

Controvérsia sobre a veracidade dos fatos apontados [^2].

> Robert Moses, the master builder of roads, parks, bridges, and other public
> works from the 1920s to the 1970s in New York, had these overpasses built to
> specifications that would discourage the presence of buses on his parkways.
> According to evidence provided by Robert A. Caro in his biography of Moses,
> the reasons reflect Moses's social-class bias and racial prejudice. Automobile [124]
> wning whites of "upper" and "comfortable middle" classes, as he called them,
> would be free to use the parkways for recreation and commuting. Poor people
> and blacks, who normally used public transit, were kept off the roads because
> the twelve-foot tall buses could not get through the overpasses. One con
> sequence was to limit access of racial minorities and low-income groups to Jones
> Beach, Moses's widely acclaimed public park. Moses made doubly sure of this
> result by vetoing a proposed extension of the Long Island Railroad to Jones
> Beach.
> (p. 123-124)

> For generations after
> Moses has gone and the alliances he forged have fallen apart, his public works,
> especially the highways and bridges he built to favor the use of the automobile
> over the development of mass transit, will continue to shape that city. Many of
> his monumental structures of concrete and steel embody a systematic social
> inequality, a way of engineering relationships among people that, after a time,
> becomes just another part of the landscape.
> (p. 124)

# Eficiência não é a máxima da construção tecnológica, dominação social também é levada em conta

> If
> we suppose that new technologies are introduced to achieve increased efficien
> cy, the history of technology shows that we will sometimes be disappointed.
> Technological change expresses a panoply of human motives, not the least of
> which is the desire of some to have dominion over others, even though it may
> require an occasional sacrifice of cost-cutting and some violence to the norm of
> getting more from less. 
> (p. 124)

> One poignant illustration can be found in the history of nineteenth century
> industrial mechanization. At Cyrus McCormick's reaper manufacturing plant in
> Chicago in the middle 1880s, pneumatic molding machines, a new and largely
> untested innovation, were added to the foundry at an estimated cost of
> $500,000. In the standard economic interpretation of such things, we would
> expect that this step was taken to modernize the plant and achieve the kind of
> efficiencies that mechanization brings. But historian Robert Ozanne has shown
> why the development must be seen in a broader context. At the time, Cyrus
> McCormick II was engaged in a battle with the National Union of Iron Mold
> ers. He saw the addition of the new machines as a way to "weed out the bad 
> [125] element among the men," namely, the skilled workers who had organized the
> union local in Chicago.10 The new machines, manned by unskilled labor, ac
> tually produced inferior castings at a higher cost than the earlier process. After
> three years of use the machines were, in fact, abandoned, but by that time they
> had served their purpose?the destruction of the union. Thus, the story of these
> technical developments at the McCormick factory cannot be understood ade
> quately outside the record of workers' attempts to organize, police repression of
> the labor movement in Chicago during that period, and the events surrounding
> the bombing at Hay market Square. Technological history and American politi
> cal history were at that moment deeply intertwined. 
> (p. 124-125)

# Política e tecnologia estão embricadas

> Robert Moses's bridges, after all, were used to carry automobiles from one point
> to another; McCormick's machines were used to make metal castings; both tech
> nologies, however, encompassed purposes far beyond their immediate use. If
> our moral and political language for evaluating technology includes only cate
> gories having to do with tools and uses, if it does not include attention to the
> meaning of the designs and arrangements of our artifacts, then we will be
> blinded to much that is intellectually and practically crucial.
> (p. 125)

# Intencionalidade

> These are instances in which the very
> process of technical development is so thoroughly biased in a particular direc
> tion that it regularly produces results counted as wonderful breakthroughs by some social interests and crushing setbacks by others. In such cases it is neither
> correct nor insightful to say, "Someone intended to do somebody else harm."
> Rather, one must say that the technological deck has been stacked long in ad
> [126] vance to favor certain social interests, and that some people were bound to
> receive a better hand than others. 
> (p. 125-126)

# A colheitadeira de tomates mecânica

> The mechanical tomato harvester, a remarkable device perfected by re
> searchers at the University of California from the late 1940s to the present,
> offers an illustrative tale. The machine is able to harvest tomatoes in a single
> pass through a row, cutting the plants from the ground, shaking the fruit loose,
> and in the newest models sorting the tomatoes electronically into large plastic
> gondolas that hold up to twenty-five tons of produce headed for canning. To
> accommodate the rough motion of these "factories in the field," agricultural
> researchers have bred new varieties of tomatoes that are hardier, sturdier, and
> less tasty. The harvesters replace the system of handpicking, in which crews of
> farmworkers would pass through the fields three or four times putting ripe to
> matoes in lug boxes and saving immature fruit for later harvest.11 Studies in
> California indicate that the machine reduces costs by approximately five to sev
> en dollars per ton as compared to hand-harvesting.12 But the benefits are by no
> means equally divided in the agricultural economy. In fact, the machine in the
> garden has in this instance been the occasion for a thorough reshaping of social
> relationships of tomato production in rural California. 
> (p. 126)

> By their very size and cost, more than $50,000 each to purchase, the ma
> chines are compatible only with a highly concentrated form of tomato growing.
> With the introduction of this new method of harvesting, the number of tomato
> growers declined from approximately four thousand in the early 1960s to about
> six hundred in 1973, yet with a substantial increase in tons of tomatoes pro
> duced. By the late 1970s an estimated thirty-two thousand jobs in the tomato
> industry had been eliminated as a direct consequence of mechanization.13 Thus,
> a jump in productivity to the benefit of very large growers has occurred at a
> sacrifice to other rural agricultural communities. 
> (p. 126)

# Padrões sociotécnicos que se reforçam

> What we see here instead is an ongoing social
> process in which scientific knowledge, technological invention, and corporate
> profit reinforce each other in deeply entrenched patterns that bear the unmistak
> able stamp of political and economic power. Over many decades agricultural
> research and development in American land-grant colleges and universities has
> tended to favor the interests of large agribusiness concerns.
> (p. 126)

# Tecnologias como formas de ordenar o mundo

Instituição de invenções/inovações tecnológicas como instituição de leis da política.

> The
> things we call "technologies" are ways of building order in our world. Many
> technical devices and systems important in everyday life contain possibilities for
> many different ways of ordering human activity. Consciously or not, deliber
> ately or inadvertently, societies choose structures for technologies that influence
> how people are going to work, communicate, travel, consume, and so forth over
> a very long time. In the processes by which structuring decisions are made,
> different people are differently situated and possess unequal degrees of power as
> well as unequal levels of awareness. By far the greatest latitude of choice exists
> the very first time a particular instrument, system, or technique is introduced.
> Because choices tend to become strongly fixed in material equipment, economic 
> [128] investment, and social habit, the original flexibility vanishes for all practical
> purposes once the initial commitments are made. In that sense technological
> innovations are similar to legislative acts or political foundings that establish a
> framework for public order that will endure over many generations. For that
> reason, the same careful attention one would give to the rules, roles, and rela
> tionships of politics must also be given to such things as the building of high
> ways, the creation of television networks, and the tailoring of seemingly
> insignificant features on new machines. The issues that divide or unite people in
> society are settled not only in the institutions and practices of politics proper,
> but also, and less obviously, in tangible arrangements of steel and concrete,
> wires and transistors, nuts and bolts. 
> (p. 127-128)

# Engels sobre autoridade

> A remarkably forceful statement of one version of this argument appears in
> Friedrich Engels's little essay "On Authority" written in 1872. Answering anar
> chists who believed that authority is an evil that ought to be abolished altogeth
> er, Engels launches into a panegyric for authoritarianism, maintaining, among
> other things, that strong authority is a necessary condition in modern industry.
> To advance his case in the strongest possible way, he asks his readers to imagine
> that the revolution has already occurred. "Supposing a social revolution de
> throned the capitalists, who now exercise their authority over the production
> and circulation of wealth. Supposing, to adopt entirely the point of view of the
> anti-authoritarians, that the land and the instruments of labour had become the
> collective property of the workers who use them. Will authority have dis
> appeared or will it have only changed its form?"
> (p. 128)

> His answer draws upon lessons from three sociotechnical systems of his day,
> cotton-spinning mills, railways, and ships at sea. He observes that, on its way to
> becoming finished thread, cotton moves through a number of different opera
> tions at different locations in the factory. The workers perform a wide variety of
> tasks, from running the steam engine to carrying the products from one room to
> another. Because these tasks must be coordinated, and because the timing of the
> work is "fixed by the authority of the steam," laborers must learn to accept 
> [129] rigid discipline. They must, according to Engels, work at regular hours and
> agree to subordinate their individual wills to the persons in charge of factory
> operations. If they fail to do so, they risk the horrifying possibility that produc
> tion will come to a grinding halt. Engels pulls no punches. "The automatic
> machinery of a big factory," he writes, "is much more despotic than the small
> capitalists who employ workers ever have been."
> (p. 128-129)

> Engels finds that, far from being an idiosyncracy of capitalist social organ
> ization, relationships of authority and subordination arise "independently of all
> social organization, [and] are imposed upon us together with the material condi
> tions under which we produce and make products circulate." 
> (p. 129)

> The roots of unavoidable authoritarianism are, he argues, deeply implanted in the human involvement with
> science and technology. "If man, by dint of his knowledge and inventive genius,
> has subdued the forces of nature, the latter avenge themselves upon him by
> subjecting him, insofar as he employs them, to a veritable despotism independ
> ent of all social organization."
> (p. 129)

# O navio de Platão

> Because large
> sailing vessels by their very nature need to be steered with a firm hand, sailors
> must yield to their captain's commands; no reasonable person believes that ships
> can be run democratically. Plato goes on to suggest that governing a state is
> rather like being captain of a ship or like practicing medicine as a physician.
> Much the same conditions that require central rule and decisive action in orga
> nized technical activity also create this need in government. 
> (p. 129)

# Marx sobe determinismo tecnológico

> In this respect, his
> stand in "On Authority" appears to be at variance with Karl Marx's position in
> Volume One of Capital. Marx tries to show that increasing mechanization will
> render obsolete the hierarchical division of labor and the relationships of subor
> dination that, in his view, were necessary during the early stages of modern
> manufacturing. The "Modern Industry," he writes, "... sweeps away by
> technical means the manufacturing division of labor, under which each man is
> bound hand and foot for life to a single detail operation. At the same time, the
> capitalistic form of that industry reproduces this same division of labour in a
> still more monstrous shape; in the factory proper, by converting the workman
> into a living appendage of the machine. . . ." In Marx's view, the conditions
> [130] that will eventually dissolve the capitalist division of labor and facilitate proletarian revolution are conditions latent in industrial technology itself. The differences between Marx's position in Capital and Engels's in his essay raise an
> important question for socialism: What, after all, does modern technology make
> possible or necessary in political life? The theoretical tension we see here mirrors many troubles in the practice of freedom and authority that have muddied
> the tracks of socialist revolution. 
> (p. 129-130)

# Uma visão sobre tecnologia inerentemente política: necessidade prática de um contexto social específico

> One version claims that the adoption of a given technical sys
> tem actually requires the creation and maintenance of a particular set of social
> conditions as the operating environment of that system. Engels's position is of
> this kind. A similar view is offered by a contemporary writer who holds that "if
> you accept nuclear power plants, you also accept a techno-scientific-industrial
> military elite. Without these people in charge, you could not have nuclear
> power."29 In this conception, some kinds of technology require their social en
> vironments to be structured in a particular way in much the same sense that
> an automobile requires wheels in order to run. The thing could not exist as an
> effective operating entity unless certain social as well as material conditions
> were met. The meaning of "required" here is that of practical (rather than logi
> cal) necessity. Thus, Plato thought it a practical necessity that a ship at sea have
> one captain and an unquestioningly obedient crew. 
> (p. 130)

# Outra visão sobre tecnologia inerentemente política: compatibilidade com um contexto social específico

> A second, somewhat weaker, version of the argument holds that a given
> kind of technology is strongly compatible with, but does not strictly require,
> social and political relationships of a particular stripe. Many advocates of solar
> energy now hold that technologies of that variety are more compatible with a
> democratic, egalitarian society than energy systems based on coal, oil, and nu
> clear power; at the same time they do not maintain that anything about solar
> energy requires democracy. Their case is, briefly, that solar energy is decentral
> izing in both a technical and political sense: technically speaking, it is vastly
> more reasonable to build solar systems in a disaggregated, widely distributed
> manner than in large-scale centralized plants; politically speaking, solar energy
> accommodates the attempts of individuals and local communities to manage
> their affairs effectively because they are dealing with systems that are more
> accessible, comprehensible, and controllable than huge centralized sources. In
> this view, solar energy is desirable not only for its economic and environmental
> benefits, but also for the salutary institutions it is likely to permit in other areas
> of public life.
> (p. 130)

# Condições internas e externas de compatibilidade

> Within both versions of the argument there is a further distinction to be
> made between conditions that are _internal_ to the workings of a given technical
> system and those that are _external_ to it. Engels's thesis concerns internal social
> relations said to be required within cotton factories and railways, for example;
> what such relationships mean for the condition of society at large is for him a
> separate question. In contrast, the solar advocate's belief that solar technologies
> are compatible with democracy pertains to the way they complement aspects of
> society removed from the organization of those technologies as such. 
> (p. 130)

# A bomba atômica

> Taking the most obvious example, the atom bomb is an inherently political
> artifact. As long as it exists at all, its lethal properties demand that it be con
> trolled by a centralized, rigidly hierarchical chain of command closed to all
> influences that might make its workings unpredictable. The internal social sys
> tem of the bomb must be authoritarian; there is no other way. The state of
> affairs stands as a practical necessity independent of any larger political system
> in which the bomb is embedded, independent of the kind of regime or character
> of its rulers. Indeed, democratic states must try to find ways to ensure that the
> social structures and mentality that characterize the management of nuclear
> weapons do not "spin off' or "spill over" into the polity as a whole. 
> (p. 131)

# Neutralidade técnica como alienação política

> In many instances, to
> say that some technologies are inherently political is to say that certain widely
> accepted reasons of practical necessity?especially the need to maintain crucial
> technological systems as smoothly working entities?have tended to eclipse
> other sorts of moral and political reasoning. 
> (p. 133)

# O debate sobre plutônio e os riscos de monitoramento severo

> Well-known objections to plutonium recycling focus on its unac
> ceptable economic costs, its risks of environmental contamination, and its dan
> gers in regard to the international proliferation of nuclear weapons. Beyond these concerns, however, stands another less widely appreciated set of haz
> ards?those that involve the sacrifice of civil liberties. The widespread use of
> [134]
> plutonium as a fuel increases the chance that this toxic substance might be sto
> len by terrorists, organized crime, or other persons. This raises the prospect,
> and not a trivial one, that extraordinary measures would have to be taken to
> safeguard plutonium from theft and to recover it if ever the substance were
> stolen. Workers in the nuclear industry as well as ordinary citizens outside
> could well become subject to background security checks, covert surveillance,
> wiretapping, informers, and even emergency measures under martial law?all
> justified by the need to safeguard plutonium. 
> (p. 133-134)

> It is still true that, in a world in which human
> beings make and maintain artificial systems, nothing is "required" in an absolute
> sense. Nevertheless, once a course of action is underway, once artifacts like
> nuclear power plants have been built and put in operation, the kinds of reason
> ing that justify the adaptation of social life to technical requirements pop up as
> spontaneously as flowers in the spring. In Ayres's words, "Once recycling be
> gins and the risks of plutonium theft become real rather than hypothetical, the
> case for governmental infringement of protected rights will seem compelling."
> (p. 134)

# Ambas as formas de não-neutralidade tecnológica não são excludentes

> Indeed, it can happen that within a particular complex of technology?
> a system of communication or transportation, for example?some aspects may
> be flexible in their possibilities for society, while other aspects may be (for
> better or worse) completely intractable. The two varieties of interpretation I
> have examined here can overlap and intersect at many points. 
> (p. 135)

[^1]: WINNER, L. Do artifacts have politics? **Daedalus**, v. 109, n. 1, p. 121–36, 1980. 

[^2]: Cf. JOERGES, B. Do Politics Have Artefacts? **Social Studies of Science**, v. 29, n. 3, p. 411–431, 1999. 
