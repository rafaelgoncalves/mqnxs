---
title: A fábrica do sujeito neoliberal
tags:
    - Pierre Dardot
    - Christian Laval
    - psicanálise
    - saúde mental
    - desempenho
    - dispositivo de desempenho-gozo
    - capitalismo
    - neoliberalismo
    - fichamento

path: "/fabrica-do-sujeito-neoliberal"
date: 2024-04-22
category: fichamento
featuredImage: "dardot-laval.jpg"
srcInfo: <a href="https://blogdaboitempo.com.br/2016/04/14/a-esquerda-precisa-inventar-uma-outra-governamentalidade/">Dardot e Laval</a>
published: true
---

Fichamento do capítulo "A fábrica do sujeito neoliberal"[^1] do livro _A nova razão do mundo_ de Pierre Dardot e Christian Laval.

[^1]: A fábrica do sujeito neoliberal. Em: DARDOT, P.; LAVAL, C. **A nova razão do mundo**. 1. ed. São Paulo: Boitempo, 2016 [2009]. p. 321–376. 

# Sujeito neoliberal como correlato de um dispositivo de desempenho e gozo

> A concepção que vê a sociedade como uma empresa constituída de empresas necessita de uma nova norma subjetiva, que não é mais exatamente aquela do sujeito produtivo das sociedades industriais. **O sujeito neoliberal em formação, do qual gostaríamos de delinear aqui algumas das características principais, é correlato de um dispositivo de desempenho e gozo que foi objeto de inúmeros trabalhos. Não faltam hoje descrições do homem “hipermoderno”, “impreciso”, “flexível”, “precário”, “fluido”, “sem gravidade”.** Esses trabalhos preciosos, e muitas vezes convergentes, no cruzamento da psicanálise com a sociologia, revelam uma condição nova do homem, a qual, para alguns, afetaria a própria economia psíquica.
> (p. 321)

# Transformações do sujeito na comtemporaneidade na psicanálise e na sociologia ("ciência" e "capitalismo")

> De um lado, muitos psicanalistas dizem receber no consultório pacientes que sofrem de sintomas que revelam uma nova era do sujeito. Esse novo estado subjetivo é frequentemente referido na literatura clínica a amplas categorias, como a “era da ciência” ou o “discurso capitalista”. O fato de o histórico apropriar-se do estrutural não deveria surpreender os leitores de Lacan, para quem **o sujeito da psicanálise não é uma substância eterna nem uma invariante trans-histórica, mas efeito de discursos que se inserem na história e na sociedade.** De outro lado, no campo sociológico, a transformação do “indivíduo” é um fato inegável. **O que se designa no mais das vezes com o termo equívoco de “individualismo” é remetido ora a mutações morfológicas, segundo a tradição durkheimiana, ora à expansão das relações mercantis, segundo a tradição marxista, ora à extensão da racionalização a todos os domínios da existência, segundo uma linha mais weberiana.**
> (p. 321-2)

> Portanto, cada uma a sua maneira, psicanálise e sociologia registram uma mutação do discurso sobre o homem que pode ser reportado, como em Lacan, à ciência de um lado e ao capitalismo de outro: **trata-se precisamente de um discurso científico que, a partir do século XVII, começa a enunciar o que o homem é e o que ele deve fazer; e é para fazer do homem esse animal produtivo e consumidor, esse ser de labor e necessidade, que um novo discurso científico se propôs redefinir a medida humana.**
> (p. 322)

# O sujeito empresarial (homem-empresa) do fim do século XX

> Se existe um novo sujeito, ele deve ser distinguido nas práticas discursivas e institucionais que, no fim do século XX, engendraram a figura do homem-empresa ou do “sujeito empresarial”, favorecendo a instauração de uma rede de sanções, estímulos e comprometimentos que tem o efeito de produzir funcionamentos psíquicos de um novo tipo. Alcançar o objetivo de reorganizar completamente a sociedade, as empresas e as instituições pela multiplicação e pela intensificação dos mecanismos, das relações e dos comportamentos de mercado implica necessariamente um devir-outro dos sujeitos.
> (p. 322)

# Homem calculador e produtivo => homem competitivo

> O homem benthamiano era o homem _calculador_ do mercado e o homem _produtivo_ das organizações industriais. **O homem neoliberal é o homem _competitivo_, inteiramente imerso na competição mundial.** Foi dessa transformação que se falou nas páginas precedentes. Trataremos agora de descrever mais sistematicamente suas múltiplas formas.
> (p. 322)

# O mercado exerce a dissolução dos vínculos humanos (liberdade individual como sujeição às leis impessoais da valorização do Capital - Marx). Mercantilização das relações sociais.

> Marx, como outros, mas talvez melhor do que outros, apontou os efeitos de dissolução que o mercado exerce sobre os vínculos humanos. A mercantilização das relações sociais, juntamente com a urbanização, foi um dos fatores mais poderosos da “emancipação” do indivíduo com relação a tradições, raízes, apegos familiares e fidelidades pessoais. **A grandeza de Marx foi ter mostrado que o preço dessa liberdade subjetiva foi uma nova forma de sujeição às leis impessoais e incontroláveis da valorização do capital.**
> (p. 323)

> O indivíduo liberal, a exemplo do sujeito lockiano proprietário de si mesmo, podia acreditar que gozava de todas as suas faculdades naturais, do livre exercício de sua razão e vontade, podia proclamar ao mundo sua autonomia irredutível, mas continuava a ser uma engrenagem dos grandes mecanismos que a economia política clássica começava a analisar.
> (p. 323-4)

# Individualismo moderno como contratualização (substituto da filiação, aliança e reciprocidade simbólica). As relações sociais hoje são experimentadas como contratos.

> Nas relações humanas, essa mercantilização expansiva tomou a forma geral da _contratualização_. Os contratos voluntários entre pessoas livres – obviamente sempre garantidos pela instância soberana – substituíram as formas institucionais da aliança e da filiação e, mais em geral, as formas antigas da reciprocidade simbólica.
> (p. 324)

> **O contrato tornou-se mais do que nunca a medida de todas as relações humanas, de modo que o indivíduo passou a experimentar cada vez mais na relação com o outro sua plena e total liberdade de compromisso voluntário e a perceber a “sociedade” como um conjunto de relações de associação entre pessoas dotadas de direitos sagrados.** Esse é o cerne do que se convencionou chamar “individualismo” moderno.
> (p. 324)

# Dispositivo de eficácia (processos de normatização e técnicas disciplinares)

> Como mostrou Émile Durkheim, havia nisso uma ilusão singular, na medida em que, no contrato, há sempre mais do que o contrato: sem o Estado garantidor, não existiria liberdade pessoal. Mas também podemos dizer, como Michel Foucault, que, sob o contrato, há algo diferente do contrato ou ainda que, sob a liberdade subjetiva, há algo diferente da liberdade subjetiva. **Há um arranjo de processos de normatização e técnicas disciplinares que constituem o que podemos chamar de _dispositivo de eficácia_. Os sujeitos nunca teriam se “convertido” de forma voluntária ou espontânea à sociedade industrial e mercantil apenas por causa da propaganda do livre-câmbio ou dos atrativos do enriquecimento privado.** Era preciso pensar e implantar, “por uma estratégia sem estrategistas”, os tipos de educação da mente, de controle do corpo, de organização do trabalho, moradia, descanso e lazer que seriam a forma institucional do novo ideal de homem, a um só tempo indivíduo calculador e trabalhador produtivo.
> (p. 324)

> Foi esse dispositivo de eficácia que forneceu à atividade econômica os “recursos humanos” necessários, foi ele que produziu incessantemente as mentes e os corpos aptos a funcionar no grande circuito da produção e do consumo. Em uma palavra, **a nova normatividade das sociedades capitalistas impôs-se por uma normatização subjetiva de um tipo particular.**
> (p. 324)

# Ação disciplinar sobre os corpos como momento de um funcionamento da subjetividade (do ponto de vista da disciplina)

> Foucault forneceu uma primeira cartografia desse processo – aliás, uma cartografia problemática. O princípio geral do dispositivo de eficácia não é tanto, como se disse muitas vezes, um “adestramento dos corpos”, mas uma “gestão das mentes”. Ou antes deveríamos dizer que **a ação disciplinar sobre os corpos foi apenas um momento e um aspecto da elaboração de certo modo de funcionamento da subjetividade.**
> (p. 325)

# Panóptico de Bentham como moldagem subjetiva

> O panóptico de Bentham é particularmente emblemático dessa moldagem subjetiva.
> (p. 325)

> O novo governo dos homens penetra até em seu pensamento, acompanha, orienta, estimula, educa esse pensamento. **O poder já não é somente a vontade soberana, mas, como Bentham diz tão bem, torna-se “método oblíquo” ou “legislação indireta”, destinada a conduzir os interesses.** Postular a liberdade de escolha, suscitar e constituir na prática essa liberdade, pressupõe que os sujeitos sejam conduzidos por uma “mão invisível” a fazer as escolhas que serão proveitosas a todos e cada um.
> (p. 325)

> Por trás dessa representação encontra-se não tanto um grande engenheiro, como no modelo do grande Relojoeiro, mas uma máquina que funciona idealmente por si só e encontra em cada sujeito uma engrenagem pronta a responder às necessidades de arranjo do conjunto. Contudo, é preciso fabricar e manter essa engrenagem.
> (p. 325)

# Panóptico como vigilância de todos por cada um e por todos 

> Já no século XVIII, iniciam-se as bodas da mecânica econômica com a psicofisiologia das sensações. Esse é, sem dúvida, o cruzamento decisivo que vai definir a nova economia do homem governado pelos prazeres e pelas dores. Governado e governável pelas sensações: se o indivíduo deve ser considerado em sua liberdade, ele também é um rematado patife, um “delinquente em potencial”, um ser movido antes de tudo por seu próprio interesse. A nova política inaugura-se com o monumento panóptico erguido em glória da vigilância de todos por cada um e de cada um por todos.
> (p. 325)

# O homem eficaz como agente ideal para a felicidade geral

> Mas, podemos nos perguntar, por que vigiar os sujeitos e maximizar o poder? A resposta impõe-se por si só: para produzir a maior felicidade. **A lei da eficácia é intensificar os esforços e os resultados e minimizar os gastos inúteis. Fabricar homens úteis, dóceis ao trabalho, dispostos ao consumo, fabricar o _homem eficaz_ é o que já começa a se delinear, e de que maneira, na obra benthamiana.**
> (p. 325-6)

# Utilitarismo clássico -> neoliberalismo moderno (homogeneização do discurso do humano em torno da figura empresa)

> Mas o utilitarismo clássico, apesar de seu enorme trabalho de pulverização das categorias antigas, não conseguiu explicar a pluralidade interna do sujeito nem a separação das esferas a que correspondia essa pluralidade. O princípio de utilidade, cuja vocação homogeneizante era clara, não conseguiu abranger todos os discursos e as instituições, do mesmo modo que o equivalente geral da moeda não conseguiu subordinar todas as atividades sociais. **Precisamente esse caráter plural do sujeito e essa separação das esferas práticas é que estão em questão hoje.**
> (p. 326)

> Por oposição, **o momento neoliberal caracteriza-se por uma homogeneização do discurso do homem em torno da figura da empresa.** Essa nova figura do sujeito opera uma unificação sem precedentes das formas plurais da subjetividade que a democracia liberal permitiu que se conservassem e das quais sabia aproveitar-se para perpetuar sua existência.
> (p. 326)

# Sujeito empresarial, sujeito neoliberal, neossujeito

> A partir de então, diversas técnicas contribuem para a fabricação desse **novo sujeito unitário, que chamaremos indiferentemente de “sujeito empresarial”, “sujeito neoliberal” ou, simplesmente, _neossujeito_.** Não estamos mais falando das antigas disciplinas que se destinavam, pela coerção, a adestrar os corpos e a dobrar os espíritos para torná-los mais dóceis – metodologia institucional que se encontrava em crise havia muito tempo. **Trata-se agora de governar um ser cuja subjetividade deve estar inteiramente envolvida na atividade que se exige que ele cumpra.** Para isso, deve-se reconhecer nele a parte irredutível do desejo que o constitui.
> (p. 327)

# O desejo como alvo do poder. Gestão do sujeito do envolvimento total (vontade de realização pessoal). Identificação do sujeito trabalhador com a empresa empregadora (trabalha para a empresa como se trabalhasse para si mesmo).

> As grandes proclamações a respeito da importância do “fator humano” que pululam na literatura da neogestão devem ser lidas à luz de um novo tipo de poder; não se trata mais de reconhecer que o homem no trabalho continua a ser um homem, que ele nunca se reduz ao status de objeto passivo; **trata-se de ver nele o sujeito ativo que deve participar inteiramente, engajar-se plenamente, entregar-se por completo a sua atividade profissional. O sujeito unitário é o sujeito do envolvimento total de si mesmo. A vontade de realização pessoal, o projeto que se quer levar a cabo, a motivação que anima o “colaborador” da empresa, enfim, o _desejo_ com todos os nomes que se queira dar a ele é o alvo do novo poder.** O ser desejante não é apenas o ponto de aplicação desse poder; ele é o substituto dos dispositivos de direção das condutas. Porque **o efeito procurado pelas novas práticas de fabricação e gestão do novo sujeito é fazer com que o indivíduo trabalhe para a empresa como se trabalhasse para si mesmo e, assim, eliminar qualquer sentimento de alienação e até mesmo qualquer distância entre o indivíduo e a empresa que o emprega.** Ele deve trabalhar para sua própria eficácia, para a intensificação de seu esforço, como se essa conduta viesse dele próprio, como se esta lhe fosse comandada de dentro por uma ordem imperiosa de seu próprio desejo, à qual ele não pode resistir.
> (p. 327)

# Alienação pela aparência de supressão da alienação

> As novas técnicas da “empresa pessoal” chegam ao cúmulo da alienação ao pretender suprimir qualquer sentimento de alienação: obedecer ao próprio desejo ou ao Outro que fala em voz baixa dentro de nós dá no mesmo.
> (p. 327)

# Gestão moderna como governo "lacaniano": o desejo do sujeito é o desejo do Outro. Poder moderno como Outro (racionalidade neoliberal === supereu?).

> Nesse sentido, a gestão moderna é um governo “lacaniano”: o desejo do sujeito é o desejo do Outro. Desde que o poder moderno se torne o Outro do sujeito. A construção das figuras tutelares do mercado, da empresa e do dinheiro tende exatamente a isso. Mas é isso sobretudo que se consegue obter com as técnicas refinadas de motivação, estímulo e incentivo.
> (p. 327-8)

# Governamentalidade empresarial ligado a uma racionalidade neoliberal homogênea quanto ao discurso em diversas esferas da vida. Racionalidade cria o sujeito que precisa para governá-lo.

> A _governamentalidade empresarial_ está ligada a uma racionalidade de conjunto que tira força de seu próprio caráter abrangente, já que permite descrever as novas aspirações e as novas condutas dos sujeitos, prescrever os modos de controle e influência que devem ser exercidos sobre eles em seus comportamentos e redefinir as missões e as formas da ação pública. **Do sujeito ao Estado, passando pela empresa, um mesmo discurso permite articular uma definição do homem pela maneira como ele quer ser “bem-sucedido”, assim como pelo modo como deve ser “guiado”, “estimulado”, “formado”, “empoderado” (empowered) para cumprir seus “objetivos”.** Em outras palavras, **a racionalidade neoliberal produz o sujeito de que necessita ordenando os meios de governá-lo para que ele se conduza realmente como uma entidade em competição e que, por isso, deve maximizar seus resultados, expondo-se a riscos e assumindo inteira responsabilidade por eventuais fracassos.** “Empresa” é também o nome que se deve dar ao governo de si na era neoliberal.
> (p. 328)

# Cultura de empresa > governo de si empresarial. Superação no imaginário da contradição entre hedonismo no consumo e ascetismo no trabalho

> O que quer dizer que esse “governo de si empresarial” é diferente e muito mais do que a “cultura de empresa” da qual falamos acima. É claro que a valorização ideológica do modelo da empresa faz parte dele; é claro que a empresa é considerada em toda parte um lugar de realização pessoal, a instância onde finalmente se podem conjugar o desejo de realização pessoal dos indivíduos, seu bem-estar material, o sucesso comercial e financeiro da “comunidade” de trabalho e sua contribuição para a prosperidade geral da população. A nova gestão ambiciona superar no plano imaginário a contradição que Daniel Bell encontrou entre os valores hedonistas do consumo e os valores ascéticos do trabalho.
> (p. 328)

# Precarização do trabalho (o trabalhador vira uma mercadoria) nas formas de sujeição modernas

> Estas [[formas mais eficazes de sujeição, produzidas pelo humanismo/hedonismo da gestão moderna]], por mais novas que sejam, têm a marca da mais inflexível e mais clássica das violências sociais típicas do capitalismo: a tendência a transformar o trabalhador em uma simples mercadoria. A corrosão progressiva dos direitos ligados ao status de trabalhador, a insegurança instilada pouco a pouco em todos os assalariados pelas “novas formas de emprego” precárias, provisórias e temporárias, as facilidades cada vez maiores para demitir e a diminuição do poder de compra até o empobrecimento de frações inteiras das classes populares são elementos que produziram um aumento considerável do grau de dependência dos trabalhadores com relação aos empregadores.
> (p. 329)

# Contexto de medo e neogestão empresarial (naturalização dos "riscos" do neoliberalismo)

> Foi esse contexto de medo social que facilitou a implantação da neogestão nas empresas. Nesse sentido, a “naturalização” do risco no discurso neoliberal e a exposição cada vez mais direta dos assalariados às flutuações do mercado, pela diminuição das proteções e das solidariedades coletivas, são apenas duas faces de uma mesma moeda. Transferindo os riscos para os assalariados, produzindo o aumento da sensação de risco, as empresas puderam exigir deles disponibilidade e comprometimento muito maiores.
> (p. 329)

# Novidade da neogestão: reação em cadeia produzindo sujeitos empreendedores que ampliam e reforçam as relações de competição neoliberais

> Isso não significa que a neogestão não seja novidade e o capitalismo no fundo seja sempre o mesmo. Ao contrário, **a grande novidade reside na modelagem que torna os indivíduos aptos a suportar as novas condições que lhe são impostas, enquanto por seu próprio comportamento contribuem para tornar essas condições cada vez mais duras e mais perenes.** Em uma palavra, a novidade consiste em promover uma **“reação em cadeia”, produzindo “sujeitos empreendedores” que, por sua vez, reproduzirão, ampliarão e reforçarão as relações de competição entre eles, o que exigirá, segundo a lógica do processo autorrealizador, que eles se adaptem subjetivamente às condições cada vez mais duras que eles mesmos produziram.**
> (p. 329)

# O novo espírito do capitalismo. Problema: considerar como ideologia, não como racionalidade. Acreditar no discurso sobre si mesmo.

> É isso que escapa a Luc Boltanski e Ève Chiapello em _O novo espírito do capitalismo_. Tomando como objeto a ideologia que, segundo a definição que dão do espírito do capitalismo, “justifica o engajamento no capitalismo”, eles tendem a acreditar piamente no que o novo capitalismo diz de si mesmo na literatura gerencial dos anos 1990. Sem dúvida é importante destacar que essa literatura recuperou certo tipo de crítica da burocracia, da organização e da hierarquia para melhor desacreditar o modelo antigo de poder baseado na gestão dos diplomas, dos status e das carreiras. Também é importante mostrar a que ponto a apologia da incerteza, da reatividade, da flexibilidade, da criatividade e da rede de contatos constitui uma representação coerente, cheia de promessas, que favorece a adesão dos assalariados ao modelo “conexionista” do capitalismo.
> (p. 329-30)

> Isso, porém, é ressaltar apenas a face sedutora e estritamente retórica dos novos modos de poder. É esquecer que estes últimos tiveram como efeito a constituição de uma subjetividade particular por meio de técnicas específicas. Em suma, é subestimar o aspecto propriamente disciplinar do discurso gerencial, tomando sua argumentação muito ao pé da letra. 
> (p. 330)

> **A neogestão não é “antiburocrática”. Ela corresponde a uma nova fase, mais sofisticada, mais “individualizada”, mais “competitiva” da racionalização burocrática, e é apenas em consequência de uma ilusão que ele se apoiou na “crítica artista” de 1968 para assegurar a mutação de uma forma de poder organizacional em outra.** Nós não saímos da “jaula de aço” da economia capitalista a que se referia Weber. Em certos aspectos, **seria melhor dizer que cada indivíduo é obrigado a construir, por conta própria, sua “jaula de aço” individual.**
> (p. 330)

# A empresa como locus de competição. Produção do sujeito competitivo através da economia.

> Com efeito, o novo governo dos sujeitos pressupõe que a empresa não seja uma “comunidade” ou um lugar de realização pessoal, mas um instrumento e um espaço de competição.
> (p. 330)

> Desse modo, injunge-se o sujeito a conformar-se intimamente, por um trabalho interior constante, à seguinte imagem: ele deve cuidar constantemente para ser o mais eficaz possível, mostrar-se inteiramente envolvido no trabalho, aperfeiçoar-se por uma aprendizagem contínua, aceitar a grande flexibilidade exigida pelas mudanças incessantes impostas pelo mercado. **Especialista em si mesmo, empregador de si mesmo, inventor de si mesmo, empreendedor de si mesmo: a racionalidade neoliberal impele o eu a agir sobre si mesmo para fortalecer-se e, assim, sobreviver na competição.** Todas as suas atividades devem assemelhar-se a uma produção, a um investimento, a um cálculo de custos. **A economia torna-se uma disciplina pessoal. Foi Margaret Thatcher quem deu a formulação mais clara dessa racionalidade: “_Economics are the method_. The object is to change the soul” [_A economia é o método_. O objetivo é mudar a alma].**
> (p. 330-1)

# Técnicas de gestão como instrumentos de modulação da conduta, autoavaliação, etc. adesão da norma neoliberal "a força". Sujeição subjetiva total.

> As técnicas de gestão (avaliação, projeto, normatização dos procedimentos, descentralização) supostamente permitem objetivar a adesão do indivíduo à norma de conduta que se espera dele, avaliar por tabelas e outras ferramentas de registro do “painel de gestão” seu comprometimento subjetivo, sob pena de sofrer sanções no emprego, no salário e no desenvolvimento de sua carreira.
> (p. 331)

> No entanto, o essencial não é a verdade dessa medição, mas o tipo de poder que é exercido “profundamente” sobre o sujeito impelido a “entregar-se completamente”, a “transcender-se” pela empresa, a “motivar-se” cada vez mais para satisfazer o cliente, isto é, **intimado pelo tipo de contrato que o vincula à empresa e pelo modo de avaliação que lhe é aplicado a provar seu comprometimento pessoal com o trabalho.**
> (p. 331)

# Nova ética baseada na empresa (governo de si e dos outros)

> Estabelecendo uma correspondência íntima entre o governo de si e o governo das sociedades, a empresa define uma nova ética, isto é, certa disposição interior, certo _ethos_ que deve ser encarnado com um trabalho de vigilância sobre si mesmo e que os procedimentos de avaliação se encarregam de reforçar e verificar.
> (p. 332)

# Autoajuda como ética empreendedora

> Nessas condições, pode-se dizer que o primeiro mandamento da ética do empreendedor é “ajuda-te a ti mesmo” e que, nesse sentido, ela é a ética do _self-help_ [autoajuda].
> (p. 332)

# Tecnologia neoliberal vincula o governo de si ao governo exercido por outro sobre si

> Precisamente, a grande inovação da tecnologia neoliberal é vincular diretamente a maneira como um homem “é governado” à maneira como ele próprio “se governa”.
> (p. 332-3)

# Governo de si e autoajuda como racionalização dos desejos (para posterior gestão, avaliação, etc.)

> **Isso pressupõe todo um trabalho de racionalização até o mais íntimo do sujeito: uma racionalização do desejo.** Esta está no centro da norma da empresa de si mesmo. Como ressalta um de seus tecnólogos, Bob Aubrey, consultor internacional californiano, “falar em empresa de si mesmo é traduzir a ideia de que cada indivíduo pode ter domínio sobre sua vida: conduzi-la, geri-la e controlá-la em função de seus desejos e necessidades, elaborando estratégias adequadas”.
> (p. 333)

# Formação continuada de si (não _homo oeconomicus_ natural)

> Trata-se do indivíduo competente e competitivo, que procura maximizar seu capital humano em todos os campos, que não procura apenas projetar-se no futuro e calcular ganhos e custos como o velho homem econômico, mas que procura sobretudo _trabalhar a si mesmo_ com o intuito de transformar-se continuamente, aprimorar-se, tornar-se sempre mais eficaz.
> (p. 333)

# Identificação de objetivos individuais e empresariais que só se efetiva se o sujeito se torna uma pequena empresa (sociedade seria composta de empresas de si mesmos)

> O grande princípio dessa nova ética do trabalho é a ideia de que a conjunção entre as aspirações individuais e os objetivos de excelência da empresa, entre o projeto pessoal e o projeto da empresa, somente é possível se cada indivíduo se tornar uma pequena empresa. Em outras palavras, **isso pressupõe conceber a empresa como uma entidade composta de pequenas empresas de si mesmo.**
> (p. 334)

> Preocupado em dar uma caução teórica a essa nova ética, Aubrey afirma ter tomado a expressão “empresa de si mesmo” de Foucault para transformá-la num método de formação profissional. 
> (p. 335)

> O que devemos entender por essa afirmação? A empresa de si mesmo é uma “entidade psicológica e social, e mesmo espiritual”, ativa em todos os domínios e presente em todas as relações. É sobretudo a resposta a uma nova regra do jogo que muda radicalmente o contrato de trabalho, a ponto de aboli-lo como relação salarial. **A responsabilidade do indivíduo pela valorização de seu trabalho no mercado tornou-se um princípio absoluto.**
> (p. 335)

> Desse ponto de vista, **o uso da palavra “empresa” não é uma simples metáfora, porque toda a atividade do indivíduo é concebida como um processo de valorização do eu.** O termo significa que a
> 
> > atividade do indivíduo, sob suas diferentes facetas (trabalho remunerado, trabalho beneficente para uma associação, gestão do lar familiar, aquisição de competências, desenvolvimento de uma rede de contatos, preparação para uma mudança de atividade etc.), é pensada em sua essência como empresarial.
>  (p. 335)

> Toda atividade é empresarial, porque nada mais é garantido para toda a vida. Tudo deve ser conquistado e defendido a todo momento. A criança mesmo deve ser “empreendedora de seu saber”. Desse ponto de vista, **tudo se torna empresa: o trabalho, mas também o consumo e o lazer, já que “se procura tirar deste o máximo de riquezas, utilizá-lo para a realização de si mesmo como maneira de criar”.**
> (p. 336)

# Objetivo da gestão moderna é aliciar as subjetividades pelo controle, avaliações, etc.

> Se essa ética neoliberal do eu não se restringe aos limites da empresa, é não só porque o ser bem-sucedido na carreira confunde-se com o ser bem-sucedido na vida, mas, ainda mais fundamentalmente, porque a gestão moderna tenta “aliciar as subjetividades” com a ajuda de controles e avaliações de personalidade, inclinações de caráter, maneiras de ser, falar e mover-se, quando não de motivações inconscientes.
> (p. 338)

# Sobre programação neurolinguística (PNL), análise transacional (AT), coaching, etc. como técnicas de conduta de si e dos outros.

> É importante notar, sobretudo, que são técnicas que visam à “conduta de si e dos outros” ou, em outras palavras, técnicas de governamentalidade que visam essencialmente a aumentar a eficácia da relação com o outro.
> (p. 339-40)

# Domínio de si como remédio para o descontrole do mundo

> O domínio de si mesmo e das relações comunicacionais aparece como contrapartida de uma situação global que ninguém consegue mais controlar. Se não há mais domínio global dos processos econômicos e tecnológicos, o comportamento dos indivíduos não é mais programável, não é mais inteiramente descritível e prescritível. O domínio de si mesmo coloca-se como uma espécie de compensação ao domínio impossível do mundo. O indivíduo é o melhor, senão o único “integrador” da complexidade e o melhor ator da incerteza.
> (p. 342)

# Norma geral de eficácia na empresa -> na subjetividade (isso gerará bem estar e gratificação profissional)

> No fim das contas, trata-se de **fazer com que a norma geral de eficácia que se aplica à empresa seja substituída, no nível individual, por um uso da subjetividade destinado a melhorar o desempenho do indivíduo** – seu bem-estar e sua gratificação profissional são dados apenas como consequência dessa melhoria.
> (p. 343)

# Coerção econômica e financeira -> autocoerção, autoculpabilização

> O chefe não pode mais impor: ele deve vigiar, fortalecer, apoiar a motivação. Dessa forma, **a coerção econômica e financeira transforma-se em _autocoerção_ e _autoculpabilização_, já que somos os únicos responsáveis por aquilo que nos acontece.**
> (p. 345)

# Toda a subjetividade deve ser modulada pelas práticas de gestão neoliberais

> Todos os domínios da vida individual tornam-se potencialmente “recursos” indiretos para a empresa, já que são uma oportunidade para o indivíduo melhorar seu desempenho pessoal; todos os domínios da existência são da competência da gestão de si. Portanto, _toda a subjetividade_, e não apenas o “homem no trabalho”, é convocada para esse modo de gestão, mais ainda na medida em que a empresa seleciona e avalia de acordo com critérios cada vez mais “pessoais”, físicos, estéticos, relacionais e comportamentais.
> (p. 346)

# A sociedade de risco: o capitalismo destrói a dimensão coletiva da existência (individualização)

> Aliás, a nosso ver, é isso que deve ser retido dos trabalhos de Ulrich Beck e da _Sociedade de risco_. Para ele, o capitalismo avançado destrói a dimensão coletiva da existência: destrói não só as estruturas tradicionais que o precederam, sobretudo a família, mas também as estruturas que ajudou a criar, como as classes sociais. **Assistimos a uma individualização radical que faz com que todas as formas de crise social sejam percebidas como crises individuais, todas as desigualdades sejam atribuídas a uma responsabilidade individual.** A maquinaria instaurada “transforma as causas externas em responsabilidades individuais e os problemas ligados ao sistema em fracassos pessoais”.
> (p. 348)

# Dispositivo informacional transfere o risco para cada agente que possui informações e escolhe

> Isso significa ver uma complementaridade ideológica entre a norma de mercado baseada na “livre escolha” do sujeito racional e a “transparência” do funcionamento social, condição necessária para uma escolha ótima. Mas significa, sobretudo, **instaurar um mecanismo que identifica o _compartilhamento_ da informação e o _compartilhamento_ do risco: a partir do momento que se supõe que o indivíduo tem condições de acessar as informações necessárias para sua escolha, deve-se supor que ele se torna plenamente responsável pelos riscos envolvidos.** Em outras palavras, a implantação de um dispositivo informacional de tipo comercial ou legal permite uma transferência do risco para o doente que “escolhe” determinado tratamento ou operação, para o estudante ou o “desempregado” que “escolhem” certo curso de formação, o futuro aposentado que “escolhe” uma modalidade de poupança, o turista que aceita as condições do percurso etc.
> (p. 349)

# Accountability como subjetivação contábil

> Se seguirmos os diferentes sentidos do termo inglês em uso [[_accountability_]], significa que o indivíduo deve ser responsável por si mesmo, responder por seus atos diante dos outros e ser inteiramente calculável. Como diz Pezet: “a ‘responsabilização’ dos indivíduos não os torna apenas responsáveis: eles devem responder por seu comportamento a partir de escalas de medida dadas pelos serviços de gestão de recursos humanos e pelos administradores”. **A “avaliação” tornou-se o primeiro meio de orientar a conduta pelo estímulo ao “bom desempenho” individual. Ela pode ser definida como uma relação de poder exercida por superiores hierárquicos encarregados da _expertise_ dos resultados, uma relação cujo efeito é uma _subjetivação contábil_ dos avaliados.**
> (p. 350-1)

# Sujeito neoliberal é controlado por instrumentos imediatos, constantes e mais objetiváveis (do que o sujeito calculista bemthamiano)

> O sujeito neoliberal, portanto, não é o sujeito benthamiano. Este último, como sabemos, é governável pelo cálculo, porque é calculista. Ora, não se trata mais, como no utilitarismo clássico, de dispor de um quadro legal e de um conjunto de medidas de “legislação indireta” conhecidos de todos para que o indivíduo calcule melhor; **trata-se de empregar instrumentos muito mais próximos do indivíduo (superior imediato), mais constantes (resultados contínuos da atividade) e mais objetiváveis (medidas quantitativas levantadas por registro informatizado).**
> (p. 351)

# Transição de um regime probabilistico baseado em classificações (diplomas, status, ...) para um mais refinado baseado em uma avaliação fina e regular da competência prática do indivíduo a todo instante

> O antigo sistema de julgamento burocrático baseava-se na probabilidade estatística de um elo entre a posição do indivíduo na classificação e sua eficácia pessoal.
> (p. 351)

> Tudo isso muda quando se deixa de querer prejulgar a eficácia do sujeito por títulos, diplomas, status, experiência acumulada, ou seja, a posição que ele ocupa numa classificação, porque **passa-se a confiar na avaliação mais fina e regular de suas competências postas efetivamente em prática a todo instante.**
> (p. 351-2)

> O sujeito não vale mais pelas qualidades estatutárias que lhe foram reconhecidas durante sua trajetória escolar e profissional, mas pelo valor de uso diretamente mensurável de sua força de trabalho. Vemos, então, que **o modelo humano da empresa de si mesmo é requerido nesse modo de poder que deseja impor um regime de sanção homólogo ao do mercado.**
> (p. 352)

# Subjetivação financeira: responsabilização/avaliação visando valor acionário produzido por cada atividade desempenhada. O assalariado se torna um objeto financeiro, criador de valor acionário e responsável ante aos acionistas.

> O ideal – que constitui como que o modelo dessa atividade de avaliação, inclusive nos setores mais distantes da prática financeira, como saúde mental, educação, serviços de cuidado à pessoa e justiça – consistiria em poder avaliar os ganhos produzidos por cada equipe ou indivíduo considerados responsáveis pelo valor acionário produzido pela atividade que realizam. **A transposição da auditoria a que estão sujeitos os “centros de resultados” da empresa ao conjunto das atividades econômicas, sociais, culturais e políticas envolve uma verdadeira lógica de _subjetivação financeira_ dos assalariados.** Todo produto torna-se um “objeto financeiro”, e o próprio sujeito é instituí­do como um criador de valor acionário, responsável perante os acionistas.
> (p. 352)

# Dispositivo desempenho/gozo como substituto poderoso do dispositivo industrial da eficácia

> Não compreenderíamos a extensão do desdobramento da racionalidade neoliberal, ou as formas de resistência encontradas por ela, se a víssemos como imposição de uma força mecânica sobre uma sociedade e indivíduos da qual eles seriam pontos de aplicação externos. O poder dessa racionalidade, como vimos, deve-se à instauração de situações que forçam os sujeitos a funcionar de acordo com os termos do jogo imposto a eles. Mas o que é funcionar como uma empresa num contexto de situação de concorrência? Em que medida isso nos leva a um “novo sujeito”? Abordaremos aqui apenas alguns dos elementos que compõem **o dispositivo de desempenho/gozo e mostram diretamente sua novidade em relação ao dispositivo industrial de eficácia.**
> (p. 353)

# O neosujeito deve produzir sempre mais e gozar sempre mais (mais-de-gozar)

> Exige-se do novo sujeito que produza “sempre mais” e goze “sempre mais” e, desse modo, conecte-se diretamente com um “mais-de-gozar” que se tornou sistêmico. **A própria vida, em todos os seus aspectos, torna-se objeto dos dispositivos de desempenho e gozo.**
>  (p. 355-6)

# Unificação da economia política e libidinal em "Capitalismo e Esquizofrenia". Mecanismo desterriotorialização-reterritorialização (decodificação e axiomatização de fluxos) na máquina capitalista. A sociedade de controle.

> [67] Essa intensificação e essa aceleração é que deram a Gilles Deleuze e Félix Guattari a ideia inicial de outra economia política não separada da economia libidinal, exposta em _O anti-Édipo_ [trad. Luiz B. L. Orlandi, 2. ed., São Paulo, Editora 34, 2014] e _Mil platôs_ [trad. Ana Lúcia de Oliveira, Aurélio Guerra Neto e Célia Pinto Costa, 2. ed., São Paulo, Editora 34, 2014]. Para eles, o capitalismo só pode funcionar com a liberação dos fluxos desejantes que excedem os quadros sociais e políticos estabelecidos para a própria reprodução do sistema de produção. É nesse sentido que o processo de subjetivação próprio do capitalismo é qualificado como “esquizofrênico”. Mas, **apesar de o capitalismo só poder funcionar com a liberação de doses cada vez maiores de energia libidinal que “decodificam” e “desterritorializam”, ele tenta reincorporá-las continuamente à máquina produtiva. “Quanto mais a máquina capitalista desterritorializa, decodificando e axiomatizando os fluxos para extrair deles o mais-valor, mais seus aparatos anexos, burocráticos e policiais, reterritorializam, absorvendo uma parte crescente de mais-valor”** (Gilles Deleuze e Félix Guattari, _L’anti-Œdipe_, Paris, Minuit, 1972, p. 42). Se nos anos 1970 Deleuze dá ênfase às máquinas repressivas “paranoicas”, que tentam dominar inutilmente as linhas de fuga do desejo, **mais tarde ele ressaltará a relação entre essa liberação dos fluxos desejantes e os dispositivos de guiamento dos fluxos na “sociedade de controle”, entre o modo de subjetivação por estímulo do “desejo” e a avaliação generalizada dos desempenhos.** Ver Gilles Deleuze, “Contrôle et devenir” e “Post-scriptum sur les sociétés de contrôle”, em _Pourparlers_ (Paris, Minuit, 1990) [ed. bras.: _Conversações: 1972-1990_, trad. Peter Pál Pelbart, São Paulo, Editora 34, 2013].
> (p. 355-6)


# Mais-valor e acumulação do capital em Marx e subjetivação pelo excesso de si (ultrassubjetivação) no neoliberalismo (mercado mundial como cosmo)

> A máquina econômica, mais do que nunca, não pode funcionar em equilíbrio e, menos ainda, com perda. Ela tem de mirar um “além”, um “mais”, que Marx identificou como “mais-valor”.
> (p. 356)

> Até então, essa exigência própria do regime de acumulação do capital não havia desdobrado todos os seus efeitos. Isso aconteceu quando o comprometimento subjetivo foi tal que a procura desse “além de si mesmo” tornou-se a condição de funcionamento tanto dos sujeitos como das empresas. Daí o interesse da identificação do sujeito como empresa de si mesmo e capital humano: a extração de um “mais-de-gozar”, tirado de si mesmo, do prazer de viver, do simples fato de viver, é que faz funcionar o novo sujeito e o novo sistema de concorrência. **Em última análise, subjetivação “contábil” e subjetivação “financeira” definem uma _subjetivação pelo excesso de si em si_ ou, ainda, pela _superação indefinida de si_.**
> (p. 356-7)

> Consequentemente, aparece uma figura inédita da subjetivação. Não uma “trans-subjetivação”, o que implicaria mirar um além de si mesmo que consagraria um rompimento consigo mesmo e uma renúncia de si mesmo. Tampouco uma “autossubjetivação” pela qual se procuraria alcançar uma relação ética consigo mesmo, independentemente de qualquer outra finalidade, de tipo político ou econômico. De certa forma, **trata-se de uma “*ultra*ssubjetivação”, cujo objetivo não é um estado último e estável de “posse de si”, mas um além _de_ si sempre repelido e, além do mais, constitucionalmente ordenado, em seu próprio regime, segundo a lógica da empresa e, para além, segundo o “cosmo” do mercado mundial.**
> (p. 357)

# Concepção psicológica do ser humano, normatização econômica da concorrência, representação do indivíduo como capital humano, coesão da organização pela comunicação e vinculo social como rede = empresa de si

> Da mesma maneira, é pela combinação da concepção psicológica do ser humano, da nova norma econômica da concorrência, da representação do indivíduo como “capital humano”, da coesão da organização pela “comunicação”, do vínculo social como “rede”, que se construiu pouco a pouco essa figura da “empresa de si”.
> (p. 358)

# O discurso "psi" como técnica de governo de si e dos outros. Conjunção de enunciados economicos e "psi". Identificação do sujeito psicológico e produtivo.

> Nikolas Rose mostrou em seus trabalhos, muito inspirado nas pesquisas de Foucault, que o discurso “psi”, com seu poder de _expertise_ e sua legitimidade científica, contribuiu largamente para a definição do indivíduo governável moderno. O discurso “psi”, entendido como “tecnologia intelectual”, permitiu que os indivíduos fossem conduzidos a partir de um saber relativo a sua constituição interna. Fazendo isso, **formou indivíduos que aprenderam a conceber-se como seres psicológicos, a julgar-se e modificar-se por um trabalho em si mesmos, ao mesmo tempo que deu às instituições e aos governantes meios de dirigir a conduta desses indivíduos.** Concebendo o sujeito como lugar de paixões, desejos e interesses, mas também de normas e julgamentos morais, pôde-se compreender como as forças psicológicas são móbiles de conduta, e como agir tecnicamente no campo psíquico por meio de sistemas adaptados de estímulo, incentivo, recompensa, punição. **Todo um conjunto de técnicas de diagnóstico e “ortopedia psíquica”, no campo educacional, profissional e familiar, foi integrado ao grande dispositivo de eficácia das sociedades industriais. A ideia diretriz era a da adaptação mútua dos móbiles psicológicos e das coerções sociais e econômicas, o que nos ensinou a ver a “personalidade” e o “fator humano” como um recurso econômico pelo qual se deve “zelar”.**
> (p. 358-9)

> Foi ainda essa conjunção [[de enunciados econômicos e do tipo "psi"]] que deu origem a essas técnicas de si que visam ao desempenho individual por meio de uma racionalização gerencial do desejo. Mas foi outra modalidade dessa conjunção que permitiu o desenvolvimento do dispositivo de desempenho/gozo, uma modalidade que consiste não em perguntar em que medida o indivíduo e a empresa, cada qual com suas exigências próprias, podem adaptar-se um ao outro, mas como o sujeito psicológico e o sujeito da produção podem _identificar-se_. 
> (p. 360)

# Liberdade como obrigação de desempenho

> **A liberdade tornou-se uma obrigação de desempenho.** O normal não é mais o domínio e a regulação das pulsões, mas sua estimulação intensiva como principal fonte de energia. É em torno da norma da competição entre empresas de si mesmo que a fusão do discurso “psi” com o discurso econômico se opera, que as aspirações individuais e os objetivos de excelência da empresa se identificam, que, em suma, o “microcosmo” e o “macrocosmo” se harmonizam.
> (p. 360-1)

# Marketing como empuxo-ao-gozo. Sucesso como valor máximo. 

> Evidentemente, a gestão não é a única a assegurar essa conjunção. **O marketing é empuxo-ao-gozo [_pousse-à-jouir_] incessante e onipresente, ainda mais eficaz na medida em que promete, pela simples posse dos signos e dos objetos do “sucesso”, o impossível gozo último.** Uma imensa literatura de revistas, uma enxurrada de programas de televisão, um teatro político e mediático non stop e um imenso discurso publicitário e propagandista exibem incessantemente **o “sucesso” como valor supremo, sejam quais forem os meios para consegui-lo.** Esse “sucesso” como espetáculo vale por si mesmo. O que ele atesta é apenas uma vontade de ser bem-sucedido, apesar dos fracassos inevitáveis, e um contentamento por tê-lo conseguido, ao menos por um breve momento da vida. Essa é a própria imagem em que se resume o dispositivo de desempenho/gozo. Desse ângulo, autoridades políticas de um tipo novo, como Silvio Berlusconi ou Nicolas Sarkozy, simbolizam o novo curso subjetivo.
> (p. 361)

# Dispositivo do desempenho produz efeitos patológicos

> Tal sujeito encontra sua verdade no veredito do sucesso, submete-se a um “jogo da verdade” em que prova seu ser e seu valor. O desempenho é, muito precisamente, a verdade tal como o poder gerencial a define. **Esse dispositivo de conjunto produz efeitos patológicos aos quais ninguém escapa completamente.**
> (p. 361)

# "Mundo sem limites" como regime institucional

> O “mundo sem limites” não está ligado a um retorno à “natureza”: ele tem o efeito de um regime institucional particular que vê todo limite como potencialmente já superado. **Longe do modelo de um poder central que comandaria remotamente os sujeitos, o dispositivo de desempenho/gozo distribui-se em mecanismos diversificados de controle, avaliação e incentivo e participa de todas as engrenagens da produção, de todos os modos de consumo, de todas as formas de relações sociais.**
> (p. 362)

# Depressão e dopagem generalizadas

> **O remédio mais propalado para essa “doença da responsabilidade”, essa usura provocada pela escolha permanente, é uma dopagem generalizada.** O medicamento faz as vezes da instituição que não apoia mais, não reconhece mais, não protege mais os indivíduos isolados. Vícios diversos e dependências às mídias visuais são alguns desses estados artificiais. O consumo de mercadorias também faria parte dessa medicação social, como suplemento de instituições debilitadas.
> (p. 367)

# Escolha permanente, desejo ilimitado e dessimbolização

> A relação entre gerações, assim como a relação entre sexos, estruturadas e transformadas em narrativas por uma cultura que distribuía os diferentes lugares, tornaram-se vagas, para dizer o mínimo. Nenhum princípio ético, nenhuma proibição parece resistir à exaltação de uma escolha infinita e ilimitada. Posto em estado de “antigravidade simbólica”, o neossujeito é obrigado a fundamentar-se em si mesmo, em nome da livre escolha, para conduzir-se na vida. **Essa intimação à escolha permanente, essa solicitação de desejos pretensamente ilimitados, faz do sujeito um joguete flutuante: num dia ele é convidado a trocar de carro; no outro, de parceiro; no outro, de identidade; e no outro, de sexo, ao sabor de suas satisfações e suas insatisfações.**
> (p. 368)

# A identidade como produto consumível

> Em outras palavras, a identidade tornou-se um produto consumível. Se, como indicava Lacan, o discurso capitalista consome tudo, e se consome tanto os recursos naturais como o material humano, também consome formas institucionais e simbólicas, como Marx já observava no _Manifesto Comunista_. Não para fazê-las desaparecer, mas para substituí-las por aquelas que lhe copertencem: as empresas e os mercados.
> (p. 368)

# Psicotização pela destruição do simbólico (D&G, Lacan). Norma capitalista como rejeição da castração.

> Para os psicanalistas, o ponto nevrálgico é ainda o do caráter indisponível de uma figura do Outro – o plano simbólico – a fim de desligar o pequeno ser humano do desejo da mãe e fazê-lo ascender ao status de um sujeito da lei e do desejo pela mediação do Nome-do-Pai. Ora, com o enfraquecimento das instâncias religiosas e políticas, não existem mais no social outras referências comuns, a não ser o mercado e suas promessas. **Em muitos aspectos, o discurso capitalista acarretaria uma psicotização de massa pela destruição das formas simbólicas.** Essa era a tese de Gilles Deleuze e Félix Guattari, como lembramos anteriormente. O que é menos sabido, porém, é que essa era a tese também de Lacan. “O que distingue o discurso do capitalista é o seguinte: a _Verwerfung_, a rejeição, a rejeição para fora de todos os campos do simbólico com aquilo que eu disse que isso tem como consequência. Rejeição de quê? Da castração.”
> (p. 369-70)

# Neurótico (inadequação do desejo) substituído pelo perverso (ilusão imaginária do gozo total): tudo é negociável e tudo é possível (apologia a transgressão)

> Para alguns psicanalistas, favorecidos por uma distância de cerca de trinta anos em relação a Lacan, nós entramos num universo em que a decepção típica do neurótico, exposto à inadequação da coisa ao desejo, é substituída por **uma relação perversa com o objeto baseada na ilusão imaginária do gozo total.** Tudo se equivale, tem preço e se negocia. Mas, se tudo parece possível, tudo é duvidoso, tudo é suspeito, porque nada é lei para ninguém. **O fato de que tudo é transformado em negócio ou a propensão à apologia constante da transgressão como nova norma seriam alguns dos indícios dessa equivalência geral.**
> (p. 370)

# Neoliberalismo como relação de gozo obrigatório com todo outro (objetalização)

> Na verdade, **a subjetivação neoliberal institui cada vez mais explicitamente uma relação de gozo obrigatório com todo outro indivíduo, uma relação que poderíamos chamar também de relação de objetalização.** Nesse caso, não se trata simplesmente de transformar o outro em coisa – segundo um mecanismo de “reificação” ou “coisificação”, para retomarmos um tema recorrente da Escola de Frankfurt –, **mas de não poder mais conceder ao outro, nem a si mesmo enquanto outro, nada além de seu valor de gozo, isto é, sua capacidade de “render” um plus.** Assim definida, a objetalização apresenta-se sob um triplo registro: os sujeitos, por intermédio das técnicas gerenciais, provam seu ser enquanto “recurso humano” consumido pelas empresas para a produção de lucro; submetidos à norma do desempenho, tomam uns aos outros, na diversidade de suas relações, por objetos que devem ser possuídos, moldados e transformados para melhor alcançar sua própria satisfação; alvo das técnicas de marketing, os sujeitos buscam no consumo das mercadorias um gozo último que se afasta enquanto eles se esfalfam para alcançá-lo.
> (p. 371)

# Perversão como norma das relações sociais (economia psíquica organizada pelo recalque -> economia organizada pela exibição do gozo)

> **A perversão que se distingue clinicamente pelo consumo de parceiros como objetos que são jogados fora assim que são considerados insuficientes teria se tornado a nova norma das relações sociais.** Dessa forma, o imperativo categórico do desempenho concilia-se com as fantasias de onipotência, com a ilusão socialmente difundida de um gozo total e sem limite. Segundo Melman, **passaríamos, assim, de uma economia psíquica organizada pelo recalque para uma “economia organizada pela exibição do gozo”.**
> (p. 371-2)

# Gozo-de-si e o papel da instituição como limitadora dessa aspiração à plenitude impossível

> A psicanálise pode nos ajudar a refletir sobre a maneira como funcionam os neossujeitos de acordo com o regime do _gozo de si_. Segundo Lacan, esse gozo de si, entendido como aspiração à plenitude impossível – nesse sentido, muito diferente do simples prazer –, apresenta-se na ordem social como sempre limitado e parcial. **A instituição é, de certo modo, aquilo que tem a responsabilidade de limitar o gozo e dar sentido a esse limite.**
> (p. 372)

# A empresa limita o gozo (de modo denegado) pela coerção do trabalho, da disciplina, da hierarquia

> A empresa, forma geral da instituição humana nas sociedades capitalistas ocidentais, não foge a essa regra, salvo por fazer isso hoje de maneira _denegada_. **Ela limita o gozo de si pela coerção do trabalho, da disciplina, da hierarquia, por todas as renúncias que fazem parte de certa ascese laboriosa.** A perda de gozo não é menos pronunciada do que nas sociedades religiosas, mas é _diferente_. Os sacrifícios não são mais administrados e justificados por uma lei dada como inerente à condição humana, sob suas diferentes variedades locais e históricas, mas pela reivindicação de uma decisão individual “que não deve nada a ninguém”.
> (p. 372)

# Ultrassubjetivação como gozo de si imaginário e denegação do limite

> Todo um discurso social de valorização exagerada do indivíduo autoconstruído, funcionando como uma denegação, torna possível tal pretensão subjetiva: a perda não é realmente uma perda, uma vez que é decidida pelo próprio sujeito. Mas esse mito social, cujos efeitos sobre a educação familiar e escolar não podem mais ser negligenciados, é apenas um dos aspectos do funcionamento do neossujeito. Ele tem de concordar em entregar-se ao trabalho, em curvar-se às exigências mundanas da vida. Se é exigido dele que o faça, é enquanto empresa de si mesmo, de modo que o eu pode apoiar-se num gozo _imaginário_ pleno num mundo completo. Cada um de nós é mestre ou, ao menos, acredita que pode sê-lo. **Desse modo, gozo de si na ordem do imaginário e denegação do limite aparecem como lei da ultrassubjetivação.**
> (p. 372)

# Denegação da perda -> ilimitação do gozo no imaginário (ilimitação da acumulação mercantil)

> Hoje é diferente. **Se a perda é denegada, a ilimitação do gozo pode ser mobilizada no plano imaginário a serviço da empresa, pega ela mesma em lógicas imaginárias de expansão infinita, de valorização sem limites na bolsa.** Para isso, é claro, é necessário passar por uma racionalização técnica da subjetividade, mas será sempre para que ela “se realize”. O trabalho não é castigo, é gozo de si por intermédio do desempenho que se deve ter. Não há perda, porque é imediatamente “para si” que o indivíduo trabalha. **Portanto, o objeto da denegação é o caráter heteronômico da ultrassubjetivação, isto é, o fato de que a ilimitação do gozo no além de si seja alinhada à ilimitação da acumulação mercantil.**
> (p. 373)

# Denegação da perda como ganho universal no novo capitalismo

> Enquanto no velho capitalismo todo mundo perdia alguma coisa (o capitalista perdia o gozo garantido de seus bens pelo risco assumido, e o proletário, a livre disposição de seu tempo e força), no novo capitalismo ninguém perde, todos ganham. O sujeito neoliberal não pode perder, porque é a um só tempo o trabalhador que acumula capital e o acionista que desfruta dele. Ser seu próprio trabalhador e seu próprio acionista, ter um desempenho sem limites e gozar sem obstáculos os frutos de sua acumulação, esse é o imaginário da condição neossubjetiva.
> (p. 373)

# Ilimitação do gozo de si no imaginário como o oposto de dessimbolização. Sentimento de si no excesso.

> O essencial aqui é compreender que a ilimitação do gozo de si é, na ordem do imaginário, o exato oposto da dessimbolização. **O sentimento de si é dado no excesso, na rapidez, na sensação bruta proporcionada pela agitação, o que certamente expõe o neossujeito à depressão e à dependência,** mas também possibilita aquele estado “conexionista” do qual ele tira, na falta de um vínculo legítimo com uma instância outra, um apoio frágil e uma eficácia esperada. **O diagnóstico clínico da subjetividade neoliberal nunca deve perder de vista que o “patológico” é parte da mesma normatividade que o “normal”.**
> (p. 373-4)

# Vigilância, rastrabilidade, avaliação e autocontrole como características negativas paralelas ao gozo ilimitado

> Portanto, ver na situação presente das sociedades apenas o gozo sem obstáculos, que é identificado ora com a “interiorização dos valores de mercado”, ora com a “expansão ilimitada da democracia”, é esquecer a face sombria da normatividade neoliberal: **a vigilância cada vez mais densa do espaço público e privado, a rastreabilidade cada vez mais precisa dos movimentos dos indivíduos na internet, a avaliação cada vez mais minuciosa e mesquinha da atividade dos indivíduos, a ação cada vez mais pregnante dos sistemas conjuntos de informação e publicidade e, talvez sobretudo, as formas cada vez mais insidiosas de autocontrole dos próprios sujeitos.** Em resumo, é esquecer o caráter de conjunto do governo dos neossujeitos que articula, pela diversidade de seus vetores, a exposição obscena do gozo, a injunção empresarial do desempenho e da reticulação da vigilância generalizada.
> (p. 374)

> É melhor tentar compreender como todas essas instituições, valores e atividades são hoje incorporados e transformados no dispositivo de desempenho/gozo, em nome de sua necessária “modernização”; é melhor examinar de perto todas as tecnologias de controle e vigilância de indivíduos e populações, sua medicalização, o fichar, o registro de seus comportamentos, inclusive os mais precoces; é melhor analisar como disciplinas médicas e psicológicas se articulam com o discurso econômico e com o discurso sobre segurança pública para reforçar os instrumentos da gestão social. Porque, **do dispositivo de governo dos neossujeitos, nada ainda foi definitivamente estabelecido.** Os impulsos são diversos, não faltam ciências candidatas e suas fusões estão em curso ou se farão no futuro.
> (p. 375)

# Gestão social do desempenho como forma de programar sujeitos para o gozo/desempenho ilimitado sem que acarrete numa explosão

> A questão central que se coloca ao governo dos indivíduos é saber **como programar os indivíduos o quanto antes para que essa injunção à superação ilimitada de si mesmo não descambe em comportamentos excessivamente violentos e explicitamente delituosos; é saber como manter uma “ordem pública” quando é preciso incitar os indivíduos ao gozo, evitando ao mesmo tempo a explosão da desmedida.** A “gestão social do desempenho” corresponde precisamente a esse imperativo governamental.
> (p. 375-6)

## Surgimento da neuroeconomia

> [106] Depois do desenvolvimento da “sociobiologia”, o surgimento de uma “neuroeconomia” não deve ser ignorado. Não há dúvida de que **a fusão da biologia do cérebro com a microeconomia oferece perspectivas interessantes de controle do comportamento.**
> (p. 375)
