---
title: Colonialismo digital, racismo e acumulação primitiva de dados
tags:
    - Deivison Faustino
    - Walter Lippold
    - colonialismo digital
    - colonialismo de dados
    - colonialismo
    - big data
    - tecnologias da informação
    - racismo
    - racismo digital
    - racismo algorítmico
    - acumulação primitiva
    - fichamento

date: 2023-10-09
category: fichamento
path: "/colonialismo-digital-racismo-acumulacao-primitiva-de-dados"
featuredImage: "germinal.png"
srcInfo: <a href="https://periodicos.ufba.br/index.php/revistagerminal/article/view/49760">Revista Germinal</a>
published: true
---

Fichamento do artigo _Colonialismo digital, racismo e acumulação primiriva de dados_[^1] de Walter Lippold e Deivison Faustino.

[^1]: LIPPOLD, W.; FAUSTINO, D. Colonialismo digital, racismo e acumulação primitiva de dados. **Germinal: marxismo e educação em debate**, v. 14, n. 2, p. 56–78, 7 out. 2022. 

# Abstract

> **O colonialismo digital não é uma nova fase, mas um dos traços objetivos do atual estágio de desenvolvimento do modo de produção capitalista** e representa um largo passo em direção à uma reificação, cada vez mais profunda, da nossa experiência e senso de realidade, elevando a um novo patamar, a objetificação e mercantilização das relações, das mais simples às mais complexas. **Refere-se, em primeiro lugar, à uma nova partilha do mundo que atualiza o imperialismo e o subimperialismo, ao reduzir o chamado Sul global a mero território de mineração extrativista de dados informacionais ou a consumidores retardatários de tecnologia. No entanto, atualiza e viabiliza novas formas de exploração, opressão e controle político, ideológico e subjetivo, a partir de um fenômeno aqui nomeado como acumulação primitiva de dados.** Ambos os processos, marcados por um assombroso desenvolvimento tecnológico a serviço da apropriação e exploração do valor, **redefinem e ao mesmo tempo, são possibilitados por, por novas expressões de racismo e da racialização.** Assim, **problematizamos a relação entre o colonialismo digital e o chamado racismo algorítmico a partir da apresentação de categorias como acumulação primitiva de dados, _fardo do nerd branco_, _racialização digital_.**

# Perguntas do artigo: implicações das novas tecnologias e relação com colonialismo e racismo

> As  perguntas  que  se  perseguem  no  presente  artigo  são:  **quais  as  implicações  do  incremento tecnológico às formas clássicas de exploração capitalista do trabalho e, sobretudo,qual é a relação desses processos  econômicos  e  tecnológicos  com  o  colonialismo  e  com  o  racismo?**  Como  já  descrevemos  em outro lugar (Faustino e Lippold, 2022), o colonialismo digital é um dos traços objetivos do atual estágio de desenvolvimento  do  modo  de produção  capitalista  e  representa  um  largo  passo  em  direção  à  uma reificação,  cada  vez  mais  profunda,  da  nossa  experiência  e  senso  de  realidade,  elevando  a  um  novo patamar, a objetificação e mercantilização das relações, das mais simples às mais complexas. **Refere-se, em primeiro lugar, à uma nova partilha do mundo que atualiza o imperialismo e o subimperialismo, ao reduzir o  chamado  Sul  global  a  mero  território  de  mineração  extrativista  de  dados  informacionais  ou  a consumidores retardatários de tecnologia.**
> (p. 58)

# Acumulação primitiva de dados

> No  entanto,  **atualiza  e  viabiliza  novas  formas  de  exploração,  opressão  e  controle  político, ideológico  e  subjetivo,  a  partir  de  um  fenômeno  aqui  nomeado  como  acumulação  primitiva  de  dados.** Ambos   os   processos,   marcados   por   um   assombroso   desenvolvimento   tecnológico   a   serviço   da apropriação  e  exploração  do  valor,  redefinem  e  ao  mesmo  tempo,  são  possibilitados  por,  por  novas expressões  de  racismo  e  da  racialização.
> (p. 58)

# Tendências do colonialismo digital: neocolonialismo e colonialismo de dados

> **Sua  existência  se  materializa  a  partir  de  duas  tendências:  A primeira é a emergência de uma nova partilha territorial do globo terrestre entre os grandes monopólios da indústria da informação: as chamadas Big Techs, majoritariamente concentradas no Vale do Silício, mas não apenas.** Partilha essa que atualiza o imperialismo, o subimperialismo e neocolonialismo tardio ao reduzir o chamado Sul global a mero território de mineração extrativista de dados informacionais.
> (p. 58)

> **A segunda tendência, também nomeada como _colonialismo de dados_, é aquela que subsume cada vez mais a vida humana, o ócio, a criatividade, a cognição e os processos  produtivos às lógicas extrativistas, automatizadas e panópticas do _colonialismo digital_**. Não se trata, aqui, de uma simples alteração dos ritmos de vida ou mesmo da percepção humana pela introdução de novas tecnologias, como poderia se presumir, mas, sim, da **manipulação intencional da cognição humana por grandes corporações empresariais a partir dessas tecnologias com vistas à ampliação da acumulação de capitais.** Como veremos, ambas as expressões de  colonialismo,  não  se  materializam  sem  se  apropriar  e  ressignificar  o  racismo  moderno  em  termos digitais.  Assim, problematizamos a relação entre o colonialismo digital e o chamado racismo algorítmico a partir  da  apresentação  de  categorias  como  acumulação  primitiva  de  dados,  fardo  do  nerdbranco, racialização digital. 
> (p. 58)

# Colonialismo digital como traço do capitalismo contemporâneo em Kwet


> **O colonialismo digital é um dos traços objetivos do atual estágio de desenvolvimento do modo de produção capitalista. Para o sociólogo sul-africano Michael Kwet, se caracteriza pelo uso da tecnologia digital  para  a  dominação  política,  econômica  e  social  de  outra  nação  ou  território.**  Se  o  colonialismo clássico  era  baseado  na  ocupação  de  terras  estrangeiras  para  a  instalação  de  infraestruturas  (militares, transporte, comunicaçãoe administrativa) apropriação e expropriação de recursos, controle do território e da  infraestrutura,  extração  violenta  de  trabalho,  conhecimento  e  mercadorias  e  do  exercício  do  poder estatal para viabilizar a pilhagem de um determinado território, hoje:
> 
> [...] as “veias abertas” do Sul Global de Eduardo Galeano são as “veias digitais” que cruzam   os   oceanos,   conectando   um   ecossistema   tecnológico   de   propriedade   e controlado por um punhado de corporações baseadas principalmente nos EUA. Alguns dos  cabos  de  fibra  óptica  transoceânicos  são  equipados  com  fios  de  propriedade  ou alugados  por  empresas  como  Google  e  Facebook  para  promover  sua  extração  e monopolização de dados. O maquinário pesado de hoje são os farms de servidores em nuvem  dominados  pela  Amazon  e  pela  Microsoft  que  são  usados para  armazenar, agrupar  e  processar  big  data,  proliferando  como  bases  militares  para  o  império  dos EUA (KWET, 2021)
> 
> (p. 58-9)

# Big techs como elemento central da (re)produção capitalista

> As chamadas Big Techs–grandes corporações do ramo  da tecnologia digital –representam um elo fundamental desteestágio de acumulação capitalista ao controlarem amplos setores  econômicos e se apresentarem, cada vez mais, como elemento central à produção e reprodução capitalista. Não é possível ignorar,  no  entanto,  o  papel  decisivo  oferecido  pelas  pequenas  startups  AdTechs  (Adverstising.  + Technology),  MarcTechs  (Marketing  + Technology)  e  FinTechs  +  Technology)  no  setor.  Embora  essas empresas  girem  em  torno  das  Big  techs,  elas  têm  autonomia  maior  de  funcionamento  e  liberdade  para criatividade, inclusive, para burlar legislações locais.As corporações do Vale do Silício juntas valem mais de 10 trilhões de dólares. Das 10 empresas mais valiosas do mundo, somente duas (Aramco, Hathaway) não  atuam  diretamente  ou  indiretamente  no  ramo  da  indústria  digital.  Só  as  chamadas _Big  Five_ (Apple, Amazon,  Alphabet,  Microsoft  e  Facebook) [[ GAFAM, FAAMG ]] somaram  quase  900  bilhões  em  receita  em  2019.Este faturamento cresceu 25% em relação ao período anterior à pandemia (SHIMABUKURO, 2021).
> (p. 59)

# Hipertrofia das atividades logísticas e fabricalização da cidade [na indústria 3.0]

> O  que  Ferrari  constatou  é  que  **as  atividades  logísticas  se  hipertrofiaram  devido  às  "novas" necessidades de produção e circulação capitalista. O _just in time_ foi a máxima deste processo que _fabricalizou_ as cidades ao transformar suas vias em grandes esteiras produtivas de forma a viabilizar a apropriação do valor em uma nova escala.** Para que o Mercado Livre ou a Amazon, por exemplo, possam entregar suas mercadorias  em  um  curto  tempo,  estasmercadorias  precisam,  em  primeiro  lugar,  serem  produzidas fisicamente –pelos já conhecidos processos de exploração de trabalho –, mas, em segundo lugar, precisam estar “disponíveis”, em tempo e quantidade, o mais próximo possível dos potenciais compradores.
> (p. 60)

# Uberização do trabalho como intensificação da fabricalização

> Por outro lado, **a _uberização do trabalho_, por exemplo, intensificou os efeitos da fabricalização à patamares inéditos ao permitir a disponibilidade _just in time_ de outra mercadoria fundamental à valorização do  valor:  a  força  de  trabalho.**  Óbvio  também,  como  destaca  Ferrari  (2008),  que  o  desenvolvimento tecnológico informacional permitiu ao capital a ampliação astronômica do grau de previsibilidade do que, quando, ondee por  quemdeterminado  produto  será  comprado.  Mas  as  possibilidades  de  exploração que  se abriamcom essas inovações tecnológicas não pararam aí.
> (p. 60-1)

# Intensificação e redefinição da luta de classes com as tecnologias digitais e a Indústria 4.0

> O posterior desenvolvimento das tecnologias digitais no interior da assim chamada Indústria 4.0redefiniu,  novamente,  a  luta  de  classes,  ao  complexificar  qualitativamente –sem  contudo,  superar -os processos  de  dominação  econômica,  política,  social  e  racial  de  determinados  ou  territórios,  grupos  ou países. Tomemos como exemplo a principal fonte de receita da Amazon.
> (p. 61)

# Amazon como exemplo

> Fundada  em  1994  como  empresa  de  varejo,  foi  se  convertendo –especialmente  a  partir  do lançamento do Amazon Web Services –AWS –em 2004 –em uma plataforma mundial de computação em  nuvem  sob  demanda  entrelaçado  a  um  ecossistema  de  vendedores,  desenvolvedores,  empresas  e criadores  de  conteúdo. **Atualmente,  o  segmento  que  mais  contribui  para  os lucros  da  empresa  não  é  o varejo  físico  ou  online,  mas  sim  o  AWS,  o  segmento  que  mais  cresce  em  sua  receita  líquida,  com  uma participação de 12,5 % (o que equivale a US $ 35,0 bilhões).** Ao  mesmo  tempo, o seu lucro operacional contribui com mais de US $ 9,2bilhões, ou seja, 63,3% da receita operacional total da Amazon (2019)é obtida pela fusão e, ao mesmo  tempo, elevação à outro patamar da relação entre produção, circulação e consumo, extrapolando a meradisponibilidade logística de seus produtos físicos ou digitais
> (p. 61)

# Aproximação entre corporações (Tesla, Elon Musk) e autoritarismo de direita (Bolívia, Amazônia).

> Do outro lado do ringue monopolista, Elon Musk, dono da Tesla, empresa que viu seu valor de mercado  avançar  mais  de  300%  durante  a  pandemia,  respondeu  em  seu  twitter  à  uma  crítica  que  o associava  ao  golpe  orquestrado  pelos  EUA  contra  o  presidente  boliviano  Evo  Morales  com  a  seguinte frase: "vamos dar golpe em quem quisermos! Lide com  isso". A Bolívia é detentora da maior reserva de lítio do mundo, matéria prima fundamental para a produção de baterias da Tesla.Musk continua sua senda de apoiar golpes e regimes autoritários de extrema-direita, que perseguem, torturam e exterminam povos indígenas, militantes, ativistas ambientais, como no caso brasileiro, onde recebeu a Medalha de Honra ao Mérito da Defesa. Musk reuniu-se com o presidente da República para tratar da proteção da Amazônia e de  proporcionar  conexões  e  acessos  via  seu  sistema  starlink  de  satélites,  já  usado  em  prol  do  regime ucraniano.
> (p. 61)

# Intensificação da exploração e valorização via capital financeiro com a emergência das Big Techs

> Ao  que  tudo  indica,  a  recente  eclosão  das  Big  Techs  apresenta  novidades  a  esse  processo  de exploração e apropriação do valor [[ teorizado por Lênin em relação ao Imperialismo ]]: **o enlace entre o mundo da tecnologia e o mundo das finanças -o que não é, em si, uma novidade desde o início do capitalismo –ganha novas expressões a partir de uma dada articulação  entre  datificação,  financeirização  e  neoliberalismo  que  elevam  as  Big  Techsa  condição  deespinhas dorsais (_Backbones_) das inovações financeiras constituindo uma espécie de "teia de aranha digital-financeira"** (MORAES, 2021).
> (p. 62)

> Como explica o engenheiro Roberto de Moraes (2021):
> 
> > a Microsoft, que está próximo de passar a Apple na liderança de valor de mercado entre as Big Techs, rumo aos US$ 3 trilhões, divulga que a maioria das grandes empresas do ocidente,  de  váriossetores  da  economia,  dependem  do  seu  “Workspace”,  para continuar  operando,  existindo  e  capturando  valor  da  economia  real.  É  aí  que  as  Big Techs encontram ponto de tangência para se imbricar à economia real no e-commerce, indústria 4.0, indústria das informações e mídia e também nos bancos digitais-fintechs, moedas  digitais,  tokenização  (divisão  de  propriedades  com  uso  de  metadados  e registros no blockchain), etc
> 
> (p. 62)

# Colonialismo digital não como regressão feudal, mas como desenvolvimento ampliado capitalista

> Mas  as  inovações  não  se  encerram  aí.  Embora  o  desenvolvimento  das  forças  produtivas representem, efetivamente, a ampliação das capacidades humanas em seu trajeto não linear e complexo de afastamento das capacidades naturais, a inversão produtivaoperada pela necessidade valorização do valor faz,  no  atual  estágio  de  acumulação  capitalista,com  que  as  forças  produtivas  se  apresentem,  cada  vez mais,  como  aquilo  que  Mészáros  (1989)  descreveu  como  forças  destrutivas,  ampliando  ainda  mais  a submissão  da  vida  à  um  tempo  estranhado  e  cada  vez  mais  violento  sob  o  qual  parece  perdermos  o controle (POSTONE,  2014).É  verdade  que,  neste  contexto,  o  Vale  do  Silício  se  apresenta  como  a representação encantada e mitológica de um suposto "novo capitalismo" (Durand, 2020:30), mas não se observa,  portanto,  regressão  tecnificada  à  formas  feudais  de  dominaçãomas,  sim,  intensificação  do controle  e  da  predação  monopolista  própria  ao  capitalismo,  mesmo  em  seu  período  industrial.  A concorrência,  a  democracia  e  o  bem-estar  social  capitalista  sempre  conviveram  com  violência    e  o genocídio colonial, longe dos olhos atentos de importantes críticos ocidentais (Faustino e Lippold, 2022). O  colonialismo  digital,  no  entanto,  ocorrem  em  um  estágio  de  desenvolvimento  e -ao  mesmo  tempo) destruição  capitalista  em  que  essa  violência  passa  a  ser  percebida  também  nos  centros  capitalistas, curiosamente, como degeneração do capitalismo ou regressão feudal, quando é, na verdade, expressão de seu desenvolvimento ampliado.
> (p. 62)

# Acoplamento entre corporações e Estados nacionais

> Assim como  no velho imperialismo, não era possível aosgrandes conglomerados renunciarem aos  Estados  Nacionais  e,  sobretudo,  de  seu  poder  de  soberania  e  tirania  geopolítica.  Este  traço,  não apenas  permanece,  mas  é  agravado  por  novas  tecnologias  de  espionagem,  golpes  de  Estado,  controle social e morte. **Mas há aqui uma tendência à privatização de algumas dessas funções que passam a ocorrer em paralelo ou até em disputa com os aparelhos estatais.** Não se trata, como previu Antônio Negri em seu _Império_ (2001),de  uma  derrota  do  Estado  capitalista  pelas  grandes  empresas  transnacionais,  mas  de  uma nova  modalidade  de  coabitação  promíscua  entre  eles.    Como  explica  Moraes  (2021),  "o  poder  de monopólio não apenas de emissão de moedas e meios de circulação, mas de registro de fluxos de negócios e de garantia, que antes só o Estado exercia".
> (p. 63)

# Colonialismo como nova partilha (geográfica, de classe e racial)

> Como  veremos  mais  à  frente,  **esta nova  partilha  mantém,    intensifica  ou  atualiza  as  "velhas"  tendências  da  divisão  racial  e  geográfica  do  trabalho,  não  apenas  por  concentrar  o  poder  no  chamado "Norte  Global"  mas,  sobretudo,  por  reproduzir  uma  racialização  do  acesso  e  da  exclusão  dessas tecnologias.** Se o desenvolvimento das Tecnologias da Informação permitiu a milhões de alunos assistirem suas aulas em casa, em segurança, durante a pandemia, dados do observatório _Brasil Digital_ revelam que 4,1 milhões  de  estudantes  não  conseguiram  participar  das aulas  virtuais  por  falta  de  infraestrutura adequada em um país como o Brasil. **O estudo ainda evidencia uma intensa desigualdade regional, de classe e racial de  acesso  entre  os  estudantes  que  conseguiram  assistir  às  aulas.  Esta  desigualdade, explicitamente racializada,  será  continuamente  explorada  como  possibilidade  de  maximizar  a  exploração  de  valor  ou mesmo a expropriação (_acumulação primitiva_) de dados.** Vejamos como isso se dá, concretamente.
> (p. 63)

# Colonialismo de dados como subsunção da vida cotidiana e processos cognitivos ao universo digital (via extração de dados)

> Trata-se de uma verdadeira _acumulação primitiva digital_. **Uma tendência à colonização, ou melhor, subsunção,  da  vida  cotidiana  e  de  seus  processos  cognitivos  ao  universo  digital.** Um  largo  passo, aparentemente sem volta, em direção à uma ciborguização cada vez mais profunda danossa experiência e senso de realidade de forma que **a objetificação e mercantilização das relações, das mais simples às mais complexas.** Esse violento  extrativismo,  no  entanto,  não  é  um  mero  discurso  de  poder,  mas  o  reflexo  de uma disputa pelas novas matérias primas indispensáveis à ampliação e expropriação  das frações de mais valor: os dados.
> (p. 63-4)

# Sobre valor dos dados e o duplo aspecto que ele inaugura: obtenção, controle e processamento de dados e neocolonislismo sobre os big data

> Independentemente das posições que se assumam no debate [[ sobre os dados serem ou não uma _commodity_ como o petróleo ]], há um consenso em relação ao seu valor elevado, quando comparado ao velho e valioso “ouro negro". O ponto que se quer destacar aqui é que  esse  novo  ativo  tem  movimentado  os  setores  mais  dinâmicosdo  capital,  mas  as  disputas  por  sua extração seguem os antigos padrões coloniais monopolistas. **Trata-se, de um lado, de novas disputas pela obtenção, controle e análise de dados, coletados com ou sem o consentimento de seus produtores  pelas grandes corporações e do outro lado, da velha disputa neocolonial pelos recursos materiais necessários à produção e reprodução da big data.**
> (p. 64)

# Novo extrativismo ~ neocolonialismo tardio e divisão de trabalho

> A grande questão que não se pode perder de vista é que **esse novo extrativismo não dilui, mas intensifica e é intensificado pelos efeitos do _neocolonialismo tardio_ (Yeros e Jah, 2019), ampliando ainda mais os  antigos  fossos  criados  pela  divisão  internacional  do  trabalho.**  Como  afirma  a  jurista  guatemalteca Renata  “tecnologias  de  informação  e  comunicação  (TIC),  a  inovação  em  inteligência  artificial  e  a capacidade   de   implantar   sistemas   e   infraestrutura   rapidamente   em   mercados   emergentes   estão concentradas em apenas alguns países, que agora estão em uma corrida para ser o número um” (PINTO, 2018, p.16).
> (p. 64)

> A jurista argumenta que esse grandecapital − muitas vezes marcado pela fusão do setor público e  privado  em _joint ventures_ com  vistas à  dominação  global –se  configura  pela  grande  concentração  (mais uma  vez,  monopolista)  de  alguns  elementos  ausentes  nas  economias  em  desenvolvimento  como:
> 
> 1.  os recursos  de  capital  (propriedade  e  controle  de  cabos  e  servidores  e  dados)  e  os  recursos  intelectuais  (os técnicos  e  instituições  de  pesquisa  mais  avançados);
> 2. Uma  arquitetura  jurídica  nacional  e  internacional que  limita  a  capacidade  de  inovação  dos  países  em  desenvolvimento  (como  o  sistema  de  patentes  e direitos  autorais,  por  exemplo);  e
> 3.  A  disponibilidade  de  capital  financeiro  para  projetar  e  investir  em pesquisa pesada de desenvolvimento ou, sobretudo, explorar as formas inovadoras que emergem,nestes contextos.
> 
> (p. 64)

> Em 2013, Schmidt e Cohen declararam [[ em relação ao _lobby_ estadunidense ]]: “O que a Lockheed Martin foi para o século XX, as empresas de tecnologia e cibersegurança serão para o século XXI” (ASSANGE, 2015, p. 40). A outra faceta  do  colonialismo  digital  é  a  sua  incontornável  materialidade.  A  divisão  de  trabalho  própria  do neocolonialismo tardio impõe drásticos limites geográficos até para as relações de produção. **A democracia e  o  bem  estar  social,  tão  importantes  à  reprodução  capitalista  nas  metrópoles,  nunca  foram  viáveis  nas colônias,  territórios  rasgados  pela  violência  em  estado  bruto  e  a  racialização.  Essa  dimensão  também  se agrava com o advento do colonialismo digital.**
> (p. 67)

# Fibra ótica (neocolonialismo) ~ ferrovias (colonialismo)

> É  possível  comparar  a  distribuição  mundial  de  fibra  óptica  com  a  expansão  imperialista  das linhas ferroviárias, no século XIX. Nos dois casos a exportação de capitais que viabiliza tal monta só foi possível mediante a partilha colonial do mundo de forma a inserir de maneira subordinada os territórios colonizados ou recém independentes na economia mundial. **Não se tratou de uma transferência horizontal de  tecnologia –desenvolvida, inclusive a  partir  das  matérias  primas  e  do  trabalho  excedente  extraídos  e apropriados desses territórios –mas de uma expansão da malha de comunicação que permitiu converter os povos o resto do mundoem fornecedores de matérias primas bruta e, ao mesmo tempo, consumidores de bens manufaturados.**
> (p. 64-5)

# Criptomoedas, computação quântica ~ extração de recursos naturais

> A  suposta  "era  informacional"  segue,  na  verdade,  pautada  pelos  velhos  limites  materiais  de produção. A límpida e supostamente imaterial mineração de criptomoedas, embora dispense a impressão de cédulas e a sua valorização crescentemente especulativa (D-D') o seu lastro é garantido pelo fato de que o  descobrimento  de  novos  blocos  depende  do  cálculo  realizado  por  supercomputadores  milionários criados  especialmente  para  esse  fim  e  reunidos  em  fazendas  de  mineração  que  consomem  mais  energia elétrica que alguns países europeus.  Ao lado -e como condição de existência -da corrida entre a IBM e a  Google  pela  chamada  supremacia  quântica  por  uma  tecnologia  computacional  que  supere  a  lógica binária dos supercomputadores atualmente existentes, existe  uma demanda crescente por minérios como o coltan, formado por columbita -de onde se extrai nióbio -e tantalita, pois são base para condensadores eletrônicos e supercondutores.
> (p. 65)

# Evangelismo tecnológico na África

> É professado um verdadeiro _evangelismo tecnológico_, é em conferências como o CyFy África, como a ocorrida em Tânger, em 2019, onde o mantra de aceleração, “inovação” e  crença  inexorável  no  uso  de I.A.s,  são promovidos por governos, empresas e mídia. **África, um continente rico em dados, dados que estariam  ao  livre  dispor  do  colonialismo  digital,  assim  como  no  passado  se  considerou  a  terra  e  os minérios, além da força detrabalho africana.**
> (p. 65)

# Disputas sobre o 5G/IoT no Brasil (EUA e China como colonizadores digitais)

> Outro  trágico  exemplo  é  a  política  do  Governo  Brasileiro  diante  da  "guerra  comercial"  entre EUA e China em torno das tecnologias do chamado 5G, em que o Brasil não tem protagonismo algum, exceto na escolha dos novos “colonizadores digitais”. Como afirma Patrícia Maurício et al.:
> 
> > O Brasil tem um imenso mercado consumidor, no entanto, a disputa pela hegemonia da Internet das Coisas (loT) guarda semelhancas com o pacto colonial em que o pais exportava matéria prima e importava produtos manufaturados. Se na época do Brasil Colonia víamos sair do país cana-de-acúcar e metais preciosos, agora o que se fornece são milhões de “nativos” dependentes desses “manufaturados pósmodernas”. EUA e China säo “colonizadores digitais”. Em vez de desbravarem mares turbulentos e desconhecidos com bussolas e astrolabios, os novos colonizadores navegam com aplicativos de ultima geraçäo, que fornecem aos colonizados a oportunidade do consumo e a sensacäo de pertencimento a uma aldeia hiperconectada. Essa aldeia hiperconectada forma também uma Agora Digital, um espaco que pode ser definido como o da vida social, em que säo realizados em várias arenas debates sobre os mais diversos objetos de interesse.
> 
> (p. 66)

# Google e NSA (EUA) em relação ao Brasil

> Assange (2015) alerta para os perigos em termos de espionagem política e industrial por parte do Google, que está totalmente alinhada com os interesses imperialistas estadunidenses. As denúncias feitas por Snowden deveriam ter aberto os olhos dos brasileiros e do mundo sobre a ingerência do imperialismo na  produção  de  petróleo  nacional  e  até  mesmo  no lawfareque  derrubou  Dilma  Rousseff.  Sérgio  Moro obteve  formação  nos  Estados  Unidos  e  acesso  a  informações  oriundas  de  espionagem  via  NSA.  O lavajatismo  destruiu  o  capitalismo  nacional,  e  adequou os  interesses  geopolíticos  aos  ditames  da  política exterior dos EUA, além de tirar Lula da eleição, prendendo o ex-presidente.
> (p. 66)

# Materialidade do colonialismo digital

> Como já foi dito anteriormente, **não  há _softwares_ sem _hardwares_. Falta dizer que também não há hardwaresem  ouro,  lítio,  columbita  e  tantalita,  coltan,  cobalto,  entre  outros.**  Dado  a  importância  da indústria  eletrônica  para  os  modos  de  existir  do  capitalismo  contemporâneo,  é  fácil  concluir  que  a  sua reprodução seria inviável sem o acesso a essas matérias primas. Novamente, observa-se uma das facetas mais  violentas  do  colonialismo  digital  uma  vez  que  aqui,  o  extrativismo  não  evoluiu  desde  as  antigas colônias do século XIX.
> (p. 67)

> Outro  elemento  relacionado  à  materialidade  concreta  do  colonialismo  digital  se  apresenta através  do  controle  monopolista  da  infraestrutura  de  hardware  e  software  de  redes,  data  centers, servidores e controle da força de trabalho, do cognitariado e precariado, que são a carne a ser moída para a acumulação  atual,  programando  e  pedalando,  sendo  colocados  como  biorobôs  que  executam  ordens emitidas por uma voz robotizada, controlada pela I.A. da plataforma. Uma ciborguização alienante, onde o conhecimento evanesce e é proclamado o reino dataísta, o fetiche pelos dados e a morte da narrativa.
> (p. 68)

# Colonialismo digital ~ inovação


> Por  fim,  e  não  menos  importante,  é  a  ligação  do  colonialismo  digital  com  as  políticas  de investimento e inovação. O acesso a _joint venture capitalists_ em _start ups_, em disrupção -“a palavra predileta das elites digitais” (MOROZOV,  2018,  p. 27),  pesquisa, colonização  das  universidades  em  prol  dos ditames das corporações big tech, moldando pesquisadores já na sua formação, “parcerias” entre as empresas e universidades, fundações público-privadas.
> (p. 68-9)

# Colonialismo digital -> colonialismo de dados (fabricalização da cidade e proletarização da vida privada)

> **A já mencionada _fabricalização da cidade_, de que fala Ferrari é, também, como a própria socióloga identificou,  uma  proletarização  da  vida  privada,  em  suas  dimensões  individuais  e,  sobretudo  subjetiva.** Atualmente,  estamos  assistindo  à  uma  uberização  da  vida  cotidiana  através  da  monetização  da  nossa imagem  cotidianamente  capturada  por  aparelhos  cada  vez  mais  presentes  em  todos  os  momentos (Faustino e Lippold, 2022). É, pois, neste ponto que **o colonialismo digital se converte em uma forma de dominação que tem sido nomeada como i-colonialismou colonialismo de dados.** A esse respeito, Achille Mbembe  (2013)  chega  a  afirmar  que  **o  rebaixamento  reificador  que  reduziu  aos  povos  africanos  à condição de _homem moeda_ é, agora, universalizada pelo desenvolvimento tecnológico e vivida por todos os usuários informacionais.**
> (p. 69)

# Luta entre humanos x máquinas -> humanos (detentores) x humanos (não-detentores). [Mas não é também entre máquinas produzidas pela classe dominante e contra-maquinarias da classe dominada?]

> Podemos  supor  que  a  racialização,  como  discurso  que  fixa  os  sujeitos  nas  representações objetificantes  criadas  pelo  colonialismo,  aparece  aqui  como  enclausuramento  de  toda  humanidade  pelas máquinas  que  outrora  criou.  Ocorre,  como  enfatizamos  (Faustino  e  Lippold,  2022),  que  **a  verdadeira  e principal  contradição  não  se  dá  entre  humanos  e  máquinas,  mas  sim,  entre  humanos  (detentores)  e humanos (não-detentores) dos meios e possibilidade de produção da vida. O que não significa que a luta de  classes  não  tenha  no  desenho  e emprego  das  máquinas  uma  dimensão  fundamental  dessa  luta  (de classes).** Veremos, pois, que essa luta também é incontornavelmente racializada
> (p. 69)

# Lacuna na literatura de colonialismo digital/de dados sobre racismo digital.

> **O  silêncio  da  literatura  especializada  em  digital  colonialism,  i-colonialism  ou  data  colonialism, sobre  o  racismo  no  universo  digital  é  ensurdecedor.  Se  o  racismo  foi  e  continua  sendo  a  base  para  as velhas e novas formas de colonialismo, nos perguntamos, como nos foi possível o advento de toda uma literatura  sobre  COLONIALISMO  (digital)  que  não  discute  o  racismo?**  Embora  estejamos  tratando, fundamentalmente, de um arsenal teórico muito mais crítico que esse, a revisão bibliográfica que sustenta esse trabalho nos fez perguntar se esse campo de estudos não é, em sua grande maioria, contaminado pelo fardo  do  nerd  branco.  Frantz  Fanon  nos  lembra  que  o  racismo  não  se  expressa  apenas  sobre  as  ofensas abertamente  violentas  ou  estereotipadas,  mas,  sobretudo,  na  suposta  universalização  dos  referenciais particulares  europeus.  **Uma  espécie  de _identitarismo branco_ permite  ao  pensamento  crítico  se  supor  radical sem, contudo, enfrentar as dimensões raciais da exploração de classe (FAUSTINO, 2021a; 2018).**
> (p. 69)

# Valor/preço como expressão do racismo

> O que procuramos chamar a atenção aqui, é que a tendente universalização da “condição negra” narrada por Mbembe (2013) e muitas vezes mobilizada para problematizar o colonialismo de dados, não substituiu a diferenciação fenotípica promovida pelo racismo antinegro. Em resultado, uma vez que todos tendemos (cada vez mais) a ser reduzidos à mercadoria, encontramos no racismo um elemento ideológico que diferencia o __preço__ de cada mercadoria e, sobretudo, os critérios que definem e autorizam quais delas podem serdescartadas e quais, mesmo quando supérfluas, não são passíveis de tais redução. **Falamos em preço,  ao  invés  de  valor,  porque  o  tempo  de  trabalho  socialmente  necessário  empreendido  por  um trabalhador negro é o mesmo que o de um branco, já o seu preço no mercado de trabalho não.** Mais do que  isso,  a  experiência  colonial  nos  desafia  a  equacionar  a  exploração  capitalista  para  além  da  simples exploração da mais valia, como prevista pela teoria do valor.
> (p. 69-70)

> Denise  Ferreira  Silva  (2017)  retoma  os  cálculos  de  Marx  a respeito  do  valor  do  linho  na revolução industrial para direcionar reflexão **sobre o valor para a exploração escravista, não contabilizada no  cálculo  da  mais  valia.  Esse  quantum  de  valor  obtido  pelo  trabalho  não  pago  e  não  mensurado representa uma parte fundamental da riqueza produzida na modernidade.** Ainda assim, a máxima cantada por Elza Soares não se desatualizou e, em consequência, _a carne mais barata do mercado continua sendo a carne negra_, justamente a que mais contribuiu para o enriquecimento humano genérico a partir de sua exploração em estado bruto. Se há uma colonização digital, ergue-se como prioridade a investigação sobre como e em que medida a racialização se presentifica neste contexto
> (p. 70)

> Essa  investigação  vem  sendo  feita  por  uma  rede  sólida -embora  ainda  pequena -de pesquisadores alocados em diversas partes do mundo. Destacamos, neste sentido, o brilhante trabalho do Professor  Tarcízio  Silva(2019),  Joy  Buolamwini  (2018),  a  Professora  Safiya  Umoja  Noble  (2018),  entre outros.  **Como já foi discutido, algoritmos são produções humanas [[?]] e, portanto, atravessados por tradições, valores subjetivamente e intersubjetivamente partilhados (SILVA, 2019), mas, sobretudo, com finalidades historicamente determinadas.**
> 
> > [...] grupos de cientistas, teóricas e ativistas da comunicação e tecnologia apontaram os processos  pelos  quais  a  construção  tanto  das  tecnologias  digitais  de  comunicação quanto  da  ideologia  do  Vale  do  Silício  são  racializadas,  a  partir  de  uma  lógica  da supremacia branca[...] (SILVA, 2020, p.129)
> 
> (p. 70)

# Tarcízio sobre racismo algorítmico

> Em  uma  palestra  oferecida  ao  NEABI  do  Campus  Avançado  Ubá,  no  ano  de  2021,  o pesquisador Tarcízio Silva falou dos cinco pilares do racismo algorítmico.
> 
> 1. O  primeiro é o que ele chama de _Looping  de  feedback_:  **o  modo  como  sistemas  de  inteligência  artificial  promove  vieses  de  discriminação racial  já  existentes  na  sociedade.**  Cita  como  exemplos  os  sistemas  de  reconhecimento  de  objeto (aprendizado  de  máquinas)  e  imagens  que  tendem  a  incorporar  os  vieses  raciais  e  fazer  associações racializadas.
> 2. O  segundo  pilar  é  o  que  ele  chama  de _humanidade diferencial_:  **o  modo  como  o  racismo  acaba promovendo o grupo hegemônico em detrimento de minorias, consolidando uma espécie de distribuição racial do sistema tecnológico.**
> 3. O  terceiro  pilar,  segundo  argumenta,  é  o  paradoxo  entre  invisibilidade  e  hipervisibilidade. Baseado nos estudos de Joy Buolamwini sobre a _disparidade interseccional_, ele argumenta **o racismo pode se manifestar,  de  um  lado,  no  não  reconhecimento  correto  do  traço  de  mulheres  negras  nos  app  de reconhecimento lúdico oufuncionais, e do outro lado, a hipervisibilidade negra nas formas de dominação e  controle.**  Como  exemplo,  Silva  lembra  que  90,5%  das  pessoas  presas  por  reconhecimento  facial  no Brasil são pessoas negras.
> 4. O quarto pilar é a _colonialidade global no negócio da tecnologia_. Segundo Tarcízio, **grandes empresas de big tech colonizam infra-estruturas tecnológicas em alguns países menos conectados, de forma a restringir o acesso desses povos ao seu monopólio.** Um exemplo famoso é a oferta de internet gratuita e de baixa qualidade  pela  google  e  o  facebook  para  países  com  baixíssima  conexão  como  Gâmbia,  Sri  Lanka,  no entanto,  o  preço  cobrado  é  que  as  pessoas  só  podem  acessar  os  produtos  dessas  mesmas  empresas  ao invés  de  terem  o  acesso  ilimitado  à internet.  Essa  proposta  chegou  a  ser  apresentada  pelo  Facebook  ao Brasil, mas foi rejeitada pela então Presidente Dilma.
> 5. O quinto pilar é o que Silva chama de _colonialidade de campo_. O  pesquisador  observa  **como  as  disciplinas  do  campo  da  informação  tendem  a  negligenciar  a presença  do  racismo  em  seus  objetos  de  estudo  e  formação  de  profissionais,  professores  e  novos pesquisadores.**
> 
> (p. 70-1)

# Questionamento sobre a noção "racismo algorítmico" por esconder agências humanas (confrontar com Latour e Galloway sobre dominação nas agênciase não-humanas)

> Este aspecto é importante para o argumento aqui assumido. **Se os códigos são, mesmo em sua tendente automação, padrões socialmente determinados, o termo “racismo algorítmico” não tenderia  a escamotear a autoria do racismo, transferindo-a para os códigos enquanto oculta os seus programadores, esse sim humanos formados e informados por dadas relações sociais de poder?**
> (p. 71)

# Proposta dos termos "racialização codificada" ou "racialização digital"

> Acreditamos,  portanto,  que  a  noção  de _racialização  codificada_ ou _racialização  digital_ possa ser  mais abrangente  para  dar  conta  da  explicitação  do  contexto  material  de  desenho  dos  algoritmos  de  forma  a evidenciar  a  seletividade  racial  dos  cargos  técnicos  em  empresas  de  programação,  a  distribuição  social desigual de prestígio entre produtores de conteúdo digitais na internet (PROPMARK, 2020) e codificação naturalizada dos discursos e estética racistas nas mídias sociais e bancos de imagem digitais.
> (p. 71)

> Convém  destacar,  ainda,  a  racialização  codificada  em  aplicativos  de  reconhecimento  facial,  ao não identificarem os traços negros com precisão (NOBLE, 2018) e, sobretudo, uma certa eugenia política (SILVEIRA, 2020) presente no “aprendizado de máquinas”. A eugenia se materializa tanto na utilização estética e cultural branco-ocidental como parâmetro de  humanidade quanto na exclusão ou desigualdade do acesso às tecnologias informacionais.
> (p. 71)

# Questionamento sobre os limites do ativismo em plataformas digitais (hegemônicas)

> A pergunta que cabe fazer é: em que medida o ativismo quando restrito às grandes plataformas privadas − fornecidas pelos grandes monopólios informacionais − representa realmente uma subversão da ordem  estabelecida  ou  apenas  mais uma  estratégia  de  ampliação  de  tempo  de  permanência  dos  usuários em  seu  interior,  com  vistas  às  já  anunciadas  finalidades  de  extração  e  venda  de  dados?  Poderiam  as ferramentas do Senhor desmantelar a casa grande? (LORDE, 1984). 
> (p. 72)

# Tensão monopolismo-democratização da tecnologia no capitalismo

> Parece que no capitalismo, toda revolução tecnológica enfrenta esse aparente antagonismo entre privatização monopolista versus democratização. Alguns exemplos são a prensa mecânica criada na China por Bi Sheng, no século XI, depois difundida para Europa por Gutenberg e as tentativas de controle da tecnologia do livro, pela Igreja através do _index librorum prohibitorum_, e pelo capitalismo, com a criação da propriedade  intelectual, _copyright_.  Do  mesmo  modo,  a  tecnologia  da  radiodifusão,  tão  bem  analisada  por Brecht em sua teoria sobre o rádio.
> (p. 72)

# Ideologia californiana do vale do silício

> A   ideologia  que   imperou   no   mundo   digital   foi  aquela   que   emergiu   das   entranhas   das corporações  do  Vale  do  Silício,  que  segundo  Foletto  do  BaixaCultura,  na  Introdução  de  Ideologia Californiana (BARBROOK; CAMERON, 2018, p.5) consiste em
> 
> > [...]  uma  improvável  mescla  das  atitudes  boêmias e  antiautoritárias  da contracultura  da costa  oeste  dos  EUA  com  o  utopismo  tecnológico  e  o  liberalismo  econômico.  Dessa mistura hippie com yuppie nasceria o espírito das empresas .com do Vale do Silício, que passaram a alimentar a ideia de que todos podem ser “hip and rich” –para isso  basta acreditar  em  seu  trabalho  e  ter  fé  que  as  novas  tecnologias  de  informação  vão emancipar o ser humano ampliando a liberdade de cada um e reduzir o poder do estado burocrático.
> 
> (p. 72-3)

> Uma das principais contradições da ideologia californiana é **o culto ao livre mercado, e o anti-estatismo,   sendo   que   grande   parte   dos   investimentos   na   internet   foram   estatais,   via   militares   e universidades.** Além disso, professa a crença ideológica no Robison Crusoé capitalista. **A imagem de um hacker solitário lutando contra o sistema, idealizado pela literatura cyberpunk, não deixa de reproduzir a ideologiado _self  made man_ e  do _do it  yourself_ da  ideologia californiana.**  O  hacktivismo  atual  compreende  a força do agir coletivo, da coletividade (BARBROOK; CAMERON, 2018).
> (p. 73)

> O elemento que uniu nova direita e nova esquerda na costa oeste, é a defesa de uma democracia jeffersoniana,  com  ideias  oriundas  de  um  escravista  e  latifundiário  da  Virgínia,  que  assentou  a  liberdade dos brancos sobre a escravização negra. Para Thomas Jefferson, o negro é um ser humano, mas antes de tudo  é  uma  propriedade,  e  o  direito  sagrado  dapropriedade  não  poderia  ser  violado  (BARBROOK; CAMERON,  2018).    As  contradições  envolvendo  classe  e  raça  na  costa  oeste,  continuaram  a  se manifestar na ideologia californiana, pois a classe virtual foi formada por brancos, que em geral se retiram para  seus  bairros  vigiados  e  segregados  dos  negros  e  hispânicos.  Não  podemos  deixar  de  lembrar  das palavras  de  Fanon,  acerca  da  compartimentação  racializada  do  espaço  colonial,  a  cidade  do  colono  e  a cidade do colonizado.
> (p. 73)

# Dialética mestre-ciborgue escravo-robô

> Cria-se uma dialética do mestre ciborgue edo escravo robô, uma releitura de Hegel poderia nos apoiar  nesse  processo  de  desenvolvimento  contraditório.  Jefferson  em  sua  propriedade  escravista produziu uma série de tecnologias para intermediar seu contato com os escravizados. Os brancos da classe sonham com a tecnoutopia do “pós-humano”, “[...] uma manifestação biotecnológica dos privilégios[...]” (BARBROOK;  CAMERON,  2018)  de  classe.  Não  só  o  desejo  de  otimizar  desempenho  de  modo ciborguiano, mas uma manifestação das bases da ideologia atual veiculada nas redes sociais, que reforçam a  busca  de  auto  satisfação  narcísica,  através  de  terapias  alternativas,  misticismo  e  um  egocentrismo  que engole o outro em um Grande Eu.
> (p. 73-4)

> Por  outro  lado,  a  fantasia  de  criar  o  escravo  perfeito,  o _robota_,  que  em  língua  eslava  significa escravo, trabalho forçado. Assim, como slave, escravo se origina na palavra eslavo, devido a escravização dos povos eslavos, pelo Sacro Império Germânico (I Reich). 
> 
> > Apesar  destas  fantasias,  os  brancos  da  Califórnia  continuam  dependentes  de  seus colegas  humanos  de  pele  mais  escura  para  trabalhar  em  suas  fábricas,  colher  seus cereais,  cuidar  de  suas  crianças  e  cultivar  seus  jardins.  Após  os  tumultos  de  Los Angeles, eles cada vez mais temem que esta “subclasse” vá um dia exigir sua libertação. **Se escravos humanos não são totalmente confiáveis, então escravos mecânicos terão de ser inventados. A busca pelo Santo Graal da “Inteligência Artificial” revela este desejo pelo Golem –um forte e leal escravo cuja pele tem a cor da terra e cujas entranhassão feitas de areia.** Como nas histórias de robôs de Asimov, os tecno-utópicos imaginam ser possível obter mão-de-obra como a escrava por meio de máquinas inanimadas. **Porém, apesar  de  a  tecnologia  poder  armazenar  ou  amplificar  o  trabalho,  ela  não  pode  nunca remover  a  necessidade  de  os  humanos  inventarem,  construírem  e  manterem  estas máquinas  em  primeiro  lugar.**  Trabalho  escravo  não  pode  ser  obtido  sem  escravizar alguém.  Por  todo  o  mundo,  a  Ideologia  Californiana  foi  aceita  como  uma  forma otimista e emancipadora de determinismo tecnológico. Porém, esta fantasia utópica da costa oeste depende de sua cegueira frente à –e dependência de –polarização social e racial da sociedade em que nasceu. (BARBROOK; CAMERON, 2018, p.33)
> 
> (p. 74)

# Fardo do nerd branco

> **O _fardo  do  nerd  branco_ é  uma  reabilitação da ideologia do White Man’s Burden**, plasmada no “poema” do branco inglês Rudyard Kipling, lançado na ocasião da conquista das Filipinas pelos Estados Unidos.  Suas  bases  em  termos  de  ideologia  são:  1.  racismo  pseudocientífico;  2.  destino  manifesto estadunidense, 3. mission civilisatrice europeia. **O  homem branco é alavancado como o universal e telos em-si da civilização, e por ter alçado tal lugar, tem que iluminar as trevas infamantes do colonizado, como afirmou Memmi (1977).** O  colonialismo é um bem supremo  para os bárbaros e selvagens, a Europa diz introduzir a higiene, os hospitais, as estradas, as técnicas e tecnologias. Bondade, brancura, branquidade e branquitude: os colonizados deveriam agradecer as “benesses” da civilização. Nada mais justo que roubar suas terras e minérios, exaurir a força de trabalho  e domesticar a napalm, baionetada e metralhadas suas “criancices”, pois o colonizado sempre é tratado como infantil.
> (p. 74)

> Assim  como  o  racismo  religioso,  baseado  na  leitura  cristão  medieval  da  Maldição  de  Cam,  foi reconfigurado  e  rearticulado  pelo  racismo  pseudocientífico,  o  nerd  branco  reabilita  o  fardo  do  homem branco.   Iniciativas   oriundas   das   big   techs   do   Vale   do   Silício   querem   ajudar   o   sul   do   mundo, principalmente o continente africano:
> 
> > No livro A nova era digital,os senhores Schmidt e Cohen assumem alegremente o fardo do ‘nerd branco’. O texto é cheio de figuras de pele escura convenientes e hipotéticas: pescadores congolenses, designers gráficos de Botsuana, ativistas anticorrupção de San Salvador  ecriadores  de  gado  analfabetos  do  povo  massai  no  Serengeti...  todos obedientemente   convocados   para   demonstrar   as   propriedades   progressistas   dos telefones  do  Google conectados  à  cadeia  de fornecimento  de informações  do  império ocidental[...].  O  livro  é uma  obra  funestamente  seminal, e  nenhum  dos  autores  parece ter  a  capacidade  de  enxergar,  e  muito  menos  de  expressar,  a  titânica  perversidade centralizadora que estão construindo.  (ASSANGE, 2015).
> 
> (p. 74-5)

# Capitalismo > fase neocolonialista tardia > colonialismo digital (acumulação primitiva de dados, racialização digital, fardo do nerd branco)

> O  colonialismo  digital  é  um  fenômeno  inserido  dentro  da  fase  neocolonialista  tardia  (Yeros  e Jha,  2020)  do  modo  de  produção  capitalista.  Para  compreendê-lo  como  totalidade  concreta  fez-se necessário correlacionar os elementos e as propriedades que compõem o fenômeno, ou seja, a acumulação primitiva de dados, a racialização digital e o fardo do nerd branco. Partimos de questionamentos sobre a constante  ausência  de  análises  do  racismo  nos  estudos sobre  colonialismo  digital  e  buscamos  em Fanon aportes críticos sobre  tecnologia, colonialismo e descolonização. A visão de uma potência benevolente  e neutra  que  emana  do  Silicon  Valley  desdobra-se  da  ideologia  californiana:  é  a  sua  versão  imperialista, incluindo, portanto, as contradições de classe e raça na West Coastestadunidense.
> (p. 75)

# Aspecto destrutivo do atual estágio da acumulação capitalista

> As tecnologias informacionais têm expulsado -a uma velocidade exponencial -cada vez mais trabalho  vivo  do  interior  das fábricas  e  que  há  no  atual  estágio  de acumulação  capitalista, uma  tendente conversão das forças produtivas em forças destrutivas. **É sabido que em momentos de crise, a queima de trabalho morto através de  guerras é uma forma de dinamizar a economia. Talvez, a atualidade do presente momento é que a queima de trabalho vivo também passa a ser lucrativa, ainda que pareça antieconômica.** Ela pode ser lucrativa desde que devidamente controlada em territórios malditos, delimitados por grandes interesses imperialistas de acumulação. **O racismo segue tendo uma função econômica bastante atual aqui: distinguir aqueles que podem ser queimados,sem comoção e implicações éticas daqueles cujo a dor será tomada como parâmetro universal.**
> (p. 75-6)
