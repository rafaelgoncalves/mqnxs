---
title: Modulação depois do controle
category: fichamento
date: 2021-12-02
path: "/modulation-after-control"
tags: [Yuk Hui, modulação, fichamento, amplificação, controle, redes sociais, Gilbert Simondon, Gilles Deleuze]
featuredImage: "./social_network_diagram.png"
srcInfo: <a href="https://commons.wikimedia.org/wiki/File:Social_Network_Diagram_(segment).svg">Imagem de DarwinPeacock, Maklaan (retirado de wikimedia)</a>
published: true

---

Fichamento do artigo _Modulation after control_[^1] do filósofo (e engenheiro da computação)
Yuk Hui partindo das noções de sociedade de controle de Deleuze e amplificação moduladora de Simondon.

[^1]: HUI, Y. Modulation after Control. **new formations: a journal of culture/theory/politics**, v. 84, n. 84, p. 74–91, 2015. 

# Sociedades de soberania, disciplinares e de controle

> The passage from
> the society of sovereignty to disciplinary society is characterised by the shift
> from direct commands (e.g. to tax, to rule on death) to a disciplinary mode
> of production (e.g. prisons, factories, or other enclosures of space). In control
> societies, Deleuze proposes, we can observe a new form of operation that is
> no longer about the enclosure of space. To be more precise, it is no longer a
> control that explicitly and directly imposes its violence or force on individuals;
> and nor does it archive their obedience according to its institutional and
> social code, as we can see in the example of prisons. Rather, this new type of
> control is characterised by creating a _space_ for the individual, as if he or she
> has the freedom to tangle and to create, while their production as well their
> ends follow the logic of intangible forces.
> (p. 75)

# Moldagem x modulação

> If we understand the first form of
> control - direct intervention - as moulding [moulage], then this second form
> of control can be understood in terms of modulation. In Deleuze’s own words:
> ‘enclosures are moulds, distinct mouldings, but controls are modulations, like
> an auto-deforming mould that would continuously change from one moment
> to the other’.
> (p. 75)

# Esfera do trabalho (controle por modulação)

> Deleuze’s description of modulation can clearly be understood in terms of
> changing labour conditions: under Taylorism, workers worked according to
> strict codes and followed well-defined instructions in the factory; towards the
> 1980s, a new mode of control began to appear, which the French sociologist
> Philippe zarifian calls a ‘control by modulation’, which ‘gave the worker
> a certain freedom to manage his time, displacement and a good number
> of his actions’.
> (p. 75)

# Analógico x Digital

> Deleuze made another comparison between disciplinary
> society and control societies in terms of the analogical and the numerical.
> Retrospectively, the word numérique, has a double meaning according to the
> common translations of this word today: firstly numerical, as number for
> management; secondly digital, which is closely related to the digital networked
> technologies used for management and surveillance, recently amplified due
> to the Snowden affair.
> (p. 75)

# O conceito de modulação

> The concept of modulation was introduced by Simondon in his principle
> thesis L’individuation à la lumière des notions de forme et d’information (Individuation
> Considered in the Light of the Notions of Form and Information) in order to
> resist the idea of moulding, which has been central to Western ideas of the
> relationship between form and materiality at least since Aristotle.
> (p. 76)

# O hylemorfismo

> Moulding
> is the paradigmatic example of what Simondon calls ‘hylomorphism’.
> hylomorphism is his name for the theory of matter and form first posited
> by Aristotle. This model understands being in terms of form and matter,
> conceived as absolutely distinct categories, from which we can derive the
> essence of any entity’s being: an object of such and such a form [morph] and
> consisting of such and such matter [hyle] is what an entity is.
> (p. 76)

> Simondon
> understands hylomorphism as an obstacle that prevents thinking about the
> nature of becoming; worse still, hylomorphism opposes being and becoming,
> for becoming - a processual condition of ongoing immanent transformation - destroys individuality when the latter is considered only in terms of a
> relationship between form and matter. Concerning the Metaphysics of Aristotle,
> Simondon wrote ‘becoming remains conceived as movement, and movement
> as imperfection’
> (p. 76)

# Ser e devir como complementaridade

> Instead, Simondon proposed, ‘becoming should not be
> opposed to being; it is a constitutive relation of being as individual’ (ibid).
> (p. 76)

# Dialética x disparidade

> If we can say that hylomorphism
> operates dialectically (form+matter=synthesis), then modulation operates in
> terms of _disparation_ [disparity], a word used by Simondon to describe internal
> tensions within any given being.
> (p. 77)

# Modulação como resistência (Deleuze)

> Modulation for Deleuze serves as a form of resistance, not only against
> moulding or cohesive forces, but also against a certain history of philosophy
> (e.g. the Aristotelian - Kantian tradition).
> (p. 77)

# Modulação como operação de poder nas sociedades de controle (Deleuze)

> however, it is interesting to note that in the later works such
> as the ‘Postscript on Control Societies’, the concept of modulation becomes
> the paradigm of capitalistic production, or more precisely the operation of
> power in control societies.
> (p. 77)

# Molde (essência) x modulaçã (devir) (Deleuze)

> In the idea of moulding we encounter an essentialist conception of the
> object conceived in terms of a rigid distinction between form and matter;
> in modulation, the mannerist concept of the object understands becoming
> as an event in which certain immanent properties of matter are expressed.
> (p. 78)

# Deleuze sobre a TV (curso sobre pintura)

> Deleuze derived three results from
> this understanding of modulation: (1) there is a passage from moulding to
> modulation which one should distinguish; (2) all code is in fact a ‘grafting’ of
> code onto an analogue flux, meaning that analogue is the background to the
> digital; (3) the analogue, in its most strict sense or in an aesthetic sense, can
> be precisely defined by modulation.
> (p. 78)

> The discussion on modulation aims to
> redefine painting neither as modelling nor as moulding, but as modulation
> of colour and/or light.
> (p. 78)

| Painting        | Modulation         |
| ---             | ---                |
| Canvas          | Surface/background |
| Light and color | Wave               |
| Space           | Signal             |

# Ser como devir em modulação

> By bringing
> Simondon and leibniz together, Deleuze went beyond their descriptive
> scope, and constructed an ontological understanding, which includes not
> only technical objects (Simondon) or curves (leibniz), but also all kind of
> objects as well as subjects. his discussion of painting further demonstrated the
> profundity of his modulative thinking, which cannot be reduced to a simple
> opposition between moulding and modulation, but rather implies a totally
> new ontological ground for understanding being as modulated becoming.
> later Deleuze deploys this metaphysical framework to understand the nature
> objects as well as subjects.
> (p. 79)

# Efetivamente modulação e moldagem coexistem (hibridos)

> Conceptually one opposes
> modulation and moulding; in reality, modulation and moulding co-exist,
> and consists of a hybrid mode, which Simondon calls _modelage_.
> (p. 79)

# Ontogênese, amplificação e cibernética (Simondon)

> We can understand modulation as a constant becoming according to certain
> measures and constraints. Once being is understood in terms of relations,
> then being can be imagined as an amplification in which different relations
> are modulated according to respective causes and effects. This ontogenetic
> understanding of being opens the way not only for a new metaphysics, but also
> a new politics that proposes new models of organisation based on feedback,
> namely cybernetics.
> (p. 80)

# Amplificação transdutiva (Simondon)

> Transductive amplification could be
> exemplified by the process of crystallisation, in which the propagation
> of information is ‘transductive’, meaning that it is robust and multiple,
> involving a transfer of information from one phase-state to another. As a
> model of information-exchange, transduction has to be distinguished from
> classical logic, which is based on step by step inference of propositions. In the
> crystallisation of a supersaturated solution, for example, once the nuclei are
> formed they release energy that triggers the crystallisation of the surrounding
> solution and propagates until the solution becomes metastable. For Simondon,
> a social metaphor would be the spread of rumours, which does not follow a
> linear propagation, and relies upon the affective potency of the rumours and
> their implications more than any rational cognitive logic.
> (p. 80-81)

# Amplificação moduladora

> The second type of amplification effect, the modulative - named after
> modulation - has a more specific meaning here, and is our central focus.
> Simondon gives the example of a triode to explain the effect of modulative
> amplification. The triode is the basic form of electronic amplifier that powered
> electronic devices prior to the invention of microchips (which are still based on
> its principles). The glass valves which powered mid-twentieth century radios,
> TVs and stereo amplifiers were all based on this model. A triode works by
> adding a positive-feedback control to a diode. In a diode, there is a cathode
> with a negative charge, and another with positive charge. The cathode is
> heated to emit electrons, the positive charge of the anode attracts electrons
> towards it, and a current is produced. The triode puts a grid between the
> anode and cathode: a small potential charge applied to the grid can greatly
> amplify the current. The grid between the anode and the triode effectively
> modulates the current.
> (p. 81)

> With the concept of relay (e.g. the employment of a smaller voltage difference
> (e.g. the grid) to trigger a larger voltage difference (between the anode and
> the cathode), Simondon claims that ‘the model of the triode is the functional
> analogy of a social structure’ (ibid). We can imagine the social group as a
> unity, in which the sub-ensembles have a common polarisation constituted
> by norms, for example moral norms. The polarisation allows the group to be
> amplified by certain determined information or patterns of conduct, just as
> that of the anode and the cathode is effectively modulated by a grid (p169).
> (p. 81-82)

# Amplificação organizadora

> The third model of amplification,
> or amplification at the highest
> level, is organising amplification,
> which is the synthesis of both
> transductive amplification and
> modulative amplification. Th
> difference between it and modulative amplification is that it is more about
> _auto-regulation_ (p168). Simondon gives the example of the perception of retinal
> images. The right eye and the left eye receive two different images, which are
> incompatible. Organising amplification is the resolution of the incompatibility,
> giving a final single image as the synthesis of the two. Simondon writes:
> ‘transduction, modulation, organization are the three levels of amplification
> of information process, through positive input [recrutement], limitation, and
> the discovery of a system of compatibility.’19
> (p. 82)

# Amplificação técnica -> social

> In pursuing this last point,
> we might further characterise these three modes of amplification, in terms
> of (1) crowd effect - e.g. crowd sourcing or crowd funding - characterized by
> transductive speed; (2) the repetition of behavioural patterns, or of particular
> units of information, which act as a relay to create more significant effects
> (e.g. marketing); (3) the self-regulation of social systems, for example the self-
> regulation of local neighbourhoods.
> (p. 82)

> The substitution of hylomorphism based
> on moulding with a theory of information and intensity based on modulation,
> renders visible a social and political reality of our time: the emergence of
> new patterns of regulation and governance which Deleuze ultimately names
> with his concept of ‘control societies’.
> (p. 83)

> Due to the lack of
> rigid regulations (which would equate with moulding), the subject conceived
> in terms of modulation and modulatory processes seems to have the freedom
> to act, even if such freedom is already anticipated by regulatory systems, and
> the free acts themselves are modulated in such a way that they take on a self-
> regulatory character.
> (p. 84)

# David Savat em "Deleuze's Objectile: From Discipline to Modulation"

> Nonetheless Savat makes
> some interesting observations concerning technological modulations under
> contemporary conditions. he summarises three mechanisms of modulation:
> (1) the recognition of patterns; (2) the anticipation of activities; (3) the
> responsibility of individuals for the organisation of working time. The first
> two mechanisms clearly correlate with key processes of regulatory control that
> we have already discussed, and the third mechanism refers back to zarafian’s
> observations, cited at the beginning of this essay, to the effect that giving
> individual workers the freedom to organise their time may be more ‘productive’
> (whether in terms of strict efficiency or of profitability) than following a strictly
> defined division of time, as in the classical Fordist/Taylorist model.
> (p. 84)

# Alexander Galloway em "Protocol: How Control Exists after Decentralisation"

![Redes](./networks.png)

> Galloway compares the
> three models of network proposed by the RAND scientist Paul Baran (namely
> centralised network, decentralised network and distributed network (see Figure
> 2)) and recognises that ‘… the distributed network is an entirely different matter.
> Distributed networks are native to Deleuze’s control societies [italics added]’.22
> Galloway suggests that the distributed network fully demonstrates the logic
> of Deleuze’s control societies.
> (p. 84)

# Governamentalidade algoritmica (Ruvroy)

> Following on from Galloway’s analysis, the Foucauldian legal scholar Antoinette
> Rouvroy goes deeper into the subject and proposes that the neoliberal mode
> of governmentality, as described by Foucault, Deleuze and Galloway, has been
> already overtaken by what she calls algorithmic governmentality. The logic of
> modulation does not only operate through infrastructures such as networks,
> but is rather embedded in all types of apparatus (for the purpose of data
> collection, recommendation, restriction). This means that as digitisation
> has pervaded into different institutions (be they local or international
> enterprises, government or non-governmental organisations) it has made
> the operation of algorithm central to any form of governance.
> (p. 85)

# Data behaviourism (Ruvroy)

> Like Savat,
> Rouvroy acknowledges the use of mechanisms such as pattern recognition
> and anticipation of user activities as fundamental to such operations, and
> identifies them all as elements of what she calls ‘data behaviourism’. Data
> behaviourism, advanced by technologies of data-collection and processing
> - now often referred to as ‘big data’ or ‘machine learning’ - has re-oriented
> neoliberal governmentality into an algorithmic process. All patterns of
> behaviour are monitored and registered as information that can be used to
> trigger social interactions on a larger scale.
> (p. 85)

# Produção de sujeitos fragmentados (Rouvroy)

> For Rouvroy, the use of algorithms in governance no longer produces
> subjectification, which means that the subject, instead of being conceived as
> a type of mini-enterprise, or defined by its income (as Foucault described the
> neoliberal subject in the Birth of Biopolitics23) instead enters into a process of de-
> subjectification, where the subject is fragmented and can no longer maintain
> a coherent identity. Rouvroy’s understanding of this modulation process is
> in fact closer to Simondon’s than Deleuze’s. That is to say, she assumes that
> human existence is modulated in such a way that it can be amplified and
> controlled, producing a significant effect when it is demodulated, meaning
> that these modulations lead to actions in everyday life. Rouvroy writes:
> > Algorithmic government thus contrasts with what we know about a
> > neoliberal mode of government which produces the subjects it needs.
> > Through the ubiquitous injunction - and its internalisation by subjects - of
> > maximisation of performance (production) and enjoyment (consumption),
> > neoliberalism produces ‘hyper-subjects’ having, as their normative
> > horizon, the continuously reiterated project of ‘becoming themselves’,
> > and passionately engaged in ‘self-control’, ‘self-entrepreneurship’ and
> > ‘self-evaluation’.24
> (p. 85)

# Infra-indivíduo e supra-indivíduo

> Rouvroy observes that this form of
> governmentality does not address the subject, but rather the infra-individual
> (which are numerised relations) and the supra-individual (meaning the
> profile automatically constructed by machines through pattern analysis).
> (p. 85)

# Desindividuação

> We
> can probably also understand this in terms of what is called disindividuation,
> meaning that the individual has lost its capacity to individuate both
> psychically and collectively.
> (p. 85-86)

> For
> Simondon, disindividuation is one of the phase of individuation, in which the
> preceding structure dissolves in favour of a new one (hence it is neutral and
> necessary);
> (p. 86)

> for Stiegler, disindividuation implies an inability to individuate
> both psychically and individually due to the destruction and dissolution
> of desire (hence it is a destructive phenomenon, or in his terms, a short-
> circuiting of desire).25 Consumer society, for Stiegler effectuates a psychic
> and collective disindividuation, consequently transforming individuals into
> mere buying-power, and the ‘we’ to the ‘they’.
> (p. 86)

# Capacidade normatizadora da sociedade algoritmica

> For Antoinette Rouvroy, this
> disindividuation has a destructive effect, in that the potentiality-possibility of
> the subject is replaced by the actuality-probability of algorithmic operations.
> For example, online marketing effectively uses user information and data to
> propose recommendations and decisions, which the user can take for granted.
> (p. 86)

> Rouvroy sees the disastrous effect of algorithmic governmentality as being
> that the subject loses the possibility to doubt what is given and to develop his
> or her own judgment. This type of modulation is also destructive of groups,
> since it only creates groups according to their behaviours. living beings have
> the capacity to modify themselves and create norms, while in the algorithmic
> modulation, norms are created by objective data.
> (p. 86)

# Relações sociais e ordens de magnitude (modulação como controle)

> The first is that modulatory processes of social control operate
> through a particular set of mechanisms which seek to understand and select
> social relations according to specific orders of magnitude, for example: inter-
> individual relations, individual-group relations, group-group relations, which
> can then be represented by corresponding technical apparatuses, or more
> precisely by corresponding data structures. The way in which such processes
> divide and classify such orders of magnitude reflects the basic assumptions on
> which their technical implementations are based. For example, with online
> recommender systems, what is understood as fundamental is individual-group
> relations, since they are fundamental to the logics of social ‘contagion’, group-
> formation, identification and cultural influence that are central to mechanisms
> of marketing and promotion.
> (p. 87)

# Auto-regulação (modulação como controle)

> Secondly systems of self-regulation which
> operate through modulation are always characterised by some teleological
> end, whether it be profit-making, the promotion of internal competition
> between group members, the preservation of the social group, etc. This
> teleological end is inscribed in the algorithms, which recursively modulate
> the social relations with precisely defined orders of magnitude and attempt
> to move the system toward ever-greater efficiency. Or more precisely, they
> promote a kind of frictionless collective and personal individuation - in which
> there is no tension experienced between different modes, sites and scales of
> individuation - by producing logical effects which recur at various social scales.
> For example, one-click shopping on Amazon based on recommendations, or
> the ‘like’ button of Facebook as a symbolic form of participation, are precisely
> mechanisms which seek to replicate particular types of personal interaction
> in coded forms and at ascending social scales.
> (p. 87)

# Multiplas ordens de magnitude e uma não fixidez teleológica em Simondon

> The key point here is that Simondon’s concept of individuation necessarily involves relations between multiple orders of magnitude. At the
> same time it is not necessarily defined by a teleological end, but rather it moves
> towards an undetermined end driven by the tendency to resolve tensions and
> incompatibilities, as Simondon illustrated with his discussion of the concept
> of organising amplification.
> (p. 88)

# Aporia em Deleuze e sua resolução

> The aporia that we set up at the beginning of this article, namely that
> between the role of modulation in Deleuze’s thinking in general and the
> specific function of modulation in Deleuze’s conceptualisation of control
> societies, can also be resolved by moving from modulation=control to
> modulation=individuation.
> (p. 88)

# Invenção de novas formas de modulação como tarefa analítico-política

> At the same time, this move opens up a
> political-analytic task for the theory of modulation. For if modulation is
> identified with control societies today, then the task for those who wish to
> find ways to supersede existing forms of social control will be to invent new
> forms of modulation that are not limited to them or by them. The current
> understanding of ‘modulation after control’, which we have encountered in
> the analyses of Galloway and Rouvroy, is one that lacks any understanding
> of tensions and incompatibilities as inherent to processes of personal and
> collective individuation, since the only modulatory process that it can
> imagine is one motivated by the cybernetic goal of maximum efficiency. The
> question now is: how can the profound concept of modulation in Deleuze
> and Simondon’s thought (e.g. its intention to re-found a metaphysics), and
> the analytical tools that they developed around this concept, be helpful in
> thinking through this political objective and its implication?
> (p. 88-89)

# Cartografia sociométrica para Jacob L. Moreno

> His belief in the value of representing social relations by
> ‘charting’ them prompted Moreno to write that, ‘as the pattern of the social
> universe is not visible to us, it is made visible through charting. Therefore
> the sociometric chart is the more useful the more accurately and realistically
> it portrays the relations discovered.’
> (p. 89)

> These relations are materialised as lines and numbers on a map. We may
> also observe that in Moreno’s methodology, every individual was considered
> a social atom; the society represented on this basis is a network composed
> of social atoms linked together by numerical relations. here we see a clear
> instance of neglect of the question of the ground, as forms are taken for
> totality. Individualism is promoted through technological networks.
> (p. 89)

# Redes sociais como Facebook estão no paradigma sociométrico

> Social networking websites like Facebook stay within
> the sociometric paradigm by materialising social relations in terms of digital
> objects, and allowing new associations based on different discovery algorithms
> to emerge. If we look at the Graph API that defines the core data structure
> of Facebook, we can immediately see its relevance to Moreno’s sociometry.
> (p. 89-90)

# Desindividuação em redes sociais

> Under the guise of being _free_ and _friendly_ to use, we can see in this example
> that the modulation of social relations can actually lead to what we have
> called ‘disindividuation’, which is not a condition of collective empowerment
> or mystical oblivion, but one in which, personally or collectively, agency as
> such is rendered unobtainable, as the coherence of personally or collectively
> individuated entities is disrupted.
> (p. 90)

> To take the universe of social media as an
> example, just consider the ways in which the attention of each social atom (or
> ‘person’) is sliced into ever smaller pieces and dispersed across networks via
> status updates, interactions, advertisements for marketing purposes. One can
> spend hours on Facebook out of curiosity without achieving anything. The
> ‘collective’ on Facebook becomes a distraction, a cause of the dissolution of
> structures within individuals, but not a site of new modes of empowerment.
> (p. 90)

# Ideia alternativa de rede social

> The introduction of the idea of collective individuation into the new model
> involved an attempt to reintroduce incompatibility and intensity into the
> modulation process. In this conceptualisation, projects - which must also
> be understood here as projections instead of telos - are prioritised instead
> of being subject to the random status updates of individuals.
> (p. 90)

> The question
> which emerges here is: how can we transform individuals into groups capable
> of actually achieving social ends? One of the answers that we proposed was
> that this could be achieved through finding mechanisms to modulate their
> relations, by deliberately setting up creative constraints, which would act as
> the grid of the triode modifying the dynamic of the flow of electrons. 
> (p. 90)

> For
> example, after registration, the user can only use the full functions when he
> or she participates in a group or creates a project; the other example is to
> limit the number of tags one can add to an object to five, so that adding a
> tag means one has to delete the less relevant one: by doing so the object can
> be described in the most updated and accurate way. This rearrangement of
> relations makes the group and project the default instead of the individual.`G
> The groups become the places where incompatibilities arise and also the
> place where they can be resolved according to the progress of the projects.
> (p. 90)

