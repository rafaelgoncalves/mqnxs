---
title: "Cultura: um conceito reacionário?"
date: 2020-06-10
path: "/cultura-Guattari"
category: comentário
author: Rafael Gonçalves
featuredImage: "../../images/Broadway_Boogie_Woogie.jpg"
srcInfo: Piet Mondrian, 1942 - <a href="https://commons.wikimedia.org/wiki/File:Piet_Mondrian,_1942_-_Broadway_Boogie_Woogie.jpg">Broadway Boogie Woogie</a>
tags: ["Félix Guattari", "cultura", "capitalismo", "Suelly Rolnik", "comentário"]
published: true
---

Esse é o título do primeiro capítulo do livro Micropolítica <a href=#fn1 class="footnote-ref" id="fnref1" role="doc-noteref"><sup>1</sup></a>. Ele me deixou bastante curioso ao propor a possibilidade de que o conceito de cultura - para mim imediatamente relacionado com discursos das esquerdas - seja de alguma forma relacionada com manutenção do *status quo*. Escrevo então alguns comentários.

<!-- more -->

O pensador faz uma separação em 3 sentidos que o conceito de cultura desenvolveu cronologicamente, mas que se expressariam hoje simultâneamente e de forma complementar:

  1. Cultura-valor
  2. Cultura-alma coletiva
  3. Cultura-mercadoria

O primeiro sentido, <em>cultura-valor</em>, diz respeito a uma certa aristocracia cultural. Algumas pessoas <strong>têm</strong> cultura: possuem uma educação formal de prestígio, falam e escrevem conforme a norma gramatical, tiveram contato com uma gama de produções intelectuais, consomem arte erudita com frequência, etc. Outras não. Esse sentido estabelece um binarismo hierárquico culto-inculto, ou ainda permite a existência de uma elite intelectual ou cultural.

<em>Cultura-alma coletiva</em> tem a ver com territórios coletivos mais ou menos secretos (discretos?) e anteriores aos indivíduos que os compõe. Para o autor é sinônimo de civilização. Aqui a cultura atua separando as pessoas em grupos, almas-coletivas: cada etnia, povo, grupo social, etc. têm suas próprias culturas (cultura negra, cultura underground, ...). E cada pessoa se encontra em categorias, cada uma com seu pacote cultural pré-estabelecido diferente.

Por fim, <em>cultura-mercadoria</em> tem a ver com a cultura que se produz, se reproduz, se modifica e é difundida. Guattari também chama de cultura de massa. Essa forma de cultura é quantitativamente medida por sistemas classificatórios como índices culturais que levam em conta número de livros/filmes produzido, quantidade de salas de cinema, etc. Esse sentido engloba tudo (pessoas, estruturas, equipamentos, …) que participam na produção de objetos semióticos (livros, filmes, etc.) difundidos num mercado de circulação monetária. "Difunde-se cultura exatamente como Coca-Cola, cigarros 'de quem sabe o que quer', carros ou qualquer coisa" ([1](#fn1), p. 17).

Assim o conceito de cultura seria reacionário na medida que <em>separa</em> atividades semióticas em esferas às quais as pessoas são submetidas (seja hierarquica, geografica ou socioeconomicamente). Essa separação é interessante em termos de capitalização de tais atividades, mas também em termos de controle (através da sujeição de indivíduos à esferas específicas pré-establecidas).

<blockquote>
Desse ponto de vista o capital funciona de modo complementar à cultura enquanto conceito de equivalência: o capital ocupa-se da sujeição econômica, e a cultura, da sujeição subjetiva. E quando falo em sujeição subjetiva não me refiro apenas à publicidade para a produção e o consumo de bens. <em> É a própria essência do lucro capitalista que não se reduz ao campo da mais-valia econômica: ela está também na tomada de poder da subjetividade.</em> (<a href=#fn1 class="footnote-ref" id="fnref1" role="doc-noteref">1</a>, p. 16)
</blockquote>

Guattari também coloca uma outra questão: com o surgimento de Ministérios da Cultura ao redor de todo o globo, desenha-se um cenário de democratização da cultura e de incentivo de formas de cultura particularizadas (pode-se dizer o mesmo do surgimento/incorporação de novos campos ou territórios subjetivos: os indivíduos, as famílias, os grupos sociais, as minorias, etc.). Mas seria isso porque o sistema capitalista se tornou "tolerante" ou alguma outra coisa?

O autor propõe que, por trás dessa produção cultural que parece se assemelhar unicamente com os sentidos 2 e 3 de cultura, não houve uma superação do sentido 1, mas apenas sua denegação. O modo como cultura é produzida, difundida e consumida parece fluir num campo de "livres trocas", sem nenhuma hierarquia estabelecida.

<blockquote>
No entanto, o que se omite aqui é que o campo social que recebe a cultura não é homogêneo. A difusão do livro, do disco, etc., não tem absolutamente a mesma significação quando veiculada nos meios de elites sociais ou nos meios de comunicação de massa, a título de formação ou de animação cultural. (<a href=#fn1 class="footnote-ref" id="fnref1" role="doc-noteref">1</a>, p. 20)
</blockquote>

Nada muda, então, em termos da existência de uma aristocracia cultural capitalística:

<blockquote>
A cultura não é apenas uma transmissão de informação cultural, uma transmissão de sistemas de modelização, mas é também uma maneira de as elites capitalísticas exporem o que eu chamaria de um mercado geral de poder. (<a href=#fn1 class="footnote-ref" id="fnref1" role="doc-noteref">1</a>, p. 20)
</blockquote>

Tendo tudo isso em vista, o autor questiona a existência de uma "cultura erudita" capitalística e uma "cultura popular" que seja emancipatória e deva ser incentivada.

<blockquote>
No fundo há somente uma cultura: a capitalística. É uma cultura sempre etnocêntrica e intelectocêntrica (logocêntrica), pois separa os universos semióticos das produções subjetivas.
<br/>
Há muitas maneiras de a cultura ser etnocêntrica, e não apenas na relação racista do tipo cultura masculina, branca, adulta, etc. Ela pode ser relativamente policêntrica ou polietnocêntrica, e preservar a postulação de uma referência de "cultura-valor", um padrão de tradutibilidade geral de produções semióticas, inteiramente paralelo ao capital.
<br/>
Assim como o capital é um modo de semiotização que permite ter um equivalente geral para as produções econômicas e sociais, a cultura é o equivalente geral para as produções de poder. As classes dominantes sempre buscam essa dupla mais-valia: a mais-valia econômica, através do dinheiro, e a mais-valia de poder, através da cultura-valor. (<a href=#fn1 class="footnote-ref" id="fnref1" role="doc-noteref">1</a>, p. 23, 24)
</blockquote>

Por fim nos resta indagar, se a questão não é mais *quem* produz cultura, o que é preciso produzir? A resposta não é simples, mas o autor nos da algumas pistas: agenciar outros modos de produção semiótica que transgrida as esferas culturais fechadas que o capitalismo nos oferece. "Modos de produção semiótica que permitam assegurar uma divisão social da produção, sem por isso fechar os indivíduos em sistemas de segregação opressora ou categorizar suas produções semióticas em esferas distintas da cultura." ([1](#fn1), p. 22)

<blockquote>
Como fazer com que essas categorias ditas "de cultura" possam ser ao mesmo tempo, altamente especializadas, singularizadas, [...] sem que haja por isso uma espécie de posse hegemônica pelas elites capitalisticas? Como fazer com que a música, a dança, a criação, todas as formas de sensibilidade, pertençam de pleno direito ao conjunto dos componentes sociais? Como proclamar um direito à singularidade no campo de todos esses níveis de produção, dita "cultural", sem que essa singularidade seja confinada num novo tipo de etnia? Como fazer para que esses diferentes modos de produção cultural não se tornem unicamente especialidade, mas possam articular-se uns aos outros, articular-se ao cojunto do campo social, articular-se ao conjunto de outros tipo de produção [...]? Como abrir - e até quebrar - essas antigas esferas culturais fechadas sobre si mesmas? Como produzir novos agenciamentos de singularização que trabalhem por uma sensibilidade estética, pela mudança da vida num plano mais cotidiano e, ao mesmo tempo, pelas transformações sociais a nível dos grandes conjuntos econômicos e sociais?
(<a href=#fn1 class="footnote-ref" id="fnref1" role="doc-noteref">1</a>, p. 22)
</blockquote>



<br/>
<section class="footnotes" role="doc-endnotes">
<hr />
<ol>
<li id="fn1" role="doc-endnote">GUATTARI, F.; ROLNIK, S. - <em>Micropolítica, cartografias do desejo.</em> 1986. Editora Vozes.<a href="#fnref1" class="footnote-back" role="doc-backlink">↩︎</a></li>
</ol>
</section>
