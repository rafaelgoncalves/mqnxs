---
title: A proposição cosmopolítica
date: 2022-09-11
path: "/proposicao-cosmopolitica"
category: fichamento
tags:
 - fichamento
 - Isabelle Stengers
 - cosmopolítica
 - política
 - química
 - metodologia

featuredImage: "Isabelle-stengers.jpg"
srcInfo: <a href="https://en.wikipedia.org/wiki/File:Isabelle-stengers.jpg">Isabelle Stengers</a>
published: true
---

Fichamento do texto "A proposição cosmopolítica"[^1] de Isabelle Stengers.

[^1]: STENGERS, I. A proposição cosmopolítica. **Revista do Instituto de Estudos Brasileiros**, n. 69, p. 442–464, 27 abr. 2018 [2007].

# Dificuldade da proposta do texto: criar uma sensibilidade diferente pela "desaceleração" e de forma diferente que uma "teoria" (generalista e autoritária)

> Como apresentar uma proposição **cujo desafio não é o de dizer o que ela é, nem de dizer o que ela deve ser, mas de fazer pensar; e que não requer outra verificação senão esta: a forma como ela terá “desacelerado” os raciocínios cria a ocasião de uma sensibilidade um pouco diferente no que concerne aos problemas e situações que nos mobilizam?** Como, portanto, separar essa proposição das questões de autoridade e de generalidade que se agenciam em torno da noção de “teoria”? Essa questão é ainda mais importante considerando que a proposição “cosmopolítica”, da maneira como eu tentarei caracterizá-la, não se destina em primeiro lugar aos “generalistas”. **Ela apenas adquire sentido nas situações concretas, lá onde trabalham os praticantes; e ela requer praticantes que – e isso é um problema político, não cosmopolítico – aprenderam a ser indiferentes às pretensões dos teóricos generalizantes, estes que tendem a definir aqueles como executantes, encarregados de “aplicar” uma teoria ou de capturar sua prática como ilustração de uma teoria**.
> (p. 443)

# Tema do texto: comparar a proposição política e a cosmopolítica (distinção, inseparabilidade)

> Essa dificuldade é uma primeira apresentação do que será um tema deste texto: a distinção e o caráter inseparável das proposições políticas e cosmopolíticas. 
> (p. 443)

# Pertinência da cosmopolítica a partir da ecologia política

> Tal como tentarei transmitir, é na medida mesmo em que se tornam pertinentes as proposições relevantes para o que podemos chamar de “ecologia política” – a entrada na política das questões de saberes ditos positivos ou das práticas relacionadas às “coisas” – que a proposição cosmopolítica pode também se tornar pertinente.
> (p. 443)

# Outro tema do texto: expor as vulnerabilidades desse tipo de proposição (como a captura teórica)

> Um outro tema deste texto, relacionado ao primeiro, será a questão da vulnerabilidade de proposições desse gênero, expostas a todos os mal-entendidos possíveis, a começar por sua tão previsível captura teórica.
> (p. 443)

# Cosmopolitismo em Kant como deficiência de partida na conceituação de cosmopolítica em Stengers

> É possível que me digam, já posso prever: não precisava, afinal, retomar um termo kantiano. Com efeito, não foi Kant que renovou o antigo tema do cosmopolitismo, visando a um projeto de tipo político no âmbito de uma “Paz perpétua”, na qual cada um “se pensaria como um membro integral da sociedade civil mundial em conformidade ao direito dos cidadãos”? Eu devo aqui me confessar culpada, pois ignorava o uso kantiano do termo quando, em 1996, enquanto trabalhava no primeiro do que se tornaria uma série de sete volumes de _Cosmopolitiques_, ele a mim se impôs. E quando eu descobri que o termo “_cosmopolitique_” afirmava a confiança kantiana em um progresso geral do gênero humano que encontraria sua expressão na autoridade de um “_jus cosmopoliticum_”, já era muito tarde..A palavra havia tomado para mim sua vida e necessidade próprias. Ela está, portanto, na acepção que dei a ela, marcada por uma deficiência de partida. Uma deficiência que eu aceito porque, de toda forma, ela apenas acentua a questão que se coloca a todo “nome” dado a uma proposição, no momento mesmo em que esse nome é retomado: ainda que se trate de um verdadeiro neologismo, esse nome será sempre vulnerável, e é normal que seja assim.
> (p. 443-4)

# Rejeição da proximidade com o termo Kantiano, exposição nos temos do personagem conceitual do Idiota (Deleuze)

> No presente caso, devo, portanto, afirmar que a proposição cosmopolítica, tal como eu a apresentarei, renega explicitamente todo parentesco com Kant ou com o pensamento antigo. O “cosmos”, no sentido que tentarei transmitir, pouco tem a ver com o mundo no qual o cidadão antigo, por toda parte, se afirmava em seu território, nem com uma terra por fim unificada, onde cada um seria cidadão. É exatamente o contrário. Por outro lado, a “proposição cosmopolítica” poderia de fato ter afinidades com uma personagem conceitual que o filósofo Gilles Deleuze fez existir com uma força tamanha que me marcou: o idiota.
> (p. 444)

> Mas o idiota de Deleuze, que ele tomou de empréstimo de Dostoïevski para dele fazer uma personagem conceitual, é aquele que sempre desacelera os outros, aquele que resiste à maneira como a situação é apresentada, cujas urgências mobilizam o pensamento ou a ação. E resiste não porque a apresentação seja falsa, não porque as urgências sejam mentirosas, mas porque “há algo de mais importante”. Que não lhe perguntemos o quê. O idiota não responderá, ele não discutirá. O idiota faz presença, ou, como diria Whitehead3 , ele coloca um interstício. Não se trata de interrogá-lo: “o que é mais importante?”. “Ele não sabe.” Mas sua eficácia não está em desfazer os fundamentos dos saberes, em criar uma noite onde todas os gatos são pardos. Nós sabemos, existem saberes, mas o idiota pede que não nos precipitemos, que não nos sintamos autorizados a nos pensar detentores do significado daquilo que sabemos.
> (p. 444)

# Proximidades com a noção de "regimes de enunciação" em Latour

> 5 Bruno Latour segue uma pista complementar, mas diferente, desviando-se da antropologia no seu sentido convencional, que tem o humano como categoria central, rumo à questão dos “regimes de enunciação”, tal como eles se distinguem das instituições históricas e de suas pretensões autojustificativas gerais de acordo com uma relação que não é do tipo “transcendental” (condições de possibilidade), mas empírica no sentido do desdobramento aventureiro da maneira como cada regime perturba, a cada vez de maneira específica, as distribuições do bom senso moderno entre o sujeito (que age, julga, conhece, crê etc.) e seus objetos (elaborados, julgados, conhecidos, imaginados etc.). O regime de enunciação não permite julgar as instituições que correspondem a ele, não é o ideal ao qual algumas [instituições] se aproximariam mais que outras, ele propõe aproximá-las de acordo com um ângulo que coloca em cena sua irredutibilidade a todas as “razões gerais”, culturais, simbólicas ou sociais. Consideradas por esse ângulo, elas devem todas aparecer de forma “surpreendente”, de maneira tal que nós deixaríamos de nos surpreender com o fato de que “os outros” tenham se valido de instituições tão diferentes. Trata-se, então, de nos desenraizarmos de nós mesmos [dépayser] para que “os outros” deixem de ser exóticos aos nossos olhos. Uma tal abordagem deveria, se exitosa, descolar de maneira bastante radical o regime de enunciação política das práticas, instituições, ideais, controvérsias que nós associamos ao político, para que deixemos de nos apresentar como tendo inventado “o político”, e isso sem no entanto chegar à conclusão de que outros povos “faziam política sem saber” (posição tradicional que implica que nós os compreendemos melhor do que eles compreendem a si próprios). Essa empreitada, delicada e arriscada, deve ser concebida como distinta da proposição cosmopolítica, mas as duas estão unidas por uma relação de entreprovação recíproca, visto que compartilham uma preocupação comum: sair de maneira não trivial (pós-moderna) das narrativas do progresso que conduzem até “nós”
> (p. 446)

# Cosmopolítica como desaceleração da construção do mundo

> É aqui que a proposição corre o risco do mal-entendido, pois o atrativo kantiano pode induzir à ideia de que se trata de uma política visando a fazer existir um “cosmos”, um “bom mundo comum”. Ora, trata-se justamente de desacelerar a construção desse mundo comum, de criar um espaço de hesitação a respeito daquilo que fazemos quando dizemos “bom”.
> (p. 446)

# Cosmos como desconhecido

> O cosmos, aqui, deve portanto ser distinguido de todo cosmos particular, ou de todo mundo particular, tal como pode pensar uma tradição particular. E ele não designa um projeto que visaria a englobá-los todos, pois é sempre uma má ideia designar um englobante para aqueles que se recusam a ser englobados por qualquer outra coisa. **O cosmos, tal qual ele figura nesse termo, cosmopolítico, designa o desconhecido que constitui esses mundos múltiplos, divergentes, articulações das quais eles poderiam se tornar capazes, contra a tentação de uma paz que se pretenderia final, ecumênica, no sentido de que uma transcendência teria o poder de requerer daquele que é divergente que se reconheça como uma expressão apenas particular do que constitui o ponto de convergência de todos.**
> (p. 446-7)

# Cosmos como operador de igualdade, não de equivalência

> Poderíamos dizer que o cosmos é um operador de colocação em igualdade [_mise à égalité_], sob a condição de dissociar radicalmente entre colocação em igualdade e colocação em equivalência [_mise en équivalence_], que implica uma medida comum, implicando a intercambialidade de posições. Pois dessa igualdade não se desdobra nenhum “e portanto...” mas, bem ao contrário, o põe em suspensão. Operar, aqui, é criar uma colocação em inquietude [_mise en inquiétude_] das vozes políticas, um sentimento de que elas não definem aquilo que discutem; que a arena política está povoada pelas sombras do que não tem, não pode ter ou não quer ter voz política: sentimento este que a boa vontade política poderia tão facilmente obliterar no momento em que uma resposta não puder ser dada à exigência “exprima-se, explicite suas objeções, suas proposições, sua relação com o mundo comum que nós construímos”.
> (p. 447)

# Proposição cosmopolítica como grito de pavor (?)

> A proposição cosmopolítica, portanto, nada tem a ver com um programa, mas, muito mais com a passagem de um pavor, que faz balbuciar as seguranças. É esse pavor que podemos escutar no grito, dizem, um dia entoado por Cromwell: “_My Brethren, by the bowels of Christ I beseech you, bethink that you may be mistaken!_”. Citar Crowell, aqui, esse tão brutal político, carrasco da Irlanda, se dirigindo aos seus irmãos puritanos, habitados por um verdade segura e vingativa, é insistir que esse balbuciamento não se merece, não traduz uma particular grandeza de alma, mas acontece. E acontece sob o modo da indeterminação, isto é, como um acontecimento ao qual nada se segue, nenhum “e portanto...”, mas coloca a cada um a questão sobre a maneira como se herdará algo disso. Claro, é aos seus irmãos, enquanto cristãos, que Cromwell se dirige, e o seu endereçamento, caso exitoso, faria existir entre eles a presença do Cristo. Mas o Cristo não é, aqui, o portador de uma mensagem em particular, sua eficácia é a de uma presença sem interação, que não apela a nenhuma transação, nenhuma negociação quanto à maneira com que ela deva ser levada em conta.
> (p. 447)

# Proposição cosmopolítica como a escuta coletiva de gritos de pavor e sussurros de idiotas e construção (em sua presença) da questão ao qual insistem

> É preciso ser cauteloso quanto à boa vontade individual. Conferir uma dimensão “cosmopolítica” aos problemas que pensamos sob o modo da política não se refere ao registro das respostas, mas coloca a questão sobre a maneira como podem ser escutados “coletivamente”, no âmbito do agenciamento através do qual se propõe uma questão política, o grito de pavor ou o sussurro do idiota. Nem o idiota, nem Cromwell apavorado, nem o homem da lei obcecado por Bartleby o sabem. Não se trata de se dirigir a eles, mas de agenciar o conjunto de maneira tal que o pensamento coletivo se construa “em presença” da questão insistente que eles fazem existir. Dar a essa insistência um nome, cosmos, inventar a maneira mediante a qual a “política”, que é a nossa assinatura, poderia fazer existir seu “duplo cósmico” [_doublure cosmique_], as repercussões disso que vai ser decidido, disso que constrói suas razões legítimas, sobre isso que permanece surdo a essa legitimidade, eis a proposição cosmopolítica.
> (p. 448)

# Exemplo concreto: experimentação animal

> Eu gostaria de citar aqui um exemplo concreto do que pode significar esse “em presença”. O exemplo concerne à questão, doravante colocada na política, da experimentação animal. Deixemos de lado os casos múltiplos nos quais podemos dizer que “há abuso”, crueldade inútil e cega ou redução sistemática dos animais de criação ao status de carne sobre patas. O que me interessa são os casos difíceis, nos quais são “colocadas na balança” a experimentação e uma causa que consideramos legítima, a luta contra uma epidemia, por exemplo.
> (p. 448-9)

> Outros [[diferente de escalas de sofrimento tidas como utilitárias]], e isso foi o que me interessou, designaram um ponto de atração inesperado. Nós sabemos que nos laboratórios onde se pratica a experimentação animal existe todo tipo de rito, de maneira de falar, de designar os animais, o que testemunha a necessidade dos pesquisadores de se protegerem. Poderíamos, ademais, nos perguntar se as grandes evocações do progresso dos conhecimentos, da racionalidade, das necessidades do método, não fazem parte desses ritos, obstruindo os interstícios por onde insiste o “o que estou fazendo” . A necessidade de “decidir” quanto à legitimidade de uma experimentação teria então por correlato a invenção de restrições destinadas ativamente a essas manobras de proteção, forçando os pesquisadores implicados a se expor, a decidir “em presença” daquilo que será eventualmente vítima de sua decisão. A proposição vai então no sentido de uma “autorregulação”, mas ela tem por interesse colocar em cena a questão do “auto”, de dar a sua plena significação ao desconhecido da questão: o que decidiria o pesquisador “por ele mesmo” se esse “ele mesmo” estivesse ativamente despojado daquilo que as decisões atuais parecem ter necessidade.
> (p. 449)

# Sobre uma perspectiva eto-ecológica

> Uma tal questão destaca uma perspectiva que chamo de “eto-ecológica” [_étho-écologique_] , que afirma a inseparabilidade do _éthos_, da maneira de se comportar própria a um ser, e do _oikos_, do hábitat desse ser, da maneira que esse hábitat satisfaz ou contraria as exigências associadas a tal _éthos_, ou oferece aos novos _éthos_ a oportunidade de se atualizarem7 . Quem diz inseparabilidade não diz dependência funcional. Um _éthos_ não é uma função do seu meio ambiente, do seu _oikos_, ele sempre será o _éthos_ do ser que se revela capaz dele. Nós não o transformaremos de modo previsível transformando o meio ambiente. Mas nenhum _éthos_ é, em si mesmo, detentor da sua própria significação, mestre de suas razões. Nós não sabemos de que um ser é capaz, do que pode se tornar capaz. **O meio ambiente, poderíamos dizer, propõe, mas é o ser que dispõe dessa proposição, que lhe dá ou lhe nega uma significação “etológica”.**
> (p. 449)

# Continuação do exemplo a luz da perspectiva eto-ecológica

> Nós não sabemos do que um pesquisador, afirmando hoje a legitimidade, mesmo a necessidade de determinada experimentação animal, poderia se tornar capaz em um _oikos_ que lhe exige pensar “em presença” das vítimas de sua decisão. O que importa é que esse devir será aquele de um pesquisador, e é nesse devir que ele produziria um acontecimento e no qual aquilo que chamo de “cosmos” poderia ser nomeado. Localmente, no caso em que a exigência ecológica seria eficaz, uma articulação terá sido criada entre o que parecia se contradizer: as necessidades da pesquisa e suas consequências para os animais que são vítimas dela. Acontecimento “cósmico”.
> (p. 449-50)

# Pretensão de saber como problema. Sussuro idiota como acontecimento que muda os termos (a situação ecológica)

> Portanto, o problema não é o dos saberes articulados, mas da pretensão que redobra esses saberes: aqueles que sabem se apresentam como pretendendo saber aquilo que sabem, como sendo capazes de conhecer de um modo independente de sua situação “ecológica”, independente do que o seu _oikos_ lhes obriga a levar em conta, ou, ao contrário, lhes permite ignorar. O que o idiota sussurra não transcende os saberes e não possui em si mesmo qualquer significação. É o modo como esse sussurro modificará eventualmente (na forma de um acontecimento) não as razões, mas a maneira como as razões daqueles que discutem se apresentam, que pode resultar nessa significação.
> (p. 450)

# Outro exemplo, a posição dos desempregados no progresso tecnocientífico e capitalista

> Assim, nós estamos inundados de discursos que nos pedem para aceitar que os fechamentos de usinas e o desemprego de milhares de trabalhadores são uma consequência dura, porém inevitável da guerra econômica. Se nossas indústrias não podem fazer “os sacrifícios” que a competitividade exige, elas serão vencidas, e nós seremos todos perdedores. Que seja, mas é preciso então nomear e honrar os desempregados como vítimas de guerra, aqueles cujo sacrifício nos permite sobreviver: cerimônias, medalhas, desfiles anuais, placas comemorativas, todas as manifestações do reconhecimento nacional, em suma, todas as manifestações de uma dívida que nenhuma vantagem financeira será suficiente para compensar lhes são devidas. Mas imagine-se as repercussões se todos os sofrimentos e mutilações impostos pela guerra (econômica) fossem assim “celebrados”, rememorados, ativamente protegidos do esquecimento e da indiferença, e não anestesiados pelos temas da flexibilidade necessária, da ardente mobilização de todos por uma “sociedade de conhecimentos” onde cada um deverá aceitar a rápida obsolescência do que sabe e tomar para si a responsabilidade permanente de sua autorreciclagem. O fato de sermos tomados por uma guerra sem perspectiva concebível de paz se tornaria talvez intolerável. Proposição “idiota”, pois não se trata de um programa para um outro mundo, de um afrontamento entre razões, mas de um diagnóstico quanto ao modo de estabilidade “eto-ecológica” deste mundo daqui.
> (p. 450-1)

# A ecologia política como necessidade de explicitar o papel político do pesquisador

> Trata-se de suscitar um “ambiente” recalcitrante a tais generalizações fatalistas: não há lugar para estar “decepcionado”, como se vivêssemos em um mundo onde as boas intenções proclamadas pudessem ser tidas por confiáveis, mas há lugar para aprender a descrever com precisão a maneira como as histórias que nós podíamos ter pensando como sendo promissoras acabam por se voltar ao fracasso, à contrafação ou à perversão: em outras palavras, construir uma experiência e uma memória ativas, compartilháveis, criadoras de exigências políticas. Para isso, é preciso, evidentemente, que os pesquisadores interessados assumam o risco de construir seus saberes de um modo que os torne “politicamente ativos”, engajados na experimentação do que pode fazer a diferença entre sucesso e fracasso ou contrafação. Não se constituirá jamais memória ou experiência sob o signo de uma neutralidade metodológica. Isso não significa “abandonar a ciência”. Jamais teria existido ciência experimental se os pesquisadores no laboratório não estivessem apaixonadamente interessados pela diferença entre aquilo que “funciona”, que cria uma relação pertinente, que produz um saber que importa, que pode interessar, e uma observação metodologicamente impecável, mas que não é suscetível de criar qualquer diferença, qualquer consequência.
> (p. 452)

# A ecologia política substitui a possibilidade de um arbítrio externo que conjuraria a possibilidade de qualquer política

> Mas a aposta eto-ecológica associada à ecologia política supõe igualmente que as práticas produtoras de saber não possuem elas próprias a necessidade de um árbitro externo, que detém a responsabilidade de fazer com que o interesse geral prevaleça. Caso contrário, a questão da diferença entre o sucesso e o fracasso ou a contrafação seria vazia, e a questão política não se colocaria. Essa aposta supõe então a possibilidade de um processo no qual a situação problemática em torno da qual se reúnem os “experts” – aqueles que possuem os meios de objetar e de propor – tenha o poder de obrigá-los. É por isso que, desde o começo, adiantei que nada disso do que eu sugiro possui o mínimo sentido se aqueles a quem me dirijo não tiverem aprendido a dar de ombros diante do poder de teorias que os define como executantes. Pois o poder das teorias é o de definir cada situação como um simples caso, isto é, proibir aos seus representantes de serem obrigados a pensar, de serem colocados em risco por esse caso.
> (p. 452)

# Aproximações e distanciamentos com o mecanicismo do mercado capitalista

> Os _stakeholders_, aqueles que possuem interesses em um novo empreendimento e que por ele se conectam, não devem ser limitados pelo que quer que seja exterior aos seus empreendimentos: o mundo emerge da multiplicidade de suas conexões heterogêneas, e essa emergência tem por única “mecânica” os entreimpedimentos que eles constituem uns para os outros. Nós sempre ressaltamos a ligação entre essa concepção da livre emergência, sem transcendência, com a mecânica. Os empreendedores (e um consumidor é igualmente um empreendedor) “compõem” à maneira de forças mecânicas, por adição; e a emergência não é outra coisa que a consequência dos obstáculos fatuais que eles constituem uns para os outros. Cada empreendedor é, então, movido por seus interesses bem definidos. Certamente, ele pode estar aberto a tudo aquilo que lhe permite avançar (ver os mecanismos de recrutamento descritos por Bruno Latour em _La science en action_). **Mas o essencial é que ele seja o homem da “oportunidade”, surdo e cego à questão do mundo de cuja construção os seus esforços participam. Com efeito, é precisamente essa desconexão das escalas – aquelas dos indivíduos e aquela do que, juntos, eles fazem emergir – que permite a matematização do “mercado” como composição automática, maximizando uma função que os economistas escolherão assimilar ao bem coletivo. Toda intrusão em nome de um outro princípio de composição, mas também toda “escuta/acordo [_entente_]”, isto é, toda distância em relação à surdez, podem então ser colocados no mesmo saco: eles não serão descritos, mas condenados, pois todos têm o efeito de diminuir aquilo que o “livre mercado” maximiza (poder do teorema matemático).**
> (p. 454)

# Shareholders x stakeholders

> É isso que o Greenpeace bem compreendeu quando opôs aos _stakeholders_ aquilo que ele nomeou como _shareholders_, palavra um pouco infeliz, pois ter “fatias de mercado” é ter um interesse bem definido, mas que vale pelo seu contraste: trata-se de dar voz àqueles que pretendem “tomar parte”, “participar”, mas em nome daquilo que emerge, em nome das consequências, das repercussões, de tudo aquilo cujos diferentes interesses fazem a economia. Em poucas palavras, trata-se de opor aos empreendedores – definidos pelos seus interesses, por aquilo que os concerne – aqueles que “se envolvem com aquilo que não deveria concernir a ninguém”, aquilo que não deveria intervir na composição de forças. 
> (p. 454-5)

# Proposição cosmopolítica levaria as ideias de economia política para a realidade institucional (como?)

> A questão, certamente, é política, e o direito de empreender permanece hoje, desse ponto de vista, como a primeira palavra. O princípio de precaução tende a limitar um pouco esse direito, mas ele o respeita antes de mais nada: para limitá-lo, é preciso estar em jogo a saúde humana ou os danos graves e/ ou irreversíveis ao meio ambiente. Não há, então, lugar para a questão dos _shareholders_: em que mundo queremos viver?, mas apenas desenvolvimento da possibilidade de uma posição defensiva. Certamente, a ideia de “sustentabilidade” vai mais longe, mas não nos surpreendamos que se trate apenas de uma ideia: a sua aplicação efetiva transformaria o “direito” de empreender em “proposição”, e implicaria que as ideias da ecologia política se tornassem realidade institucional.
> (p. 455)

# Nem a emergência mecânica do mercado, mas também não à emergência harmônica do modelo biológico

> Eu destaquei o caráter mecânico da emergência por composição dos interesses. **Eu seguirei essa pista a fim de verificar se as ciências da natureza nos fornecem outros modelos de emergência sem transcendência**. Certamente, o primeiro que se apresenta é o modelo biológico: a vida democrática poderia ser assimilada à harmoniosa participação de cada um em um corpo único... Velha ideia, muito sedutora, à qual convém, entretanto, resistir. **Pois esse corpo, a serviço do qual cada um encontraria sua verdade e sua realização, é uma péssima mistura, antipolítica, de naturalismo e de religião.**
> (p. 455)

> **O ideal de uma composição harmônica poderia ser caracterizado como o “outro” do espírito de empresa, um sonho (não é assim que as sociedades tradicionais funcionam) que se torna um pesadelo no momento em que ele busca a sua própria realização, pois ele se limita a inverter os polos do modelo mecânico em relação a uma invariante**. O que não varia é que a composição não tem necessidade de pensamento político, de dúvida, de imaginação quanto às consequências. O corpo “sabe mais”, ele é o cosmos, um cosmos realizado, não aquilo que insiste no sussurro do idiota, daquele que duvida. E, de maneira previsível, a intuição, o instinto, o sentir imediato serão celebrados contra os artifícios do pensamento.
> (p. 455-6)

> Se o “cosmos” pode nos proteger de uma versão “empreendedorista” da política, acolhendo apenas os interesses bem definidos, que têm os meios para se entreimpedir, nós vemos no momento que a política pode nos proteger de um cosmos misantropo, de um cosmos que se comunica diretamente com uma verdadeira oposição aos artifícios, hesitações, divergências, desmedidas, conflitos associados às desordens humanas. O modelo de harmonia biológica é bastante esmagador. **Pensar o que emerge é resistir tanto à composição mecânica de forças indiferentes quanto à composição harmônica do que apenas encontra a sua verdade no fazer corpo.**
> (p. 456)

# O corpo químico (da química do século XVIII) como alternativa ao modelo de corpo físico (mecanicismo) ou biológico

> Falar da arte do químico é se voltar não para a química contemporânea, que frequentemente se pensa como uma espécie de “física aplicada”, mas para aquela química do século XVIII, que alguns pensadores das Luzes (notadamente Diderot, e certamente não Kant, que é muito mais o abajur dessa aventura das Luzes) opuseram ao modelo mecânico, ao ideal de uma definição teórica dos corpos químicos da qual se deveria deduzir o modo como eles entram em reação (esse “ideal” está longe de ser alcançado pela química contemporânea). **Se existe arte, é porque os corpos químicos são definidos como “ativos”, mas sua atividade não pode ser atribuída a eles, ela depende das circunstâncias e pertence à arte dos químicos criar tipos de circunstâncias nos quais os corpos se tornarão capazes de produzir o que o químico deseja: arte de catálise, de ativação, de moderação.**
> (p. 456)

# Fazer com que as coisas realizem algo espontaneamente seria uma modulação? ou outra coisa? Centrado na eficácia? (produtivismo?)

> Se vocês lerem o bonito livro de François Jullien, _La propension des choses_, descobrirão uma arte de emergência bastante próxima daquela do químico. Com efeito, nesse livro se encontra descrito o modo como os chineses honram aquilo que menosprezamos, **a manipulação, a arte da disposição que permite aproveitar-se da propensão das coisas, de “dobrá-las” de tal sorte que elas realizem “espontaneamente” o que o artista, o homem de guerra ou o homem político desejam. Fora da oposição entre submissão e liberdade, um pensamento centrado na eficácia.**
> (p. 456)

# Questão cosmopolítica: como favorecer a emergência do que seria capaz de ser importante?

> Dir-se-á que esse é um estranho modelo para o político, mas esse sentimento de estranheza traduz nossa ideia de que a “boa” política deveria encarnar uma forma de emancipação universal: removam a alienação que separava os humanos da sua liberdade e vocês obterão qualquer coisa parecida com uma democracia. A ideia de uma arte, mesmo de uma “técnica” política, é, portanto, anátema, artifício que separa o humano da sua verdade. **Referir-se à arte do químico é afirmar que o encontro político não tem nada de espontâneo. O que chamamos de democracia ou é a forma menos ruim de gerir o rebanho humano ou é uma aposta centrada na questão não do que são os humanos, mas do que eles podem se tornar capazes**. É a questão que John Dewey colocou no centro da sua vida: como “favorecer”, “cultivar” os hábitos democráticos? E essa questão, considerando que a referência à química propõe colocá-la de maneira técnica, pode ser prolongada pela questão “cosmopolítica”: **como, por quais artifícios, por quais procedimentos, desacelerar a ecologia política, conferir uma eficácia ao sussurro do idiota, esse “há algo de mais importante” que é tão facilmente ignorado, já que ele não pode ser “levado em conta”, pois o idiota nem objeta nem propõe nada que “conta”. A questão é “eto-ecológica”: qual _oikos_ pode dar lugar à emergência do que seria capaz de “importar”, o que não pode ser imposto?**
> (p. 456-7)

# Operar na ordem do acontecimento

> Não é uma questão de boa vontade, individual ou coletiva, pois o que se trata de pensar é da ordem do acontecimento. Mas o acontecimento não remete, certamente, à inspiração inefável, à revelação súbita. Ele não se opõe à explicação. É o alcance da explicação que se encontra transformado, articulado ao registro da arte e não da dedução. **Não se explica um acontecimento [_événement_], mas o acontecimento se explica a partir daquilo que terá sabido nele criar um lugar**.
> (p. 457)

> 10 Diante da emergência de um sentido um pouco enigmático quando da tradução dessa frase para o português, pedimos à autora que nos ajudasse a esclarecê-la. Sua resposta, cuja tradução e publicação nos foram autorizadas a nosso pedido, acrescenta esclarecimentos, tal como segue: “O acontecimento se explica: é preciso entender que é o acontecimento ele mesmo que produz a maneira como ele será caracterizado – e a partir daquilo que o situa. O que significa: um acontecimento não possui “em si mesmo” o poder de fazer acontecimento, mas ele também não é apenas relativo à situação no sentido geral, como se pudéssemos deduzi-lo dessa situação (explicação sociológica). **A situação não explica o acontecimento, ela poderia ter acolhido outros [em seu lugar]. Mas ela permite compreender a criação do lugar que é aquele _deste_ acontecimento, isto é, compreender a maneira como este acontecimento se situará. O acontecimento se situa ativamente em relação à situação, ele intervém na situação**. Ele se ampara em certos elementos da situação e cria um lugar, ou um ponto de vista, que permite caracterizá-la. Por exemplo, na época de Galileu, a situação é aquela de uma crise da autoridade, mas o acontecimento Galileu, isto é, a criação de um lugar onde essa questão da autoridade é ao mesmo tempo dramatizada (todas as nossas interpretações são ficções?) e encontra uma solução pela intervenção do fato experimental – que faz autoridade. Galileu ‘faz acontecimento’ propondo os fatos como solução geral à crise. Ele cria o ‘laboratório’ como lugar do acontecimento ‘nascimento da ciência como capaz por de acordo os humanos’. O acontecimento se explica a partir do tipo de fato obtido no laboratório no sentido novo do termo, no sentido em que o laboratório é o lugar onde se inventam dispositivos experimentais que dão ‘o poder de conferir às coisas o poder de conferir ao experimentador o poder de falar em seu nome’ (_Invention des sciences modernes_, p.102). Na China, onde a situação não era aquela de uma crise de autoridade, a ciência que os missionários apresentaram fez sorrir os letrados, mas os chineses muito apreciaram os relógios...”. (N.T.)
> (p. 457)

# Modelos sociais/políticos (física, biologia e química)

**Mecanicismo (modelo da física):**

 - leis universais emergem de interações individuais
 - o todo é explicado pelas partes
 - naturalização da forma mercado (neoliberalismo)
 - economicismo ("lei da oferta e demanda", etc.)
 - não é preciso pensar o todo, pois ele é uma emergência automática


**Funcionalismo (modelo da biologia):**

 - funcionamentos particulares compõem um todo harmônico
 - as partes são explicadas pelo todo
 - mistura de naturalismo e religião, isento de pensamento político
 - funcionalismo de Durkheim
 - não é preciso pensar o todo, pois ele já está dado


**Associativismo (modelo da química):**

 - reações que acontecem a partir da criação de situações
 - as partes e o todo se relacionam em acontecimentos (emergências contingentes à partir de associações de ativos em situações propícias)
 - perspectiva eto-ecológica (criar um _oikos_ propício para um determinado _éthos_)
 - é preciso pensar o todo (arte de criar situações para os ativos - fazer fazer / manipulação), então se imporia uma desaceleração/hesitação
 - Outras questões:
    - pensar na química como experimentação e alquimia
    - natureza problemática, heterogênea, dinâmica (instável, estável ou metaestável) dos processos químicos (e sociais)
    - opera no nível do acontecimento (contingência, criação, novo? - penso em D&G) != explicação
    - existe algo de espontaneidade (o artificial como o natural suscitado em Simondon)
    - pensamento centrado na eficácia (?)
       - em que medida isso difere do utilitarismo?
       - seria um pragmatismo?
    - em que medida o fazer fazer se diferencia da modulação (entendido como controle contínuo de um processo) em Deleuze?
    - como o modelo da química (que exige o químico que pensa) seria uma abordagem imanente?
       - o químico como agente de uma perspectiva parcial / situacional (Haraway)?
       - em que medida isso se diferenciaria do mecanicismo do mercado? as situações não são leis, mas potências

# Sobre o dispositivo do "discurso" (?)

> Eu penso principalmente naquilo que pude aprender com o dispositivo do “discurso” e com a maneira com que ele faz intervir o que eu chamaria, em poucas palavras, a ordem do mundo. **O que há de muito interessante é que esse dispositivo ritual, que parece supor a existência de uma ordem do mundo que dará a sua justa solução a uma situação problemática, não confere qualquer autoridade a essa ordem. Se existe um discurso [_palabre_], é porque aqueles que se reúnem, aqueles que são reconhecidos como sabendo de alguma coisa dessa ordem não sabem, nesse caso específico, como ela vai se passar. Se eles estão reunidos, é por conta de uma situação em relação à qual nenhum de seus saberes é suficiente**. Portanto, a ordem do mundo não é um argumento, ela é aquilo que confere aos participantes um papel que os “des-psicologiza”, que faz com que eles não se apresentem como “proprietários” das suas opiniões, mas como aqueles que se encontram todos igualmente habilitados a testemunhar que o mundo possui uma ordem. É por isso que ninguém discute o que diz o outro, não contesta, não questiona a pessoa.
> (p. 458)

> Do ponto de vista dos saberes dos antigos químicos, o fato de que o discurso [_palabre_] não pede aos protagonistas que decidam, mas que determinem como se passa aqui a ordem do mundo, confere a essa ordem um papel que seria aquele do ácido que desagrega os corpos e lhes permite entrar em proximidade, e também do fogo que os ativa. **Em poucas palavras, pode-se caracterizar a ordem do mundo em termos de eficácia: ela constrange cada um a se produzir, a fabricar-se a si mesmo, a partir de um modo que confere ao problema em torno do qual eles se reúnem o poder de gerar pensamento, um pensamento que não pertencerá a ninguém e a ninguém dará razão.**
> (p. 458)

# Sobre a magia na contemporaneidade e sua eficácia (imanência radical)

> Nós continuamos a falar de magia nos diversos registros. Nós falaremos da magia negra dos grandes rituais nazistas, mas também da magia de um momento, de um livro, de um olhar, de tudo que torna capaz de pensar e sentir de outra maneira. O termo magia, entretanto, não é pensado, e o mesmo acontece com todas palavras associadas à sua eficácia. **Para as bruxas, nomear-se bruxas e definir a sua arte por essa palavra, “magia”, já são atos “mágicos”, que criam uma experiência desconfortável para todos aqueles que vivem em um mundo onde supostamente a página foi definitivamente virada, com a erradicação de tudo que foi desqualificado, menosprezado, destruído, enquanto triunfava o ideal de uma racionalidade pública, de um homem idealmente mestre de suas razões, logo acompanhado da trivialidade da psicologia dita científica com suas pretensões de identificar aquilo a que as razões humanas obedecem.** Ousar nomear de “magia” a arte de suscitar os acontecimentos nos quais está em jogo um “tornar-se capaz” é aceitar que se deixe ecoar em nós um grito que pode lembrar aquele de Cromwell: o que fizemos, o que continuamos fazendo quando utilizamos palavras que nos fazem os herdeiros daqueles que erradicaram as bruxas.
> (p. 458)

> A sua eficácia é bem antes aquela de catalisar um regime de pensamento e de sentir que confere àquilo que importa, àquilo em torno do que se dá a reunião, o poder de se tornar causa de pensamento. **A eficácia do ritual não é, portanto, a convocação de uma deusa que inspiraria a resposta, mas a convocação daquilo cuja presença transforma as relações que cada protagonista entretém com os seus próprios saberes, esperanças, medos, memórias, e permite ao conjunto fazer emergir o que cada um, separadamente, não teria sido capaz de produzir**. _Empowerment_, produção graças ao coletivo, de partes capazes daquilo que elas não teriam sido capazes sem ele. **Arte de imanência radical, mas a imanência é precisamente aquilo que está para se criar, sendo o regime usual de pensamento aquele da transcendência que autoriza posição e julgamento.** 
> (p. 459)

# Questão de eficácia na cosmopolítica (?) o vírus e a inundação

> A ecologia política afirma que não existe conhecimento que seja ao mesmo tempo pertinente e separado: **não é de uma “definição objetiva”, de um vírus ou de uma inundação que podemos ter necessidade, mas daqueles cujas práticas foram engajadas de modos múltiplos “com” esse vírus ou “com” esse rio**. Mas cabe à perspectiva cosmopolítica colocar a questão da eficácia, que poderia estar associada ao “não existe” da ecologia política, e de conceber a cena política a partir dessa questão. Como o processo de emergência da decisão política pode, ao mesmo tempo, ser ativamente protegido da ficção segundo a qual “os seres humanos de boa vontade decidem em nome do interesse geral”, e ativado pela obrigação de colocar o problema associado ao vírus ou ao rio “em presença” daquele que, do contrário, correria o risco de ser desqualificado como “interesse egoísta”, nada tendo a propor e criando um obstáculo à “conta comum” em formação?
> (p. 459)

# O heterogêneo na arte do químico

> Mas a arte do químico possui um outro aspecto que pode nos guiar: é uma arte da heterogeneidade, da colocação em presença de corpos enquanto heterógenos. Esse aspecto é levado em conta no discurso [_palabre_] (interdição de retornar às intenções de quem fala, isto é, ao “em comum” que permite que um pretenda compreender o outro) e ele foi objeto de muitas atenções em todos os grupos nos quais, como no caso das “bruxas”, o _empowerment_ está em jogo. Papéis foram criados, cujos constrangimentos criam as salvaguardas que protegem a emergência dos tipos de entendido e mal-entendido “espontâneos” que dominam nossas reuniões de “boa vontade”. É essa noção de papéis heterogêneos que eu vou prolongar.
> (p. 460)

# O papel do expert

> O _expert_ é aquele cuja prática não é ameaçada pelo problema discutido, e seu papel exigirá dele que se apresente, e apresente aquilo que sabe, de um modo que não prejulgue a maneira como esse conhecimento será levado em conta. Um tal constrangimento faz provação, pois ele se opõe ativamente ao conjunto das retóricas que ligam um conhecimento às pretensões que costumam curto-circuitar o político com temas tais como definição objetiva do problema, abordagem racional, progresso etc. É o conhecimento no sentido de que ele se propõe como pertinente, suscetível de se articular a outros conhecimentos, de ter necessidade de outros conhecimentos para encontrar uma significação em relação ao problema posto, que é acolhido; e toda pretensão de lhe atribuir uma autoridade, de apresentá-lo como aquilo cuja decisão deveria poder ser deduzida, será “notada”: o _expert_ “saindo do seu papel” suscitará dúvidas quanto à confiabilidade da sua contribuição. **O papel constitui, portanto, uma verdadeira provação, e é inútil dizer que essa provação implica uma modificação bastante drástica da atividade profissional de pesquisador: o tipo de segurança conferido por um paradigma, no sentido de Kuhn, se torna aqui uma deficiência, pois o _expert_ “sob paradigma” verá toda situação como atribuindo um lugar central ao seu paradigma.**
> (p. 460-1)

# O papel do diplomata

> A aposta eto-ecológica é a de que o expert possa aceitar esse constrangimento, pois, seja qual for a forma como a decisão integrará o seu saber, isso não é colocado em questão. Em contrapartida, o diplomata está lá para dar voz àqueles cuja prática, o modo de existência, o que comumente chamamos de identidade, estão ameaçados por uma decisão. “Se vocês decidirem isso, vocês nos destruirão”, um tal anúncio é corrente e pode advir de toda parte, inclusive dos grupos que, em outros casos, delegam experts. Mas muitas vezes ouvimos: “reflexo identitário” ou “expressão de interesses corporativos (portanto, egoístas)”, e respondemos: é o preço do progresso ou do interesse geral. A diplomacia intervém usualmente entre a guerra provável e a paz possível, e tem o grande interesse de definir os potenciais beligerantes segundo o modo da igualdade. **O papel dos diplomatas é, portanto, antes de mais nada, o de suspender a anestesia produzida pela referência ao progresso ou ao interesse geral, o de dar voz àqueles que se definem como ameaçados, de um modo a fazer hesitar os experts, a obrigá-los a pensar na possibilidade de que as suas decisões sejam um ato de guerra.** 
> (p. 461)

# A consulta como prática cosmopolítica

> Nós estamos de novo lidando com uma aposta “eto-ecológica”, que corresponde ao risco de “dar uma chance à paz”. **Que um povo seja solenemente consultado nos termos que colocam sua identidade em causa, ou que um invisível seja consultado, nos dois casos o _oikos_ próprio à consulta coloca em suspenso os hábitos que nos fazem pensar que sabemos aquilo que sabemos e quem somos, que detemos o sentido daquilo que nos faz existir.** A identidade, então, não é um obstáculo, mas uma condição do exercício diplomático, ao menos a identidade “civilizada”, que sabe como consultar, como criar o momento da interrogação acerca daquilo que a sustenta (interrogação não reflexiva no sentido de que não refletimos “sobre”, mas “interrogamos” o que faz sustentar junto, o que obriga, o que importa à prova da modificação proposta). 
> (p. 461-2)

# Em um caso há a a consulta e no outro não?

> Não há diferença de natureza entre os que delegam _experts e os que enviam diplomatas_, mas sim uma diferença em relação à situação.
> (p. 462)

# Cosmopolítica não como consenso, mas como uma proibição do esquecimento (não ser levado em conta)

> Não mais que a entrada em cena da diplomacia, a presença das vítimas não garante evidentemente o que quer que seja: a proposição cosmopolítica nada tem a ver com o milagre de decisões “que colocam todo o mundo de acordo”. O que aqui importa é a proibição do esquecimento, ou pior, da humilhação. Notadamente, aquela que produz a ideia indigna de que uma compensação financeira deveria ser suficiente, essa tentativa obscena de dividir as vítimas, de isolar os relutantes se dirigindo antes àqueles que, por uma razão ou outra, aceitarão se curvar mais facilmente. Tudo terminará talvez com o dinheiro, mas não “pelo” dinheiro, pois o dinheiro não fecha a conta. Aqueles que se reúnem devem saber que nada poderá apagar a dívida que liga sua eventual decisão às suas vítimas.
> (p. 462)

# O cosmos como possibilidade do murmúrio do idiota, do constrangimento, da desaceleração (sem a qual não pode haver criação)

> Eu apresentei, no início deste texto, o “cosmos” como um operador de igualdade por oposição a toda noção de equivalência. Os papéis que acabo de caracterizar sumariamente correspondem a essa ideia. Nenhuma situação problemática se dirige a protagonistas definidos como intercambiáveis, de forma que entre eles uma medida comum permitiria colocar na balança os interesses e argumentos. A igualdade não significa que todos possuem “igualmente o mesmo direito de voz”, mas que todos devem estar presentes de um modo que confira à decisão o seu grau máximo de dificuldade, que proíba qualquer atalho, qualquer simplificação, qualquer diferenciação a priori entre aquilo que conta e aquilo que não conta. **O cosmos, tal como ele figura na proposição cosmopolítica, não possui representante, ninguém fala em seu nome e ele não pode ser feito objeto de nenhum procedimento de consulta. O seu modo de existência se traduz pelo conjunto dos modos de fazer, dos artifícios cuja eficácia é a de expor aqueles que terão que decidir, de constrangê-los a esse pavor que eu associei ao grito de Cromwell**. Em resumo, trata-se de abrir a possibilidade de que ao murmúrio do idiota se responda não, por certo, com a definição “daquilo que há de mais importante”, mas com a desaceleração sem a qual não pode haver criação.
> (p. 463)

# Cosmopolítica como movimento de contraposição, não como política universal

> Eu ressaltei que a proposição cosmopolítica não é uma proposição vale-tudo, aquela que “nós” poderíamos apresentar a todos como igualmente aceitável por todos. Ela é muito mais uma maneira de civilizar, de tornar “apresentável” essa política que temos, um pouco exageradamente, a tendência de pensar como um ideal neutro, bom para todos. 
> (p. 463)

# Chamados universais a unidade como decisões precipitadas (com a definição da legitimidade de autores confiáveis de um outro mundo)

> O chamado à unidade ontem endereçado aos trabalhadores de todos os países, ou hoje aos cidadãos de um novo regime cosmopolita de tipo kantiano12, **comunica de maneira precipitada o grito “um outro mundo é possível!” com a definição da legitimidade daqueles serão os autores confiáveis desse outro mundo.**
> (p. 463)

# Sobre o senso comum e a urgência do argumento da ecologia política (~Latour em como distinguir amigos...). A proposição cosmopolítica como método de colocar em suspenso as explicações ("e portanto...").

> Nós não somos confiáveis! E sobretudo quando pretendemos participar da necessária criação de um “_senso comum cosmopolítico_, um espírito de reconhecimento da alteridade do outro capaz de apreender as tradições étnicas, nacionais e religiosas e de fazer com que elas se beneficiem de suas trocas mútuas”. E sobretudo quando a necessidade dessa criação de um “bom mundo comum”, onde cada um estaria apto e pronto para ver “com os olhos do outro”, se funda naquilo que deve ser aceito por todos: **não mais em um interesse geral sempre discutível, mas em um argumento de peso que constitui a urgência por excelência, a sobrevida da própria humanidade**.
> (p. 463)

> Pensar a partir dessas consequências ditas secundárias, temerosas face à ideia de que um senso comum qualquer possa aplainar, pacificar a questão sempre delicada, hesitante entre a guerra e a paz, que é todo encontro entre heterogêneos, não é certamente responder à urgência. É mesmo idiota, não apenas do ponto de vista das mobilizações proclamadas em nome da urgência, mas também em face da urgência ela mesma, inegável. **É preciso ousar dizer que o murmúrio do idiota cósmico é indiferente ao argumento da urgência como a qualquer outro. Ele não o nega, ele apenas coloca em suspenso os “e portanto...” dos quais nós, tão plenos de boa vontade, tão empreendedores, sempre prontos a falar por todos, somos os mestres.**
> (p. 464)
