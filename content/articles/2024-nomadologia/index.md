---
title: "1227 - Tratado de nomadologia"
date: 2024-05-08
path: "/nomadologia"
category: fichamento
author: Rafael Gonçalves
featuredImage: "img.jpg"
srcInfo: Carruagem nômade inteiramente em madeira, Altai, séc. V-/V a. C.
tags: 
 - Gilles Deleuze
 - Félix Guattari
 - Gabriel Tarde
 - Pierre Clastres
 - nomadismo
 - estado
 - capitalismo
 - máquina de guerra
 - filo maquínico
 - linhagem técnica
 - tecnologia
 - guerra
 - ciência
 - Mil Platôs

published: true
---

Fichamento do capítulo "1227 - Tratado de nomadologia: a máquina de guerra"[^1] do livro _Mil Platôs_ de Gilles Deleuze e Félix Guattari.

[^1]: DELEUZE, G.; GUATTARI, F. 1227 - Tratado de nomadologia: a máquina de guerra. **Em: Mil platôs: capitalismo e esquizofrenia 2**, vol. 5. 2. ed. São Paulo: Editora 34, 2012. 

# Síntese das proposições

_Axioma I: A máquina de guerra é exterior ao aparelho de Estado._

_Proposição I: Essa exterioridade é confirmada, inicialmente, pela mitologia, a epopeia, o drama e os jogos._

_Problema I: Existe algum meio de conjurar a formação de um aparelho de Estado (ou de seus equivalentes num grupo)?_

_Proposição II: A exterioridade da máquina de guerra é igualmente confirmada pela etnologia (homenagem à memória de Pierre Clastres)._

_Proposição III: A exterioridade da máquina de guerra é confirmada ainda pela epistemologia, que deixa pressentir a existência e a perpetuação de uma “ciência menor” ou “nômade”._

_Problema II: Existe algum meio de subtrair o pensamento ao modelo de Estado?_

_Proposição IV: A exterioridade da máquina de guerra é confirmada finalmente pela noologia._

_Axioma II: A máquina de guerra é a invenção dos nômades (por ser exterior ao aparelho de Estado e distinta da instituição militar). A esse título, a máquina de guerra nômade tem três aspectos: um aspecto espacial-geográfico, um aspecto aritmético ou algébrico, um aspecto afectivo._

_Proposição V: A existência nômade efetua necessariamente as condições da máquina de guerra no espaço._

_Proposição VI: A existência nômade implica necessariamente os elementos numéricos de uma máquina de guerra._

_Proposição VII: A existência nômade tem por “afectos” as armas de uma máquina de guerra._

_Problema III: Como os nômades inventam ou encontram suas armas?_

_Proposição VIII: A metalurgia constitui por si mesma um fluxo que concorre necessariamente para o nomadismo._

_Axioma III: A máquina de guerra nômade é como a forma de expressão, e a metalurgia itinerante seria a forma de conteúdo correlativa._

_Proposição IX: A guerra não tem necessariamente por objeto a batalha, e, sobretudo, a máquina de guerra não tem necessariamente por objeto a guerra, ainda que a guerra e a batalha possam dela decorrer necessariamente (sob certas condições)._

# Uma (concepção de) ciência implica uma organização do campo social (em particular na divisão de trabalho)

> A diferença não é extrínseca: **a maneira pela qual uma ciência, ou uma concepção da ciência, participa na organização do campo social, e em particular induz uma divisão do trabalho, faz parte dessa mesma ciência.**
> (p. 37)

# Ciência régia ~ modelo hilemórfico (forma organizadora, matéria preparada)

> **A ciência regia é inseparável de um modelo “hilemórfico”**, que implica ao mesmo tempo uma forma organizadora para a matéria, e uma matéria preparada para a forma; com frequência **mostrou-se como esse esquema derivava menos da técnica ou da vida que de uma sociedade dividida em governantes-governados, depois em intelectuais-manuais.**
> (p. 37)

> 28: Gilbert Simondon levou muito longe a análise e a crítica do esquema hilemórfico, e de seus pressupostos sociais (“a forma corresponde a que o homem que comanda pensou em si mesmo e que deve exprimir de maneira positiva quando dá suas ordens: a forma é, por conseguinte, da ordem do exprimível”). **A esse esquema forma-matéria, Simondon opõe um esquema dinâmico, matéria provida de singularidades-forças ou condições energéticas de um sistema. O resultado é uma concepção inteiramente distinta das relações ciência-técnica.** Cf. _L’individu et sa gênese physico-biologique_, PUF, pp. 42-56.
> (p. 38)

# O cômpar (ciência régia/matéria-forma/constantes) e o díspar (ciência nômade/material-forças/variáveis) como dois modelos de ciência. Oposição entre logos e nomos.

> Seria preciso opor dois modelos científicos, à maneira de Platão no _Timeu_. Um se denominaria _Cômpar_, e o outro _Díspar_. **O cômpar é o modelo legal ou legalista adotado pela ciência regia. A busca de leis consiste em pôr constantes em evidência, mesmo que essas constantes sejam apenas relações entre variáveis (equações). O esquema hilemórfico está baseado numa forma invariável das variáveis, numa matéria variável do invariante.** **Porém o díspar, como elemento da ciência nômade, remete mais ao par material-forças do que ao da matéria-forma. Já não se trata exatamente de extrair constantes a partir de variáveis, porém de colocar as próprias variáveis em estado de variação contínua. Se há ainda equações, são adequações, inequações, equações diferenciais irredutíveis à forma algébrica, e inseparáveis por sua vez de uma intuição sensível da variação. Captam ou determinam singularidades da matéria em vez de constituir uma forma geral.Operam individuações por acontecimentos ou hecceidades, e não por “objeto” como composto de matéria e de forma; as essências vagas não são senão hecceidades.** Com respeito a todos esses aspectos, há uma oposição entre o _logos_ e o _nomos_, entre a lei e o nomos, que permite dizer que a lei tem ainda “um ranço demasiado moral”. 
> (p. 38)

# Força gravitacional ~ espaço laminar, estriado, homogêneo e centrado (multiplicidades métricas, arborescentes)

> Em suma, parece que a força gravitacional está na base de um espaço laminar, estriado, homogêneo e centrado; ela condiciona precisamente as multiplicidades ditas métricas, arborescentes, cujas grandezas são independentes das situações e se exprimem com a ajuda de unidades ou de pontos (movimentos de um ponto a outro).
> (p. 39)

# Espaço liso, heterogêneo, tátil (multiplicidades não-métricas, acentradas, rizomáticas)

> O espaço liso é justamente o do menor desvio: por isso, só possui homogeneidade entre pontos infinitamente próximos, e a conexão das vizinhanças se faz independentemente de qualquer via determinada. **E um espaço de contato, de pequenas ações de contato, táctil ou manual, mais do que visual, como era o caso do espaço estriado de Euclides.** O espaço liso é um campo sem condutos nem canais. **Um campo, um espaço liso heterogêneo, esposa um tipo muito particular de multiplicidades: as multiplicidades não métricas, acentradas, rizomáticas, que ocupam o espaço sem “medi-lo”, e que só se pode explorar “avançando progressivamente”.** Não respondem à condição visual de poderem ser observadas desde um ponto do espaço exterior a elas: **por exemplo, o sistema dos sons, ou mesmo das cores, por oposição ao espaço euclidiano.**
> (p. 40)

# Hegelianismo de direita (até Max Weber) racional-técnico (Estado moderno ~ Razão). Obedece e será senhor (pois estará obedecendo a razão) ~ neoliberalismo.

> 36: Há um hegelianismo de direita que continua vivo na filosofia política oficial, e que solda o destino do pensamento e do Estado. Kojève (_Tyrannie et sagesse_, Gallimard) e Eric Weil (_Hegel et l'Etat_; _Philosophie politique_, Vrin) são seus representantes recentes. **De Hegel a Max Weber desenvolveu-se toda uma reflexão sobre as relações do Estado moderno com a Razão, a um só tempo como racional-técnico e como razoável-humano. Se se objeta que essa racionalidade, já presente no Estado imperial arcaico, é o optimum dos próprios governantes, os hegelianos respondem que o racional-razoável não pode existir sem um mínimo de participação de todos. Mas a questão é antes de saber se a própria forma do racional-razoável não é extraída do Estado, de maneira a dar-lhe necessariamente “razão”.**
> (p. 47)

> É uma curiosa troca que se produz entre o Estado e a razão, mas essa troca é igualmente uma proposição analítica, **visto que a razão realizada se confunde com o Estado de direito, assim como o Estado de fato é o devir da razão.** Na filosofia dita moderna e no Estado dito moderno ou racional, tudo gira em torno do legislador e do sujeito. **É preciso que o Estado realize a distinção entre o legislador e o sujeito em condições formais tais que o pensamento, de seu lado, possa pensar sua identidade.**
> (p. 47)

> Obedece sempre, pois quanto mais obedeceres, mais serás senhor, visto que só obedecerás à razão pura, isto é, a ti mesmo... Desde que a filosofia se atribuiu ao papel de fundamento, não parou de bendizer os poderes estabelecidos, e decalcar sua doutrina das faculdades dos órgãos de poder do Estado. **O senso comum, a unidade de todas as faculdades como centro do Cogito, é o consenso de Estado levado ao absoluto.**
> (p. 47)

# Como falar em raça sem cair em racismo? Só existe raça oprimida, inferior, minoritária. Raça como impureza elencada por um sistema de dominação. Não se deve tentar reencontrar a raça, ela é raça pois é nômade num espaço liso, o pensamento como devir.

> Desde logo, vê-se bem os perigos, as ambiguidades profundas que coexistem com esse empreendimento [[ em propor uma complementariedade dissimétrica entre uma tribo-raça e um espaço-meio (ex: Celtas e o Oriente) que inspiraria um pensamento nômade (que arrasta a literatura inglêsa para o que se tornará a literatura americana) ]], como se cada esforço e cada criação se confrontasse com uma infâmia possível, pois, **como fazer para que o tema de uma raça não se transforme em racismo, em fascismo dominante e englobante ou, mais simplesmente, em aristocratismo, ou então em seita e folclore, em micro-fascismos? E como fazer para que o polo Oriente não seja um fantasma que reative, de maneira distinta, todos os fascismos, todos os folclores também, yoga, zen e karatê?** Certamente não basta viajar para escapar ao fantasma; e decerto não é invocando o passado, real ou mítico, que se escapa ao racismo.
> (p. 53)

> Mas, ainda aí, os critérios de distinção são fáceis, sejam quais forem as misturas de fato que obscurecem em tal ou qual nível, em tal ou qual momento. **A tribo-raça só existe no nível de uma raça oprimida, e em nome de uma opressão que ela sofre: só existe raça inferior, minoritária, não existe raça dominante, uma raça não se define por sua pureza, mas, ao contrário, pela impureza que um sistema de dominação lhe confere.**
> (p. 53)

> **Bastardo e mestiço são os verdadeiros nomes da raça.** Rimbaud disse tudo sobre esse ponto: só pode autorizar-se da raça aquele que diz: “Sempre fui de raça inferior, (...). sou de raça inferior por toda a eternidade, (...). eis-me na praia armoricana, (...). sou um animal, um negro, (...). sou de raça longínqua, meus pais eram escandinavos”. **E assim como a raça não é algo a ser reencontrado, o Oriente não é algo a ser imitado: ele só existe graças à construção de um espaço liso, assim como a raça só existe graças à constituição de uma tribo que a povoa e a percorre.** Todo o pensamento é um devir, um duplo devir, em vez de ser o atributo de um Sujeito e a representação de um Todo.
> (p. 53-4)

# Desterritorialização como relação com a terra no nômade (reterritorialização na desterritorialização). A terra como suporte.

> É nesse sentido que o nômade não tem pontos, trajetos, nem terra, embora evidentemente ele os tenha. Se o nômade pode ser chamado de o Desterritorializado por excelência, é justamente porque a reterritorialização não se faz depois, como no migrante, nem em outra coisa, como no sedentário (com efeito, a relação do sedentário com a terra está mediatizada por outra coisa, regime de propriedade, aparelho de Estado...). **Para o nômade, ao contrário, é a desterritorialização que constitui sua relação com a terra, por isso ele se reterritorializa na própria desterritorialização. É a terra que se desterritorializa ela mesma, de modo que o nômade aí encontra um território. A terra deixa de ser terra, e tende a tornar-se simples solo ou suporte.** A terra não se desterritorializa em seu movimento global e relativo, mas em lugares precisos, ali mesmo onde a floresta recua, e onde a estepe e o deserto se propagam.
> (p. 56)

# Destruição do Estado (Oriente) x Transformação do Estado (Ocidente). As duas ideias conciliam mal (crítica a ideia de ditadura do proletariado?). Socialismo autoritário (proletariado como força de trabalho) x Anarquismo (proletariado como força de nomadização)

> 54: As observavões de Karl Marx sobre as formações despóticas na Ásia são confirmadas pelas análises africanas de Max Gluckman (_Custam and Conflictm África_, Oxford): ao mesmo tempo, imutabilidade formal e rebelião constante. **A ideia de uma “transformação” do Estado parece claramente ocidental. Não obstante, a outra ideia, de uma “destruição” do Estado, remete muito mais ao Oriente, e às condições de uma máquina de guerra nômade. Por mais que se apresente as duas ideias como fases sucessivas da revolução, são diferentes demais e conciliam-se mal; elas resumem a oposição das correntes socialistas e anarquistas no século XIX.** O próprio proletariado ocidental é considerado de dois pontos de vista: **enquanto deve conquistar o poder e transformar o aparelho de Estado, representa o ponto de vista de uma força de trabalho, mas, enquanto quer ou quereria uma destruição do Estado, representa o ponto de vista de uma força de nomadização.** Mesmo Marx define o proletariado não apenas como alienado (trabalho), mas como desterritorializado. **O proletário, sob esse último aspecto, aparece como o herdeiro do nômade no mundo ocidental. Não só muitos anarquistas invocam temas nomádicos vindos do Oriente, mas sobretudo a burguesia do século XIX identifica de bom grado proletários e nômades, e assimilam Paris a uma cidade assediada pelos nômades** (cf. Louis Chevalier, _Classes laborieuses et classes dangerenses_, LGF, pp. 602-604).
> (p. 62)

# Estriar o espaço liso como tarefa fundamental do Estado. Controlar migrações. Regulação das regulações e fluxos.

> **Uma das tarefas fundamentais do Estado é estriar o espaço sobre o qual reina, ou utilizar os espaços lisos como um meio de comunicação a serviço de um espaço estriado. Para qualquer Estado, não só é vital vencer o nomadismo, mas controlar as migrações e, mais geralmente, fazer valer uma zona de direitos sobre todo um “exterior”, sobre o conjunto dos fluxos que atravessam o ecúmeno.** Com efeito, sempre que possível **o Estado empreende um processo de captura sobre fluxos de toda sorte, de populações, de mercadorias ou de comércio, de dinheiro ou de capitais, etc.** Mas são necessários trajetos fixos, com direções bem determinadas, que limitem a velocidade, que regulem as circulações, que relativizem o movimento, que mensurem nos seus detalhes os movimentos relativos dos sujeitos e dos objetos.
> (p. 63)

# Espaço liso associado a Guerra total (criado pelo Estado)

> É verdade que esse novo nomadismo [[ a reconstituição do espaço liso pelo Estado no mar, no ar e em toda terra vista como grande deserto ]] acompanha uma máquina de guerra mundial cuja organização extravasa os aparelhos de Estado, e chega aos complexos energéticos, militares-industriais, multinacionais. **Isto para lembrar que o espaço liso e a forma de exterioridade não têm uma vocação revolucionária irresistível, mas, ao contrário, mudam singularmente de sentido segundo as interações nas quais são tomados e as condições concretas de seu exercício ou de seu estabelecimento (por exemplo, a maneira pela qual a guerra total e a guerra popular, ou mesmo a guerrilha, lançam mão de métodos).**
> (p. 65)

# O número como forma de dominar a matéria (cálculo) no Estado, submetê-la a um _spatium_ imperial ou a uma _extensio_ moderna

> **A aritmética, o número, sempre tiveram um papel decisivo no aparelho de Estado: já era o caso na burocracia imperial, com as três operações conjugadas do recenseamento, do censo e da eleição.** E com mais forte razão, as formas modernas do Estado não se desenvolveram sem utilizar todos os cálculos que surgiam na fronteira entre a ciência matemática e a técnica social (todo um cálculo social como base da economia política, da demografia, da organização do trabalho, etc). **Este elemento aritmético do Estado encontrou seu poder específico no tratamento de qualquer matéria: matérias-primas, matérias segundas dos objetos trabalhados, ou a última matéria, constituída pela população humana.O número sempre serviu, assim, para dominar a matéria, para controlar suas variações e seus movimentos, isto é, para submetê-los ao quadro espaço-temporal do Estado — seja _spatium_ imperial, seja _extensio_ moderna.** O Estado tem um princípio territorial ou de desterritorialização, o qual liga o número a grandezas métricas (tendo em conta métricas cada vez mais complexas que operam a sobrecodificação). Não acreditamos que o Número tenha podido encontrar aí as condições de uma independência ou de uma autonomia, ainda que aí tenha encontrado todos os fatores de seu desenvolvimento.
> (p. 68-9)

# Estatística como número numerado, não numerante. Especificidade da organização numérica (existência nômade, função-máquina-de-guerra).

> **Número numerante, móvel, autônomo, direcional, rítmico, cifrado: a máquina de guerra é como a consequência necessária da organização nômade** (Moisés fará a experiência disso com todas as suas consequências). Critica-se hoje essa organização numérica de maneira apressada demais, nela denunciando-se uma sociedade militar ou mesmo concentracionária, onde os homens já não passam de “números” desterritorializados. Mas isto é falso. Horror por horror, **a organização numérica dos homens certamente não é mais cruel do que a das linhagens ou dos Estados. Tratar os homens como números não é forçosamente pior do que tratá-los como árvores que se talha, ou figuras geométricas que se recorta e modela. Bem mais, o uso do número como dado, como elemento estatístico, é próprio do número numerado de listado, não do número numerante.** E o mundo concentracionário opera tanto por linhagens e territórios, quanto por numeração. **A questão não é, portanto, do bom e do ruim, mas da especificidade.** A especificidade da organização numérica vem do modo de existência nômade e da função-máquina de guerra. **O número numerante se opõe ao mesmo tempo aos códigos de linhagem e à sobrecodificação de Estado. A composição aritmética vai, de um lado, selecionar, extrair das linhagens os elementos que entrarão no nomadismo e na máquina de guerra; de outro lado, vai dirigi-las contra o aparelho de Estado, vai opor uma máquina e uma existência ao aparelho de Estado, traçar uma desterritorialização que atravessa a um só tempo as territorialidades de linhagem, e o território ou a desterritoriali-dade de Estado.**
> (p. 70-1)

# Sobre o phylum maquínico

> Pôde-se falar de um “ecossistema”, que não se situa apenas na origem, e onde as ferramentas de trabalho e as armas de guerra trocam suas determinações: parece que o mesmo phylum maquínico atravessa umas e outras.
> (p. 77)

# O agenciamento maquínico como concretização do elemento técnico abstrato

> Mas **o princípio de toda tecnologia é mostrar como um elemento técnico continua abstrato, inteiramente indeterminado, enquanto não for reportado a um _agenciamento_ que a máquina supõe.** A máquina é primeira em relação ao elemento técnico: **não a máquina técnica que é ela mesma um conjunto de elementos, mas a máquina social ou coletiva, o agenciamento maquínico que vai determinar o que é elemento técnico num determinado momento, quais são seus usos, extensão, compreensão..., etc.**
> (p. 81)

# Relação entre agenciamento concreto e _phylum_ abstrato. Características internas aos artefatos remetem a agenciamentos específicos.

> É por intermédio dos agenciamentos que o _phylum_ seleciona, qualifica e mesmo inventa os elementos técnicos, de modo que não se pode falar de armas ou ferramentas antes de ter definido os agenciamentos constituintes que eles supõem e nos quais entram. **É nesse sentido que dizemos que as armas e as ferramentas não se distinguem apenas de maneira extrínseca, e contudo não têm características distintivas intrínsecas. Têm características internas (e não intrínsecas) que remetem aos agenciamentos respectivos nos quais são tomados.** O que efetua um modelo de ação livre não são, portanto, as armas em si mesmas e no seu ser físico, mas o agenciamento “máquina de guerra” como causa formal das armas.
> (p. 81)

# Agenciamentos são composições de desejo. Só há desejo agenciado.

> Os agenciamentos são passionais, são composições de desejo. O desejo nada tem a ver com uma determinação natural ou espontânea, só há desejo agenciando, agenciado, maquinado. **A racionalidade, o rendimento de um agenciamento não existem sem as paixões que ele coloca em jogo, os desejos que o constituem, tanto quanto ele os constitui.**
> (p. 83-4)

# Afecto (máquina de guerra) x sentimento (Estado?)

> Mas o regime da máquina de guerra é antes a dos _afectos_, que só remetem ao móvel em si mesmo, a velocidades e a composições de velocidade entre elementos. O afecto é a descarga rápida da emoção, o revide, ao passo que o sentimento é uma emoção sempre deslocada, retardada, resistente. Os afectos são projéteis, tanto quanto as armas, ao passo que os sentimentos são introceptivos como as ferramentas. Há uma relação afetiva com a arma, da qual dão testemunho não apenas as mitologias, mas a canção de gesta, o romance de cavalaria e cortês. 
> (p. 84)

# Necessidade de um conceito de linhagem tecnológica: os phylums maquínicos

> Afinal de contas, o que torna as discussões tão difíceis (tanto para o caso controverso do estribo como para o caso seguro do sabre) não são apenas os preconceitos sobre os nômades, é a ausência de **um conceito suficientemente elaborado de linhagem tecnológica (o que define uma _linhagem ou continuum tecnológico_, e sua extensão variável desde tal ou qual ponto de vista?).**
> (p. 92)

> **É possível falar de um _phylum maquínico_, ou de uma linhagem tecnológica, a cada vez que se depara com um conjunto de singularidades, prolongáveis por operações, que convergem e as fazem convergir para um ou vários traços de expressão assinaláveis.** Se as singularidades ou operações divergem, em materiais diferentes ou no mesmo, é preciso distinguir dois phylums diferentes: por exemplo, justamente para a espada de ferro, proveniente do punhal, e o sabre de aço, proveniente da faca.
> (p. 93)

> Cada _phylum_ tem suas singularidades e operações, suas qualidades e traços, que determinam a relação do desejo com o elemento técnico (os afectos “do” sabre não são os mesmos que os da espada).
> (p. 93-4)

# Phylum maquínico como única linhagem filogenética (fluxo matéria-movimento). Agenciamentos como singulariedades extraídas do fluxo (invenção).

> **Mas sempre é possível instalar-se no nível de singularidades prolongáveis de um phylum a outro, e reunir ambos. No limite, não há senão uma única e mesma linhagem filogenética, um único e mesmo phylum maquínico, idealmente contínuo: o fluxo de matéria-movimento, fluxo de matéria em variação contínua, portador de singularidades e traços de expressão. Esse fluxo operatório e expressivo é tanto natural como artificial: é como a unidade do homem com a Natureza.** Mas, ao mesmo tempo, não se realiza aqui e agora sem dividir-se, diferenciar-se. **Denominaremos agenciamento todo conjunto de singularidades e de traços extraídos do fluxo — selecionados, organizados, estratificados — de maneira a convergir (consistência) artificialmente e naturalmente: um agenciamento, nesse sentido, é uma verdadeira invenção.**
> (p. 94)

# Agenciamentos (culturas, idades, etc.) recortam o phylum, e o phylum os atravessa. Ação seletiva dos agenciamentos sobre o phylym, reação evolutiva do phylum

> **Os agenciamentos podem agrupar-se em conjuntos muito vastos que constituem “culturas”, ou até “idades”; nem por isso deixam de diferenciar o phylum ou o fluxo, dividindo-o em outros tantos phylums diversos, de tal ordem, em tal nível, e introduzem as descontinuidades seletivas na continuidade ideal da matéria-movimento.** Os agenciamentos recortam o phylum em linhagens diferenciadas distintas e, ao mesmo tempo, o phylum maquínico os atravessa todos, abandona um deles para continuar num outro, ou faz com que coexistam. **Tal singularidade enterrada nos flancos de um phylum, por exemplo a química do carvão, será trazida à superfície por tal agenciamento que a seleciona, a organiza, a inventa, e graças ao qual, então, todo o phylum, ou parte dele, passa em tal lugar e em tal momento.** Em qualquer caso será preciso distinguir muitas linhas diferentes: **umas, filogenéticas, passam a longa distância por agenciamentos de idades e culturas diversas (da zarabatana ao canhão? do moinho de orações ao de hélice? do caldeirão ao motor?); outras, ontogenéticas, são internas a um agenciamento, e ligam seus diversos elementos, ou então fazem passar um elemento, frequentemente com um tempo de atraso, a um outro agenciamento de natureza diferente, mas de mesma cultura ou de mesma idade (por exemplo, a ferradura que se propaga nos agenciamentos agrícolas).** É preciso, pois, levar em conta a ação seletiva dos agenciamentos sobre o phylum, e a reação evolutiva do phylum, sendo este o fio subterrâneo que passa de um agenciamento a outro, ou sai de um agenciamento, arrasta-o e o abre.
> (p. 94-5)

# Mais que elan vital (Bergson), tendência universal (Leroi-Gurhan)

> Impulso vital? **Leroi-Gourhan foi o mais longe num vitalismo tecnológico que modela a evolução técnica pela evolução biológica em geral: uma Tendência universal, encarregada de todas as singularidades e traços de expressão, atravessa meios internos e técnicos que a refratam ou a diferenciam, segundo singularidades e traços retidos, selecionados, reunidos, tornados convergentes, inventados por cada um. Há, com efeito, um phylum maquínico em variação que cria os agenciamentos técnicos, ao passo que os agenciamentos inventam os phylums variáveis.** Uma linhagem tecnológica muda muito, segundo seja traçada no phylum ou inscrita nos agenciamentos; mas os dois são inseparáveis.
> (p. 95)

> 84: Leroi-Gourhan, _Milieu et techniques_, Albin Michel, pp. 356 ss. Gilbert Simondon retomou, acerca de séries curtas, a questão das “origens absolutas de uma linhagem técnica”, ou da criação de uma “essência técnica”: _Du mode d’existence des objets techniques_, Aubier, pp. 41 ss.
> (p. 95)

# Matéria movimento: essências materiais e vagas (Husserl)

> Portanto, como definir essa matéria-movimento, essa matéria-energia, essa matéria-fluxo, essa matéria em variação, que entra nos agenciamentos, e que deles sai? É uma matéria desestratificada, desterritorializada. **Parece-nos que Husserl fez o pensamento dar um passo decisivo quando descobriu uma região de essências materiais e vagas, isto é, vagabundas, anexatas e no entanto rigorosas, distinguindo-as das essências fixas, métricas e formais.** Vimos que essas essências vagas se distinguem tanto das coisas formadas como das essências formais. Constituem conjuntos vagos. Desprendem uma corporeidade (materialidade) que não se confunde nem com a essencialidade formal inteligível, nem com a coisidade sensível, formada e percebida. **Essa corporeidade tem duas características: de um lado é inseparável de passagens ao limite como mudanças de estado, de processos de deformação ou de transformação operando num espaço-tempo ele mesmo anexato, agindo à maneira de acontecimentos (ablação, adjunção, projeção..).; de outro lado, é inseparável de qualidades expressivas ou intensivas, suscetíveis de mais e de menos, produzidas como afectos variáveis (resistência, dureza, peso, cor...).**
> (p. 95-6)

# Essências vagas como afetos-limiares / processos-limite. Intermediário autônomo.

> Há, portanto, um acoplamento ambulante acontecimentos-afetos que constitui a essência corpórea vaga, e que se distingue do liame sedentário “essência fixa-propriedades que dela decorrem na coisa”, “essência formal-coisa formada”. Sem dúvida Husserl tinha tendência a fazer da essência vaga uma espécie de intermediário entre a essência e o sensível, entre a coisa e o conceito, um pouco como o esquema kantiano. **O redondo não seria uma essência vaga ou esquemática, intermediária entre as coisas arredondadas sensíveis e a essência conceituai do círculo? dom efeito, o redondo só existe como afeto-limiar (nem plano nem pontudo) e como processo-limite (arredondar), através das coisas sensíveis e dos agentes técnicos, mó, torre, roda, rodinha, alvado...** Mas, então, ele só é “intermediário” se o intermediário for autônomo, ele mesmo estendendo-se primeiro entre as coisas e entre os pensamentos, para instaurar uma relação totalmente nova entre os pensamentos e as coisas, uma vaga identidade entre ambos.
> (p. 95-6)

> 
